<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Instance_Core {

	public function __construct($name)
	{
		$this->_name = $name;
		$this->_class_prefix = strtolower($this->_name).'_';
	}
	
	public function is_allowed($config)
	{
		if (isset($config['acl_resource']))
		{
			// If resource is not allowed do not add it to menu
			if ( ! A2::instance()->allowed($config['acl_resource'], Arr::get($config, 'acl_privilege', 'manage')))
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	public function config($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			return Kohana::config('jellyrest');
		}

		return Arr::get(Kohana::config('jellyrest'), $key, $default);
	}

	/**
	 * Remove XSS from user input.
	 * Changes from Security::xss_clean, allows iframe tags
	 *
	 *     $str = Admin::xss_clean($str);
	 *
	 * @author     Christian Stocker <chregu@bitflux.ch>
	 * @copyright  (c) 2001-2006 Bitflux GmbH
	 * @param   mixed  string or array to sanitize
	 * @return  string
	 * @deprecated  since v3.0.5
	 */
	public function xss_clean($str)
	{
		// http://svn.bitflux.ch/repos/public/popoon/trunk/classes/externalinput.php
		// +----------------------------------------------------------------------+
		// | Copyright (c) 2001-2006 Bitflux GmbH                                 |
		// +----------------------------------------------------------------------+
		// | Licensed under the Apache License, Version 2.0 (the "License");      |
		// | you may not use this file except in compliance with the License.     |
		// | You may obtain a copy of the License at                              |
		// | http://www.apache.org/licenses/LICENSE-2.0                           |
		// | Unless required by applicable law or agreed to in writing, software  |
		// | distributed under the License is distributed on an "AS IS" BASIS,    |
		// | WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or      |
		// | implied. See the License for the specific language governing         |
		// | permissions and limitations under the License.                       |
		// +----------------------------------------------------------------------+
		// | Author: Christian Stocker <chregu@bitflux.ch>                        |
		// +----------------------------------------------------------------------+
		//
		// Kohana Modifications:
		// * Changed double quotes to single quotes, changed indenting and spacing
		// * Removed magic_quotes stuff
		// * Increased regex readability:
		//   * Used delimeters that aren't found in the pattern
		//   * Removed all unneeded escapes
		//   * Deleted U modifiers and swapped greediness where needed
		// * Increased regex speed:
		//   * Made capturing parentheses non-capturing where possible
		//   * Removed parentheses where possible
		//   * Split up alternation alternatives
		//   * Made some quantifiers possessive
		// * Handle arrays recursively

		if (is_array($str) OR is_object($str))
		{
			foreach ($str as $k => $s)
			{
				$str[$k] = $this->xss_clean($s);
			}

			return $str;
		}

		// Remove all NULL bytes
		$str = str_replace("\0", '', $str);

		// Fix &entity\n;
		$str = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $str);
		$str = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $str);
		$str = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $str);
		$str = html_entity_decode($str, ENT_COMPAT, Kohana::$charset);

		// Remove any attribute starting with "on" or xmlns
		$str = preg_replace('#(?:on[a-z]+|xmlns)\s*=\s*[\'"\x00-\x20]?[^\'>"]*[\'"\x00-\x20]?\s?#iu', '', $str);

		// Remove javascript: and vbscript: protocols
		$str = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $str);
		$str = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $str);
		$str = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $str);

		// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
		$str = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#is', '$1>', $str);
		$str = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#is', '$1>', $str);
		$str = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#ius', '$1>', $str);

		// Remove namespaced elements (we do not need them)
		$str = preg_replace('#</*\w+:\w[^>]*+>#i', '', $str);
		// Get tag filter from config
		$tag_filter = $this->config('xss_tag_filter');
		$tag_filter = '#</*('.$tag_filter.')[^>]*+>#i';

		do
		{
			// Remove really unwanted tags
			$old = $str;
			$str = preg_replace($tag_filter, '', $str);
		}
		while ($old !== $str);

		return $str;
	}

	/**
	 * Converts Jelly Model to Jelly REST Model.
	 *
	 * @param   Jelly_Model  $model
	 * @return  Jelly_REST_Model
	 */
	public function model(Jelly_Model $model)
	{
		$model_name = Jelly::model_name($model);
		
		if ( ! isset($this->_models[$model_name]))
		{
			if (class_exists($this->_class_prefix.Jelly::model_prefix().$model_name))
			{
				$rest_class = $this->_class_prefix.Jelly::model_prefix().$model_name;
			}
			elseif (class_exists(Jelly_REST::$default_class_prefix.Jelly::model_prefix().$model_name))
			{
				$rest_class = Jelly_REST::$default_class_prefix.Jelly::model_prefix().$model_name;
			} 
			else
			{
				$rest_class = 'jelly_rest_model';

				$parent_class = Jelly::class_name($model);
				while (($parent_class = strtolower(get_parent_class($parent_class))) != 'jelly_model')
				{
					$tmp_model_name = Jelly::model_name($parent_class);
					if (class_exists($this->_class_prefix.Jelly::model_prefix().$tmp_model_name))
					{
						$rest_class = $this->_class_prefix.Jelly::model_prefix().$tmp_model_name;
						break;
					}
					elseif (class_exists(Jelly_REST::$default_class_prefix.Jelly::model_prefix().$tmp_model_name))
					{
						$rest_class = Jelly_REST::$default_class_prefix.Jelly::model_prefix().$tmp_model_name;
						break;
					}
				}
			}

			$this->_models[$model_name] = new $rest_class($this->_name);
		}

		$rest_object = clone $this->_models[$model_name];
		// Assign jelly model
		$rest_object->initialize($model);

		return $rest_object;
	}

	/**
	 * Converts Jelly Field to Jelly REST Field.
	 *
	 * @param   Jelly_Field  $field
	 * @return  Jelly_REST_Field
	 */
	public function field(Jelly_Field $field)
	{
		$class = strtolower(get_class($field));

		if ( ! isset($this->_fields[$class]))
		{
			if (class_exists($this->_class_prefix.$class))
			{
				$rest_class = $this->_class_prefix.$class;
			}
			elseif (class_exists(Jelly_REST::$default_class_prefix.$class))
			{
				$rest_class = Jelly_REST::$default_class_prefix.$class;
			}
			else
			{
				$rest_class = 'jelly_rest_field';

				$parent_class = $class;
				while (($parent_class = strtolower(get_parent_class($parent_class))) != 'jelly_field')
				{
					if (class_exists($this->_class_prefix.$parent_class))
					{
						$rest_class = $this->_class_prefix.$parent_class;
						break;
					}
					elseif (class_exists(Jelly_REST::$default_class_prefix.$parent_class))
					{
						$rest_class = Jelly_REST::$default_class_prefix.$parent_class;
						break;
					}
				}
			}

			$this->_fields[$class] = new $rest_class($this->_name);
		}

		$rest_object = clone $this->_fields[$class];
		// Assign jelly model
		$rest_object->initialize($field);

		return $rest_object;
	}

	/**
	 * Converts Jelly Collection to Jelly REST Collection.
	 *
	 * @param   Jelly_Collection  $collection
	 * @param	string|Jelly_Model for collection
	 * @return  Jelly_REST_Collection
	 */
	public function collection(Jelly_Collection $collection, $model)
	{
		$model_name = Jelly::model_name($model);

		if ( ! isset($this->_collections[$model_name]))
		{
			if (class_exists($this->_class_prefix.'collection_'.$model_name))
			{
				$rest_class = $this->_class_prefix.'collection_'.$model_name;
			}
			elseif (class_exists(Jelly_REST::$default_class_prefix.'collection_'.$model_name))
			{
				$rest_class = Jelly_REST::$default_class_prefix.'collection_'.$model_name;
			}
			else
			{
				$rest_class = 'jelly_rest_collection';

				$parent_class = Jelly::class_name($model);
				while (($parent_class = strtolower(get_parent_class($parent_class))) != 'jelly_model')
				{
					$tmp_model_name = Jelly::model_name($parent_class);
					if (class_exists($this->_class_prefix.'collection_'.$tmp_model_name))
					{
						$rest_class = $this->_class_prefix.'collection_'.$tmp_model_name;
						break;
					}
					elseif (class_exists(Jelly_REST::$default_class_prefix.'collection_'.$tmp_model_name))
					{
						$rest_class = Jelly_REST::$default_class_prefix.'collection_'.$tmp_model_name;
						break;
					}
				}
			}

			$this->_collections[$model_name] = new $rest_class($this->_name);
		}

		$rest_object = clone $this->_collections[$model_name];
		// Assign jelly collection and jelly model
		$rest_object->initialize($collection, Jelly::factory($model_name));

		return $rest_object;
	}
	
	/**
	 * @var string	Specific name for this instance
	 */
	protected $_name;
	
	/**
	 * @var string	Class prefix used for classes called from this REST instance
	 */
	protected $_class_prefix;
	
	/**
	 * @var	array	List of Jelly_REST_Model objects
	 */
	protected $_models = array();

	/**
	 * @var	array	List of Jelly_REST_Collection objects
	 */
	protected $_collections = array();

	/**
	 * @var	array	List of Jelly_REST_Field objects
	 */
	protected $_fields = array();

}

