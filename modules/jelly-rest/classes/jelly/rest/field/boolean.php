<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field BelongsTo class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_Boolean extends Jelly_REST_Field {

	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		return array(
			'value' => ($value) ? $this->_jelly_field->label_true : $this->_jelly_field->label_false
		);
	}

}

