<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field Timestamp class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_Timestamp extends Jelly_REST_Field {

	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		return array(
			'value' => ( ! isset($this->_jelly_field->default) && empty($value)) ? '' : date($this->_jelly_field->pretty_format, $value)
		);
	}

}

