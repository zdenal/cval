<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field ManyToMany class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_ManyToMany extends REST_Field_Relationship {

	/**
	 * @var bool	Is relationship multiple
	 */
	public $multiple = TRUE;

	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		return array(
			'value' => $value->count(),
			'relation' => array(
				'model' => $this->_jelly_field->foreign['model'],
				'count' => $value->count(),
			)
		);
	}

}

