<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field Enum class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_Enum extends Jelly_REST_Field {

	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		if ((is_int($value) OR is_string($value)) AND array_key_exists($value, $this->_jelly_field->choices))
		{
			$value = Arr::get($this->_jelly_field->choices, $value, $this->_jelly_field->default);
		}
		else
		{
			$value = $this->_jelly_field->default;
		}
		
		return array(
			'value' => $value
		);
	}

}

