<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_Core {

	/**
	 * @param	string	Name of associated REST instance
	 */
	public function __construct($rest_name)
	{
		$this->_rest_name = $rest_name;
	}
	
	/**
	 * Returns associated REST instance object.
	 * 
	 * @return	Jelly_REST_Instance
	 */
	public function rest()
	{
		return Jelly_REST::instance($this->_rest_name);
	}

	public function initialize(Jelly_Field $field)
	{
		$this->_jelly_field = $field;
	}

	public function render_meta()
	{
		return $this->_render_meta();
	}

	public function render(Jelly_Model $model)
	{
		return $this->_render($model);
	}
	
	/**
	 * @var	string	Name of associated REST instance
	 */
	protected $_rest_name;
	
	/**
	 * @var Jelly_Field
	 */
	protected $_jelly_field;

	protected function _render_meta()
	{
		return array(
			'type' => str_replace(Jelly::field_prefix(), '', strtolower(get_class($this->_jelly_field))),
			'label' => $this->_jelly_field->label,
			'name' => $this->_jelly_field->name,
			'required' => $this->_jelly_field->required()
		);
	}
	
	protected function _render(Jelly_Model $model)
	{
		$value = $this->_value($model);

		return array_merge(array(
			'value' => $value
		), $this->_render_specific($model, $value));
	}
	
	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		return array();
	}
	
	/**
	 * Returns field values as members of the object.
	 * 
	 * @param	Jelly_Model $model
	 * @return	mixed
	 * @uses	Jelly_Model::__get()
	 */	
	protected function _value(Jelly_Model $model)
	{
		return $model->__get($this->_jelly_field->name);
	}
	
	/**
	 * Gets the internally represented value from a field or unmapped column.
	 * 
	 * @param	Jelly_Model $model
	 * @return	mixed
	 * @uses	Jelly_Model::get()
	 */
	protected function _value_internal(Jelly_Model $model)
	{
		return $model->get($this->_jelly_field->name);
	}

}

