<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field HasOne class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_HasOne extends REST_Field_Relationship {

	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		if ($value->loaded())
		{
			return array(
				'value' => $value->id(),
				'relation' => array(
					'model' => $this->_jelly_field->foreign['model'],
					'id' => $value->id(),
					'label' => $value->name()
				)
			);
		}
		else
		{
			return array(
				'value' => NULL,
				'relation' => array(
					'model' => $this->_jelly_field->foreign['model']
				)
			);
		}
	}

}

