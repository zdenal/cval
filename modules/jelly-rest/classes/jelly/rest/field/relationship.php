<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Field Relationship class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Field_Relationship extends Jelly_REST_Field {

	/**
	 * @var bool	Is relationship multiple
	 */
	public $multiple = FALSE;

	protected function _render_meta()
	{
		return array_merge(parent::_render_meta(), array(
			'relation' => array(
				'model' => $this->_jelly_field->foreign['model'],
				'multiple' => $this->multiple
			)
		));
	}

}

