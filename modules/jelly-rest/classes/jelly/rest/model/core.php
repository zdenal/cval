<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Model class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Model_Core {

	/**
	 * @param	string	Name of associated REST instance
	 */
	public function __construct($rest_name)
	{
		$this->_rest_name = $rest_name;
	}
	
	/**
	 * Returns associated REST instance object.
	 * 
	 * @return	Jelly_REST_Instance
	 */
	public function rest()
	{
		return Jelly_REST::instance($this->_rest_name);
	}
	
	public function initialize(Jelly_Model $model)
	{
		$this->_jelly_model = $model;
	}

	public function render(array $fields = array())
	{
		$data = array(
			'model'		=> $this->_jelly_model->meta()->model(),
			'id'		=> $this->_jelly_model->id(),
			'label'		=> $this->_jelly_model->name(),
			//'url'		=>
		);

		$data_fields = array();

		foreach ($fields as $field)
		{
			$data_fields[$field->name] = $this->rest()->field($field)->render($this->_jelly_model);
		}

		$data['fields'] = $data_fields;

		return $data;
	}

	public function render_meta(array $fields = array())
	{
		$data = array(
			'model' => $this->_jelly_model->meta()->model(),
			'label' => $this->_jelly_model->meta()->label()
		);

		$data_fields = array();

		foreach ($fields as $field)
		{
			$data_fields[$field->name] = $this->rest()->field($field)->render_meta();
		}

		$data['fields'] = $data_fields;

		return $data;
	}

	public function render_with_meta(array $fields = array())
	{
		return array(
			'meta' => $this->render_meta($fields),
			'object' => $this->render($fields)
		);
	}
	
	/**
	 * @var	string	Name of associated REST instance
	 */
	protected $_rest_name;
	
	/**
	 * @var Jelly_Model
	 */
	protected $_jelly_model;

}

