<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST Collection class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Collection_Core {

	/**
	 * @param	string	Name of associated REST instance
	 */
	public function __construct($rest_name)
	{
		$this->_rest_name = $rest_name;
	}
	
	/**
	 * Returns associated REST instance object.
	 * 
	 * @return	Jelly_REST_Instance
	 */
	public function rest()
	{
		return Jelly_REST::instance($this->_rest_name);
	}
	
	public function initialize(Jelly_Collection $collection, Jelly_Model $model)
	{
		$this->_jelly_collection = $collection;
		$this->_jelly_model = $model;
	}

	public function render(array $fields = array())
	{
		$data = array(
			'meta' => $this->rest()->model($this->_jelly_model)->render_meta($fields)
		);
		$data_objects = array();

		foreach ($this->_jelly_collection as $item)
		{
			$data_objects[] = $this->rest()->model($item)->render($fields);
		}
		$data['objects'] = $data_objects;

		return $data;
	}
	
	/**
	 * @var	string	Name of associated REST instance
	 */
	protected $_rest_name;
	
	/**
	 * @var Jelly_Collection
	 */
	protected $_jelly_collection;

	/**
	 * @var Jelly_Model
	 */
	protected $_jelly_model;

}

