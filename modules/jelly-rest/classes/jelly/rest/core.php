<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Jelly REST class
 *
 * @package     Jelly REST
 * @author      Zdenek Kalina
 */
abstract class Jelly_REST_Core {

	/**
	 * @var type	Class prefix used for classes called from REST instances
	 *				when specific class for instance is not found
	 */
	public static $default_class_prefix = 'rest_';
	
	/**
	 * @var	array	List of REST instances with its names as keys
	 */
	protected static $_instances = array();
	
	/**
	 * Singleton control for REST instances
	 * 
	 * @param	string	Specific name for REST instance
	 * @return	Jelly_REST_Instance
	 */
	public static function instance($name = NULL)
	{
		if ($name === NULL)
		{
			$name = 'REST';
		}
		
		if ( ! isset(Jelly_REST::$_instances[$name]))
		{
			Jelly_REST::$_instances[$name] = new Jelly_REST_Instance($name);
		}
		
		return Jelly_REST::$_instances[$name];
	}

}

