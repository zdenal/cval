<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * @package     Jelly REST
 * @category    Controller
 * @author      Zdenek Kalina
 */
abstract class Core_Controller_Jelly_REST_Model extends Controller_REST_Like
{

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;

	/**
	 * @var array   Actions requiring ACL checking
	 */
	protected $_acl_required = TRUE;

	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'model';

	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;

	/**
	 * Jelly model meta of loded model
	 * @var		Jelly_Meta
	 */
	protected $_model_meta;

	/**
	 * Jelly model name. Use this if model param is not passed in uri.
	 * @var		string
	 */
	protected $_model_name;

	/**
	 * @var array   Jelly resource
	 */
	protected $_jelly_resource = NULL;

	/**
	 * defines which action fields are taken
	 * can be overriden by $this->_list_fields
	 * @var type 
	 */
	protected $_action_fields = array(
		'list' => 'rest/list',
		'view' => 'rest/view'
	);

	/**
	 * fields which will be listed (user can define he wants to see more/less 
	 * fields than defined in admin or rest)
	 * @var type 
	 */
	protected $_list_fields = array();

	/**
	 * @var	string	Name of REST instance. This way own classes for REST collections,
	 * 				models and fields can be created.
	 */
	protected $_rest_instance_name = NULL;

	/**
	 * @var	string	Name of Search instance. This way own classes for Search collections,
	 * 				models and fields can be created.
	 */
	protected $_search_instance_name = NULL;

	/**
	 * Returns associated REST instance object.
	 * 
	 * @return	Jelly_REST_Instance
	 */
	public function rest()
	{
		return Jelly_REST::instance($this->_rest_instance_name);
	}

	/**
	 * Returns associated Search instance object.
	 * 
	 * @return	Jelly_Search_Instance
	 */
	public function search_instance()
	{
		return Jelly_Search::instance($this->_search_instance_name);
	}

	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{
		$this->_jelly_load_resource();
		$this->_acl_resource = $this->_jelly_resource;
	}

	/**
	 * Load a specified model
	 */
	protected function _jelly_load_resource()
	{
		if ($this->_jelly_resource !== NULL)
		{
			return;
		}

		if ($this->_model_name)
		{
			$model_name = $this->_model_name;
		}
		else
		{
			$model_name = $this->request->param('model');
		}

		if (!$model_name)
		{
			throw new Kohana_Exception('Model parameter must be specified.');
		}

		$this->_model_meta = Jelly::meta($model_name);

		$id = $this->request->param('id', 0);

		if ($id AND !in_array($this->request->action, array('create')))
		{
			$this->_jelly_resource = Jelly::select($this->_model_meta->model())->load($id);
			if (!$this->_jelly_resource->loaded())
			{
				throw new Kohana_Exception('Model `:model` with ID `:id` does not exist.', array(
					':model' => $this->_model_meta->model(),
					':id' => $id
						), 404);
			}
		}
		else
		{
			$this->_jelly_resource = $this->_model_meta->model();
		}
	}

	public function before()
	{
		parent::before();

		// Load jelly resource
		$this->_jelly_load_resource();
	}

	protected function _action_fields(Jelly_Meta $meta, $action = NULL, $default = TRUE)
	{
		// Get all the fields as objects if not set the action
		$fields = $meta->action_fields($action, NULL, $default);

		if (empty($fields))
		{
			return $fields;
		}

		foreach ($fields as $name => $field)
		{
			if (!$this->rest()->is_allowed($field->action_settings()))
			{
				unset($fields[$name]);
				continue;
			}
		}

		return $fields;
	}

	/**
	 * Redirect index action to list
	 */
	public function action_index()
	{
		if (!is_object($this->_jelly_resource))
		{
			$this->_action_list();
		}
		else
		{
			$this->_action_view();
		}
	}

	public function action_delete()
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Core_Controller_Jelly_REST_Model::action_delete');

		if ($this->_jelly_resource instanceof Jelly_Model)
		{
			// Backup resource for response
			$orig_resource = clone $this->_jelly_resource;
			// Delete resource
			$this->_jelly_resource->delete();

			// Get fields
			$fields = $this->_action_fields($this->_model_meta, Arr::get($this->_action_fields, 'view', 'rest/view'));

			// Render same fields as in view
			$this->request->response = $this->rest()->model($orig_resource)->render_with_meta($fields);

			return;
		}

		// Try to delete objects sended in POST
		$object_wrappers = Arr::get($_POST, 'objects');

		if (!empty($object_wrappers))
		{
			// Unset object from POST otherwise deadlock
			unset($_POST['objects']);

			foreach ($object_wrappers as $objects)
			{
				foreach ($objects as $object)
				{
					//throw new Kohana_Exception(implode(', ', $object));
					if (Arr::get($object, 'model') != $this->_model_name)
					{
						throw new Kohana_Exception("DELETE on different models than resource requested is forbidden");
					}

					$this->_call_delete_action_delete( Arr::get($object, 'id'), Arr::get($object, 'model') );
				}
			}

			// Paste back objects to POST for further use in overloaded classes
			$_POST['objects'] = $object_wrappers;
		}

		// Response is empty
		$this->request->response = array();
	}

	protected function _call_delete_action_delete($id, $model)
	{
		$delete_uri = $this->request->route->uri(array_merge($this->request->param(), array(
					'controller' => $this->request->controller,
					'action' => $this->request->action,
					'id' => $id
				)));

		// Call this method for only one object
		Request::factory($delete_uri)->execute();
	}

	protected function _fill_list_fields()
	{
		if (empty($this->_list_fields))
			$this->_list_fields = $this->_action_fields($this->_model_meta, Arr::get($this->_action_fields, 'list', 'rest/list'));
	}

	/**
	 * Display list of models
	 */
	public function _action_list()
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Core_Controller_Jelly_REST_Model::_action_list');

		$this->_fill_list_fields();

		$items = Jelly::select($this->_model_meta->model());

		$search = $this->_action_list_search($items);
		$sorting = $this->_action_list_sorting($items);
		$pagination = $this->_action_list_pagination($items);

		// Fetch items
//		Kohana::debug_exit($items->__toString());
		$items = $items->execute();
//		Kohana::debug_exit($items);
		$this->request->response = $this->rest()->collection($items, $this->_model_meta->model())->render($this->_list_fields);
	}

	protected function _action_list_search(Jelly_Builder $items)
	{
		$search_key = implode('_', array('jellyrest', 'model', 'list', $this->_model_meta->model(), 'search'));

		if (Arr::get($_REQUEST, 'clear_search'))
		{
			Session::instance()->set($search_key, array()); // Clear the search
		}

		$search = Session::instance()->get($search_key, array());

		$search_object = Jelly::factory($this->_model_meta->model());

		$search_fields = $search_object->meta()->action_fields('rest/search');

		if (isset($_POST['perform_search']))
		{
			if ($search_fields)
			{
				foreach ($search_fields as $name => $field)
				{
					$search_name = 'search-'.$name;

					$value = Arr::get($_POST, $search_name);

					if (strlen($value))
					{
						$search[$name] = $value;
					}
					elseif (isset($search[$name]))
					{
						unset($search[$name]);
					}
				}
			}

			Session::instance()->set($search_key, $search);
		}

		if ($search_fields)
		{
			foreach ($search_fields as $name => $field)
			{
				// Set value to object
				$value = Arr::get($search, $name, $field->default_search_value);

				$search_object->$name = $value;

				if (isset($search[$name]) AND $value === $field->default_search_value)
				{
					unset($search[$name]);
				}

				if (!isset($search[$name]))
					continue;

				$value = $field->search($search[$name]);

				if ($value === FALSE)
					continue;

				if (is_array($value))
				{
					foreach ($value as $tmp_name => $tmp_value)
					{
						$items->where($tmp_name, 'LIKE', '%'.$tmp_value.'%');
					}
				}
				else
				{
					$items->where($name, 'LIKE', '%'.$value.'%');
				}
			}
		}

		$search_active = count($search);
		// When search is not active delete it from session
		if (!$search_active)
		{
			Session::instance()->delete($search_key);
		}

		return array(
			'search_active' => $search_active,
			'search_object' => $search_object
		);
	}

	protected function _action_list_sorting(Jelly_Builder $items)
	{
		// Sorting
		$sorting_key = implode('_', array('jellyrest', 'model', 'list', $this->_model_meta->model(), 'sorting'));

		if (isset($_REQUEST['sorting']) AND $this->_model_meta->fields($_REQUEST['sorting']))
		{
			$sorting = Session::instance()->get($sorting_key, array());

			$sorting_field_name = $this->_model_meta->fields($_REQUEST['sorting'])->name;

			if (Arr::get($sorting, 'field') == $sorting_field_name)
			{
				$sorting['way'] = !((bool) $sorting['way']);
			}
			else
			{
				$sorting = array(
					'field' => $sorting_field_name,
					'way' => TRUE
				);
			}
			// Remove query string for sorting, otherwise is used in pagination
			if (isset($_GET['sorting']))
			{
				unset($_GET['sorting']);
			}

			Session::instance()->set($sorting_key, $sorting);
		}

		$sorting = Session::instance()->get($sorting_key, NULL);

		//echo Kohana::debug($sorting);

		if ($sorting)
		{
			$items->order_by(Arr::get($sorting, 'field'), Arr::get($sorting, 'way') ? 'ASC' : 'DESC');
		}

		return array();
	}

	protected function _action_list_pagination(Jelly_Builder $items)
	{

		$pagination_key = implode('_', array('jellyrest', 'model', 'list', $this->_model_meta->model(), 'pagination'));

		if (isset($_REQUEST['current_page']))
		{
			Session::instance()->set($pagination_key, (int) $_REQUEST['current_page']);
		}

		$_GET['current_page'] = Session::instance()->get($pagination_key, 1);

		$pagination = Pagination::factory(array(
					'current_page' => array(
						'source' => 'query_string',
						'key' => 'current_page',
					),
					'total_items' => $items->count(),
					'items_per_page' => 10,
					'first_page_in_url' => TRUE,
				));

		//echo Kohana::debug($pagination);

		$items->limit($pagination->items_per_page)
				->offset($pagination->offset);

		return $pagination;
	}

	/**
	 * Display details for a model
	 */
	public function _action_view()
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Core_Controller_Jelly_REST_Model::action_view');

		// Get fields
		$fields = $this->_action_fields($this->_model_meta, Arr::get($this->_action_fields, 'view', 'rest/view'));

		$this->request->response = $this->rest()->model($this->_jelly_resource)->render_with_meta($fields);
	}

}

// End of Core_Controller_Jelly_REST_Model

