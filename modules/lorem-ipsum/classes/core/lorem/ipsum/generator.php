<?php defined('SYSPATH') or die('No direct access allowed.');

abstract class Core_Lorem_Ipsum_Generator {
	/**
	*	Copyright (c) 2009, Mathew Tinsley (tinsley@tinsology.net)
	*	All rights reserved.
	*
	*	Redistribution and use in source and binary forms, with or without
	*	modification, are permitted provided that the following conditions are met:
	*		* Redistributions of source code must retain the above copyright
	*		  notice, this list of conditions and the following disclaimer.
	*		* Redistributions in binary form must reproduce the above copyright
	*		  notice, this list of conditions and the following disclaimer in the
	*		  documentation and/or other materials provided with the distribution.
	*		* Neither the name of the organization nor the
	*		  names of its contributors may be used to endorse or promote products
	*		  derived from this software without specific prior written permission.
	*
	*	THIS SOFTWARE IS PROVIDED BY MATHEW TINSLEY ''AS IS'' AND ANY
	*	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	*	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	*	DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
	*	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	*	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	*	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	*	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	*	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	*	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	*/

	/**
	 * @var array[Lorem_Ipsum_Generator]
	 */
	protected static $instances;

	protected static $_words_per_paragraph = 100;

	protected static $_words = array(
		'lorem',
		'ipsum',
		'dolor',
		'sit',
		'amet',
		'consectetur',
		'adipiscing',
		'elit',
		'curabitur',
		'vel',
		'hendrerit',
		'libero',
		'eleifend',
		'blandit',
		'nunc',
		'ornare',
		'odio',
		'ut',
		'orci',
		'gravida',
		'imperdiet',
		'nullam',
		'purus',
		'lacinia',
		'a',
		'pretium',
		'quis',
		'congue',
		'praesent',
		'sagittis',
		'laoreet',
		'auctor',
		'mauris',
		'non',
		'velit',
		'eros',
		'dictum',
		'proin',
		'accumsan',
		'sapien',
		'nec',
		'massa',
		'volutpat',
		'venenatis',
		'sed',
		'eu',
		'molestie',
		'lacus',
		'quisque',
		'porttitor',
		'ligula',
		'dui',
		'mollis',
		'tempus',
		'at',
		'magna',
		'vestibulum',
		'turpis',
		'ac',
		'diam',
		'tincidunt',
		'id',
		'condimentum',
		'enim',
		'sodales',
		'in',
		'hac',
		'habitasse',
		'platea',
		'dictumst',
		'aenean',
		'neque',
		'fusce',
		'augue',
		'leo',
		'eget',
		'semper',
		'mattis',
		'tortor',
		'scelerisque',
		'nulla',
		'interdum',
		'tellus',
		'malesuada',
		'rhoncus',
		'porta',
		'sem',
		'aliquet',
		'et',
		'nam',
		'suspendisse',
		'potenti',
		'vivamus',
		'luctus',
		'fringilla',
		'erat',
		'donec',
		'justo',
		'vehicula',
		'ultricies',
		'varius',
		'ante',
		'primis',
		'faucibus',
		'ultrices',
		'posuere',
		'cubilia',
		'curae',
		'etiam',
		'cursus',
		'aliquam',
		'quam',
		'dapibus',
		'nisl',
		'feugiat',
		'egestas',
		'class',
		'aptent',
		'taciti',
		'sociosqu',
		'ad',
		'litora',
		'torquent',
		'per',
		'conubia',
		'nostra',
		'inceptos',
		'himenaeos',
		'phasellus',
		'nibh',
		'pulvinar',
		'vitae',
		'urna',
		'iaculis',
		'lobortis',
		'nisi',
		'viverra',
		'arcu',
		'morbi',
		'pellentesque',
		'metus',
		'commodo',
		'ut',
		'facilisis',
		'felis',
		'tristique',
		'ullamcorper',
		'placerat',
		'aenean',
		'convallis',
		'sollicitudin',
		'integer',
		'rutrum',
		'duis',
		'est',
		'etiam',
		'bibendum',
		'donec',
		'pharetra',
		'vulputate',
		'maecenas',
		'mi',
		'fermentum',
		'consequat',
		'suscipit',
		'aliquam',
		'habitant',
		'senectus',
		'netus',
		'fames',
		'quisque',
		'euismod',
		'curabitur',
		'lectus',
		'elementum',
		'tempor',
		'risus',
		'cras'
	);

	/**
	 * Returns a new or already instanciated Lorem_Ipsum_Generator object by name argument.
	 *
	 *     $lorem = Lorem_Ipsum_Generator::instance($name, $words_per, $words);
	 *
	 * @param   string	instance name
	 * @param   int		words per paragraph
	 * @param   array	words to use
	 * @return  Lorem_Ipsum_Generator
	 */
	public static function instance($name, $words_per = NULL, array $words = NULL)
	{
		if ( ! Lorem_Ipsum_Generator::$instances[$name])
		{
			Lorem_Ipsum_Generator::$instances[$name] = Lorem_Ipsum_Generator::factory($words_per, $words);
		}
		return Lorem_Ipsum_Generator::$instances[$name];
	}

	/**
	 * Returns a new Lorem_Ipsum_Generator object.
	 *
	 *     $lorem = Lorem_Ipsum_Generator::factory($words_per, $words);
	 *
	 * @param   int		words per paragraph
	 * @param   array	words to use
	 * @return  Lorem_Ipsum_Generator
	 */
	public static function factory($words_per = NULL, array $words = NULL)
	{
		return new Lorem_Ipsum_Generator($words_per, $words);
	}

	protected $words, $words_per_paragraph, $words_per_sentence;
	
	public function __construct($words_per = NULL, array $words = NULL)
	{
		$this->words_per_paragraph = $words_per ? $words_per : Lorem_Ipsum_Generator::$_words_per_paragraph;
		$this->words_per_sentence = 24.460;
		$this->words = $words ? $words : Lorem_Ipsum_Generator::$_words;
	}
		
	public function get_content($count, $format = 'html', $loremipsum = true)
	{
		$format = strtolower($format);
		
		if($count <= 0)
			return '';

		switch($format)
		{
			case 'txt':
				return $this->get_text($count, $loremipsum);
			case 'plain':
				return $this->get_plain($count, $loremipsum);
			default:
				return $this->get_HTML($count, $loremipsum);
		}
	}
	
	protected function get_words(&$arr, $count, $loremipsum)
	{
		$i = 0;
		if($loremipsum)
		{
			$i = 2;
			$arr[0] = 'lorem';
			$arr[1] = 'ipsum';
		}
		
		for($i; $i < $count; $i++)
		{
			$index = array_rand($this->words);
			$word = $this->words[$index];
			//echo $index . '=>' . $word . '<br />';
			
			if($i > 0 && $arr[$i - 1] == $word)
				$i--;
			else
				$arr[$i] = $word;
		}
	}
	
	protected function get_plain($count, $loremipsum, $return_str = true)
	{
		$words = array();
		$this->get_words($words, $count, $loremipsum);
		//print_r($words);
		
		$delta = $count;
		$curr = 0;
		$sentences = array();
		while($delta > 0)
		{
			$sen_size = $this->gaussian_sentence();
			//echo $curr . '<br />';
			if(($delta - $sen_size) < 4)
				$sen_size = $delta;

			$delta -= $sen_size;
			
			$sentence = array();
			for($i = $curr; $i < ($curr + $sen_size); $i++)
				$sentence[] = $words[$i];

			$this->punctuate($sentence);
			$curr = $curr + $sen_size;
			$sentences[] = $sentence;
		}
		
		if($return_str)
		{
			$output = '';
			foreach($sentences as $s)
				foreach($s as $w)
					$output .= $w . ' ';
					
			return $output;
		}
		else
			return $sentences;
	}
	
	protected function get_text($count, $loremipsum)
	{
		$sentences = $this->get_plain($count, $loremipsum, false);
		$paragraphs = $this->get_paragraph_arr($sentences);
		
		$paragraph_str = array();
		foreach($paragraphs as $p)
		{
			$paragraph_str[] = $this->paragraph_to_string($p);
		}
		
		$paragraph_str[0] = "\t" . $paragraph_str[0];
		return implode("\n\n\t", $paragraph_str);
	}
	
	protected function get_paragraph_arr($sentences)
	{
		$words_per = $this->words_per_paragraph;
		$sentence_avg = $this->words_per_sentence;
		$total = count($sentences);
		
		$paragraphs = array();
		$p_count = 0;
		$curr_count = 0;
		$curr = array();
		
		for($i = 0; $i < $total; $i++)
		{
			$s = $sentences[$i];
			$curr_count += count($s);
			$curr[] = $s;
			if($curr_count >= ($words_per - round($sentence_avg / 2.00)) || $i == $total - 1)
			{
				$curr_count = 0;
				$paragraphs[] = $curr;
				$curr = array();
				//print_r($paragraphs);
			}
			//print_r($paragraphs);
		}
		
		return $paragraphs;
	}
	
	protected function get_HTML($count, $loremipsum)
	{
		$sentences = $this->get_plain($count, $loremipsum, false);
		$paragraphs = $this->get_paragraph_arr($sentences);
		//print_r($paragraphs);
		
		$paragraph_str = array();
		foreach($paragraphs as $p)
		{
			$paragraph_str[] = "<p>\n" . $this->paragraph_to_string($p, true) . '</p>';
		}
		
		//add new lines for the sake of clean code
		return implode("\n", $paragraph_str);
	}
	
	protected function paragraph_to_string($paragraph, $html_clean_code = false)
	{
		$paragraph_str = '';
		foreach($paragraph as $sentence)
		{
			foreach($sentence as $word)
				$paragraph_str .= $word . ' ';
				
			if($html_clean_code)
				$paragraph_str .= "\n";
		}		
		return $paragraph_str;
	}
	
	/*
	* Inserts commas and periods in the given
	* word array.
	*/
	protected function punctuate(& $sentence)
	{
		$count = count($sentence);
		$sentence[$count - 1] = $sentence[$count - 1] . '.';
		
		if($count < 4)
			return $sentence;
		
		$commas = $this->number_of_commas($count);
		
		for($i = 1; $i <= $commas; $i++)
		{
			$index = (int) round($i * $count / ($commas + 1));
			
			if($index < ($count - 1) && $index > 0)
			{
				$sentence[$index] = $sentence[$index] . ',';
			}
		}
	}
	
	/*
	* Determines the number of commas for a
	* sentence of the given length. Average and
	* standard deviation are determined superficially
	*/
	protected function number_of_commas($len)
	{
		$avg = (float) log($len, 6);
		$std_dev = (float) $avg / 6.000;
		
		return (int) round($this->gauss_ms($avg, $std_dev));
	}
	
	/*
	* Returns a number on a gaussian distribution
	* based on the average word length of an english
	* sentence.
	* Statistics Source:
	*	http://hearle.nahoo.net/Academic/Maths/Sentence.html
	*	Average: 24.46
	*	Standard Deviation: 5.08
	*/
	protected function gaussian_sentence()
	{
		$avg = (float) 24.460;
		$std_dev = (float) 5.080;
		
		return (int) round($this->gauss_ms($avg, $std_dev));
	}
	
	/*
	* The following three functions are used to
	* compute numbers with a guassian distrobution
	* Source:
	* 	http://us.php.net/manual/en/function.rand.php#53784
	*/
	protected function gauss()
	{   // N(0,1)
		// returns random number with normal distribution:
		//   mean=0
		//   std dev=1
		
		// auxilary vars
		$x=$this->random_0_1();
		$y=$this->random_0_1();
		
		// two independent variables with normal distribution N(0,1)
		$u=sqrt(-2*log($x))*cos(2*pi()*$y);
		$v=sqrt(-2*log($x))*sin(2*pi()*$y);
		
		// i will return only one, couse only one needed
		return $u;
	}

	protected function gauss_ms($m=0.0,$s=1.0)
	{
		return $this->gauss()*$s+$m;
	}

	protected function random_0_1()
	{
		return (float)rand()/(float)getrandmax();
	}

} // End generator