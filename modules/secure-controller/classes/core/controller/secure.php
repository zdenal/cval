<?php defined('SYSPATH') or die('No direct script access.');

abstract class Core_Controller_Secure extends Controller_Ext implements Acl_Resource_Interface
{
	/**
	 * @var	A2	A2 instance assigned in before method only when controller is secured
	 */
	public $a2;

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	/**
	 * @var array   Action-to-privilege ACL map
	 */
	protected $_acl_map = array('default' => NULL);

	/**
	 * @var array   Actions requiring ACL checking
	 */
	protected $_acl_required = array();

	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'Controller';

	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = array();
	
	/**
	 * Returns the string identifier of the Resource
	 *
	 * @return string
	 */
	public function get_resource_id()
	{
		return $this->_acl_resource;
	}
	
	/**
	 * The before() method is called before your controller action.
	 * In our template controller we override this method so that we can
	 * set up default values. These variables are then available to our
	 * controllers if they need to be modified.
	 */
	public function before()
	{
		parent::before();
        
        // ACL
		if ($this->_acl_is_secure 
			AND ($this->_acl_required === TRUE OR $this->_acl_required === 'all' OR in_array($this->request->action, $this->_acl_required)))
		{
			// Set A2 instance
			$this->a2 = A2::instance();
	
			// Perform resource loads and ACL check
			if ($this->_acl_resource_required === TRUE OR $this->_acl_resource_required === 'all' OR in_array($this->request->action, $this->_acl_resource_required))
			{
				$this->_acl_load_resource();
			}
			
			$privilege = isset($this->_acl_map[$this->request->action])
				? $this->_acl_map[$this->request->action]
				: $this->_acl_map['default'];
			$this->a2->allowed($this->_acl_resource, $privilege, TRUE);
		}
	}
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource() {}
	
}