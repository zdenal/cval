<?php

defined('SYSPATH') or die('No direct script access.');

class Jelly_Meta extends Jelly_Meta_Core
{

	/**
	 * @var bool Set multipart to TRUE if model has an file upload
	 */
	protected $multipart = FALSE;
	protected $label = '';
	protected $label_plural = '';
	protected $action_fields = array();

	/**
	 * @var array	Additional config params
	 */
	protected $_config = array();

	/**
	 * Constructor.
	 *
	 * Passed model to constructor is set before calling initialize
	 * static method.
	 *
	 * @param  array  $model
	 * */
	public function __construct($model)
	{
		// Set model
		$this->model = $model;
	}
	
	/**
	 * Gets or sets the builder attached to this object
	 * @param   string  $value
	 * @return  string|$this
	 */
	public function builder($value = NULL)
	{
		if (func_num_args() === 0)
		{
			return parent::builder();
		}
		
		if ($value === TRUE)
		{
			$value = $this->default_builder();
		}
		
		return parent::builder($value);
	}
	
	/**
	 * Generate default builder name
	 * @return  string
	 */
	public function default_builder()
	{
		return Jelly::model_prefix().'builder_'.$this->model;
	}

	public function multipart($value = NULL)
	{
		if ($value !== NULL)
		{
			$this->multipart = (bool) $value;

			return $this;
		}

		return $this->multipart;
	}

	public function label($value = FALSE)
	{
		if (!is_bool($value))
		{
			if (is_array($value))
			{
				list($this->label, $this->label_plural) = $value;
			}
			else
			{
				$this->label = $value;
				$this->label_plural = Inflector::plural($value);
			}

			return $this;
		}

		return $value ? $this->label_plural : $this->label;
	}

	/**
	 * This is called after initialization to
	 * finalize any changes to the meta object.
	 *
	 * @return  void
	 */
	public function finalize($model)
	{
		if ($this->initialized)
			return;

		parent::finalize($model);

		// Create label if not set
		if (empty($this->label))
		{
			$this->label = inflector::humanize($model);
			$this->label_plural = inflector::plural($this->label);
		}
	}

	public function action_fields($action = NULL, $fields = NULL, $default = NULL)
	{
		if ($fields !== NULL AND $fields !== TRUE)
		{
			$action_fields = array();

			foreach ($fields as $field => $settings)
			{
				if (is_numeric($field))
				{
					$field = $settings;
					$settings = array();
				}
				// Make settings as array
				$action_fields[$field] = is_array($settings) ? $settings : array($settings => TRUE);
			}
			$this->action_fields[$action] = $action_fields;

			return $this;
		}
		elseif (is_array($action))
		{
			foreach ($action as $tmp_action => $tmp_fields)
			{
				$this->action_fields($tmp_action, $tmp_fields);
			}

			return $this;
		}

		// If second parameter is TRUE return only config array
		if ($fields === TRUE)
		{
			// If fields not set
			if (!isset($this->action_fields[$action]))
			{
				// If default is not TRUE return default value
				if ($default !== TRUE)
				{
					return $default;
				}

				$fields = array();

				foreach ($this->fields() as $field)
				{
					$fields[$field->name] = array();
				}

				return $this->_sort_action_fields($fields);
			}

			return $this->_sort_action_fields($this->action_fields[$action]);
		}

		// Else return fields as objects
		$fields = array();

		// If fields not set
		if (!isset($this->action_fields[$action]))
		{
			// If default is not TRUE return default value
			if ($default !== TRUE)
			{
				return $default;
			}

			foreach ($this->fields() as $field)
			{
				$fields[$field->name] = $field->action_settings(array());
			}

			return $this->_sort_action_fields($fields);
		}

		foreach ($this->action_fields[$action] as $field => $settings)
		{
			$field_object = $this->fields($field);
			
			if ( ! is_object($field_object))
			{
				throw new Kohana_Exception('Field with name `:name` does not exists in model `:model`', array(
					':name' => $field,
					':model' => $this->model()
				));
			}
			
			$fields[$field] = $field_object->action_settings($settings);
		}

		return $this->_sort_action_fields($fields);
	}

	public function action_fields_remove($action = NULL, $fields = NULL)
	{
		if (is_array($action))
		{
			foreach ($action as $tmp_action => $tmp_fields)
			{
				$this->action_fields_remove($tmp_action, $tmp_fields);
			}

			return $this;
		}

		// Action fields not set yet, so add all meta fields
		if ($this->action_fields($action) === NULL)
		{
			$this->action_fields($action, array_keys($this->fields));
		}

		foreach ($fields as $field)
		{
			if (isset($this->action_fields[$action][$field]))
			{
				unset($this->action_fields[$action][$field]);
			}
		}
	}

	public function action_fields_modify($action = NULL, $fields = NULL)
	{
		if (is_array($action))
		{
			foreach ($action as $tmp_action => $tmp_fields)
			{
				$this->action_fields_modify($tmp_action, $tmp_fields);
			}

			return $this;
		}

		// Action fields not set yet, so add all meta fields
		if ($this->action_fields($action) === NULL)
		{
			$this->action_fields($action, array_keys($this->fields));
		}

		foreach ($fields as $field => $settings)
		{
			if (is_numeric($field))
			{
				$field = $settings;
				$settings = array();
			}

			$this->action_fields[$action][$field] = is_array($settings) ? $settings : array($settings);
		}
	}

	public function field_names()
	{
		$names = array();

		foreach ($this->fields() as $field)
		{
			$names[] = $field->name;
		}

		return $names;
	}

	/**
	 * Returns the fields for this object.
	 *
	 * If $field is specified, only the particular field is returned.
	 * If $name is TRUE, the name of the field specified is returned.
	 *
	 * You can pass an array for $field to set more fields. Calling
	 * this multiple times while setting will append fields, not
	 * overwrite fields.
	 *
	 * @param   $field  string
	 * @param   $name   boolean
	 * @return  array
	 */
	public function fields($field = NULL, $name = FALSE)
	{
		if (func_num_args() == 0)
		{
			return $this->fields;
		}

		if (is_array($field))
		{
			if (!$this->initialized)
			{
				// Allows fields to be appended
				$this->fields = array_merge($this->fields, $field);
				
				return $this;
			}

			throw new Kohana_Exception('Model `:model` is already initialized, you can not set fields now.', array(
				':model' => $this->model()
			));
		}

		if (!isset($this->field_cache[$field]))
		{
			$resolved_name = $field;

			if (isset($this->aliases[$field]))
			{
				$resolved_name = $this->aliases[$field];
			}

			if (isset($this->fields[$resolved_name]))
			{
				$this->field_cache[$field] = $this->fields[$resolved_name];
			}
			else
			{
				return NULL;
			}
		}

		if ($name)
		{
			return $this->field_cache[$field]->name;
		}
		else
		{
			return $this->field_cache[$field];
		}
	}

	public function dump()
	{
		$model = Jelly::factory($this->model());

		return array(
			'object' => 'Jelly_Meta',
			'model' => $this->label(),
			'fields' => $this->field_names(),
			'methods' => $model->methods()
		);
	}

	/**
	 * Set or get config params array or get specific key from array
	 *
	 * @param	mixed	config params array to set or key to get or NULL to get all config
	 * @param	mixed	default value if settings
	 * @return	mixed	Route object when setting config or config value
	 */
	public function config($config = NULL, $default = NULL, $overwrite = FALSE)
	{
		if ($config === NULL)
		{
			return $this->_config;
		}
		elseif (!is_array($config))
		{
			return Arr::path($this->_config, $config, $default);
		}

		if ($overwrite)
		{
			$this->_config = $config;
		}
		else
		{
			$this->_config = Arr::merge($this->_config, $config);
		}

		return $this;
	}

	/**
	 * Set config param
	 *
	 * @param	mixed	config key to set
	 * @param	mixed	config value
	 * @return	mixed	Route object
	 */
	public function config_set($key = NULL, $value = NULL)
	{
		$this->_config[$key] = $value;

		return $this;
	}

	/**
	 * Sorts action fields
	 * 
	 * @param	array	Array of Jelly_Field object to sort
	 * @return	array	Sorted fields by position param
	 */
	protected function _sort_action_fields(array $action_fields)
	{
		$positions = array();
		$empty_pos = 0;
		
		foreach ($action_fields as $name => $action_field)
		{
			$pos = Arr::get($action_field->action_settings, 'position', FALSE);
			
			if ($pos === FALSE)
			{
				$pos = $empty_pos++;
			}
			elseif ($pos == 'first')
			{
				$pos = (int) (PHP_INT_MAX * -1);
			}
			elseif ($pos == 'last')
			{
				$pos = (int) PHP_INT_MAX;
			}
			
			$positions[$name] = $pos;
		}
		
		asort($positions, SORT_NUMERIC);
		
		$action_fields_sorted = array();
		
		foreach ($positions as $name => $pos)
		{
			$action_fields_sorted[$name] = $action_fields[$name];
		}
		
		return $action_fields_sorted;
	}

}
