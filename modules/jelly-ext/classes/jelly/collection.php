<?php defined('SYSPATH') or die('No direct script access.');

class Jelly_Collection extends Jelly_Collection_Core
{

	public function dump()
	{
		return array(
			'object' => 'Jelly_Collection',
			'model' => $this->_model->dump(),
			'count' => $this->count()
		);
	}

}
