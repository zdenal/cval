<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Ext_Core extends Jelly_Core
{	
	/**
	 * Returns the prefix to use for all fields.
	 *
	 * @return  string
	 */
	public static function field_prefix()
	{
		return Jelly::$_field_prefix;
	}
	
	/**
	 * Automatically loads a model, if it exists,
	 * into the meta table.
	 *
	 * Models are not required to register
	 * themselves; it happens automatically.
	 *
	 * @param   string  $model
	 * @return  boolean
	 */
	public static function register($model)
	{
		if (0 === strpos($model, '_'))
		{
			return FALSE;
		}
		
		$class = Jelly::class_name($model);
		$model = Jelly::model_name($model);

		// Don't re-initialize!
		if (isset(Jelly::$_models[$model]))
		{
			return TRUE;
		}

		 // Can we find the class?
		if (class_exists($class))
		{
			// Prevent accidentally trying to load ORM or Sprig models
			if ( ! is_subclass_of($class, "Jelly_Model"))
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

		// Load it into the registry
		Jelly::$_models[$model] = $meta = new Jelly_Meta($model);

		// Call initialize part of this method
		Jelly::register_initialize($class, $model, $meta);
		
		// Call initialize action fields part of this method
		Jelly::register_initialize_action_fields($class, $model, $meta);

		// Finalize the changes
		$meta->finalize($model);

		return TRUE;
	}
	
	/**
	 * Initialize part of register() method.
	 *
	 * @param type $class
	 * @param type $model
	 * @param Jelly_Meta $meta 
	 */
	public static function register_initialize($class, $model, Jelly_Meta $meta)
	{
		// Let the intialize() method override defaults.
		call_user_func(array($class, 'before_initialize'), $meta);
		
		// Let the intialize() method override defaults.
		call_user_func(array($class, 'initialize'), $meta);
		
		// Let the intialize() method override defaults.
		call_user_func(array($class, 'after_initialize'), $meta);
	}
	
	/**
	 * Initialize action fields part of register() method.
	 *
	 * @param type $class
	 * @param type $model
	 * @param Jelly_Meta $meta 
	 */
	public static function register_initialize_action_fields($class, $model, Jelly_Meta $meta)
	{
		// Let the intialize() method override defaults.
		call_user_func(array($class, 'before_initialize_action_fields'), $meta);
		
		// Let the intialize() method override defaults.
		call_user_func(array($class, 'initialize_action_fields'), $meta);
		
		// Let the intialize() method override defaults.
		call_user_func(array($class, 'after_initialize_action_fields'), $meta);
	}
	
	/**
	 * Tries to convert column name to field for specific model.
	 * 
	 * @param	mixed	Jelly model that is accepted in Jelly::meta($model)
	 * @param	string	Column name
	 * @return	Jelly_Field	If not found returns NULL
	 */
	public static function column_to_field($model, $column)
	{
		$meta = Jelly::meta($model);
		$field_name = NULL;
		
		try
		{	
			// First resolve meta aliases if column is in alias format
			$column = Jelly::meta_alias($meta, $column);
		}
		catch (Kohana_Exception $e) {}
		
		// Than go through all fields and compare column with field column
		foreach ($meta->fields() as $field)
		{
			if ($field->column == $column)
			{
				$field_name = $field->name;
				break;
			}
		}
		
		// If we have field name get field object from meta, otherwise return NULL
		return $field_name ? $meta->fields($field_name) : NULL;
	}
}