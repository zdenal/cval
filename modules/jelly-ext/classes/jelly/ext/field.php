<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Ext_Field extends Jelly_Field_Core
{

	/**
	 * @var array	settings which are set by Jelly_Meta::action_fields methods
	 */
	public $action_settings = array();

	/**
	 * @var array	default field vars not affected with action settings
	 */
	protected $_original_vars = array();
	
	/**
	 * @var bool set if field is audited
	 */
	public $audited = FALSE;

	/**
	 * @var	bool	set if field acts as required, look at required() method
	 */
	public $required;

	/**
	 * @var	mixed	default value for field when searching
	 */
	public $default_search_value = NULL;

	/**
	 * @var mixed	valid php callback used before calling save() method
	 */
	public $before_save_callback = NULL;

	/**
	 * @var mixed	valid php callback used after calling save() method
	 */
	public $after_save_callback = NULL;

	/**
	 * @var mixed	valid php callback used before calling set() method
	 */
	public $before_set_callback = NULL;

	/**
	 * @var mixed	valid php callback used after calling set() method
	 */
	public $after_set_callback = NULL;

	/**
	 * @var mixed	valid php callback used before calling get() method
	 */
	public $before_get_callback = NULL;

	/**
	 * @var mixed	valid php callback used after calling get() method
	 */
	public $after_get_callback = NULL;
	
	/**
	 * @var string	valid php callback used after calling html_id() method
	 */
	public $html_id_callback = NULL;
	
	/**
	 * @var int		interger for next unique html id 
	 */
	protected static $_html_id_unique = 0;

	/**
	 * Set or get action settings array
	 *
	 * @param	array	settings which are set by Jelly_Meta::action_fields methods
	 * @return	Jelly_Field
	 */
	public function action_settings($settings = NULL, $default = NULL)
	{
		if ($settings === NULL)
		{
			return $this->action_settings;
		}
		elseif ( ! is_array($settings))
		{
			return Arr::get($this->action_settings, $settings, $default);
		}

		// Restore original vars
		if ($this->model !== NULL)
		{
			foreach ($this->_original_vars as $key => $value)
			{
				$this->$key = $value;
			}
			// Backup object vars
			$this->_original_vars = get_object_vars($this);
			// Except action settings
			unset($this->_original_vars['action_settings']);

			// Apply action settings to object
			foreach ($settings as $key => $value)
			{
				if (isset($this->$key))
				{
					$this->$key = $value;
				}
			}
		}

		// Set action settings
		$this->action_settings = $settings;
		
		return $this;
	}

	/**
	 * Validates single field with its rules
	 *
	 * @param	mixed	field value
	 * @throws	Validate_Exception
	 */
	public function validate($value = NULL)
	{
		// Create the validation object
		$data = Validate::factory(array($this->name => $value));

		$data->label($this->name, $this->label);
		$data->filters($this->name, $this->filters);
		$data->rules($this->name, $this->rules);
		$data->callbacks($this->name, $this->callbacks);

		if ( ! $data->check())
		{
			throw new Validate_Exception($data);
		}
	}

	/**
	 * Called just before save() method.
	 *
	 * @param   Jelly	$model
	 * @param   mixed	$value
	 * @param	bool	$loaded
	 * @return  mixed
	 */
	public function before_save($model, $value, $loaded)
	{
		if (is_callable($this->before_save_callback))
		{
			$args = func_get_args();
			$value = call_user_func_array($this->before_save_callback, $args);
		}

		return $value;
	}

	/**
	 * Called just after save() method.
	 *
	 * @param   Jelly	$model
	 * @param   mixed	$value
	 * @param	bool	$loaded
	 * @return  mixed
	 */
	public function after_save($model, $value, $loaded)
	{
		if (is_callable($this->after_save_callback))
		{
			$args = func_get_args();
			$value = call_user_func_array($this->after_save_callback, $args);
		}

		return $value;
	}

	/**
	 * Called just before set() method.
	 *
	 * @param   mixed	$value
	 * @return  mixed
	 */
	public function before_set($value)
	{
		if (is_callable($this->before_set_callback))
		{
			$args = func_get_args();
			$value = call_user_func_array($this->before_set_callback, $args);
		}

		return $value;
	}

	/**
	 * Called just after set() method.
	 *
	 * @param   mixed	$value
	 * @return  mixed
	 */
	public function after_set($value)
	{
		if (is_callable($this->after_set_callback))
		{
			$args = func_get_args();
			$value = call_user_func_array($this->after_set_callback, $args);
		}

		return $value;
	}

	/**
	 * Called just before get() method.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed        $value
	 * @return  mixed
	 */
	public function before_get($model, $value)
	{
		if (is_callable($this->before_get_callback))
		{
			$args = func_get_args();
			$value = call_user_func_array($this->before_get_callback, $args);
		}

		return $value;
	}

	/**
	 * Called just after get() method.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed        $value
	 * @return  mixed
	 */
	public function after_get($model, $value)
	{
		if (is_callable($this->after_get_callback))
		{
			$args = func_get_args();
			$value = call_user_func_array($this->after_get_callback, $args);
		}

		return $value;
	}

	/**
	 * Called just after deleting object.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed  $value
	 * @return  void
	 */
	public function delete(Jelly_Model $model, $value) {}
	
	/**
	 * Called just after save() method on model. Now ID is known.
	 *
	 * @param   Jelly_Model	$model
	 * @param   mixed	$value
	 * @param	bool	$loaded
	 * @return  mixed
	 */
	public function after_save_model(Jelly_Model $model, $value, $loaded) {}

	/**
	 * Sets a particular value for searching processed according
	 * to the class's standards.
	 * When FALSE is returned search is not performed.
	 *
	 * @param   mixed  $value
	 * @return  mixed
	 **/
	public function search($value)
	{
		return $value;
	}

	/**
	 * Test if field is required
	 *
	 * @return	bool
	 */
	public function required()
	{
		// First check if required param is not set implicit
		if (isset($this->required))
		{
			return $this->required;
		}

		// If field has not_empty OR Upload::not_empty rule say it is required
		if ((key_exists('not_empty', $this->rules) AND $this->rules['not_empty'] !== FALSE)
				OR (key_exists('Upload::not_empty', $this->rules) AND $this->rules['Upload::not_empty'] !== FALSE))
		{
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Displays the particular field as a form item
	 *
	 * @param string $prefix The prefix to put before the filename to be rendered
	 * @return View
	 **/
	public function input($prefix = 'jelly/field', $data = array())
	{
		// Add action settings to data
		$data = array_merge($this->action_settings, $data);

		return parent::input($prefix, $data);
	}

	/**
	 * Used internally to allow fields to inherit input views from parent classes
	 *
	 * @param   Jelly_Field  $class [optional]
	 * @return  string
	 */
	protected function _input_view($prefixes, $field_class = NULL)
	{
		// Make sure we have an array of prefixes
		if ( ! is_array($prefixes))
		{
			$prefixes = array($prefixes);
		}

		foreach ($prefixes as $prefix)
		{
			// Try to find a view
			$view = $this->_input_view_single($prefix);

			// We founded view
			if ($view !== NULL)
			{
				break;
			}
		}

		return $view;
	}

	/**
	 * Used internally to allow fields to inherit display views from parent classes
	 *
	 * @param	string	prefix used for searched views
	 * @param   string  $class [optional]
	 * @return  string
	 */
	protected function _input_view_single($prefix, $field_class = NULL)
	{
		if (is_null($field_class))
		{
			$field_class = get_class($this);
		}

		// Determine the view name, which matches the class name
		$file = strtolower($field_class);

		// Could be prefixed by Jelly_Field, or just Field_
		// MODIFICATION - search in subfolders, same like Kohana class loading
		$file = str_replace(array('jelly_field_', 'field_', '_'), array('', '', '/'), $file);

		// Allowing a prefix means inputs can be rendered from different paths
		$view_path = $prefix.'/'.$file;

		try
		{
			$view = View::factory($view_path);
		}
		catch (Kohana_View_Exception $e)
		{
			$view = NULL;
			$view_path = NULL;
		}

		// Check we can find a view for this field type, if not inherit view from parent
		if ($view === NULL
			// Don't try going beyond this base Jelly_Model class!
			AND get_parent_class($field_class) !== __CLASS__)
		{
			return $this->_input_view_single($prefix, get_parent_class($field_class));
		}

		// Either we've found a suitable view or there is no suitable one so just return what it should be
		return $view_path;
	}
	
	/**
	 * Converts a bunch of types to an array of ids
	 *
	 * @param   mixed  $models
	 * @return  array
	 */
	protected function _ids($models)
	{
		// Fix passing empty value to not add it as single item to array
		// Instead return empty array
		if (empty($models))
		{
			return array();
		}
		
		return parent::_ids($models);
	}

	/**
	 * Try to get value from action settings array.
	 *
	 * @param   string  $name
	 * @return  mixed
	 * @throw	Kohana_Exception
	 */
	public function __get($name)
	{
		if (array_key_exists($name, $this->action_settings))
		{
			return $this->action_settings[$name];
		}

		throw new Kohana_Exception('Access to undeclared property `:name` in `:class`', array(
			':name' => $name,
			':class' => get_class($this)
		));
	}

	/**
	 * Returns true if name is found in action settings.
	 *
	 * @param   string  $name
	 * @return  boolean
	 */
	public function __isset($name)
	{
		return (bool)(array_key_exists($name, $this->action_settings));
	}
	
	/**
	 * Returns very unique id for this field, that can be used in html.
	 * 
	 * @param	Jelly_Model	Model instance
	 */
	public function html_id($model, $unique = FALSE)
	{
		$html_id = ('field-'.$this->name.'-'.$model->meta()->model().'-'.($model->loaded()?$model->id():0));
		
		if ($unique)
		{
			$html_id .= ('-'.Jelly_Field::$_html_id_unique++.'-'.Text::random('alnum'));
		}
		
		if (is_callable($this->html_id_callback))
		{
			$html_id = call_user_func_array($this->html_id_callback, array($html_id, $model, $unique));
		}
		
		return $html_id;
	}

}
