<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Ext_Field_Boolean extends Jelly_Field_Boolean
{

	public $default_search_value = 'null';

	/**
	 * Validates a boolean out of the value with filter_var
	 *
	 * @param   mixed  $value
	 * @return  void
	 */
	public function set($value)
	{
		if ($value == $this->default_search_value)
		{
			return $value;
		}
		return parent::set($value);
	}

	/**
	 * Boolean search fix
	 *
	 * @param   mixed  $value
	 * @return  void
	 */
	public function search($value)
	{
		if ($value == $this->default_search_value)
		{
			return FALSE;
		}

		return $value;
	}

}