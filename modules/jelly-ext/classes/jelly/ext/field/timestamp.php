<?php defined('SYSPATH') or die('No direct script access.');

class Jelly_Ext_Field_Timestamp extends Jelly_Field_Timestamp
{

	/**
	 * @var  string  A pretty format used for representing the date to users
	 */
	public $pretty_format = 'd.m.Y H:i:s';
	
	/**
	 * @var	bool	Contains this field time 
	 */
	public $date = TRUE;
	
	/**
	 * @var	bool	Contains this field time 
	 */
	public $time = TRUE;

	/**
	 * Converts the time to a UNIX timestamp
	 *
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function set($value)
	{
		if ($value === NULL OR ($this->null AND empty($value)))
		{
			return NULL;
		}

		// Already a timestamp?
		if (is_numeric($value))
		{
			return (int) $value;
		}
		elseif ($this->_strtotime_test($value))
		{
			return $this->_strtotime($value);
		}

		return $value;
	}
	
	/**
	 * Automatically creates or updates the time and
	 * converts it, if necessary
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function save($model, $value, $loaded)
	{
		if (( ! $loaded AND $this->auto_now_create) OR ($loaded AND $this->auto_now_update))
		{
			$value = time();
		}

		return $this->format_for_db($value);
	}
	
	public function format_for_db($value)
	{
		// Convert if necessary
		if ($this->format)
		{
			// Does it need converting?
			if ( ! is_numeric($value) AND $this->_strtotime_test($value))
			{
				$value = strtotime($value);
			}

			if (is_numeric($value))
			{
				$value = date($this->format, $value);
			}
		}
		
		return $value;
	}
	
	/**
	 * Formats value according to pretty_format settings
	 * 
	 * @param	float	
	 * @return	float
	 */
	public function display_value($value)
	{
		if ( ! isset($this->default) AND empty($value))
		{
			return '';
		}
		
		return date($this->pretty_format, $value);
	}
	
	/**
	 * Check if timestamp is string and should be converted to time
	 * 
	 * @param	string	$value
	 * @return	int 
	 */
	protected function _strtotime_test($value)
	{
		return FALSE !== strtotime($value);
	}
	
	/**
	 * Convert string timestamp to time
	 * 
	 * @param	string	$value
	 * @return	int 
	 */
	protected function _strtotime($value)
	{
		return strtotime($value);
	}

}