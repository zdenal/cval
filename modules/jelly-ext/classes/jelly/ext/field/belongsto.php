<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Ext_Field_BelongsTo extends Jelly_Field_BelongsTo
{

	/**
	 * @var	string	Defines how this field is displayed, possible values are:
	 *					datatable
	 *					enum
	 */
	public $display_type = 'default';
	
	public function __construct($options = array())
	{
		if (empty($options['null']))
		{
			$options['null'] = TRUE;
		}
		parent::__construct($options);
	}

	/**
	 * Displays a selection of models to relate to
	 *
	 * @param   string  $prefix  The prefix to put before the filename to be rendered
	 * @return  View
	 **/
	public function input($prefix = 'jelly/field', $data = array())
	{
		$options_set = isset($data['options']);

		$view = parent::input($prefix, $data);

		if ( ! $options_set AND ! $this->required())
		{
			$options = $view->get('options');
			Arr::unshift($options, '', '&nbsp;');
			$view->set('options', $options);
		}

		return $view;
	}
	
	/**
	 * Tries to get related field instance from foreign model. If related field
	 * is not defined in foreign model it returns NULL.
	 * 
	 * @return	Jelly_Field
	 */
	public function foreign_field() 
	{
		return Jelly::column_to_field($this->foreign['model'], $this->foreign['column']);
	}

}