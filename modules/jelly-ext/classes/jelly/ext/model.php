<?php

defined('SYSPATH') or die('No direct script access.');

class Jelly_Ext_Model extends Jelly_Model_Core implements Acl_Resource_Interface
{
	
	/**
	 * @var	bool	Tells if object has finished its __construct method
	 * @see	this::constructed()
	 */
	protected $_constructed = FALSE;
	
	/**
	 * Constructor.
	 *
	 * If $values is passed and it is an array, it will be
	 * applied to the model as if it were a database result.
	 * The model is then considered to be loaded.
	 *
	 * It is important to note that, although Jelly Models are
	 * not instantiated from Database_Results (by using
	 * as_object()), they can be instantiated this way.
	 *
	 * @param  array  $values
	 **/
	public function __construct($values = array())
	{
		parent::__construct($values);
		
		// Call after construct method
		$this->_after_construct();
		
		// Set this object to be constructed
		$this->_constructed = TRUE;
	}
	
	/**
	 * Check if object has finished its construct method.
	 * In construct there are filled values from DB, so we can
	 * check in fields, if set method is called by system or by user input.
	 * 
	 * @return bool 
	 */
	public function constructed()
	{
		return $this->_constructed;
	}
	
	/**
	 * Called immediatelly after construct call to do some init thing.
	 * BUT carrefull values are not yet loaded.
	 */
	protected function _after_construct()
	{
		
	}

	/**
	 * If we set system update to TRUE, it means that object is modified
	 * by system and not by user. For ex. update logins count when user logs in.
	 * We can set this on save by passing it in second parameter.
	 *
	 * @var		bool
	 */
	protected $_system_update = FALSE;
	/**
	 * @var		array	Array of changed fields from last save.
	 */
	protected $_last_changed = array();

	/**
	 * audits the changed fields
	 * @param array $values - key is the name of the field, value is new value, 
	 * 		  contains only modified fields
	 */
	protected function _audit(array $values = array(), $loaded = NULL)
	{
		if ( ! $this->meta()->config('audited', FALSE))
		{
			return;
		}
		foreach (array_keys($values) as $field_name)
		{
			$field = $this->meta()->fields($field_name);

			if ( ! $field->audited)
				continue;
			$model_audit = new Model_Audit;
			$model_audit->db_table = $this->meta()->table();

			$model_audit->db_column = $field->column; //column name
			$model_audit->old_value = $this->_original[$field->name]; //old value
			$model_audit->new_value = $values[$field->name]; //new value			
			$model_audit->db_id = $this->id();
			$model_audit->save();
		}
	}

	/**
	 * called when audited object is deleted, 
	 * deletes all audit fields related to this object
	 */
	protected function _audit_delete()
	{
		if ( ! $this->meta()->config('audited', FALSE))
		{
			return;
		}
		Jelly::delete('audit')
				->where('db_table', '=', $this->meta()->table())
				->where('db_id', '=', $this->id())
				->execute();
	}

	/**
	 * Returns field values as members of the object.
	 *
	 * A few things to note:
	 *
	 * * Values that are returned are cached (unlike get()) until they are changed
	 * * Relations are automatically execute()ed
	 *
	 * @see     get()
	 * @param   string  $name
	 * @return  mixed
	 */
	public function __get($name)
	{
		// Alias the field to its actual name. We must do this now
		// so that any aliases will be cached under the real fields
		// name, rather than under its alias name
		$name = $this->_meta->fields($name, TRUE);

		if ( ! array_key_exists($name, $this->_retrieved))
		{
			$this->_retrieved[$name] = $this->_retrieve_field($name);
		}

		return $this->_retrieved[$name];
	}
	
	/**
	 * Retrieves field if not retrieved yet
	 *
	 * @see     __get()
	 * @param   string  $name
	 * @return  mixed
	 */
	protected function _retrieve_field($name)
	{
		$value = $this->get($name);

		// Auto-load relations
		if ($value instanceof Jelly_Builder)
		{
			$value = $value->execute();
		}

		$translate_fields = $this->meta()->config('translate_fields');

		if ( ! empty($translate_fields) AND in_array($name, $translate_fields))
		{
			$value = I18n::get($value, $this->meta()->config('translate_lang'));
		}

		return $value;
	}


	/**
	 * Creates or updates the current record.
	 *
	 * If $key is passed, the record will be assumed to exist
	 * and an update will be executed, even if the model isn't loaded().
	 *
	 * @param   mixed	$key
	 * @param	bool	$system_update - set variable Jelly_Model::$_system_update
	 * @return  $this
	 * */
	public function save($key = NULL, $system_update = FALSE)
	{
		$this->_system_update = $system_update;

		try
		{
			Database::instance()->transaction_start();

			$loaded = $this->_loaded;

			$this->_before_save($loaded);

			// Call modified save() method from Jelly_Model
			$this->_do_save($key, $loaded);

			$this->_after_save($loaded);

			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();

			throw $e;
		}

		return $this;
	}

	/**
	 * Creates or updates the current record.
	 *
	 * If $key is passed, the record will be assumed to exist
	 * and an update will be executed, even if the model isn't loaded().
	 *
	 * @param   mixed  $key
	 * @return  $this
	 * */
	protected function _do_save($key = NULL)
	{
		// Run validation
		$data = $this->validate_save($key);

		// Run after validate call
		$data = $this->_after_validate_save_call($this->_loaded, $data);

		// Set the key to our id if it isn't set
		if ($this->_loaded)
		{
			$key = $this->_original[$this->_meta->primary_key()];
		}

		// These will be processed later
		$values = $relations = array();

		// Iterate through all fields in original incase any unchanged fields
		// have save() behavior like timestamp updating...
		foreach ($this->_changed + $this->_original as $column => $value)
		{
			// Filters may have been applied to data, so we should use that value
			if (array_key_exists($column, $data))
			{
				$value = $data[$column];
			}

			$field = $this->_meta->fields($column);
			
			// Only save in_db values
			if ($field->in_db)
			{
				// Call before save method
				$value = $field->before_save($this, $value, (bool) $key);

				// See if field wants to alter the value on save()
				$value = $field->save($this, $value, (bool) $key);

				// Call after save method
				$value = $field->after_save($this, $value, (bool) $key);

				if ($value !== $this->_original[$column])
				{
					// Value has changed (or has been changed by field:save())
					$values[$field->name] = $value;
				}
				else
				{
					// Insert defaults
					if ( ! $key AND ! $this->changed($field->name) AND ! $field->primary)
					{
						$values[$field->name] = $field->default;
					}
				}
			}
			elseif ($this->changed($column) AND $field instanceof Jelly_Field_Behavior_Saveable)
			{
				$relations[$column] = $value;
			}
		}

		// If we have a key, we're updating
		if ($key)
		{
			// Do we even have to update anything in the row?
			if ($values)
			{
				Jelly::update($this)
						->where(':unique_key', '=', $key)
						->set($values)
						->execute();
			}
		}
		else
		{	
			list($id) = Jelly::insert($this)
					->columns(array_keys($values))
					->values(array_values($values))
					->execute();

			// Gotta make sure to set this
			$this->_changed[$this->_meta->primary_key()] = $id;
		}

		$this->_audit($values);
		
		// Reset the saved data, since save() may have modified it
		$this->set($values);

		// Make it appear as original data instead of changed
		$this->_original = array_merge($this->_original, $this->_changed);
		
		// Iterate through all fields and call after save method
		foreach ($this->_original as $column => $value)
		{
			$field = $this->_meta->fields($column);
			
			if (empty($field))
			{
				continue;
			}
			// Call after save method
			$field->after_save_model($this, $value, (bool) $key);
		}

		// Set last changed fields
		$this->_last_changed = $this->_changed;

		// We're good!
		$this->_loaded = $this->_saved = TRUE;
		$this->_retrieved = $this->_changed = array();

		// Save the relations
		foreach ($relations as $column => $value)
		{
			$this->_meta->fields($column)->before_save($this, $value, (bool) $key);

			$this->_meta->fields($column)->save($this, $value, (bool) $key);

			$this->_meta->fields($column)->after_save($this, $value, (bool) $key);
		}

		return $this;
	}
	
	/**
	 * Allows to modify fields in _after_validate_save method with classic
	 * style by setting $this->$field = $value.
	 *
	 * @param	bool	Is object loaded
	 * @param	array	Data returned from validate_save method
	 * @return	array	Modified validated data if some field has changed
	 */
	protected function _after_validate_save_call($loaded, array $validate_data)
	{
		$original_changed = $this->_changed;
		
		$this->_after_validate_save($loaded);
		
		// Check if some fields has changed after validation.
		foreach ($this->_changed as $column => $value)
		{
			if ( ! isset($original_changed[$column])
					OR $original_changed[$column] !== $value)
			{
				// If so overwrite its value in validate data array
				$validate_data[$column] = $value;
			}
		}
		
		// Return updated validate data
		return $validate_data;
	}

	protected function _before_save()
	{
		
	}

	protected function _after_validate_save()
	{
		
	}

	protected function _after_save()
	{
		
	}

	/**
	 * Called before object saving from save() method.
	 *
	 * @param   array	$data
	 * @param	bool	$return_array	return as array or as Validate object
	 * @throws  Validate_Exception
	 * @return  array
	 */
	public function validate_save($key = NULL)
	{
		// Determine whether or not we're updating
		$data = ($this->_loaded OR $key) ? $this->_changed : $this->_changed + $this->_original;

		if ( ! is_null($key))
		{
			// There are no rules for this since it is a meta alias and not an actual field
			// but adding it allows us to check for uniqueness when lazy saving
			$data[':unique_key'] = $key;
		}

		// Run validation
		$data = $this->validate($data);

		return $data;
	}

	/**
	 * Returns whether or not this object has changed.
	 *
	 * @return  boolean
	 */
	public function is_changed()
	{
		return (count($this->changed()) > 0);
	}

	/**
	 * Returns whether or not the particular $field has changed.
	 *
	 * If $field is NULL, all changed fields and their values are returned.
	 * If $field is array, return value tells if all fields in array are changed.
	 * If $field is array and $all is FALSE, return value tells if atleast one
	 * of the fields in array is changed.
	 *
	 * @param   string  $field
	 * @return  boolean|array
	 */
	public function changed($field = NULL, $all = TRUE)
	{
		if (is_array($field))
		{
			foreach ($field as $tmp_field)
			{
				$changed = $this->changed($tmp_field);

				if ($all)
				{
					if ( ! $changed)
						return FALSE;
				}
				else
				{
					if ($changed)
						return TRUE;
				}
			}

			return $all ? TRUE : FALSE;
		}

		return parent::changed($field);
	}

	/**
	 * Returns whether or not the particular $field has changed on last save.
	 *
	 * If $field is NULL, all changed fields and their values are returned.
	 * If $field is array, return value tells if all fields in array are changed.
	 * If $field is array and $all is FALSE, return value tells if atleast one
	 * of the fields in array is changed.
	 *
	 * @param   string  $field
	 * @return  boolean|array
	 */
	public function last_changed($field = NULL, $all = TRUE)
	{
		if (is_array($field))
		{
			foreach ($field as $tmp_field)
			{
				$changed = $this->last_changed($tmp_field);

				if ($all)
				{
					if ( ! $changed)
						return FALSE;
				}
				else
				{
					if ($changed)
						return TRUE;
				}
			}

			return $all ? TRUE : FALSE;
		}

		if ($field)
		{
			return array_key_exists($this->_meta->fields($field, TRUE), $this->_last_changed);
		}

		return $this->_last_changed;
	}

	/**
	 * Deletes a single record.
	 *
	 * @param   $key  A key to use for non-loaded records
	 * @return  boolean
	 * */
	public function delete($key = NULL)
	{
		$result = FALSE;

		try
		{
			Database::instance()->transaction_start();

			$this->_before_delete();

			// Are we loaded? Then we're just deleting this record
			if ($this->_loaded OR $key)
			{
				if ($this->_loaded)
				{
					$key = $this->id();
				}

				$this->_audit_delete();

				$result = Jelly::delete($this)
						->where(':unique_key', '=', $key)
						->execute();
			}

			$this->_after_delete_call_fields();

			$this->_after_delete();

			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();

			throw $e;
		}

		// Clear the object so it appears deleted anyway
		$this->clear();

		return (boolean) $result;
	}

	protected function _after_delete_call_fields()
	{
		// Iterate through all fields and call delete() used ie. for file
		// delete...
		foreach ($this->_meta->fields() as $field)
		{
			$value = $this->get($field->name);

			$field->delete($this, $value);
		}
	}

	protected function _before_delete()
	{
		
	}

	protected function _after_delete()
	{
		
	}

	/**
	 * Returns whether or not this object has by user changed.
	 *
	 * @return  boolean
	 * @see		Jelly_Model::$_system_update
	 */
	public function is_user_changed()
	{
		return ($this->is_changed() AND ! $this->_system_update);
	}
	
	/**
	 * Setter and getter for system update.
	 *
	 * @return  boolean
	 * @see		Jelly_Model::$_system_update
	 */
	public function system_update($val = NULL)
	{
		if ($val !== NULL)
		{
			$this->_system_update = (bool) $val;
		}
		return $this->_system_update;
	}

	/**
	 * Pass the model object to field set method.
	 * Sets values in the fields. Everything passed to this
	 * is converted to an internally represented value.
	 *
	 * @param   string  $name
	 * @param   string  $value
	 * @return  Jelly   Returns $this
	 */
	public function set($values, $value = NULL)
	{
		// Accept set('name', 'value');
		if ( ! is_array($values))
		{
			$values = array($values => $value);
		}

		foreach ($values as $key => $value)
		{
			$field = $this->_meta->fields($key);

			// If this isn't a field, we just throw it in unmapped
			if ( ! $field)
			{
				$this->_unmapped[$key] = $value;
				continue;
			}

			// Call before set method
			$value = $field->before_set($value, $this);

			$value = $field->set($value, $this);

			// Call after set method
			$value = $field->after_set($value, $this);

			$current_value = array_key_exists($field->name, $this->_changed) ? $this->_changed[$field->name] : $this->_original[$field->name];


			// Ensure data is really changed
			if ($value === $current_value)
			{
				continue;
			}

			// Data has changed
			$this->_changed[$field->name] = $value;

			// Invalidate the cache
			if (array_key_exists($field->name, $this->_retrieved))
			{
				unset($this->_retrieved[$field->name]);
			}

			// Model is no longer saved
			$this->_saved = FALSE;
		}

		return $this;
	}

	/**
	 * Gets the internally represented value from a field or unmapped column.
	 *
	 * Relationships that are returned are raw Jelly_Builders, and must be
	 * execute()d before they can be used. This allows you to chain
	 * extra statements on to them.
	 *
	 * Set $changed to FALSE to get original values from the database.
	 *
	 * @param   string  $name  The field's name
	 * @return  mixed
	 */
	public function get($name, $changed = TRUE)
	{
		if ($field = $this->_meta->fields($name))
		{
			// Alias the name to its actual name
			$name = $field->name;

			if ($changed AND array_key_exists($name, $this->_changed))
			{
				// Call before get method
				$value = $field->before_get($this, $this->_changed[$name]);

				$value = $field->get($this, $value);

				// Call after get method
				$value = $field->after_get($this, $value);
			}
			elseif ($changed AND array_key_exists($name, $this->_with))
			{
				$value = Jelly::factory($field->foreign['model'])->load_values($this->_with[$name]);

				// Try and verify that it's actually loaded
				if ( ! $value->id())
				{
					$value->_loaded = FALSE;
					$value->_saved = FALSE;
				}
			}
			else
			{
				// Call before get method
				$value = $field->before_get($this, $this->_original[$name]);

				$value = $field->get($this, $value);

				// Call after get method
				$value = $field->after_get($this, $value);
			}

			return $value;
		}
		// Return unmapped data from custom queries
		elseif (isset($this->_unmapped[$name]))
		{
			return $this->_unmapped[$name];
		}
	}

	/**
	 * Validates the current state of the model.
	 *
	 * Only changed data is validated, unless $data is passed.
	 *
	 * @param   array	$data
	 * @param	bool	$return_array	return as array or as Validate object
	 * @throws  Validate_Exception
	 * @return  array
	 */
	public function validate($data = NULL)
	{
		if ($data === NULL)
		{
			$data = $this->_changed;
		}

		if (empty($data))
		{
			return $data;
		}

		// Create the validation object
		$data = Validate::factory($data);

		// If we are passing a unique key value through, add a filter to ensure it isn't removed
		if ($data->offsetExists(':unique_key'))
		{
			$data->filter(':unique_key', 'trim');
		}

		// Loop through all columns, adding rules where data exists
		foreach ($this->_meta->fields() as $column => $field)
		{
			// Do not add any rules for this field
			if ( ! $data->offsetExists($column))
			{
				continue;
			}

			$data->label($column, $field->label);

			// Add model object to filter as third parameter
			foreach ($field->filters as $filter)
			{
				$data->filter($column, $filter, array($this));
			}

			$data->rules($column, $field->rules);

			// Add model object to callback as third parameter
			foreach ($field->callbacks as $callback)
			{
				$data->callback($column, $callback, array($this));
			}
		}

		if ( ! $data->check())
		{
			throw new Validate_Exception($data);
		}

		return $data->as_array();
	}

	public function __toString()
	{
		return $this->name();
	}

	/**
	 * Returns the string identifier of the Resource
	 *
	 * @return string
	 */
	public function get_resource_id()
	{
		return $this->meta()->model();
	}

	/**
	 * Output model to given view as variable named model.
	 * If no view given use default view in jelly/render.
	 *
	 * @return	View
	 */
	public function render($view = NULL)
	{
		if ($view === NULL)
		{
			$view = 'jelly/render';
		}

		return View::factory($view, array('model' => $this));
	}

	public function dump()
	{
		return $this->meta()->dump();
	}

	public function methods()
	{
		return NULL;
	}

	/**
	 * Same as Jelly_Model::as_array() but returns internally represented values.
	 * Returns an array of values in the fields.
	 *
	 * You can pass a variable number of field names
	 * to only retrieve those fields in the array:
	 *
	 *     $model->as_array('id', 'name', 'status');
	 *
	 * @param  string  $fields
	 * @param  ...
	 * @return array
	 * @uses   Jelly_Model::as_array_internal_recursive()
	 */
	public function as_array_internal($fields = NULL)
	{
		$fields = func_get_args();

		array_unshift($fields, 0);

		return call_user_func_array(array($this, 'as_array_internal_recursive'), $fields);
	}

	/**
	 * Same as Jelly_Model::as_array() but returns internally represented values.
	 * Returns an array of values in the fields.
	 *
	 * You can pass a variable number of field names
	 * to only retrieve those fields in the array:
	 *
	 *     $model->as_array('id', 'name', 'status');
	 *
	 * @param  string  $fields
	 * @param  ...
	 * @return array
	 */
	public function as_array_internal_recursive($depth = 1, $fields = NULL)
	{
		$fields = func_get_args();

		if (count($fields))
		{
			array_shift($fields);
		}

		if (count($fields) AND is_array($fields[0]))
		{
			$fields = $fields[0];
		}

		$result = call_user_func_array(array($this, 'parent::as_array'), $fields);

		foreach ($result as $field => $value)
		{
			if ( ! is_object($value))
			{
				// Do nothing
			}
			elseif ($value instanceof Jelly_Model)
			{
				if ($depth > 0)
				{
					$value = $value->as_array_internal_recursive($depth - 1);
				}
				else
				{
					$value = $value->id();
				}
			}
			elseif ($value instanceof Jelly_Collection)
			{
				if ($depth > 0)
				{
					$collection = $value;

					$value = array();
					foreach ($collection as $model)
					{
						$value[] = $model->as_array_internal_recursive($depth - 1);
					}
				}
				else
				{
					$value = array_keys($value->as_array(':primary_key', ':primary_key'));
				}
			}
			$result[$field] = $value;
		}

		return $result;
	}

	/**
	 * Clears the object and loads an array of values into the object.
	 *
	 * This should only be used for setting from database results
	 * since the model declares itself as saved and loaded after.
	 *
	 * @param   array    $values
	 * @param   boolean  $alias
	 * @return  $this
	 */
	public function load_values(array $values, $alias = FALSE)
	{
		// Load object
		parent::load_values($values, $alias);

		// Run after load method
		$this->_after_load();

		return $this;
	}

	protected function _after_load()
	{
		
	}

	/**
	 * Returns a view object that represents the field.
	 *
	 * If $prefix is an array, it will be used for the data
	 * and $prefix will be set to the default.
	 *
	 * @param   string        $name
	 * @param   string|array  $prefix
	 * @param   array         $data
	 * @return  View
	 */
	public function input($name, $prefix = NULL, $data = array())
	{
		$field = $this->_meta->fields($name);

		// More data munging. But it makes the API so much more intuitive
		// Arr must be also associative, otherwise data has no sense
		if (is_array($prefix) AND Arr::is_assoc($prefix))
		{
			$data = $prefix;
			$prefix = NULL;
		}

		// Convert empty prefixes to empty array
		if (empty($prefix))
		{
			$prefix = array();
		}
		// If prefix is not array, create it
		if ( ! is_array($prefix))
		{
			$prefix = array($prefix);
		}
		// Add default model prefix
		array_push($prefix, $this->_meta->input_prefix());

		// Ensure there is a default value. Some fields override this
		$data['value'] = $this->__get($name);
		$data['model'] = $this;

		return $field->input($prefix, $data);
	}
	
	/**
	 * Returns primary related object
	 * 
	 * @param   string  $name of field
	 * @param	bool	$execute if builder will be executed before returning
	 * @return  Jelly_Model or Jelly_Builder
	 */
	public function get_primary_relation($name, $execute = TRUE)
	{
		$field = $this->_meta->fields($name);
		
		// Don't continue without knowing we have something to work with
		if ( ! $field instanceof Jelly_Field_Behavior_Haveable)
		{
			throw new Kohana_Exception('Model method `get_primary_relation` can be called only on haveable fields');
		}
		
		$builder = $this
			->get($name);
		
		$is_primary_field = Jelly::meta($field->foreign['model'])->fields('is_primary');
		if ($is_primary_field)
		{
			$builder->order_by($is_primary_field->name, 'DESC');
		}
		
		return $execute ? $builder->load() : $builder;
	}
	
	/**
	 * Reloads this object from database. It is not real reload, because
	 * reloaded instance is returned from method and this object stays same.
	 * 
	 * @return	$this
	 * @throws	Kohana_Exception	When object is not loaded
	 */
	public function reload()
	{
		if ( ! $this->loaded())
		{
			throw new Kohana_Exception('Only saved models can be reloaded!');
		}
		
		return Jelly::select($this->meta()->model())->load($this->id());
	}
	
	/**
	 * This method initializes action fields.
	 * Is called after initialize() method.
	 *
	 * @param Jelly_Meta $meta
	 */
	public static function initialize_action_fields(Jelly_Meta $meta)
	{
		// Place for action fields calls
	}
	
	/**
	 * Called before initialize() method.
	 * 
	 * @param Jelly_Meta $meta 
	 */
	public static function before_initialize(Jelly_Meta $meta) {}
	
	/**
	 * Called after initialize() method.
	 * 
	 * @param Jelly_Meta $meta 
	 */
	public static function after_initialize(Jelly_Meta $meta) {}
	
	/**
	 * Called before initialize_action_fields() method.
	 * 
	 * @param Jelly_Meta $meta 
	 */
	public static function before_initialize_action_fields(Jelly_Meta $meta) {}
	
	/**
	 * Called after initialize_action_fields() method.
	 * 
	 * @param Jelly_Meta $meta 
	 */
	public static function after_initialize_action_fields(Jelly_Meta $meta) {}

}
