<?php

defined('SYSPATH') or die('No direct script access.');

class Jelly_Ext_Builder extends Jelly_Builder_Core
{
	/**
	 * Model name getter
	 * 
	 * @return	string
	 */
	public function model()
	{
		return $this->_model;
	}
	
	/**
	 * Model meta getter
	 * 
	 * @return	Jelly_Meta
	 */
	public function meta()
	{
		return $this->_meta;
	}

	/**
	 * Builds the builder into a native query
	 *
	 * @param   string  $type
	 * @param	bool	TRUE for return result always as collection also if limit is 1
	 * @return  void
	 */
	public function execute($db = 'default', $force_collection = FALSE)
	{
		// Don't repeat queries
		if (!$this->_result)
		{
			if ($this->_meta)
			{
				// See if we can use a better $db group
				$db = $this->_meta->db();

				// Select all of the columns for the model if we haven't already
				if ($this->_type === Database::SELECT AND empty($this->_select))
				{
					$this->select('*');
				}
			}

			// We've now left the Jelly
			$this->_result = $this->_build()->execute($db);

			// Hand it over to Jelly_Collection if it's a select
			if ($this->_type === Database::SELECT)
			{
				$model = ($this->_meta) ? $this->_meta->model() : NULL;
				$this->_result = new Jelly_Collection($model, $this->_result);

				// If the record was limited to 1 and collection return is not
				// forced, we only return that model.
				// Otherwise we return the whole result set.
				if ($this->_limit === 1 AND $force_collection === FALSE)
				{
					$this->_result = $this->_result->current();
				}
			}
		}

		// Hand off the result to the Jelly_Collection
		return $this->_result;
	}

	/**
	 * Builds the builder into a native query and returns as collection always.
	 *
	 * @param   string  $type
	 * @return  void
	 */
	public function execute_collection($db = 'default')
	{
		return $this->execute($db, TRUE);
	}

	/**
	 * This is an internal method used for aliasing only things coming
	 * to the query builder, since they can come in so many formats.
	 *
	 * $value is passed so the :unique_key meta alias can be used.
	 *
	 * @param   string   $field
	 * @param   boolean  $join
	 * @param   mixed    $value
	 * @return  string
	 */
	protected function _column($field, $join = TRUE, $value = NULL)
	{
		$model = NULL;

		// Check for functions
		if (strpos($field, '"') !== FALSE)
		{
			// Quote the column in FUNC("ident") identifiers
			return preg_replace('/"(.+?)"/e', '"\\"".$this->_column("$1")."\\""', $field);
		}

		// Test for Database Expressions
		if ($field instanceof Database_Expression)
		{
			return $field;
		}

		// Set if we find this is a reference to a joined field
		$join_table_alias = FALSE;

		// Field has no model
		if (strpos($field, '.') === FALSE)
		{
			// If we have a meta alias with no model use this model to resolve it
			// or if we have a valid field for this model assume that's what we mean
			if (strpos($field, ':') !== FALSE OR ($this->_meta AND $this->_meta->fields($field)))
			{
				$field = $this->_model.'.'.$field;
			}
			else
			{
				// This is not a model field or meta alias, so don't bother trying to alias it and
				// return it as it is
				return $field;
			}
		}
		else
		{
			list($model, $field) = explode('.', $field, 2);
			if (!is_object($this->_meta))
			{
				throw new Kohana_Exception("Undefined model `$model`, check if correct plural is set in inflector.");
			}
			// Check to see if the 'model' passed is actually a relationship alias
			if ($field_object = $this->_meta->fields($model) AND $field_object instanceof Jelly_Field_Behavior_Joinable)
			{
				// The model specified looks like a relationship alias in this context
				// that means we alias the field name to a column but use the join alias for the table
				$join_table_alias = Jelly::join_alias($field_object);

				// Change the field to use the appropriate model so it can be properly aliased
				$field = $field_object->foreign['model'].'.'.$field;
			}
			else
			{
				// Put field back together
				$field = $model.'.'.$field;
			}
		}

		$alias = Jelly::alias($field, $value);

		if ($join_table_alias)
		{
			// Replace the actual table with the join alias
			$alias['table'] = $join_table_alias;
		}

		if ($join)
		{
			return implode('.', $alias);
		}
		else
		{
			return $alias['column'];
		}
	}
	
	/**
	 * Counts the current query builder result
	 *
	 * @return  int
	 */
	public function count()
	{
		if ($this->_distinct)
		{
			// Use specific counting method for distinct
			return $this->_count_distinct();
		}
		
		$query = $this->_build(Database::SELECT);
		$db = (is_object($this->_meta)) ? $this->_meta->db() : 'default';
		
		// Dump a few unecessary bits that cause problems anyway
		$query->_select = $query->_order_by = array();

		// Find the count
		return (int) $query
						->select(array('COUNT("*")', 'total'))
						->execute($db)
						->get('total');
	}
	
	/**
	 * Counts the current query builder result when distinct is TRUE
	 *
	 * @return  int
	 */
	protected function _count_distinct()
	{
		$query = $this->_build(Database::SELECT);
		$db = (is_object($this->_meta)) ? $this->_meta->db() : 'default';
		
		// Dump a few unecessary bits that cause problems anyway
		$query->_select = $query->_order_by = array();
		
		// Find the count
		return (int) $query
						->select(array('COUNT(DISTINCT '.$this->_column(':unique_key').')', 'total'))
						->execute($db)
						->get('total');
	}

	public function load_idstring($key)
	{
		return $this->where('idstring', '=', $key)
						->load();
	}

	public function filter_deleted($alias = NULL, $state = TRUE)
	{
		if ($state === TRUE)
		{
			$state = Behavior_Restorable::$deleted_filter;
		}

		if ($state !== NULL)
		{
			$this->where(($alias ? $alias.'.' : '').'deleted', '=', $state);
		}

		return $this;
	}

	public function filter_active($alias = NULL, $state = TRUE)
	{
		if ($state === TRUE)
		{
			$state = Behavior_Activeable::$active_filter;
		}

		if ($state !== NULL)
		{
			$this->where(($alias ? $alias.'.' : '').'active', '=', $state);
		}

		return $this;
	}
	
	public function filter_i18n($alias = NULL, $lang = NULL)
	{
		if ($lang === NULL)
		{
			$lang = I18n::lang();
		}

		$this->where(($alias ? $alias.'.' : '').'i18n', '=', $lang);

		return $this;
	}

}
