<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Field_Date_Auto extends Field_Date
{

	/**
	 * @var	mixed	Use for generating default value
	 */
	public $default_offset;

	/**
	 * Set actual time on object create when not set
	 *
	 * @param   mixed  $value
	 * @param	Jelly_Model
	 * @return  mixed
	 */
	public function set($value, Jelly_Model $model = NULL)
	{
		// Set it only when we are creating record and value is not set
		if ( $model AND ! $model->loaded() AND empty($value))
		{
			$value = $this->default_value();
		}

		return parent::set($value, $model);
	}

	/**
	 * Returns a particular value processed according
	 * to the class's standards.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed        $value
	 * @param   boolean      $loaded
	 * @return  mixed
	 **/
	public function get($model, $value)
	{
		if ($value == $this->default)
		{
			return $this->default_value();
		}

		return $value;
	}

	/**
	 * Create default timestamp from default_offset variable if given
	 *
	 * @return	timestamp
	 * @uses	Date::time()
	 * @uses	Date::move()
	 */
	protected function default_value()
	{
		$value = Date::time();

		if ($this->default_offset)
		{
			if (is_numeric($this->default_offset))
			{
				$value = Date::move($value, $this->default_offset, 'second');
			}
			elseif (is_array($this->default_offset))
			{
				$value = call_user_func_array('Date::move', array_merge(array($value), $this->default_offset));
			}
			else
			{
				$value = Date::move($value, 1, $this->default_offset);
			}
		}

		return $value;
	}

}
