<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles timestamps and conversions to and from different formats.
 *
 * All timestamps are represented internally by UNIX timestamps, regardless
 * of their format in the database. When the model is saved, the value is
 * converted back to the format specified by $format (which is a valid
 * date() string).
 *
 * This means that you can have timestamp logic exist relatively indepentently
 * of your database's format. If, one day, you wish to change the format used
 * to represent dates in the database, you just have to update the $format
 * property for the field.
 *
 * @package  Jelly
 */
abstract class Jelly_Field_Updated_By extends Field_BelongsTo
{

	/**
	 * @var  string  The column's name in the database
	 */
	public $column = 'updated_by';

	/**
	 * A string pointing to the foreign model and (optionally, a
	 * field, column, or meta-alias).
	 *
	 * Assuming an author belongs to a role named 'role':
	 *
	 *  * '' would default to role.:primary_key
	 *  * 'role' would expand to role.:primary_key
	 *  * 'role.id' would remain untouched.
	 *
	 * The model part of this must point to a valid model, but the
	 * field part can point to anything, as long as it's a valid
	 * column in the database.
	 *
	 * @var  string
	 */
	public $foreign = 'user.id';

	/**
	 * Called just before saving if the field is $in_db, and just after if it's not.
	 *
	 * If $in_db, it is expected to return a value suitable for insertion
	 * into the database.
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function save($model, $value, $loaded)
	{
		// Set it only when we are creating record or when updating and record was changed
		// and value of this field value is not changed
		if ( ! $model->changed($this->name) AND ( ! $loaded OR $model->is_user_changed()))
		{
			$value = A2::instance()->a1->get_user_id();
		}

		return parent::save($model, $value, $loaded);
	}

}
