<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles timestamps and conversions to and from different formats.
 *
 * All timestamps are represented internally by UNIX timestamps, regardless
 * of their format in the database. When the model is saved, the value is
 * converted back to the format specified by $format (which is a valid
 * date() string).
 *
 * This means that you can have timestamp logic exist relatively indepentently
 * of your database's format. If, one day, you wish to change the format used
 * to represent dates in the database, you just have to update the $format
 * property for the field.
 *
 * @package  Jelly
 */
abstract class Jelly_Field_Date extends Field_Timestamp
{

	/**
	 * @var  string  A date formula representing the time in the database
	 */
	public $format = 'Y-m-d';
	
	/**
	 * @var  string  A pretty format used for representing the date to users
	 */
	public $pretty_format = 'd.m.Y';
	
	/**
	 * @var	bool	Contains this field time 
	 */
	public $time = FALSE;

}
