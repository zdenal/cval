<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Field_TimeInterval extends Jelly_Field_Integer
{

	/**
	 * @var  string  Time parts delimiter
	 */
	public $delimiter = ':';

	/**
	 * @var  string  Presentation format
	 */
	public $pretty_format = 'h:m:s';

	/**
	 *
	 * @var	 array	Set times value for each time interval part to convert to second
	 */
	public $parts_to_seconds = array('h' => 3600, 'm' => 60, 's' => 1);

	/**
	 * Converts the value to an integer
	 *
	 * @param   mixed  $value
	 * @return  int
	 */
	public function set($value)
	{
		if ($value === NULL OR ($this->null AND empty($value)))
		{
			return NULL;
		}

		// Already in seconds
		if (is_numeric($value))
		{
			return (int) $value;
		}

		// Clear seconds value
		$seconds = 0;

		// Get time parts
		$time_parts = explode($this->delimiter, $value);
		$format_parts = explode($this->delimiter, $this->pretty_format);

		// Get only possible values according to format
		$time_parts = array_slice($time_parts, 0, count($format_parts));

		foreach ($time_parts as $pos => $part)
		{
			$part = (int) trim($part);
			$seconds_times = $this->parts_to_seconds[$format_parts[$pos]];

			$seconds += ($part * $seconds_times);
		}

		return $seconds;
	}

	/**
	 * Converts the value to pretty format
	 *
	 * @param   mixed  $value
	 * @return  int
	 */
	public function get_pretty($value)
	{
		if (empty($value))
		{
			return NULL;
		}

		$string_values = array();

		$format_parts = explode($this->delimiter, $this->pretty_format);

		foreach ($format_parts as $pos => $part)
		{
			$seconds_times = $this->parts_to_seconds[$part];

			$val = floor($value / $seconds_times);
			$value -= ($val * $seconds_times);

			$string_values[] = sprintf('%02d', $val);
		}

		$string = implode($this->delimiter, $string_values);

		return $string;
	}

	/**
	 * Displays the particular field as a form item
	 *
	 * @param string $prefix The prefix to put before the filename to be rendered
	 * @return View
	 **/
	public function input($prefix = 'jelly/field', $data = array())
	{
		$view = parent::input($prefix, $data);

		$view->set('pretty_value', $this->get_pretty($view->get('value')));

		return $view;
	}

}
