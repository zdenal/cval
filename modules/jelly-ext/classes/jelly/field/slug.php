<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Slug field, which autogenerates when no value set
 *
 * @package  Jelly
 */
abstract class Jelly_Field_Slug extends Field_String
{

	/**
	 * @var  string  Source field for generating slug value
	 */
	public $slug_field = 'label';

	/**
	 * @var  string  Source method for generating slug value
	 */
	public $slug_method = 'slug_source';

	/**
	 * @var	 mixed	FALSE if we do not check slug with i18n or i18n field name
	 */
	public $i18n_field = FALSE;

	/**
	 * Converts a slug to value valid for a URL.
	 *
	 * We could validate it by setting a rule, but for the most part, who cares?
	 *
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function set($value, Jelly_Model $model = NULL)
	{
		// Set it only when we are creating record or value is not set
		if ( $model AND ! $model->loaded() AND empty($value))
		{
			// Try to use slug method if found
			if (is_callable(array($model, $this->slug_method)))
			{
				$value = call_user_func(array($model, $this->slug_method));
			}
			else
			{
				$value = $model->{$this->slug_field};
			}
		}
		
		// Create slug
		$value = URL::title_ascii($value);

		return $value;
	}

	/**
	 * Called just before saving if the field is $in_db, and just after if it's not.
	 *
	 * If $in_db, it is expected to return a value suitable for insertion
	 * into the database.
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function save($model, $value, $loaded)
	{
		// Save orig value for appending numeric values
		$orig_value = $value;

		$valid = FALSE;
		$suffix = 1;

		// Validate fields and add suffix if same found
		do
		{
			$count = Jelly::select($this->model)
				->where($this->name, '=', $value);

			if ($loaded)
			{
				$count = $count->where($model->meta()->primary_key(), '<>', $model->{$model->meta()->primary_key()});
			}

			if ($this->i18n_field !== FALSE)
			{
				$count = $count->where($this->i18n_field, '=', $model->{$this->i18n_field});
			}

			// If no same slug found it is ok
			if ( ! $count->count())
			{
				$valid = TRUE;
			}
			else
			{
				// Append suffix to slug and increment it
				$value = $orig_value.'-'.$suffix++;
			}
		}
		while ( ! $valid);

		return $value;
	}

}
