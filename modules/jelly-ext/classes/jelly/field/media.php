<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles files and file uploads.
 *
 * If a valid upload is set on the field, the upload will be saved
 * automatically to the $path set and the value of the field will
 * be the filename used.
 *
 * @package  Jelly
 */
abstract class Jelly_Field_Media extends Jelly_Field
{
	/**
	 * @var  boolean  Whether or not to delete the old file when a new file is added
	 */
	public $delete_old_file = TRUE;

	/**
	 * @var string	path to folder
	 */
	public $path = '';
	
	/**
	 * @var string	path to folder which is accesible to web
	 */
	public $folder = '';

	/**
	 * @var string	suffix appended to path and folder
	 */
	public $path_suffix = '';

	public $tmp_path = '';

	public $tmp_folder = '';

	/**
	 * @var string	suffix appended to tmp path and tmp folder
	 */
	public $tmp_path_suffix = 'media';

	public $tmp_lifetime = 1800;

	public $chmod = NULL;

	public $upload = array();

	// Enable jcrop functionality
	public $jcrop = FALSE;

	public $transform = array();

	public $transform_image = array();

	public $transform_image_quality = 90;

	public $transform_image_force = TRUE;

	public $transform_on_upload = FALSE;

	public $image_types = array('jpg', 'png', 'gif');

	/**
	 * This is called after construction so that fields can finish
	 * constructing themselves with a copy of the column it represents.
	 *
	 * @param   string  $model
	 * @param   string  $column
	 * @return  void
	 **/
	public function initialize($model, $column)
	{
		parent::initialize($model, $column);

		// Init paths
		$this->path = $this->init_path('path', strtr($this->path, array(
			':model' => str_replace('_', '/', $this->model),
			':field' => $this->name
		)), strtr(UPLOADMODELPATH, array(
			':model' => str_replace('_', '/', $this->model),
			':field' => $this->name
		)));
		$this->tmp_path = $this->init_path('tmp_path', $this->tmp_path, UPLOADTMPPATH);

		// Init folders
		$this->folder = $this->init_folder('folder', strtr($this->folder, array(
			':model' => str_replace('_', '/', $this->model),
			':field' => $this->name
		)), strtr(UPLOADMODELFOLDER, array(
			':model' => str_replace('_', '/', $this->model),
			':field' => $this->name
		)));
		$this->tmp_folder = $this->init_folder('tmp_folder', $this->tmp_folder, UPLOADTMPFOLDER);

		// Add suffixes to paths and folders
		if ($this->path_suffix)
		{
			$this->path = $this->init_path('path', $this->path.$this->path_suffix);
			$this->folder = $this->init_folder('folder', $this->folder.$this->path_suffix);
		}
		if ($this->tmp_path_suffix)
		{
			$this->tmp_path = $this->init_path('tmp_path', $this->tmp_path.$this->tmp_path_suffix);
			$this->tmp_folder = $this->init_folder('tmp_folder', $this->tmp_folder.$this->tmp_path_suffix);
		}
		//echo Kohana::debug_exit($this->path, $this->tmp_path, $this->folder, $this->tmp_folder);
	}

	protected function init_path($name, $value, $default = NULL)
	{
		$path = $value;

		if (empty($path))
		{
			$path = $default;
		}
		// Normalize the path
		$path = Dir::normalize($path);

		// Ensure path is writeable
		if ( ! is_writable($path))
		{
			if (file_exists($path) OR ! Dir::create($path))
			{
				throw new Kohana_Exception('Media field `:field` for model `:model` have a `:name` property set that points to directory `:path`. This directory must be writeable.', array(
					':model' => $this->model,
					':field' => $this->name,
					':name' => $name,
					':path' => $path,
				));
			}
		}

		return $path;
	}

	protected function init_folder($name, $value, $default = NULL)
	{
		$folder = $value;

		if (empty($folder))
		{
			$folder = $default;
		}
		// Normalize the folder
		$folder = rtrim($folder, '/').'/';

		return $folder;
	}

	public function destination($value, $url = FALSE, $default = NULL)
	{
		$exists = $value ? Media::destination($this->path.$value, FALSE, FALSE) : NULL;

		if ($url)
		{
			if ($exists)
			{
				return Media::url($this->folder.$value, FALSE);
			}
			elseif ( ! empty($default))
			{
				return Media::url($default, FALSE);
			}
		}

		return $exists;
	}

	public function destination_alternate($prepend, $value, $url = FALSE, $default = NULL)
	{
		$exists = $value ? Media::destination($this->path.$prepend.$value, FALSE, FALSE) : NULL;

		if ($url)
		{
			if ($exists)
			{
				return Media::url($this->folder.$prepend.$value, FALSE);
			}
			elseif ( ! empty($default))
			{
				return Media::url($default, FALSE);
			}
		}

		return $exists;
	}

	/**
	 * Uploads a file if we have a valid upload
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @param   bool   $loaded
	 * @return  string|NULL
	 */
	public function save($model, $value, $loaded)
	{
		// Get original value
		$original = $model->get($this->name, FALSE);

		// When empty value do not save file
		if (empty($value))
		{
			// Delete when value is empty and changed from original
			if ($original != $value)
			{
				$this->unlink($original, $this->path.$original, $model, $loaded);
			}

			return $value;
		}
		
		// New file is not uploaded
		if ($value == $original)
		{
			return $value;
		}

		// Construct temp file path
		$tmp_file = $this->tmp_path.$value;

		$new_file = FALSE;

		// If temp path exists
		if (file_exists($tmp_file))
		{
			$file = $this->path.$value;

			if ($this->_copy($tmp_file, $file, $value, $model, $loaded))
			{
				if ($this->chmod !== FALSE)
				{
					// Set permissions on filename
					$this->_chmod($file, $this->chmod);
				}

				if ($this->delete_old_file AND ! empty($original) AND $original != $value AND $original != $this->default)
				{
					$this->unlink($original, $this->path.$original, $model, $loaded);
				}

				$new_file = TRUE;
			}
			else
			{
				throw new Kohana_Exception('File upload failed. Moving temp file from `:from` to `:to` was not successful.', array(
					':from' => $tmp_file,
					':to' => $file
				));
			}

			$this->_unlink_tmp($tmp_file);
		}
		else
		{
			throw new Kohana_Exception('File upload failed. Temp file `:file` does not exists.', array(
				':file' => $tmp_file
			));
		}

		if ( ! $this->transform_on_upload)
		{
			// We have a new file here
			$this->transform($value, $this->path.$value, $model, $loaded, $new_file);
		}

		return $value;
	}

	protected function _copy($tmp_file, $file, $value, Jelly_Model $model, $loaded)
	{
		// Copy file
		return File::copy($tmp_file, $file);
	}

	protected function _chmod($file, $chmod)
	{
		// Set permissions on filename
		return File::chmod($file, $chmod);
	}

	protected function _unlink_tmp($tmp_file)
	{
		return unlink($tmp_file);
	}

	/**
	 * Called just after deleting object.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed  $value
	 * @return  void
	 */
	public function delete(Jelly_Model $model, $value)
	{
		if ($value)
		{
			// Delete file when deleting object
			$this->unlink($value, $this->path.$value, $model, TRUE);
		}
	}

	public function unlink($file, $path, Jelly_Model $model, $loaded)
	{
		// File not found, so suppose its unlinked
		if ( ! file_exists($path) OR is_dir($path))
		{
			return;
		}

		// Make sure same file does not have any other item,
		// otherwise do not delete it
		$models = Jelly::select($model->meta()->model())
			->where($this->name, '=', $file);

		// If we are loaded do not count us
		if ($loaded)
		{
			$models->where($model->meta()->primary_key(), '!=', $model->id());
		}

		$models = $models->execute();

		// Nothing found so unlink it
		if ( ! $models->count())
		{
			unlink($path);
		}
	}

	public function transform($file, $path, Jelly_Model $model, $loaded, $new_file = FALSE, $options = NULL)
	{
		// File not found, so do nothing
		if ( ! file_exists($path) OR is_dir($path))
		{
			throw new Kohana_Exception('File transform failed. File `:file` does not exists or is a directory.', array(
				':file' => $path
			));
		}

		if ($options === NULL)
		{
			$options = array(
				'target' => NULL,
				'image' => array(
					'jcrop' => $this->jcrop === FALSE ? FALSE : array(
						'id' => 'jcrop-coord-'.'au-preview-image-'.$this->name.'-',
						'data' => $_POST
					),
					'force' => $this->transform_image_force,
					'quality' => $this->transform_image_quality,
					'methods' => $this->transform_image
				)
			);
		}

		if (($target = Arr::get($options, 'target', NULL)) !== NULL)
		{
			if (is_dir($target))
			{
				throw new Kohana_Exception('File transform failed. Target file `:file` is a directory.', array(
					':file' => $target
				));
			}
		}

		$jcrop_options = Arr::path($options, 'image.jcrop', FALSE);

		// Try to append crop before other crop tools
		if ($jcrop_options !== FALSE AND $this->is_image($path))
		{
			$jcrop_coord_id = Arr::get($jcrop_options, 'id');
			$jcrop_data = Arr::get($jcrop_options, 'data', array());

			// Check if we have coords values
			if (Arr::get($jcrop_data, $jcrop_coord_id.'w'))
			{
				$jcrop_coord_keys = array('x','y','w','h','jcropw');

				// Overwrite setSelect array
				$jcrop_setSelect = array();
				foreach ($jcrop_coord_keys as $jcrop_coord_key)
				{
					$jcrop_setSelect[] = Arr::get($jcrop_data, $jcrop_coord_id.$jcrop_coord_key, 0);
				}
				
				// Add crop to image transform
				$options['image']['methods'] = array_merge(array(
					'jcrop' => $jcrop_setSelect
				), Arr::path($options, 'image.methods', array()));

				// Act file as new, so all transform will proceed
				$new_file = TRUE;
			}
		}

		// Transform only when new file
		if ( ! $new_file)
		{
			return;
		}

		$image_options = Arr::get($options, 'image', FALSE);

		$image_force = Arr::get($image_options, 'force');
		$image_methods = Arr::get($image_options, 'methods', array());
		// There needs to be some methods to use or transform needs to be forced
		if (($image_force OR ! empty($image_methods)) AND $this->is_image($path))
		{
			$image = Image::factory($path);

			// Set if resizing is forced or not
			$image->force_transform($image_force);

			foreach ($image_methods as $method => $params)
			{
				call_user_func_array(array($image, $method), empty($params) ? array() : $params);
			}

			$image->save(Arr::get($options, 'target'), Arr::get($image_options, 'quality', $this->transform_image_quality));
		}
	}

	public function upload(Jelly_Model $model, $value)
	{
		// Clear tmp folder
		$this->delete_old_tmp_files();

		// Upload a file?
		if (is_array($value) AND Upload::valid($value))
		{
			// Get separated extension and name and construct uniqid for tmp folder
			$ext = strtolower(pathinfo($value['name'], PATHINFO_EXTENSION));
			$name = URL::title_ascii(substr($value['name'], 0, (strlen($ext)+1)*-1));
			$uniqid = $this->upload_uniqid($name, $ext);

			if (FALSE !== ($upload = Upload::save_verbose($value, $uniqid, $this->tmp_path)))
			{
				// Ensure we have no leading slash
				$upload['filename'] = trim($upload['filename'], '/');

				if ($this->transform_on_upload)
				{
					$this->_upload_transform($upload['filename'], $upload['path'], $model, FALSE, TRUE);
				}

				$upload['ext'] = $ext;
				$upload['url'] = Media::url($this->tmp_folder.$upload['filename'], FALSE);
				$upload['is_image'] = $this->is_image($upload['path']);

				return $this->_upload_response($model, $uniqid, $upload);
			}
		}

		return FALSE;
	}

	public function upload_uniqid($name, $ext)
	{
		return $name.uniqid().'.'.$ext;
	}

	protected function _upload_response(Jelly_Model $model, $uniqid, $upload)
	{
		return array(
			'name' => $upload['filename'],
			'file' => $upload['path'],
			'url' => $upload['url'],
			'is_image' => $upload['is_image'],
		);
	}

	protected function _upload_transform($file, $path, Jelly_Model $model, $loaded, $new_file = FALSE, $options = NULL)
	{
		$this->transform($file, $path, $model, $loaded, $new_file, $options);
	}

	public function delete_old_tmp_files()
	{
		Media::delete_old_tmp_files($this->tmp_path, $this->tmp_lifetime);
	}

	/**
	 * Validates single field with its rules
	 *
	 * @param	mixed	field value
	 * @throws	Validate_Exception
	 */
	public function validate($value = NULL, $upload = FALSE)
	{
		// Create the validation object
		$data = Validate::factory(array($this->name => $value));

		if ($upload === TRUE)
		{
			$data->label($this->name, $this->label);
			$data->filters($this->name, Arr::get($this->upload, 'filters', array()));
			$data->rules($this->name, Arr::get($this->upload, 'rules', array()));
			$data->callbacks($this->name, Arr::get($this->upload, 'callbacks', array()));
		}
		else
		{
			$data->label($this->name, $this->label);
			$data->filters($this->name, $this->filters);
			$data->rules($this->name, $this->rules);
			$data->callbacks($this->name, $this->callbacks);
		}

		if ( ! $data->check())
		{
			throw new Validate_Exception($data);
		}
	}

	public function is_image($ext)
	{
		// Check if extension is not actually file with extension
		if (strpos($ext, '.') !== FALSE)
		{
			// Get extension
			$ext = strtolower(pathinfo($ext, PATHINFO_EXTENSION));
		}

		return in_array($ext, $this->image_types);
	}
}
