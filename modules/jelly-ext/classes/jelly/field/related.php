<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Allows to get field value from relationship object
 *
 * @package  Cval
 * @author   Zdenal
 */
abstract class Jelly_Field_Related extends Jelly_Field
{
	/**
	 * This field is not in table
	 *
	 * @var  string
	 */
	public $in_db = FALSE;

	/**
	 * Name of relationship field for getting the value
	 *
	 * @var	 string
	 */
	public $relationship = '';

	/**
	 * Name of related object field
	 *
	 * @var	 string
	 */
	public $relationship_field = '';

	/**
	 * Overrides the initialize to automatically provide the column name
	 *
	 * @param   string  $model
	 * @param   string  $column
	 * @return  void
	 */
	public function initialize($model, $column)
	{
		// Default to the name of the column
		if (empty($this->relationship_field))
		{
			$this->relationship_field = $column;
		}

		parent::initialize($model, $column);
	}

	/**
	 * Returns a particular value processed according
	 * to the class's standards.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed        $value
	 * @param   boolean      $loaded
	 * @return  mixed
	 **/
	public function get($model, $value)
	{
		return $model->{$this->relationship}->{$this->relationship_field};
	}
	
	/**
	 * Sets a particular value processed according
	 * to the class's standards.
	 *
	 * @param   mixed  $value
	 * @return  mixed
	 **/
	public function set($value, $model = NULL)
	{
		$model->{$this->relationship}->{$this->relationship_field} = $value;
		
		return $this->get($model, NULL);
	}

	/**
	 * Displays a selection of models to relate to
	 *
	 * @param   string  $prefix  The prefix to put before the filename to be rendered
	 * @return  View
	 **/
	public function input($prefix = 'jelly/field', $data = array())
	{
		return $data['model']->{$this->relationship}->input($this->relationship_field, $prefix, $data);
	}
}
