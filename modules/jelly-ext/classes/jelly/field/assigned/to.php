<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles timestamps and conversions to and from different formats.
 *
 * All timestamps are represented internally by UNIX timestamps, regardless
 * of their format in the database. When the model is saved, the value is
 * converted back to the format specified by $format (which is a valid
 * date() string).
 *
 * This means that you can have timestamp logic exist relatively indepentently
 * of your database's format. If, one day, you wish to change the format used
 * to represent dates in the database, you just have to update the $format
 * property for the field.
 *
 * @package  Jelly
 */
abstract class Jelly_Field_Assigned_To extends Field_BelongsTo
{

	/**
	 * @var  string  The column's name in the database
	 */
	public $column = 'assigned_to';

	/**
	 * A string pointing to the foreign model and (optionally, a
	 * field, column, or meta-alias).
	 *
	 * Assuming an author belongs to a role named 'role':
	 *
	 *  * '' would default to role.:primary_key
	 *  * 'role' would expand to role.:primary_key
	 *  * 'role.id' would remain untouched.
	 *
	 * The model part of this must point to a valid model, but the
	 * field part can point to anything, as long as it's a valid
	 * column in the database.
	 *
	 * @var  string
	 */
	public $foreign = 'user.id';
	
	/**
	 * Returns a particular value processed according
	 * to the class's standards.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed        $value
	 * @param   boolean      $loaded
	 * @return  mixed
	 **/
	public function get($model, $value)
	{
		// Changed to set default value on create, user can deselect the field
		if ( ! $model->loaded() AND empty($value))
		{
			$value = A2::instance()->a1->get_user_id();
		}

		return parent::get($model, $value);
	}

}
