<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Wysiwyg field, same as text, but differs in view
 *
 * @package  Jelly
 */
abstract class Jelly_Field_Wysiwyg extends Field_Text
{
	/**
	 * @var	int		Wysiwyg config name
	 */
	public $config = 'default';

	/**
	 * @var	int		Number of rows for textarea
	 */
	public $rows = 12;
	
	/**
	 * @var	int		Number of cols for textarea
	 */
	public $cols = 40;

}
