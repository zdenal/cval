<?php defined('SYSPATH') or die('No direct script access.');

/**
 * URL field, prepend protocol if not set
 *
 * @package  Jelly
 */
abstract class Jelly_Field_URL extends Field_String
{

	/**
	 * @var  string  Protocol to add if no set. Set this NULL to not add it.
	 */
	public $protocol = 'http';

	/**
	 * Sets all options
	 *
	 * @return  void
	 **/
	public function __construct($options = array())
	{
		parent::__construct($options);
		
		// Add url rule
		$this->rules['url'] = NULL;
	}
	
	/**
	 * Casts to a string, preserving NULLs along the way
	 *
	 * @param  mixed   $value
	 * @return string
	 */
	public function set($value)
	{
		$value = parent::set($value);
		
		if (empty($value))
		{
			return $value;
		}
		
		// No protocol
		if ($this->protocol !== NULL AND ! parse_url($value, PHP_URL_SCHEME))
		{
			// Add http
			$value = $this->protocol.'://'.$value;
		}

		return $value;
	}

}
