<?php defined('SYSPATH') or die('No direct script access.');

/**
 * An abstract class that is useful for identifying which
 * fields that are relationships.
 *
 * @package  Jelly
 * @author   Jonathan Geiger
 */
abstract class Jelly_Field_Relationship extends Jelly_Field
{
	/**
	 * Most relationship are in fact not part of the model's table
	 *
	 * @var  string
	 */
	public $in_db = FALSE;

	/**
	 * Generally contains details of the field's relationship
	 *
	 * @var  string
	 */
	public $foreign = array();
	
	/**
	 * Specify what should be on related object delete in database layer.
	 * Possible values:
	 *		"RESTRICT"
	 *		"CASCADE"
	 *		"SET NULL" - default
	 *		"NO ACTION" - same as NULL
	 * 
	 * @var  string
	 */
	public $on_delete = "SET NULL";
	
	/**
	 * Specify what should be on related object delete in database layer.
	 * Possible values:
	 *		"RESTRICT"
	 *		"CASCADE"
	 *		"SET NULL"
	 *		"NO ACTION" - same as NULL, default
	 * 
	 * @var  string
	 */
	public $on_update = NULL;
	
	/**
	 * @var mixed	valid php callback used in input method when constructing
	 *				options to enum field
	 */
	public $input_items_callback = NULL;

	/**
	 * Displays a selection of models to relate to
	 *
	 * @param   string  $prefix  The prefix to put before the filename to be rendered
	 * @return  View
	 **/
	public function input($prefix = 'jelly/field', $data = array())
	{
		if ( ! isset($data['options']))
		{
			if ( ! isset($data['add_options']) OR $data['add_options'])
			{
				$items = $this->input_items($prefix, $data, Jelly::select($this->foreign['model']))
					->execute();

				$options = array();
				foreach ($items as $item)
				{
					$options[$item->id()] = $item->name();
				}
			}
			else
			{
				$options = array();
			}
			
			$data['options'] = $options;
		}

		return parent::input($prefix, $data);
	}
	
	/**
	 * Tries to get related field instance from foreign model. If related field
	 * is not defined in foreign model it returns NULL.
	 * 
	 * @return	Jelly_Field
	 */
	public function foreign_field()
	{
		return NULL;
	}
	
	/**
	 * Called just before get() method.
	 *
	 * @param   string  $prefix  The prefix to put before the filename to be rendered
	 * @param   mixed   $value	 aditional data
	 * @param   Jelly_Builder	items builder	
	 * @return  Jelly_Builder
	 */
	public function input_items($prefix, $data, Jelly_Builder $items)
	{
		if (is_callable($this->input_items_callback))
		{
			$args = func_get_args();
			$items = call_user_func_array($this->input_items_callback, $args);
		}

		return $items;
	}
}
