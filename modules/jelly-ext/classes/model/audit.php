<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model Audit
 * @package Jelly Ext
 * @author Zdennal
 */
class Model_Audit extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{

		$meta->fields(array(
			'id' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'max_length' => array(255)
				)
			)),
			'db_table' => new Field_String(array(
				'rules' => array(
					'max_length' => array(255)
				)
			)),
			'db_column' => new Field_String(array(
				'rules' => array(
					'max_length' => array(255)
				)
			)),
			'db_id' => new Field_String(array(
				'rules' => array(
					'max_length' => array(255)
				)
			)),
			'old_value' => new Field_Text,
			'new_value' => new Field_Text
		));
		Behavior_Timestampable::initialize($meta, array(
			'created_at' => FALSE,
		));
		Behavior_Userable::initialize($meta, array(
			'created_by' => FALSE,
		));
	}

	protected function _before_save()
	{
		parent::_before_save();
		$this->updated_at = Date::time();
		// Construct primary key
		$parts = array(
			$this->updated_at,
			$this->db_id,
			$this->db_table,
			$this->db_column,
		);

		$this->id = substr(implode('__', $parts), 0, 255);
	}

}
