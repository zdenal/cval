<?php defined('SYSPATH') or die('No direct script access.');

class Field_Text extends Jelly_Field_Text
{
	/**
	 * @var	int		Number of rows for textarea
	 */
	public $rows = 8;
	
	/**
	 * @var	int		Number of cols for textarea
	 */
	public $cols = 40;
}