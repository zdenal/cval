<?php defined('SYSPATH') or die('No direct script access.');

class Field_Primary extends Jelly_Field_Primary
{
	/**
	 * @var  boolean  Tells if field is autoincrement. Just for generator needs.
	 */
	public $autoincrement = TRUE;
	
	/**
	 * @var  boolean  Tells if field is integer or string when FALSE.
	 *				  Just for generator needs.
	 */
	public $integer = TRUE;
}