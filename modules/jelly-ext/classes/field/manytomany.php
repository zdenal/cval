<?php defined('SYSPATH') or die('No direct script access.');

class Field_ManyToMany extends Jelly_Field_ManyToMany
{

	/**
	 * Tries to get related field instance from foreign model. If related field
	 * is not defined in foreign model it returns NULL.
	 * 
	 * @return	Jelly_Field
	 */
	public function foreign_field() 
	{
		$foreign_meta = Jelly::meta($this->foreign['model']);
		$foreign_field_name = NULL;
		$through_model = $this->through['model'];
		
		foreach ($foreign_meta->fields() as $field)
		{
			if ( ! $field instanceof Jelly_Field_ManyToMany)
			{
				continue;
			}
			
			if ($field->through['model'] == $through_model)
			{
				$foreign_field_name = $field->name;
				break;
			}
		}
		
		if ( ! $foreign_field_name)
		{
			return NULL;
		}
		
		return $foreign_meta->fields($foreign_field_name);
	}
	
}