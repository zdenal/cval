<?php defined('SYSPATH') or die('No direct script access.');

class Field_Enum extends Jelly_Field_Enum
{
	/**
	 * @var  boolean  Tells if field is integer or string when FALSE.
	 *				  Just for generator needs.
	 */
	public $integer = FALSE;
	
	/**
	 * @var  boolean  Tells if choices should be translated
	 */
	public $translate_choices = FALSE;
	
	/**
	 * @var	stores array of default not translated choices
	 */
	public $not_translated_choices;
	
	/**
	 * @var mixed	valid php callback used when translating choices
	 */
	public $translate_choices_callback = 'I18n::get';
	
	/**
	 * @var  boolean  Tells if choices should be translated
	 */
	public $ucfirst_choices = FALSE;
	
	/**
	 * @var mixed	valid php callback used when ucfirsting choices
	 */
	public $ucfirst_choices_callback = 'UTF8::ucfirst';
	
	/**
	 * Translate and ucfirst choices if set so
	 *
	 * @param  array $options
	 */
	public function __construct($options = array())
	{
		parent::__construct($options);

		$this->choices = $this->translated_choices();
	}
	
	public function translated_choices()
	{
		if ($this->not_translated_choices === NULL)
		{
			$this->not_translated_choices = $this->choices;
		}
		
		$choices = $this->not_translated_choices;
		
		if ($this->translate_choices OR $this->ucfirst_choices)
		{
			foreach ($choices as $key => $choice)
			{
				if ($this->translate_choices)
				{
					$choice = call_user_func($this->translate_choices_callback, $choice);
				}
				if ($this->ucfirst_choices)
				{
					$choice = call_user_func($this->ucfirst_choices_callback, $choice);
				}
				
				$choices[$key] = $choice;
			}
		}
		
		return $choices;
	}
}