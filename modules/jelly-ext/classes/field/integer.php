<?php defined('SYSPATH') or die('No direct script access.');

class Field_Integer extends Jelly_Field_Integer
{

	/**
	 * @var array	PHP number_format() function params used in 
	 *				this->display_value() method to format value.
	 */
	public $display_format = array(
		'decimals' => 0,
		'dec_point' => '.',
		'thousands_sep' => ' ',
		'symbol' => ''
	);
	
	/**
	 * Converts to float and rounds the number if necessary
	 *
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function set($value)
	{
		if ($value === NULL)
		{
			return NULL;
		}
		
		$value = str_replace('&nbsp;', '', $value);
		$value = str_replace(Arr::get($this->display_format, 'thousands_sep', ','), '', $value);
		$value = str_replace(Arr::get($this->display_format, 'symbol', ''), '', $value);
		
		return parent::set($value);
	}
	
	/**
	 * Formats value according to display_format settings
	 * 
	 * @param	float	
	 * @return	float
	 */
	public function display_value($value, $in_array_with_plain = FALSE, $not_breaking = FALSE)
	{
		$display_value = number_format($value, 
				Arr::get($this->display_format, 'decimals', 0),
				Arr::get($this->display_format, 'dec_point', '.'),
				Arr::get($this->display_format, 'thousands_sep', ',')
			).Arr::get($this->display_format, 'symbol', '');
		
		if ($not_breaking)
		{
			$display_value = str_replace(' ', '&nbsp;', $display_value);
		}
		
		if($in_array_with_plain)
		{
			//TODO not a good way, should be changed with datatable redoing
			return array(
				'value' => $display_value,
				'field_object' => $this
			);
		}
		return $display_value;
	}
	
}