<?php defined('SYSPATH') or die('No direct script access.');

class Field_HasMany extends Jelly_Field_HasMany
{

	/**
	 * Tries to get related field instance from foreign model. If related field
	 * is not defined in foreign model it returns NULL.
	 * 
	 * @return        Jelly_Field
	 */
	public function foreign_field()
	{
		return Jelly::column_to_field($this->foreign['model'], $this->foreign['column']);
	}

}