<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Jelly_Generator creates database structure for Jelly Models.
 *
 * @package Jelly Generator
 */
abstract class Core_Jelly_Generator
{
	
	public static function sql($models = NULL, $fields = NULL, array $options = array())
	{
		if ($models === NULL)
		{
			$models = Kohana::config('jelly/generator.models');
		}
		if ( ! is_array($models))
		{
			$models = array($models);
		}
		
		$sql_parts = array();
		
		if (Arr::get($options, 'transaction'))
		{
			$sql_parts[] = Jelly_Generator::_transaction_open();
		}
		
		foreach ($models as $model)
		{
			$model = Jelly::factory($model);
			
			$sql_parts[] = Jelly_Generator::model($model, $fields);
		}
		
		if (Arr::get($options, 'transaction'))
		{
			$sql_parts[] = Jelly_Generator::_transaction_close();
		}
		
		return implode('', $sql_parts);
	}
	
	public static function model(Jelly_Model $model, $fields = NULL)
	{
		return Jelly_Generator_Model::sql($model, $fields);
	}
	
	public static function field(Jelly_Model $model, Jelly_Field $field)
	{
		return Jelly_Generator_Field::sql($model, $field);
	}
	
	protected static function _transaction_open()
	{
		return "SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE=\"NO_AUTO_VALUE_ON_ZERO\";
SET AUTOCOMMIT=0;
START TRANSACTION;\n\n";
	}
	
	protected static function _transaction_close()
	{
		return "SET FOREIGN_KEY_CHECKS=1;
COMMIT;\n";
	}
	
}
