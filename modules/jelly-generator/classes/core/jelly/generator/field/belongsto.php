<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Handles belongs to relationships
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_BelongsTo extends Core_Jelly_Generator_Field_Relationship
{

	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";

		$sql .= $this->type();

		if ($this->required())
		{
			$sql .= " NOT NULL";
		}

		if ($this->field->default !== NULL)
		{
			$sql .= " DEFAULT ".$this->field->default;
		}

		return $sql;
	}

	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		$foreign_model = $this->field->foreign['model'];
		$foreign_column = $this->field->foreign['column'];

		$foreign_model = Jelly::factory($foreign_model);
		$foreign_field = NULL;

		// Check for a meta-alias first
		if (FALSE !== strpos($foreign_column, ':'))
		{
			$foreign_column = Jelly::meta_alias($foreign_model, $foreign_column);
			$foreign_field = $foreign_model->meta()->fields($foreign_column);
		}
		else
		{
			foreach ($foreign_model->meta()->fields() as $foreign_field_tmp)
			{
				if ($foreign_field_tmp->column == $foreign_column)
				{
					$foreign_field = $foreign_field_tmp;
					break;
				}
			}
		}

		if ($foreign_field === NULL)
		{
			throw new Kohana_Exception('`:model` does not have column with name `:column`.', array(
				':model' => get_class($foreign_model),
				':column' => $foreign_column
			));
		}

		return Jelly_Generator_Field::factory($foreign_model, $foreign_field)->type();
	}

	/**
	 * Returns generated sql for given field if has some foreign keys.
	 * 
	 * @return	string			SQL string
	 */
	public function foreign_key()
	{
		$foreign_model = $this->field->foreign['model'];
		$foreign_column = $this->field->foreign['column'];

		$foreign_model = Jelly::factory($foreign_model);
		$foreign_table = $foreign_model->meta()->table();

		// Check for a meta-alias first
		if (FALSE !== strpos($foreign_column, ':'))
		{
			$foreign_column = Jelly::meta_alias($foreign_model, $foreign_column);
		}

		$foreign_constraint = $this->model->meta()->table()."_".$this->field->column."_".$foreign_table;

		$foreign_constraint_length = strlen($foreign_constraint);
		if ($foreign_constraint_length > 61)
		{
			$trim_length = $foreign_constraint_length - 61;
			$foreign_constraint = substr($foreign_constraint, floor($trim_length / 2), ceil($trim_length / 2) * -1);
		}

		$foreign_constraint = "fk_".$foreign_constraint;

		$sql = "	INDEX `idx_".$this->field->column."` (`".$this->field->column."`),
	CONSTRAINT `".$foreign_constraint."`
		FOREIGN KEY (`".$this->field->column."`)
		REFERENCES `".$foreign_table."` (`".$foreign_column."`)";
		
		if ($this->field->on_delete !== NULL AND $this->required())
		{
			$this->field->on_delete = 'CASCADE';
		}
		
		if ($this->field->on_delete != 'CASCADE' AND $this->required())
			throw new Kohana_Exception('Field `:field` model `:model` is required, it has to be set on_delete `CASCADE`',
					array(
						':model' => $this->model->meta()->model(),
						':field' => $this->field->name
			));

		if ($this->field->on_delete !== NULL)
		{
			$sql .= "
		ON DELETE ".$this->field->on_delete;
		}

		if ($this->field->on_update !== NULL)
		{
			$sql .= "
		ON UPDATE ".$this->field->on_update;
		}

		return $sql;
	}

}
