<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles floats.
 *
 * You can specify an optional places property to
 * round the value to the specified number of places.
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_Float extends Jelly_Generator_Field
{
	
}
