<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles primary keys.
 *
 * Currently, a primary key can be an integer or a string.
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_Primary extends Jelly_Generator_Field
{
	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";
		
		$sql .= $this->type();
		
		if ($this->field->integer AND $this->field->autoincrement)
		{
			$sql .= " AUTO_INCREMENT";
		}
			
		$sql .= " NOT NULL";
		
		return $sql; 
	}
	
	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		if ($this->field->integer)
		{
			return $this->_type_integer();
		}
		else
		{
			return $this->_type_string();
		}
	}
	
	/**
	 * @return	string			SQL string
	 */
	protected function _type_integer()
	{
		$max_int = Kohana::max_int(TRUE);
		
		$max_size = $this->max_size($max_int);
		
		if ($max_size > $max_int)
		{
			throw new Kohana_Exception('`:field` max size of big int can not be greater than :length.', array(
				':length' => $max_int,
				':field' => get_class($this->field),
			));
		}
		
		if ($max_size < 4)
		{
			$column_type = "TINYINT";
		}
		elseif ($max_size < 6)
		{
			$column_type = "SMALLINT";
		}
		elseif ($max_size < 9)
		{
			$column_type = "MEDIUMINT";
		}
		elseif ($max_size < 11)
		{
			$column_type = "INT";
		}
		else
		{
			$column_type = "BIGINT";
		}
		
		return $column_type."(".$max_size.") UNSIGNED";
	}
	
	/**
	 * @return	string			SQL string
	 */
	protected function _type_string()
	{
		$max_size = $this->max_size(50);
		
		return "VARCHAR(".$max_size.")";
	}
}
