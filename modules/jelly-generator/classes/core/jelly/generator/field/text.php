<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles long strings
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_Text extends Jelly_Generator_Field
{
	/**
	 * @param	Jelly_Field		Field object
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";
		
		$sql .= $this->type();
		
		if ($this->required())
		{
			$sql .= " NOT NULL";
		}
		
		if ($this->field->default !== NULL)
		{
			$sql .= " DEFAULT '".$this->field->default."'";
		}
		
		return $sql; 
	}
	
	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		return "TEXT";
	}
}
