<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles timestamps and conversions to and from different formats.
 *
 * All timestamps are represented internally by UNIX timestamps, regardless
 * of their format in the database. When the model is saved, the value is
 * converted back to the format specified by $format (which is a valid
 * date() string).
 *
 * This means that you can have timestamp logic exist relatively indepentently
 * of your database's format. If, one day, you wish to change the format used
 * to represent dates in the database, you just have to update the $format
 * property for the field.
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_Timestamp extends Jelly_Generator_Field
{
	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";
		
		$sql .= $this->type();
		
		if ($this->required())
		{
			$sql .= " NOT NULL";
		}
		
		if ($this->field->default !== NULL)
		{
			$sql .= " DEFAULT '".$this->field->default."'";
		}
		
		return $sql; 
	}
	
	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		if ($this->field->format == 'Y-m-d H:i:s')
		{
			$column_type = 'DATETIME';
		}
		elseif ($this->field->format == 'Y-m-d')
		{
			$column_type = 'DATE';
		}
		elseif ($this->field->format == 'H:i:s')
		{
			$column_type = 'TIME';
		}
		elseif ($this->field->format == 'Y')
		{
			$column_type = 'YEAR';
		}
		else
		{
			$column_type = 'TIMESTAMP';
		}
		
		return $column_type;
	}
}
