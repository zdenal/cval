<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles strings
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_String extends Jelly_Generator_Field
{
	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";
		
		$sql .= $this->type();
		
		if ($this->required())
		{
			$sql .= " NOT NULL";
		}
		
		if ($this->field->default !== NULL)
		{
			$sql .= " DEFAULT '".$this->field->default."'";
		}
		
		return $sql; 
	}
	
	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		$max_size = $this->max_size(50);
		
		return "VARCHAR(".$max_size.")";
	}
}
