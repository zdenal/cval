<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles serialized data.
 *
 * When set, the field attempts to unserialize the data into it's
 * actual PHP representation. When the model is saved, the value
 * is serialized back and saved as a string into the column.
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_Serialized extends Jelly_Generator_Field
{
	
}
