<?php defined('SYSPATH') or die('No direct script access.');

/**
 * An abstract class that is useful for identifying which
 * fields that are relationships.
 *
 * @package  Jelly
 * @author   Jonathan Geiger
 */
abstract class Core_Jelly_Generator_Field_Relationship extends Jelly_Generator_Field
{
	
	/**
	 * Returns generated sql for given field if has some foreign keys.
	 * 
	 * @return	string			SQL string
	 */
	public function foreign_key()
	{
		return NULL; // TODO - only for testing, remove after module is ready
		
		throw new Kohana_Exception('`:field` has not implemented foreign key generator.', array(
			':field' => get_class($this->field)
		));
	}
	
}
