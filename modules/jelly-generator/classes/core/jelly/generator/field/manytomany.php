<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Handles many to many relationships
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_ManyToMany extends Core_Jelly_Generator_Field_Relationship
{

	
	protected static $done_tables = array();
	/**
	 * Returns generated sql for given field if has some foreign keys.
	 * 
	 * @return	string			SQL string
	 */
	public function relation_table()
	{
		$join_table = $this->field->through['model'];
		
		if(in_array($join_table, self::$done_tables))
			return '';
		else
			self::$done_tables[] = $join_table;
		
		$join_column1 = Jelly::meta_alias($this->model, $this->field->through['columns'][0]);
		$join_column2 = Jelly::meta_alias($this->field->foreign['model'], $this->field->through['columns'][1]);


		$sql = "\n"."DROP TABLE IF EXISTS `".$join_table."`;"."\n"."\n";

		$sql .= "CREATE TABLE `".$this->field->through['model']."`("."\n";
		$sql .= "\t`$join_column1` ".$this->_column_type($this->model)." NOT NULL,"."\n";
		$sql .= "\t`$join_column2` ".$this->_column_type($this->field->foreign['model'])." NOT NULL,"."\n";
		$sql .= "\tPRIMARY KEY (`$join_column1`,`$join_column2`),"."\n";
		$sql .= "\t".$this->_one_relate_field_with_constrains($this->model, $join_column1).","."\n";
		$sql .= "\t".$this->_one_relate_field_with_constrains($this->field->foreign['model'], $join_column2)."\n";
		$sql .= ") ENGINE=InnoDB  DEFAULT CHARSET=utf8;"."\n"."\n";

		return $sql;
	}

	protected function _column_type($model_from)
	{
		 $model_from = Jelly::factory($model_from);
		 return Jelly_Generator_Field::factory($model_from,  
				 $model_from->meta()->fields($model_from->meta()->primary_key()))->type();		 
	}

	protected function _one_relate_field_with_constrains($model_from, $join_column)
	{
		$join_table_name = $this->field->through['model'];
		$model_from = Jelly::factory($model_from);
		$model_from_table = $model_from->meta()->table();

		// Check for a meta-alias first
		if (FALSE !== strpos($join_column, ':'))
		{
			$join_column = Jelly::meta_alias($model_from, $join_column);
		}

		$foreign_constraint = $join_table_name."_".$join_column."_".$model_from_table;

		$foreign_constraint_length = strlen($foreign_constraint);
		if ($foreign_constraint_length > 61)
		{
			$trim_length = $foreign_constraint_length - 61;
			$foreign_constraint = substr($foreign_constraint, floor($trim_length / 2), ceil($trim_length / 2) * -1);
		}

		$foreign_constraint = "fk_".$foreign_constraint;

		$sql = "INDEX `idx_".$join_column."` (`".$join_column."`),
	CONSTRAINT `".$foreign_constraint."`
		FOREIGN KEY (`".$join_column."`)
		REFERENCES `".$model_from_table."` (`".$model_from->meta()->primary_key()."`)";


			$sql .= "
		ON DELETE CASCADE";
		return $sql;
	}

}
