<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles has one relationships
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_HasOne extends Core_Jelly_Generator_Field_HasMany
{
	
}
