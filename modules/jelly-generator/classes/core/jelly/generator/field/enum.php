<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Handles enumeration data-types
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_Enum extends Jelly_Generator_Field
{

	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";

		$sql .= $this->type();

		if ($this->required())
		{
			$sql .= " NOT NULL";
		}

		if ($this->field->default !== NULL)
		{
			if (empty($this->field->integer))
			{
				$sql .= " DEFAULT '".$this->field->default."'";
			}
			else
			{
				$sql .= " DEFAULT ".$this->field->default;
			}
		}

		return $sql;
	}

	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		if (empty($this->field->integer))
		{
			$max_size = $this->max_size(50);
		
			return "VARCHAR(".$max_size.")";
		}
		
		$max_int = Kohana::max_int(TRUE);

		$max_size = $this->max_size($max_int);

		if ($max_size > $max_int)
		{
			throw new Kohana_Exception('`:field` max size of big int can not be greater than :length.', array(
				':length' => $max_int,
				':field' => get_class($this->field),
			));
		}

		if ($max_size < 4)
		{
			$column_type = "TINYINT";
		}
		elseif ($max_size < 6)
		{
			$column_type = "SMALLINT";
		}
		elseif ($max_size < 9)
		{
			$column_type = "MEDIUMINT";
		}
		elseif ($max_size < 11)
		{
			$column_type = "INT";
		}
		else
		{
			$column_type = "BIGINT";
		}

		return $column_type."(".$max_size.")";
	}

}
