<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Handles has many relationships
 *
 * @package  Jelly
 */
abstract class Core_Jelly_Generator_Field_HasMany extends Core_Jelly_Generator_Field_Relationship
{
	/**
	 * Returns generated sql for given field if has some foreign keys.
	 * 
	 * @return	string			SQL string
	 */
	public function foreign_key()
	{
		return NULL;
	}
}
