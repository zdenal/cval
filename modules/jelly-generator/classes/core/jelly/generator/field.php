<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Jelly_Generator_Field creates database structure for Jelly Field.
 *
 * @package Jelly Generator
 */
abstract class Core_Jelly_Generator_Field
{

	/**
	 * Returns generated sql for given field.
	 * It decide which generator class use.
	 * 
	 * @param	mixed		Jelly_Field or field name
	 * @return	string			SQL string
	 * @uses	Jelly_Generator_Field::_sql_generate()
	 */
	public static function factory(Jelly_Model $model, $field, $options = array())
	{
		if (!$field instanceof Jelly_Field)
		{
			$field = $model->meta()->fields($field);
		}

		// Get field class
		$field_class = get_class($field);

		do
		{
			// Could be prefixed by Jelly_Field, or just Field_
			$generator_class = str_replace(array('jelly_field_', 'field_'), array('core_jelly_generator_field_', 'jelly_generator_field_'), strtolower($field_class));

			if (class_exists($generator_class))
			{
				return new $generator_class($model, $field, $options);
			}

			$field_class = get_parent_class($field_class);

			if (!$field_class OR $field_class == 'Jelly_Field')
			{
				break;
			}
		}
		while (TRUE);

		return new Jelly_Generator_Field($model, $field, $options);
	}

	/**
	 * @var		Jelly_Model 
	 */
	public $model;
	/**
	 * @var		Jelly_Field
	 */
	public $field;

	/**
	 * Returns generator field instance.
	 * 
	 * @param	Jelly_Model		Field object
	 * @param	Jelly_Field		Field object
	 * @return	Jelly_Generator_Field			
	 */
	public function __construct(Jelly_Model $model, Jelly_Field $field, $options = array())
	{
		$this->model = $model;
		$this->field = $field;

		if (is_array($options))
		{
			// Just throw them into the class as public variables
			foreach ($options as $name => $value)
			{
				$this->$name = $value;
			}
		}
	}

	/**
	 * Returns generated sql for given field.
	 * 
	 * @return	string			SQL string
	 */
	public function column()
	{
		throw new Kohana_Exception('`:field` has not implemented column generator.', array(
			':field' => get_class($this->field)
		));
	}

	/**
	 * Returns generated sql for given field if has some foreign keys.
	 * 
	 * @return	string			SQL string
	 */
	public function relation_table()
	{
		return NULL;
	}

	/**
	 * Returns generated sql for given field if has some foreign keys.
	 * 
	 * @return	string			SQL string
	 */
	public function foreign_key()
	{
		return NULL;
	}

	/**
	 * Returns generated sql for given field if is a primary key.
	 * 
	 * @return	string			SQL string
	 */
	public function primary_key()
	{
		if (!$this->field->primary)
		{
			return NULL;
		}

		return "`".$this->field->column."`";
	}

	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		throw new Kohana_Exception('`:field` has not implemented type generator.', array(
			':field' => get_class($this->field)
		));
	}

	/**
	 * Test if field is required
	 *
	 * @return	bool
	 */
	public function required()
	{
		// If field has not_empty OR Upload::not_empty rule say it is required
		if ((key_exists('not_empty', $this->field->rules) AND $this->field->rules['not_empty'] !== FALSE)
				OR (key_exists('Upload::not_empty', $this->field->rules) AND $this->field->rules['Upload::not_empty'] !== FALSE))
		{
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Returns max size for DB field
	 *
	 * @return	int		Size of field
	 * @return	FALSE	If max size is not set
	 */
	public function max_size($default = FALSE)
	{
		if (!isset($this->field->rules['max_length']))
		{
			return $default;
		}

		return $this->field->rules['max_length'][0];
	}

}
