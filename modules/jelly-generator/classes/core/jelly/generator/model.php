<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Jelly_Generator_Model creates database structure for Jelly Models.
 *
 * @package Jelly Generator
 */
abstract class Core_Jelly_Generator_Model
{
	
	public static function sql(Jelly_Model $model, $fields = NULL)
	{
		// Convert field names to generator field instances
		$fields = Jelly_Generator_Model::_fields($model, $fields);
		
		$sql_parts = array();
		
		$sql_parts[] = Jelly_Generator_Model::_sql_table_open($model);
		
		// Table inner SQL
		$sql_parts_inner = array();
		
		// Insert columns
		$sql_parts_inner[] = Jelly_Generator_Model::_columns($model, $fields);
		
		// Insert primary keys
		$sql_parts_inner[] = Jelly_Generator_Model::_primary_keys($model, $fields);
		
		// Insert foreign keys
		$sql_parts_inner[] = Jelly_Generator_Model::_foreign_keys($model, $fields);
		
		foreach ($sql_parts_inner as $key => $sql_part_inner)
		{
			if ( ! strlen($sql_part_inner))
			{
				unset($sql_parts_inner[$key]);
			}
		}
		
		$sql_parts[] = implode(",\n", $sql_parts_inner)."\n";
		
		$sql_parts[] = Jelly_Generator_Model::_sql_table_close($model);
		
		$sql_parts[] = Jelly_Generator_Model::_relation_tables($model, $fields);
		
		return implode('', $sql_parts);
	}
	
	protected static function _sql_table_open(Jelly_Model $model)
	{
		$meta = $model->meta();
		
		return "DROP TABLE IF EXISTS `".$meta->table()."`;

CREATE TABLE IF NOT EXISTS `".$meta->table()."` (\n";
	}
	
	protected static function _sql_table_close(Jelly_Model $model)
	{
		return ") ENGINE=InnoDB  DEFAULT CHARSET=utf8;\n\n";
	}
	
	protected static function _fields(Jelly_Model $model, $fields = NULL)
	{
		$sql_parts = array();
		
		if ($fields === NULL)
		{
			$fields = array_keys($model->meta()->fields());
		}
		if ( ! is_array($fields))
		{
			$fields = array($fields);
		}
		
		foreach ($fields as $key => $field)
		{
			$field = $model->meta()->fields($field);	
			$fields[$key] = Jelly_Generator_Field::factory($model, $field);
		}
		
		return $fields;
	}
	
	protected static function _columns(Jelly_Model $model, array $fields = array())
	{
		$sql_parts = array();
		
		foreach ($fields as $field)
		{
			if ( ! $field->field->in_db)
			{
				continue;
			}
			$sql_field = $field->column();
			
			if ( ! strlen($sql_field))
			{
				continue;
			}
			
			$sql_field = "\t".$sql_field;
			$sql_parts[] = $sql_field;
		}
		
		return implode(",\n", $sql_parts);
	}
	
	protected static function _primary_keys(Jelly_Model $model, array $fields = array())
	{
		$sql_parts = array();
		
		foreach ($fields as $field)
		{
			if ( ! $field->field->in_db)
			{
				continue;
			}
			$sql_field = $field->primary_key();
			
			if ( ! strlen($sql_field))
			{
				continue;
			}
			
			$sql_parts[] = $sql_field;
		}
		
		if ( ! count($sql_parts))
		{
			throw new Kohana_Exception('`:model` has no primary key.', array(
				':model' => $model->meta()->model()
			));
		}
		
		return "\tPRIMARY KEY (".implode(",", $sql_parts).")";
	}
	
	protected static function _foreign_keys(Jelly_Model $model, array $fields = array())
	{
		$sql_parts = array();
		
		foreach ($fields as $field)
		{
			if ( ! $field->field->in_db)
			{
				continue;
			}
			$sql_field = $field->foreign_key();
			
			if ( ! strlen($sql_field))
			{
				continue;
			}
			
			$sql_field = $sql_field;
			$sql_parts[] = $sql_field;
		}
		
		return implode(",\n", $sql_parts);
	}
	
	protected static function _relation_tables(Jelly_Model $model, array $fields = array())
	{
		$sql_parts = array();
		
		foreach ($fields as $field)
		{
			$sql_field = $field->relation_table();
			
			if ( ! strlen($sql_field))
			{
				continue;
			}
			
			$sql_field = "\t".$sql_field;
			$sql_parts[] = $sql_field;
		}
		
		return implode("\n", $sql_parts);
	}
	
}
