<?php

defined('SYSPATH') or die('No direct script access.');

class Jelly_Generator_Field_Float extends Core_Jelly_Generator_Field_Float
{

	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";

		$sql .= $this->type();

		if ($this->required())
		{
			$sql .= " NOT NULL";
		}

		if ($this->field->default !== NULL)
		{
			$sql .= " DEFAULT '".$this->field->default."'";
		}

		return $sql;
	}

	/**
	 * Returns field type in DB.
	 * 
	 * @return	string			SQL string
	 */
	public function type()
	{
		//TODO enable use different db tables for float, now only decimal(M,D)
		$precision = 65;
		if (isset($this->field->precision))
		{
			$max_length_decimal = $this->field->precision;
		}
		$scale = 30;
		if (isset($this->field->scale))
		{
			$scale = $this->field->scale;
		}
		if ($precision < $scale)
			throw new Kohana_Exception('`:field` has smaller precision than scale.', array(
				':field' => get_class($this->field->name)));
		return "DECIMAL(".$precision.",".$scale.")";
	}

}