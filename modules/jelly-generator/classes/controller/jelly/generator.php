<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Jelly_Generator extends Controller_Ext
{
	
	public function action_index()
	{
		$response = NULL;
		$model = $this->request->param('model');
		
		// Allow to pass more models delimited by comma
		if ($model AND strpos($model, ',') !== FALSE)
		{
			$model = explode(',', $model);
		}
		
		$response = Jelly_Generator::sql(
				$model,
				$this->request->param('fields'), 
				array(
					'transaction' => Arr::get($_REQUEST, 'transaction', 1)
				)
		);
		
		$this->request->response = $response;
		
		$format = Arr::get($_REQUEST, 'format', 'html');
		
		switch ($format)
		{
			case 'file':
				$this->request->send_file(TRUE, 'jelly_generator.sql');
				exit();
			case 'html':
			default:
				$this->request->response = "<pre>".$this->request->response."</pre>";
		}
	}

}

// End Controller_Jelly_Generator