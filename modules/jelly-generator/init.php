<?php defined('SYSPATH') or die('No direct script access.');

Route::set('jelly/generator', 'jelly/generator(/<model>(/<fields>))', array('model' => '[0-9a-zA-Z,_]+'))
	->defaults(array(
		'directory'  => 'jelly',
		'controller' => 'generator',
		'action'     => 'index',
	))->environment(Kohana::DEVELOPMENT);
