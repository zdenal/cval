<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds userable fields to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Userable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$fields = array_merge(array(
			'created_by' => new Field_Created_By(array(
				'column' => 'created_by',
				'foreign' => 'user'
			)),
			'updated_by' => new Field_Updated_By(array(
				'column' => 'updated_by',
				'foreign' => 'user'
			)),
		), $config);

		foreach ($fields as $key => $field)
		{
			if ( ! $field)
			{
				unset($fields[$key]);
			}
		}

		$meta->fields($fields);
	}

}
