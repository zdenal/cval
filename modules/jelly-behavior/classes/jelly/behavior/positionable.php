<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds position field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Positionable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'position' => new Field_Integer(array_merge(array(
				'default' => 0
			), $config)),
		));
	}

}
