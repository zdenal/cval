<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds active field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Activeable
{

	/**
	 * @var	bool	Used for database select. Tells if select active only items.
	 *				Possible values are 1 (active), 0 (inactive), NULL (both)
	 */
	public static $active_filter = 1;
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'active' => new Field_Boolean(array_merge(array(
				'default' => 0
				), $config)),
		));
	}

}
