<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds label field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Labelable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'label' => new Field_String(Arr::merge(array(
				'null' => TRUE,
				'rules' => array(
						'max_length' => array(255)
					)
				), $config)),
		));
	}

}
