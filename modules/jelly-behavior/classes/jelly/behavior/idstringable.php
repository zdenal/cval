<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds userable fields to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Idstringable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'idstring' => new Field_String(array_merge(array(
				'rules' => array(
					'max_length' => array(255)
				)
			), $config)),
		));
	}
}
