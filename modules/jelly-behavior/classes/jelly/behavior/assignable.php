<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds active field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Assignable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'assigned_to' => new Field_Assigned_To(array(
				'null' => TRUE,
				'column' => 'assigned_to',
				'foreign' => 'user'
			), $config),
		));
	}

}
