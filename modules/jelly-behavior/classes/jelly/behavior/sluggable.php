<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds slug field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Sluggable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'slug' => new Field_Slug(Arr::merge(array(
				'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(255)
					),
				), $config)),
		));
	}

}