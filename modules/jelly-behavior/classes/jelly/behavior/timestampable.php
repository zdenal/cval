<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds timestampable fields to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Timestampable
{
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$fields = array_merge(array(
			'created_at' => new Field_Created_At,
			'updated_at' => new Field_Updated_At,
		), $config);

		foreach ($fields as $key => $field)
		{
			if ( ! $field)
			{
				unset($fields[$key]);
			}
		}

		$meta->fields($fields);
	}

}
