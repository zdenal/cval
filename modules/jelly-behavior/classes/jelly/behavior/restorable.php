<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds active field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Restorable 
{

	/**
	 * @var	bool	Used for database select. Tells if select only non-deleted items.
	 *				Possible values are 1 (deleted), 0 (not deleted), NULL (both)
	 */
	public static $deleted_filter = 0;
	
	public static function initialize(Jelly_Meta $meta, array $config = array())
	{
		$meta->fields(array(
			'deleted' => new Field_Boolean(array_merge(array(
				'default' => 0
				), $config)),
		));
	}

}
