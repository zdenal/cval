<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds slug field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_I18nable
{
	
	public static function initialize(Jelly_Meta $meta, $config = NULL)
	{
		$meta->fields(array(
			'i18n' => new Field_Enum(array(
				'choices' => I18n::available_langs(TRUE),
				'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(2)
					)
				)),
			'i18n_parent' => new Field_BelongsTo(array(
				'null' => TRUE,
				'foreign' => Arr::get($config, 'foreign', $meta->model().'.id'),
				'column' => 'i18n_parent_id'
				)),
		));
	}

}
