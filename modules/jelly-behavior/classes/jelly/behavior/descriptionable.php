<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds description field to model
 *
 * @package  Jelly-Behavior
 */
abstract class Jelly_Behavior_Descriptionable
{
	
	public static function initialize(Jelly_Meta $meta, $config = array())
	{
		$meta->fields(array(
			'description' => new Field_Text(array_merge(array(), $config)),
		));
	}

}
