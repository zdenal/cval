<?php defined('SYSPATH') or die('No direct script access.');
return array
(
	'filenoexp'    => array
	(
		'driver'             => 'filenoexp',
		'cache_dir'          => APPPATH.'cache/.kohana_cache_noexp',
		'default_expire'     => 0,
	)
);