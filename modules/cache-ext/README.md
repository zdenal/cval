EXTENDED Kohana Cache library
====================

Requirements
-------------------------
Kohana Cache library module installed - git://github.com/kohana/cache.git


Supported cache solutions
-------------------------

Currently this module supports the following cache methods.

Same as Kohana Cache library plus

1. File with no expiration
