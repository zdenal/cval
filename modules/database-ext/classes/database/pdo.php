<?php defined('SYSPATH') or die('No direct script access.');

class Database_PDO extends Kohana_Database_PDO {

	/**
	 * Should be overriden in database driver
	 */
	protected function _transaction_start()
	{
		$this->_connection->beginTransaction();
	}

	/**
	 * Should be overriden in database driver
	 */
	protected function _transaction_commit()
	{
		$this->_connection->commit();
	}

	/**
	 * Should be overriden in database driver
	 */
	protected function _transaction_rollback() 
	{
		$this->_connection->rollBack();
	}
	
}
