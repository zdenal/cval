<?php defined('SYSPATH') or die('No direct script access.');

abstract class Database_Query_Builder_Where extends Kohana_Database_Query_Builder_Where 
{
	/**
	 * Number of where clauses remove from end of _where array.
	 * 
	 * @param	int		Number of where clauses to remove
	 * @return	$this 
	 */
	public function where_remove($count = 1)
	{
		while ($count > 0)
		{
			array_pop($this->_where);
			$count--;
		}
		
		return $this;
	}
	
}
