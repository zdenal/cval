<?php defined('SYSPATH') or die('No direct script access.');

abstract class Database_Query_Builder extends Kohana_Database_Query_Builder {
	

	/**
	 * Compiles an array of conditions into an SQL partial. Used for WHERE
	 * and HAVING.
	 *
	 * @param   object  Database instance
	 * @param   array   condition statements
	 * @return  string
	 */
	protected function _compile_conditions(Database $db, array $conditions)
	{
		$last_condition = NULL;

		$sql = '';
		foreach ($conditions as $group)
		{
			// Process groups of conditions
			foreach ($group as $logic => $condition)
			{
				if ($condition === '(')
				{
					if (!empty($sql) AND $last_condition !== '(')
					{
						// Include logic operator
						$sql .= ' '.$logic.' ';
					}

					$sql .= '(';
				}
				elseif ($condition === ')')
				{
					$sql .= ')';
				}
				else
				{
					if (!empty($sql) AND $last_condition !== '(')
					{
						// Add the logic operator
						$sql .= ' '.$logic.' ';
					}

					// Split the condition
					list($column, $op, $value) = $condition;

					if ($value === NULL)
					{
						if ($op === '=')
						{
							// Convert "val = NULL" to "val IS NULL"
							$op = 'IS';
						}
						elseif ($op === '!=')
						{
							// Convert "val != NULL" to "valu IS NOT NULL"
							$op = 'IS NOT';
						}
					}

					// Database operators are always uppercase
					$op = strtoupper($op);

					if ($op === 'BETWEEN' AND is_array($value))
					{
						// BETWEEN always has exactly two arguments
						list($min, $max) = $value;

						if (is_string($min) AND array_key_exists($min, $this->_parameters))
						{
							// Set the parameter as the minimum
							$min = $this->_parameters[$min];
						}

						if (is_string($max) AND array_key_exists($max, $this->_parameters))
						{
							// Set the parameter as the maximum
							$max = $this->_parameters[$max];
						}

						// Quote the min and max value
						$value = $db->quote($min).' AND '.$db->quote($max);
					}
					else
					{
						if (is_string($value) AND array_key_exists($value, $this->_parameters))
						{
							// Set the parameter as the value
							$value = $this->_parameters[$value];
						}

						//MODIFICATION
						//DO not quote if is string already quoted in front and back by `
						if (!is_string($value)
								OR !$this->starts_with($value, '`')
								OR !$this->ends_with($value, '`'))
						{
							//// Quote the entire value normally
							$value = $db->quote($value);
						}
						//MODIFICATION END
					}

					// Append the statement to the query
					$sql .= $db->quote_identifier($column).' '.$op.' '.$value;
				}

				$last_condition = $condition;
			}
		}

		return $sql;
	}

	public function starts_with($haystack, $needle)
	{
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	public function ends_with($haystack, $needle)
	{
		$length = strlen($needle);
		$start = $length * -1; //negative
		return (substr($haystack, $start) === $needle);
	}

}
