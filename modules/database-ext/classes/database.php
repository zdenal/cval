<?php defined('SYSPATH') or die('No direct script access.');

abstract class Database extends Kohana_Database {

	// SET if we use transactions
	protected $_transaction = FALSE;

	// transaction counter
	protected $_transaction_count = 0;

	protected function __construct($name, array $config)
	{
		parent::__construct($name, $config);

		if (isset($this->_config['transaction']))
		{
			// Allow the transaction to be overloaded per-connection
			$this->_transaction = (bool) $this->_config['transaction'];
		}
	}

	/**
	 * Starts new transaction. If another is already started,
	 * only increase the counter.
	 */
	public function transaction_start()
	{
		// Proccess only when configured for transactions
		if ( ! $this->_transaction)
			return;

		// Make sure the database is connected
		$this->_connection or $this->connect();

		if ($this->_transaction_count === 0)
		{
			$this->_transaction_start();
		}
		$this->_transaction_count++;
	}

	/**
	 * Commit last transaction. If no transaction started nothing to do.
	 * If one transaction only, do the commit.
	 * And then decrease the counter.
	 */
	public function transaction_commit()
	{
		// Proccess only when configured for transactions
		if ( ! $this->_transaction)
			return;

		// Make sure the database is connected
		$this->_connection or $this->connect();

		if ($this->_transaction_count === 0)
		{
			return;
		}
		if ($this->_transaction_count === 1)
		{
			$this->_transaction_commit();
		}
		$this->_transaction_count--;
	}

	/**
	 * Roolback all started transactions. If no transaction started nothing to do.
	 * Otherwise call rollback.
	 * And then clears the counter.
	 */
	public function transaction_rollback()
	{
		// Proccess only when configured for transactions
		if ( ! $this->_transaction)
			return;

		// Make sure the database is connected
		$this->_connection or $this->connect();

		if ($this->_transaction_count === 0)
		{
			return;
		}
		$this->_transaction_rollback();

		$this->_transaction_count = 0;
	}

	/**
	 * Should be overriden in database driver
	 */
	protected function _transaction_start() {}

	/**
	 * Should be overriden in database driver
	 */
	protected function _transaction_commit() {}

	/**
	 * Should be overriden in database driver
	 */
	protected function _transaction_rollback() {}

}
