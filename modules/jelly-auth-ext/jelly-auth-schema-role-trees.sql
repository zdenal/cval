
SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(127) NOT NULL,
	`username` VARCHAR(32) NOT NULL,
	`password` char(50) NOT NULL,
	`logins` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`last_login` DATETIME,
	`label` VARCHAR(255),
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`created_by` INT(11) UNSIGNED,
  	`updated_by` INT(11) UNSIGNED,
  	PRIMARY KEY  (`id`),
  	UNIQUE KEY `uniq_username` (`username`),
  	UNIQUE KEY `uniq_email` (`email`),
  	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_users_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_users_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL,
	`user_agent` VARCHAR(40) NOT NULL,
	`token` VARCHAR(32) NOT NULL,
	`created` DATETIME NOT NULL,
	`expires` DATETIME NOT NULL,
	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_token` (`token`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_user_tokens_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `role_trees`;

CREATE TABLE `role_trees` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`label` VARCHAR(255),
	`description` TEXT,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`created_by` INT(11) UNSIGNED,
	`updated_by` INT(11) UNSIGNED,
	PRIMARY KEY (`id`),
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_role_trees_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_role_trees_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (	
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(32) NOT NULL,
	`description` TEXT,
	`parent_role_id` INT(11) UNSIGNED NOT NULL,
	`role_tree_id` INT(11) UNSIGNED NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`created_by` INT(11) UNSIGNED,
  	`updated_by` INT(11) UNSIGNED,
	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_name` (`name`),
	INDEX `idx_role_tree_id` (`role_tree_id`),
	CONSTRAINT `fk_roles_role_tree_id_role_trees`
		FOREIGN KEY (`role_tree_id`)
		REFERENCES `role_trees` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_parent_role_id` (`parent_role_id`),
	CONSTRAINT `fk_roles_parent_role_id_roles`
		FOREIGN KEY (`parent_role_id`)
		REFERENCES `roles` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_roles_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_roles_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
	`user_id` INT(11) UNSIGNED NOT NULL,
	`role_id` INT(11) UNSIGNED NOT NULL,
	`created_at` DATETIME NOT NULL,
	`created_by` INT(11) UNSIGNED,
	PRIMARY KEY  (`user_id`,`role_id`),
	CONSTRAINT `fk_roles_users_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_role_id` (`role_id`),
	CONSTRAINT `fk_roles_users_role_id_roles`
		FOREIGN KEY (`role_id`)
		REFERENCES `roles` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_roles_users_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `email`, `username`, `password`, `label`, `created_at`, `updated_at`) VALUES
(1, 'test@test.com', 'admin', '3662d2255270ad6f0ec9b6f999f8e71c0eb83d2b3fa4b03271', 'Admin', '2010-01-01 00:00:00', '2010-01-01 00:00:00');

INSERT INTO `role_trees` (`id`, `label`, `description`, `created_at`, `updated_at`) VALUES (1, 'default', 'Default application role tree.', '2010-01-01 00:00:00', '2010-01-01 00:00:00');

INSERT INTO `roles` (`id`, `name`, `description`, `parent_role_id`, `role_tree_id`, `created_at`, `updated_at`) VALUES (1, 'admin', 'Administrative user, has access to everything.', NULL, 1, '2010-01-01 00:00:00', '2010-01-01 00:00:00');
INSERT INTO `roles` (`id`, `name`, `description`, `parent_role_id`, `role_tree_id`, `created_at`, `updated_at`) VALUES (2, 'login', 'Login privileges, granted after account confirmation', 1, 1, '2010-01-01 00:00:00', '2010-01-01 00:00:00');

INSERT INTO `roles_users` (`user_id`, `role_id`, `created_at`) VALUES (1, 1, '2010-01-01 00:00:00');

SET FOREIGN_KEY_CHECKS=1;
COMMIT;
