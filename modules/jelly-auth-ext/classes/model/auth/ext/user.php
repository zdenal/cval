<?php defined('SYSPATH') or die ('No direct script access.');
/**
 * Jelly Auth User Model
 * @package Jelly Auth Ext
 * @author	Zdennal
 */
class Model_Auth_Ext_User extends Model_Auth_User implements Acl_Role_Interface
{
	public static function initialize(Jelly_Meta $meta)
    {
		$meta->name_key('username')
			->sorting(array('username' => 'ASC'))
			->fields(array(
			'id' => new Field_Primary));

		Behavior_Labelable::initialize($meta, array(
			'label' => 'name'
		));

		$meta->fields(array(
			'username' => new Field_String(array(
				'unique' => TRUE,
				'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(32),
						'min_length' => array(3),
						'regex' => array('/^[\pL\pN_.-]+$/ui')
					)
				)),
			'password' => new Field_Password(array(
				'hash_with' => array(Auth::instance(), 'hash_password'),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(50),
					'min_length' => array(5)
				)
			)),
			'password_confirm' => new Field_Password(array(
				'in_db' => FALSE,
				'callbacks' => array(
					'matches' => array('Model_Auth_User', '_check_password_matches')
				),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(50),
					'min_length' => array(5)
				)
			)),
			'email' => new Field_Email,
			'logins' => new Field_Integer(array(
				'default' => 0
			)),
			'last_login' => new Field_Datetime,
			'tokens' => new Field_HasMany(array(
				'foreign' => 'user_token'
			)),
			'roles' => new Field_ManyToMany
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);

		// Set action fields
		$meta->action_fields(array(
			'admin/list' => array('username' => 'action', 'label' => 'action', 'email', 'roles'),
			'admin/profile' => array('label', 'username', 'email'),
			'admin/profile_edit' => array('label', 'username', 'password', 'password_confirm', 'email')
		))->action_fields_remove(array(
			'admin/view' => array('id', 'password', 'password_confirm', 'tokens'),
			'admin/new' => array('id', 'tokens', 'created_at', 'updated_at', 'created_by', 'updated_by', 'last_login', 'logins'),
			'admin/edit' => array('id', 'tokens', 'created_at', 'updated_at', 'created_by', 'updated_by', 'last_login', 'logins')
		));
    }

	/**
	 * Returns the string identifier of the Role
	 *
	 * @return	array of roles strings
	 */
	public function get_role_id()
	{
		$roles = array();

		foreach ($this->roles as $role)
		{
			$roles[] = $role->get_role_id();
		}

		return $roles;
	}

	/**
	 * Returns the value of the model's name key
	 *
	 * @return  mixed
	 */
	public function name()
	{
		if ($this->label)
		{
			return $this->label;
		}

		return parent::name();
	}
	
} // End Model_Auth_Ext_User