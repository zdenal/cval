<?php defined('SYSPATH') or die ('No direct script access.');
/**
 * Jelly Auth Role Model
 * @package Jelly Auth Ext
 * @author 	Zdennal
 */
class Model_Auth_Ext_Role extends Model_Auth_Role implements Acl_Role_Interface
{
	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('name')
			->fields(array(
			'id' => new Field_Primary,
		));

		Behavior_Labelable::initialize($meta);

		$meta->fields(array(
			'name' => new Field_String(array(
				'unique' => TRUE,
				'rules' => array(
					'max_length' => array(32),
					'not_empty' => NULL
				)
			)),
			'parent_role' => new Field_BelongsTo(array(
				'foreign' => 'role.id',
				'column' => 'parent_role_id'
			)),
			'users' => new Field_ManyToMany
		));

		Behavior_Descriptionable::initialize($meta);
		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);

		// Set action fields
		$meta->action_fields(array(
			'admin/list' => array('name' => 'action', 'description')
		))->action_fields_remove(array(
			'admin/new' => array('id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'users', 'parent_role'),
			'admin/edit' => array('id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'users', 'parent_role'),
			'admin/view' => array('parent_role'),
		))->action_fields_modify(array(
			'admin/view' => array('users' => array('grid' => 16))
		));
	}

	/**
	 * Returns the string identifier of the Role
	 *
	 * @return	string
	 */
	public function get_role_id()
	{
		return $this->name;
	}

} // End Model_Auth_Ext_Role