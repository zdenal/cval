<?php echo Form::open() ?>
<?php if ( ! empty($errors)): ?>
<ul class="errors">
<?php foreach ($errors as $message): ?>
    <li><?php echo $message ?></li>
<?php endforeach ?>
<?php endif ?>
</ul> 
<dl>
    <dt><?php echo Form::label('username', 'Username') ?></dt>
    <dd><?php echo Form::input('username', @$post['username']) ?></dd>
 
    <dt><?php echo Form::label('password', 'Password') ?></dt>
    <dd><?php echo Form::password('password') ?></dd>
</dl>
 
<?php echo Form::submit(NULL, 'Login') ?>
<?php echo Form::close() ?>