<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
<head>
	<title><?php echo $template_title ?></title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
	<?php foreach ($template_styles as $file => $type) echo HTML::style($file, array('media' => $type)), "\n" ?>
	<?php foreach ($template_scripts as $file) echo HTML::script($file), "\n" ?>
	
</head>
<body>
<?php echo $content ?>
</body>
</html>