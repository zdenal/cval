<?php echo Form::open() ?>
<?php if ( ! empty($errors)): ?>
<ul class="errors">
<?php foreach ($errors as $message): ?>
    <li><?php echo $message ?></li>
<?php endforeach ?>
<?php endif ?>
</ul> 
<dl>
    <dt><?php echo Form::label('username', 'Username') ?></dt>
    <dd><?php echo Form::input('username', @$post['username']) ?></dd>
    
    <dt><?php echo Form::label('email', 'Email') ?></dt>
    <dd><?php echo Form::input('email', @$post['email']) ?></dd>
 
    <dt><?php echo Form::label('password', 'Password') ?></dt>
    <dd><?php echo Form::password('password') ?></dd>
    
    <dt><?php echo Form::label('password_confirm', 'Password confirm') ?></dt>
    <dd><?php echo Form::password('password_confirm') ?></dd>
</dl>
 
<?php echo Form::submit(NULL, 'Register') ?>
<?php echo Form::close() ?>