
SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;

DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users` (
	`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` varchar(32) NOT NULL DEFAULT '',
	`password` char(50) NOT NULL,
	`email` varchar(127) NULL,
	`logins` int(10) UNSIGNED NOT NULL DEFAULT '0',
	`last_login` DATETIME,
	`label` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` INT(11) UNSIGNED,
  	`updated_by` INT(11) UNSIGNED,
	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_username` (`username`),
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_users_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_users_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE `user_tokens` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL,
	`user_agent` VARCHAR(40) NOT NULL,
	`token` VARCHAR(32) NOT NULL,
	`created` DATETIME NOT NULL,
	`expires` DATETIME NOT NULL,
	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_token` (`token`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_user_tokens_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles`;

CREATE TABLE IF NOT EXISTS `roles` (
	`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` varchar(32) NOT NULL,
	`description` varchar(255) NOT NULL,
	`parent_role_id` INT(11) UNSIGNED,
	`label` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` INT(11) UNSIGNED,
  	`updated_by` INT(11) UNSIGNED,
	PRIMARY KEY  (`id`),
	UNIQUE KEY `uniq_name` (`name`),
	INDEX `idx_parent_role_id` (`parent_role_id`),
	CONSTRAINT `fk_roles_parent_role_id_roles`
		FOREIGN KEY (`parent_role_id`)
		REFERENCES `roles` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_roles_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_roles_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE IF NOT EXISTS `roles_users` (
	`user_id` int(11) UNSIGNED NOT NULL,
	`role_id` int(11) UNSIGNED NOT NULL,
	PRIMARY KEY  (`user_id`,`role_id`),
	CONSTRAINT `fk_roles_users_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_role_id` (`role_id`),
	CONSTRAINT `fk_roles_users_role_id_roles`
		FOREIGN KEY (`role_id`)
		REFERENCES `roles` (`id`)
		ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `email`, `label`) VALUES
(1, 'admin', '06bdf69d8008e8a1ab3efc23ffa368a625ccdb63bafa9ae705', 'test@test.com', 'Admin');

INSERT INTO `roles` (`id`, `name`, `description`, `label`) VALUES (1, 'login', 'Login privileges, granted after account confirmation', 'Login');
INSERT INTO `roles` (`id`, `name`, `description`, `label`) VALUES (2, 'admin', 'Administrative user, has access to everything.', 'Admin');

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES (1, 1);
INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES (1, 2);

SET FOREIGN_KEY_CHECKS=1;
COMMIT;