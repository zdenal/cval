<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Authentication Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Auth extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	/**
	 * @var array	Array of actions which need not to be called as AJAX or internal request
	 */
	public $non_ajax_actions = array(
		'resend_registration_email',
		'verify'
	);

	public function before()
	{
		parent::before();
        
		// Set A2 instance
		$this->a2 = A2::instance();
	}
	
	/**
	 * Perform login
	 */
	public function action_login() 
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Controller_Auth::action_login');
		
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/auth/login', TRUE);

		// If user is already logged in
		if (Cval::logged_in())
		{
			Kohana::$log->add('ACCESS', "Attempt to login made by logged-in user");
			
			$user = $this->cval->user();
			
			$this->request->response = array(
				'redirect' => Route::url_protocol('home'),
				'object' => array(
					'id' => $user->id(),
					'model' => $user->meta()->model()
				)
			);
			
			return;
		}

		$post = Security::xss_clean(Arr::extract($_POST, array('email','password','remember')));
		
		$validate = Validate::factory($post)
			->filter(TRUE, 'trim')
			->label('email', 'email address')
			->rule('email', 'not_empty')
			->rule('password', 'not_empty');

		// Check if fields are filled with exception
		$validate->check(FALSE, TRUE);
		
		// If so, check if user with username exists
		$user = Jelly::select('user')
				->where('email', '=', Arr::get($post, 'email'))
				->load();
		
		if ( ! $user->loaded())
		{
			$user = Jelly::select('user')
				->where('username', '=', Arr::get($post, 'email'))
				->load();
			
			if ( ! $user->loaded())
			{
				$validate->error('email', 'not_found', array(Arr::get($post, 'email')));
				// throw exception
				throw new Validate_Exception($validate);
			}
			else
			{
				$post['email'] = $user->username;
			}
		}
		
		Database::instance()->transaction_start();
		
		if ($this->a2->a1->login($post['email'], $post['password'],
			! empty($post['remember'])))
		{
			try
			{
				Kohana::$log->add('ACCESS', 'Successful login made with username `:username`', array(
					':username' => $post['email']
				));

				$user = $this->a2->get_user();

				if ($user->is_verified(TRUE))
				{
					// Make sure that verified user has profile
					$user->create_profile();
				}

				Cval_Message::set_flash(Cval_Message::SUCCESS, Cval::i18n('You were logged in!'));

				$this->request->response = array(
					'redirect' => Route::url_protocol('home'),
					'object' => array(
						'id' => $user->id(),
						'model' => $user->meta()->model()
					)
				);

				Database::instance()->transaction_commit();
			}
			catch (Exception $e)
			{
				$this->a2->a1->logout();
				
				throw $e;
			}
			
			return;
		}
		else
		{
			Kohana::$log->add('ACCESS', 'Unsuccessful login attempt made with username `:username`', array(
				':username' => $post['email']
			));
			
			if ($user->is_blocked())
			{
				$validate->error('email', 'blocked');
			}
			else
			{
				$validate->error('password', 'incorrect');
			}
		}

		throw new Validate_Exception($validate);
	}

	/**
	 * Perform user logout
	 */
	public function action_logout() 
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Controller_Auth::action_logout');
		
		// Get logged user
		$user = $this->a2->a1->get_user();
		// Logout
		$this->a2->a1->logout();

		if ($user AND $user instanceof Model_User)
		{
			Kohana::$log->add('ACCESS', 'Successful logout made by user with username `:username`', array(
				':username' => $user->username
			));
		}
		
		Cval_Message::set_flash(Cval_Message::SUCCESS, Cval::i18n('You were logged out! Thank you for using :cval.', array(
			':cval' => Cval::instance()->location()->name()
		)));

		$this->request->response = array(
			'redirect' => Route::url_protocol('home'),
		);
	}
	
	/**
	 * Signup user
	 */
	public function action_signup() 
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Controller_Auth::action_signup');
		
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));

		Security::check(Arr::get($_POST, 'csrf'), 'cval/auth/signup', TRUE);
		
		// If user is already logged in
		if (Cval::logged_in())
		{
			Kohana::$log->add('ACCESS', "Attempt to signup made by logged-in user");
			
			$user = $this->cval->user();
			
			$this->request->response = array(
				'redirect' => Route::url_protocol('home'),
				'object' => array(
					'id' => $user->id(),
					'model' => $user->meta()->model()
				)
			);
			
			return;
		}

		$post = Security::xss_clean(Arr::extract($_POST, array(
			'label',
			'email',
			'password',
			'password_confirm'
		)));
		
		$user = Jelly::factory('user')
			->set($post);
		
		// Copy username from email
		$user->username = Arr::get($post, 'email');
		// Set email to required and username not
		$user->meta()->fields('email')->rules['not_empty'] = NULL;
		Arr::remove($user->meta()->fields('username')->rules, array('not_empty'));
		array_pop($user->meta()->fields('username')->callbacks);
		
		Database::instance()->transaction_start();
		
		// Try to save user
		$user->save();
		
		// Create verification id
		$user->verification_id = md5(uniqid());
		
		$needed_roles = array('cval');
		// Get user roles
		$roles = Jelly::select('role')
				->where('name', 'IN', $needed_roles)
				->execute();
		
		if ($roles->count() < count($needed_roles))
		{
			throw new Kohana_Exception('One of roles with names `:names` not found, user not created.', array(
				':names' => implode(', ', $needed_roles)
			));
		}
		
		// Add user roles to user
		$user->add('roles', $roles);
		$user->save(NULL, TRUE);
		
		Kohana::$log->add('ACCESS', 'New user created with username `:username`', array(
			':username' => $user->username
		));
		
		if ( ! Cval_Registration::send_registration_email($user))
		{
			throw new Kohana_Exception('Registration email for new user with username `:username` to email address `:email` not send', array(
				':username' => $user->username,
				':email' => $user->email
			));
		}
		
		// Log user in
		$this->a2->a1->force_login($user);
		
		Cval_Message::set_flash(Cval_Message::SUCCESS, Cval::i18n('Thank you for your registration!'));
		
		$this->request->response = array(
			'redirect' => Route::url_protocol('home'),
			'object' => array(
				'id' => $user->id(),
				'model' => $user->meta()->model()
			)
		);
		
		Database::instance()->transaction_commit();
	}
	
	/**
	 * Reset your password
	 */
	public function action_password_reset() 
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Controller_Auth::action_password_reset');
		
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/auth/password/reset', TRUE);

		// If user is already logged in
		if (Cval::logged_in())
		{
			Kohana::$log->add('ACCESS', "Attempt to reset password made by logged-in user");
			
			$user = $this->cval->user();
			
			$this->request->response = array(
				'redirect' => Route::url_protocol('home'),
				'object' => array(
					'id' => $user->id(),
					'model' => $user->meta()->model()
				)
			);
			
			return;
		}

		$post = Security::xss_clean(Arr::extract($_POST, array(
			'email',
		)));
		
		$validate = Validate::factory($post)
			->filter(TRUE, 'trim')
			->label('email', 'email address')
			->rule('email', 'not_empty');

		// Check if fields are filled with exception
		$validate->check(FALSE, TRUE);
		
		// If so, check if user with username exists
		$user = Jelly::select('user')
				->where('email', '=', Arr::get($post, 'email'))
				->load();
		
		if ( ! $user->enabled())
		{
			$user = Jelly::select('user')
				->where('username', '=', Arr::get($post, 'email'))
				->load();
			
			if ( ! $user->enabled())
			{
				$validate->error('email', 'not_found', array(Arr::get($post, 'email')));
				// throw exception
				throw new Validate_Exception($validate);
			}
			else
			{
				$post['email'] = $user->username;
			}
		}
		
		$password = Text::random('alnum', 6);
		$user->password = $password;

		Database::instance()->transaction_start();

		$user->save();

		if ( ! Cval_Registration::send_reset_password_email($user, $password))
		{
			throw new Kohana_Exception('Email with new password for username `:username` to email address `:email` not send', array(
				':username' => $user->username,
				':email' => $user->email
			));
		}

		Kohana::$log->add('ACCESS', 'New password send for username `:username` to email address `:email`', array(
			':username' => $user->username,
			':email' => $user->email
		));
				
		$this->request->response = array(
			'object' => array(
				'id' => $user->id(),
				'model' => $user->meta()->model()
			),
			'html' => View::factory('cval/content/password/reset/success', array(
				'user' => $user
			))->render(),
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('New password was sent!'))
		);
		
		Database::instance()->transaction_commit();
	}
	
	/**
	 * Resend the registration email
	 */
	public function action_resend_registration_email() 
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Controller_Auth::action_registration_resend');
		
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/auth/verification/resend', TRUE);

		// If user is not logged in
		if ( ! Cval::logged_in())
		{
			Kohana::$log->add('ACCESS', "Attempt to resend registration email made by not logged-in user");
			
			$this->request->response = array(
				'redirect' => Route::url_protocol('login'),
				'message' => Cval_Message::create(Cval_Message::ERROR, Cval::i18n('You must be logged in for resending the registration email!'))
			);
			
			return;
		}
		
		$user = $this->cval->user();

		if ( ! Cval_Registration::send_registration_email($user))
		{
			throw new Kohana_Exception('Registration email for new user with username `:username` to email address `:email` not send', array(
				':username' => $user->username,
				':email' => $user->email
			));
		}
		
		$response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Registration email was resent to :email.', array(
				':email' => $user->email
			)))
		);
		
		// Do not redirect when ajax
		if ( ! Request::$is_ajax)
		{
			$response['redirect'] = Route::url_protocol('home');
		}
		
		$this->request->response = $response;
	}
	
	public function action_verify()
	{
		Kohana::$log->add(Kohana::DEBUG, 'Executing Controller_Auth::action_verify');
		
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		$user = Jelly::select('user')
				->where('id', '=', $this->request->param('id'))
				->where('verification_id', '=', $this->request->param('verification_id'))
				->load();
		
		if ( ! $user->enabled())
		{
			throw new Kohana_Exception('Wrong verification parameters.');
		}
		
		// Check if user is not already verified using user roles
		$is_verified = $user->is_verified(TRUE);
		
		Database::instance()->transaction_start();
		
		// Verify user
		$user->verify();
		
		$csrf = Security::token(TRUE, 'cval/auth/verification/verified');
		
		if ($is_verified)
		{
			$this->request->response = array(
				'redirect' => Route::url_protocol('verified', array(
					'csrf' => $csrf
				)),
				'message' => Cval_Message::create(Cval_Message::INFO, Cval::i18n('Your email address :email was already verified.', array(
					':email' => $user->email
				)))
			);
			
			return;
		}
		
		$this->request->response = array(
			'redirect' => Route::url_protocol('verified', array(
				'csrf' => $csrf
			)),
			'message' => Cval_Message::create(Cval_Message::INFO, Cval::i18n('Congratulation! Your email address :email is verified.', array(
				':email' => $user->email
			)))
		);
		
		Database::instance()->transaction_commit();
	}

}

