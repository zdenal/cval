<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Cval Auth Driver
 *
 * @package    Cval
 * @author     Zdennal
 */
class Auth_Cval extends Auth_Jelly_Ext 
{

	/**
	 * Creates a hashed password from a plaintext password, inserting salt
	 * based on the configured salt pattern.
	 *
	 * @param   string  plaintext password
	 * @return  string  hashed password string
	 */
	public static function hash_password_static($password, $salt = FALSE)
	{
		return Auth::instance()->hash_password($password, $salt);
	}
	
	/**
	 * Logs a user in.
	 *
	 * @param   string   username
	 * @param   string   password
	 * @param   boolean  enable auto-login
	 * @return  boolean
	 */
	public function _login($user, $password, $remember)
	{
		// Make sure we have a user object
		$user = $this->_get_object($user);

		// If the passwords match, perform a login
		// Do not perform user role check
		if ($user->password === $password)
		{
			if ($remember === TRUE)
			{
				// Create a new autologin token
				$token = Model::factory('user_token');

				// Set token data
				$token->user = $user->id();
				$token->expires = time() + $this->_config['lifetime'];

				$token->create();

				// Set the autologin Cookie
				Cookie::set('authautologin', $token->token, $this->_config['lifetime']);
			}

			// Finish the login
			$this->complete_login($user);

			return TRUE;
		}

		// Login failed
		return FALSE;
	}
	
	/**
	 * Gets the currently logged in user from the session.
	 * Returns FALSE if no user is currently logged in.
	 *
	 * @return  mixed
	 */
	public function get_user()
	{
		// Get user identifier from session
		$user = parent::get_user();

		if ($user === FALSE)
		{
			return FALSE;
		}

		// Convert it to object
		$user = $this->_get_object($user);

		if ($user instanceof Model_User AND $user->enabled())
		{
			return $user;
		}

		return FALSE;
	}
	
	/**
	 * Logs a user in, based on the authautologin Cookie.
	 *
	 * @return  boolean
	 */
	public function auto_login()
	{
		if ($token = Cookie::get('authautologin'))
		{
			// Load the token and user
			$token = Jelly::select('user_token')->where('token', '=', $token)->load();

			if ($token->loaded() AND $token->user->enabled())
			{
				if ($token->user_agent === sha1(Request::$user_agent))
				{
					// Save the token to create a new unique token
					$token->update();

					// Set the new token
					Cookie::set('authautologin', $token->token, $token->expires - time());

					// Complete the login with the found data
					$this->complete_login($token->user);

					// Automatic login was successful
					return TRUE;
				}

				// Token is invalid
				$token->delete();
			}
		}

		return FALSE;
	}
	
	/**
	 * Convert a unique identifier string to a user object
	 *
	 * @param mixed $user
	 * @return Model_User
	 */
	protected function _get_object($user)
	{
		static $current;

		//make sure the user is loaded only once.
		if ( ! is_object($current))
		{
			// Load the user
			if (is_numeric($user))
			{
				// From id
				$current = Jelly::select('user')->where('id', '=', $user)->limit(1)->execute();
			}
			elseif (is_string($user))
			{
				// From username
				$current = Jelly::select('user')->where('username', '=', $user)->limit(1)->execute();
			}
		}

		if ($user instanceof Model_User AND $user->enabled())
		{
			$current = $user;
		}
		
		// User is not active
		if ( ! $current->enabled())
		{
			// So use empty object
			$current = Jelly::factory('user');
		}

		return $current;
	}

} // End Auth_Cval