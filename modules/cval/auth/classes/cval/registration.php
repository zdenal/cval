<?php defined('SYSPATH') OR die('No direct script access.');

class Cval_Registration
{
	public static function send_registration_email($user)
	{
		if ( ! $user instanceof Model_User)
		{
			$user = Jelly::select('user')
				->where('email', '=', trim($user))
				->load();
			
			if ( ! $user->loaded())
			{
				throw new Kohana_Exception('Registration email not send. User with email `:email` not found.', array(
					':email' => $user
				));
			}
		}
		
		$params = array(
			'user' => $user,
			'verification_url' => Route::url_protocol('verify', array(
				'id' => $user->id(),
				'verification_id' => $user->verification_id
			))
		);

		return Email::send_template(array($user->email, $user->name()), 'auth/verify', $params);
	}
	
	public static function send_manual_registration_email(Model_User $user, $password)
	{	
		$params = array(
			'user' => $user,
			'password' => $password,
			'login_url' => Route::url_protocol('login')
		);

		return Email::send_template(array($user->email, $user->name()), 'auth/account/created', $params);
	}
	
	public static function send_reset_password_email(Model_User $user, $password)
	{
		$params = array(
			'user' => $user,
			'password' => $password
		);

		return Email::send_template(array($user->email, $user->name()), 'auth/password/reset', $params);
	}
	
	/**
	 * Checks if registration is enabled
	 * 
	 * @return	bool 
	 */
	public static function is_enabled()
	{
		return Cval_Config::get('registration_type') > 10;
	}
}
