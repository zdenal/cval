<?php defined('SYSPATH') or die('No direct script access.');

class ACL extends ACL_Core 
{
	
	public function is_allowed($role = NULL, $resource = NULL, $privilege = NULL)
	{
		// save command data (in case of assertion, then the original objects are used)
		$this->command = array
		(
			'role'      => $role,
			'resource'  => $resource,
			'privilege' => $privilege
		);

		// normalize role
		$roles = $role !== NULL 
			? ($role instanceof Acl_Role_Interface ? $role->get_role_id() 
			: (is_array($role) ? $role : (string) $role)) : NULL;

		// make array (we support checking multiple roles at once, the first matching rule for any of the roles will be returned)
		if( ! is_array($roles))
		{
			$roles = array($roles);
		}
		
		// CVAL-MODIFICATION
		// User needs to have role 'user' assigned, otherwise means is not verified
		if ( ! in_array('user', $roles))
		{
			return FALSE;
		}

		// normalize resource to a string value (or NULL)
		$resource = $resource !== NULL
			? ($resource instanceof Acl_Resource_Interface ? $resource->get_resource_id() : (string) $resource)
			: NULL;

		// resource unknown
		if( $resource !== NULL && !$this->has_resource($resource))
		{
			return FALSE;
		}

		// loop for matching rule
		do
		{
			if ( $rule = $this->_find_match_role($resource,$roles,$privilege))
			{
				return $rule['allow'];
			}
		}
		// go level up in resources tree (child resources inherit rules from parent)
		while ( $resource !== NULL AND ($resource = $this->_resources[$resource]['parent']));

		return FALSE;
	}
	
}