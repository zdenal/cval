<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model Auth Cval User
 * @package Cval
 * @author	zdennal
 */
abstract class Model_Auth_Cval_User extends Model_Auth_Ext_User
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('username')
				->fields(array(
					'id' => new Field_Primary));

		Behavior_Labelable::initialize($meta, array(
			'label' => 'name',
			'rules' => array(
				'not_empty' => NULL,
				'max_length' => array(255)
			)
		));

		$meta->fields(array(
			'username' => new Field_String(array(
				'unique' => TRUE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(255),
					'min_length' => array(3),
				)
			)),
			'email' => new Field_Email(array(
				'label' => 'email address',
				'unique' => TRUE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(255),
					'min_length' => array(3),
				)
			)),
			'password' => new Field_Password(array(
				'hash_with' => array('Auth_Cval', 'hash_password_static'),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(50),
					'min_length' => array(5)
				)
			)),
			'password_confirm' => new Field_Password(array(
				'in_db' => FALSE,
				'callbacks' => array(
					'matches' => array('Model_User', '_check_password_matches')
				),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(50),
					'min_length' => array(5)
				)
			)),
			'logins' => new Field_Integer(array(
				'default' => 0
			)),
			'last_login' => new Field_Datetime,
			'verified' => new Field_Boolean,
			'verification_id' => new Field_String(array(
				'rules' => array(
					'max_length' => array(32),
				)
			)),
			'blocked' => new Field_Boolean,
			'block_message' => new Field_Text,
			'tokens' => new Field_HasMany(array(
				'foreign' => 'user_token'
			)),
			'roles' => new Field_ManyToMany,
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);

		// Set action fields
		$meta->action_fields(array(
			'admin/list' => array('username' => 'action', 'label' => 'action', 'email', 'roles'),
			'admin/profile' => array('label', 'username', 'email'),
			'admin/profile_edit' => array('label', 'username', 'password', 'password_confirm', 'email')
		))->action_fields_remove(array(
			'admin/view' => array('id', 'password', 'password_confirm', 'tokens'),
			'admin/new' => array('id', 'tokens', 'created_at', 'updated_at', 'created_by', 'updated_by', 'last_login', 'logins'),
			'admin/edit' => array('id', 'tokens', 'created_at', 'updated_at', 'created_by', 'updated_by', 'last_login', 'logins')
		));
	}
	
	protected function _before_save()
	{
		parent::_before_save();
		
		if ($this->changed('email'))
		{
			// Update username according email
			$this->username = $this->email;
		}
	}

	public function verify()
	{
		$this->verified = 1;
		
		$needed_roles = array('user');
		// Get user roles
		$roles = Jelly::select('role')
				->where('name', 'IN', $needed_roles)
				->execute();
		
		if ($roles->count() < count($needed_roles))
		{
			throw new Kohana_Exception('One of roles with names `:names` not found, user can\'t be verified.', array(
				':names' => implode(', ', $needed_roles)
			));
		}
		
		// Add user roles to user
		$this->add('roles', $roles);
		
		// Save verified user
		$this->save();

		return $this;
	}
	
	public function unverify()
	{
		$this->verified = 0;
		
		$needed_roles = array('user');
		// Get user roles
		$roles = Jelly::select('role')
				->where('name', 'IN', $needed_roles)
				->execute();
		
		// Remove user roles from user 
		$this->remove('roles', $roles);
		
		// Save unverified user
		$this->save();
		
		return $this;
	}
	
	public function is_verified($user_role_check = FALSE)
	{
		if ( ! $user_role_check)
		{
			return (bool) $this->verified;
		}
		return $this->has_role('user');
	}
	
	public function is_blocked()
	{
		return (bool) $this->blocked;
	}

}