<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('auth', 'auth(/<action>)', array('action' => 'login|logout|signup|password_reset|resend_registration_email'))
        ->defaults(array(
            'controller' => 'auth',
			'action' => 'login'
        ));

Route::set('verify', 'account-verify/<id>/<verification_id>')
        ->defaults(array(
            'controller' => 'auth',
			'action' => 'verify'
        ));
