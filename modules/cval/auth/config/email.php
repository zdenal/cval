<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(

	/**
	 * Email templates config.
	 * Each template is defined with key, which is used in Email class.
	 * Usage:
	 *
	 *		test' => array(
	 *			'from' => 'test',
	 *			'subject' => 'Test',
	 *			'view' => 'email/templates/test',
	 *			'html' => FALSE
	 *		),
	 *
	 * Definition of each variable:
	 *
	 *		from		string|array	sender email (and name) or named config
	 *		subject		string			message subject
	 *		view		string			view used for message body
	 *		html		boolean			send email as HTML
	 */
	'templates' => array(
		'auth/verify' => array(
			'subject' => Cval::instance()->location()->name().' - Verify your email address',
			'view' => 'email/templates/cval/auth/verify',
			'html' => FALSE
		),
		'auth/password/reset' => array(
			'subject' => Cval::instance()->location()->name().' - New password',
			'view' => 'email/templates/cval/auth/password/reset',
			'html' => FALSE
		)
	)

);