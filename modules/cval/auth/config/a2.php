<?php

return array(

	/**
	 * A2 uris
	 */
	'uris' => array(
		/**
		 * Not allowed uri, user is already logged in
		 */
		403 => 'denied',
		/**
		 * Login uri
		 */
		401 => 'login',
		/**
		 * Not verified uri
		 */
		470 => 'verification'
	),
	
	/*
	 * The ACL Roles (String IDs are fine, use of ACL_Role_Interface objects also possible)
	 * Use: ROLE => PARENT(S) (make sure parent is defined as role itself before you use it as a parent)
	 */
	'roles' => A2::get_roles(array(
		'user' => 'guest',
		'admin-access' => NULL,
		'admin' => NULL,
		'cval' => 'user',
	)),
	/*
	 * The ACL Resources (String IDs are fine, use of ACL_Resource_Interface objects also possible)
	 * Use: ROLE => PARENT (make sure parent is defined as resource itself before you use it as a parent)
	 */
	'resources' => array
		(
		// ADD YOUR OWN RESOURCES HERE
		'Controller' => NULL,
		'cval' => NULL,
		'admin' => NULL,
		'admin-model' => NULL,
		'model' => NULL,
		
		'location' => 'admin-model',
		'user' => 'admin-model',
		'role' => 'admin-model',
		
		'user_profile' => 'model',
		'user_profile_shared' => 'model',
		'calendar' => 'model',
		'contact' => 'model',
		'contact_group' => 'model',
		'schedule' => 'model',
		'schedule_group' => 'model',
	),
	/*
	 * The ACL Rules (Again, string IDs are fine, use of ACL_Role/Resource_Interface objects also possible)
	 * Split in allow rules and deny rules, one sub-array per rule:
	  array( ROLES, RESOURCES, PRIVILEGES, ASSERTION)
	 *
	 * Assertions are defined as follows :
	  array(CLASS_NAME,$argument) // (only assertion objects that support (at most) 1 argument are supported
	  //  if you need to give your assertion object several arguments, use an array)
	 */
	'rules' => array
		(
		'allow' => array
			(
			'user' => array(
				'role' => 'user',
				'resource' => array('Controller'),
			),
			'admin-access' => array(
				'role' => 'admin-access',
				'resource' => 'admin',
			),
			'admin' => array(
				'role' => 'admin'
			),
			'cval' => array(
				'role' => 'cval',
				'resource' => array('cval','model'),
			),
		),
		'deny' => array
		(
		)
	)

);