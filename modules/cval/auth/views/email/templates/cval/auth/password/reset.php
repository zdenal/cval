We have registered password reset for your <?php echo Cval::instance()->location()->name() ?> account.

This is your new password: <?php echo $password."\n" ?>
Please update it as soon as possible in your account settings.

<?php echo View::factory('cval/email/footer/text')->include_view($thiss) ?>