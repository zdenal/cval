<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'email' => array(
		'not_found' => 'The email address provided is incorrect',
		'not_verified' => 'Your account is not verified',
		'blocked' => 'Your account is blocked',
		'available' => 'That email address is already registered. Please choose another',
		'email'  => 'You must specify a valid email address',
		'unique' => 'That email is already registered. Please use a different email address',
	),
	'password' => array(
		'incorrect' => 'The password provided is incorrect',
	),
	'username' => array(
		'not_found' => 'The username provided is incorrect',
		'username_available' => 'That username is already registered. Please choose another',
	),
);

