<?php defined('SYSPATH') or die('No direct script access.');	

/*
 * LoadShared memory base class
 */
$path = Kohana::find_file('vendor', 'SharedMemory/SharedMemory');
if ($path)
{
	require_once $path;
}
