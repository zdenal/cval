// KEXT[on]
(function(Cval, $) {
	
	Cval_page_control_calendars_object = {
		staticParams : {
			defaults : {
				calendarAddSelector : '.Cval-calendar-add',
				timetableTriggerSelector : '.Cval-menu-item-calendars_timetable',
				listTriggerSelector : '.Cval-menu-item-calendars_list',
				
				activeSection : 'calendars_list',
				refreshOnShow : false,
				inHistory : false,
				loadUrl : {
					route : 'calendars_main'
				},
				menuSelector : '.Cval-menu-id-calendars'
			},
			show : function(params, callback, section) {
				if ( ! section) {
					Cval.page.show(this.defaults.activeSection);
					return;
				}

				// Set active section
				this.defaults.activeSection = section;

				this._super(params, callback);
			},
			_bindEvents : function() {
				this._super();
				var thiss = this;

				// Bind add calendar click
				this.getElement().find(this.defaults.calendarAddSelector)
					.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-calendar',
							secondary : 'ui-icon-plus'
						}
					})
					.click(function(){
						Cval.dialog.loadControl('calendar_add', function(){
							Cval.dialog.control.calendar_add.show()
						});
						return false;
					});
					
				// Bind trigger timetable
				this.getElement().find(this.defaults.timetableTriggerSelector)
					.Cval_button()
					.click(function(){
						Cval.page.show('calendars/timetable');
						return false;
					});
				// Bind trigger list
				this.getElement().find(this.defaults.listTriggerSelector)
					.Cval_button()
					.click(function(){
						Cval.page.show('calendars/list');
						return false;
					});
			},
			refreshCalendars : function(callback) {
				// Propagate refreshing to subpage control
				Cval.page.getControl(this.defaults.activeSection).refreshCalendars(callback);
			}
		},
		params : {}
	};
	
// KEXTSTART
	
	Cval.page.control.def.extend('Cval.page.control.calendars', 
		Cval_page_control_calendars_object.staticParams,
		Cval_page_control_calendars_object.params
	);

})(Cval, jQuery);