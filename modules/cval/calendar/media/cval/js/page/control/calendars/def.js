(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.calendars_def', {
		defaults : {
			/**
			 * Page holder
			 */
			parentElement : '.Cval-page-id-calendars-content',
			/**
			 * Page main element classes. It must be same for all instances,
			 * otherwise this.closeOtherPages will not work properly.
			 */
			pageClass : 'Cval-page-id-calendars-page',
			menuSelector : '.Cval-menu-id-calendars'
		},
		refreshCalendars : function(callback) {
			// Implement this, to refresh calendars list, timetable or etc.
			// Now just call the callback
			Cval.core.callback(callback);
		},
		_beforeDisplay : function(callback) {
			var thiss = this;

			Cval.page.show('calendars', null, function(){
				Cval.core.callback(callback);
			}, thiss.defaults.id);
		},
		getUrlFromId : function() {
			var parts = this.defaults.id.split('_');
			var routeConf = { 
				route : parts[0],
				routeParams : {
					controller : parts[1]
				}
			};
			return routeConf;
		},
		updateMenu : function() {
			$(this.defaults.menuSelector).find('.Cval-calendar-trigger-page').hide();
			$(this.defaults.menuSelector).find('.Cval-calendar-trigger-page').not('.Cval-menu-item-'+this.defaults.id).show();
		},
		_show : function() {
			this._super();
			this.updateMenu();
		}
	}, {});

})(Cval, jQuery);