(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.calendars_list', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('calendars_def', function(){
					
					
					Cval.page.control.calendars_def.extend('Cval.page.control.calendars_list', {
						defaults : {
							refreshOnShow : false,
							
							calendarRemoveSelector : '.Cval-trigger-remove',
							calendarEditSelector : '.Cval-trigger-edit',
							calendarMoveSelector : '.Cval-trigger-move',
							calendarRefreshSelector : '.Cval-trigger-refresh',
							calendarListSelector : '.Cval-list-calendars-wrapper',
							calendarSelectorPrefix : '.Cval-model-calendar-id-',

							positions : {
								modified : false
							}
						},
						init : function(callback) {
							var thiss = this;

							// Needed resources
							var resources = [
								Cval.route('cval/media', {
									file : 'css/page/calendars/list.css'
								})
							];

							Cval.resource.get(resources, function(){
								Cval.core.callback(callback);
							});	
						},
						_show : function() {
							this._super();
							this.refreshCalendarList();
						},
						_hide : function() {
							this._super();
							this.positionsMessageHide();
						},
						_bindEvents : function() {
							this._super();
							var thiss = this;

							// Bind remove calendar
							this.getElement().find(this.defaults.calendarRemoveSelector).livequery(function(){
								$(this).click(function(){
									var data = thiss.getCalendarEl($(this)).Cval_data();

									Cval.dialog.loadControl('calendar_remove', function(){
										Cval.dialog.control.calendar_remove.show(null, {
											data : data
										})
									});
									return false;
								})
							})

							// Bind edit calendar
							this.getElement().find(this.defaults.calendarEditSelector).livequery(function(){
								$(this).click(function(){
									var data = thiss.getCalendarEl($(this)).Cval_data(),
										control = 'calendar_'+data.calendar_type+'_edit';

									Cval.dialog.loadControl(control, function(){
										Cval.dialog.control[control].show(null, {
											id : data.id
										})
									});
									return false;
								})
							})

							// Bind refresh calendar
							this.getElement().find(this.defaults.calendarRefreshSelector).livequery(function(){
								$(this).click(function(){
									var id = thiss.getCalendarEl($(this)).Cval_data('id');
									thiss.refreshCalendar(id);
									return false;
								})
							})

							// Hover effects for table rows
							this.getElement().find(this.defaults.calendarListSelector+' tbody tr').livequery(function(){
								$(this).hover(function(){
									$(this).addClass('Cval-hover');
								}, function(){
									$(this).removeClass('Cval-hover')
								});
							});

							this.getElement().find(this.defaults.calendarListSelector+' tbody').sortable({
								handle: this.defaults.calendarMoveSelector,
								axis: 'y',
								containment: this.getElement().find(this.defaults.calendarListSelector),
								helper: function(event, ui) {
									ui.children().each(function() {
										$(this).width($(this).width());
									});
									return ui;
								},
								update : function(event, ui) {
									if ( ! thiss.defaults.positions.modified) {
										thiss.defaults.positions.modified = true;

										var content = thiss.getElement().find('.Cval-list-calendars-message-positions-content').html();
										var message = Cval.message.info(content, null, {
											wrapper : thiss.getElement().find('.Cval-list-calendars-info-box'),
											autoDestroy : false,
											closer : false,
											outerClass : 'Cval-list-calendars-message-positions'
										});

										message.find('a.save').click(function(e){
											e.preventDefault();
											thiss.positionsSave(message);
											return false;
										});
										message.find('a.dismiss').click(function(e){
											e.preventDefault();
											thiss.positionsDismiss(message);
											return false;
										});
									}

								}
							});
						},
						refreshCalendars : function(callback) {
							this.refreshCalendarList(callback);
						},
						refreshCalendarList : function(callback) {
							var thiss = this;

							Cval.ajax.callModal({
								route : 'calendar',
								routeParams : {
									controller : 'list_table'
								},
								data : {
									body_only : 1
								}
							}, function(json) {
								thiss.positionsMessageHide();
								thiss.updateCalendarList(json, false, callback);
							});
						},
						updateCalendarList : function(data, complete, callback) {
							var table = this.getElement().find(this.defaults.calendarListSelector);

							// Display, show no calendar message
							if (data.count > 0)
								this.noCalendarMessageHide();
							else
								this.noCalendarMessageShow();
							// Display/show table header
							table.toggle(data.count > 0);

							// Replace only body content
							if ( ! complete)
								table = table.find('tbody');

							table.html(data.html);
							// Inject js data
							Cval.ajax.data.inject(data);

							Cval.core.callback(callback);
						},
						getCalendarEl : function(el) {
							el = $(el);

							if ( ! el.is('tr'))
								el = el.parents('tr:first');

							return el;
						},
						getCalendarElFromId : function(id) {
							var selectorClass = this.defaults.calendarSelectorPrefix+id,
								el = $(this.getElement().find(this.defaults.calendarListSelector+' tbody tr'+selectorClass));

							return el;
						},
						positionsSave : function(messageEl) {
							var thiss = this,
								calendars = [],
								position = 1;

							// Hover effects for table rows
							$.each(this.getElement().find(this.defaults.calendarListSelector+' tbody tr'), function(){
								calendars.push({
									id : thiss.getCalendarEl($(this)).Cval_data('id'),
									position : position
								});
								position++;
							});

							Cval.ajax.callModal({
								route : 'calendar',
								routeParams : {
									controller : 'list_positions',
									action : 'update'
								},
								data : {
									calendars : calendars
								}
							}, function(json) {
								thiss.refreshCalendarList(function(){
									Cval.message.fromOptions(json.message);
								});
							});
						},
						positionsDismiss : function(messageEl) {
							var thiss = this;

							thiss.refreshCalendarList();
						},
						positionsMessageHide : function(messageEl) {
							if (messageEl === undefined) {
								messageEl = this.getElement().find('.Cval-list-calendars-info-box .Cval-list-calendars-message-positions');
							}
							Cval.message.destroy(messageEl, true);
							this.defaults.positions.modified = false;
						},
						noCalendarMessageShow : function() {
							var thiss = this;
							
							if (thiss.getElement().find('.Cval-list-calendars-info-box .Cval-list-calendars-message-nocalendar').length)
								return;
							
							var content = thiss.getElement().find('.Cval-list-calendars-message-nocalendar-content').html();
							Cval.message.info(content, null, {
								wrapper : thiss.getElement().find('.Cval-list-calendars-info-box'),
								autoDestroy : false,
								closer : false,
								outerClass : 'Cval-list-calendars-message-nocalendar'
							});
						},
						noCalendarMessageHide : function() {
							var messageEl = this.getElement().find('.Cval-list-calendars-info-box .Cval-list-calendars-message-nocalendar');
							Cval.message.destroy(messageEl, true);
						},
						refreshCalendar : function(id) {
							var thiss = this;

							Cval.ajax.call({
								route : 'calendar',
								routeParams : {
									controller : 'refresh'
								},
								data : {
									id : id
								},
								beforeSend : function(jqXHR, settings) {
									var el = thiss.getCalendarElFromId(id);
									el.find('td.Cval-column-refreshed_at .refreshed').toggle(false);
									el.find('td.Cval-column-refreshed_at .refreshing').toggle(true);
								},
								complete : function() {
									var el = thiss.getCalendarElFromId(id);
									el.find('td.Cval-column-refreshed_at .refreshing').toggle(false);
									el.find('td.Cval-column-refreshed_at .refreshed').toggle(true);
								}
							}, function(json) {
								var el = thiss.getCalendarElFromId(id);
								// Update refreshed at value
								el.find('td.Cval-column-refreshed_at .refreshed .text')
									.text(json.refreshed_at);
							});
						}
					}, {});
					
					
					Cval.page.control.calendars_list.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);