(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.calendars_timetable', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('calendars_def', function(){
					
					
					Cval.page.control.calendars_def.extend('Cval.page.control.calendars_timetable', {
						defaults : {
							refreshOnShow : false,
							
							calendarTimetableSelector : '.Cval-timetable-calendars-wrapper',
							timetableInitiated : false
						},
						refreshLimits : null,
						init : function(callback) {
							var thiss = this;

							// Needed resources
							var resources = [
								Cval.route('cval/media', {
									file : 'css/page/calendars/timetable.css'
								})
							];

							Cval.async('fullcalendar', function(){
								Cval.resource.get(resources, function(){
									Cval.core.callback(callback);
								});	
							});
						},
						_show : function() {
							this._super();
							this.refreshTimetable();
						},
						_bindEvents : function() {
							// Store refresh limits
							this._updateRefreshLimitsFromServer(this.getLoadedResponse().refresh_interval);
							this._super();
						},
						refreshCalendars : function(callback) {
							this.refreshTimetable(callback);
						},
						refreshTimetable : function(callback) {
							if ( ! this.defaults.timetableInitiated)
								this._timetableInit();
							else
								this.getTimetableEl().fullCalendar('refetchEvents');
							Cval.core.callback(callback);
						},
						_updateRefreshLimitsFromServer : function(limits) {
							this.refreshLimits = [
								Date.parse(limits[0]),
								Date.parse(limits[1])
							];
						},
						timetableLimits : function() {
							return this.refreshLimits;
						},
						_timetableInit : function() {
							var thiss = this;
							this.defaults.timetableInitiated = true;
							
							// Hover effects for table rows
							this.getTimetableEl().fullCalendar($.extend(true, {}, Cval.fullcalendar.defaults, {
								header: {
									right: 'agendaWeek,agendaDay'
								},
								viewDisplay: function(view) {
									thiss.fullcalendarViewDisplay.call(this, view, thiss);
								},
								events : {
									success: function(json) {
										// Store refresh limits
										thiss._updateRefreshLimitsFromServer(json.refresh_interval);
										return Cval.fullcalendar.defaults.events.success.apply(this, arguments);
									}
								}
							}));
							
							$(window).resize();
						},
						getTimetableEl : function() {
							return this.getElement().find(this.defaults.calendarTimetableSelector);
						},
						fullcalendarViewDisplay : function(view, thiss) {
							// display only when timetable is initiated
							if ( ! thiss.defaults.timetableInitiated)
								return;

							if ( ! view)
								view = thiss.getTimetableEl().fullCalendar('getView');

							var limits = thiss.timetableLimits();

							Cval.fullcalendar.clearTimeRanges(view);

							var pastTimeEl = Cval.fullcalendar.getTimeRange(view, limits[0].clone().addYears(-10), limits[0]);
							if (pastTimeEl) {
								pastTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').show();
							}

							var futureTimeEl = Cval.fullcalendar.getTimeRange(view, limits[1], limits[1].clone().addYears(10));
							if (futureTimeEl) {
								futureTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').show();
							}
						}
					}, {});
					
					
					Cval.page.control.calendars_timetable.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);