// KEXT[on]
(function(Cval, $) {

	Cval_dialog_control_calendar_remove_object = {
		staticParams : {
			defaults : {
				dialog : {
					title : Cval.i18n.get('Remove the calendar'),
					CvalHeight : false,
					destroyOnClose : true,
					width: 400
				},
				mainClass : 'alpha omega Cval-dialog-no-padding',
				loadRemoteContent : false,
				refreshOnShow : false,
				buttons : {
					confirm : $('<span />').text(Cval.i18n.get('Remove')),
					cancel : $('<span />').text(Cval.i18n.get('Cancel'))
				},
				calendarLabel : $('<span />').addClass('Cval-helper-font-weight-bold')
			},
			init : function(callback) {
				this._createHtml();
				Cval.core.callback(callback);
			},
			_beforeHideConfirmed : function(dialog, event, ui) {
				this._beforeHide.apply(this, arguments);
				Cval.page.getControl('calendars').refreshCalendars();
			},
			_show : function() {
				var data = this.data(this.getElement()).options.data;
				
				// Update calendar label
				this.defaults.calendarLabel.text(data.label);
				
				this._super();
			},
			_bindEvents : function() {
				var thiss = this;

				this.defaults.buttons.confirm.click(function(){
					Cval.ajax.callModal({
						route : 'calendar',
						routeParams : {
							controller : 'remove'
						},
						data : {
							id : thiss.data(thiss.getElement()).options.data.id
						}
					}, function(json) {
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}

						thiss.hide(null, 'confirmed');
					});

					return false;
				});

				this.defaults.buttons.cancel.click(function(){
					thiss.hide();

					return false;
				});
			},
			_createHtml : function() {
				if (this.defaults.loadedResponse.html)
					return;

				var thiss = this;
				var content = $('<div />')
						.addClass('Cval-dialog-content')
						.append(Cval.i18n.get('Do you really want to remove the calendar "'))
						.append(this.defaults.calendarLabel)
						.append(Cval.i18n.get('"?'));

				var footer = $('<div />')
						.addClass('Cval-dialog-footer ui-widget-header ui-corner-bottom Cval-helper-align-right');

				this.defaults.buttons.cancel
					.addClass('Cval-button Cval-button-live')
					.appendTo(footer);

				this.defaults.buttons.confirm
					.addClass('Cval-button Cval-button-live')
					.addClass('ui-state-highlight')
					.appendTo(footer);

				this.defaults.loadedResponse.html = $('<div />')
					.append(content).append(footer);
			}
		},
		params : {}
	}
	
	// KEXTSTART
	
	Cval.dialog.control.def.extend('Cval.dialog.control.calendar_remove', 
		Cval_dialog_control_calendar_remove_object.staticParams,
		Cval_dialog_control_calendar_remove_object.params
	);

})(Cval, jQuery);