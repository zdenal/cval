(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.calendar_def_edit', {
		defaults : {
			calendarType : null,
			dialog : {
				title : Cval.i18n.get('Edit the calendar'),
				CvalHeight : false,
				destroyOnClose : true
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'calendar',
				routeParams : {
					controller : null
				}
			},
			loadConfig : {
				data : {
					id : null
				}
			},
			refreshOnShow : true,
			colorSelectedItemSelector : '.Cval-calendar-color-edit-item-selected',
			colorSelectedItemClass : 'Cval-calendar-color-edit-item-selected'
		},
		init : function(callback) {
			var thiss = this;
			
			this.defaults.loadUrl.routeParams.controller 
			= this.defaults.calendarType+'_edit';
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jWizard.base.css'
			}),
			Cval.route('cval/media', {
				file : 'css/jquery/ui/jWizard.css'
			}),
			Cval.route('cval/media', {
				file : 'js/jquery/ui/jWizard.js'
			}),
			Cval.route('cval/media', {
				file : 'jquery/js/jquery.disabletextselect.js'
			}),
			Cval.route('cval/media', {
				file : 'css/dialog/calendar/edit.css'
			}),
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_beforeHideSave : function(dialog, event, ui) {
			this._beforeHide.apply(this, arguments);
			Cval.page.getControl('calendars').refreshCalendars();
		},
		getLoadConfig : function() {
			this.defaults.loadConfig.data.id = this.data(this.getElement()).options.id;
			return this._super();
		},
		_bindEvents : function() {
			var thiss = this;
			
			var wizardEl = this.getElement().find('.jWizard');
			
			wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: false
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.wizardNext(wizardEl, event, ui);
				},
				finish : function() {
					thiss.wizardFinish(wizardEl);
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
			//stepsHeight : $(window).height()-300
			});
			
			this._initSettingsStep(wizardEl);
			this._initColorSelection(wizardEl);
		},
		_initSettingsStep : function(wizardEl) {
			var thiss = this;
			
			wizardEl.find('.Cval-dialog-calendar-edit-settings form').submit(function(e){
				e.preventDefault();
				// On form submit go to nextStep
				wizardEl.Cval_jWizard('nextStep');
				return false;
			})
		},
		_initColorSelection : function(wizardEl) {
			var thiss = this,
				colorInput = wizardEl.find('.Cval-dialog-calendar-edit-settings form input[name=color]');
			
			wizardEl.find('.Cval-calendar-color-edit-item').disableTextSelect().click(function(e){
				if ($(this).is(thiss.defaults.colorSelectedItemSelector))
					return false;
				
				colorInput.val(Cval.core.htmlClassValue($(this), 'Cval-calendar-color-edit-item-id-'));
				wizardEl.find(thiss.defaults.colorSelectedItemSelector)
					.html('').removeClass(thiss.defaults.colorSelectedItemClass);
					
				$(this).html('&bull;').addClass(thiss.defaults.colorSelectedItemClass);
				return false;
			})
		},
		wizardNext : function(wizardEl, event, ui) {},
		wizardFinish : function(wizardEl) {
			var settingsEl = $(wizardEl).find('.Cval-dialog-calendar-edit-settings');
			
			this._wizardFinish(wizardEl, settingsEl);
		},
		_wizardFinish : function(wizardEl, settingsEl) {
			var thiss = this,
			form = settingsEl.find('form');

			Cval.ajax.callModal({
				route : 'calendar',
				routeParams : {
					controller : thiss.defaults.calendarType+'_edit',
					action : 'save'
				},
				data: $.extend({}, {
					id : thiss.data(thiss.getElement()).options.id
				}, thiss._finishAjaxData(wizardEl, settingsEl))
			}, function(json) {
				thiss._wizardFinishSuccess(wizardEl, settingsEl, json);
			})

			return false;
		},
		_wizardFinishSuccess : function(wizardEl, settingsEl, json) {
			if (json.message !== undefined)
			{
				Cval.message.fromOptions(json.message);
			}
			
			this.hide(null, 'save');
		},
		/**
		 * Returns data from dialog you want send to server after save is invoked.
		 * Should be overidden in subclasses.
		 */
		_finishAjaxData : function(wizardEl, settingsEl) {
			return $(settingsEl).find('form:first').serializeJSON();
		}
	}, {});

})(Cval, jQuery)