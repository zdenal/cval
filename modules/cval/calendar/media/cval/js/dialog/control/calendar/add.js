(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.calendar_add', {
		defaults : {
			dialog : {
				title : Cval.i18n.get('Add calendar'),
				CvalHeight : false,
				destroyOnClose : true
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'calendar',
				routeParams : {
					controller : 'add'
				}
			},
			refreshOnShow : true
		},
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
				Cval.route('cval/media', {
					file : 'jquery/css/jWizard.base.css'
				}),
				Cval.route('cval/media', {
					file : 'css/jquery/ui/jWizard.css'
				}),
				Cval.route('cval/media', {
					file : 'js/jquery/ui/jWizard.js'
				})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_beforeHideSave : function(dialog, event, ui) {
			this._beforeHide.apply(this, arguments);
			Cval.page.getControl('calendars').refreshCalendars();
		},
		_bindEvents : function() {
			var thiss = this;
			
			var wizardEl = this.getElement().find('.jWizard');
			
			wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: false
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.callCalendarControl(function(dialogEl){
						this.addWizardNext(dialogEl, wizardEl, event, ui);
					})
				},
				finish : function(event, ui) {
					thiss.callCalendarControl(function(dialogEl){
						this.addWizardFinish(dialogEl, wizardEl, event, ui);
					})
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
				//stepsHeight : $(window).height()-300
			});
			
			this._initTypeStep(wizardEl);
		},
		_initTypeStep : function(wizardEl) {
			var thiss = this,
				form = wizardEl.find('.Cval-dialog-calendar-add-type form');
			
			form.find('.Cval-calendar-add-type').change(function(){
				thiss._typeSelectOnChange(wizardEl, $(this));
			});
			
			wizardEl.find('.Cval-dialog-calendar-add-type form').submit(function(e){
				e.preventDefault();
				// On form submit go to nextStep
				wizardEl.Cval_jWizard('nextStep');
				return false;
			})
		},
		_wizardNext : function(wizardEl, event, ui) {
			this.callCalendarControl(function(dialogEl){
				this.addWizardNext(dialogEl, wizardEl, event, ui);
			})
		},
		calendarControl : function() {
			return this.data(this.getElement()).calendarControl;
		},
		callCalendarControl : function(callback, secondCall) {
			var thiss = this;
			
			if ( ! this.calendarControl()) {
				if (secondCall) {
					Cval.core.exception('Calendar control not loaded');
					return;
				}
				this._loadCalendarControl(function() {
					thiss.callCalendarControl(callback, true);
				})
				return;
			}
			
			callback.call(this.calendarControl(), this.getElement());
		},
		_loadCalendarControl : function(callback) {
			var thiss = this,
				calendarControlType = this.getElement()
					.find('.Cval-dialog-calendar-add-type form')
					.find('.Cval-calendar-add-type')
					.val();
					
			if ( ! calendarControlType) {
				Cval.core.exception('Calendar type is not selected');
				return;
			}
			
			Cval.calendar.loadControl(calendarControlType, function(){
				Cval.calendar.control[calendarControlType].addPairWithDialogControl(thiss.getElement(), callback);
			});
		},
		_typeSelectOnChange : function(wizardEl, selectEl) {
			var thiss = this,
				descWrapperEl = wizardEl.find('.Cval-calendar-add-type-description-wrapper'),
				calendarControlType = selectEl.val(),
				typeDescElClass = 'Cval-calendar-add-type-description-name-'+calendarControlType;
				
			// Hide every other descriptions
			descWrapperEl.find('> div').hide();
			
			// Find calendar description
			var typeDescEl = descWrapperEl.find('.'+typeDescElClass);
			if (typeDescEl.length > 0) {
				// Description is created already, just show it
				typeDescEl.show();
			} else {
				// Not created, so load description and append it to wrapper
				Cval.ajax.callModal({
					route : 'calendar',
					routeParams : {
						controller : calendarControlType+'_add',
						action : 'type_description'
					}
				}, function(json) {
					var typeDescEl = $('<div/>')
							.addClass(typeDescElClass)
							.html(json.html);
							
					descWrapperEl.append(typeDescEl);
				})
			}

			return false;
		}
	}, {});

})(Cval, jQuery)