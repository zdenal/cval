(function(Cval, $) {

	$.Class.extend('Cval.calendar.control.def', {
		defaults : {
			/**
			 * Load URL used for loading settings steps in dialog wizard while
			 * adding a calendar. Can be string or JSON object
			 * with route and routeParams
			 */
			loadUrlAddSettings : null,
			/**
			 * Load config data used in load call.
			 */
			loadConfigAddSettings : {}
		},
		init : function(callback) {
			Cval.core.callback(callback);			
		},
		data : function(el, data) {
			if (data !== undefined)
				$(el).data('Cval-calendar', data);
			
			data = $(el).data('Cval-calendar');
			if (data === undefined)
				$(el).data('Cval-calendar', {});
			
			return $(el).data('Cval-calendar');
		},
		dataExtend : function(el, data) {
			return this.data(el, $.extend(true, this.data(el), data));
		},
		addPairWithDialogControl : function(dialogEl, callback) {
			var thiss = this;
			
			this.addGetDialogControl(dialogEl).dataExtend(dialogEl, {
				calendarControl : thiss
			})
			Cval.core.callback(callback);
		},
		addWizardNext : function(dialogEl, wizardEl, event, ui) {
			if (ui.nextStepIndex === 1) {
				this._addUpdateSettingsContent(dialogEl, wizardEl, event, ui);
			}
		},
		addWizardFinish : function(dialogEl, wizardEl) {
			var settingsEl = $(wizardEl).find('.Cval-dialog-calendar-add-settings');
			
			this._addWizardFinish(dialogEl, wizardEl, settingsEl);
		},
		addGetDialogControl : function(dialog) {
			return Cval.dialog.getControl(dialog);
		},
		_addUpdateSettingsContent : function(dialog, wizardEl) {
			var thiss = this,
				form = this.addGetDialogControl(dialog).getElement()
							.find('.Cval-dialog-calendar-add-type form');

			var data = form.serializeJSON();

			Cval.ajax.callModal({
				route : 'calendar',
				routeParams : {
					controller : thiss.shortName+'_add',
					action : 'select_type'
				},
				data : data
			}, function(json) {
				var settingsEl = thiss.addGetDialogControl(dialog).getElement()
					.find('.Cval-dialog-calendar-add-settings');
				
				settingsEl.html(json.html);
				thiss._addBindSettingsEvents(dialog, wizardEl, settingsEl);
			})

			return false;
		},
		_addBindSettingsEvents : function(dialog, wizardEl, settingsEl) {
			var thiss = this;
			$(settingsEl).find('form').submit(function(){
				thiss.addWizardFinish(dialog, wizardEl);
				return false;
			})
		},
		_addWizardFinish : function(dialogEl, wizardEl, settingsEl) {
			var thiss = this,
				form = settingsEl.find('form');

			var data = form.serializeJSON();

			Cval.ajax.callModal({
				route : 'calendar',
				routeParams : {
					controller : thiss.shortName+'_add',
					action : 'finish'
				},
				data : data
			}, function(json) {
				thiss._addWizardFinishSuccess(dialogEl, wizardEl, settingsEl, json);
			})

			return false;
		},
		_addWizardFinishSuccess : function(dialogEl, wizardEl, settingsEl, json) {
			if (json.message !== undefined)
			{
				Cval.message.fromOptions(json.message);
			}
			
			this.addGetDialogControl(dialogEl).hide(null, 'save');
		}
	}, {});

})(Cval, jQuery)