(function(Cval, $) {

	Cval.calendar = {
		control : {}, // storage for calendar controls
		config : {
			controlClassPrefix : 'Cval-calendar-control-'
		},
		init : function(callback) {
			this.loadControl('def', callback);
		},
		loadControl : function(type, callback) {
			var thiss = this;
			// load default control if type is 
			if (type === null)
				Cval.core.exception('calendar control type must be set');

			// If we have array of resources
			if (Object.prototype.toString.apply(type) === '[object Array]') {
				var i;
				// We have only one item in array
				if (type.length == 1) {
					// So create a single resource from it
					for (i in type) {
						type = type[i];
						break;
					}
				} else {
					for (i in type) {
						// Get first resource
						var t = type[i];
						// Remove this element from array
						type.splice(i, 1);
						// Load single resource and construct callback
						// to call other resources
						Cval.async('calendar.control.'+t, function(){
							thiss.loadControl(type, callback);
						});
						break;
					}
					return;
				}
			}
			Cval.async('calendar.control.'+type, function(){
				Cval.core.callback(callback);
			});
		}
	}

})(Cval, jQuery)