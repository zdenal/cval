<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	/**
	 * Array of calendar types. It is assocative array where key is model name.
	 * Config array has required `id` parameter stored in DB.
	 * 
	 * Example:
	 *	'icalendar' => array(
	 *		'id' => 1,
	 *		'label' => 'iCalendar'
	 *	)
	 */
	'types' => array(),

);

