<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	
	'colors' => array(
		'AC725E',
		'D06B64',
		'F83A22',
		'FA573C',
		'FF7537',
		'FFAD46',
		'42D692',
		'16A765',
		'7BD148' => array(
			'textColor' => '#000000'
		),
		'B3DC6C' => array(
			'textColor' => '#000000'
		),
		'FBE983' => array(
			'textColor' => '#000000'
		),
		'FAD165' => array(
			'textColor' => '#000000'
		),
		'92E1C0' => array(
			'textColor' => '#000000'
		),
		'9FE1E7' => array(
			'textColor' => '#000000'
		),
		'9FC6E7' => array(
			'textColor' => '#000000'
		),
		'4986E7',
		'9A9CFF',
		'B99AFF',
		'C2C2C2' => array(
			'textColor' => '#000000'
		),
		'CABDBF' => array(
			'textColor' => '#000000'
		),
		'CCA6AC',
		'F691B2',
		'CD74E6',
		'A47AE2',
		'CCCCCC' => array(
			'alias' => 'cval_draft',
			'selectable' => FALSE,
			'textColor' => '#000000',
			'borderColor' => '#000000'
		),
		'0000FF' => array(
			'alias' => 'cval_event',
			'selectable' => FALSE,
		)
	),
	
	'default' => 'AC725E'

);

