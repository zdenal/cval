<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	
	'refresh' => array(
		/**
		 * Length interval for retrieving events. Needs to be an array which
		 * is used in Date::move() method.
		 */
		'length' => array(1, 'month', TRUE, FALSE), // 1 month, not strict
		/**
		 * Period for refreshing calendars. Must be in seconds.
		 */
		'period' => 600 // 10 minutes
	)

);

