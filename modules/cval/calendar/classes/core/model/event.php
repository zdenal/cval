<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Core_Model_Event
 * @package Cval
 * @author	zdennal
 */
class Core_Model_Event extends Jelly_Model
{
	
	public static function initialize(Jelly_Meta $meta)
    {
		$meta->primary_key('idmpk');
		$meta->sorting(array('start' => 'ASC'));
		
		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(82),
				)
			)),
			'calendar' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			/**
			 * Automatically created before save.
			 */
			'id' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'start' => new Field_Datetime(array(
				'rules' => array(
					'not_empty' => NULL,
					'less' => array('end')
				)
			)),
			'end' => new Field_Datetime(array(
				'rules' => array(
					'not_empty' => NULL,
					'greater' => array('start')
				)
			)),
			'length' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'title' => new Field_String(array(
				'null' => TRUE,
				'rules' => array(
					'max_length' => array(255),
				)
			))
		));

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }
	
	/**
	 * Creates new instance of event
	 * 
	 * @param	Model_Calendar	Owner calendar
	 * @param	int		Unique ID
	 * @param	int		Start timestamp
	 * @param	int		End timestamp
	 * @return	Model_Event	or NULL, timestamps are not ok	
	 */
	public static function create(Model_Calendar $calendar, $id, $start, $end, array $data = array())
	{
		// Simple check for not empty events
		if ($end <= $start)
		{
			return NULL;
		}
		
		// Create event
		$event = Jelly::factory('event')
			->set(array(
				'calendar' => $calendar,
				'id' => $id,
				'start' => $start,
				'end' => $end,
				'title' => Arr::get($data, 'title')
			))->save();
		
		return $event;
	}
	
	public function event_data()
	{
		return $this->as_array_internal();
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// Calculate length
		$this->length = $this->end - $this->start;
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
		}
		
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		return implode('__', array($this->calendar->id(), $this->id));
	}
	
} // End Core_Model_Event