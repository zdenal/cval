<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Core_Model_Calendar
 * @package Cval
 * @author	zdennal
 */
class Core_Model_Calendar extends Jelly_Model
{
	/**
	 * @var	Model_Calendar_Default Calendar subclass
	 */
	protected $_subclass;
	
	public static function initialize(Jelly_Meta $meta)
    {
		$meta->name_key('label')
			 ->primary_key('idmpk')
             ->sorting(array('position' => 'ASC'));
		
		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(61),
				)
			)),
			'user' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL,
				)
			)),
			'calendar_id' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'calendar_type_id' => new Field_Enum(array(
				'integer' => TRUE,
				'choices' => Cval_Calendar::types('id', 'label'),
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'refreshed_at' => new Field_Datetime,
			'refreshing' => new Field_Boolean,
			'color' => new Field_Cval_Calendar_Color(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'events' => new Field_HasMany
		));
		
		Behavior_Labelable::initialize($meta, array(
			'null' => FALSE,
			'label' => 'name',
			'rules' => array(
				'not_empty' => NULL,
				'max_length' => array(1000)
			)
		));

		Behavior_Positionable::initialize($meta);
        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }
	
	/**
	 * Returns calendar subclass with specific settings.
	 * 
	 * @return	Model_Calendar_Default
	 */
	public function subclass(Model_Calendar_Default $subclass = NULL)
	{
		// Allows to set subclass manually, but consistency check is run
		if ($subclass !== NULL
				AND $subclass->meta()->model() == 'calendar_'.$this->calendar_type()
				AND $subclass->user->id() == $this->user->id()
				AND $subclass->id == $this->calendar_id)
		{
			$this->_subclass = $subclass;
			$this->_subclass->calendar = $this;
		}
		
		if ($this->_subclass === NULL)
		{
			$this->_subclass = Jelly::select('calendar_'.$this->calendar_type())
					->where('user', '=', $this->user->id())
					->where('id', '=', $this->calendar_id)
					->load();
			$this->_subclass->calendar = $this;
		}
		
		return $this->_subclass;
	}
	
	public function calendar_type()
	{
		return $this->_calendar_config('name');
	}
	
	protected function _calendar_config($value = NULL, $default = NULL)
	{
		if (empty($this->calendar_type_id))
		{
			throw new Kohana_Exception('`calendar_type_id` field is empty, calendar config can not be loaded');
		}
		
		return Cval_Calendar::type($this->calendar_type_id, 'id', $value, $default);
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
			unset($this->meta()->fields('label')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
			$this->meta()->fields('label')->rules['not_empty'] = NULL;
			// Set empty label, so it is not treated as null
			$this->label = '';
		}
		
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		// in case position not set
		if (empty($this->position))
		{
			// find out how many calendars user have, 
			// and set position +1 from the last one.
			$position = Jelly::select($this->meta()->model())
					->where('user', '=', $this->user->id())
					->count();

			$this->position = $position + 1;
		}
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		return implode('__', array($this->user->id(), $this->calendar_id, $this->calendar_type_id));
	}
	
	protected function _before_delete()
	{
		parent::_before_delete();
			
		// Subclass found delete it as first
		if ($this->subclass()->loaded())
		{
			// Delete subclass
			$this->subclass()->delete(NULL, TRUE);
		}
		
		// Delete all events
		$this->delete_events();
	}
	
	public function delete_events()
	{
		try
		{
			Database::instance()->transaction_start();
		
			foreach ($this->events as $event)
			{
				$event->delete();
			}
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			throw $e;
		}
	}
	
	/**
	 * Get all events. Default as timestamps.
	 * 
	 * @param	timestamp	start of interval
	 * @param	timestamp	end of interval
	 * @param	bool		TRUE return events as data or object
	 * @return	array		array of events
	 */
	public function events($start = NULL, $end = NULL, $data = TRUE)
	{
		// First do refresh of calendar, not forced and synchronous
		$calendar = $this->refresh();
		
		$events = array();
		
		$event = Jelly::factory('event');
		$objects = $calendar->get('events');
		
		if ($start !== NULL)
		{
			$start = $event->meta()->fields('start')->format_for_db($start);
			$objects->where('end', '>=', $start);
		}
		if ($end !== NULL)
		{
			$end = $event->meta()->fields('end')->format_for_db($end);
			$objects->where('start', '<=', $end);
		}
			
		$objects = $objects->execute();
		
		foreach ($objects as $event)
		{
			if ($data)
			{
				$event_data = $event->event_data();
				$event_data['color'] = $this->color_data();
				$events[] = $event_data;
			}
			else
			{
				$events[] = $event;
			}
		}
		
		return $events;
	}
	
	/**
	 * Checks if calendar has free time in given interval.
	 * 
	 * @param	timestamp	start of interval
	 * @param	timestamp	end of interval
	 * @return	bool		FALSE when calendar has at least one event in interval
	 */
	public function has_free_time($start, $end)
	{
		// First do refresh of calendar, not forced and synchronous
		$this->refresh();
		
		$event = Jelly::factory('event');
		// Format time for DB select
		$start = $event->meta()->fields('start')->format_for_db($start);
		$end = $event->meta()->fields('end')->format_for_db($end);
		
		// Count events in given interval for this user
		$count = Jelly::select('event')
			->where('calendar', '=', $this->id())
			->where('start', '<', $end)
			->where('end', '>', $start)
			->count();
		
		return $count > 0 ? FALSE : TRUE;
	}
	
	/**
	 * Tests if calendar needs to be refreshed according tu actual refresh
	 * interval.
	 * 
	 * @return	bool 
	 */
	public function needs_refresh()
	{
		// Get refresh interval
		list($refresh_start, $refresh_end) = Cval_Calendar_Timer::refresh_interval();
		
		return $this->refreshed_at < $refresh_start;
	}
	
	/**
	 * Trigger for calendar refresh
	 * 
	 * @return $this
	 */
	public function refresh($force = FALSE, $synchronous = TRUE)
	{	
		// Refresh only if it is needed and not forced
		if ( ! $force AND ! $this->needs_refresh())
		{
			return $this;
		}
		
		// When already refreshing
		if ($this->refreshing)
		{
			// Do not run refresh on asynchronous call
			if ( ! $synchronous)
			{
				return $this;
			}
			
			// Periodically wait till calendar is refreshed
			$actual_state = $this->reload();
			while ($actual_state->refreshing)
			{
				// Sleep one second to unload CPU
				sleep(1);
				$actual_state = $this->reload();
			}
			// And than do not return this object, but reloaded one
			return $actual_state;
		}
		
		// Start refreshing
		$this->_set_refreshing(TRUE);
		
		try
		{
			Database::instance()->transaction_start();
	
			// Get refresh interval
			$refresh_interval = Cval_Calendar_Timer::refresh_interval();
			
			// Get event timestamps
			$events_data 
				= call_user_func_array(array($this->subclass(), 'refresh'), $refresh_interval);
			
			// Update calendar data so changes made in subclass are propagated
			$this->_changed = $this->subclass()->calendar->_changed; 
			
			// Delete old events
			$this->delete_events();
			
			// Get refresh limits
			list($refresh_start, $refresh_end) = $refresh_interval;
			
			$id_counter = 1;
			// Create new events
			foreach ($events_data as $event_data)
			{
				if ($event_data['start'] > $refresh_end OR $event_data['end'] < $refresh_start)
				{
					// Do not add events which are not in refresh interval
					continue;
				}
				// Create and save event
				Model_Event::create($this, $id_counter++, $event_data['start'], $event_data['end'], $event_data);
			}
			
			// Make sure some label is set
			if ( ! strlen($this->label))
			{
				$this->label = Cval::i18n('Untitled');
			}
			// Update refresh at date
			$this->refreshed_at = Date::time();
			// Save this model
			$this->save();
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			// Stop refreshing
			$this->_set_refreshing(FALSE);
			
			throw $e;
		}
		
		// Stop refreshing
		$this->_set_refreshing(FALSE);
		
		return $this;
	}
	
	protected function _set_refreshing($running)
	{
		// Set refreshing param to TRUE
		$this->refreshing = (bool) $running;
		$this->save(NULL, TRUE);
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-calendar-id-'.$this->id();
	}
	
	/**
	 * Returns color config array
	 * 
	 * @return	array 
	 */
	public function color_data()
	{
		return Cval_Calendar_Color::color($this->color);
	}
	
} // End Core_Model_Calendar