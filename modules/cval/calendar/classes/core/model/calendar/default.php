<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Core_Model_Calendar_Default
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Calendar_Default extends Jelly_Model
{
	protected $_calendar_object;
	
	public static function initialize(Jelly_Meta $meta)
    {	
		$meta->primary_key('idmpk');
		
		$meta->fields(array(
			/**
			 * Automatically created before save. It is concacted with user
			 * and id.
			 */
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(40),
				)
			)),
			'user' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL,
				)
			)),
			/**
			 * Automatically created before save.
			 */
			'id' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			/**
			 * Just for better connection to parent db object.
			 * Is not required with rules, because is automatically created
			 * before save.
			 * 
			 */
			'calendar' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'label' => new Field_Related(array(
				'relationship' => 'calendar'
			)),
			'color' => new Field_Related(array(
				'relationship' => 'calendar'
			))
		));

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }
	
	/**
	 * Method called when calendar has no label. Data from refresh call
	 * are passed along, which is useful for calendar resources which are
	 * file based, so label is determined from file and can be appended to
	 * refresh data array on refresh, otherwise two calls for sometimes large 
	 * file needs to be made.
	 * 
	 * @return	string
	 */
	abstract protected function _create_label(array $refresh_data);
	
	/**
	 * Internal method for calendar model. Is called from refresh method with
	 * same parameters and also returns same data.
	 * 
	 * @param	int		Start timestamp
	 * @param	int		End timestamp
	 * @return	array	Array of timestamps
	 */
	abstract protected function _refresh($start, $end);
	
	/**
	 * Method called to refresh data for this calendar. Usually remote call.
	 * It needs to return array of timestamps representing events between
	 * $start end $end parameters.
	 * Also calendar label can be set here if is updated on refresh.
	 * 
	 * Example:
	 * 
	 *	array(
	 *		array(
	 *			'start' => '12345678', // event start timestamp
	 *			'end' => '23456789' // event end timestamp
	 *		),
	 *		array(
	 *			'start' => '23456789', // event start timestamp
	 *			'end' => '34567890' // event end timestamp
	 *		)
	 *	);
	 * 
	 * @param	int		Start timestamp
	 * @param	int		End timestamp
	 * @return	array	Array of timestamps
	 */
	public function refresh($start, $end)
	{
		$data = $this->_refresh($start, $end);
		
		// If we do not have label yet, try to set it
		if ( ! strlen($this->label))
		{
			// Set updated label from refresh call
			$label = $this->_create_label($data);
			$this->label = strlen($label) ? $label : Cval::i18n('Untitled');
		}
		
		$data = $this->_after_refresh($data);
		
		return $data;
	}
	
	public function calendar_config($value = NULL, $default = NULL)
	{
		$calendar_type = str_replace('calendar_', '', $this->meta()->model());
		
		return Cval_Calendar::type($calendar_type, 'name', $value, $default);
	}
	
	/**
	 * Deletes a single record.
	 *
	 * @param   $key  A key to use for non-loaded records
	 * @return  boolean
	 * */
	public function delete($key = NULL, $from_parent = FALSE)
	{
		if ( ! $from_parent AND $this->loaded() AND $this->calendar->loaded())
		{
			throw new Kohana_Exception('Delete of `:model`, id `:id`, must be invoked from parent class', array(
				':model' => $this->meta()->model(),
				':id' => $this->id()
			));
		}
		
		return parent::delete($key);
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// On create
		if ( ! $loaded)
		{
			$this->color = $this->_get_oncreate_color();
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
			unset($this->meta()->fields('id')->rules['not_empty']);
			unset($this->meta()->fields('calendar')->rules['not_empty']);
		}
	}
	
	protected function _after_save($loaded = NULL)
	{
		// Save also main calendar object. Useful for label updates for example.
		$this->calendar->save();
		
		// Refresh calendar on create
		if ( ! $loaded OR $this->last_changed('url'))
		{
			$this->calendar->refresh(TRUE, FALSE);
		}
		
		parent::_after_save();
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
			$this->meta()->fields('id')->rules['not_empty'] = NULL;
			$this->meta()->fields('calendar')->rules['not_empty'] = NULL;
		}
		
		// Generate ID
		$this->id = $this->_create_id($loaded);
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		// Set calendar
		$this->_set_calendar($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_id($loaded)
	{
		if ($loaded AND ! empty($this->id))
		{
			return $this->id;
		}
		
		$last_calendar = Jelly::select($this)
				->where('user', '=', $this->user->id())
				->order_by('id', 'DESC')
				->load();
		
		$id = 1;
		
		if ($last_calendar->loaded())
		{
			$id = $last_calendar->id + 1;
		}
		
		return $id;
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		if (empty($this->id))
		{
			throw new Kohana_Exception('Model `:model` needs to have field `id` set', array(
				':model' => $this->meta()->model
			));
		}
		
		return implode('__', array($this->user->id(), $this->id));
	}
	
	protected function _set_calendar($loaded)
	{
		// Create calendar
		$calendar = $this->_create_calendar($loaded);
		// Update label
		$calendar->label = $this->calendar->label;
		$calendar->color = $this->calendar->color;
		// Save calendar
		$calendar->save();
		
		// Set calendar to this field
		$this->calendar = $calendar;
	}
	
	/**
	 * Called on before save, when object is created
	 * 
	 * @return	Model_Calendar
	 */
	protected function _create_calendar($loaded)
	{
		if ($loaded AND $this->calendar->loaded()
				AND $this->calendar->user->id() == $this->user->id()
				AND $this->calendar->calendar_id == $this->id
				AND $this->calendar->calendar_type_id == $this->calendar_config('id'))
		{
			return $this->calendar;
		}
		
		$calendar = Jelly::select('calendar')
				->where('user', '=', $this->user->id())
				->where('calendar_id', '=', $this->id)
				->where('calendar_type_id', '=', $this->calendar_config('id'))
				->load();
		
		if ( ! $calendar->loaded())
		{
			$calendar->user = $this->user;
			$calendar->calendar_id = $this->id;
			$calendar->calendar_type_id = $this->calendar_config('id');
		}
		
		return $calendar;
	}
	
	/**
	 * Called after _refresh call.
	 * 
	 * @param	array	Return data from _refresh method
	 * @return	array 
	 */
	protected function _after_refresh(array $refresh_data)
	{
		return $refresh_data;
	}
	
	/**
	 * Returns color for calendar when is created
	 * 
	 * @return	string 
	 */
	protected function _get_oncreate_color()
	{
		$colors = array_keys(Cval_Calendar_Color::colors());
		$calendars = Jelly::select('calendar')
				->where('user', '=', $this->user->id())
				->execute();
		
		foreach ($calendars as $calendar)
		{
			$color_index = array_search($calendar->color, $colors);
			if ($color_index !== FALSE)
			{
				unset($colors[$color_index]);
			}
		}
		
		$color = reset($colors);
		if ($color !== FALSE)
		{
			return $color;
		}
		return Cval_Calendar_Color::config('default');
	}
	
} // End Core_Model_Calendar_Default