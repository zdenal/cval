<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_Default_Edit
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_Default_Edit extends Controller_Cval_Ajax {

	/**
	 * @var	string	Calendar type
	 */
	protected $_calendar_type;
	
	/**
	 * @var Model_Calendar_Default
	 */
	protected $_calendar;
	
	/**
	 * @var Model_Calendar
	 */
	protected $_base_calendar;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		if ( ! $this->_calendar_type)
		{
			throw new Kohana_Exception('Calendar type parameter is not set');
		}
		
		$this->_base_calendar = Jelly::select('calendar')
				->load(Arr::get($_POST, 'id', 0));

		if ( ! $this->_base_calendar->loaded())
		{
			throw Model_Not_Found_Exception::create($this->_base_calendar->meta()->model(), Arr::get($_POST, 'id', 0));
		}
		if ( $this->_base_calendar->calendar_type() != $this->_calendar_type)
		{
			throw new Kohana_Exception('Calendar type does not match');
		}
		
		$this->_calendar = $this->_base_calendar->subclass();
		
		if ( ! $this->_calendar->loaded())
		{
			throw new Kohana_Exception('Subclass model `:submodel` for model `:model` with ID `:id` does not exist', array(
				':model' => $this->_base_calendar->meta()->model(),
				':id' => $this->_base_calendar->id(),
				':submodel' => $this->_calendar->meta()->model()
			));
		}

		$this->_acl_resource = $this->_base_calendar;
	}
	
	public function action_index()
	{
		$html = View::factory('cval/content/calendar/default/edit/settings')
				->set('csrf', Security::token(TRUE, 'cval/calendar/edit/settings'))
				->set('specific_view', View::factory('cval/content/calendar/'.$this->_calendar_type.'/edit/settings'))
				->bind('calendar', $calendar);
		
		$calendar = $this->_calendar;
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	public function action_save() 
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/calendar/edit/settings', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, $this->_save_post_fields()));
		
		$this->_calendar->set($post_fields);
		$this->_calendar->save();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Calendar was updated!')),
			'calendar' => Cval_REST_Model::get($this->_calendar)
		);
	}
	
	/**
	 * Array of fields to extract from post data
	 * 
	 * @return	array
	 */
	protected function _save_post_fields()
	{
		return array('label', 'color');
	}

}

