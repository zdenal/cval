<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_Default_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_Default_Add extends Controller_Cval_Ajax {

	/**
	 * @var	string	Calendar type
	 */
	protected $_calendar_type;
	
	/**
	 * @var Model_Calendar_Default
	 */
	protected $_calendar;
	
	/**
	 * @var Model_Calendar
	 */
	protected $_base_calendar;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		if ( ! $this->_calendar_type)
		{
			throw new Kohana_Exception('Calendar type parameter is not set');
		}
		
		$this->_calendar = Jelly::factory('calendar_'.$this->_calendar_type);
		
		$this->_base_calendar = Jelly::factory('calendar');
		$this->_base_calendar->calendar_type_id = $this->_calendar->calendar_config('id');

		$this->_acl_resource = $this->_base_calendar;
	}
	
	public function action_select_type() 
	{
		Security::check(Arr::get($_POST, 'csrf'), 'cval/calendar/add', TRUE);
		
		$html = View::factory('cval/content/calendar/default/add/settings')
				->set('csrf', Security::token(FALSE, 'cval/calendar/add'))
				->set('specific_view', View::factory('cval/content/calendar/'.$this->_calendar_type.'/add/settings'))
				->bind('calendar', $calendar);
		
		$calendar = $this->_calendar;
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	public function action_finish() 
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/calendar/add', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, $this->_finish_post_fields()));
		
		$calendar = $this->_calendar;
		$calendar->set($post_fields);
		$calendar->user = $this->cval->user();
		$calendar->save();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Calendar was added!')),
			'calendar' => Cval_REST_Model::get($calendar)
		);
	}
	
	public function action_type_description()
	{
		$html = View::factory(array(
			'cval/content/calendar/'.$this->_calendar_type.'/add/description',
			'cval/content/calendar/default/add/description', 
			))->bind('calendar', $calendar);
		
		$calendar = $this->_calendar;
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	/**
	 * Array of fields to extract from post data
	 * 
	 * @return	array
	 */
	abstract protected function _finish_post_fields();
	
}

