<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_List
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_List_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{
		$view = Arr::get($_POST, 'body_only') 
				? 'cval/content/calendars/list/table/body'
				: 'cval/content/calendars/list/table';
		
		$html = View::factory($view)
				->bind('calendars', $calendars);
		
		$calendars = Jelly::select('calendar')
				->where('user', '=', $this->cval->user()->id())
				->execute();
		
		$js_data = array();
		
		foreach ($calendars as $calendar)
		{
			// Set js data
			$js_data[$calendar->html_class(TRUE)] 
					= Cval_REST::instance()->model($calendar)->render();
		}
		
		$this->request->response = array(
			'html' => $html->render(),
			'count' => $calendars->count(),
			'js' => array(
				'data' => $js_data
			)
		);
	}

}

