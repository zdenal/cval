<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_List_Positions
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_List_Positions extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_update() 
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$calendars = Security::xss_clean(Arr::get($_POST, 'calendars', array()));
		
		$not_found = array();
		
		foreach ($calendars as $data)
		{	
			$calendar = Jelly::select('calendar')
					->where('user', '=', $this->cval->user()->id())
					->load(Arr::get($data, 'id'));
			
			if ( ! $calendar->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			// Update position
			$calendar->position = Arr::get($data, 'position');
			$calendar->save();
		}
		
		$message = Cval::i18n('The order of calendars was updated.');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
	}

}

