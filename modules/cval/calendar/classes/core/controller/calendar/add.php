<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_Add extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/calendar/add')
				->set('csrf', Security::token(TRUE, 'cval/calendar/add'))
				->bind('calendar', $calendar)
				->bind('default_type_name', $default_type_name)
				->bind('default_type_description', $default_type_description);
		
		$calendar = Jelly::factory('calendar');
		
		$default_type_name = Cval_Calendar::default_type('name');
		
		$action_description = Request::factory(Route::get('calendar')->uri(array(
			'controller' => $default_type_name.'_add',
			'action' => 'type_description'
		)))->execute();
		
		$default_type_description = $action_description->response['html'];
		
		$this->request->response = array(
			'html' => $html->render(),
		);
	}

}

