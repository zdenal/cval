<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_Refresh
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_Refresh extends Controller_Cval_Ajax {
	
	/**
	 * @var Model_Calendar_Default
	 */
	protected $_calendar;
	
	/**
	 * @var Model_Calendar
	 */
	protected $_base_calendar;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$this->_base_calendar = Jelly::select('calendar')
				->load(Arr::get($_POST, 'id', 0));

		if ( ! $this->_base_calendar->loaded())
		{
			throw Model_Not_Found_Exception::create($this->_base_calendar->meta()->model(), Arr::get($_POST, 'id', 0));
		}
		
		$this->_calendar = $this->_base_calendar->subclass();
		
		if ( ! $this->_calendar->loaded())
		{
			throw new Kohana_Exception('Subclass model `:submodel` for model `:model` with ID `:id` does not exist', array(
				':model' => $this->_base_calendar->meta()->model(),
				':id' => $this->_base_calendar->id(),
				':submodel' => $this->_calendar->meta()->model()
			));
		}

		$this->_acl_resource = $this->_base_calendar;
	}
	
	public function action_index() 
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$this->_base_calendar->refresh(TRUE, FALSE);
		
		$this->request->response = array(
			'base_calendar' => Cval_REST_Model::get($this->_base_calendar),
			'calendar' => Cval_REST_Model::get($this->_calendar),
			'refreshed_at' => Cval_Date::format($this->_base_calendar->refreshed_at, FALSE, TRUE)
		);
	}

}

