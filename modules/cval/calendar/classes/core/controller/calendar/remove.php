<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Calendar_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendar_Remove extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to delete, `id` parameter is empty');
		}
		
		$ids = $id;
		$multi_delete = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_delete = TRUE;
		}
		
		$not_found = array();
		
		Database::instance()->transaction_start();
		
		foreach ($ids as $id)
		{
			$calendar = Jelly::select('calendar')
					->where('user', '=', $this->cval->user()->id())
					->load($id);
			
			if ( ! $calendar->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			// So delete simple calendar object
			$calendar->delete();
		}
		
		$message = $multi_delete ? Cval::i18n('Calendars were removed')
					: Cval::i18n('Calendar was removed');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
		
		Database::instance()->transaction_commit();
	}

}

