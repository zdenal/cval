<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Calendars Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendars extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Calendars',
		));
		
		$this->request->response = array(
			'html' => View::factory('cval/content/calendars')->render(),
			'active_menu' => 'calendars'
		);
	}

}

