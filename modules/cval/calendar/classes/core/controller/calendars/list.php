<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Calendars List Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendars_List extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'My calendars list',
		));
		
		$html = View::factory('cval/content/calendars/list/content')
				->bind('table', $table)
				->bind('calendars_count', $calendars_count);
		
		$action_table = Request::factory(Route::get('calendar')->uri(array(
			'controller' => 'list_table'
		)))->execute();
		
		$table = $action_table->response['html'];
		$calendars_count = $action_table->response['count'];
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'calendars'
		);
	}

}

