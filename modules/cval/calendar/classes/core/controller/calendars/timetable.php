<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Calendars Timetable Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Calendars_Timetable extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'My calendars timetable',
		));
		
		$html = View::factory('cval/content/calendars/timetable/content');
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'calendars',
			'refresh_interval' => $this->_refresh_interval()
		);
	}
	
	public function action_events() 
	{	
		// Set start and end to event, so timezones are solved
		$event_object = Jelly::factory('event')
			->set(array(
				'start' => Arr::get($_POST, 'start'),
				'end' => Arr::get($_POST, 'end'),
			));
		$start_field = $event_object->meta()->fields('start');
		$end_field = $event_object->meta()->fields('end');
		
		$events = $this->cval->user()->events($event_object->start, $event_object->end);
		
		$cval_events = $this->cval->user()->cval_events($event_object->start, $event_object->end, Arr::get($_POST, 'cval_events_exclude'));
		foreach ($cval_events as $event)
		{
			$events[] = $event;
		}
		
		foreach ($events as $key => $event)
		{
			$events[$key] = array(
				'id' => $event['idmpk'],
				// Do not move timestamps
				/*'start' => $start_field->to_display_timezone($event['start']),
				'end' => $end_field->to_display_timezone($event['end']),*/
				// Fullcalendar supports ISO 8601 format 
				'start' => Date::format($event['start'], 'c'),
				'end' => Date::format($event['end'], 'c'),
				'title' => Arr::get($event, 'title', '')
			) + $event['color'];
		}
		
		// Do not add system response
		$this->add_system_response = FALSE;
		$this->request->response = array(
			'events' => $events,
			'refresh_interval' => $this->_refresh_interval()
		);
	}
	
	protected function _refresh_interval()
	{
		$refresh_interval = Cval_Calendar_Timer::refresh_interval();
		$refresh_interval[0] = Date::format(Cval_Date::to_timezone($refresh_interval[0]));
		$refresh_interval[1] = Date::format(Cval_Date::to_timezone($refresh_interval[1]));
		
		return $refresh_interval;
	}

}

