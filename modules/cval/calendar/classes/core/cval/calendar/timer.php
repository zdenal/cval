<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Calendar Timer
 * @package Cval
 * @author	zdennal
 */
class Core_Cval_Calendar_Timer
{
	/**
	 * @var	array	Refresh interval storage used in self::refresh_interval()
	 */
	protected static $_refresh_interval;
	
	/**
	 * @var	array	Refresh length in seconds self::refresh_length()
	 */
	protected static $_refresh_length;
	
	/**
	 * Gets actual interval for updating calendars
	 * 
	 * @return	array	Array of start and end interval in timestamp format
	 */
	public static function refresh_interval()
	{
		if (Cval_Calendar_Timer::$_refresh_interval === NULL)
		{
			$config = Kohana::config('cval/calendar/timer.refresh');
			
			// Get length
			$length = Arr::get($config, 'length');
			// Check length
			if (empty($length) OR ! is_array($length))
			{
				throw new Kohana_Exception('Refresh config length parameter not valid');
			}
			// Get period
			$period = Arr::get($config, 'period');
			// Check length
			if (empty($period) OR ! is_int($period))
			{
				throw new Kohana_Exception('Refresh config period parameter not valid');
			}
			
			// Get actual time
			$time = Date::time();
			// Get start of interval, actual time rounded to previous period
			$start = (int) (floor($time / $period) * $period);
			// Move start about period
			$period_end = Date::move($start, $period, 'second');
			// Add tmp end
			array_unshift($length, $period_end);
			// Calculate total end
			$end = (int) call_user_func_array('Date::move', $length);
			
			Cval_Calendar_Timer::$_refresh_interval = array($start, $end);
		}
		
		return Cval_Calendar_Timer::$_refresh_interval;
	}
	
	/**
	 * Gets refresh length for updating calendars
	 * 
	 * @return	int		Refresh length in seconds
	 */
	public static function refresh_length()
	{
		if (Cval_Calendar_Timer::$_refresh_length === NULL)
		{
			$config = Kohana::config('cval/calendar/timer.refresh');
			
			// Get length
			$length = Arr::get($config, 'length');
			// Check length
			if (empty($length) OR ! is_array($length))
			{
				throw new Kohana_Exception('Refresh config length parameter not valid');
			}
			
			// Get actual time
			$time = Date::time();
			// Add time to start config
			array_unshift($length, $time);
			// Get difference for start limit from now in seconds
			$length = call_user_func_array('Date::move', $length) - $time;
			
			Cval_Calendar_Timer::$_refresh_length = $length;
		}
		
		return Cval_Calendar_Timer::$_refresh_length;
	}
	
}