<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Calendar Color
 * @package Cval
 * @author	zdennal
 */
class Core_Cval_Calendar_Color
{	
	/**
	 * @var	array	Array of colors for using in fullcalendar
	 */
	protected static $_colors;
	
	/**
	 * Gets colors choices used for select html element
	 * 
	 * @return	array
	 */
	public static function colors_choices()
	{
		$choices = array();
		foreach (Cval_Calendar_Color::colors() as $key => $config)
		{
			$choices[$key] = $config['color'];
		}
		return $choices;
	}
	
	/**
	 * Returns colors config array
	 * 
	 * @return	array 
	 */
	public static function colors()
	{
		if (Cval_Calendar_Color::$_colors === NULL)
		{
			$colors = Cval_Calendar_Color::config('colors');
			
			$colors_array = array();
			foreach ($colors as $key => $config)
			{
				if (is_int($key))
				{
					$key = $config;
					$config = NULL;
				}
				if (empty($config))
				{
					$config = array();
				}
				$config = array_merge(array(
					'color' => '#'.$key,
					'textColor' => '#FFFFFF'
				), $config);
				
				$colors_array[$key] = $config;
			}
			
			Cval_Calendar_Color::$_colors = $colors_array;
		}
		return Cval_Calendar_Color::$_colors;
	}
	
	/**
	 * Returns color config array
	 * 
	 * @return	array 
	 */
	public static function color($key, $alias = FALSE)
	{
		$colors = Cval_Calendar_Color::colors();
		if ( ! $alias)
		{
			return Arr::get($colors, $key, array());
		}
		
		foreach ($colors as $color)
		{
			if (Arr::get($color, 'alias') == $key)
			{
				return $color;
			}
		}
		return array();
	}
	
	/**
	 * Returns config key or all config array.
	 * 
	 * @return	array
	 */
	public static function config($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			return (array) Kohana::config('cval/calendar/color');
		}
		return Arr::path((array) Kohana::config('cval/calendar/color'), $key, $default);
	}
	
}