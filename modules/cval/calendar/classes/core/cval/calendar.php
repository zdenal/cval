<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Calendar
 * @package Cval
 * @author	zdennal
 */
class Core_Cval_Calendar
{
	
	/**
	 * Types
	 *
	 * @var array
	 */
	protected static $_types;

	/**
	 * Return calendar types.
	 *
	 * @param   string  key for associative keys
	 * @param   string  key for values
	 * @param   string  default value for in key value not exists
	 * @return  array
	 * @uses	Arr::remake()
	 */
	public static function types($key = NULL, $value = NULL, $default = NULL)
	{
		if (Cval_Calendar::$_types === NULL)
		{
			$types = Kohana::config('cval/calendar.types');

			foreach ($types as $name => $type)
			{
				$types[$name] = Cval_Calendar::_init_type($name, $type);
			}

			Cval_Calendar::$_types = $types;
		}

		return Arr::remake(Cval_Calendar::$_types, $key, $value, $default);
	}

	/**
	 * Return calendar type.
	 *
	 * @param	string	name
	 */
	public static function type($id, $key = NULL, $value = NULL, $default = NULL)
	{
		$data = Arr::get(Cval_Calendar::types($key), $id);

		if ($value === NULL)
		{
			return $data;
		}

		return Arr::get($data, $value, $default);
	}
	
	/**
	 * Return calendar default type.
	 */
	public static function default_type($value = NULL, $default = NULL)
	{
		$types = Cval_Calendar::types();
		$data = reset($types);

		if ($value === NULL)
		{
			return $data;
		}

		return Arr::get($data, $value, $default);
	}

	/**
	 * Fills all needed configs to type config array.
	 * 
	 * @return	array	Initiated config for passed type
	 */
	protected static function _init_type($name, array $config)
	{
		// Set name to config keys
		$config['name'] = $name;
		
		if (empty($config['id']))
		{
			throw new Kohana_Exception('Calendar type `:name` has no `id` config parameter', array(
				':name' => Arr::get($config, 'name')
			));
		}
		
		if (empty($config['model']))
		{
			$config['model'] = $name;
		}

		return $config;
	}
	
}