<?php defined('SYSPATH') OR die('No direct script access.');

class REST_Model_Calendar extends Jelly_REST_Model 
{
	public function render(array $fields = array())
	{
		$data = parent::render($fields);

		$data['calendar_type'] = $this->_jelly_model->calendar_type();

		return $data;
	}
}

