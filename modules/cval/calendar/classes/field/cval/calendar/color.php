<?php defined('SYSPATH') or die('No direct script access.');

class Field_Cval_Calendar_Color extends Field_Enum
{
	/**
	 * Translate and ucfirst choices if set so
	 *
	 * @param  array $options
	 */
	public function __construct($options = array())
	{
		$options['choices'] = Cval_Calendar_Color::colors_choices();
		$options['default'] = Cval_Calendar_Color::config('default');
		
		parent::__construct($options);
		
		$this->rules['max_length'] = array(6);
	}
}