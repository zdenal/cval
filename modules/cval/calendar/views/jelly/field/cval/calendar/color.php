<div class="Cval-calendar-color-edit-box">
	<?php foreach (Cval_Calendar_Color::colors() as $key => $config): ?>
	<div class="Cval-calendar-color-edit-item Cval-calendar-color-edit-item-id-<?php echo $key ?> Cval-helper-float-left<?php echo ($key == $value) ? ' Cval-calendar-color-edit-item-selected' : '' ?>" style="background-color: <?php echo $config['color'] ?>; color: <?php echo $config['textColor'] ?>;">
		<?php echo ($key == $value) ? '&bull;' : '' ?>
	</div>
	<?php endforeach; ?>
</div>
<input type="hidden" name="color" value="<?php echo $value ?>"/>