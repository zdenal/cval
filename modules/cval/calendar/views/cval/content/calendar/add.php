<div class="jWizard">
	<div class="Cval-dialog-calendar-add-type">
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live')) ?>
			<?php echo Form::hidden('csrf', $csrf) ?>
		
			<h3><?php echo Cval::i18n('Select calendar type') ?></h3>
			<div class="clear"></div>
			<div class="field grid_16">
				<?php echo $calendar->input('calendar_type_id', NULL, array(
					'choices' => Cval_Calendar::types('name', 'label'),
					'attributes' => array(
						'class' => 'Cval-calendar-add-type'
					)
				)) ?>
			</div>
			<div class="clear"></div>
			<div class="Cval-calendar-add-type-description-wrapper grid_16 Cval-helper-margin-top-50">
				<div class="Cval-calendar-add-type-description-name-<?php echo $default_type_name ?>">
					<?php echo $default_type_description ?>
				</div>
			</div>
			<div class="clear"></div>
			
		<?php echo Form::close(); ?>
	</div>
	<div class="Cval-dialog-calendar-add-settings"></div>
</div>