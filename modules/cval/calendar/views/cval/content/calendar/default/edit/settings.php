<div class="jWizard">
	<div class="Cval-dialog-calendar-edit-settings">
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live')) ?>
	
			<?php echo Form::hidden('csrf', $csrf) ?>

			<?php echo $specific_view->include_view($thiss) ?>

		<?php echo Form::close(); ?>
	</div>
</div>