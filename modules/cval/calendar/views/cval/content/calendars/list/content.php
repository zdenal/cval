<div class="container_16">
	
	<div class="Cval-list-calendars-message-positions-content ui-helper-hidden">
		<?php echo View::factory('cval/content/calendars/list/message/positions')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-calendars-message-nocalendar-content ui-helper-hidden">
		<?php echo View::factory('cval/content/calendars/list/message/nocalendar')->include_view($thiss) ?>
	</div>
	
	<div class="grid_16 Cval-list-calendars-info-box Cval-helper-margin-top"></div>
	
	<div class="grid_16 Cval-list-calendars-wrapper<?php echo $calendars_count ? '' : ' ui-helper-hidden' ?>">
		<?php echo $table ?>
	</div>
		
	<div class="clear"></div>
</div>