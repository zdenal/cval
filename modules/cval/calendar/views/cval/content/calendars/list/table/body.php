<?php foreach ($calendars as $calendar): ?>
<tr class="<?php echo $calendar->html_class() ?>">
	<td class="Cval-column-color">
		<div class="Cval-calendar-color-list-item" style="background-color: <?php echo '#'.$calendar->color ?>;"></div>
	</div>
	</td>
	<td class="Cval-column-label">
		<div><?php echo $calendar->label ?></div>
	</td>
	<td class="Cval-column-type"><?php echo Arr::get($calendar->meta()->fields('calendar_type_id')->choices, $calendar->calendar_type_id) ?></td>
	<td class="Cval-column-refreshed_at">
		<div class="refreshed">
			<span class="ui-icon ui-icon ui-icon-arrowrefresh-1-e Cval-helper-float-left Cval-trigger-refresh" title="<?php echo Cval::i18n('Refresh') ?>"></span>
			<span class="text"><?php $refreshed_at = Cval_Date::format($calendar->refreshed_at, FALSE, TRUE); echo empty($refreshed_at) ? Cval::i18n('Never') : $refreshed_at ?></span>
		</div>
		<div class="refreshing ui-helper-hidden">
			<?php echo Cval::i18n('Is refreshing') ?>
		</div>
	</td>
	<td class="Cval-column-actions">
		<span class="ui-icon ui-icon-trash Cval-helper-float-right Cval-trigger-remove" title="<?php echo Cval::i18n('Remove') ?>"></span>
		<span class="ui-icon ui-icon-arrow-4 Cval-helper-float-right Cval-trigger-move" title="<?php echo Cval::i18n('Move') ?>"></span>
		<span class="ui-icon ui-icon-pencil Cval-helper-float-right Cval-trigger-edit" title="<?php echo Cval::i18n('Setting') ?>"></span>
	</td>
</tr>
<?php endforeach; ?>