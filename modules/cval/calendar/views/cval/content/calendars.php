<div class="container_16">
	
	<div class="grid_6 Cval-page-label">
		<?php echo Cval::i18n($page_title) ?>
	</div>
	<div class="grid_10 Cval-helper Cval-menu-id-calendars" style="text-align: right">
		<span class="Cval-calendar-add Cval-helper-float-right"><?php echo Cval::i18n('Add calendar') ?></span>
		<span class="Cval-helper-float-right Cval-helper-margin-right-50">
			<span class="Cval-calendar-trigger-page Cval-menu-item-calendars_timetable Cval-helper-float-right" style="display: none;"><?php echo Cval::i18n('Switch to timetable') ?></span>
			<span class="Cval-calendar-trigger-page Cval-menu-item-calendars_list Cval-helper-float-right" style="display: none;"><?php echo Cval::i18n('Switch to list') ?></span>
		</span>
	</div>
	
	<div class="clear"></div>
</div>

<div class="Cval-page-id-calendars-content"></div>