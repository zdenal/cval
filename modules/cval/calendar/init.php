<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('calendars_main', 'calendars')
        ->defaults(array(
            'controller' => 'calendars',
        ))->config_set('route_dash', 'page');

Route::set('calendars', 'calendars/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'calendars',
        ))->config_set('route_dash', 'page');

Route::set('calendar/global', 'calendars/<action>')
        ->defaults(array(
            'controller' => 'calendar',
        ));

Route::set('calendar', 'calendar/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'calendar',
        ));

