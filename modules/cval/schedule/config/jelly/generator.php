<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	'models' => array(
		'schedule',
		'schedule_group',
		'schedule_plan',
		'schedule_plan_period',
		'schedule_plan_negotiated_period',
		'schedule_user_profile_shared',
		'schedule_request',
	)

);

