<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(

	'templates' => array(
		'schedule/inbound/received' => array(
			'from' => 'scheduler',
			'view' => 'email/templates/cval/schedule/inbound/received',
			'html' => FALSE
		),
	)

);