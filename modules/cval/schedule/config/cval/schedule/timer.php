<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	
	'limit' => array(
		/**
		 * Length interval for limiting starting schedule date, i.e. all
		 * schedule periods must start at least after this length from now. 
		 * Needs to be an array which is used in Date::move() method.
		 */
		'start' => array(1, 'hour'), // 1 hour
		/**
		 * Length interval for limiting ending schedule date, i.e. all
		 * schedule periods must end at most before this length from now. 
		 * Needs to be an array which is used in Date::move() method, or it
		 * can be TRUE, than it is used value from config 
		 * cval/calendar/timer.refresh.length.
		 */
		'end' => TRUE // Use value from config cval/calendar/timer.refresh.length
	),
	
	'request' => array(
		/**
		 * Lifetime of requested inbound schedules. If schedule is received
		 * and its request does not exists or is older than lifetime, schedule
		 * is denied. 
		 * It needs to be in seconds.
		 */
		'lifetime' => 1800 // 30 minutes
	)

);

