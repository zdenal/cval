<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('schedules_main', 'schedules')
        ->defaults(array(
            'controller' => 'schedules',
        ))->config_set('route_dash', 'page');

Route::set('schedules', 'schedules/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'schedules',
        ))->config_set('route_dash', 'page');

Route::set('schedule_main', 'schedule')
        ->defaults(array(
            'controller' => 'schedule',
        ))->config_set('route_dash', 'page');

Route::set('schedule_download', 'schedule/download/<id>/<format>')
        ->defaults(array(
			'directory' => 'schedule',
            'controller' => 'download',
        ));

Route::set('schedule', 'schedule/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'schedule',
        ));

Route::set('schedule_group', 'schedule_group/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'schedule/group',
        ));

