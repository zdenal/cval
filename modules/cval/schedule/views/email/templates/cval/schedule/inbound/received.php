Title:			<?php echo $schedule->label."\n" ?>
When:			<?php echo Cval_Date::format($schedule->start, TRUE) ?> - <?php echo Cval_Date::format($schedule->end, TRUE)."\n" ?>
Organizer:		<?php echo $schedule->organizer->name()."\n" ?>
Guests (<?php echo $guests_count ?>):		<?php echo $guests[0]->name()."\n" ?>
<?php for ($i = 1; $i < $guests_count; $i++): ?>
				<?php echo $guests[$i]->name()."\n" ?>
<?php endfor; ?>
<?php if ($guests_count > $i): ?>
				<?php echo '(+'.($guests_count - $i).' more)'."\n" ?>
<?php endif; ?>
<?php if (strlen($schedule->place)): ?>
Where:			<?php echo $schedule->place."\n" ?>
<?php endif; ?>
<?php if (strlen($schedule->description)): ?>
Description:	
<?php echo $schedule->description."\n" ?>
<?php endif; ?>

You can review this schedule at:
<?php echo $schedule_url."\n" ?>

<?php echo View::factory('cval/email/footer/text')->include_view($thiss) ?>