<table cellspacing="0" cellpadding="0" class="Cval-list Cval-list-schedules">
	<thead>
		<tr>
			<th class="Cval-column-listcheck"><div></div></th>
			<th class="Cval-column-organizer"><div><?php echo Cval::i18n('Organizer') ?></div></th>
			<th class="Cval-column-label"><div><?php echo Cval::i18n('Title') ?></div></th>
			<th class="Cval-column-contacts"><div><?php echo Cval::i18n('Guests') ?></div></th>
			<th class="Cval-column-date"><div><?php echo Cval::i18n('Date') ?></div></th>
			<th class="Cval-column-actions"><div></div></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>