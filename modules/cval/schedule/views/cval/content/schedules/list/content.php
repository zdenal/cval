<div class="container_16 full">
	
	<!--<div class="grid_16 full">
		<div class="Cval-helper-float-right">
			<span class="Cval-paginator"></span>
			<span class="Cval-button Cval-button-live Cval-schedule-actions"><?php echo Cval::i18n('Actions') ?></span>
		</div>
	</div>
	<div class="clear"></div>-->
	
	<div class="Cval-list-schedules-message-positions-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/positions')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule_inbound-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule/inbound')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule_outbound-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule/outbound')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule_draft-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule/draft')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule_all-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule/all')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule_favorite-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule/favorite')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-schedules-message-noschedule_unsync-content ui-helper-hidden">
		<?php echo View::factory('cval/content/schedules/list/message/noschedule/unsync')->include_view($thiss) ?>
	</div>
	
	<div class="grid_16 full Cval-list-schedules-info-box Cval-helper-margin-top-50"></div>
	
	<div class="grid_16 full Cval-list-wrapper Cval-list-schedules-wrapper">
		<?php echo $table ?>
	</div>
		
	<div class="clear"></div>
</div>