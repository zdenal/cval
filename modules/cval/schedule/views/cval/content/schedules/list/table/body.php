<?php foreach ($schedules as $schedule): ?>
<tr class="<?php echo $schedule->html_class() ?><?php echo $schedule->is_unread() ? ' Cval-helper-bold' : '' ?>">
	<td class="Cval-column-listcheck"><input type="checkbox" class="Cval-list-checkbox"/></td>
	<td class="Cval-column-organizer">
		<?php if ($schedule->is_draft()): ?>
		<div class="ui-state-error-text"><?php echo Cval::i18n('Draft') ?></div>
		<?php else: ?>
		<div title="<?php echo $schedule->organizer->location->name() ?>"><?php echo $schedule->organizer->friendly_name() ?></div>
		<?php endif; ?>
	</td>
	<td class="Cval-column-label">
		<div><?php echo $schedule->name() ?></div>
	</td>
	<td class="Cval-column-contacts">
		<?php if ($guest_count = $schedule->schedule_user_profiles_shared->count()): ?>
			<?php foreach ($schedule->schedule_user_profiles_shared as $guest): ?>
			<span title="<?php echo $guest->user_profile_shared->location->name() ?>"><?php echo $guest->friendly_name(); break; ?></span>
			<?php endforeach; ?>
			<?php if ($guest_count > 1): ?>
			(+<?php echo $guest_count-1; ?>)
			<?php endif; ?>
		<?php endif; ?>
	</td>
	<td class="Cval-column-date">
		<?php if ($schedule->has_date()): ?>
		<span title="<?php echo Cval_Date::format($schedule->start, TRUE) ?> - <?php echo Cval_Date::format($schedule->end, TRUE) ?>">
			<?php echo Cval_Date::format($schedule->start, FALSE, TRUE) ?>
		</span>
		<?php else: ?>
		<span><?php echo '' ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-actions">
		<span class="ui-icon ui-icon-trash Cval-helper-float-right Cval-trigger-remove" title="<?php echo Cval::i18n('Delete') ?>"></span>
		<?php if ($schedule->allow_edit()): ?>
		<span class="ui-icon ui-icon-pencil Cval-helper-float-right Cval-trigger-edit" title="<?php echo Cval::i18n('Edit') ?>"></span>
		<?php endif; ?>
		<?php if ($schedule->check_state_equal(Model_Schedule::STATE_NEGOTIATING)): ?>
		<span class="Cval-icon-loading Cval-helper-float-right Cval-trigger-loading" title="<?php echo Cval::i18n('Cval is getting available time from guests') ?>"></span>
		<?php endif; ?>
		<?php if ($schedule->check_state_equal(Model_Schedule::STATE_SENDING)): ?>
		<span class="Cval-icon-loading Cval-helper-float-right Cval-trigger-loading" title="<?php echo Cval::i18n('Cval is sending this schedule to guests') ?>"></span>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; ?>