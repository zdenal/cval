<ul class="Cval-menu-vertical Cval-menu-vertical-id-schedules<?php echo ! empty($notlive) ? '' : ' Cval-menu-vertical-live'?>">
	<?php foreach ($schedule_groups as $id => $schedule_group): ?>
	<?php $highlighted_schedules_count = $schedule_group->higlighted_schedules_count(Cval::instance()->user()) ?>
	<li class="Cval-menu-item-schedules_group_<?php echo $id ?> <?php echo $schedule_group->html_class() ?><?php echo ($highlighted_schedules_count > 0) ? ' Cval-helper-bold' : '' ?>">
		<a href="<?php echo Route::url_protocol('schedules', array(
			'controller' => 'list'
		)).'&'.http_build_query(array(
			'group' => $id
		)) ?>" class="Cval-link-page"><?php echo $schedule_group->name() ?><?php echo ($highlighted_schedules_count > 0) ? (' ('.$highlighted_schedules_count.')') : '' ?></a></li>
	<?php endforeach; ?>
</ul>