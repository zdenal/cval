<div class="container_16">	
	
	<div class="grid_8 Cval-page-label Cval-helper-margin-bottom">
		<span class="ui-icon ui-icon-calculator ui-state-noclick Cval-helper-float-left Cval-helper-margin-right"></span>
		<div class="Cval-helper-float-left">
			<span class="Cval-schedule-label"><?php echo $page_title ?></span>
			<span class="Cval-helper-margin-left Cval-schedule-target-groups-container Cval-helper-font-size-default"><?php echo implode(', ', $assigned_schedule_groups_labels); ?></span>
		</div>
	</div>
	<div class="grid_8 Cval-helper Cval-menu-horizontal-id-schedule Cval-helper-margin-bottom" style="text-align: right">
		<span class="Cval-schedule-close Cval-trigger-cancel Cval-helper-float-right"><?php echo Cval::i18n('Close') ?></span>
		<span class="Cval-schedule-controls Cval-helper-float-right Cval-helper-margin-right ui-helper-hidden">
			<span class="Cval-schedule-assign-groups"><?php echo Cval::i18n('Assign groups') ?></span>
			<span class="Cval-schedule-remove"><?php echo Cval::i18n('Delete schedule') ?></span>
			<span class="Cval-schedule-download-ics"><?php echo Cval::i18n('Download as iCalendar file') ?></span>
		</span>
	</div>
	<div class="clear"></div>
	
	<?php echo Form::select('target_schedule_groups', $schedule_groups, $selected_schedule_groups, array(
		'class' => 'Cval-schedule-target-groups-select ui-helper-hidden'
	)) ?>
	<?php echo Form::hidden('csrf_groups_assign', $csrf_groups_assign) ?>
	
	<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget Cval-helper-margin-top')) ?>

		<div class="grid_8">
			<div class="field-label">
				<?php echo Cval::i18n('When') ?>
			</div>
			<div title="<?php echo Cval_Date::format($schedule->start, TRUE) ?> - <?php echo Cval_Date::format($schedule->end, TRUE) ?>">
				<?php echo Cval_Date::format($schedule->start, TRUE, TRUE) ?> - <?php echo Cval_Date::format($schedule->end, TRUE, TRUE) ?>
			</div>
			<div class="field-label Cval-helper-margin-top">
				<?php echo Cval::i18n('Title') ?>
			</div>
			<div>
				<?php echo $schedule->label ?>
			</div>
			<?php if (strlen($schedule->place)): ?>
			<div class="field-label Cval-helper-margin-top">
				<?php echo Cval::i18n('Where') ?>
			</div>
			<div>
				<?php echo $schedule->place ?>
			</div>
			<?php endif; ?>
			<?php if (strlen($schedule->description)): ?>
			<div class="field-label Cval-helper-margin-top">
				<?php echo Cval::i18n('Description') ?>
			</div>
			<div>
				<div style="white-space: pre-wrap;"><?php echo $schedule->description ?></div>
			</div>
			<?php endif; ?>
		</div>

		<div class="grid_8">
			<div class="field-label">
				<?php echo Cval::i18n('Organizer') ?>
			</div>
			<div title="<?php echo $schedule->organizer->location->name() ?>">
				<?php echo $schedule->organizer->friendly_name() ?>
			</div>
			<div class="field-label Cval-helper-margin-top">
				<?php echo Cval::i18n('Guests') ?>
			</div>
			<div class="grid_16 full Cval-menu-id-schedule-guests" style="text-align: right">
				<div class="Cval-helper-float-left Cval-schedule-guests-list-controls Cval-helper-margin-bottom ui-helper-hidden">
					<span class="Cval-paginator"></span>
				</div>
			</div>
			<div class="clear"></div>
			<div class="grid_16 full">
				<div class="Cval-schedule-guests-message-noguest-content ui-helper-hidden">
					<?php echo View::factory('cval/content/schedule/guests/list/message/noguest')->include_view($thiss) ?>
				</div>
				<div class="Cval-schedule-guests-info-box"></div>
				<div class="Cval-schedule-guests-wrapper">
					<?php echo $guests_table ?>
				</div>
				&nbsp;
			</div>
		</div>

		<div class="clear"></div>

	<?php echo Form::close(); ?>
	
	<div class="clear"></div>
	
	<div class="grid_16 Cval-timetable-schedule-wrapper Cval-helper-margin-top"></div>
	<div class="clear"></div>
</div>