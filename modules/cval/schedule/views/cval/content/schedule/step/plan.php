
<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget grid_16')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="grid_16 Cval-schedule-step-help Cval-help-box Cval-helper-margin-bottom ui-helper-hidden">
		<div>
			<div class="Cval-h3 Cval-helper-margin-bottom">How to propose times?</div>
			Click on preferred days first. Then limit day time using slider or time fields. Proposed times are blue highlighted. At least one time must be selected in order to continue planning. Cval will check if guests are available on proposed times only. The more times you propose, the better your chances of finding a time that works for everyone!
		</div>
	</div>
	<div class="clear"></div>

	<div class="grid_16">
		<div class="field-label Cval-helper-float-left"><?php echo Cval::i18n('Time range') ?></div>
		<div class="Cval-helper-float-right"><span class="Cval-helper-cursor-pointer Cval-helper-underline Cval-schedule-step-help-trigger"><?php echo Cval::i18n('Need help?') ?></span></div>
	</div>
	<div class="clear"></div>
	<div class="grid_5 Cval-schedule-plan-timerange-text">
		<div class="field Cval-helper-float-left" style="width: 85px">
			<?php echo Form::input('start_time', Cval_Date::format(NULL), array(
				'class' => 'Cval-schedule-plan-timerange-start'
			)) ?>
		</div>
		<span class="Cval-helper-float-left Cval-helper-margin-left-50 Cval-helper-margin-right-50 Cval-helper-margin-top-50"><?php echo Cval::i18n('to') ?></span>
		<div class="field Cval-helper-float-left" style="width: 85px">
			<?php echo Form::input('end_time', Cval_Date::format(NULL), array(
				'class' => 'Cval-schedule-plan-timerange-end'
			)) ?>
		</div>
	</div>
	<div class="grid_7">
		<span class="Cval-helper-float-left Cval-helper-margin-top-50"><?php echo Cval::i18n('Minimum schedule length') ?>:</span>
		<div class="field Cval-helper-float-left Cval-helper-margin-left" style="width: 55px"><?php echo $schedule_plan->input('min_length') ?></div>
		<div class="Cval-helper-float-left Cval-helper-margin-left-50"><?php echo $schedule_plan->input('min_length_units') ?></div>
	</div>
	<div class="clear"></div>
	
<?php echo Form::close(); ?>
	
<div class="grid_16 Cval-helper-margin-top Cval-helper-margin-bottom Cval-schedule-plan-timerange-slider"></div>

<div class="grid_16 Cval-timetable-schedule-plan-wrapper Cval-helper-margin-top"></div>