<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget grid_16')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="field-label grid_10">
		<?php echo Cval::i18n('Title') ?>
		<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
	</div>
	<div class="clear"></div>
	<div class="field grid_10">
		<?php echo $schedule->input('label') ?>
	</div>
	<div class="clear"></div>
	<div class="field-label grid_8">
		<?php echo Cval::i18n('Where') ?>
	</div>
	<div class="clear"></div>
	<div class="field grid_8">
		<?php echo $schedule->input('place') ?>
	</div>
	<div class="clear"></div>
	<div class="field-label grid_8">
		<?php echo Cval::i18n('Description') ?>
	</div>
	<div class="clear"></div>
	<div class="field grid_8">
		<?php echo $schedule->input('description', NULL, array(
			'attributes' => array(
				'style' => 'width: 100%;'
			)
		)) ?>
	</div>
	<div class="clear"></div>

	<div class="grid_16 button-line Cval-helper-margin-top ui-helper-hidden">
		<span class="Cval-trigger-next Cval-button Cval-button-live Cval-helper-float-left Cval-helper-margin-right ui-state-highlight"><?php echo Cval::i18n($schedule->has_plan ? 'Save properties' : 'Save properties and continue to proposing times') ?></span>
		<?php if ( ! $schedule->loaded()): ?>
		<span class="Cval-trigger-cancel Cval-button Cval-button-live Cval-helper-float-left"><?php echo Cval::i18n('Discard') ?></span>
		<?php endif; ?>
	</div>
	<div class="clear"></div>

<?php echo Form::close(); ?>

<div class="clear"></div>