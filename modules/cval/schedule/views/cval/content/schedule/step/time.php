
<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget grid_16')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="grid_16 Cval-schedule-step-help Cval-help-box Cval-helper-margin-bottom ui-helper-hidden">
		<div>
			<div class="Cval-h3 Cval-helper-margin-bottom">How to select schedule time?</div>
			Click on green highlighted time. An event representing the schedule time will appear. You can move or resize the event using your mouse. For precise timing please use slider or time fields.
		</div>
	</div>
	<div class="clear"></div>

	<div class="grid_16">
		<div class="field-label Cval-helper-float-left"><?php echo Cval::i18n('Selected time') ?></div>
		<div class="Cval-helper-float-right"><span class="Cval-helper-cursor-pointer Cval-helper-underline Cval-schedule-step-help-trigger"><?php echo Cval::i18n('Need help?') ?></span></div>
	</div>
	<div class="clear"></div>
	<div class="grid_10 Cval-schedule-time-selected-text">
		<div class="Cval-schedule-time-selected-no Cval-helper-margin-bottom ui-helper-hidden">
			No time selected
		</div>
		<div class="Cval-schedule-time-selected-yes ui-helper-hidden">
			<div class="field Cval-helper-float-left Cval-helper-margin-right Cval-helper-margin-top-50 Cval-schedule-time-selected-startdate"></div>
			<div class="field Cval-helper-float-left" style="width: 85px">
				<?php echo Form::input('start_time', Cval_Date::format(NULL), array(
					'class' => 'Cval-schedule-time-selected-start'
				)) ?>
			</div>
			<span class="Cval-helper-float-left Cval-helper-margin-left-50 Cval-helper-margin-right-50 Cval-helper-margin-top-50"><?php echo Cval::i18n('to') ?></span>
			<div class="field Cval-helper-float-left" style="width: 85px">
				<?php echo Form::input('end_time', Cval_Date::format(NULL), array(
					'class' => 'Cval-schedule-time-selected-end'
				)) ?>
			</div>
			<div class="field Cval-helper-float-left Cval-helper-margin-left Cval-helper-margin-top-50 Cval-schedule-time-selected-enddate"></div>
		</div>
	</div>
	<div class="clear"></div>
	
<?php echo Form::close(); ?>
	
<div class="grid_16 Cval-helper-margin-top Cval-helper-margin-bottom Cval-schedule-time-selected-slider Cval-schedule-time-selected-yes ui-helper-hidden"></div>

<div class="grid_16 Cval-timetable-schedule-time-wrapper Cval-helper-margin-top"></div>