<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget grid_16 Cval-helper-margin-top')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="grid_16">
		<?php if ($schedule->check_state_less(Model_Schedule::STATE_NEGOTIATING)): ?>
		<div>
			<div>Proposed times needs to be checked against guest's time plan.</div>
			<div>Do it now and get free time from guests.</div>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Check availability') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_NEGOTIATING)): ?>
		<div>
			<span class="Cval-icon-loading Cval-helper-float-left Cval-helper-margin-right-50"></span>
			<span class="Cval-helper-float-left">Cval is collecting free time from guests.</span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_NEGOTIATING_FAILED)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but something went wrong.</div>
			<div>Please try it again later.</div>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Check availability once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_NEGOTIATING_NORESPONSE_ORGANIZER)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but YOUR time plan is not responding.</div>
			<div>Please make sure that your calendars are available, then try it again.</div>
			<span class="Cval-schedule-step-negotiate-trigger-calendars Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Show calendars') ?></span>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Calendars are fine, check availability once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_NEGOTIATING_NORESPONSE_GUEST)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but one or more GUEST are not responding.</div>
			<div>Not responding guests are highlighted in guest list. You can remove them from guest list or try it again later.</div>
			<span class="Cval-schedule-step-negotiate-trigger-contacts Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Review guests') ?></span>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Guests are fine, check availability once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_PLAN_FULL_ORGANIZER)): ?>
		<div>
			<div class="ui-state-error-text">Unfortunately no time from your plan is suitable for YOU.</div>
			<div>Try to modify proposed times.</div>
			<span class="Cval-schedule-step-negotiate-trigger-prev Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Modify proposed times') ?></span>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Check availability once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_PLAN_FULL_GUEST)): ?>
		<div>
			<div class="ui-state-error-text">Unfortunately no time from your plan is suitable for GUESTS.</div>
			<div>Try to modify proposed times.</div>
			<span class="Cval-schedule-step-negotiate-trigger-prev Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Modify proposed times') ?></span>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Check availability once more') ?></span>
		</div>
		<?php else: ?>
		<div>
			<div class="ui-state-highlight-text">Free time is known.</div>
			<div>Select your preferred time for meeting.</div>
			<span class="Cval-schedule-step-negotiate-trigger-next Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Select time') ?></span>
			<span class="Cval-schedule-step-negotiate-trigger-negotiate Cval-helper-margin-top"><?php echo Cval::i18n('Check availability once more') ?></span>
		</div>
		<?php endif; ?>
		<div class="clear"></div>
	</div>
	
<?php echo Form::close(); ?>