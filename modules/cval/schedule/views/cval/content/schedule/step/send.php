<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_16 ui-widget Cval-helper-margin-top')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="grid_16 Cval-helper-margin-bottom">
		<?php if ($schedule->check_state_less(Model_Schedule::STATE_TIME_CHECKING)): ?>
		<div>
			<div>Schedule was not sent to guests yet.</div>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Send schedule') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_CHECKING)): ?>
		<div>
			<span class="Cval-icon-loading Cval-helper-float-left Cval-helper-margin-right-50"></span>
			<span class="Cval-helper-float-left">Cval is checking if guests are available on scheduled time.</span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_CHECKING_FAILED)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but something went wrong during availability check.</div>
			<div>Please try it again later.</div>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Try to send schedule once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_CHECKING_NORESPONSE_ORGANIZER)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but YOUR time plan is not responding.</div>
			<div>Please make sure that your calendars are available, then try it again.</div>
			<span class="Cval-schedule-step-send-trigger-calendars Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Show calendars') ?></span>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Calendars are fine, try to send schedule once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_CHECKING_NORESPONSE_GUEST)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but one or more GUEST are not responding.</div>
			<div>Not responding guests are highlighted in guest list. You can remove them from guest list or try it again later.</div>
			<span class="Cval-schedule-step-send-trigger-contacts Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Review guests') ?></span>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Guests are fine, try to send schedule once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_FULL_ORGANIZER)): ?>
		<div>
			<div class="ui-state-error-text">Unfortunately schedule time is not suitable for YOU.</div>
			<div>Try to select another time or modify proposed times.</div>
			<span class="Cval-schedule-step-send-trigger-prev Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Select another time') ?></span>
			<span class="Cval-schedule-step-send-trigger-plan Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Modify proposed times') ?></span>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Try to send schedule once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_TIME_PLAN_FULL_GUEST)): ?>
		<div>
			<div class="ui-state-error-text">Unfortunately schedule time is not suitable for GUESTS.</div>
			<div>Try to select another time or modify proposed times.</div>
			<span class="Cval-schedule-step-send-trigger-prev Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Select another time') ?></span>
			<span class="Cval-schedule-step-send-trigger-plan Cval-helper-margin-top Cval-helper-margin-right"><?php echo Cval::i18n('Modify proposed times') ?></span>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Try to send schedule once more') ?></span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_SENDING)): ?>
		<div>
			<span class="Cval-icon-loading Cval-helper-float-left Cval-helper-margin-right-50"></span>
			<span class="Cval-helper-float-left">Cval is sending schedule to guests.</span>
		</div>
		<?php elseif($schedule->check_state_equal(Model_Schedule::STATE_SENDING_FAILED)): ?>
		<div>
			<div class="ui-state-error-text">Sorry, but something went wrong during sending.</div>
			<div>Please try it again later.</div>
			<span class="Cval-schedule-step-send-trigger-time_check Cval-helper-margin-top"><?php echo Cval::i18n('Try to send schedule once more') ?></span>
		</div>
		<?php else: ?>
		<div>
			<?php // This should not be visible, because page redirect to detail instead ?>
			<div class="ui-state-highlight-text">Schedule was send.</div>
		</div>
		<?php endif; ?>
		<div class="clear"></div>
	</div>

	<div class="grid_8">
		<div class="field-label">
			<span class="Cval-helper-float-left"><?php echo Cval::i18n('Title') ?></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-schedule-step-send-trigger-properties" title="<?php echo Cval::i18n('Modify title') ?>"></span>
			<div class="clear"></div>
		</div>
		<div>
			<?php echo $schedule->label ?>
		</div>
		<div class="clear"></div>
		<div class="field-label Cval-helper-margin-top">
			<span class="Cval-helper-float-left"><?php echo Cval::i18n('When') ?></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-schedule-step-send-trigger-time" title="<?php echo Cval::i18n('Select another time') ?>"></span>
			<div class="clear"></div>
		</div>
		<div title="<?php echo Cval_Date::format($schedule->start, TRUE) ?> - <?php echo Cval_Date::format($schedule->end, TRUE) ?>">
			<?php echo Cval_Date::format($schedule->start, TRUE, TRUE) ?> - <?php echo Cval_Date::format($schedule->end, TRUE, TRUE) ?>
		</div>
		<div class="clear"></div>
		<div class="field-label Cval-helper-margin-top">
			<span class="Cval-helper-float-left"><?php echo Cval::i18n('Where') ?></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-schedule-step-send-trigger-properties" title="<?php echo Cval::i18n('Modify place') ?>"></span>
			<div class="clear"></div>
		</div>
		<div>
			<?php if ( ! strlen($schedule->place)): ?>
			<span class="field-description"><?php echo Cval::i18n('Unknown') ?></span>
			<?php else: ?>
			<?php echo $schedule->place ?>
			<?php endif; ?>
		</div>
		<div class="field-label Cval-helper-margin-top">
			<span class="Cval-helper-float-left"><?php echo Cval::i18n('Description') ?></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-schedule-step-send-trigger-properties" title="<?php echo Cval::i18n('Modify description') ?>"></span>
			<div class="clear"></div>
		</div>
		<div>
			<?php if ( ! strlen($schedule->description)): ?>
			<span class="field-description"><?php echo Cval::i18n('None') ?></span>
			<?php else: ?>
			<div style="white-space: pre-wrap;"><?php echo $schedule->description ?></div>
			<?php endif; ?>
		</div>
	</div>

	<div class="grid_8">
		<div class="field-label">
			<?php echo Cval::i18n('Organizer') ?>
		</div>
		<div title="<?php echo $schedule->organizer->location->name() ?>">
			<?php echo $schedule->organizer->friendly_name() ?>
		</div>
		<div class="field-label Cval-helper-margin-top">
			<span class="Cval-helper-float-left"><?php echo Cval::i18n('Guests') ?></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-schedule-step-send-icon-trigger-contacts" title="<?php echo Cval::i18n('Modify guests') ?>"></span>
			<div class="clear"></div>
		</div>
		<div class="grid_16 full Cval-menu-id-schedule-guests" style="text-align: right">
			<div class="Cval-helper-float-left Cval-schedule-guests-list-controls Cval-helper-margin-bottom ui-helper-hidden">
				<span class="Cval-paginator"></span>
			</div>
		</div>
		<div class="clear"></div>
		<div class="grid_16 full">
			<div class="Cval-schedule-guests-message-noguest-content ui-helper-hidden">
				<?php echo View::factory('cval/content/schedule/guests/list/message/noguest')->include_view($thiss) ?>
			</div>
			<div class="Cval-schedule-guests-info-box"></div>
			<div class="Cval-schedule-guests-wrapper">
				<?php echo $guests_table ?>
			</div>
			&nbsp;
		</div>
	</div>

	<div class="clear"></div>

<?php echo Form::close(); ?>

<div class="clear"></div>