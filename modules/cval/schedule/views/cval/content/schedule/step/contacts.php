<?php echo Form::hidden('csrf', $csrf) ?>
<div>
	<div class="grid_9 Cval-helper-margin-bottom Cval-helper-margin-top">
		<div class="Cval-helper-float-left">
			<span class="Cval-h3"><?php echo Cval::i18n('Contacts') ?></span>
		</div>
		<div class="Cval-helper-float-right" style="width: 150px">
			<?php echo Form::select('contact_group', $contact_groups_choices, $selected_contact_group_id, array(
				'class' => 'Cval-schedule-contacts-search-contact_groups-select'
			)) ?>
		</div>
		<div class="Cval-helper-font-weight-bold Cval-helper-float-right Cval-helper-margin-right" style="padding-top: 3px;">
			<?php echo Cval::i18n('Group') ?>
		</div>
	</div>
	<div class="grid_1 Cval-helper-margin-top">&nbsp;</div>
	<div class="grid_6 Cval-helper-margin-bottom Cval-helper-margin-top">
		<div class="Cval-helper-float-left">
			<span class="Cval-h3"><?php echo Cval::i18n('Guests') ?></span>
		</div>
	</div>
	<div class="clear"></div>
	<div class="grid_9 Cval-menu-id-schedule-contacts-search" style="text-align: right">
		<div class="Cval-helper-float-left Cval-schedule-contacts-search-list-controls ui-helper-hidden">
			<span class="Cval-selectall"></span>
			<span class="Cval-paginator"></span>
		</div>
		<div class="Cval-helper-float-right">
			<span class="Cval-schedule-contacts-search-controls">
				<span class="Cval-contact-add"><?php echo Cval::i18n('Add contact') ?></span>
			</span>
		</div>
		<div class="Cval-helper-float-right" style="width: 170px; margin-right: 1%">
			<?php echo View::factory('cval/util/search/input'); ?>
		</div>
	</div>
	<div class="grid_1">&nbsp;</div>
	<div class="grid_6 Cval-menu-id-schedule-contacts-guests" style="text-align: right">
		<div class="Cval-helper-float-left Cval-schedule-contacts-guests-list-controls ui-helper-hidden">
			<span class="Cval-selectall"></span>
			<span class="Cval-paginator"></span>
		</div>
		<div class="Cval-helper-float-right">
			<span class="Cval-schedule-contacts-guests-controls">
				<span class="Cval-guest-remove"><?php echo Cval::i18n('Remove from guests') ?></span>
			</span>
		</div>
		<div class="Cval-helper-float-right ui-helper-hidden" style="width: 125px; margin-right: 1%">
			<?php echo View::factory('cval/util/search/input'); ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="grid_9 full">
		<div class="Cval-schedule-contacts-search-message-nocontact-content ui-helper-hidden">
			<?php echo View::factory('cval/content/schedule/contacts/search/list/message/nocontact')->include_view($thiss) ?>
		</div>
		<div class="Cval-schedule-contacts-search-info-box"></div>
		<div class="Cval-schedule-contacts-search-wrapper">
			<?php echo $contacts_table ?>
		</div>
		&nbsp;
	</div>
	<div class="grid_1">
		<div class="Cval-schedule-contacts-search-to-guests-controls">
			<div class="Cval-contact-add"><?php echo Cval::i18n('Add selected contacts to guests') ?></div>
			<div class="Cval-contact-addall Cval-helper-margin-top"><?php echo Cval::i18n('Add all contacts to guests') ?></div>
		</div>
	</div>
	<div class="grid_6 full">
		<div class="Cval-schedule-contacts-guests-message-noguest-content ui-helper-hidden">
			<?php echo View::factory('cval/content/schedule/contacts/guests/list/message/noguest')->include_view($thiss) ?>
		</div>
		<div class="Cval-schedule-contacts-guests-message-notrespondingguest-content ui-helper-hidden">
			<?php echo View::factory('cval/content/schedule/contacts/guests/list/message/notrespondingguest')->include_view($thiss) ?>
		</div>
		<div class="Cval-schedule-contacts-guests-info-box"></div>
		<div class="Cval-schedule-contacts-guests-wrapper">
			<?php echo $guests_table ?>
		</div>
		&nbsp;
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>