<div class="container_16">	
	
	<div class="grid_8 Cval-page-label Cval-helper-margin-bottom">
		<span class="ui-icon ui-icon-calculator ui-state-noclick Cval-helper-float-left Cval-helper-margin-right"></span>
		<div class="Cval-helper-float-left">
			<span class="Cval-schedule-label"><?php echo $page_title ?></span>
			<span class="Cval-helper-margin-left Cval-schedule-target-groups-container Cval-helper-font-size-default"><?php echo implode(', ', $assigned_schedule_groups_labels); ?></span>
		</div>
	</div>
	<div class="grid_8 Cval-helper Cval-menu-horizontal-id-schedule Cval-helper-margin-bottom" style="text-align: right">
		<span class="Cval-schedule-close Cval-trigger-cancel Cval-helper-float-right"><?php echo Cval::i18n('Close') ?></span>
		<span class="Cval-schedule-controls Cval-helper-float-right Cval-helper-margin-right ui-helper-hidden">
			<span class="Cval-schedule-assign-groups"><?php echo Cval::i18n('Assign groups') ?></span>
			<span class="Cval-schedule-remove"><?php echo Cval::i18n('Delete schedule') ?></span>
		</span>
		<span class="Cval-schedule-step-controls Cval-helper-float-right Cval-helper-margin-right">
			<span class="Cval-schedule-step-control-next Cval-helper-float-right ui-state-highlight"><?php echo Cval::i18n('Go to next step') ?></span>
			<span class="Cval-schedule-step-control-prev Cval-helper-margin-right Cval-helper-margin-top-50 Cval-helper-float-right Cval-helper-cursor-pointer Cval-helper-underline"><?php echo Cval::i18n('Go back') ?></span>
		</span>
	</div>
	<div class="clear"></div>
	
	<div class="grid_3">
		<ul class="Cval-menu-vertical Cval-menu-vertical-id-schedule">
			<li class="Cval-menu-item-schedule-step-properties"><span class="Cval-menu-vertical-content">1. <?php echo Cval::i18n('Details') ?></span></li>
			<li class="Cval-menu-item-schedule-step-contacts"><span class="Cval-menu-vertical-content">2. <?php echo Cval::i18n('Guests') ?></span></li>
			<li class="Cval-menu-item-schedule-step-plan"><span class="Cval-menu-vertical-content">3. <?php echo Cval::i18n('Propose times') ?></span></li>
			<li class="Cval-menu-item-schedule-step-negotiate"><span class="Cval-menu-vertical-content">4. <?php echo Cval::i18n('Check availability') ?></span></li>
			<li class="Cval-menu-item-schedule-step-time"><span class="Cval-menu-vertical-content">5. <?php echo Cval::i18n('Select time') ?></span></li>
			<li class="Cval-menu-item-schedule-step-send"><span class="Cval-menu-vertical-content">6. <?php echo Cval::i18n('Preview & Send') ?></span></li>
		</ul>
	</div>
	
	<?php echo Form::select('target_schedule_groups', $schedule_groups, $selected_schedule_groups, array(
		'class' => 'Cval-schedule-target-groups-select ui-helper-hidden'
	)) ?>
	<?php echo Form::hidden('csrf_groups_assign', $csrf_groups_assign) ?>
	
	<div class="Cval-page-id-schedule-content grid_13 full"></div>
	
	<div class="clear"></div>
</div>