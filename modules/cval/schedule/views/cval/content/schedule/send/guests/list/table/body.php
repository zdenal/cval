<?php foreach ($guests as $guest): ?>
<tr class="<?php echo $guest->html_class() ?>">
	<td class="Cval-column-label" title="<?php echo $guest->user_profile_shared->location->name() ?>">
		<div><?php echo $guest->name() ?></div>
	</td>
</tr>
<?php endforeach; ?>