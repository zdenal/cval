<div class="jWizard">
	<div>
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live')) ?>
		<?php echo Form::hidden('csrf', $csrf) ?>
		<div class="field-label grid_16">
			<?php echo UTF8::ucfirst(Cval::i18n($schedule_group->meta()->fields('label')->label)) ?>
			<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
		</div>
		<div class="clear"></div>
		<div class="field grid_16">
			<?php echo $schedule_group->input('label') ?>
		</div>
		<div class="clear"></div>
		<?php echo Form::close(); ?>
	</div>
</div>