<?php foreach ($guests as $guest): ?>
<tr class="<?php echo $guest->html_class() ?><?php echo $guest->is_responding ? '' : ' ui-state-error-text Cval-helper-bold' ?>">
	<td class="Cval-column-listcheck"><div><input type="checkbox" class="Cval-list-checkbox"/></div></td>
	<td class="Cval-column-label" title="<?php echo $guest->user_profile_shared->location->name() ?>">
		<div><?php echo $guest->name() ?></div>
	</td>
	<td class="Cval-column-actions">
		<div>
			<span class="ui-icon ui-icon-trash Cval-helper-float-right Cval-trigger-remove" title="<?php echo Cval::i18n('Remove from guests') ?>"></span>
			<?php if ($guest->user_profile_shared->blocked): ?>
			<span class="ui-icon ui-icon-cancel ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('User seems to be blocked') ?>"></span>
			<?php elseif ($guest->user_profile_shared->deleted): ?>
			<span class="ui-icon ui-icon-cancel ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('User seems to be deleted') ?>"></span>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
	</td>
</tr>
<?php endforeach; ?>