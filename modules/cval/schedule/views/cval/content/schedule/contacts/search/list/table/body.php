<?php foreach ($contacts as $contact): ?>
<tr class="<?php echo $contact->html_class() ?>">
	<td class="Cval-column-listcheck"><div><input type="checkbox" class="Cval-list-checkbox"/></div></td>
	<td class="Cval-column-label" title="<?php echo $contact->user_profile_shared->location->name() ?>">
		<div><?php echo $contact->name() ?></div>
	</td>
	<td class="Cval-column-actions">
		<div>
			<span class="ui-icon ui-icon-play Cval-helper-float-right Cval-trigger-add-to-guests" title="<?php echo Cval::i18n('Add to guests') ?>"></span>
			<?php if ($contact->user_profile_shared->blocked): ?>
			<span class="ui-icon ui-icon-cancel ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('User seems to be blocked') ?>"></span>
			<?php elseif ($contact->user_profile_shared->deleted): ?>
			<span class="ui-icon ui-icon-cancel ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('User seems to be deleted') ?>"></span>
			<?php endif; ?>
			<div class="clear"></div>
		</div>
	</td>
</tr>
<?php endforeach; ?>