<?php foreach ($guests as $guest): ?>
<tr class="<?php echo $guest->html_class() ?>">
	<td class="Cval-column-label" title="<?php echo $guest->user_profile_shared->location->name() ?>">
		<div><?php echo $guest->friendly_name() ?></div>
	</td>
	<td class="Cval-column-actions">
		<div>
			<?php if ($schedule->is_outbound() AND ! $guest->has_received_schedule): ?>
			<span class="ui-icon ui-icon-notice ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('Schedule wasn\'t delivered to this guest') ?>"></span>
			<?php endif; ?>
		</div>
	</td>
</tr>
<?php endforeach; ?>