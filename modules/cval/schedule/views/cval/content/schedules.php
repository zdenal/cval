<div class="container_16">	
	
	<div class="grid_3 Cval-page-label">
		<?php echo Cval::i18n($page_title) ?>
	</div>
	<div class="grid_13 Cval-helper Cval-menu-id-schedules" style="text-align: right">
		<div class="Cval-helper-float-left Cval-schedule-list-controls ui-helper-hidden">
			<span class="Cval-selectall"></span>
			<span class="Cval-paginator"></span>
		</div>
		<div class="Cval-helper-float-right">
			<span class="Cval-schedule-controls">
				<span class="Cval-schedule-add"><?php echo Cval::i18n('Create schedule') ?></span>
				<span class="Cval-schedule-remove"><?php echo Cval::i18n('Remove schedule') ?></span>
			</span>
		</div>
		<div class="grid_4 Cval-helper-float-right">
			<?php echo View::factory('cval/util/search/input'); ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="grid_3">
		<?php echo View::factory('cval/content/schedules/groups/menu/vertical')->include_view($thiss) ?>
		<div class="Cval-helper-margin-top-50 Cval-schedule_group-controls">
			<span class="ui-icon ui-icon-plus Cval-helper-float-left Cval-trigger-create" title="<?php echo Cval::i18n('Create a schedule group') ?>"></span>
			<span class="ui-icon ui-icon-trash Cval-helper-float-left Cval-trigger-remove" title="<?php echo Cval::i18n('Delete the schedule group') ?>"></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-trigger-edit" title="<?php echo Cval::i18n('Rename the schedule group') ?>"></span>
		</div>
	</div>
	
	<div class="Cval-page-id-schedules-content grid_13"></div>
	
	<div class="clear"></div>
</div>