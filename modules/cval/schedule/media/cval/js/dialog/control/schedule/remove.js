// KEXT[on]
(function(Cval, $) {

	Cval_dialog_control_schedule_remove_object = {
		staticParams : {
			defaults : {
				dialog : {
					title : Cval.i18n.get('Remove the schedule'),
					CvalHeight : false,
					destroyOnClose : true
				},
				mainClass : 'alpha omega Cval-dialog-no-padding',
				loadRemoteContent : false,
				refreshOnShow : false,
				buttons : {
					confirm : $('<span />'),
					cancel : $('<span />').text(Cval.i18n.get('Cancel')),
					confirmGroup : $('<span />').text(Cval.i18n.get('Remove from group'))
				},
				scheduleLabel : $('<span />').addClass('Cval-helper-font-weight-bold'),
				scheduleGroupLabel : $('<span />').addClass('Cval-helper-font-weight-bold'),
				buttonTexts : {
					confirm : {
						assignable : Cval.i18n.get('Remove completely'),
						notAssignable : Cval.i18n.get('Remove')
					}
				},
				htmlText : {
					assignable : $('<span />'),
					notAssignable : $('<span />')
				},
				contentHtml : null
			},
			init : function(callback) {
				this._createHtml();
				Cval.core.callback(callback);
			},
			_beforeHideConfirmed : function(dialog, event, ui) {
				this._beforeHide.apply(this, arguments);
				
				var hideCallback = this.data(this.getElement()).options.confirmedHideCallback;
				if ( ! hideCallback)
					hideCallback = Cval.page.getControl('schedules').refreshSchedules(null, true);
				
				Cval.core.callback(hideCallback);
			},
			_show : function() {
				var options = this.data(this.getElement()).options,
					data = options.data,
					groupData = options.groupData,
					assignable = groupData ? groupData.fields.assignable.value : false,
					multiple = options.multiple || false;
				
				// Display/Hide remove from group button
				this.defaults.buttons.confirmGroup.toggle(assignable);
				// Change text of remove button
				this.defaults.buttons.confirm.text(assignable
					? this.defaults.buttonTexts.confirm.assignable
					: this.defaults.buttonTexts.confirm.notAssignable)
					.toggleClass('ui-state-highlight', ! assignable);
					
				this.getElement().Cval_dialog('option', 'width', assignable ? 500 : 400);
				
				// Update schedule and group label
				this.defaults.scheduleLabel.text(multiple ? data.length : data.label);
				this.defaults.scheduleGroupLabel.text(groupData ? groupData.label : '');
				
				// Update content text
				this.defaults.contentHtml.html(assignable 
					? (multiple ? this._constructHtmlContentAssignableMultiple() : this._constructHtmlContentAssignable())
					: (multiple ? this._constructHtmlContentNotAssignableMultiple() : this._constructHtmlContentNotAssignable()));	
				
				this._super();
			},
			_bindEvents : function() {
				var thiss = this,
					options = this.data(this.getElement()).options,
					data = options.data,
					groupData = options.groupData,
					multiple = options.multiple || false,
					id = null;
					
				if (multiple) {
					id = [];
					$.each(data, function(){
						id.push(this.id);
					})
				} else {
					id = data.id;
				}

				this.defaults.buttons.confirm.click(function(){
					Cval.ajax.callModal({
						route : 'schedule',
						routeParams : {
							controller : 'remove'
						},
						data : {
							id : id
						}
					}, function(json) {
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}

						thiss.hide(null, 'confirmed');
					});

					return false;
				});
				
				this.defaults.buttons.confirmGroup.click(function(){
					Cval.ajax.callModal({
						route : 'schedule',
						routeParams : {
							controller : 'remove'
						},
						data : {
							id : id,
							group_id : groupData ? groupData.id : undefined
						}
					}, function(json) {
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}

						thiss.hide(null, 'confirmed');
					});

					return false;
				});

				this.defaults.buttons.cancel.click(function(){
					thiss.hide();

					return false;
				});
			},
			_createHtml : function() {
				if (this.defaults.loadedResponse.html)
					return;

				var thiss = this;
				this.defaults.contentHtml = $('<div />')
						.addClass('Cval-dialog-content');

				var footer = $('<div />')
						.addClass('Cval-dialog-footer ui-widget-header ui-corner-bottom Cval-helper-align-right');

				this.defaults.buttons.cancel
					.addClass('Cval-button Cval-button-live')
					.appendTo(footer);

				this.defaults.buttons.confirm
					.addClass('Cval-button Cval-button-live')
					.appendTo(footer);
					
				this.defaults.buttons.confirmGroup
					.addClass('Cval-button Cval-button-live')
					.addClass('ui-state-highlight')
					.appendTo(footer);

				this.defaults.loadedResponse.html = $('<div />')
					.append(this.defaults.contentHtml).append(footer);
			},
			_constructHtmlContentAssignable : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you want to remove the schedule "')))
					.append(this.defaults.scheduleLabel)
					.append($('<span />').text(Cval.i18n.get('" completely or remove it from the group "')))
					.append(this.defaults.scheduleGroupLabel)
					.append($('<span />').text(Cval.i18n.get('" only?')));
			},
			_constructHtmlContentNotAssignable : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you really want to remove the schedule "')))
					.append(this.defaults.scheduleLabel)
					.append($('<span />').text(Cval.i18n.get('"?')));
			},
			_constructHtmlContentAssignableMultiple : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you want to remove selected schedules (')))
					.append(this.defaults.scheduleLabel)
					.append($('<span />').text(Cval.i18n.get(') completely or remove them from the group "')))
					.append(this.defaults.scheduleGroupLabel)
					.append($('<span />').text(Cval.i18n.get('" only?')));
			},
			_constructHtmlContentNotAssignableMultiple : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you really want to remove selected schedules (')))
					.append(this.defaults.scheduleLabel)
					.append($('<span />').text(Cval.i18n.get(')?')));
			}
		},
		params : {}
	}
	
	// KEXTSTART
	
	Cval.dialog.control.def.extend('Cval.dialog.control.schedule_remove', 
		Cval_dialog_control_schedule_remove_object.staticParams,
		Cval_dialog_control_schedule_remove_object.params
	);

})(Cval, jQuery);