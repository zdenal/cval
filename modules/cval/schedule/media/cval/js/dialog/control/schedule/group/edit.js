(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.schedule_group_edit', {
		defaults : {
			dialog : {
				title : Cval.i18n.get('Rename the schedule group'),
				CvalHeight : false,
				destroyOnClose : true,
				width: 300
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'schedule_group',
				routeParams : {
					controller : 'edit'
				}
			},
			refreshOnShow : true
		},
		wizardEl : null,
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jWizard.base.css'
			}),
			Cval.route('cval/media', {
				file : 'css/jquery/ui/jWizard.css'
			}),
			Cval.route('cval/media', {
				file : 'js/jquery/ui/jWizard.js'
			})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_bindEvents : function() {
			var thiss = this;
			
			this.wizardEl = this.getElement().find('.jWizard');
			
			this.wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: false,
					finishText: Cval.i18n.get('Rename')
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.wizardNext(event, ui);
				},
				finish : function(event, ui) {
					thiss.wizardFinish(event, ui);
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
			});
			
			this._initWizard();
		},
		getLoadConfig : function() {			
			return $.extend({}, this._super(), {
				data : {
					id : this.data(this.getElement()).options.data.id
				}
			});
		},
		_initWizard : function() {
			var thiss = this;
			
			this.wizardEl.find('form').submit(function(e){
				e.preventDefault();
				// On form submit go to nextStep
				thiss.wizardEl.Cval_jWizard('nextStep');
				return false;
			})
		},
		wizardNext : function(event, ui) {},
		wizardFinish : function() {
			this._wizardFinish();
		},
		_wizardFinish : function() {
			var thiss = this,
				form = this.wizardEl.find('form');

			var data = form.serializeJSON();

			Cval.ajax.callModal({
				route : 'schedule_group',
				routeParams : {
					controller : 'edit',
					action : 'submit'
				},
				data : $.extend({}, data, {
					id : this.data(this.getElement()).options.data.id
				})
			}, function(json) {
				thiss._wizardFinishSuccess(json);
			})

			return false;
		},
		_wizardFinishSuccess : function(json) {
			if (json.message !== undefined)
			{
				Cval.message.fromOptions(json.message);
			}
			
			// Get schedules page control
			var schedulesControl = Cval.page.getControl('schedules');

			// Update complete menu
			schedulesControl.getElement().find(schedulesControl.defaults.menuVerticalSelector)
				.replaceWith(json.vertical_menu_html);
			// Inject data for menu
			Cval.ajax.data.inject(json);
			
			// Manual run vertical menu
			schedulesControl.getElement().find(schedulesControl.defaults.menuVerticalSelector).Cval_menu_vertical();
			
			// Set active group to new group
			schedulesControl.defaults.activeSectionParams.group = json.schedule_group.id;	
			// Then show schedules page
			Cval.page.show('schedules');
			
			this.hide();
		}
	}, {});

})(Cval, jQuery)