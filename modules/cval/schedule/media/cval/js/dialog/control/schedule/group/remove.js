// KEXT[on]
(function(Cval, $) {

	Cval_dialog_control_schedule_group_remove_object = {
		staticParams : {
			defaults : {
				dialog : {
					title : Cval.i18n.get('Delete the schedule group'),
					CvalHeight : false,
					destroyOnClose : true,
					width: 400
				},
				mainClass : 'alpha omega Cval-dialog-no-padding',
				loadRemoteContent : false,
				refreshOnShow : false,
				buttons : {
					confirm : $('<span />').text(Cval.i18n.get('Delete')),
					cancel : $('<span />').text(Cval.i18n.get('Cancel'))
				},
				scheduleGroupLabel : $('<span />').addClass('Cval-helper-font-weight-bold')
			},
			init : function(callback) {
				this._createHtml();
				Cval.core.callback(callback);
			},
			_show : function() {
				var data = this.data(this.getElement()).options.data;
				
				// Update schedule_group label
				this.defaults.scheduleGroupLabel.text(data.label);
				
				this._super();
			},
			_bindEvents : function() {
				var thiss = this;

				this.defaults.buttons.confirm.click(function(){
					Cval.ajax.callModal({
						route : 'schedule_group',
						routeParams : {
							controller : 'remove'
						},
						data : {
							id : thiss.data(thiss.getElement()).options.data.id
						}
					}, function(json) {
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}

						// Get schedules page control
						var schedulesControl = Cval.page.getControl('schedules');

						// Update complete menu
						schedulesControl.getElement().find(schedulesControl.defaults.menuVerticalSelector)
							.replaceWith(json.vertical_menu_html);
						// Inject data for menu
						Cval.ajax.data.inject(json);

						// Manual run vertical menu
						schedulesControl.getElement().find(schedulesControl.defaults.menuVerticalSelector).Cval_menu_vertical();

						// Set active group to new group
						schedulesControl.defaults.activeSectionParams.group = json.active_schedule_group.id;	
						// Then show schedules page
						Cval.page.show('schedules');

						thiss.hide();
					});

					return false;
				});

				this.defaults.buttons.cancel.click(function(){
					thiss.hide();

					return false;
				});
			},
			_createHtml : function() {
				if (this.defaults.loadedResponse.html)
					return;

				var thiss = this;
				var content = $('<div />')
						.addClass('Cval-dialog-content')
						.append(Cval.i18n.get('Do you really want to delete the schedule group "'))
						.append(this.defaults.scheduleGroupLabel)
						.append(Cval.i18n.get('"?'));

				var footer = $('<div />')
						.addClass('Cval-dialog-footer ui-widget-header ui-corner-bottom Cval-helper-align-right');

				this.defaults.buttons.cancel
					.addClass('Cval-button Cval-button-live')
					.appendTo(footer);

				this.defaults.buttons.confirm
					.addClass('Cval-button Cval-button-live')
					.addClass('ui-state-highlight')
					.appendTo(footer);

				this.defaults.loadedResponse.html = $('<div />')
					.append(content).append(footer);
			}
		},
		params : {}
	}
	
	// KEXTSTART
	
	Cval.dialog.control.def.extend('Cval.dialog.control.schedule_group_remove', 
		Cval_dialog_control_schedule_group_remove_object.staticParams,
		Cval_dialog_control_schedule_group_remove_object.params
	);

})(Cval, jQuery);