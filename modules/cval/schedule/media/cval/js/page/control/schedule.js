
(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.schedule', {
		defaults : {
			refreshOnShow : true,
			destroyOnClose : true,
			inHistory : true,
			loadUrl : {
				route : 'schedule_main'
			},
			
			showSystemParams : {}, 
				
			controlsSelector : '.Cval-menu-horizontal-id-schedule .Cval-schedule-controls',
			cancelSelector : '.Cval-trigger-cancel',
			cancelButtonTopSelector : '.Cval-schedule-close',
			
			scheduleAssignGroupsSelector : '.Cval-schedule-assign-groups',
			scheduleRemoveSelector : '.Cval-schedule-remove',
			scheduleDownloadIcsSelector : '.Cval-schedule-download-ics',
			
			scheduleLabelSelector : '.Cval-schedule-label',
			
			targetGroupsSelectSelector : '.Cval-schedule-target-groups-select',
			targetGroupsContainerSelector : '.Cval-schedule-target-groups-container',
			targetGroupsApplySelector : '.Cval-schedule-target-groups-apply',
			targetGroupsApplyClass : 'Cval-schedule-target-groups-apply',
			
			guestControlsSelector : '.Cval-schedule-guests-controls',
			guestSelectAllSelector : '.Cval-schedule-guests-list-controls .Cval-selectall',
			guestListSelector : '.Cval-schedule-guests-wrapper',
			guestListControlsSelector : '.Cval-schedule-guests-list-controls',
			
			calendarTimetableSelector : '.Cval-timetable-schedule-wrapper',
			timetableInitiated : false
		},
		state : null,
		controlButtonsetEl : null,
		assignGroupsButtonEl : null,
		assignedGroups : [],
		guestLastRefresh : {
			data : null
		},
		guestPaginationEl : null,
		guestListControlsEl : null,
		refreshLimits : null,
		init : function(callback) {
			var thiss = this;

			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jquery.multiselect.css'
			}),
			Cval.route('cval/media', {
				file : 'jquery/js/jquery.multiselect.js'
			}),
			Cval.route('cval/media', {
				file : 'css/page/schedule.css'
			})
			];

			//Cval.async('fullcalendar', function(){
				Cval.resource.get(resources, function(){
					// Init state config
					thiss.state = Cval.core.config('schedule').states;
					Cval.core.callback(callback);
				});	
			//});
		},
		show : function(params, callback, systemParams) {
			// Set system params
			this.defaults.showSystemParams = systemParams || {};
			
			this._super(params, callback);
		},
		getLoadConfig : function() {
			return $.extend({}, this._super(), {
				data : {
					id : this.defaults.showParams.id
				}
			});
		},
		_show : function() {
			this._super();
			this.refreshGuestList();
			//this.refreshTimetable();
		},
		_hide : function() {
			//this.defaults.timetableInitiated = false;
			this._super();
		},
		_bindEvents : function() {
			var thiss = this;

			// Store refresh limits
			this._updateRefreshLimitsFromServer(this.getLoadedResponse().refresh_interval);
			this._initTopMenu();
			this._initCancelButton();
			this._initGroupSelect();
			
			// Hover effects for table rows
			this.getElement().find(this.defaults.guestListSelector+' tbody tr').livequery(function(){
				$(this).hover(function(){
					$(this).addClass('Cval-hover');
				}, function(){
					$(this).removeClass('Cval-hover')
				});

				$(this).find('.Cval-list-checkbox').change(function(){
					thiss.guestListUpdate();
				});
			});
			this._initGuestListControls();
			this._displaySentMessage();
			
			this.updateScheduleData();
		},
		_initTopMenu : function() {
			var thiss = this;
				
			this.controlButtonsetEl = this.getElement().find(this.defaults.controlsSelector);

			this.controlButtonsetEl.find(this.defaults.scheduleRemoveSelector)
				.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-trash'
						}
				}).click(function(){
					Cval.dialog.loadControl('schedule_remove', function(){
						Cval.dialog.control.schedule_remove.show(null, {
							data : thiss.scheduleData(),
							multiple : false,
							confirmedHideCallback : function(){
								Cval.page.show('schedules');
							} 
						})
					});
				});
			
			this.controlButtonsetEl.find(this.defaults.scheduleDownloadIcsSelector)
				.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-arrowthickstop-1-s'
						}
				}).click(function(){
					window.open(Cval.route('schedule_download', {
						id: thiss.defaults.showParams.id,
						format: 'ics'
					}));
					return false;
				});
				
			this.assignGroupsButtonEl = this.controlButtonsetEl.find(this.defaults.scheduleAssignGroupsSelector)
				.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-tag'
						}
				});

			this.controlButtonsetEl.buttonset({
				options : ':radio:visible'
			});
		},
		_initCancelButton : function() {
			this.getElement().find(this.defaults.cancelButtonTopSelector)
				.Cval_button({
					icons : {
						primary : 'ui-icon-close'
					}
				});
			
			this.getElement().find(this.defaults.cancelSelector).click(function(e){
				e.preventDefault();
				Cval.page.show('schedules');
				return false;
			});
		},
		_initGroupSelect : function() {
			var thiss = this;

			var select = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				specificClass = 'Cval-multiselect-schedule-assign-groups';
			
			var header = '<span class="ui-helper-hidden Cval-helper-cursor-pointer '+this.defaults.targetGroupsApplyClass+'">'+Cval.i18n.get('Apply')+'</span>'
						+'<span class="'+this.defaults.targetGroupsApplyClass+'-nochanges">'+Cval.i18n.get('Assign groups')+'</span>';
			
			select.multiselect({
				minWidth : 150,
				header: header,
				position : {
					my : 'right top',
					at : 'right bottom',
					of : this.assignGroupsButtonEl
				},
				classes : 'Cval-multiselect-hidden '+specificClass,
				beforeopen : function(event, ui){
					thiss.restoreOriginalSelectedGroups();
				},
				height: 'auto'
			});
			
			this.assignedGroups = this.getSelectedGroupsIds();
			
			var selectMenu = $('.ui-multiselect-menu.'+specificClass),
				applyButton = selectMenu.find(this.defaults.targetGroupsApplySelector),
				selectMenuHeader = selectMenu.find(this.defaults.targetGroupsApplySelector+'-nochanges');
				
			select.change(function(){
				if (Cval.helper.json.compare(thiss.assignedGroups, thiss.getSelectedGroupsIds())) {
					applyButton.hide();
					selectMenuHeader.show();
				} else {
					selectMenuHeader.hide();
					applyButton.show();
				}
			})
			
			this.assignGroupsButtonEl.click(function(){
				applyButton.hide();
				selectMenuHeader.show();
				select.multiselect('open');
				return false;
			});
			
			applyButton.click(function(e){
				e.preventDefault();
				thiss._groupSelectApply();
				return false;
			});
		},
		getSelectedGroupsIds : function(){
			var groupsEl = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				groups = [];
			
			groupsEl.multiselect('getChecked').each(function(){
				var id = $(this).val();
				groups[id] = id;
			});
			
			return groups;
		},
		restoreOriginalSelectedGroups : function(){
			var groupsEl = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				groups = this.assignedGroups;
			
			groupsEl.multiselect('widget').find('input[type=checkbox]').each(function(){
				var id = $(this).val();
				$(this).attr('checked', groups[id] == id);
			});
		},
		updateAssignedGroups : function(groups){
			// groups are in rest format
			var labels = [],
				ids = {};
				
			$.each(groups, function(key, group){
				labels.push(group.label);
				if (group.fields.assignable.value)
					ids[group.id] = group.id;
			});
			
			this.assignedGroups = ids;
			this.restoreOriginalSelectedGroups();
			
			this.getElement().find(this.defaults.targetGroupsContainerSelector)
				.html(labels.join(', '));
		},
		_groupSelectApply : function(callback) {
			var thiss = this,
				groupsEl = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				csrf = this.getElement().find('input[name=csrf_groups_assign]:first');

			var groups = [];
			groupsEl.multiselect('getChecked').each(function(){
				groups.push({
					id : $(this).val()
				});
			});

			Cval.ajax.callModal({
				route : 'schedule',
				routeParams : {
					controller : 'groups_assign',
					action : 'submit'
				},
				data : {
					id : thiss.defaults.showParams.id,
					csrf : csrf.val(),
					groups : groups
				}
			}, function(json) {
				groupsEl.multiselect('close');
				thiss.getElement().find(thiss.defaults.targetGroupsApplySelector).hide();

				if (json.message !== undefined)
				{
					Cval.message.fromOptions(json.message);
				}

				// Propagate actual schedule data to parent control
				thiss.updateScheduleData(json.schedule);
				thiss.updateAssignedGroups(json.assigned_groups);
				Cval.core.callback(callback);
			});
		},
		scheduleData : function() {
			return this.getLoadedResponse().schedule;
		},
		updateScheduleData : function(data) {
			if (data)
				this.getLoadedResponse().schedule = data;
			data = this.getLoadedResponse().schedule;
			
			if (data.id)
				this.defaults.showParams.id = data.id;
			
			this.getElement().find(this.defaults.scheduleLabelSelector)
				.html(data.label ? data.label : Cval.i18n.get('Untitled'));
				
			this.controlButtonsetEl.show();
		},
		getState : function(data) {
			if ( ! data)
				data = this.getLoadedResponse().schedule;
			
			return data.fields.state.value;
		},
		_initGuestListControls : function() {
			var thiss = this,
				listControls = this.getElement()
					.find(this.defaults.guestListControlsSelector);

			this.guestPaginationEl = listControls.find('.Cval-paginator')
				.Cval_pagination({
					clickCallback : function(page) {
						thiss.guestPaginateResults(page);
					}
				});

			this.guestListControlsEl = listControls;
		},
		guestPaginateResults : function(page) {
			this.refreshGuestList(null, $.extend({}, this.guestLastRefresh.data || {}, {
				pagination : page
			}));
		},
		refreshGuests : function(callback) {
			this.refreshGuestList(callback);
		},
		refreshGuestList : function(callback, data) {
			var thiss = this;
			
			data = $.extend({}, {
				body_only : 1,
				schedule : thiss.defaults.showParams.id
			}, data || {});

			Cval.ajax.callModal({
				route : 'schedule',
				routeParams : {
					controller : 'guests_table'
				},
				data : data
			}, function(json) {
				thiss.guestLastRefresh = {
					data : data
				};
				
				thiss.updateGuestList(json, false, callback);
			});
		},
		updateGuestList : function(data, complete, callback) {
			var table = this.getElement().find(this.defaults.guestListSelector);

			// Display, show no guest message
			if (data.count > 0)
				this.noGuestMessageHide();
			else
				this.noGuestMessageShow();
			// Display/show table header
			table.toggle(data.count > 0);

			// Replace only body content
			if ( ! complete)
				table = table.find('tbody');

			table.html(data.html);
			// Inject js data
			Cval.ajax.data.inject(data);

			// update pagination
			this.guestPaginationEl.Cval_pagination('update', data.pagination);
			this.guestListUpdate();

			// Display, show list controls
			if (data.count > 0)
				this.guestListControlsEl.show();
			else
				this.guestListControlsEl.hide();

			Cval.core.callback(callback);
		},
		guestListUpdate : function() {
			// Nothing needs update after refresh now
		},
		noGuestMessageShow : function() {
			var thiss = this,
				selectorPrefix = 'Cval-schedule-guests';

			if (thiss.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-noguest').length)
				return;

			var content = thiss.getElement().find('.'+selectorPrefix+'-message-noguest-content').html();

			thiss.getElement().find('.'+selectorPrefix+'-info-box')
				.html(content);
		},
		noGuestMessageHide : function() {
			var thiss = this,
				selectorPrefix = 'Cval-schedule-guests';

			thiss.getElement().find('.'+selectorPrefix+'-info-box').html('');
		},
		_displaySentMessage : function() {
			// Show message only when already sent
			if ( ! this.defaults.showSystemParams.justSent)
				return;
			
			var wrapper = $('<div/>').addClass('grid_6');
			this.getElement().find('form:first')
					.before(wrapper)
					.before('<div class="clear"/>');
			
			var content = Cval.i18n.get('This schedule was just sent!');
			
			Cval.message.info(content, null, {
					wrapper : wrapper,
					autoDestroy : false
				});
		},
		refreshTimetable : function(callback) {
			if ( ! this.defaults.timetableInitiated)
				this._timetableInit();
			else
				this.getTimetableEl().fullCalendar('refetchEvents');
			Cval.core.callback(callback);
		},
		_updateRefreshLimitsFromServer : function(limits) {
			this.refreshLimits = [
				Date.parse(limits[0]),
				Date.parse(limits[1])
			];
		},
		timetableLimits : function() {
			return this.refreshLimits;
		},
		_timetableInit : function() {
			var thiss = this;
			this.defaults.timetableInitiated = true;
			
			var scheduleObjectFields = this.getLoadedResponse().schedule.fields,
				initDate = Date.parse(scheduleObjectFields.start.string_value);

			// Hover effects for table rows
			this.getTimetableEl().fullCalendar($.extend(true, {}, Cval.fullcalendar.defaults, {
				header: {
					right: 'agendaWeek,agendaDay'
				},
				viewDisplay: function(view) {
					thiss.fullcalendarViewDisplay.call(this, view, thiss);
				},
				events : {
					success: function(json) {
						// Store refresh limits
						thiss._updateRefreshLimitsFromServer(json.refresh_interval);
						return Cval.fullcalendar.defaults.events.success.apply(this, arguments);
					}
				}
			}, Cval.fullcalendar.getInitCurrentDateFromDate(initDate)));

			$(window).resize();
		},
		getTimetableEl : function() {
			return this.getElement().find(this.defaults.calendarTimetableSelector);
		},
		fullcalendarViewDisplay : function(view, thiss) {
			// display only when timetable is initiated
			if ( ! thiss.defaults.timetableInitiated)
				return;

			if ( ! view)
				view = thiss.getTimetableEl().fullCalendar('getView');

			var limits = thiss.timetableLimits();

			Cval.fullcalendar.clearTimeRanges(view);

			var pastTimeEl = Cval.fullcalendar.getTimeRange(view, limits[0].clone().addYears(-10), limits[0]);
			if (pastTimeEl) {
				pastTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').show();
			}

			var futureTimeEl = Cval.fullcalendar.getTimeRange(view, limits[1], limits[1].clone().addYears(10));
			if (futureTimeEl) {
				futureTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').show();
			}
		}
	}, {});

})(Cval, jQuery);