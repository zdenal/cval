
(function(Cval, $) {
	
	Cval_page_control_schedule_step_properties_object = {
		staticParams : {
			onLoadData : null,
			_bindEvents : function() {
				var thiss = this;

				this._super();
				
				this.getElement().find('form:first').submit(function(e){
					e.preventDefault();
					// On form submit go to nextStep
					thiss.getParentControl().goToNextStep();
					return false;
				})
				
				this.onLoadData = this._formData();
			},
			getLoadConfig : function() {
				return $.extend({}, this._super(), {
					data : {
						id : this.defaults.showParams.id,
						not_loaded :  this.getParentControl().isScheduleLoaded() ? 0 : 1,
						target_schedule_group : this.defaults.showParams.group
					}
				});
			},
			_updateMenuData : function() {
				return $.extend({}, this._super(), {
					next : {
						label : Cval.i18n.get('Step 2: Guests')
					},
					prev : false
				});
			},
			_nextClick : function(callback) {
				var thiss = this,
					data = this._formData();
					
				//$.Console.Debug(data, this.onLoadData);
				if (this.getParentControl().isScheduleLoaded() 
						&& Cval.helper.compare(data, this.onLoadData))
				{
					Cval.core.callback(callback);
					return;
				}

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'step_properties',
						action : 'submit'
					},
					data: data
				}, function(json) {
					thiss._nextClickSuccess(json, callback);
				}, null, {
					440 : function (errorThrown, JSONobject, XMLHttpRequest) {
						Cval.ajax._error[440].apply(null, arguments);
						thiss.getParentControl().highlightStepMenu(thiss.stepId())
					}
				})

				return;
			},
			_nextClickSuccess : function(json, callback) {
				if (json.message !== undefined)
				{
					Cval.message.fromOptions(json.message);
				}
				
				// Propagate actual schedule data to parent control
				this.updateScheduleData(json.schedule);
				
				Cval.core.callback(callback);
			},
			/**
			* Returns data from dialog you want send to server after save is invoked.
			* Should be overidden in subclasses.
			*/
			_formData : function() {
				var thiss = this,
					formData = this.getElement().find('form:first').serializeJSON();

				var groups = [],
					targetGroup = this.getParentControl().defaults.showParams.group;
				if (targetGroup)
					groups.push({
						id : targetGroup
					});
				
				return $.extend({}, formData, {
					groups : groups,
					id : this.defaults.showParams.id,
					not_loaded : this.getParentControl().isScheduleLoaded() ? 0 : 1
				});
			},
			/**
			* This is called when going to next step
			*/
			goToForwardStep : function(step, callback) {
				this._nextClick(callback);
			}
		},
		params : {}
	};
	
	$.Class.extend('Cval.page.control.schedule_step_properties', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedule_step_def', function(){
					
					Cval.page.control.schedule_step_def.extend('Cval.page.control.schedule_step_properties', 
						Cval_page_control_schedule_step_properties_object.staticParams,
						Cval_page_control_schedule_step_properties_object.params
					);
					
					Cval.page.control.schedule_step_properties.init(callback);
				})
			})
		}
	}, {});

})(Cval, jQuery);