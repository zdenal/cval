
(function(Cval, $) {
	
	Cval_page_control_schedule_step_plan_object = {
		staticParams : {
			defaults : {
				minLengthSelector : 'input[name=min_length]',
				minLengthUnitsSelector : 'select[name=min_length_units]',
				
				timeRangeSliderSelector : '.Cval-schedule-plan-timerange-slider',
				timeRangeStartSelector : '.Cval-schedule-plan-timerange-start',
				timeRangeEndSelector : '.Cval-schedule-plan-timerange-end',
				
				calendarTimetableSelector : '.Cval-timetable-schedule-plan-wrapper',
				timetableInitiated : false,

				helpSelector: '.Cval-schedule-step-help',
				helpTriggerSelector: '.Cval-schedule-step-help-trigger'
			},
			timeRangeSliderEl : null,
			timeRangeStartEl : null,
			timeRangeEndEl : null,
			timeRangeFormat : null,
			selectedDates : {},
			minLengthEl : null,
			minLengthUnitsEl : null,
			minLengthMinutes : null,
			scheduleLimits : null,
			init : function(callback) {
				var thiss = this;

				// Needed resources
				var resources = [
					Cval.route('cval/media', {
						file : 'jquery/css/jquery.multiselect.css'
					}),
					Cval.route('cval/media', {
						file : 'jquery/js/jquery.multiselect.js'
					}),
					Cval.route('cval/media', {
						file : 'js/jquery/ui/slider.js'
					}),
					Cval.route('cval/media', {
						file : 'css/page/schedule/step/plan.css'
					})
				];

				Cval.async('fullcalendar', function(){
					Cval.resource.get(resources, function(){
						Cval.core.callback(callback);
					});	
				});
			},
			_show : function() {
				this._super();
				this.refreshTimetable();
			},
			_hide : function() {
				this.defaults.timetableInitiated = false;
				this._super();
			},
			_updateMenuData : function() {
				return $.extend({}, this._super(), {
					next : {
						label : Cval.i18n.get('Step 4: Check availability')
					},
					prev : true
				});
			},
			_bindEvents : function() {
				var thiss = this;

				// Store schedule limits
				this.scheduleLimits = this.getLoadedResponse().schedule_limits;
				this._updateSelectedDatesFromServer(this.getLoadedResponse().dates);
				
				this._super();

				var helpEl = this.getElement().find(this.defaults.helpSelector);
				this.getElement().find(this.defaults.helpTriggerSelector).click(function(){
					helpEl.toggle();
				});
				
				this.getElement().find('form:first').submit(function(e){
					e.preventDefault();
					// On form submit go to nextStep
					thiss.getParentControl().goToNextStep();
					return false;
				})
				
				this._initTimeRange();
				this._initMinLengthUnits();
					
				this.onLoadData = this._saveCallData();
			},
			getLoadConfig : function() {
				return $.extend({}, this._super(), {
					data : {
						id : this.defaults.showParams.id
					}
				});
			},
			/**
			* This is called when going to next step
			*/
			goToForwardStep : function(step, callback) {
				this._nextClick(callback);
			},
			_nextClick : function(callback) {
				var thiss = this,
					data = this._saveCallData();
					
				if (this.getParentControl().getStep(this.stepId()).valid 
						&& Cval.helper.compare(data, this.onLoadData))
				{
					Cval.core.callback(callback);
					return;
				}
				
				data.id = this.defaults.showParams.id;

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'step_plan',
						action : 'submit'
					},
					data: data
				}, function(json) {
					thiss._nextClickSuccess(json, callback);
				}, null, {
					440 : function (errorThrown, JSONobject, XMLHttpRequest) {
						Cval.ajax._error[440].apply(null, arguments);
						thiss.getParentControl().highlightStepMenu(thiss.stepId())
					}
				})

				return;
			},
			_nextClickSuccess : function(json, callback) {
				if (json.message !== undefined)
				{
					Cval.message.fromOptions(json.message);
				}
				
				//$.Console.Debug(json);
				
				this._updateSelectedDatesFromServer(json.dates);
				this.fullcalendarViewDisplay(null, this);
				
				// Propagate actual schedule data to parent control
				this.updateScheduleData(json.schedule);
				
				Cval.core.callback(callback);
			},
			/**
			* Returns data from dialog you want send to server after save is invoked.
			* Should be overidden in subclasses.
			*/
			_saveCallData : function() {
				var data = this.getElement().find('form:first').serializeJSON(),
					timeRangeMinutes = this.getTimeRangeMinutes();
				
				data.start_minutes = timeRangeMinutes[0];
				// 0 in end minutes is 24:00
				data.end_minutes = timeRangeMinutes[1];
				
				var dates = [];
				$.each(this.selectedDates, function(timestamp, dateObject){
					dates.push(Cval.helper.date.toString(dateObject));
				});
				dates.sort();
				data.dates = dates;
				
				return $.extend({}, data);
			},
			_updateSelectedDatesFromServer : function(dates) {
				var thiss = this;
				
				this.selectedDates = {};
				$.each(dates, function(key, timestamp){
					var date = Date.parse(timestamp);
					thiss.selectedDates[date.getTime()] = date;
					//$.Console.Debug(date)
				});
			},
			_initMinLengthUnits : function() {
				var thiss = this;

				this.minLengthUnitsEl = this.getElement().find(this.defaults.minLengthUnitsSelector);
				this.minLengthUnitsEl.multiselect({
					minWidth: 80,
					multiple: false,
					header: false,
					selectedList: 1,
					height: 'auto'
				}).change(function(){
					thiss._minLengthChange();
				});
				
				this.minLengthEl = this.getElement().find(this.defaults.minLengthSelector);
				this.minLengthEl.change(function(){
					thiss._minLengthChange();
				});
			
				thiss._minLengthChange();
			},
			_minLengthChange : function(){
				var length = parseInt(this.minLengthEl.val()),
					units = this.minLengthUnitsEl.val(),
					minutes = 0;
					
				if ( ! length || length < 1) {
					length = 1;
					this.minLengthEl.val(length);
				}
				
				minutes = length * (units == 'm' ? 1 : 60);
				
				this.minLengthMinutes = minutes;
				this._setMinimumTimeRange(minutes);
			},
			_initTimeRange : function(){
				var thiss = this;
				
				this.timeRangeFormat = Cval.core.config('time_24_hour') ? 'H:mm' : 'h:mm tt';
				
				this.timeRangeStartEl = this.getElement().find(this.defaults.timeRangeStartSelector);
				this.timeRangeEndEl = this.getElement().find(this.defaults.timeRangeEndSelector);
				
				var schedulePlanObjectFields = this.getLoadedResponse().schedule_plan.fields,
					defaultSliderValues = [
					schedulePlanObjectFields.start_minutes.value,
					schedulePlanObjectFields.end_minutes.value
				];
				
				var lastSliderPos = [];
				
				this.timeRangeSliderEl = this.getElement().find(this.defaults.timeRangeSliderSelector)
					.Cval_slider({
						range: true,
						min: 0,
						max: 1440,
						step: 15,
						values: defaultSliderValues,
						start: function(e, ui) {
							lastSliderPos = ui.values;
						},
						slide: function(e, ui) {
							if ((ui.values[1] - ui.values[0]) < thiss.minLengthMinutes)
								return false;
							
							var updatedValues = [
								ui.values[0] == lastSliderPos[0] ? false : ui.values[0],
								ui.values[1] == lastSliderPos[1] ? false : ui.values[1]
							];
							
							lastSliderPos = ui.values;
							
							thiss._setTimeRangeInputs(updatedValues);
							return true;
						},
						change : function(e, ui) {
							thiss.fullcalendarViewDisplay(null, thiss);
						}
					});
					
				this.timeRangeStartEl.change(function(){
					var val = thiss._formatTimeRangeInputValueForSlider($(this).val());
					if (val === false) {
						thiss._setTimeRangeInputs();
						return;
					}
					
					thiss._setTimeRangeInputs([val,false]);
					thiss.timeRangeSliderEl.slider("values", 0, val);
				});
				
				this.timeRangeEndEl.change(function(){
					var val = thiss._formatTimeRangeInputValueForSlider($(this).val());
					if (val === false) {
						thiss._setTimeRangeInputs();
						return;
					}
					if (val < 1)
						val = 1440;
					
					thiss._setTimeRangeInputs([false,val]);
					thiss.timeRangeSliderEl.slider("values", 1, val);
				});
					
				this._setTimeRangeInputs(defaultSliderValues);
			},
			_setMinimumTimeRange : function(minutes){
				var timeRangeMinutes = this.getTimeRangeMinutes(),
					start = timeRangeMinutes[0],
					end = timeRangeMinutes[1];
					
				if ((end - start) >= minutes)
					return;
				
				var diff = (end - start) - minutes;
				
				// Make it possitive
				diff *= -1;
				diff = Math.ceil(diff/2);
				start -= diff;
				end += diff;
				
				if (start < 0) {
					end += start*-1; 
					start = 0;
				}
				if (end > 1440) {
					start -= (end-1440); 
					end = 1440;
				}
				if (start < 0)
					start = 0;
				
				this._setTimeRangeInputs([start, end]);
				this.timeRangeStartEl.trigger('change');
				this.timeRangeEndEl.trigger('change');
			},
			_setTimeRangeInputs : function(values){
				if (values === undefined) {
					values = [
						this.timeRangeSliderEl.slider("values", 0),
						this.timeRangeSliderEl.slider("values", 1),
					]
				}
				
				if (values[0] !== false) {
					this.timeRangeStartEl.val(this._formatTimeRangeSliderValue(values[0]));
				}
				
				if (values[1] !== false) {
					if (values[1] < 1)
						values[1] = 1440;
					this.timeRangeEndEl.val(this._formatTimeRangeSliderValue(values[1]));
				}
			},
			_formatTimeRangeSliderValue : function(val){
				var hours = Math.floor(val / 60),
					minutes = val - (hours * 60),
					date = new Date();

				date.setHours(hours);
				date.setMinutes(minutes);
				
				return date.toString(this.timeRangeFormat).toLowerCase();
			},
			_formatTimeRangeInputValueForSlider : function(val){
				if ( ! val.length) {
					return false;
				}

				var date = Date.parse(val);
				if ( ! date) {
					return false;
				}
				return (date.getHours() * 60 + date.getMinutes());
			},
			getTimeRangeMinutes : function() {
				var start = this._formatTimeRangeInputValueForSlider(this.timeRangeStartEl.val()),
					// 0 in end minutes is 24:00
					end = this._formatTimeRangeInputValueForSlider(this.timeRangeEndEl.val()) || 1440;
					
				return [start, end];
			},
			refreshTimetable : function(callback) {
				if ( ! this.defaults.timetableInitiated)
					this._timetableInit();
				else
					this.getTimetableEl().fullCalendar('refetchEvents');
				Cval.core.callback(callback);
			},
			timetableLimits : function() {
				return [
					Date.now().addSeconds(this.scheduleLimits[0]),
					Date.now().addSeconds(this.scheduleLimits[1]) 
				];
			},
			limitSelectedDates : function(useLimits) {
				var thiss = this,
					limits = useLimits || this.timetableLimits();
					
				limits = [limits[0].clone().clearTime(), limits[1].clone().clearTime()]
				
				$.each(this.selectedDates, function(timestamp, date){
					if (date < limits[0] || date > limits[1])
						delete thiss.selectedDates[timestamp];
				});
			},
			_timetableInit : function() {
				var thiss = this;
				this.defaults.timetableInitiated = true;

				var initDate;
				$.each(this.selectedDates, function(){
					initDate = this;
					return false;
				});
				if (!initDate)
					initDate = Date.now();

				// Hover effects for table rows
				this.getTimetableEl().fullCalendar($.extend(true, {}, Cval.fullcalendar.defaults, {
					header: {
						right: 'agendaWeek,agendaDay'
					},
					events: {
						data: {
							cval_events_exclude: thiss.scheduleData().id 
						}
					},
					dayClick: function(date, allDay, jsEvent, view) {
						var clickedDate = date.clone().clearTime();
						
						var timestamp = clickedDate.getTime();
							
						if ( ! thiss.selectedDates[timestamp]) {
							thiss.selectedDates[timestamp] = clickedDate;
							thiss.fullcalendarInsertDate(view, clickedDate);
						} else {
							delete thiss.selectedDates[timestamp];
							view.element.find('.Cval-fullcalendar-schedule-plan-date-'+timestamp).remove();
						}
					},
					viewDisplay: function(view) {
						thiss.fullcalendarViewDisplay.call(this, view, thiss);
					}
				}, Cval.fullcalendar.getInitCurrentDateFromDate(initDate)));

				$(window).resize();
			},
			getTimetableEl : function() {
				return this.getElement().find(this.defaults.calendarTimetableSelector);
			},
			fullcalendarViewDisplay : function(view, thiss) {
				// display only when timetable is initiated
				if ( ! thiss.defaults.timetableInitiated)
					return;
				
				if ( ! view)
					view = thiss.getTimetableEl().fullCalendar('getView');
				
				var limits = thiss.timetableLimits();
				
				Cval.fullcalendar.clearTimeRanges(view);
				
				var pastTimeEl = Cval.fullcalendar.getTimeRange(view, limits[0].clone().addYears(-10), limits[0]);
				if (pastTimeEl) {
					pastTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').click(function(){
						alert('Date in past can\'t be selected!');
					}).show();
				}
				
				var futureTimeEl = Cval.fullcalendar.getTimeRange(view, limits[1], limits[1].clone().addYears(10));
				if (futureTimeEl) {
					futureTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').click(function(){
						alert('Only date in limited future can be selected!');
					}).show();
				}
				
				thiss.limitSelectedDates(limits);
				
				var timeRangeMinutes = thiss.getTimeRangeMinutes();
				$.each(thiss.selectedDates, function(){
					thiss.fullcalendarInsertDate(view, this, timeRangeMinutes, limits);
				});
			},
			fullcalendarInsertDate : function(view, date, timeRangeMinutes, limits) {
				var thiss = this;
				
				if ( ! timeRangeMinutes)
					timeRangeMinutes = this.getTimeRangeMinutes();
				if ( ! limits)
					limits = this.timetableLimits();
				
				var	start = date.clone().addMinutes(timeRangeMinutes[0]),
					end = date.clone().addMinutes(timeRangeMinutes[1]).addSeconds(-1);
					
				if (start.getTime() < limits[0])
					start.setTime(limits[0]);
				if (end.getTime() > limits[1])
					end.setTime(limits[1]);
				
				if ((end.getTime() - start.getTime()) < 1)
					return;
					
				var timeRangeEl = Cval.fullcalendar.getTimeRange(view, start, end),
					timestamp = date.getTime();
					
				if (timeRangeEl) {
					timeRangeEl.click(function(){
						if (thiss.selectedDates[timestamp])
							delete thiss.selectedDates[timestamp];
						
						$(this).remove();
					}).addClass('Cval-fullcalendar-timerange-item_highlight')
						.addClass('Cval-fullcalendar-schedule-plan-date-'+timestamp).show();
				}
			}
		},
		params : {}
	};
	
	$.Class.extend('Cval.page.control.schedule_step_plan', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedule_step_def', function(){
					
					Cval.page.control.schedule_step_def.extend('Cval.page.control.schedule_step_plan', 
						Cval_page_control_schedule_step_plan_object.staticParams,
						Cval_page_control_schedule_step_plan_object.params
					);
					
					Cval.page.control.schedule_step_plan.init(callback);
				})
			})
		}
	}, {});

})(Cval, jQuery);