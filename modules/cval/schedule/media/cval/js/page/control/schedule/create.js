
(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.schedule_create', {
		defaults : {
			activeStep : 'properties',
			
			refreshOnShow : false,
			destroyOnClose : true,
			inHistory : true,
			controlsSelector : '.Cval-menu-horizontal-id-schedule .Cval-schedule-controls',
			cancelSelector : '.Cval-trigger-cancel',
			cancelButtonTopSelector : '.Cval-schedule-close',
			
			scheduleAssignGroupsSelector : '.Cval-schedule-assign-groups',
			scheduleRemoveSelector : '.Cval-schedule-remove',
			
			scheduleLabelSelector : '.Cval-schedule-label',
			
			targetGroupsSelectSelector : '.Cval-schedule-target-groups-select',
			targetGroupsContainerSelector : '.Cval-schedule-target-groups-container',
			targetGroupsApplySelector : '.Cval-schedule-target-groups-apply',
			targetGroupsApplyClass : 'Cval-schedule-target-groups-apply',
			
			stepMenuSelector : '.Cval-menu-vertical-id-schedule',
			stepControlsSelector : '.Cval-schedule-step-controls',
			stepControlNextSelector : '.Cval-schedule-step-control-next',
			stepControlPrevSelector : '.Cval-schedule-step-control-prev',
			
			stepControlPrefix : 'schedule_step_'
		},
		state : null,
		steps : null,
		stepsByPos : null,
		stepMenuEl : null,
		stepControlsEl : null,
		stepControlNextEl : null,
		stepControlPrevEl : null,
		controlButtonsetEl : null,
		assignGroupsButtonEl : null,
		assignedGroups : [],
		init : function(callback) {
			var thiss = this;

			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jquery.multiselect.css'
			}),
			Cval.route('cval/media', {
				file : 'jquery/js/jquery.multiselect.js'
			}),
			Cval.route('cval/media', {
				file : 'css/page/schedule/create.css'
			})
			];

			Cval.resource.get(resources, function(){
				// Init state config
				thiss.state = Cval.core.config('schedule').states;
				Cval.core.callback(callback);
			});
		},
		_hide : function() {
			this.getActiveStepControl().hide();
			this._super();
		},
		getUrlFromId : function() {
			var parts = this.defaults.id.split('_');
			var routeConf = { 
				route : parts[0],
				routeParams : {
					controller : parts.slice(1).join('_')
				}
			};
			return routeConf;
		},
		getLoadConfig : function() {
			return $.extend({}, this._super(), {
				data : {
					id : this.defaults.showParams.id,
					not_loaded :  this.isScheduleLoaded() ? 0 : 1,
					group : this.defaults.showParams.group
				}
			});
		},
		/**
		 * This is called for updating page content after load or refresh
		 */
		setContent : function(content, callback) {
			this.updateScheduleData();
			// Test if we should redirect to detail
			if (this.checkDetailRedirect()) {
				return;
			}
			
			this._super(content, callback);
		},
		isScheduleLoaded : function() {
			return this.defaults.showParams.id && this.defaults.showParams.id.length > 0;
		},
		_bindEvents : function() {
			var thiss = this;

			this._initSteps();
			this._initStepMenu();
			this._initStepControls();
			this._initStep();
			this._initTopMenu();
			this._initCancelButton();
			this._initGroupSelect();
		},
		_showStep : function(id) {
			Cval.page.show(this.defaults.stepControlPrefix+id, {
				id : this.defaults.showParams.id
			});
		},
		_initStepMenu : function() {
			var thiss = this;
			
			this.stepMenuEl = this.getElement().find(this.defaults.stepMenuSelector)
					.Cval_menu_vertical();
					
			this.stepMenuEl.Cval_menu_vertical('items').each(function(){
				var id = Cval.core.htmlClassValue($(this), 'Cval-menu-item-schedule-step-'),
					step = thiss.getStep(id);
				$(this).click(function(){
					thiss.goToStep(step.id);
				});
			});
		},
		_initStepControls : function() {
			var thiss = this;
			
			this.stepControlsEl = this.getElement().find(this.defaults.stepControlsSelector);
			
			this.stepControlNextEl = this.getElement().find(this.defaults.stepControlNextSelector)
				.Cval_button().hide();
				
			this.stepControlPrevEl = this.getElement().find(this.defaults.stepControlPrevSelector)
				.click(function(){
					thiss.goToPrevStep();
					return false;
				}).hide();
		},
		_initStep : function() {
			this._showStep(this.defaults.activeStep);
		},
		_initSteps : function() {
			this.updateSteps();
			
			//$.Console.Debug(this.steps)
			
			var activeStep;
			$.each(this.steps, function(id, config){
				activeStep = config;
				$.Console.Debug(activeStep)
				if ( ! config.valid)
					return false;
			});
			this.defaults.activeStep = activeStep.id;
		},
		_initTopMenu : function() {
			var thiss = this;
				
			this.controlButtonsetEl = this.getElement().find(this.defaults.controlsSelector);

			this.controlButtonsetEl.find(this.defaults.scheduleRemoveSelector)
				.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-trash'
						}
				}).click(function(){
					Cval.dialog.loadControl('schedule_remove', function(){
						Cval.dialog.control.schedule_remove.show(null, {
							data : thiss.scheduleData(),
							multiple : false,
							confirmedHideCallback : function(){
								Cval.page.show('schedules');
							} 
						})
					});
				});
				
			this.assignGroupsButtonEl = this.controlButtonsetEl.find(this.defaults.scheduleAssignGroupsSelector)
				.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-tag'
						}
				});

			this.controlButtonsetEl.buttonset({
				options : ':radio:visible'
			});
		},
		_initCancelButton : function() {
			this.getElement().find(this.defaults.cancelButtonTopSelector)
				.Cval_button({
					icons : {
						primary : 'ui-icon-close'
					}
				});
			
			this.getElement().find(this.defaults.cancelSelector).click(function(e){
				e.preventDefault();
				Cval.page.show('schedules');
				return false;
			});
		},
		_initGroupSelect : function() {
			var thiss = this;

			var select = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				specificClass = 'Cval-multiselect-schedule-assign-groups';
			
			var header = '<span class="ui-helper-hidden Cval-helper-cursor-pointer '+this.defaults.targetGroupsApplyClass+'">'+Cval.i18n.get('Apply')+'</span>'
						+'<span class="'+this.defaults.targetGroupsApplyClass+'-nochanges">'+Cval.i18n.get('Assign groups')+'</span>';
			
			select.multiselect({
				minWidth : 150,
				header: header,
				position : {
					my : 'right top',
					at : 'right bottom',
					of : this.assignGroupsButtonEl
				},
				classes : 'Cval-multiselect-hidden '+specificClass,
				beforeopen : function(event, ui){
					thiss.restoreOriginalSelectedGroups();
				},
				height: 'auto'
			});
			
			this.assignedGroups = this.getSelectedGroupsIds();
			
			var selectMenu = $('.ui-multiselect-menu.'+specificClass),
				applyButton = selectMenu.find(this.defaults.targetGroupsApplySelector),
				selectMenuHeader = selectMenu.find(this.defaults.targetGroupsApplySelector+'-nochanges');
				
			select.change(function(){
				if (Cval.helper.json.compare(thiss.assignedGroups, thiss.getSelectedGroupsIds())) {
					applyButton.hide();
					selectMenuHeader.show();
				} else {
					selectMenuHeader.hide();
					applyButton.show();
				}
			})
			
			this.assignGroupsButtonEl.click(function(){
				applyButton.hide();
				selectMenuHeader.show();
				select.multiselect('open');
				return false;
			});
			
			applyButton.click(function(e){
				e.preventDefault();
				thiss._groupSelectApply();
				return false;
			});
		},
		getSelectedGroupsIds : function(){
			var groupsEl = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				groups = [];
			
			groupsEl.multiselect('getChecked').each(function(){
				var id = $(this).val();
				groups[id] = id;
			});
			
			return groups;
		},
		restoreOriginalSelectedGroups : function(){
			var groupsEl = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				groups = this.assignedGroups;
			
			groupsEl.multiselect('widget').find('input[type=checkbox]').each(function(){
				var id = $(this).val();
				$(this).attr('checked', groups[id] == id);
			});
		},
		updateAssignedGroups : function(groups){
			// groups are in rest format
			var labels = [],
				ids = {};
				
			$.each(groups, function(key, group){
				labels.push(group.label);
				if (group.fields.assignable.value)
					ids[group.id] = group.id;
			});
			
			this.assignedGroups = ids;
			this.restoreOriginalSelectedGroups();
			
			this.getElement().find(this.defaults.targetGroupsContainerSelector)
				.html(labels.join(', '));
		},
		_groupSelectApply : function(callback) {
			var thiss = this,
				groupsEl = this.getElement().find(this.defaults.targetGroupsSelectSelector),
				csrf = this.getElement().find('input[name=csrf_groups_assign]:first');

			var groups = [];
			groupsEl.multiselect('getChecked').each(function(){
				groups.push({
					id : $(this).val()
				});
			});

			Cval.ajax.callModal({
				route : 'schedule',
				routeParams : {
					controller : 'groups_assign',
					action : 'submit'
				},
				data : {
					id : thiss.defaults.showParams.id,
					csrf : csrf.val(),
					groups : groups
				}
			}, function(json) {
				groupsEl.multiselect('close');
				thiss.getElement().find(thiss.defaults.targetGroupsApplySelector).hide();

				if (json.message !== undefined)
				{
					Cval.message.fromOptions(json.message);
				}

				// Propagate actual schedule data to parent control
				thiss.updateScheduleData(json.schedule);
				thiss.updateAssignedGroups(json.assigned_groups);
				Cval.core.callback(callback);
			});
		},
		onStepShow : function(stepId, options) {
			var thiss = this;
			this.defaults.activeStep = stepId;
			this.stepMenuEl.Cval_menu_vertical('select', '.Cval-menu-item-schedule-step-'+stepId);
			
			if (options.next) {
				this.stepControlNextEl.unbind('click').click(options.next.callback 
					? options.next.callback
					: function(){
						thiss.goToNextStep();
						return false;
					});
				this.stepControlNextEl.Cval_button('option', 'label', options.next.label).show();
			} else {
				this.stepControlNextEl.hide();
			}
			
			if (options.prev) {
				this.stepControlPrevEl.show();
			} else {
				this.stepControlPrevEl.hide();
			}
		},
		scheduleData : function() {
			return this.getLoadedResponse().schedule;
		},
		updateScheduleData : function(data) {
			if (data)
				this.getLoadedResponse().schedule = data;
			data = this.getLoadedResponse().schedule;
			
			if (data.id)
				this.defaults.showParams.id = data.id;
			
			this.updateSteps();
			// Test if we should redirect to detail
			if (this.checkDetailRedirect()) {
				this.redirectToDetail();
				return;
			}
			
			this.getElement().find(this.defaults.scheduleLabelSelector)
				.html(data.label ? data.label : Cval.i18n.get('Untitled'));
			
			if (this.controlButtonsetEl) {
				if (this.isScheduleLoaded())
					this.controlButtonsetEl.show();
				else
					this.controlButtonsetEl.hide();
			}
		},
		getState : function(data) {
			if ( ! data)
				data = this.getLoadedResponse().schedule;
			
			return data.fields.state.value;
		},
		checkState : function(state, data) {
			return (this.getState(data) == this.state[state]);
		},
		updateSteps : function() {
			var thiss = this,
				data = this.scheduleData();
			
			if ( ! this.steps) {
				this.steps = {
					properties : { pos : 1 },
					contacts : { pos : 2 },
					plan : { pos : 3 },
					negotiate : { pos : 4 },
					time : { pos : 5 },
					send : { pos : 6 }
				};
				
				this.stepsByPos = {};
				$.each(this.steps, function(id, config){
					$.extend(thiss.steps[id], {
						id : id,
						valid : false
					});
					thiss.stepsByPos[config.pos] = id; 
				});
			}
			
			var state = this.getState(data);
			
			this.steps.properties.valid = data.id ? true : false;
			this.steps.contacts.valid = data.fields.has_contacts.value ? true : false;
			this.steps.plan.valid = data.fields.has_plan.value ? true : false;
			this.steps.negotiate.valid = state >= this.state.TIME_NOT_SELECTED;
			this.steps.time.valid = state >= this.state.TIME_SELECTED;
			this.steps.send.valid = state >= this.state.SENT;
		},
		getStep : function(id) {
			return this.steps[id];
		},
		getStepByPosition : function(pos) {
			return this.steps[this.stepsByPos[pos]];
		},
		getActiveStep : function() {
			return this.getStep(this.defaults.activeStep);
		},
		getActiveStepControl : function() {
			return Cval.page.getControl(this.defaults.stepControlPrefix+this.getActiveStep().id);
		},
		goToStep : function(id) {
			var thiss = this,
				activeStep = this.getActiveStep(),
				targetStep = this.getStep(id);
			
			if (activeStep.id == targetStep.id)
				return;
			
			var forward = targetStep.pos > activeStep.pos,
				stepControl = this.getActiveStepControl();
			
			$.Console.Debug('src step: '+stepControl.stepId())
			
			if ( ! forward) {
				stepControl.goToBackwardStep(targetStep, function(){
					$.Console.Debug('trg step: '+targetStep.id)
					thiss._showStep(targetStep.id);
				});
			} else {
				stepControl.goToForwardStep(targetStep, function(){
					// Get updated step data
					targetStep = thiss.getStep(id);
					var tmpPos = 1,
						tmpStep;
					while (tmpPos < targetStep.pos) {
						tmpStep = thiss.getStepByPosition(tmpPos);
						if ( ! tmpStep.valid) {
							targetStep = tmpStep;
							break;
						}
						tmpPos++;
					}
					
					$.Console.Debug('trg step: '+targetStep.id)
					
					if (targetStep.id != id) {
						thiss.highlightStepMenu(targetStep.id);
					}
					
					thiss._showStep(targetStep.id);
				});
			}
			//alert('Going to step: '+targetStep.id+', '+targetStep.pos)
		},
		goToNextStep : function() {
			var activeStep = this.getActiveStep();
			
			this.goToStep(this.getStepByPosition(activeStep.pos + 1).id);
		},
		goToPrevStep : function() {
			var activeStep = this.getActiveStep();
			if (activeStep.pos < 2)
				return;
			
			this.goToStep(this.getStepByPosition(activeStep.pos - 1).id);
		},
		highlightStepMenu : function(id) {
			var errorMenuItem = this.stepMenuEl.Cval_menu_vertical('items', '.Cval-menu-item-schedule-step-'+id);
			
			errorMenuItem.addClass('ui-state-error');
			setTimeout(function(){
				errorMenuItem.removeClass('ui-state-error');
			}, 2000);
		},
		checkDetailRedirect : function() {
			// If this schedule is sent or received fo to detail
			return this.getState() >= this.state.SENT;
		},
		redirectToDetail : function(callback, systemParams) {
			Cval.page.show('schedule', {
				id : this.defaults.showParams.id
			}, callback, systemParams);
		}
	}, {});

})(Cval, jQuery);