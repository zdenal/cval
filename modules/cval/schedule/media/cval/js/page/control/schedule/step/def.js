(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.schedule_step_def', {
		defaults : {
			/**
			 * Page holder
			 */
			parentElement : '.Cval-page-id-schedule-content',
			/**
			 * Page main element classes. It must be same for all instances,
			 * otherwise this.closeOtherPages will not work properly.
			 */
			pageClass : 'Cval-page-id-schedule-page',
			destroyOnClose : true,
			inHistory : false
		},
		_parentControl : null,
		refreshSchedules : function(callback) {
			// Implement this, to refresh schedules list, timetable or etc.
			// Now just call the callback
			Cval.core.callback(callback);
		},
		getUrlFromId : function() {
			var parts = this.defaults.id.split('_');
			var routeConf = { 
				route : parts[0],
				routeParams : {
					controller : parts.slice(1).join('_')
				}
			};
			return routeConf;
		},
		updateMenu : function() {
			// Show edit menu with this page selected
			this.getParentControl().onStepShow(this.stepId(), this._updateMenuData());
		},
		_updateMenuData : function() {
			return {};
		},
		stepId : function() {
			return this.defaults.id.replace('schedule_step_', '');
		},
		/**
		 * This is called for updating page content after load or refresh
		 */
		setContent : function(content, callback) {
			this.updateScheduleData();
			// Test if we should redirect to detail
			if (this.getParentControl().checkDetailRedirect()) {
				return;
			}
			
			this._super(content, callback);
		},
		_show : function() {
			this._super();
			this.updateMenu();
		},
		getParentControl : function() {
			if (this._parentControl === null) {
				this._parentControl = Cval.page.getControl(this.getElement().parents('.Cval-page:first'));
			}
			return this._parentControl;
		},
		scheduleData : function(){
			return this.getLoadedResponse().schedule;
		},
		updateScheduleData : function(data){
			if ( ! data)
				data = this.getLoadedResponse().schedule;
			else
				this.getLoadedResponse().schedule = data;

			// Propagate actual schedule data to parent control
			this.getParentControl().updateScheduleData(data);
		},
		/**
		 * This is called when going to next step
		 */
		goToForwardStep : function(step, callback) {
			Cval.core.callback(callback);
		},
		/**
		 * This is called when going to prev step
		 */
		goToBackwardStep : function(step, callback) {
			Cval.core.callback(callback);
		}
	}, {});

})(Cval, jQuery);