
(function(Cval, $) {
	
	Cval_page_control_schedule_step_negotiate_object = {
		staticParams : {
			defaults : {
				negotiateTriggerSelector : '.Cval-schedule-step-negotiate-trigger-negotiate',
				contactsTriggerSelector : '.Cval-schedule-step-negotiate-trigger-contacts',
				calendarsTriggerSelector : '.Cval-schedule-step-negotiate-trigger-calendars',
				prevTriggerSelector : '.Cval-schedule-step-negotiate-trigger-prev',
				nextTriggerSelector : '.Cval-schedule-step-negotiate-trigger-next'
			},
			_bindEvents : function() {
				var thiss = this;

				this._super();
				
				this.getElement().find('form:first').submit(function(e){
					e.preventDefault();
					// On form submit go to nextStep
					thiss.getParentControl().goToNextStep();
					return false;
				});
				
				this.getElement().find(this.defaults.negotiateTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss._negotiate();
					});
					
				this.getElement().find(this.defaults.contactsTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss.getParentControl().goToStep('contacts');
					});
					
				this.getElement().find(this.defaults.calendarsTriggerSelector)
					.Cval_button()
					.click(function(){
						Cval.page.show('calendars');
					});
					
				this.getElement().find(this.defaults.prevTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss.getParentControl().goToPrevStep();
					});
					
				this.getElement().find(this.defaults.nextTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss.getParentControl().goToNextStep();
					});
					
				Cval.comet.registerCallback('scheduleStepNegotiateRefresh', function(data){
					var scheduleData = data.schedule;
					if (scheduleData.id == thiss.defaults.showParams.id
							&& ( ! thiss.scheduleData() || (scheduleData.fields.update_hash.value != thiss.scheduleData().fields.update_hash.value))
							&& thiss.isVisible())
					{
						thiss.reload();
					}
				}, ['schedule_update']);
			},
			getLoadConfig : function() {
				return $.extend({}, this._super(), {
					data : {
						id : this.defaults.showParams.id
					}
				});
			},
			_updateMenuData : function() {
				return $.extend({}, this._super(), {
					next : {
						label : Cval.i18n.get('Step 5: Select time')
					},
					prev : true
				});
			},
			/**
			* This is called when going to next step
			*/
			goToForwardStep : function(step, callback) {
				var parentControl = this.getParentControl(),
					state = parentControl.getState();
				
				if (state >= parentControl.state.TIME_NOT_SELECTED) {
					Cval.core.callback(callback);
					return;
				}
				
				if (state < parentControl.state.NEGOTIATING) {
					Cval.message.error(Cval.i18n.get('Proposed times must be sent to guests.'));
				} else if (state == parentControl.state.NEGOTIATING) {
					Cval.message.error(Cval.i18n.get('Please wait until Cval collects free time from guests.'));
				} else {
					Cval.message.error(Cval.i18n.get('Availability is not checked yet.'));
				}
		
				this.getParentControl().highlightStepMenu(this.stepId());
			},
			/**
			* This is called when going to previous step
			*/
			goToBackwardStep : function(step, callback) {
				var parentControl = this.getParentControl(),
					state = parentControl.getState();
				
				if (state != parentControl.state.NEGOTIATING) {
					Cval.core.callback(callback);
					return;
				}
				
				Cval.message.error(Cval.i18n.get('Please wait until Cval collects free time from guests.'));
				this.getParentControl().highlightStepMenu(this.stepId());
			},
			_negotiate : function(callback) {
				var thiss = this;

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'negotiate'
					},
					data : {
						id : thiss.defaults.showParams.id
					}
				}, function(json) {
					if (json.message !== undefined)
					{
						Cval.message.fromOptions(json.message);
					}
					
					thiss.updateScheduleData(json.schedule);
					thiss.reload(callback);
				});
			}
		},
		params : {}
	};
	
	$.Class.extend('Cval.page.control.schedule_step_negotiate', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedule_step_def', function(){
					
					Cval.page.control.schedule_step_def.extend('Cval.page.control.schedule_step_negotiate', 
						Cval_page_control_schedule_step_negotiate_object.staticParams,
						Cval_page_control_schedule_step_negotiate_object.params
					);
					
					Cval.page.control.schedule_step_negotiate.init(callback);
				})
			})
		}
	}, {});

})(Cval, jQuery);