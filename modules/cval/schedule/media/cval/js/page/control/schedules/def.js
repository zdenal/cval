(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.schedules_def', {
		defaults : {
			/**
			 * Page holder
			 */
			parentElement : '.Cval-page-id-schedules-content',
			/**
			 * Page main element classes. It must be same for all instances,
			 * otherwise this.closeOtherPages will not work properly.
			 */
			pageClass : 'Cval-page-id-schedules-page',
			menuSelector : '.Cval-menu-id-schedules',
			menuVerticalSelector : '.Cval-menu-vertical-id-schedules'
		},
		_parentControl : null,
		refreshSchedules : function(callback) {
			// Implement this, to refresh schedules list, timetable or etc.
			// Now just call the callback
			Cval.core.callback(callback);
		},
		_beforeDisplay : function(callback) {
			var thiss = this;

			Cval.page.show('schedules', null, function(){
				Cval.core.callback(callback);
			}, thiss.defaults.id);
		},
		getUrlFromId : function() {
			var parts = this.defaults.id.split('_');
			var routeConf = { 
				route : parts[0],
				routeParams : {
					controller : parts.slice(1).join('_')
				}
			};
			return routeConf;
		},
		updateMenu : function() {
			// To implement
		},
		_show : function() {
			this._super();
			this.updateMenu();
		},
		getParentControl : function() {
			if (this._parentControl === null) {
				this._parentControl = Cval.page.getControl(this.getElement().parents('.Cval-page:first'));
			}
			return this._parentControl;
		}
	}, {});

})(Cval, jQuery);