
(function(Cval, $) {
	
	Cval_page_control_schedule_step_send_object = {
		staticParams : {
			defaults : {
				guestControlsSelector : '.Cval-schedule-guests-controls',
				guestSelectAllSelector : '.Cval-schedule-guests-list-controls .Cval-selectall',
				guestListSelector : '.Cval-schedule-guests-wrapper',
				guestListControlsSelector : '.Cval-schedule-guests-list-controls',
				
				sendTriggerSelector : '.Cval-schedule-step-send-trigger-time_check',
				prevTriggerSelector : '.Cval-schedule-step-send-trigger-prev',
		
				propertiesTriggerSelector : '.Cval-schedule-step-send-trigger-properties',
				contactsIconTriggerSelector : '.Cval-schedule-step-send-icon-trigger-contacts',
				contactsTriggerSelector : '.Cval-schedule-step-send-trigger-contacts',
				calendarsTriggerSelector : '.Cval-schedule-step-send-trigger-calendars',
				planTriggerSelector : '.Cval-schedule-step-send-trigger-plan',
				timeTriggerSelector : '.Cval-schedule-step-send-trigger-time'
			},
			guestLastRefresh : {
				data : null
			},
			guestPaginationEl : null,
			guestListControlsEl : null,
			init : function(callback) {
				Cval.core.callback(callback);
			},
			_show : function() {
				this._super();
				this.refreshGuestList();
			},
			_bindEvents : function() {
				var thiss = this;

				this._super();
				
				this.getElement().find('form:first').submit(function(e){
					e.preventDefault();
					// On form submit send
					thiss._send();
					return false;
				});
				
				this.getElement().find(this.defaults.sendTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss._send();
					});
					
				this.getElement().find(this.defaults.prevTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss.getParentControl().goToPrevStep();
					});
					
				this.getElement().find(this.defaults.propertiesTriggerSelector)
					.click(function(){
						thiss.getParentControl().goToStep('properties');
					});
					
				this.getElement().find(this.defaults.contactsIconTriggerSelector)
					.click(function(){
						thiss.getParentControl().goToStep('contacts');
					});
					
				this.getElement().find(this.defaults.contactsTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss.getParentControl().goToStep('contacts');
					});
					
				this.getElement().find(this.defaults.calendarsTriggerSelector)
					.Cval_button()
					.click(function(){
						Cval.page.show('calendars');
					});
					
				this.getElement().find(this.defaults.planTriggerSelector)
					.Cval_button()
					.click(function(){
						thiss.getParentControl().goToStep('plan');
					});
					
				this.getElement().find(this.defaults.timeTriggerSelector)
					.click(function(){
						thiss.getParentControl().goToStep('time');
					});
					
				Cval.comet.registerCallback('scheduleStepSendRefresh', function(data){
					var scheduleData = data.schedule;
					if (scheduleData.id == thiss.defaults.showParams.id
							&& ( ! thiss.scheduleData() || (scheduleData.fields.update_hash.value != thiss.scheduleData().fields.update_hash.value))
							&& thiss.isVisible())
					{
						var parentControl = thiss.getParentControl();
						if (parentControl.getState(scheduleData) >= parentControl.state.SENT) {
							parentControl.redirectToDetail(null, {
								justSent : true
							});
						} else {
							thiss.reload();
						}
					}
				}, ['schedule_update']);
				
				// Hover effects for table rows
				this.getElement().find(this.defaults.guestListSelector+' tbody tr').livequery(function(){
					$(this).hover(function(){
						$(this).addClass('Cval-hover');
					}, function(){
						$(this).removeClass('Cval-hover')
					});

					$(this).find('.Cval-list-checkbox').change(function(){
						thiss.guestListUpdate();
					});
				});
				
				this._initGuestListControls();
			},
			/**
			* This is called for updating page content after load or refresh
			*/
			setContent : function(content, callback) {
				this._super(content, callback);
				
				// Test if redirecting to detail
				if (this.getParentControl().checkDetailRedirect()) {
					Cval.message.success('Schedule was sent.');
				}
			},
			getLoadConfig : function() {
				return $.extend({}, this._super(), {
					data : {
						id : this.defaults.showParams.id
					}
				});
			},
			_updateMenuData : function() {
				var thiss = this;
				
				return $.extend({}, this._super(), {
					next : {
						label : Cval.i18n.get('Send it!'),
						callback : function(){
							thiss._send();
						}
					},
					prev : true
				});
			},
			/**
			* This is called when going to next step
			*/
			goToForwardStep : function(step, callback) {
				var parentControl = this.getParentControl(),
					state = parentControl.getState();
				
				if (state >= parentControl.state.SENT) {
					Cval.core.callback(callback);
					return;
				}
				
				if (state < parentControl.state.SENDING) {
					Cval.message.error(Cval.i18n.get('Schedule must be sent to guests.'));
				} else if (state == parentControl.state.SENDING) {
					Cval.message.error(Cval.i18n.get('Please wait until Cval sends schedule to guests.'));
				} else {
					Cval.message.error(Cval.i18n.get('Schedule time is not suitable for guests.'));
				}
		
				this.getParentControl().highlightStepMenu(this.stepId());
			},
			/**
			* This is called when going to previous step
			*/
			goToBackwardStep : function(step, callback) {
				if ( ! this._isSending()) {
					Cval.core.callback(callback);
					return;
				}
				
				Cval.message.error(Cval.i18n.get('Please wait until Cval sends schedule to guests.'));
				this.getParentControl().highlightStepMenu(this.stepId());
			},
			_send : function(callback) {
				var thiss = this;

				if (this._isSending()) {
					Cval.message.error(Cval.i18n.get('Cval is already sending schedule to guests.'));
					return;
				}

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'time_check'
					},
					data : {
						id : thiss.defaults.showParams.id
					}
				}, function(json) {
					if (json.message !== undefined)
					{
						Cval.message.fromOptions(json.message);
					}
					
					thiss.updateScheduleData(json.schedule);
					thiss.reload(callback);
				});
			},
			_isSending : function() {
				var parentControl = this.getParentControl(),
					state = parentControl.getState();
				
				if (state < parentControl.state.SENT
					&& state != parentControl.state.SENDING
					&& state != parentControl.state.TIME_CHECKING
					&& state != parentControl.state.TIME_CHECKED
				) {
					return false;
				}
				
				return true;
			},
			_initGuestListControls : function() {
				var thiss = this,
					listControls = this.getElement()
						.find(this.defaults.guestListControlsSelector);

				this.guestPaginationEl = listControls.find('.Cval-paginator')
					.Cval_pagination({
						clickCallback : function(page) {
							thiss.guestPaginateResults(page);
						}
					});

				this.guestListControlsEl = listControls;
			},
			guestPaginateResults : function(page) {
				this.refreshGuestList(null, $.extend({}, this.guestLastRefresh.data || {}, {
					pagination : page
				}));
			},
			refreshGuests : function(callback) {
				this.refreshGuestList(callback);
			},
			refreshGuestList : function(callback, data) {
				var thiss = this;

				data = $.extend({}, {
					body_only : 1,
					schedule : thiss.defaults.showParams.id
				}, data || {});

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'send_guests_table'
					},
					data : data
				}, function(json) {
					thiss.guestLastRefresh = {
						data : data
					};
					
					thiss.updateGuestList(json, false, callback);
				});
			},
			updateGuestList : function(data, complete, callback) {
				var table = this.getElement().find(this.defaults.guestListSelector);

				// Display, show no guest message
				if (data.count > 0)
					this.noGuestMessageHide();
				else
					this.noGuestMessageShow();
				// Display/show table header
				table.toggle(data.count > 0);

				// Replace only body content
				if ( ! complete)
					table = table.find('tbody');

				table.html(data.html);
				// Inject js data
				Cval.ajax.data.inject(data);

				// update pagination
				this.guestPaginationEl.Cval_pagination('update', data.pagination);
				this.guestListUpdate();
				
				// Display, show list controls
				if (data.count > 0)
					this.guestListControlsEl.show();
				else
					this.guestListControlsEl.hide();

				Cval.core.callback(callback);
			},
			guestListUpdate : function() {
				// Nothing needs update after refresh now
			},
			noGuestMessageShow : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-guests';

				if (thiss.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-noguest').length)
					return;

				var content = thiss.getElement().find('.'+selectorPrefix+'-message-noguest-content').html();
				
				thiss.getElement().find('.'+selectorPrefix+'-info-box')
					.html(content);
			},
			noGuestMessageHide : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-guests';
				
				thiss.getElement().find('.'+selectorPrefix+'-info-box').html('');
			}
		},
		params : {}
	};
	
	$.Class.extend('Cval.page.control.schedule_step_send', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedule_step_def', function(){
					
					Cval.page.control.schedule_step_def.extend('Cval.page.control.schedule_step_send', 
						Cval_page_control_schedule_step_send_object.staticParams,
						Cval_page_control_schedule_step_send_object.params
					);
					
					Cval.page.control.schedule_step_send.init(callback);
				})
			})
		}
	}, {});

})(Cval, jQuery);