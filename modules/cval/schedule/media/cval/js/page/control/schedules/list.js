(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.schedules_list', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedules_def', function(){
					
					
					Cval.page.control.schedules_def.extend('Cval.page.control.schedules_list', {
						defaults : {
							refreshOnShow : false,
							
							scheduleLoadingSelector : '.Cval-trigger-loading',
							scheduleRemoveSelector : '.Cval-trigger-remove',
							scheduleEditSelector : '.Cval-trigger-edit',
							scheduleListSelector : '.Cval-list-schedules-wrapper',
							scheduleSelectorPrefix : '.Cval-model-schedule-id-',
							
							selectedGroup : 'inbound',
							scheduleGroupSelectorPrefix : '.Cval-schedule_group-id-',

							positions : {
								modified : false
							}
						},
						lastRefresh : {
							data : null
						},
						checkerEl : null,
						paginationEl : null,
						addButtonEl : null,
						removeButtonEl : null,
						searchInputEl : null,
						init : function(callback) {
							var thiss = this;

							// Needed resources
							var resources = [
								Cval.route('cval/media', {
									file : 'css/page/schedules/list.css'
								})
							];

							Cval.resource.get(resources, function(){
								Cval.core.callback(callback);
							});	
						},
						show : function(params, callback) {
							if (params && params.group) {
								// Set active group
								this.defaults.selectedGroup = params.group;
							}
							this._super(params, callback);
						},
						_beforeDisplay : function(callback) {
							var thiss = this;

							thiss.defaults.showParams = {
								group : thiss.defaults.selectedGroup
							};
							Cval.page.show('schedules', thiss.defaults.showParams, function(){
								Cval.core.callback(callback);
							}, thiss.defaults.id);
						},
						_show : function() {
							var thiss = this;
							this._super();
							this.refreshScheduleList(null, true);
						},
						_hide : function() {
							this._super();
						},
						_bindEvents : function() {
							this._super();
							var thiss = this,
								parentControl = this.getParentControl();

							// Bind remove schedule
							this.getElement().find(this.defaults.scheduleRemoveSelector).livequery(function(){
								$(this).click(function(){
									var data = thiss.getScheduleEl($(this)).Cval_data(),
										groupData = thiss.getScheduleGroupElFromId(thiss.defaults.selectedGroup).Cval_data();

									Cval.dialog.loadControl('schedule_remove', function(){
										Cval.dialog.control.schedule_remove.show(null, {
											data : data,
											groupData : groupData
										})
									});
									return false;
								})
							});
							
							// Bind edit schedule
							this.getElement().find(this.defaults.scheduleEditSelector).livequery(function(){
								$(this).click(function(){
									var data = thiss.getScheduleEl($(this)).Cval_data();

									Cval.page.show('schedule/create', {
										id : data.id
									});
									return false;
								})
							});
							
							// Bind loading schedule
							this.getElement().find(this.defaults.scheduleLoadingSelector).livequery(function(){
								$(this).click(function(){
									alert(Cval.todo);
									return false;
								})
							})
							
							this.addButtonEl = parentControl.getElement().find(parentControl.defaults.scheduleAddSelector)
								.click(function(){
									var groupData = thiss.getScheduleGroupElFromId(thiss.defaults.selectedGroup).Cval_data(),
										showParams = {};
										
									//if (groupData.fields.allow_outbound.value)
										showParams.group = groupData.id;
									
									Cval.page.show('schedule/create', showParams);
									return false;
								});
							
							this.removeButtonEl = parentControl.getElement().find(parentControl.defaults.scheduleRemoveSelector)
								.click(function(){
									var selectedSchedules = thiss.getSelectedSchedulesEls(),
										groupData = thiss.getScheduleGroupElFromId(thiss.defaults.selectedGroup).Cval_data(),
										multipleData = [],
										multiple = true,
										data = null;
									
									$.each(selectedSchedules, function(){
										var data = $(this).Cval_data();
										multipleData.push(data);
									});
									
									if (multipleData.length < 2) {
										multiple = false;
										data = multipleData.shift();
									} else {
										data = multipleData;
									}
									
									Cval.dialog.loadControl('schedule_remove', function(){
										Cval.dialog.control.schedule_remove.show(null, {
											data : data,
											groupData : groupData,
											multiple : multiple
										})
									});
									return false;
								});

							var table = this.getElement().find(this.defaults.scheduleListSelector);
							// Hover effects for table rows
							table.find('tbody tr').livequery(function(){
								$(this).hover(function(){
									$(this).addClass('Cval-hover');
								}, function(){
									$(this).removeClass('Cval-hover')
								});
								
								$(this).find('.Cval-list-checkbox').change(function(){
									thiss.listUpdate();
								});
							});
							
							table.find('td').livequery(function(){
								if ($(this).hasClass('Cval-column-listcheck') || $(this).hasClass('Cval-column-actions'))
									return;
								
								$(this).click(function(){
									var data = thiss.getScheduleEl($(this)).Cval_data();
									
									if (data.fields.state.value >= Cval.core.config('schedule').states.SENT) {
										Cval.page.show('schedule', {
											id : data.id
										});
									} else {
										Cval.page.show('schedule/create', {
											id : data.id
										});
									}
									return false;
								});
							});
							
							this._initSearch();
							this._initListControls();
							
							Cval.comet.registerCallback('scheduleListRefresh', function(data){
								if (thiss.isVisible())
									thiss.refreshScheduleList(null, true);
							}, ['schedule_update', 'schedule_delete']);
						},
						refreshSchedules : function(callback, updateGroups) {
							this.refreshScheduleList(callback, updateGroups);
						},
						refreshScheduleList : function(callback, updateGroups, data) {
							var thiss = this;

							data = $.extend({}, {
								body_only : 1,
								group : thiss.defaults.selectedGroup,
								update_groups : updateGroups ? 1 : 0,
								search : thiss._searchData()
							}, data || {});

							Cval.ajax.callModal({
								route : 'schedule',
								routeParams : {
									controller : 'list_table'
								},
								data : data
							}, function(json) {
								thiss.lastRefresh = {
									data : data
								};
								
								thiss.updateScheduleList(json, false, callback);
								if (updateGroups)
									thiss.getParentControl().updateGroups();
							});
						},
						updateScheduleList : function(data, complete, callback) {
							var table = this.getElement().find(this.defaults.scheduleListSelector);

							// Display, show no schedule message
							if (data.count > 0)
								this.noScheduleMessageHide();
							else
								this.noScheduleMessageShow();
							// Display/show table header
							table.toggle(data.count > 0);

							// Replace only body content
							if ( ! complete)
								table = table.find('tbody');

							table.html(data.html);
							// Inject js data
							Cval.ajax.data.inject(data);
							
							// Enable/Disable checker
							this.checkerEl.Cval_button_checker({
								checked : false,
								disabled : data.count < 1
							});
							// update pagination
							this.paginationEl.Cval_pagination('update', data.pagination);
							this.listUpdate();

							Cval.core.callback(callback);
						},
						getScheduleEl : function(el) {
							el = $(el);

							if ( ! el.is('tr'))
								el = el.parents('tr:first');

							return el;
						},
						getScheduleElFromId : function(id) {
							var selectorClass = this.defaults.scheduleSelectorPrefix+id,
								el = $(this.getElement().find(this.defaults.scheduleListSelector+' tbody tr'+selectorClass));

							return el;
						},
						noScheduleMessageShow : function() {
							var thiss = this;
								
							var groupData = this.getScheduleGroupElFromId(this.defaults.selectedGroup).Cval_data(),
								isSearchActive = this.isSearchActive(),
								messageElClass = 'Cval-list-schedules-message-noschedule_'+(isSearchActive ? 'search' : groupData.id),
								content = thiss.getElement().find('.'+messageElClass+'-content');
								
							if ( ! content.length) {
								messageElClass = 'Cval-list-schedules-message-noschedule',
								content = thiss.getElement().find('.'+messageElClass+'-content');
							}
							
							if (thiss.getElement().find('.Cval-list-schedules-info-box .Cval-list-schedules-message-noschedule-global.'+messageElClass).length)
								return;
							
							this.noScheduleMessageHide();
							
							var html = isSearchActive
									? Cval.i18n.get('No schedules found, try to modify your search phrase.')
									: content.html();
									
							Cval.message.info(html, null, {
								wrapper : thiss.getElement().find('.Cval-list-schedules-info-box'),
								autoDestroy : false,
								closer : false,
								outerClass : 'Cval-list-schedules-message-noschedule-global '+messageElClass
							});
						},
						noScheduleMessageHide : function() {
							var messageEl = this.getElement().find('.Cval-list-schedules-info-box .Cval-list-schedules-message-noschedule-global');
							Cval.message.destroy(messageEl, true);
						},
						_initListControls : function() {
							var thiss = this,
								listControls = this.getParentControl()
									.getElement()
									.find('.Cval-schedule-list-controls');
							
							this.checkerEl = listControls.find('.Cval-selectall')
								.Cval_button_checker({
									disabled : true,
									clickCallback : function(check, button) {
										thiss.getElement().find(thiss.defaults.scheduleListSelector)
											.find('tbody tr .Cval-list-checkbox')
											.attr('checked', check);
										thiss.listUpdate();
									}
								})

							this.paginationEl = listControls.find('.Cval-paginator')
								.Cval_pagination({
									clickCallback : function(page) {
										thiss.paginateResults(page);
									}
								});
								
							listControls.show();
						},
						paginateResults : function(page) {
							this.refreshScheduleList(null, null, $.extend({}, this.lastRefresh.data || {}, {
								pagination : page
							}));
						},
						updateMenu : function() {
							$(this.defaults.menuVerticalSelector).Cval_menu_vertical('select', '.Cval-menu-item-schedules_group_'+this.defaults.selectedGroup);
							var groupData = this.getScheduleGroupElFromId(this.defaults.selectedGroup).Cval_data(),
								parentControl = this.getParentControl();
								
							parentControl.scheduleGroupControls.remove.toggleClass('ui-state-disabled', ! groupData.fields.deletable.value);
							parentControl.scheduleGroupControls.edit.toggleClass('ui-state-disabled', ! groupData.fields.editable.value);
							//parentControl.scheduleControls.add.Cval_button('option', 'disabled', ! groupData.fields.allow_outbound.value);
						},
						_initSearch : function() {
							var thiss = this,
								form = this.getParentControl().getElement().find('form.Cval-form-search');
							
							this.searchInputEl = form.find('input[name=search]');
							form.submit(function(e){
								e.preventDefault();
								thiss.refreshScheduleList();
								return false;
							});
						},
						_searchData : function() {
							return {
								global : this.searchInputEl.val().trim()
							};
						},
						isSearchActive : function() {
							return this.lastRefresh.data.search.global.length > 0;
						},
						listUpdate : function() {
							var thiss = this;
							
							var checkedCount = thiss.getSelectedSchedulesEls().length;
							
							this.removeButtonEl.Cval_button('option', 'disabled', checkedCount < 1);
						},
						getScheduleGroupElFromId : function(id) {
							var selectorClass = this.defaults.scheduleGroupSelectorPrefix+id,
								el = $(this.defaults.menuVerticalSelector).find(selectorClass);

							return el;
						},
						getSelectedSchedulesEls : function() {
							return this.getElement().find(this.defaults.scheduleListSelector)
								.find('tbody tr')
								.filter(function(){
									return $(this).find('.Cval-list-checkbox').is(':checked')
								});
						}
					}, {});
					
					
					Cval.page.control.schedules_list.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);