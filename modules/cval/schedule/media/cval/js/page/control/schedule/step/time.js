
(function(Cval, $) {
	
	Cval_page_control_schedule_step_time_object = {
		staticParams : {
			defaults : {
				calendarTimetableSelector : '.Cval-timetable-schedule-time-wrapper',
				timetableInitiated : false,
				
				selectedTimeEventClass : 'Cval-fullcalendar-event-schedule-time-selected',
				selectedTimeEventIDPrefix : 'Cval-schedule-selected-time-',
				selectedTimeEventColor : 'green',
				
				selectedTimeSliderSelector : '.Cval-schedule-time-selected-slider',
				selectedTimeStartSelector : '.Cval-schedule-time-selected-start',
				selectedTimeEndSelector : '.Cval-schedule-time-selected-end',
				selectedTimeStartDateSelector : '.Cval-schedule-time-selected-startdate',
				selectedTimeEndDateSelector : '.Cval-schedule-time-selected-enddate',
				
				selectedTimeYesSelector : '.Cval-schedule-time-selected-yes',
				selectedTimeNoSelector : '.Cval-schedule-time-selected-no',

				helpSelector: '.Cval-schedule-step-help',
				helpTriggerSelector: '.Cval-schedule-step-help-trigger'
			},
			selectedTimeSliderEl : null,
			selectedTimeStartEl : null,
			selectedTimeEndEl : null,
			selectedTimeStartDateEl : null,
			selectedTimeEndDateEl : null,
			selectedTimeFormat : null,
			selectedTimeDateFormat : null,
			freePeriods : {},
			minLengthMinutes : null,
			scheduleLimits : null,
			selectedTimeEventID : null,
			selectedTime : null,
			init : function(callback) {
				var thiss = this;

				// Needed resources
				var resources = [
					Cval.route('cval/media', {
						file : 'jquery/css/jquery.multiselect.css'
					}),
					Cval.route('cval/media', {
						file : 'jquery/js/jquery.multiselect.js'
					}),
					Cval.route('cval/media', {
						file : 'js/jquery/ui/slider.js'
					}),
					Cval.route('cval/media', {
						file : 'css/page/schedule/step/time.css'
					})
				];

				Cval.async('fullcalendar', function(){
					Cval.resource.get(resources, function(){
						Cval.core.callback(callback);
					});	
				});
			},
			_show : function() {
				this._super();
				this.refreshTimetable();
			},
			_hide : function() {
				this.defaults.timetableInitiated = false;
				this._super();
			},
			_updateMenuData : function() {
				return $.extend({}, this._super(), {
					next : {
						label : Cval.i18n.get('Step 6: Preview & Send')
					},
					prev : true
				});
			},
			_bindEvents : function() {
				var thiss = this;

				// Store schedule limits
				this.scheduleLimits = this.getLoadedResponse().schedule_limits;
				this.minLengthMinutes = this.getLoadedResponse().schedule_plan.fields.min_length.value;
				this._updateFreePeriodsFromServer(this.getLoadedResponse().free_periods);
				
				this._super();

				var helpEl = this.getElement().find(this.defaults.helpSelector);
				this.getElement().find(this.defaults.helpTriggerSelector).click(function(){
					helpEl.toggle();
				});
				
				this.getElement().find('form:first').submit(function(e){
					e.preventDefault();
					// On form submit go to nextStep
					thiss.getParentControl().goToNextStep();
					return false;
				})
				
				this._initSelectedTime();
			},
			getLoadConfig : function() {
				return $.extend({}, this._super(), {
					data : {
						id : this.defaults.showParams.id
					}
				});
			},
			/**
			* This is called when going to next step
			*/
			goToForwardStep : function(step, callback) {
				this._nextClick(callback);
			},
			_nextClick : function(callback) {
				var thiss = this,
					data = this._saveCallData();
				
				if (this.getParentControl().getStep(this.stepId()).valid 
						&& Cval.helper.compare(data, this.onLoadData))
				{
					Cval.core.callback(callback);
					return;
				}
				
				data.id = this.defaults.showParams.id;

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'step_time',
						action : 'submit'
					},
					data: data
				}, function(json) {
					thiss._nextClickSuccess(json, callback);
				}, null, {
					440 : function (errorThrown, JSONobject, XMLHttpRequest) {
						Cval.ajax._error[440].apply(null, arguments);
						thiss.getParentControl().highlightStepMenu(thiss.stepId())
					}
				})

				return;
			},
			_nextClickSuccess : function(json, callback) {
				if (json.message !== undefined)
				{
					Cval.message.fromOptions(json.message);
				}
				
				// Propagate actual schedule data to parent control
				this.updateScheduleData(json.schedule);
				
				Cval.core.callback(callback);
			},
			/**
			* Returns data from dialog you want send to server after save is invoked.
			* Should be overidden in subclasses.
			*/
			_saveCallData : function() {
				var data = this.getElement().find('form:first').serializeJSON();
				
				delete data.start_time;
				delete data.end_time;
				
				if (this.selectedTime) {
					data.selected_time = [
						Cval.helper.date.toString(this.selectedTime[0]),
						Cval.helper.date.toString(this.selectedTime[1])
					];
				}
				
				return $.extend({}, data);
			},
			_updateFreePeriodsFromServer : function(periods) {
				var thiss = this;
				
				this.freePeriods = {};
				$.each(periods, function(key, period){
					var startDate = Date.parse(period[0]),
						endDate = Date.parse(period[1]);
					
					thiss.freePeriods[startDate.getTime()] = [startDate, endDate];
					//$.Console.Debug(startDate, endDate)
				});
			},
			_initSelectedTime : function(){
				var thiss = this;
				
				this.selectedTimeFormat = Cval.core.config('time_24_hour') ? 'H:mm' : 'h:mm tt';
				this.selectedTimeDateFormat = Cval.core.config('date_month_second') ? 'd/M/yyyy' : 'M/d/yyyy';
				
				this.selectedTimeStartEl = this.getElement().find(this.defaults.selectedTimeStartSelector);
				this.selectedTimeEndEl = this.getElement().find(this.defaults.selectedTimeEndSelector);
				this.selectedTimeStartDateEl = this.getElement().find(this.defaults.selectedTimeStartDateSelector);
				this.selectedTimeEndDateEl = this.getElement().find(this.defaults.selectedTimeEndDateSelector);
				
				var lastSliderPos = [];
				
				this.selectedTimeSliderEl = this.getElement().find(this.defaults.selectedTimeSliderSelector)
					.Cval_slider({
						range: true,
						min: 0,
						max: 1440,
						step: 15,
						values: [0,1440],
						start: function(e, ui) {
							lastSliderPos = ui.values;
						},
						slide: function(e, ui) {
							var updatedValues = thiss._normalizeSliderValues(ui.values, lastSliderPos);
							// Set only time range inputs
							thiss._setSelectedTimeInputs(updatedValues);
							return true;
						},
						stop: function(e, ui) {
							var updatedValues = thiss._normalizeSliderValues(ui.values, lastSliderPos);
							// Update complete event, so inputs and slider also
							thiss.normalizeSelectedTimeEvent(null, null, updatedValues);
						}
					});
					
				this.selectedTimeStartEl.change(function(){
					var val = thiss._getSelectedTimeInputsDates($(this).val());
					if (val === false) {
						thiss._setSelectedTimeInputs();
						return;
					}
					
					thiss.normalizeSelectedTimeEvent(null, null, val);
				});
				
				this.selectedTimeEndEl.change(function(){
					var val = thiss._getSelectedTimeInputsDates($(this).val());
					if (val === false) {
						thiss._setSelectedTimeInputs();
						return;
					}
					
					thiss.normalizeSelectedTimeEvent(null, null, val);
				});
			},
			_normalizeSliderValues : function(values, lastValues) {
				var start = values[0],
					end = values[1],
					collide = false;

				// If sliders are in same position
				if (start == end) {
					collide = true;
					if (start == 0)
						end++;
					else if (end == 1440)
						start--;
					else if (start == lastValues[0]) // left was moved
						end++;
					else
						start--;
				}
				
				var updatedValues = [
					( ! collide && start == lastValues[0] )
						? this.selectedTime[0].clone()
						: this.selectedTime[0].clone().clearTime().addMinutes(start),
					( ! collide && end == lastValues[1] )
						? this.selectedTime[1].clone()
						: this.selectedTime[1].clone().clearTime().addMinutes(end)
				];
				
				return updatedValues;
			},
			_setSelectedTimeInputs : function(values){
				if (values === undefined) {
					values = [
						this.selectedTime[0].clone(),
						this.selectedTime[1].clone()
					]
				}
				
				this.selectedTimeStartEl.val(this._formatDateForSelectedTimeInput(values[0]));
				this.selectedTimeEndEl.val(this._formatDateForSelectedTimeInput(values[1]));
				this.selectedTimeStartDateEl.text(this._formatDateForSelectedTimeDateInput(values[0]));
				this.selectedTimeEndDateEl.text(this._formatDateForSelectedTimeDateInput(values[1]));
			},
			_formatDateForSelectedTimeInput : function(val){
				return val.toString(this.selectedTimeFormat).toLowerCase();
			},
			_formatDateForSelectedTimeDateInput : function(val){
				return val.toString(this.selectedTimeDateFormat).toLowerCase();
			},
			_getSelectedTimeInputsDates : function(){
				var startTime = Date.parse(this.selectedTimeStartEl.val());
				if ( ! startTime)
					return false;
				
				var endTime = Date.parse(this.selectedTimeEndEl.val());
				if ( ! endTime)
					return false;
				
				return [
					this.selectedTime[0].clone().clearTime().addMinutes(startTime.getHours() * 60 + startTime.getMinutes()),
					this.selectedTime[1].clone().clearTime().addMinutes(endTime.getHours() * 60 + endTime.getMinutes()),
				];
			},
			_setSelectedTimeSlider : function(values){
				if (values === undefined) {
					values = [
						this.selectedTime[0].clone(),
						this.selectedTime[1].clone()
					]
				}
				
				this.selectedTimeSliderEl.slider("values", 0, values[0].getHours() * 60 + values[0].getMinutes());
				this.selectedTimeSliderEl.slider("values", 1, values[1].getHours() * 60 + values[1].getMinutes());
			},
			refreshTimetable : function(callback) {
				if ( ! this.defaults.timetableInitiated)
					this._timetableInit();
				else
					this.getTimetableEl().fullCalendar('refetchEvents');
				Cval.core.callback(callback);
			},
			timetableLimits : function() {
				return [
					Date.now().addSeconds(this.scheduleLimits[0]),
					Date.now().addSeconds(this.scheduleLimits[1]) 
				];
			},
			limitFreePeriods : function(useLimits) {
				var thiss = this,
					limits = useLimits || this.timetableLimits();
				
				$.each(this.freePeriods, function(timestamp, period){
					// Delete out of limits periods
					if (period[0] >= limits[1] || period[1] <= limits[0])
						delete thiss.freePeriods[timestamp];
					
					// Make sure that periods starts in limits
					if (period[0] < limits[0])
						period[0].setTime(limits[0].getTime());
					
					// Delete shorter periods than min schedule length
					if (((period[1].getTime() - period[0].getTime()) / (1000*60))
							< this.minLengthMinutes) {
						delete thiss.freePeriods[timestamp];	
					}
						
				});
			},
			_timetableInit : function() {
				var thiss = this;
				this.defaults.timetableInitiated = true;
				this.selectedTimeEventID = this.defaults.selectedTimeEventIDPrefix+Date.now().getTime();

				var scheduleObjectFields = this.getLoadedResponse().schedule.fields,
					hasDate = scheduleObjectFields.has_date.value,
					initDate;
					
				if (hasDate) {
					initDate = Date.parse(scheduleObjectFields.start.string_value);
				} else {
					$.each(this.freePeriods, function(){
						initDate = this[0];
						return false;
					});
				}
				if (!initDate)
					initDate = Date.now();

				// Hover effects for table rows
				this.getTimetableEl().fullCalendar($.extend(true, {}, Cval.fullcalendar.defaults, {
					header: {
						right: 'agendaWeek,agendaDay'
					},
					events: {
						data: {
							cval_events_exclude: thiss.scheduleData().id 
						}
					},
					eventDrop : function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
						var normalizedEvent = thiss.normalizeSelectedTimeEvent(view, event);
						if ( ! normalizedEvent) {
							revertFunc();
							return;
						}
						
						event = normalizedEvent;
					},
					eventResize : function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
						var normalizedEvent = thiss.normalizeSelectedTimeEvent(view, event);
						if ( ! normalizedEvent) {
							revertFunc();
							return;
						}
						
						event = normalizedEvent;
					},
					viewDisplay: function(view) {
						thiss.fullcalendarViewDisplay.call(this, view, thiss);
					},
					eventAfterRender: function(event, element, view) {
						Cval.fullcalendar.defaults.eventAfterRender.apply(this, arguments);
						
						if (event.id != thiss.selectedTimeEventID) {
							return;
						}
						
						element.find('.fc-event-head')
							.addClass('ui-state-inverse')
							.append(
								$('<span/>')
									.addClass('ui-icon ui-icon-close')
									.css({
										position : 'absolute',
										top : '-3px',
										right : '-2px'
									})
									.click(function(){
										// Remove event
										thiss.getTimetableEl().fullCalendar('removeEvents', thiss.selectedTimeEventID);
										// Clear selected time
										thiss.selectedTime = null;
										thiss.afterSelectedTimeSet();
									})
							);
					}
				}, Cval.fullcalendar.getInitCurrentDateFromDate(initDate)));
					
				if (hasDate) {
					this.createSelectedTimeEvent(null, 
						Date.parse(scheduleObjectFields.start.string_value),
						Date.parse(scheduleObjectFields.end.string_value)
					);
				} else {
					// Only init default values
					this.afterSelectedTimeSet();
				}
				
				this.onLoadData = this._saveCallData();
				
				$(window).resize();
			},
			getTimetableEl : function() {
				return this.getElement().find(this.defaults.calendarTimetableSelector);
			},
			fullcalendarViewDisplay : function(view, thiss) {
				// display only when timetable is initiated
				if ( ! thiss.defaults.timetableInitiated)
					return;
				
				if ( ! view)
					view = thiss.getTimetableEl().fullCalendar('getView');
				
				var limits = thiss.timetableLimits();
				
				Cval.fullcalendar.clearTimeRanges(view);
				
				var pastTimeEl = Cval.fullcalendar.getTimeRange(view, limits[0].clone().addYears(-10), limits[0]);
				if (pastTimeEl) {
					pastTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').click(function(){
						alert('Time in past can\'t be selected!');
					}).show();
				}
				
				var futureTimeEl = Cval.fullcalendar.getTimeRange(view, limits[1], limits[1].clone().addYears(10));
				if (futureTimeEl) {
					futureTimeEl.addClass('Cval-fullcalendar-timerange-item_disabled').click(function(){
						alert('Only time in limited future can be selected!');
					}).show();
				}
				
				thiss.limitFreePeriods(limits);
				
				$.each(thiss.freePeriods, function(){
					thiss.fullcalendarInsertPeriod(view, this);
				});
			},
			fullcalendarInsertPeriod : function(view, period) {
				var thiss = this;
				
				var	start = period[0],
					end = period[1],
					timeRangeEl = Cval.fullcalendar.getTimeRange(view, start, end),
					timestamp = start.getTime();
					
				if (timeRangeEl) {
					timeRangeEl.bind('fullcalendarClick', function(e, data, clickedDate, clickEvent){
						thiss.createSelectedTimeEventClick(view, data, clickedDate, clickEvent);
					}).addClass('Cval-fullcalendar-timerange-item_highlight Cval-helper-cursor-pointer')
						.addClass('Cval-fullcalendar-schedule-time-period-'+timestamp).show();
				}
			},
			createSelectedTimeEventClick : function(view, periodData, clickedDate, clickEvent) {
				return this.createSelectedTimeEvent(view, clickedDate, clickedDate.clone().addMinutes(this.minLengthMinutes));
			},
			createSelectedTimeEvent : function(view, start, end) {
				// only when timetable is initiated
				if ( ! this.defaults.timetableInitiated)
					return false;
				
				var timetableEl = this.getTimetableEl();
				
				if ( ! view)
					view = timetableEl.fullCalendar('getView');
				
				// Remove already created event
				timetableEl.fullCalendar('removeEvents', this.selectedTimeEventID);
				
				var	event = {
					id : this.selectedTimeEventID,
					title : this.scheduleData().label,
					start : start.clone(),
					end : end.clone(),
					editable : true,
					className : this.defaults.selectedTimeEventClass,
					color : this.defaults.selectedTimeEventColor
				};
				
				var normalizedEvent = this.normalizeSelectedTimeEvent(view, event);
				
				if ( ! normalizedEvent)
					return false;
				
				// Add new event
				timetableEl.fullCalendar('renderEvent', normalizedEvent, true);
				
				return true;
			},
			normalizeSelectedTimeEvent : function(view, event, update) {
				var timetableEl = this.getTimetableEl(),
					eventDefined = event ? true : false;
				
				if ( ! view)
					view = timetableEl.fullCalendar('getView');
				
				if ( ! eventDefined) {
					var clientEvents = timetableEl.fullCalendar('clientEvents', this.selectedTimeEventID);

					if (clientEvents.length) {
						event = clientEvents[0];
					}
				}
				
				if ( ! event) {
					// Propagate changes
					this.afterSelectedTimeSet();
					return false;
				}
				
				if (update) {
					event.start.setTime(update[0].getTime());
					event.end.setTime(update[1].getTime());
				}
				
				var bestPeriod = null,
					eventLength = event.end.getTime() - event.start.getTime();
				
				$.each(this.freePeriods, function(timestamp){
					// Break loop if event ends before this period
					if (event.end <= this[0])
						return false;
					// Continue to next period if event start after this one
					if (event.start >= this[1])
						return true;
					
					// Now we know event is in this period
					//if (event.start < this.end && event.end > this.start)
					var overlap = Math.min(event.end.getTime(), this[1].getTime())
							- Math.max(event.start.getTime(), this[0].getTime());
					if ( ! bestPeriod || overlap > bestPeriod.overlap) {
						bestPeriod = {
							timestamp : timestamp,
							overlap : overlap,
							period : this
						}
					}
				});
				
				if ( ! bestPeriod) {
					// Propagate changes
					this.afterSelectedTimeSet();
					return false;
				}
				
				// Event is over period from start
				if (bestPeriod.period[0] > event.start) {
					event.start.setTime(bestPeriod.period[0].getTime());
					event.end.setTime(event.start.getTime() + eventLength);
				}
				
				// Event is over period from end
				if (bestPeriod.period[1] < event.end) {
					event.end.setTime(bestPeriod.period[1].getTime());
					event.start.setTime(event.end.getTime() - eventLength);
				}
				
				// Last check, shorten event from start, if over
				if (bestPeriod.period[0] > event.start) {
					event.start.setTime(bestPeriod.period[0].getTime());
				}
				
				// Update stored event values
				this.selectedTime = [event.start.clone(), event.end.clone()];
				
				if ( ! eventDefined) {
					// Event was not defined so we need to update it in calendar
					timetableEl.fullCalendar('updateEvent', event);
				}
				
				$.Console.Debug(this.selectedTime);
				// Propagate changes
				this.afterSelectedTimeSet();
				
				return event;
			},
			afterSelectedTimeSet : function() {
				var isSelected = this.selectedTime ? true : false;
				
				if ( ! isSelected) {
					// Hide slider and inputs
					this.getElement().find(this.defaults.selectedTimeYesSelector).hide();
					this.getElement().find(this.defaults.selectedTimeNoSelector).show();
					return;
				}
				
				// update inputs
				this._setSelectedTimeInputs();
				// update slider
				this._setSelectedTimeSlider();
				
				// Show slider and inputs
				this.getElement().find(this.defaults.selectedTimeNoSelector).hide();
				this.getElement().find(this.defaults.selectedTimeYesSelector).show();
			}
		},
		params : {}
	};
	
	$.Class.extend('Cval.page.control.schedule_step_time', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedule_step_def', function(){
					
					Cval.page.control.schedule_step_def.extend('Cval.page.control.schedule_step_time', 
						Cval_page_control_schedule_step_time_object.staticParams,
						Cval_page_control_schedule_step_time_object.params
					);
					
					Cval.page.control.schedule_step_time.init(callback);
				})
			})
		}
	}, {});

})(Cval, jQuery);