
(function(Cval, $) {
	
	Cval_page_control_schedules_object = {
		staticParams : {
			defaults : {
				scheduleControlsSelector : '.Cval-schedule-controls',
				scheduleAddSelector : '.Cval-schedule-add',
				scheduleRemoveSelector : '.Cval-schedule-remove',
				scheduleSelectAllSelector : '.Cval-schedule-selectall',
				
				activeSection : 'schedules_list',
				activeSectionParams : {},
				
				refreshOnShow : false,
				inHistory : false,
				loadUrl : {
					route : 'schedules_main'
				},
				menuSelector : '.Cval-menu-id-schedules',
				menuVerticalSelector : '.Cval-menu-vertical-id-schedules',
				scheduleGroupControlsSelector : '.Cval-schedule_group-controls'
			},
			scheduleControls : {
				add : null,
				remove : null
			},
			scheduleGroupControls : {
				add : null,
				remove : null,
				edit : null
			},
			show : function(params, callback, section, sectionParams) {
				if ( ! section) {
					Cval.page.show(this.defaults.activeSection, this.defaults.activeSectionParams);
					return;
				}

				// Set active section
				this.defaults.activeSection = section;
				this.defaults.activeSectionParams = sectionParams || {};

				this._super(params, callback);
			},
			_bindEvents : function() {
				this._super();
				var thiss = this;

				// Init add schedule button
				this.scheduleControls.add = this.getElement().find(this.defaults.scheduleAddSelector)
					.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-calculator',
							secondary : 'ui-icon-plus'
						}
					});
					
				// Init remove schedule button
				this.scheduleControls.remove = this.getElement().find(this.defaults.scheduleRemoveSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-trash'
						}
					});
					
				// Init select schedule button
				this.getElement().find(this.defaults.scheduleSelectAllSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-check'
						}
					});
				
				this.getElement().find('.Cval-schedule-controls').buttonset();
				//this.scheduleControls.add.Cval_button('option', 'disabled', true);
				this.scheduleControls.remove.Cval_button('option', 'disabled', true);
				
				var scheduleGroupControlsEl = this.getElement().find(this.defaults.scheduleGroupControlsSelector);
				this.scheduleGroupControls.add = scheduleGroupControlsEl.find('.Cval-trigger-create')
					.click(function(){
						Cval.dialog.loadControl('schedule_group_add', function(){
							Cval.dialog.control.schedule_group_add.show()
						});
						return false;
					});
				this.scheduleGroupControls.remove = scheduleGroupControlsEl.find('.Cval-trigger-remove')
					.addClass('ui-state-disabled')
					.click(function(){
						if ($(this).hasClass('ui-state-disabled'))
							return false;
						
						var childControl = thiss.getChildControl(),
							groupData = childControl
								.getScheduleGroupElFromId(childControl.defaults.selectedGroup).Cval_data();
						
						Cval.dialog.loadControl('schedule_group_remove', function(){
							Cval.dialog.control.schedule_group_remove.show(null, {
								data : groupData
							})
						});
						return false;
					});
				this.scheduleGroupControls.edit = scheduleGroupControlsEl.find('.Cval-trigger-edit')
					.addClass('ui-state-disabled')
					.click(function(){
						if ($(this).hasClass('ui-state-disabled'))
							return false;
						
						var childControl = thiss.getChildControl(),
							groupData = childControl
								.getScheduleGroupElFromId(childControl.defaults.selectedGroup).Cval_data();
						
						Cval.dialog.loadControl('schedule_group_edit', function(){
							Cval.dialog.control.schedule_group_edit.show(null, {
								data : groupData
							})
						});
						return false;
					});
			},
			refreshSchedules : function(callback, updateGroups) {
				// Propagate refreshing to subpage control
				this.getChildControl().refreshSchedules(callback, updateGroups);
			},
			getChildControl : function() {
				return Cval.page.getControl(this.defaults.activeSection);
			},
			updateGroups : function() {
				// Iterate over all groups items and
				this.getElement().find(this.defaults.menuVerticalSelector+' > li').each(function(){
					var data = $(this).Cval_data(),
						labelEl = $(this).find('a'),
						label = data.label+'',
						highlightCount = 0;
					
					if (data.fields && data.fields.highlighted_schedules_count)
						highlightCount = data.fields.highlighted_schedules_count.value;
					
					labelEl.toggleClass('Cval-helper-bold', highlightCount ? true : false);
					if (highlightCount) {
						label += ' ('+highlightCount+')';
					}
						
					labelEl.text(label);
				});
			}
		},
		params : {}
	};
	
	Cval.page.control.def.extend('Cval.page.control.schedules', 
		Cval_page_control_schedules_object.staticParams,
		Cval_page_control_schedules_object.params
	);

})(Cval, jQuery);