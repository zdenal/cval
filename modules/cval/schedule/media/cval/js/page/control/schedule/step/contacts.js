
(function(Cval, $) {
	
	Cval_page_control_schedule_step_contacts_object = {
		staticParams : {
			defaults : {
				nextSelector : '.Cval-trigger-next',
				
				contactControlsSelector : '.Cval-schedule-contacts-search-controls',
				contactAddSelector : '.Cval-schedule-contacts-search-controls .Cval-contact-add',
				contactRemoveSelector : '.Cval-schedule-contacts-search-controls .Cval-contact-remove',
				contactSelectAllSelector : '.Cval-schedule-contacts-search-list-controls .Cval-selectall',
				contactGroupSelectSelector : '.Cval-schedule-contacts-search-contact_groups-select',
				contactListSelector : '.Cval-schedule-contacts-search-wrapper',
				contactListControlsSelector : '.Cval-schedule-contacts-search-list-controls',
				contactAddToGuestsSelector : '.Cval-trigger-add-to-guests',
				
				guestControlsSelector : '.Cval-schedule-contacts-guests-controls',
				guestRemoveSelector : '.Cval-schedule-contacts-guests-controls .Cval-guest-remove',
				guestSelectAllSelector : '.Cval-schedule-contacts-guests-list-controls .Cval-selectall',
				guestListSelector : '.Cval-schedule-contacts-guests-wrapper',
				guestListControlsSelector : '.Cval-schedule-contacts-guests-list-controls',
				guestListRemoveSelector : '.Cval-trigger-remove',
				
				contactToGuestControlsSelector : '.Cval-schedule-contacts-search-to-guests-controls',
				contactToGuestAddSelector : '.Cval-schedule-contacts-search-to-guests-controls .Cval-contact-add',
				contactToGuestAddAllSelector : '.Cval-schedule-contacts-search-to-guests-controls .Cval-contact-addall'
			},
			contactLastRefresh : {
				data : null
			},
			contactCheckerEl : null,
			contactPaginationEl : null,
			contactRemoveButtonEl : null,
			contactListControlsEl : null,
			guestLastRefresh : {
				data : null
			},
			guestCheckerEl : null,
			guestPaginationEl : null,
			guestRemoveButtonEl : null,
			guestListControlsEl : null,
			contactToGuestAddButtonEl : null,
			contactToGuestAddAllButtonEl : null,
			contactSearchInputEl : null,
			init : function(callback) {
				var thiss = this;

				// Needed resources
				var resources = [
				Cval.route('cval/media', {
					file : 'jquery/css/jquery.multiselect.css'
				}),
				Cval.route('cval/media', {
					file : 'jquery/js/jquery.multiselect.js'
				}),
				Cval.route('cval/media', {
					file : 'css/page/schedule/step/contacts.css'
				})
				];

				Cval.resource.get(resources, function(){
					Cval.core.callback(callback);
				});
			},
			_show : function() {
				this._super();
				this.refreshGuestList();
				this.refreshContactList();
			},
			/**
			* This is called when going to next step
			*/
			goToForwardStep : function(step, callback) {
				if (this.scheduleData().fields.has_contacts.value) {
					Cval.core.callback(callback);
					return;
				}
		
				Cval.message.error(Cval.i18n.get('Please add at least one guest.'));
				this.getParentControl().highlightStepMenu(this.stepId());
			},
			_updateMenuData : function() {
				return $.extend({}, this._super(), {
					next : {
						label : Cval.i18n.get('Step 3: Propose times')
					},
					prev : true
				});
			},
			_bindEvents : function() {
				var thiss = this;

				this._super();

				// Bind add to guests button
				this.getElement().find(this.defaults.contactListSelector+' '+this.defaults.contactAddToGuestsSelector).livequery(function(){
					$(this).unbind('click').click(function(){
						thiss.addContactToGuests($(this));
						return false;
					})
				});
			
				// Bind remove button on guests list
				this.getElement().find(this.defaults.guestListSelector+' '+this.defaults.guestListRemoveSelector).livequery(function(){
					$(this).unbind('click').click(function(){
						thiss.removeGuest($(this));
						return false;
					})
				});

				// Init add contact button
				this.getElement().find(this.defaults.contactAddSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-plus'
						}
					})
					.click(function(){
						Cval.dialog.loadControl('contact_add', function(){
							Cval.dialog.control.contact_add.show(null, {
								groupData : {
									id : thiss.getContactGroupId()
								},
								hideCallback : function(){
									thiss.refreshContacts();
								}
							})
						});
						return false;
					});
					
				// Init remove contact button
				this.contactRemoveButtonEl = this.getElement().find(this.defaults.contactRemoveSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-trash'
						}
					});
					
				// Init select contact button
				this.getElement().find(this.defaults.contactSelectAllSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-check'
						}
					});
				
				this.getElement().find(this.defaults.contactControlsSelector).buttonset();
				this.contactRemoveButtonEl.Cval_button('option', 'disabled', true);
				
				// Init remove guest button
				this.guestRemoveButtonEl = this.getElement().find(this.defaults.guestRemoveSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-trash'
						}
					})
					.click(function(){
						thiss.removeGuests();
					});
					
				// Init select guest button
				this.getElement().find(this.defaults.guestSelectAllSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-check'
						}
					});
				
				this.guestRemoveButtonEl.Cval_button('option', 'disabled', true);
				
				// Init add contacts to guest button
				this.contactToGuestAddButtonEl = this.getElement().find(this.defaults.contactToGuestAddSelector)
					.Cval_button({
						text : false,
						icons : {
							secondary : 'ui-icon-play'
						}
					})
					.click(function(){
						thiss.addContactsToGuests();
					});
					
				// Init add all contacts to guest button
				this.contactToGuestAddAllButtonEl = this.getElement().find(this.defaults.contactToGuestAddAllSelector)
					.Cval_button({
						text : false,
						icons : {
							secondary : 'ui-icon-seek-next'
						}
					})
					.click(function(){
						thiss.addAllContactsToGuests();
					});
				
				this.contactToGuestAddButtonEl.Cval_button('option', 'disabled', true);
				this.contactToGuestAddAllButtonEl.Cval_button('option', 'disabled', true);
				
				// Hover effects for table rows
				this.getElement().find(this.defaults.contactListSelector+' tbody tr').livequery(function(){
					$(this).hover(function(){
						$(this).addClass('Cval-hover');
					}, function(){
						$(this).removeClass('Cval-hover')
					});

					$(this).find('.Cval-list-checkbox').change(function(){
						thiss.contactListUpdate();
					});
				});
				
				// Hover effects for table rows
				this.getElement().find(this.defaults.guestListSelector+' tbody tr').livequery(function(){
					$(this).hover(function(){
						$(this).addClass('Cval-hover');
					}, function(){
						$(this).removeClass('Cval-hover')
					});

					$(this).find('.Cval-list-checkbox').change(function(){
						thiss.guestListUpdate();
					});
				});
				
				this._initContactListControls();
				this._initGuestListControls();
				
				this._initContactGroupSelect();
				this._initContactSearch();
			},
			getLoadConfig : function() {
				return $.extend({}, this._super(), {
					data : {
						id : this.defaults.showParams.id
					}
				});
			},
			_initContactListControls : function() {
				var thiss = this,
					listControls = this.getElement()
						.find('.Cval-schedule-contacts-search-list-controls');

				this.contactCheckerEl = listControls.find('.Cval-selectall')
					.Cval_button_checker({
						disabled : true,
						clickCallback : function(check, button) {
							thiss.getElement().find(thiss.defaults.contactListSelector)
								.find('tbody tr .Cval-list-checkbox')
								.attr('checked', check);
							thiss.contactListUpdate();
						}
					})

				this.contactPaginationEl = listControls.find('.Cval-paginator')
					.Cval_pagination({
						clickCallback : function(page) {
							thiss.contactPaginateResults(page);
						}
					});

				this.contactListControlsEl = listControls;
			},
			contactPaginateResults : function(page) {
				this.refreshContactList(null, $.extend({}, this.contactLastRefresh.data || {}, {
					pagination : page
				}));
			},
			_initGuestListControls : function() {
				var thiss = this,
					listControls = this.getElement()
						.find('.Cval-schedule-contacts-guests-list-controls');

				this.guestCheckerEl = listControls.find('.Cval-selectall')
					.Cval_button_checker({
						disabled : true,
						clickCallback : function(check, button) {
							thiss.getElement().find(thiss.defaults.guestListSelector)
								.find('tbody tr .Cval-list-checkbox')
								.attr('checked', check);
							thiss.guestListUpdate();
						}
					})

				this.guestPaginationEl = listControls.find('.Cval-paginator')
					.Cval_pagination({
						clickCallback : function(page) {
							thiss.guestPaginateResults(page);
						}
					});

				this.guestListControlsEl = listControls;
			},
			guestPaginateResults : function(page) {
				this.refreshGuestList(null, $.extend({}, this.guestLastRefresh.data || {}, {
					pagination : page
				}));
			},
			_initContactGroupSelect : function() {
				var thiss = this;

				var select = this.getElement().find(this.defaults.contactGroupSelectSelector);
				select.multiselect({
					minWidth: 150,
					multiple: false,
					header: Cval.i18n.get('Select group'),
					selectedList: 1
				}).change(function(){
					thiss.refreshContactList();
				});
			},
			_initContactSearch : function() {
				var thiss = this,
					form = this.getParentControl().getElement().find('.Cval-menu-id-schedule-contacts-search form.Cval-form-search');

				this.contactSearchInputEl = form.find('input[name=search]');
				form.submit(function(e){
					e.preventDefault();
					thiss.refreshContactList();
					return false;
				});
			},
			_contactSearchData : function() {
				return {
					global : this.contactSearchInputEl.val().trim()
				};
			},
			isContactSearchActive : function() {
				return this.contactLastRefresh.data.search.global.length > 0;
			},
			getContactGroupId : function() {
				return this.getElement().find(this.defaults.contactGroupSelectSelector).val();
			},
			refreshContacts : function(callback) {
				this.refreshContactList(callback);
			},
			refreshContactList : function(callback, data) {
				var thiss = this;
				
				data = $.extend({}, {
					body_only : 1,
					group : thiss.getContactGroupId(),
					search : thiss._contactSearchData()
				}, data || {});

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'contacts_search_table'
					},
					data : data
				}, function(json) {
					thiss.contactLastRefresh = {
						data : data
					};
					
					thiss.updateContactList(json, false, callback);
				});
			},
			updateContactList : function(data, complete, callback) {
				var table = this.getElement().find(this.defaults.contactListSelector);

				// Display, show no contact message
				if (data.count > 0)
					this.noContactMessageHide();
				else
					this.noContactMessageShow();
				// Display/show table header
				table.toggle(data.count > 0);

				// Replace only body content
				if ( ! complete)
					table = table.find('tbody');

				table.html(data.html);
				// Inject js data
				Cval.ajax.data.inject(data);

				// Enable/Disable checker
				this.contactCheckerEl.Cval_button_checker({
					checked : false,
					disabled : data.count < 1
				});
				// update pagination
				this.contactPaginationEl.Cval_pagination('update', data.pagination);
				this.contactListUpdate();
				
				this.contactListControlsEl.show();

				Cval.core.callback(callback);
			},
			contactListUpdate : function() {
				var thiss = this;

				var checkedCount = thiss.getSelectedContactsEls().length;
				this.contactToGuestAddButtonEl.Cval_button('option', 'disabled', checkedCount < 1);
				
				var totalCount = this.contactPaginationEl.Cval_pagination('option', 'total_items');
				this.contactToGuestAddAllButtonEl.Cval_button('option', 'disabled', totalCount < 1);
			},
			noContactMessageShow : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-contacts-search',
					isSearchActive = this.isContactSearchActive(),
					messageElClass = selectorPrefix+'-message-nocontact_'+(isSearchActive ? 'search' : 'default');;

				if (thiss.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-nocontact.'+messageElClass).length)
					return;

				this.noContactMessageHide();

				var content = isSearchActive
						? Cval.i18n.get('No contacts found, try to modify your search phrase.')
						: thiss.getElement().find('.'+selectorPrefix+'-message-nocontact-content').html();
				Cval.message.info(content, null, {
					wrapper : thiss.getElement().find('.'+selectorPrefix+'-info-box'),
					autoDestroy : false,
					closer : false,
					outerClass : selectorPrefix+'-message-nocontact '+messageElClass
				});
			},
			noContactMessageHide : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-contacts-search';
				
				var messageEl = this.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-nocontact');
				Cval.message.destroy(messageEl, true);
			},
			getSelectedContactsEls : function() {
				return this.getElement().find(this.defaults.contactListSelector)
					.find('tbody tr')
					.filter(function(){
						return $(this).find('.Cval-list-checkbox').is(':checked')
					});
			},
			refreshGuests : function(callback) {
				this.refreshGuestList(callback);
			},
			refreshGuestList : function(callback, data) {
				var thiss = this;

				data = $.extend({}, {
					body_only : 1,
					schedule : thiss.defaults.showParams.id
				}, data || {});

				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'contacts_guests_table'
					},
					data : data
				}, function(json) {
					thiss.guestLastRefresh = {
						data : data
					};
					
					thiss.updateGuestList(json, false, callback);
				});
			},
			updateGuestList : function(data, complete, callback) {
				var table = this.getElement().find(this.defaults.guestListSelector);

				// Display, show no guest message
				if (data.count > 0)
					this.noGuestMessageHide();
				else
					this.noGuestMessageShow();
				// Display, show not responding message
				if (data.additional.not_responding_count > 0)
					this.notRespondingGuestMessageShow();
				else
					this.notRespondingGuestMessageHide();
				// Display/show table header
				table.toggle(data.count > 0);

				// Replace only body content
				if ( ! complete)
					table = table.find('tbody');

				table.html(data.html);
				// Inject js data
				Cval.ajax.data.inject(data);

				// Enable/Disable checker
				this.guestCheckerEl.Cval_button_checker({
					checked : false,
					disabled : data.count < 1
				});
				// update pagination
				this.guestPaginationEl.Cval_pagination('update', data.pagination);
				this.guestListUpdate();
				
				this.guestListControlsEl.show();
				
				// Propagate actual schedule data to parent control
				this.updateScheduleData(data.schedule);

				Cval.core.callback(callback);
			},
			guestListUpdate : function() {
				var thiss = this;

				var checkedCount = thiss.getSelectedGuestsEls().length;
				this.guestRemoveButtonEl.Cval_button('option', 'disabled', checkedCount < 1);
			},
			noGuestMessageShow : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-contacts-guests';

				if (thiss.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-noguest').length)
					return;

				var content = thiss.getElement().find('.'+selectorPrefix+'-message-noguest-content').html();
				Cval.message.error(content, null, {
					wrapper : thiss.getElement().find('.'+selectorPrefix+'-info-box'),
					autoDestroy : false,
					closer : false,
					outerClass : selectorPrefix+'-message-noguest'
				});
			},
			noGuestMessageHide : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-contacts-guests';
					
				var messageEl = this.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-noguest');
				Cval.message.destroy(messageEl, true);
			},
			notRespondingGuestMessageShow : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-contacts-guests';

				if (thiss.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-notrespondingguest').length)
					return;

				var content = thiss.getElement().find('.'+selectorPrefix+'-message-notrespondingguest-content').html();
				Cval.message.error(content, null, {
					wrapper : thiss.getElement().find('.'+selectorPrefix+'-info-box'),
					autoDestroy : false,
					closer : false,
					outerClass : selectorPrefix+'-message-notrespondingguest Cval-helper-margin-bottom'
				});
			},
			notRespondingGuestMessageHide : function() {
				var thiss = this,
					selectorPrefix = 'Cval-schedule-contacts-guests';
					
				var messageEl = this.getElement().find('.'+selectorPrefix+'-info-box .'+selectorPrefix+'-message-notrespondingguest');
				Cval.message.destroy(messageEl, true);
			},
			getContactEl : function(el) {
				el = $(el);

				if ( ! el.is('tr'))
					el = el.parents('tr:first');

				return el;
			},
			addContactToGuests : function(contactEl) {
				this._addContactToGuests(this.getContactEl(contactEl).Cval_data().id);
			},
			addContactsToGuests : function() {
				var ids = [];
				$.each(this.getSelectedContactsEls(), function(){
					ids.push($(this).Cval_data().id);
				});
				this._addContactToGuests(ids);
			},
			addAllContactsToGuests : function() {
				this._addContactToGuests(this.getContactGroupId(), true);
			},
			_addContactToGuests : function(id, fromGroup) {
				var thiss = this,
					routeParams = {
						controller : 'guest_add'
					},
					data = {
						id : id,
						schedule : thiss.defaults.showParams.id
					};
					
				if (fromGroup)
					routeParams.action = 'group';
				
				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : routeParams,
					data : data
				}, function(json) {
					thiss.refreshGuests(function(){
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}
					});
				});

				return false;
			},
			getSelectedGuestsEls : function() {
				return this.getElement().find(this.defaults.guestListSelector)
					.find('tbody tr')
					.filter(function(){
						return $(this).find('.Cval-list-checkbox').is(':checked')
					});
			},
			getGuestEl : function(el) {
				el = $(el);

				if ( ! el.is('tr'))
					el = el.parents('tr:first');

				return el;
			},
			removeGuest : function(guestEl) {
				this._removeGuest(this.getGuestEl(guestEl).Cval_data().id);
			},
			removeGuests : function() {
				var ids = [];
				$.each(this.getSelectedGuestsEls(), function(){
					ids.push($(this).Cval_data().id);
				});
				this._removeGuest(ids);
			},
			_removeGuest : function(id) {
				var thiss = this;
				
				Cval.ajax.callModal({
					route : 'schedule',
					routeParams : {
						controller : 'guest_remove'
					},
					data : {
						id : id,
						schedule : thiss.defaults.showParams.id
					}
				}, function(json) {
					thiss.refreshGuests(function(){
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}
					});
				});

				return false;
			}
		},
		params : {}
	};
	
	$.Class.extend('Cval.page.control.schedule_step_contacts', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('schedule_step_def', function(){
					
					Cval.page.control.schedule_step_def.extend('Cval.page.control.schedule_step_contacts', 
						Cval_page_control_schedule_step_contacts_object.staticParams,
						Cval_page_control_schedule_step_contacts_object.params
					);
					
					Cval.page.control.schedule_step_contacts.init(callback);
				})
			})
		}
	}, {});

})(Cval, jQuery);