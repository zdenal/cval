<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Request
 * 
 * Abstract class for schedule requests
 * @package Cval
 * @author	zdennal
 */
interface Schedule_Request_Interface
{
	/**
	 * @return Schedule_Request 
	 */
	public static function factory();
	
	/**
	 * Fetch this object from array. It is used when receiving request.
	 * 
	 * @param	array
	 * @return	Schedule_Request
	 */
	public function unserialize($data);
	
	/**
	 * Fill request data from schedule. This is called on organizer server,
	 * when request is created.
	 * 
	 * @param	Model_Schedule	Schedule for request
	 * @return	Schedule_Request
	 */
	public function create(Model_Schedule $schedule);
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize();
	
	/**
	 * @return	Model_Schedule 
	 */
	public function get_schedule();

	/**
	 * @param	Model_Schedule
	 * @return	$this
	 */
	public function set_schedule(Model_Schedule $schedule);
	
	/**
	 * @return	User_Agent 
	 */
	public function get_guest();

	/**
	 * @param	User_Agent
	 * @return	$this
	 */
	public function set_guest(User_Agent $guest);
	
	/**
	 * @return	User_Agent 
	 */
	public function get_organizer();

	/**
	 * @param	User_Agent
	 * @return	$this
	 */
	public function set_organizer(User_Agent $organizer);
	
	/**
	 * Sends request and returns response.
	 * 
	 * @param	User_Agent	Optional target user
	 * @return	Schedule_Response
	 */
	public function send(User_Agent $guest = NULL);
	
}