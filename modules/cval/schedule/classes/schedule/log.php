<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Log
 * 
 * @package Cval
 * @author	zdennal
 */
class Schedule_Log
{

	public static function debug(Model_Schedule $schedule, $message)
	{
		Kohana::$log->add(Kohana::DEBUG, Schedule_Log::_identification_message($schedule).$message);
	}
	
	public static function error(Model_Schedule $schedule, $message)
	{
		Kohana::$log->add(Kohana::ERROR, Schedule_Log::_identification_message($schedule).$message);
	}
	
	protected static function _identification_message(Model_Schedule $schedule)
	{
		return strtr('SCHEDULE[:organizer_location, :organizer_hash, :schedule_hash, :schedule_user] - ', array(
			':organizer_location' => $schedule->organizer->location->url,
			':organizer_hash' => $schedule->organizer->hash,
			':schedule_hash' => $schedule->hash,
			':schedule_user' => $schedule->user->username
		));
	}

}
