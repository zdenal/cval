<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Response
 * 
 * Abstract class for schedule responses
 * @package Cval
 * @author	zdennal
 */
interface Schedule_Response_Interface
{
	/**
	 * @return Schedule_Response 
	 */
	public static function factory();
	
	/**
	 * Fetch this object from array. It is used when response for request 
	 * is returned.
	 * 
	 * 
	 * @param	Location_Response
	 * @return	Schedule_Response
	 */
	public function unserialize(Location_Response $location_response);
	
	/**
	 * Fill response data from request. This is called on guest server,
	 * when request is received.
	 * 
	 * @param	Schedule_Request	request
	 * @return	Schedule_Response
	 */
	public function create(Schedule_Request $request);
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize();
	
	/**
	 * @return	Location_Response
	 */
	public function get_location_response();
	
	/**
	 * @return	bool
	 */
	public function is_error();
	
}