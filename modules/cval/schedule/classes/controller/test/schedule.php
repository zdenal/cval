<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Schedule extends Controller_Ext 
{
	public function action_cli()
	{
		$uri = Route::get('schedule')->uri(array(
			'controller' => 'negotiate_cli'
		));
		
		$this->request->response = CLI::internal_call($uri, array(
			'post' => array(
				'id' => '1__1'
			)
		));
	}
	
	public function action_inbound_email()
	{
		$send_response = new Schedule_Send_Response;
		$schedule = Jelly::select('schedule')->load(Arr::get($_REQUEST, 'id', 0));
		
		if ( ! $schedule->loaded())
		{
			throw Model_Not_Found_Exception::create($schedule, Arr::get($_REQUEST, 'id', 0));
		}
		
		$send_response->send_inbound_received_email($schedule);
	}
			
}
