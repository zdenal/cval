<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Schedule Period Collection
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Period_Collection
{
	
	public static function debug(array $periods)
	{
		foreach ($periods as $key => $period)
		{
			$periods[$key] = array(Date::format($period[0]), Date::format($period[1]));
		}
		
		return Kohana::debug($periods);
	}
	
	/**
	 * Converts time collection to period collection.
	 * Example:
	 * 
	 * IN
	 *  array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)  
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 00:00:00', '2012-01-01 12:00:00')
	 *		array('2012-01-01 10:00:00', '2012-01-01 16:00:00')
	 *  ) 
	 * 
	 * @param	array	time collection
	 * @return	array	period collection
	 */
	public static function from_period_time_collection(array $coll)
	{
		$periods = array();
		
		foreach ($coll as $key => $time)
		{
			if ($key%2)
			{
				$periods[] = array((int) $coll[$key-1][0], (int) $time[0]);
			}
		}
		
		return $periods;
	}
	
	/**
	 * Every overlapping periods are joined.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', '2012-01-01 12:00:00')
	 *		array('2012-01-01 10:00:00', '2012-01-01 16:00:00')
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 00:00:00', '2012-01-01 16:00:00')
	 *  ) 
	 * 
	 * @param	array	input collection
	 * @return	array	output collection
	 */
	public static function flatten(array $periods)
	{	
		$times = Schedule_Period_Time_Collection::from_period_collection($periods);
		$times = Schedule_Period_Time_Collection::flatten($times);
		
		return Schedule_Period_Collection::from_period_time_collection($times);
	}
	
	/**
	 * Creates inverse periods.
	 * If start is not supplied start interval for inverse periods
	 * is start of first period.
	 * If end is not supplied end interval for inverse periods
	 * is end of last period.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', '2012-01-01 12:00:00')
	 *		array('2012-01-01 15:00:00', '2012-01-01 16:00:00')
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 12:00:00', '2012-01-01 15:00:00')
	 *  ) 
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', '2012-01-01 12:00:00')
	 *		array('2012-01-01 15:00:00', '2012-01-01 16:00:00')
	 *  )
	 *  NULL					// start
	 *	'2012-01-01 18:00:00'	// end
	 * OUT
	 *  array(
	 *		array('2012-01-01 12:00:00', '2012-01-01 15:00:00')
	 *		array('2012-01-01 16:00:00', '2012-01-01 18:00:00')
	 *  ) 
	 * 
	 * @param	array	input collection
	 * @return	array	output collection
	 */
	public static function inverse(array $coll, $start = NULL, $end = NULL)
	{	
		$times = Schedule_Period_Time_Collection::from_period_collection($coll);
		$times = Schedule_Period_Time_Collection::inverse($times, $start, $end);
		
		return Schedule_Period_Collection::from_period_time_collection($times);
	}
	
	/**
	 * Remove periods which are shorter than limit.
	 * 
	 * @param	array	input collection
	 * @param	int		limited value
	 * @return	array	output collection
	 */
	public static function limit_length(array $coll, $limit)
	{
		foreach ($coll as $key => $period)
		{
			$length = $period[1] - $period[0];
			// Remove shorter period
			if ($length < $limit)
			{
				unset($coll[$key]);
			}
		}
		
		return $coll;
	}
	
}