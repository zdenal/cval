<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Schedule Period Time
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Period_Time
{
	
	public static function debug($time)
	{
		$time = array(Date::format($time[0]), $time[1]);
		
		return Kohana::debug($time);
	}
	
	public static function compare(array $t1, array $t2)
	{
		if ($t1[0] != $t2[0])
		{
			// Earlier timestamp is lower 
			return ($t1[0] < $t2[0]) ? -1 : 1;
		}
		
		if ($t1[1] != $t2[1])
		{
			// Period start is before end for fine overlapping
			return $t1[1] ? 1 : -1;
		}
		
		return 0;
	}
	
}