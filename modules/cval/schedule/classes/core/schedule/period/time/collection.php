<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Schedule Period Time Collection
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Period_Time_Collection
{
	
	public static function debug(array $times)
	{
		foreach ($times as $key => $time)
		{
			$times[$key] = array(Date::format($time[0]), $time[1]);
		}
		
		return Kohana::debug($times);
	}
	
	/**
	 * Converts period collection to time collection.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', '2012-01-01 12:00:00')
	 *		array('2012-01-01 10:00:00', '2012-01-01 16:00:00')
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 16:00:00', FALSE)  
	 *  ) 
	 * 
	 * @param	array	period collection
	 * @param	bool	TRUE for return sorted collection
	 * @return	array	time collection
	 */
	public static function from_period_collection(array $periods, $sort = FALSE)
	{
		$times = array();
		
		foreach ($periods as $period)
		{
			$times[] = array((int) $period[0], TRUE);
			$times[] = array((int) $period[1], FALSE);
		}
		
		if ($sort)
		{
			$times = Schedule_Period_Time_Collection::sort($times);
		}
		
		return $times;
	}
	
	/**
	 * Sorts time collection.
	 * If same time found than opening one has precedence before closing one.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)  
	 *		array('2012-01-01 16:00:00', TRUE)
	 *		array('2012-01-01 18:00:00', FALSE)
	 *  ) 
	 * OUT
	 *  array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 16:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)
	 *		array('2012-01-01 18:00:00', FALSE)
	 *  ) 
	 * 
	 * @param	array	collection
	 * @return	array	sorted collection	
	 * @uses	Schedule_Period_Time::compare
	 */
	public static function sort(array $times)
	{
		usort($times, 'Schedule_Period_Time::compare');
		
		return $times;
	}
	
	/**
	 * Every overlapping times are joined.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)  
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)  
	 *  ) 
	 * 
	 * @param	array	input collection
	 * @return	array	output collection
	 */
	public static function flatten(array $coll)
	{	
		$coll = Schedule_Period_Time_Collection::sort($coll);
		
		$flatten = array();
		$open_cnt = 0;
		
		foreach ($coll as $item)
		{
			$timestamp = $item[0];
			$open = $item[1];
			
			if ($open)
			{
				$open_cnt++;
				if ($open_cnt === 1)
				{
					$flatten[] = array($timestamp, TRUE);
				}
			}
			else
			{
				$open_cnt--;
				if ($open_cnt === 0)
				{
					$flatten[] = array($timestamp, FALSE);
				}
			}
		}
		
		return $flatten;
	}
	
	/**
	 * Creates inverse time collection.
	 * If start is not supplied start interval for inverse periods
	 * is first time in collection.
	 * If end is not supplied end interval for inverse periods
	 * is last time in collection.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 15:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 12:00:00', TRUE)
	 *		array('2012-01-01 15:00:00', FALSE)
	 *  ) 
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 15:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)
	 *  )
	 *  NULL					// start
	 *	'2012-01-01 18:00:00'	// end
	 * OUT
	 *  array(
	 *		array('2012-01-01 12:00:00', TRUE)
	 *		array('2012-01-01 15:00:00', FALSE)
	 *		array('2012-01-01 16:00:00', TRUE)
	 *		array('2012-01-01 18:00:00', FALSE)
	 *  ) 
	 * 
	 * @param	array	input collection
	 * @return	array	output collection
	 */
	public static function inverse(array $coll, $start = NULL, $end = NULL)
	{	
		if ( ! count($coll))
		{
			if ($start === NULL OR $end === NULL)
			{
				throw new Kohana_Exception('Inverse period time collection for empty collection needs have start and end specified.');
			}
			
			return array(
				array($start, TRUE),
				array($end, FALSE)
			);
		}
		
		// First flatten it
		$coll = Schedule_Period_Time_Collection::flatten($coll);
		
		if ($start === NULL)
		{
			$start = Arr::get(reset($coll), 0);
		}
		if ($end === NULL)
		{
			$end = Arr::get(end($coll), 0);
		}
		
		$times = array();
		$last_time = NULL;
		
		foreach ($coll as $time)
		{
			$timestamp = $time[0];
			$open = $time[1];
			
			if ($timestamp <= $start)
			{
				continue;
			}
			if ($timestamp >= $end)
			{
				break;
			}
			
			$reverse_time = $time;
			$reverse_time[1] = ! $reverse_time[1];
			
			if ($open)
			{
				if ($last_time === NULL)
				{
					$times[] = array($start, TRUE);
				}
				$times[] = $last_time = $reverse_time;
			}
			else
			{
				$times[] = $last_time = $reverse_time;
			}
		}
		
		if (count($times)%2)
		{
			$times[] = array($end, FALSE);
		}
		
		return $times;
	}
	
	/**
	 * Creates itersection from two time collections.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *  ),
	 *	array(
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *  )
	 * 
	 * @param	array	first collection
	 * @param	array	second collection
	 * @return	array	output collection
	 */
	public static function intersect(array $coll1, array $coll2)
	{	
		// First flatten it
		$coll1 = Schedule_Period_Time_Collection::flatten($coll1);
		$coll2 = Schedule_Period_Time_Collection::flatten($coll2);
		
		$coll = Schedule_Period_Time_Collection::combine($coll1, $coll2);
		
		$times = array();
		$open_cnt = 0;
		
		foreach ($coll as $time)
		{
			$open = $time[1];
			
			if ($open)
			{
				$open_cnt++;
				if ($open_cnt === 2)
				{
					$times[] = $time;
				}
			}
			else
			{
				$open_cnt--;
				if ($open_cnt === 1)
				{
					$times[] = $time;
				}
			}
		}
		
		return $times;
	}
	
	/**
	 * Combines two time collections and sort them as default.
	 * Example:
	 * 
	 * IN
	 *	array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *  ),
	 *	array(
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 16:00:00', FALSE)
	 *  )
	 * OUT
	 *  array(
	 *		array('2012-01-01 00:00:00', TRUE)
	 *		array('2012-01-01 10:00:00', TRUE)
	 *		array('2012-01-01 12:00:00', FALSE)
	 *		array('2012-01-01 16:00:00', FALSE)
	 *  )
	 * 
	 * @param	array	first collection
	 * @param	array	second collection
	 * @param	bool	sort times after combining
	 * @return	array	output collection
	 */
	public static function combine(array $coll1, array $coll2, $sort = TRUE)
	{	
		foreach ($coll2 as $time)
		{
			$coll1[] = $time;
		}
		
		if ($sort)
		{
			$coll1 = Schedule_Period_Time_Collection::sort($coll1);
		}
		
		return $coll1;
	}
	
}