<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Send
 * 
 * Used when negotiating 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Send
{
	/**
	 * @return	Schedule_Send
	 */
	public static function factory()
	{
		return new Schedule_Send;
	}
	
	/**
	 * Starts negotiation process
	 * 
	 * @param	Model_Schedule	schedule to negotiate
	 * @return	array			period collection
	 * @throws	Kohana_Exception	when some guest not found in local db
	 */
	public function start(Model_Schedule $schedule)
	{
		// Set schedule
		$this->_schedule = $schedule;
		
		// Create request
		$request = Schedule_Send_Request::factory()->create($schedule);
		// Empty response
		$response = NULL;
		
		try
		{
			$guest_meta = Jelly::meta('schedule_user_profile_shared');
			$guest_default_values = array(
				'is_responding' => $guest_meta->fields('is_responding')->default,
				'is_full' => $guest_meta->fields('is_full')->default
			);
			// Get guests ids and clear guests values
			$guests_ids = array();
			// Select only to guest which already has not received schedule
			$guests_collection = $schedule->get('schedule_user_profiles_shared')
									->where('has_received_schedule', '=', FALSE)
									->execute();
			
			foreach ($guests_collection as $guest)
			{
				$guests_ids[] = $guest->id();
				// Set default values for guest
				$guest->set($guest_default_values)->save();
			}
			// Make it random order
			shuffle($guests_ids);

			foreach ($guests_ids as $guest_id)
			{
				$guest = Jelly::select('schedule_user_profile_shared')->load($guest_id);
				if ( ! $guest->loaded())
				{
					continue;
				}
				// One more check for sure. Do not send schedule more than once
				if ($guest->has_received_schedule)
				{
					continue;
				}
				$profile = $guest->user_profile_shared;

				$error = FALSE;
				try
				{
					// Check guest time
					$tmp_response = $request->send($profile->agent());
					$error = $tmp_response->is_error();
				}
				catch (Location_Exception $e)
				{
					Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
					$error = TRUE;
				}
				
				$guest->is_responding = ! $error;
				
				// Not responding guest continue to another guest
				if ( ! $guest->is_responding)
				{
					// Increase failed counter
					$this->_failed_guests_count++;
					$guest->save();
					continue;
				}
				
				//echo Kohana::debug($tmp_response->serialize());
				
				// Set received schedule token
				$guest->has_received_schedule = TRUE;
				$guest->save();
				
				$response = $tmp_response;
			}
		}
		catch (Exception $e)
		{
			// Rethrow exception
			throw $e;
		}
		
		return $this->_finish(Model_Schedule::STATE_SENT, $response);
	}
	
	/**
	 * @var	Model_Schedule
	 */
	protected $_schedule;
	
	/**
	 * @var	int
	 */
	protected $_failed_guests_count = 0;
	
	/**
	 * Is called at the end of start method.
	 * 
	 * @param	int								one of Model_Schedule::STATE_x
	 * @param	Schedule_Send_Response			last send response
	 * @return	int					number of failed guests
	 */
	protected function _finish($state, Schedule_Send_Response $response = NULL)
	{
		$this->_schedule->set_state($state, FALSE);
		
		if ($this->_schedule->check_state_equal(Model_Schedule::STATE_SENT))
		{
			// Set schedule as unsync after send
			$this->_schedule->is_unsync = TRUE;
		}
		return $this->_failed_guests_count;
	}
	
}