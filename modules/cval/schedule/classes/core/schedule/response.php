<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Response
 * 
 * Abstract class for schedule responses
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Response implements Schedule_Response_Interface
{
	
	/**
	 * @var	Schedule_Request
	 */
	protected $_request;
	
	/**
	 * @var	Location_Response
	 */
	protected $_location_response;
	
	/**
	 * @var	boolean
	 */
	protected $_error = FALSE;
	
	/**
	 * @return	Location_Response
	 */
	public function get_location_response()
	{
		return $this->_location_response;
	}
	
	/**
	 * @return	bool
	 */
	public function is_error()
	{
		return (bool) $this->_error;
	}
	
}