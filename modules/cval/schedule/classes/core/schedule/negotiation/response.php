<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Negotiation Response
 * 
 * Used when negotiating for guest
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Negotiation_Response extends Schedule_Response
{
	
	/**
	 * @var	Schedule_Negotiation_Request
	 */
	protected $_request;
	
	/**
	 * @var array	Array of responded schedule periods timestamps 
	 */
	protected $_periods;
	
	/**
	 * @return Schedule_Negotiation_Response 
	 */
	public static function factory()
	{
		return new Schedule_Negotiation_Response;
	}
	
	/**
	 * Fetch this object from array. It is used when response for request 
	 * is returned.
	 * 
	 * @param	Location_Response
	 * @return	Schedule_Negotiation_Response
	 */
	public function unserialize(Location_Response $location_response)
	{
		$response = $this;
		$response->_location_response = $location_response;
		
		if ($location_response->error())
		{
			$response->_error = TRUE;
		}
		else
		{
			try
			{
				$data = $location_response->data();
		
				if (empty($data) OR ! is_array($data))
				{
					throw Cval_Unserialize_Exception::create('Schedule_Negotiation_Response', 'Parameter "data" must be not empty array');
				}
				$periods = Arr::get($data, 'periods');
				if ( ! is_array($periods))
				{
					throw Cval_Unserialize_Exception::create('Schedule_Negotiation_Response', 'Parameter "periods" must be array');
				}

				$response->_periods = $periods;
			}
			catch (Exception $e)
			{
				$this->_error = TRUE;
				Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			}
		}
		
		return $response;
	}
	
	/**
	 * Fill response data from request. This is called on guest server,
	 * when request is received.
	 * 
	 * @param	Schedule_Negotiation_Request	Negotiation request
	 * @return	Schedule_Negotiation_Response
	 */
	public function create(Schedule_Request $request)
	{
		$response = $this;
		
		$response->_request = $request;
		$response->_periods = $response->_filter_periods();
		
		return $response;
	}
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize()
	{
		$data = array(
			'periods' => $this->_periods
		);
		
		return $data;
	}
	
	/**
	 * @return	array
	 */
	public function get_periods()
	{
		return $this->_periods;
	}
	
	/**
	 * Filters requested periods against guest preferences
	 * 
	 * @param	array	Periods to filter
	 * @return	array	Filtered periods 
	 */
	protected function _filter_periods()
	{
		$periods = $this->_request->get_periods();
		
		// No periods
		if ( ! count($periods))
		{
			return $periods;
		}
		
		//echo Schedule_Period_Collection::debug($periods); 
		
		// Create sorted time collection from periods
		$periods_times = Schedule_Period_Time_Collection::from_period_collection($periods, TRUE);
		//echo Schedule_Period_Time_Collection::debug($periods_times);
		
		// Start is start of first period
		$start = Arr::get(reset($periods_times), 0);
		// End is end of last period
		$end = Arr::get(end($periods_times), 0);
		
		//echo Schedule_Period::debug(array($start, $end));
		
		// Get events in interval specified by start and end
		$events = $this->_request->get_guest()
				->user()
				->events($start, $end);
		
		// Convert them into period collection syntax
		foreach ($events as $key => $event)
		{
			$events[$key] = array($event['start'], $event['end']);
		}
		
		// Create time collection from events
		$events_times = Schedule_Period_Time_Collection::from_period_collection($events);
		//echo Schedule_Period_Time_Collection::debug($events_times);
		
		// Generate inverse time collection from events, i.e. free time collection from start and end
		$free_times = Schedule_Period_Time_Collection::inverse($events_times, $start, $end);
		//echo Schedule_Period_Time_Collection::debug($free_times);
		
		// Intersect received and local poeriods
		$updated_periods_times = Schedule_Period_Time_Collection::intersect($periods_times, $free_times);
		
		//exit;
		// Convert to period collection
		$updated_periods = Schedule_Period_Collection::from_period_time_collection($updated_periods_times);
		// Finally remove shorter periods than request minimum schedule length
		$updated_periods = Schedule_Period_Collection::limit_length($updated_periods, $this->_request->get_min_length());
		
		//exit;
		
		return $updated_periods;
	}
	
}