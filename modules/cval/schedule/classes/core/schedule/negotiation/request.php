<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Negotiation Request
 * 
 * Used when negotiating for organizer
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Negotiation_Request extends Schedule_Request
{	
	/**
	 * @var array	Array of schedule periods timestamps 
	 */
	protected $_periods;
	
	/**
	 * @var int		Min length for schedule
	 */
	protected $_min_length;
	
	/**
	 * @return Schedule_Negotiation_Request 
	 */
	public static function factory()
	{
		return new Schedule_Negotiation_Request;
	}
	
	/**
	 * Fetch this object from array. It is used when receiving request.
	 * 
	 * @param	array
	 * @return	Schedule_Negotiation_Request
	 */
	public function unserialize($data)
	{
		if (empty($data) OR ! is_array($data))
		{
			throw Cval_Unserialize_Exception::create($this, 'Parameter "data" must be not empty array');
		}
		$periods = Arr::get($data, 'periods');
		if (empty($periods) OR ! is_array($periods))
		{
			throw Cval_Unserialize_Exception::create($this, 'Parameter "periods" must be not empty array');
		}
		$min_length = Arr::get($data, 'min_length');
		if (empty($min_length) OR ! Validate::digit($min_length))
		{
			throw Cval_Unserialize_Exception::create($this, 'Parameter "min_length" must be positive integer');
		}
		
		$request = $this;
		
		$request->_guest = User_Agent::unserialize(Arr::get($data, 'guest'), TRUE);
		$request->_organizer = User_Agent::unserialize(Arr::get($data, 'organizer'));
		$request->_periods = $periods;
		$request->_min_length = (int) $min_length;
		
		return $request;
	}
	
	/**
	 * Fill request data from schedule. This is called on organizer server,
	 * when request is created.
	 * 
	 * @param	Model_Schedule	Schedule for request
	 * @return	Schedule_Negotiation_Request
	 */
	public function create(Model_Schedule $schedule)
	{
		$request = $this;
		
		$request->_schedule = $schedule;
		$request->_organizer = $schedule->organizer->agent();
		$request->_periods = $schedule->schedule_plan->get_periods_as_timestamps();
		$request->_min_length = $schedule->schedule_plan->get_min_length(Date::SECOND);
		
		return $request;
	}
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize()
	{
		$data = array(
			'guest' => $this->_guest->serialize(),
			'organizer' => $this->_organizer->serialize(),
			'periods' => $this->_periods,
			'min_length' => $this->_min_length
		);
		
		return $data;
	}

	/**
	 * @return	array
	 */
	public function get_periods()
	{
		return $this->_periods;
	}

	/**
	 * @param	array
	 * @return	$this
	 */
	public function set_periods(array $periods)
	{
		$this->_periods = $periods;
		return $this;
	}
	
	/**
	 * @return	int 
	 */
	public function get_min_length()
	{
		return $this->_min_length;
	}
	
	/**
	 * @param	int
	 * @return	$this
	 */
	public function set_min_length($min_length)
	{
		$this->_min_length = $min_length;
		return $this;
	}
	
	/**
	 * Sends request and returns response.
	 * 
	 * @param	User_Agent	Optional target user
	 * @return	Schedule_Negotiation_Response
	 */
	public function send(User_Agent $guest = NULL)
	{
		if ($guest !== NULL)
		{
			$this->_guest = $guest;
		}
		
		$location = $this->_guest->location();
		
		$uri = Route::get('schedule')->uri(array(
			'controller' => 'negotiate_request'
		));
		
		$data = $this->serialize();

		$response = $location->request($uri, $data);
		$response = Schedule_Negotiation_Response::factory()->unserialize($response);
		
		return $response;
	}
	
}