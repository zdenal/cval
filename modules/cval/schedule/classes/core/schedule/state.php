<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_State
 * 
 * Class for schedule states.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_State
{
	/**
	 * @var	array	States JS config array
	 */
	protected static $_config_js;

	/**
	 * Returns state label.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @return	string 
	 */
	public static function label($state)
	{
		return Kohana::config('cval/schedule/state.labels.'.$state);
	}
	
	/**
	 * JS config consist all defined states and their values.
	 * 
	 * @return	array 
	 */
	public static function config_js()
	{
		if (Schedule_State::$_config_js === NULL)
		{
			// Get schedule states for use in 
			$states = array();
			$schedule_class = new ReflectionClass('Model_Schedule');
			foreach ($schedule_class->getConstants() as $name => $value)
			{
				if (strpos($name, 'STATE_') !== 0)
				{
					continue;
				}
				$states[substr($name, 6)] = $value;
			}
			
			Schedule_State::$_config_js = $states;
		}
		
		return Schedule_State::$_config_js;
	}

}
