<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Exception extends Kohana_Exception {

	public $schedule;

	/**
	 * Creates a new schedule exception:
	 *
	 *	   throw Schedule_Exception::create($schedule, $message);
	 *
	 * @param   Model_Schedule   Schedule instance for exception
	 * @param   string     error message
	 * @param   array      translation variables
	 * @param   integer    the exception code
	 * @return  Schedule_Exception
	 */
	public static function create(Model_Schedule $schedule, $message, array $variables = NULL, $code = 0)
	{
		$exception = new Schedule_Exception($message, $variables, $code);
		$exception->schedule = $schedule;
		return $exception;
	}

} // End Core_Schedule_Exception
