<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Time Check Response
 * 
 * Used when checking time for guest
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Send_Response extends Schedule_Response
{
	
	/**
	 * @var	Schedule_Send_Request
	 */
	protected $_request;
	
	/**
	 * @var	bool	TRUE if schedule was received, FALSE if not
	 */
	protected $_result;
	
	/**
	 * @return Schedule_Send_Response 
	 */
	public static function factory()
	{
		return new Schedule_Send_Response;
	}
	
	/**
	 * Fetch this object from array. It is used when response for request 
	 * is returned.
	 * 
	 * @param	Location_Response
	 * @return	Schedule_Send_Response
	 */
	public function unserialize(Location_Response $location_response)
	{	
		$response = $this;
		$response->_location_response = $location_response;
		
		if ($location_response->error())
		{
			$response->_error = TRUE;
		}
		else
		{
			try
			{
				$data = $location_response->data();
		
				if (empty($data) OR ! is_array($data))
				{
					throw Cval_Unserialize_Exception::create('Schedule_Send_Response', 'Parameter "data" must be not empty array');
				}
				$response->_result = (bool) Arr::get($data, 'result');
			}
			catch (Exception $e)
			{
				$this->_error = TRUE;
				Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			}
		}
		
		return $response;
	}
	
	/**
	 * Fill response data from request. This is called on guest server,
	 * when request is received.
	 * 
	 * @param	Schedule_Send_Request	Send request
	 * @return	Schedule_Send_Response
	 */
	public function create(Schedule_Request $request)
	{
		$response = $this;
		
		$response->_request = $request;
		$response->_result = $response->_create_schedule();
		
		return $response;
	}
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize()
	{
		$data = array(
			'result' => $this->_result
		);
		
		return $data;
	}
	
	/**
	 * @return	bool
	 */
	public function get_result()
	{
		return $this->_result;
	}
	
	/**
	 * Creates Model_Schedule according requested schedule.
	 * 
	 * @return	bool	TRUE if schedule saved, FALSE otherwise
	 */
	protected function _create_schedule()
	{
		// First delete old requests
		Model_Schedule_Request::delete_old();
		
		// Get unserialized schedule
		$schedule = $this->_request->get_schedule();
		// Schedule was already received, do not update it!
		if ($schedule->loaded())
		{
			return TRUE;
		}
		
		// Check if schedule can be planned
		$schedule->planning_interval_check(TRUE);
		
		// Load schedule request
		$schedule_request 
			= Model_Schedule_Request::load_for_schedule($this->_request->get_schedule());
		
		$locations_to_pair = array();
		
		try
		{
			Database::instance()->transaction_start();
			
			// Requested schedule does not exists, or was old, so deleted before 
			if ( ! $schedule_request->loaded())
			{
				return FALSE;
			}
			else
			{
				// If loaded delete request schedule
				$schedule_request->delete();
			}

			if ( ! $schedule->organizer->loaded())
			{
				Schedule_Log::debug($schedule, 'received schedule organizer not yet created - '.json_encode($schedule->organizer->as_array_internal()));
				
				try
				{
					// Create organizer profile if not created yet
					$schedule->organizer = $schedule->organizer->save();
					Schedule_Log::debug($schedule, 'received schedule organizer created');
				}
				catch (Model_Validate_Exception $e)
				{
					Kohana::$log->add(Kohana::ERROR, 'schedule organizer not validated - '.json_encode($e->get_array()->detailed_errors()));
					throw $e;
				}
			}

			// Save schedule
			$schedule->save();
			
			Schedule_Log::debug($schedule, 'inbound schedule created');
			
			// Save schedule guests
			$locations_to_pair = $this->_create_schedule_guests($schedule);
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			throw $e;
		}
		
		// Try to send email
		$this->send_inbound_received_email($schedule);
		// Try to pair locations
		$this->pair_locations($schedule, $locations_to_pair);
		
		return TRUE;
	}
	
	protected function _create_schedule_guests(Model_Schedule $schedule)
	{
		$schedule_owner = $this->_request->get_guest()->user_profile_shared();
		// TRUE if in guests is this requests guest, i.e. local schedule owner
		$schedule_owner_found = FALSE;
		// Array of location ids to pair
		$locations_to_pair = array();
		
		foreach ($this->_request->get_serialized_guests() as $serialized_guest)
		{
			// Unserialize guest, without need for loaded location
			$guest = Model_User_Profile_Shared::unserialize($serialized_guest, FALSE, FALSE);
			
			// Schedule owner found in guests
			if ($guest->loaded() AND $guest->id() == $schedule_owner->id())
			{
				$schedule_owner_found = TRUE;
			}
			
			// Then if guest location is not loaded
			if ( ! $guest->location->loaded())
			{
				Kohana::$log->add(Kohana::ERROR, json_encode($guest->location->as_array_internal()));
				// Create the location
				$guest->location = $guest->location->save();
				// Mark this location for pair
				$locations_to_pair[] = $guest->location->id();
			}
			// Save guest if not exists
			if ( ! $guest->loaded())
			{
				$guest->save();
			}
			
			try
			{
				Jelly::factory('schedule_user_profile_shared')
					->set(array(
						'schedule' => $schedule,
						'user_profile_shared' => $guest
					))->save();
			}
			catch (Model_Validate_Exception $e)
			{
				Kohana::$log->add(Kohana::ERROR, 'schedule guest not validated - '.json_encode($e->get_array()->detailed_errors()));
				throw $e;
			}
		}
		
		try
		{
			// Make sure that schedule contains owner of schedule
			if ( ! $schedule_owner_found)
			{
				// First guest is always this one
				Jelly::factory('schedule_user_profile_shared')
					->set(array(
						'schedule' => $schedule,
						'user_profile_shared' => $schedule_owner
					))->save();
			}
		}
		catch (Model_Validate_Exception $e)
		{
			Kohana::$log->add(Kohana::ERROR, 'schedule guest not validated - '.json_encode($e->get_array()->detailed_errors()));
			throw $e;
		}
		
		return $locations_to_pair;
	}
	
	/**
	 * Try to pair locations
	 * 
	 * @param Model_Schedule			Schedule which is initiating pairing
	 * @param array|Jelly_Collection	Array of locations ids or models to pair	
	 */
	public function pair_locations(Model_Schedule $schedule, $ids)
	{
		// If local location is not in open connectivity do not try to pair 
		if ( ! Location_Pairing::allow_autopair())
		{
			return;
		}
		
		foreach ($ids as $id)
		{
			try
			{
				$location = Model_Location::load($ids);
				if ( ! $location->loaded())
				{
					continue;
				}
			
				Schedule_Log::debug($schedule, strtr('location pairing :url started', array(
					':url' => $location->url
				)));
				Location_Pairing::request($location);
				Schedule_Log::debug($schedule, strtr('location pairing :url done ', array(
					':url' => $location->url
				)).json_encode($location->as_array_internal()));
			}
			catch (Exception $e)
			{
				// Log exception on pairing
				Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			}
		}
	}
	
	public function send_inbound_received_email(Model_Schedule $schedule)
	{
		try
		{
			$user = $schedule->user;

			if ($user->user_profile()->schedule_email_on_inbound)
			{
				$guests = $schedule->get('schedule_user_profiles_shared');
				// Count all guests
				$guests_count = $guests->count();
		
				// Limit guests in email to 10
				$guests = $guests->limit(10)
					->order_by('label')
					->execute();
				
				// Set this user as cval user, so we can use date formating
				// and translate text according this user preferences in email
				Cval::instance()->user = $user;
				
				$params = array(
					'user' => $user,
					'schedule' => $schedule,
					'schedule_url' => $schedule->detail_url(),
					'guests' => $guests,
					'guests_count' => $guests_count
				);
				
				$subject = Cval::i18n(':cval - Inbound schedule - :title @ :date (:organizer)', array(
					':cval' => Cval::instance()->location()->name(),
					':title' => $schedule->label,
					':date' => Cval_Date::format($schedule->start, TRUE).' - '.Cval_Date::format($schedule->end, TRUE),
					':organizer' => $schedule->organizer->friendly_name()
				));
				
				$ics_content = Schedule_Download::format_ics($schedule, FALSE);
				$filename = Schedule_Download::filename($schedule, 'ics');
				
				// Connect first so we can use Swift classes
				Email::connect();
				$attachement = Swift_Attachment::newInstance($ics_content, $filename, 'text/calendar');

				Email::send_template(array($user->email, $user->name()), 'schedule/inbound/received', $params, $subject, NULL, NULL, $attachement);
			}
		}
		catch (Exception $e)
		{
			// Log exception on pairing
			Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
		}
	}
	
}