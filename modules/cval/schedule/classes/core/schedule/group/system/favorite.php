<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System_Favorite
 * 
 * Class for system schedule group with key 'favorite'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System_Favorite extends Schedule_Group_System
{

	public function assignable()
	{
		return TRUE;
	}
	
	public function name()
	{
		return Cval::i18n('Favorites');
	}
	
	public function is_assigned(Model_Schedule $schedule, $positive = TRUE)
	{
		return $schedule->is_favorite($positive);
	}
	
	protected function _schedules(Model_User $user)
	{
		return parent::_schedules($user)
				->is_favorite();
	}

}
