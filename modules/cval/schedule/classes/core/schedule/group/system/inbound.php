<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System_Inbound
 * 
 * Class for system schedule group with key 'inbound'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System_Inbound extends Schedule_Group_System
{

	public function assignable()
	{
		return FALSE;
	}
	
	public function allow_outbound()
	{
		return FALSE;
	}
	
	public function allow_draft()
	{
		return FALSE;
	}
	
	public function name()
	{
		return Cval::i18n('Inbound');
	}
	
	public function is_assigned(Model_Schedule $schedule, $positive = TRUE)
	{
		return $schedule->is_inbound($positive);
	}
	
	protected function _schedules(Model_User $user)
	{
		return parent::_schedules($user)
				->is_inbound();
	}

}
