<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System_Unsync
 * 
 * Class for system schedule group with key 'unsync'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System_Unsync extends Schedule_Group_System
{

	public function assignable()
	{
		return TRUE;
	}
	
	public function allow_draft()
	{
		return FALSE;
	}
	
	public function name()
	{
		return Cval::i18n('Unsynchronized');
	}
	
	public function is_assigned(Model_Schedule $schedule, $positive = TRUE)
	{
		return $schedule->is_unsync($positive);
	}
	
	protected function _schedules(Model_User $user)
	{
		return parent::_schedules($user)
				->is_unsync();
	}
	
	public function higlighted_schedules_count(Model_User $user)
	{
		// Do not display count of schedules in unsync folder
		return 0;
	}

}
