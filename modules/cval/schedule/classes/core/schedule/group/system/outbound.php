<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System_Outbound
 * 
 * Class for system schedule group with key 'outbound'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System_Outbound extends Schedule_Group_System
{

	public function assignable()
	{
		return FALSE;
	}
	
	public function name()
	{
		return Cval::i18n('Outbound');
	}
	
	public function is_assigned(Model_Schedule $schedule, $positive = TRUE)
	{
		return $schedule->is_outbound($positive);
	}
	
	protected function _schedules(Model_User $user)
	{
		return parent::_schedules($user)
				->is_outbound();
	}
	
	public function higlighted_schedules_count(Model_User $user)
	{
		// Outbound has always none higlighted items
		return 0;
	}

}
