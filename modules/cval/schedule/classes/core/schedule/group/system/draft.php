<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System_Draft
 * 
 * Class for system schedule group with key 'draft'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System_Draft extends Schedule_Group_System
{

	public function assignable()
	{
		return FALSE;
	}
	
	public function name()
	{
		return Cval::i18n('Drafts');
	}
	
	public function is_assigned(Model_Schedule $schedule, $positive = TRUE)
	{
		return $schedule->is_draft($positive);
	}
	
	protected function _schedules(Model_User $user)
	{
		return parent::_schedules($user)
				->is_draft();
	}
	
	public function higlighted_schedules_count(Model_User $user)
	{
		// Draft higlighted items are equal to count of all drafts
		return $this->schedules_count($user);
	}

}
