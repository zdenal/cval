<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System_All
 * 
 * Class for system schedule group with key 'all'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System_All extends Schedule_Group_System
{

	public function assignable()
	{
		return FALSE;
	}
	
	public function is_assigned(Model_Schedule $schedule)
	{
		return TRUE;
	}
	
	public function display_in_list()
	{
		return FALSE;
	}
	
	public function name()
	{
		return Cval::i18n('All');
	}

	public function higlighted_schedules_count(Model_User $user)
	{
		// All has always none higlighted items
		return 0;
	}

}
