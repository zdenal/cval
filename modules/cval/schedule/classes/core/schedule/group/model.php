<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_Model
 * 
 * Class for database schedule group.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_Model extends Schedule_Group
{
	
	/**
	 * @param	Model_Schedule_Group|string		database model instance or pk
	 * @return	Schedule_Group_Model
	 */
	public static function instance($model)
	{
		if ( ! $model instanceof Model_Schedule_Group)
		{
			$id = $model;
			$model = Jelly::select('schedule_group')->load($id);
			
			if ( ! $model->loaded())
			{
				throw Model_Not_Found_Exception::create($model, $id);
			}
		}
		
		return new Schedule_Group_Model($model);
	}
	
	public function __construct(Model_Schedule_Group $model)
	{
		$this->_model = $model;
	}

	public function assignable()
	{
		return TRUE;
	}
	
	public function deletable()
	{
		return TRUE;
	}

	public function editable()
	{
		return TRUE;
	}
	
	public function allow_outbound()
	{
		return TRUE;
	}
	
	public function allow_draft()
	{
		return TRUE;
	}

	public function id()
	{
		return $this->_model->id();
	}

	public function name()
	{
		return $this->_model->name();
	}
	
	public function model()
	{
		return $this->_model;
	}
	
	protected function _schedules(Model_User $user)
	{
		return $this->_model->get('schedules')
					->where('deleted', '!=', 1);
	}
	
	/**
	 * @var	Model_Schedule_Group
	 */
	protected $_model;

}
