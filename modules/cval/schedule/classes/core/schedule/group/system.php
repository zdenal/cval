<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group_System
 * 
 * Class for system schedule group.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group_System extends Schedule_Group
{
	/**
	 * @param	string	system group key
	 * @return	Schedule_Group_System
	 */
	public static function instance($key)
	{
		$class = 'schedule_group_system_'.$key;
		
		return new $class($key);
	}
	
	/**
	 * Gets array of all system instances
	 * 
	 * @return	array 
	 */
	public static function instances()
	{
		return array(
			'inbound' => Schedule_Group::factory('inbound'),
			'outbound' => Schedule_Group::factory('outbound'),
			'draft' => Schedule_Group::factory('draft'),
			'unsync' => Schedule_Group::factory('unsync'),
			'all' => Schedule_Group::factory('all'),
			'favorite' => Schedule_Group::factory('favorite'),
		);
	}
	
	/**
	 * Check if passed schedule belongs to this group
	 * 
	 * @param	Model_Schedule	
	 * @return	bool
	 */
	abstract public function is_assigned(Model_Schedule $schedule);
	
	public function __construct($key)
	{
		$this->_key = $key;
	}

	public function deletable()
	{
		return FALSE;
	}

	public function editable()
	{
		return FALSE;
	}
	
	public function allow_outbound()
	{
		return TRUE;
	}
	
	public function allow_draft()
	{
		return TRUE;
	}

	public function id()
	{
		return $this->_key;
	}
	
	/**
	 * Returns builder for all schedules for this group and given user
	 * 
	 * @params	Model_User		Owner of schedules
	 * @return	Model_Builder_Schedule
	 */
	protected function _schedules(Model_User $user)
	{
		return Jelly::select('schedule')
				->where('deleted', '!=', 1)
				->where('user', '=', $user->id());
	}
	
	/**
	 * @var	string
	 */
	protected $_key;

}
