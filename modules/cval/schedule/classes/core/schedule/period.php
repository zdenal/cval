<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Schedule Helper Period
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Period
{
	
	public static function debug($period)
	{
		$period = array(Date::format($period[0]), Date::format($period[1]));
		
		return Kohana::debug($period);
	}
	
	public static function compare(array $p1, array $p2)
	{
		if ($p1[0] != $p2[0])
		{
			// Earlier start is lower
			return ($p1[0] < $p2[0]) ? -1 : 1;
		}
		
		if ($p1[1] != $p2[1])
		{
			// Earlier end is lower
			return ($p1[1] < $p2[1]) ? -1 : 1;
		}
		
		return 0;
	}
	
	public static function _compare_timestamp(array $t1, array $t2)
	{
		if ($t1[0] != $t2[0])
		{
			// Earlier timestamp is lower 
			return ($t1[0] < $t1[0]) ? -1 : 1;
		}
		
		if ($t1[1] != $t2[1])
		{
			// Period start is before end for fine overlapping
			return $t1[1] ? 1 : -1;
		}
		
		return 0;
	}
	
	public static function merge_periods(array $periods, $start, $end)
	{
		if ( ! count($periods))
		{
			return array(
				array($start, $end)
			);
		}
		
		$timeline = array();
		
		foreach ($periods as $period)
		{
			$timeline[] = array($period[0], TRUE);
			$timeline[] = array($period[1], FALSE);
		}
		
		usort($timeline, 'Schedule::_compare_period_times');
		
		$timestamps = array();
		$open_cnt = 0;
		$last_timestamp = NULL;
		
		foreach ($timeline as $time)
		{
			$timestamp = $time[0];
			$open = $time[1];
			if ($open)
			{
				$open_cnt++;
			}
			else
			{
				$open_cnt--;
			}
			
			if ($timestamp <= $start)
			{
				continue;
			}
			if ($timestamp >= $end)
			{
				break;
			}
			
			if ($open AND $open_cnt === 1)
			{
				if ($last_timestamp === NULL)
				{
					$timestamps[] = $start;
				}
				$timestamps[] = $last_timestamp = $timestamp;
			}
			elseif ( ! $open AND $open_cnt === 0)
			{
				$timestamps[] = $last_timestamp = $timestamp;
			}
		}
		
		if (count($timestamps)%2)
		{
			$timestamps[] = $end;
		}
		
		$inverse_periods = array();
		foreach ($timestamps as $key => $timestamp)
		{
			if ($key%2)
			{
				$inverse_periods[] = array($timestamps[$key-1], $timestamp);
			}
		}
		
		return $inverse_periods;
	}
	
}