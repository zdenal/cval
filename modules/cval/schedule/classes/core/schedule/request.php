<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Request
 * 
 * Abstract class for schedule requests
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Request implements Schedule_Request_Interface
{
	
	/**
	 * @var	Model_Schedule
	 */
	protected $_schedule;
	
	/**
	 * @var	User_Agent
	 */
	protected $_organizer;
	
	/**
	 * @var	User_Agent
	 */
	protected $_guest;
	
	/**
	 * @return	Model_Schedule 
	 */
	public function get_schedule()
	{
		return $this->_schedule;
	}

	/**
	 * @param	Model_Schedule
	 * @return	$this
	 */
	public function set_schedule(Model_Schedule $schedule)
	{
		$this->_schedule = $schedule;
		return $this;
	}
	
	/**
	 * @return	User_Agent 
	 */
	public function get_guest()
	{
		return $this->_guest;
	}

	/**
	 * @param	User_Agent
	 * @return	$this
	 */
	public function set_guest(User_Agent $guest)
	{
		$this->_guest = $guest;
		return $this;
	}
	
	/**
	 * @return	User_Agent 
	 */
	public function get_organizer()
	{
		return $this->_organizer;
	}

	/**
	 * @param	User_Agent
	 * @return	$this
	 */
	public function set_organizer(User_Agent $organizer)
	{
		$this->_organizer = $organizer;
		return $this;
	}
	
}