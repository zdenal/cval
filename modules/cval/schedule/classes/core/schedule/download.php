<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Download
 * 
 * Class for downloading schedules.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Download
{

	/**
	 * Create event from given schedule and as default
	 * send it to browser for download.
	 * 
	 * @param	string	Format of downloaded file
	 * @param	Model_Schedule	Source schedule
	 * @param	bool	TRUE for download, FALSE for returning as string
	 * @return	string|void 
	 */
	public static function format($format, Model_Schedule $schedule, $download = TRUE)
	{
		$callback = 'Schedule_Download::format_'.strtolower($format);
		if ( ! is_callable($callback))
		{
			throw new Kohana_Exception('Schedule download format \':format\' is not supported', array(
				':format' => $format
			));
		}
		return call_user_func($callback, $schedule, $download);
	}
	
	/**
	 * Create iCalendar event from given schedule and as default
	 * send it to browser for download.
	 * 
	 * @param Model_Schedule	Source schedule
	 * @param bool	TRUE for download, FALSE for returning as string
	 * @return string|void 
	 */
	public static function format_ics(Model_Schedule $schedule, $download = TRUE)
	{
		$dt_stamp = gmdate('Ymd', $schedule->created_at).'T'.gmdate('His', $schedule->created_at).'Z';
		$dt_start = gmdate('Ymd', $schedule->start).'T'.gmdate('His', $schedule->start).'Z';
		$dt_end = gmdate('Ymd', $schedule->end).'T'.gmdate('His', $schedule->end).'Z';
		
		$location_url = $schedule->organizer->location->url;
		$scheme = parse_url($location_url, PHP_URL_SCHEME);
		if ($scheme)
		{
			$location_url = preg_replace('~^'.$scheme.':\/\/~u', '', $location_url);
		}

		$uid = $dt_start.'-'.$schedule->hash.'@'.$location_url;

		$ical = "BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//ETN//NONSGML v1.0//EN
BEGIN:VEVENT
UID:".$uid."
DTSTAMP:".$dt_stamp."
DTSTART:".$dt_start."
DTEND:".$dt_end."
SUMMARY:".$schedule->label;
		
		if (strlen($schedule->place))
		{
			$ical .= "\nLOCATION:".$schedule->place;
		}
		if (strlen($schedule->description))
		{
			$ical .= "\nDESCRIPTION:".$schedule->description;
		}
		
		/*
		$ical .= "
ORGANIZER;CN=".$schedule->organizer->label;
		
		foreach ($schedule->schedule_user_profiles_shared as $guest)
		{
			$ical .= "
ATTENDEE;CN=".$guest->label;
		}*/

		$ical .= "
END:VEVENT
END:VCALENDAR";
		
		// Do not download file, just return event content
		if ( ! $download)
		{
			return $ical;
		}
		
		$filename = Schedule_Download::filename($schedule, 'ics');

		// Get request object
		$request = Request::instance();
		// Send file
		$request->response = $ical;
		$request->send_file(TRUE, $filename, array(
			'mime_type' => 'text/calendar'
		));
	}
	
	/**
	 * Create filename from schedule and appends extension to it
	 * 
	 * @param	Model_Schedule $schedule
	 * @param	string $ext
	 * @return	string 
	 */
	public static function filename(Model_Schedule $schedule, $ext)
	{
		$dt_start = gmdate('Ymd', $schedule->start).'T'.gmdate('His', $schedule->start).'Z';
		return ('Cval-'.$dt_start.'-'.$schedule->hash.'.'.strtolower($ext));
	}

}
