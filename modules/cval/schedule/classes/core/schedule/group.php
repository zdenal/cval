<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_Group
 * 
 * Class for system and database schedule groups.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Group
{

	/**
	 * @param	mixed	Model_Schedule_Group or string for system groups
	 * @return	Schedule_Group 
	 */
	public static function factory($schedule_group)
	{
		if ($schedule_group instanceof Model_Schedule_Group OR strpos($schedule_group, '__') !== FALSE)
		{
			return Schedule_Group_Model::instance($schedule_group);
		}
		
		return Schedule_Group_System::instance($schedule_group);
	}
	
	/**
	 * Returns REST data with same format as classic model REST output.
	 * It is needed for non DB groups.
	 * This method accepts array of group instances and returns their rest
	 * data.
	 * 
	 * @param	array	Array of group instances
	 * @return	array
	 */
	public static function rest_list(array $groups, array $options = array())
	{
		$rest = array();
		foreach ($groups as $group)
		{
			$rest[] = $group->rest($options);
		}
		
		return $rest;
	}
	
	/**
	 * Group unique id  
	 */
	abstract public function id();
	
	/**
	 * Group name 
	 */
	abstract public function name();
	
	/**
	 * Can be edited 
	 */
	abstract public function editable();
	
	/**
	 * Can be deleted 
	 */
	abstract public function deletable();
	
	/**
	 * Can user add/remove items to/from this group 
	 */
	abstract public function assignable();
	
	/**
	 * Can user create outbound schedules in this group 
	 */
	abstract public function allow_outbound();
	
	/**
	 * Can user asign draft schedules in this group 
	 */
	abstract public function allow_draft();
	
	/**
	 * Returns builder for all schedules for this group and given user
	 * 
	 * @params	Model_User		Owner of schedules
	 * @return	Model_Builder_Schedule
	 */
	abstract protected function _schedules(Model_User $user);
	
	/**
	 * Returns all schedules for this group and given user
	 * 
	 * @return	Jelly_Collection
	 * @return	Model_Builder_Schedule		If second parameter is FALSE
	 */
	public function schedules(Model_User $user, $execute = TRUE, $order = TRUE)
	{
		$builder = $this->_schedules($user);
		
		if ($execute)
		{
			if ($order)
			{
				$builder->use_default_order();
			}
			return $builder->execute();
		}
		
		return $builder;
	}
	
	/**
	 * Returns unread schedules for this group and given user
	 * 
	 * @return	Jelly_Collection
	 * @return	Model_Builder_Schedule		If second parameter is FALSE
	 */
	public function unread_schedules(Model_User $user, $execute = TRUE, $order_by = 'label')
	{
		$builder = $this->_schedules($user)
			->is_unread();
		
		if ($execute)
		{
			return $builder->order_by($order_by)->execute();
		}
		
		return $builder;
	}
	
	/**
	 * Returns number of all schedules for this group and given user
	 * 
	 * @return	int
	 */
	public function schedules_count(Model_User $user)
	{
		$builder = $this->schedules($user, FALSE);
		
		return $builder->count();
	}
	
	/**
	 * Returns number of unread schedules for this group and given user
	 * 
	 * @return	int
	 */
	public function unread_schedules_count(Model_User $user)
	{
		$builder = $this->unread_schedules($user, FALSE);
		
		return $builder->count();
	}
	
	/**
	 * Returns number of highlighted schedules in group menu for this group 
	 * and given user
	 * 
	 * @return	int
	 */
	public function higlighted_schedules_count(Model_User $user)
	{
		return $this->unread_schedules_count($user);
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-schedule_group-id-'.$this->id();
	}
	
	/**
	 * Returns REST data with same format as classic model REST output.
	 * It is needed for non DB groups.
	 * 
	 * @return	array
	 */
	public function rest(array $options = array())
	{
		$user = Arr::get($options, 'user');
		$highlighted_schedules_count = (bool) Arr::get($options, 'highlighted_schedules_count');
		
		$rest = array(
			'id' => $this->id(),
			'label' => $this->name(),
			'fields' => array(
				'id' => array(
					'value' => $this->id()
				),
				'label' => array(
					'value' => $this->name()
				),
				'editable' => array(
					'value' => $this->editable()
				),
				'deletable' => array(
					'value' => $this->deletable()
				),
				'assignable' => array(
					'value' => $this->assignable()
				),
				'allow_outbound' => array(
					'value' => $this->allow_outbound()
				),
			)
		);
		
		if ($highlighted_schedules_count AND $user AND $user instanceof Model_User)
		{
			$rest['fields']['highlighted_schedules_count'] = array(
				'value' => $this->higlighted_schedules_count($user)
			);
		}
		
		return $rest;
	}
	
	/**
	 * URL for list of schedules in this group.
	 * 
	 * @return	string
	 */
	public function url()
	{
		return Route::url_protocol('schedules', array(
				'controller' => 'list'
			)).'&'.http_build_query(array(
				'group' => $this->id()
			));
	}
	
	/**
	 * Tells if this group is displayed in list off assigned groups for schedule.
	 * 
	 * @return	bool
	 */
	public function display_in_list()
	{
		return TRUE;
	}

}
