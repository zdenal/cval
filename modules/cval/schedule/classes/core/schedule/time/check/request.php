<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Time Check Request
 * 
 * Used when checking time for organizer
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Time_Check_Request extends Schedule_Request
{	
	/**
	 * @return Schedule_Time_Check_Request 
	 */
	public static function factory()
	{
		return new Schedule_Time_Check_Request;
	}
	
	/**
	 * Fetch this object from array. It is used when receiving request.
	 * 
	 * @param	array
	 * @return	Schedule_Time_Check_Request
	 */
	public function unserialize($data)
	{
		if (empty($data) OR ! is_array($data))
		{
			throw Cval_Unserialize_Exception::create($this, 'Parameter "data"  must be not empty array');
		}
		
		$request = $this;
		
		$request->_guest = User_Agent::unserialize(Arr::get($data, 'guest'), TRUE);
		$request->_organizer = User_Agent::unserialize(Arr::get($data, 'organizer'));
		Kohana::$log->add(Kohana::DEBUG, 'schedule time check request schedule data: '.json_encode(Arr::get($data, 'schedule')));
		$request->_schedule = Model_Schedule::unserialize(Arr::get($data, 'schedule'));
		Schedule_Log::debug($request->_schedule, 'time check request schedule unserialized');
		
		return $request;
	}
	
	/**
	 * Fill request data from schedule. This is called on organizer server,
	 * when request is created.
	 * 
	 * @param	Model_Schedule	Schedule for request
	 * @return	Schedule_Time_Check_Request
	 */
	public function create(Model_Schedule $schedule)
	{
		$request = $this;
		
		$request->_schedule = $schedule;
		$request->_organizer = $schedule->organizer->agent();
		
		return $request;
	}
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize()
	{
		$schedule_data = $this->_schedule->serialize();
		// Change local user to guest
		$schedule_data['user'] = $this->_guest->user_profile_shared()->serialize();
		
		$data = array(
			'guest' => $this->_guest->serialize(),
			'organizer' => $this->_organizer->serialize(),
			'schedule' => $schedule_data,
		);
		
		return $data;
	}
	
	/**
	 * Sends request and returns response.
	 * 
	 * @param	User_Agent	Optional target user
	 * @return	Schedule_Time_Check_Response
	 */
	public function send(User_Agent $guest = NULL)
	{
		if ($guest !== NULL)
		{
			$this->_guest = $guest;
		}
		
		$location = $this->_guest->location();
		
		$uri = Route::get('schedule')->uri(array(
			'controller' => 'time_check_request'
		));
		
		$data = $this->serialize();

		$response = $location->request($uri, $data);
		$response = Schedule_Time_Check_Response::factory()->unserialize($response);
		
		return $response;
	}
	
}