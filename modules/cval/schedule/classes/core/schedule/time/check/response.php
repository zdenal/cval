<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Time Check Response
 * 
 * Used when checking time for guest
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Time_Check_Response extends Schedule_Response
{
	
	/**
	 * @var	Schedule_Time_Check_Request
	 */
	protected $_request;
	
	/**
	 * @var	bool	TRUE if time is available, FALSE if time is full
	 */
	protected $_time_result;
	
	/**
	 * @return Schedule_Time_Check_Response 
	 */
	public static function factory()
	{
		return new Schedule_Time_Check_Response;
	}
	
	/**
	 * Fetch this object from array. It is used when response for request 
	 * is returned.
	 * 
	 * @param	Location_Response
	 * @return	Schedule_Time_Check_Response
	 */
	public function unserialize(Location_Response $location_response)
	{	
		$response = $this;
		$response->_location_response = $location_response;
		
		if ($location_response->error())
		{
			$response->_error = TRUE;
		}
		else
		{
			try
			{
				$data = $location_response->data();
		
				if (empty($data) OR ! is_array($data))
				{
					throw Cval_Unserialize_Exception::create('Schedule_Time_Check_Response', 'Parameter "data" must be not empty array');
				}
				$response->_time_result = (bool) Arr::get($data, 'time_result');
			}
			catch (Exception $e)
			{
				$this->_error = TRUE;
				Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			}
		}
		
		return $response;
	}
	
	/**
	 * Fill response data from request. This is called on guest server,
	 * when request is received.
	 * 
	 * @param	Schedule_Time_Check_Request	Time_Check request
	 * @return	Schedule_Time_Check_Response
	 */
	public function create(Schedule_Request $request)
	{
		$response = $this;
		
		$response->_request = $request;
		
		Schedule_Log::debug($response->_request->get_schedule(), 'time check start');
		
		$response->_time_result = $response->_check_time();
		
		if ($response->_time_result)
		{
			Schedule_Log::debug($response->_request->get_schedule(), 'time found');
			$response->_create_schedule_request();
		}
		else
		{
			Schedule_Log::debug($response->_request->get_schedule(), 'time not found');
		}
		
		return $response;
	}
	
	/**
	 * Converts request to array. It is used as post data.
	 * 
	 * @return	array
	 */
	public function serialize()
	{
		$data = array(
			'time_result' => $this->_time_result
		);
		
		return $data;
	}
	
	/**
	 * @return	bool
	 */
	public function get_time_result()
	{
		return $this->_time_result;
	}
	
	/**
	 * Check requested schedule time against guest preferences
	 * 
	 * @param	Model_Schedule	Schedule to check
	 * @return	bool	TRUE if time is available, FALSE otherwise
	 */
	protected function _check_time()
	{	
		$schedule = $this->_request->get_schedule();
		
		$result = $this->_request->get_guest()
				->user()
				->has_free_time($schedule->start, $schedule->end);
		
		return (bool) $result;
	}
	
	/**
	 * Creates Model_Schedule_Request for requested schedule, which than needs
	 * to be received in request when schedule is sent to guests.
	 * 
	 * @return	void 
	 */
	protected function _create_schedule_request()
	{	
		// First delete old requests
		Model_Schedule_Request::delete_old();
		
		Schedule_Log::debug($this->_request->get_schedule(), 'schedule request create start');
		
		// Do not create schedule request for organizer
		if ($this->_request->get_organizer()
				->compare($this->_request->get_guest()))
		{
			Schedule_Log::debug($this->_request->get_schedule(), 'organizer is same as guest, schedule request not created');
			return;
		}
		
		// Load schedule request
		$schedule_request 
			= Model_Schedule_Request::load_for_schedule($this->_request->get_schedule());
		
		//echo Kohana::debug_exit($this->_request->get_schedule()->as_array_internal());
		
		// If request not exists create it
		if ( ! $schedule_request->loaded())
		{
			Schedule_Log::debug($this->_request->get_schedule(), 'schedule request not yet created');
			
			$schedule = $this->_request->get_schedule();
			// Than create request
			Jelly::factory('schedule_request')
				->set(array(
					'user' => $schedule->user,
					'location' => $schedule->organizer->location,
					'hash' => $schedule->hash,
					'organizer_hash' => $schedule->organizer->hash
				))->save();
			
			Schedule_Log::debug($this->_request->get_schedule(), 'schedule request created');
		}
		else
		{
			Schedule_Log::debug($this->_request->get_schedule(), 'schedule request already created');
		}
	}
	
}