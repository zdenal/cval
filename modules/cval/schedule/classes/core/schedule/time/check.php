<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Time_Check
 * 
 * Used when negotiating 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_Time_Check
{
	/**
	 * @return	Schedule_Time_Check
	 */
	public static function factory()
	{
		return new Schedule_Time_Check;
	}
	
	/**
	 * Starts negotiation process
	 * 
	 * @param	Model_Schedule	schedule to negotiate
	 * @return	array			period collection
	 * @throws	Kohana_Exception	when some guest not found in local db
	 */
	public function start(Model_Schedule $schedule)
	{
		// Set schedule
		$this->_schedule = $schedule;
		
		// Get organizer agent
		$organizer_agent = $schedule->organizer->agent();
		
		// Create request
		$request = Schedule_Time_Check_Request::factory()->create($schedule);
		// Empty response
		$response = NULL;
		
		try
		{
			if ( ! $schedule->schedule_plan->skip_organizer)
			{
				$error = FALSE;
				try
				{
					// First check organizer time
					$response = $request->send($organizer_agent);
					$error = $response->is_error();
				}
				catch (Location_Exception $e)
				{
					Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
					$error = TRUE;
				}
				
				// Not responding guest continue to another guest
				if ($error)
				{
					return $this->_finish(Model_Schedule::STATE_TIME_CHECKING_NORESPONSE_ORGANIZER);
				}
				
				if ( ! $response->get_time_result())
				{
					return $this->_finish(Model_Schedule::STATE_TIME_FULL_ORGANIZER);
				}
				//echo Kohana::debug($response->serialize());
			}

			$guest_meta = Jelly::meta('schedule_user_profile_shared');
			$guest_default_values = array(
				'is_responding' => $guest_meta->fields('is_responding')->default,
				'is_full' => $guest_meta->fields('is_full')->default
			);
			// Get guests ids and clear guests values
			$guests_ids = array();
			foreach ($schedule->get('schedule_user_profiles_shared')->execute() as $guest)
			{
				$guests_ids[] = $guest->id();
				// Set default values for guest
				$guest->set($guest_default_values)->save();
			}
			// Make it random order
			shuffle($guests_ids);
			
			// Check if someone has no response
			$responding = TRUE;

			foreach ($guests_ids as $guest_id)
			{
				$guest = Jelly::select('schedule_user_profile_shared')->load($guest_id);
				if ( ! $guest->loaded())
				{
					continue;
				}
				
				$profile = $guest->user_profile_shared;

				$error = FALSE;
				try
				{
					// Check guest time
					$tmp_response = $request->send($profile->agent());
					$error = $tmp_response->is_error();
				}
				catch (Location_Exception $e)
				{
					Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
					$error = TRUE;
				}
				
				$guest->is_responding = ! $error;
				// Not responding guest continue to another guest
				if ( ! $guest->is_responding)
				{
					$guest->save();
					$responding = FALSE;
					continue;
				}
				
				$guest->is_full = ! $response->get_time_result();
				// Full guest end cycle
				if ($guest->is_full)
				{
					$guest->save();
					return $this->_finish(Model_Schedule::STATE_TIME_FULL_GUEST);
				}
				//echo Kohana::debug($tmp_response->serialize());
				$guest->save();
				
				$response = $tmp_response;
			}
			
			// If some guest is not responding
			if ( ! $responding)
			{
				return $this->_finish(Model_Schedule::STATE_TIME_CHECKING_NORESPONSE_GUEST);
			}
		}
		catch (Exception $e)
		{
			// Rethrow exception
			throw $e;
		}
		
		return $this->_finish(Model_Schedule::STATE_TIME_CHECKED, $response);
	}
	
	/**
	 * @var	Model_Schedule
	 */
	protected $_schedule;
	
	/**
	 * Is called at the end of start method.
	 * 
	 * @param	int								one of Model_Schedule::STATE_x
	 * @param	Schedule_Time_Check_Response	last negitiation response
	 * @return	array							period collection 
	 */
	protected function _finish($state, Schedule_Time_Check_Response $response = NULL)
	{
		$this->_schedule->set_state($state, FALSE);
		return $response ? $response->get_time_result() : FALSE;
	}
	
}