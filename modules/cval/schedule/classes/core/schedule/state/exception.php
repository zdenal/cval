<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Schedule_State_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Schedule_State_Exception extends Schedule_Exception {}
