<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Schedule
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Schedule extends Jelly_Model
{
	const STATE_INVALID = 10;
	const STATE_VALID = 20;
	const STATE_NEGOTIATING = 30;
	const STATE_NEGOTIATING_FAILED = 35;
	const STATE_NEGOTIATING_NORESPONSE_ORGANIZER = 36;
	const STATE_NEGOTIATING_NORESPONSE_GUEST = 38;
	const STATE_TIME_PLAN_FULL_ORGANIZER = 40;
	const STATE_TIME_PLAN_FULL_GUEST = 42;
	const STATE_TIME_NOT_SELECTED = 45;
	const STATE_TIME_SELECTED = 55;
	const STATE_TIME_CHECKING = 60;
	const STATE_TIME_CHECKING_FAILED = 65;
	const STATE_TIME_CHECKING_NORESPONSE_ORGANIZER = 66;
	const STATE_TIME_CHECKING_NORESPONSE_GUEST = 68;
	const STATE_TIME_FULL_ORGANIZER = 70;
	const STATE_TIME_FULL_GUEST = 72;
	const STATE_TIME_CHECKED = 75;
	const STATE_SENDING = 80;
	const STATE_SENDING_FAILED = 85;
	const STATE_SENT = 90;
	const STATE_RECEIVED = 95; // Only for inbound
	
	const ACTION_PROPERTIES = 'properties';
	const ACTION_PLAN = 'plan';
	const ACTION_CONTACTS = 'contacts';
	const ACTION_NEGOTIATE = 'negotiate';
	const ACTION_NEGOTIATE_CLI = 'negotiate_cli';
	const ACTION_SELECT_TIME = 'select_time';
	const ACTION_TIME_CHECK = 'time_check';
	const ACTION_TIME_CHECK_CLI = 'time_check_cli';
	const ACTION_SEND = 'send';
	const ACTION_SEND_CLI = 'send_cli';
	const ACTION_DETAIL = 'detail';

	/**
	 * @var	array	Array of Schedule_Group instances
	 */
	protected $_schedule_groups_instances;
	
	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('label');
		
		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(61),
				)
			)),
			'user' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'organizer' => new Field_BelongsTo(array(
				'foreign' => 'user_profile_shared',
				'column' => 'organizer_id',
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			/**
			 * Automatically created before save.
			 */
			'id' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'hash' => new Field_String(array(
				'callbacks' => array(
					'matches' => array('Model_Schedule', '_check_hash_unique')
				),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(32),
					'min_length' => array(32),
				)
			)),
			'deleted' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_outgoing' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_unread' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_favorite' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_unsync' => new Field_Boolean(array(
				'default' => 0
			)),
			'has_contacts' => new Field_Boolean(array(
				'default' => 0
			)),
			'has_plan' => new Field_Boolean(array(
				'default' => 0
			)),
			'has_date' => new Field_Boolean(array(
				'default' => 0
			)),
			'state' => new Field_Enum(array(
				'integer' => TRUE,
				'choices' => array(
					Model_Schedule::STATE_INVALID,
					Model_Schedule::STATE_VALID,
					Model_Schedule::STATE_NEGOTIATING,
					Model_Schedule::STATE_NEGOTIATING_FAILED,
					Model_Schedule::STATE_NEGOTIATING_NORESPONSE_ORGANIZER,
					Model_Schedule::STATE_NEGOTIATING_NORESPONSE_GUEST,
					Model_Schedule::STATE_TIME_PLAN_FULL_ORGANIZER,
					Model_Schedule::STATE_TIME_PLAN_FULL_GUEST,
					Model_Schedule::STATE_TIME_NOT_SELECTED,
					Model_Schedule::STATE_TIME_SELECTED,
					Model_Schedule::STATE_TIME_CHECKING,
					Model_Schedule::STATE_TIME_CHECKING_FAILED,
					Model_Schedule::STATE_TIME_CHECKING_NORESPONSE_ORGANIZER,
					Model_Schedule::STATE_TIME_CHECKING_NORESPONSE_GUEST,
					Model_Schedule::STATE_TIME_FULL_ORGANIZER,
					Model_Schedule::STATE_TIME_FULL_GUEST,
					Model_Schedule::STATE_TIME_CHECKED,
					Model_Schedule::STATE_SENDING,
					Model_Schedule::STATE_SENDING_FAILED,
					Model_Schedule::STATE_SENT,
					Model_Schedule::STATE_RECEIVED,
				),
				'default' => Model_Schedule::STATE_INVALID,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(3),
				)
			)),
			'start' => new Field_Datetime(array(
				'callbacks' => array(
					'not_empty' => array('Model_Schedule', '_check_start_not_empty')
				),
				'rules' => array(
					'less' => array('end')
				)
			)),
			'end' => new Field_Datetime(array(
				'callbacks' => array(
					'not_empty' => array('Model_Schedule', '_check_end_not_empty')
				),
				'rules' => array(
					'greater' => array('start')
				)
			)),
			'length' => new Field_Integer(array(
				'null' => TRUE,
				'rules' => array(
					'min_not_equal' => array(0)
				)
			)),
			'update_hash' => new Field_Schedule_Update_Hash(array(
				'null' => TRUE,
				'rules' => array(
					'max_length' => array(32),
					'min_length' => array(32),
				)
			)),
			'schedule_plan' => new Field_HasOne,
			'schedule_groups' => new Field_ManyToMany,
			'schedule_user_profiles_shared' => new Field_HasMany,
		));

		Behavior_Labelable::initialize($meta, array(
			'label' => 'title',
			'rules' => array(
				'not_empty' => NULL,
				'max_length' => array(255)
			)
		));
		Behavior_Descriptionable::initialize($meta, array(
			'rows' => 4
		));
		
		$meta->fields(array(
			'place' => new Field_String(array(
				'rules' => array(
					'max_length' => array(255),
				)
			))
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}
	
	/**
	 * Creates an instance of Model_Schedule from serialized data.
	 *
	 * @param	array	serialized data
	 * @return	Model_Schedule
	 */
	public static function unserialize($data, $exception = FALSE)
	{
		if (empty($data) OR ! is_array($data))
		{
			throw Cval_Unserialize_Exception::create('Model_Schedule', 'Parameter "data" must be not empty array');
		}
		$hash = Arr::get($data, 'hash');
		if (empty($hash))
		{
			throw Cval_Unserialize_Exception::create('Model_Schedule', 'Parameter "data" must contain "hash" value');
		}
		
		// Unserialize schedule owner, it must be from local instance, so exception
		// is thrown if not exists.
		$user = Model_User_Profile_Shared::unserialize(Arr::get($data, 'user'), TRUE)->user_profile->user;
		
		// Unserialize organizer
		$organizer = Model_User_Profile_Shared::unserialize(Arr::get($data, 'organizer'), $exception);
		
		if ($organizer->loaded())
		{
			$object = Jelly::select('schedule')
				->where('user', '=', $user->id())
				->where('organizer', '=', $organizer->id())
				->where('hash', '=', $hash)
				->load();
		}
		else
		{
			$object = Jelly::factory('schedule');
		}
		
		if ( ! $object->loaded() AND $exception)
		{
			throw Model_Not_Found_Exception::create($object, array(
				'hash' => $hash,
				'organizer_hash' => $organizer->hash,
				'user_id' => $user->id()
			));
		}
		
		$object->set(Arr::extract($data, array(
			'hash', 'start', 'end', 'label', 'description', 'place'
		)))->set(array(
			'user' => $user,
			'organizer' => $organizer
		));
		
		// Set objects as retrieved, otherwise if not loaded, they are discarded
		$object->_retrieved += array(
			'user' => $user,
			'organizer' => $organizer
		);
		
		return $object;
	}
	
	/**
	 * Validate callback wrapper for checking hash unique for location
	 * @param	Validate $array
	 * @param	string   $field
	 * @param	array		array of params with model
	 * @return	void
	 */
	public static function _check_hash_unique(Validate $array, $field, array $params = array())
	{
		$model = $params[0];
		
		$duplicated = Jelly::select($model->meta()->model())
			->where('user', '=', $model->user->id())
			->where('organizer', '=', $model->organizer->id())
			->where('hash', '=', $array[$field]);
		
		if ($model->loaded())
		{
			$duplicated->where(':primary_key', '!=', $model->id());
		}
		
		$duplicated = $duplicated->load();
		
		if ($duplicated->loaded())
		{
			$array->error($field, 'unique');
		}
	}
	
	/**
	 * Validate callback wrapper for checking start set if end exists
	 * @param	Validate	$array
	 * @param	string		$field
	 * @param	array		array of params with model
	 * @return	void
	 */
	public static function _check_start_not_empty(Validate $array, $field, array $params = array())
	{
		$model = $params[0];
		
		if (empty($array[$field]) AND ! empty($model->end))
		{
			$array->error($field, 'not_empty');
		}
	}
	
	/**
	 * Validate callback wrapper for checking end set if start exists
	 * @param	Validate	$array
	 * @param	string		$field
	 * @param	array		array of params with model
	 * @return	void
	 */
	public static function _check_end_not_empty(Validate $array, $field, array $params = array())
	{
		$model = $params[0];
		
		if (empty($array[$field]) AND ! empty($model->start))
		{
			$array->error($field, 'not_empty');
		}
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
	
		// Outgoing can not be unread
		if ($this->is_outgoing)
		{
			$this->is_unread = FALSE;
		}
		elseif ( ! $loaded)
		{
			// When is incoming and not loaded, so then is unread
			$this->is_unread = TRUE;
			// Set schedule as unsync after receive
			$this->is_unsync = TRUE;
		}
		
		// Set basic flags
		$this->has_contacts = count($this->schedule_user_profiles_shared) > 0;
		$this->has_plan = $this->schedule_plan->is_valid();
		
		// Get actual state
		$this->state = $this->_determine_state();
		
		// Clear start and end if state before time selected
		if ($this->state < Model_Schedule::STATE_TIME_SELECTED)
		{
			$this->start = $this->end = NULL;
		}
		
		// Calculate length and than set date flag if length is positive
		$this->length = $this->end - $this->start;
		$this->has_date = $this->length > 0;
		
		// On create
		if ( ! $loaded)
		{
			// Create hash automatically if it is outgoing schedule
			if ($this->is_outgoing)
			{
				// Has must be unique for organizer, using its hash
				$this->hash = md5($this->organizer->hash.uniqid());
			}
			
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
			unset($this->meta()->fields('id')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{	
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
			$this->meta()->fields('id')->rules['not_empty'] = NULL;
		}
		
		// Generate ID
		$this->id = $this->_create_id($loaded);
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_id($loaded)
	{
		if ($loaded AND ! empty($this->id))
		{
			return $this->id;
		}
		
		$last = Jelly::select($this)
				->where('user', '=', $this->user->id())
				->order_by('id', 'DESC')
				->load();
		
		$id = 1;
		
		if ($last->loaded())
		{
			$id = $last->id + 1;
		}
		
		return $id;
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		if (empty($this->id))
		{
			throw new Kohana_Exception('Model `:model` needs to have field `id` set', array(
				':model' => $this->meta()->model
			));
		}
	
		return implode('__', array($this->user->id(), $this->id));
	}
	
	protected function _after_save($loaded = NULL)
	{
		// If schedule has changed update last changed token for its user
		if ($this->last_changed())
		{
			Cval_Comet::write($this->user, 'schedule_update', array(
				'schedule' => Cval_REST::instance()->model($this)->render(),
				'unread_cnt' => Schedule_Group::factory('inbound')->unread_schedules_count($this->user)
			));
		}
		
		parent::_after_save($loaded);
	}
	
	protected function _before_delete()
	{
		parent::_before_delete();
		
		if ( ! $this->is_draft())
		{
			// Only drafts can be really deleted, others are marked as deleted
			$this->deleted = TRUE;
			$this->save();
			
			return FALSE;
		}
		
		return TRUE;
	}

	protected function _after_delete()
	{
		Cval_Comet::write($this->user, 'schedule_delete', array(
				'schedule' => Cval_REST::instance()->model($this)->render(),
				'unread_cnt' => Schedule_Group::factory('inbound')->unread_schedules_count($this->user)
			));
		
		parent::_after_delete();
	}
	
	/**
	 * Is called in save method and determines state of this schedule.
	 * If state is greater than VALID, actual state is returned, because
	 * greater states are set on demand and not automatically in save method.
	 * 
	 * @return	int
	 */
	protected function _determine_state()
	{
		$state = NULL;
		
		if ( ! $this->is_outgoing)
		{
			$state = Model_Schedule::STATE_RECEIVED;
		}
		elseif ( ! $this->has_contacts OR ! $this->has_plan)
		{
			$state = Model_Schedule::STATE_INVALID;
		}
		elseif ($this->state < Model_Schedule::STATE_VALID)
		{
			$state = Model_Schedule::STATE_VALID;
		}
		else
		{
			$state = $this->state;
		}
		
		return $state;
	}
	
	/**
	 * Generates update hash used in comet checking. This is called from
	 * update_hash field save method and is used in user object in field
	 * last_updated_schedule_hash.
	 * 
	 * @return	string 
	 */
	public function generate_update_hash()
	{
		return md5(Date::time().Text::random('default', 10));
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-schedule-id-'.$this->id();
	}
	
	/**
	 * Returns all schedule groups instances for this schedule. 
	 * Also adds favorite group.
	 * 
	 * @param	array		optional array of ids to exclude
	 * @return	array		array of Schedule_Group instances					
	 */
	public function schedule_groups_instances(array $exclude = array())
	{
		if ($this->_schedule_groups_instances === NULL)
		{
			$items = Schedule_Group_System::instances();
			
			foreach ($items as $id => $item)
			{
				if ( ! $item->display_in_list() OR ! $item->is_assigned($this))
				{
					unset($items[$id]);
					continue;
				}
			}

			$groups = $this->get('schedule_groups')
				->order_by('label')
				->execute();

			foreach ($groups as $group)
			{
				$items[$group->id()] = Schedule_Group::factory($group);
			}
			
			$this->_schedule_groups_instances = $items;
		}
		
		$schedule_groups = array();
		
		foreach ($this->_schedule_groups_instances as $id => $schedule_group)
		{
			if (in_array($id, $exclude))
			{
				continue;
			}
			
			$schedule_groups[$id] = $schedule_group;
		}
		
		return $schedule_groups;
	}
	
	/**
	 * Returns all schedule instances used in assign select.
	 *
	 * @return	array		array of Schedule_Group instances					
	 */
	public function schedule_groups_instances_assignable()
	{
		$items = array();
		
		foreach ($this->schedule_groups_instances() as $id => $item)
		{
			if ($item->assignable())
			{
				$items[$id] = $item;
			}
		}
		
		return $items;
	}
	
	/**
	 * Tests if draft.
	 * 
	 * @return	bool
	 */
	public function is_draft($positive = TRUE)
	{
		$is = $this->state < Model_Schedule::STATE_SENT;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Tests if favorite.
	 * 
	 * @return	bool
	 */
	public function is_favorite($positive = TRUE)
	{
		$is = (bool) $this->is_favorite;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Tests if unsync.
	 * 
	 * @return	bool
	 */
	public function is_unsync($positive = TRUE)
	{
		$is = (bool) $this->is_unsync;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Tests if outbound.
	 * 
	 * @return	bool
	 */
	public function is_outbound($positive = TRUE)
	{
		$is = ($this->is_outgoing AND ! $this->is_draft());
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Tests if inbound.
	 * 
	 * @return	bool
	 */
	public function is_inbound($positive = TRUE)
	{
		$is = ! $this->is_outgoing;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Tests if unread.
	 * 
	 * @return	bool
	 */
	public function is_unread($positive = TRUE)
	{
		$is = (bool) $this->is_unread;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Tests if this schedule can be edited.
	 * Only drafts in other state than NEGOTIATING can be edited.
	 * 
	 * @return	bool
	 */
	public function allow_edit()
	{
		return $this->is_draft()
				AND ! $this->check_state_in(array(
						Model_Schedule::STATE_NEGOTIATING,
						Model_Schedule::STATE_TIME_CHECKING,
						Model_Schedule::STATE_SENDING
					));
	}
	
	/**
	 * Add contact to this schedule. It checks also, that one contact is
	 * added only once.
	 * 
	 * @param	Model_Contact
	 * @return	$this
	 */
	public function add_contact(Model_Contact $contact)
	{
		if ($this->has_contact($contact))
		{
			return $this;
		}
		
		Jelly::factory('schedule_user_profile_shared')
			->set(array(
				'schedule' => $this,
				'user_profile_shared' => $contact->user_profile_shared
			))->save();
		
		return $this;
	}
	
	/**
	 * Check if this schedule has contact
	 * 
	 * @param	Model_Contact
	 * @return	bool
	 */
	public function has_contact(Model_Contact $contact)
	{
		return (bool) Jelly::select('schedule_user_profile_shared')
			->where('schedule', '=', $this->id())
			->where('user_profile_shared', '=', $contact->user_profile_shared->id())
			->count();
		
	}
	
	/**
	 * Check if this schedule has any guest.
	 * 
	 * @return	bool
	 */
	public function has_contacts()
	{
		return (bool) $this->has_contacts;
	}
	
	/**
	 * Check if this schedule has valid plan.
	 * 
	 * @return	bool
	 */
	public function has_plan()
	{
		return (bool) $this->has_plan;
	}
	
	/**
	 * Check if this schedule has date.
	 * 
	 * @return	bool
	 */
	public function has_date()
	{
		return (bool) $this->has_date;
	}
	
	/**
	 * Return schedule state.
	 * 
	 * @return	int		One of available states, i.e. Model_Schedule::STATE_x
	 */
	public function get_state()
	{
		return $this->state;
	}
	
	/**
	 * Check if schedule has passed state.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @return	bool
	 */
	public function check_state_equal($state)
	{
		return $this->state == $state;
	}
	
	/**
	 * Check if schedule has state greater than passed state.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @param	bool	If TRUE it checks also equal value
	 * @return	bool
	 */
	public function check_state_greater($state, $equal = FALSE)
	{
		return $equal ? $this->state >= $state : $this->state > $state;
	}
	
	/**
	 * Check if schedule has state less than passed state.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @param	bool	If TRUE it checks also equal value
	 * @return	bool
	 */
	public function check_state_less($state, $equal = FALSE)
	{
		return $equal ? $this->state <= $state : $this->state < $state;
	}
	
	/**
	 * Check if schedule has state greater than passed state.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @param	bool	If TRUE it checks also equal value
	 * @return	bool
	 */
	public function check_state_between($state1, $state2, $equal = FALSE)
	{
		$start = min($state1, $state2);
		$end = max($state1, $state2);
		
		if ($equal)
		{
			return ($this->state >= $start AND $this->state <= $end);
		}
		return ($this->state > $start AND $this->state < $end);
	}
	
	/**
	 * Check if schedule has one of passed states.
	 * 
	 * @param	array	Array of states, i.e. items like Model_Schedule::STATE_x
	 * @return	bool
	 */
	public function check_state_in(array $states)
	{
		foreach ($states as $state)
		{
			if ($this->check_state_equal($state))
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/**
	 * Returns state label.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @return	string 
	 */
	public function state_label($state = NULL)
	{
		if ($state === NULL)
		{
			$state = $this->state;
		}
		
		return Schedule_State::label($state);
	}
	
	/**
	 * Set state.
	 * 
	 * @param	int		One of available states, i.e. Model_Schedule::STATE_x
	 * @param	bool	Save schedule also
	 * @return	$this 
	 */
	public function set_state($state, $save = TRUE)
	{
		$this->state = $state;
		
		// Save it if needed
		if ($save)
		{
			$this->save();
		}
		
		return $this;
	}
	
	/**
	 * Find available time in organizer and guests calendars according
	 * plan periods.
	 * 
	 * @return	$this
	 * @uses	Model_Schedule_Plan::find_available_time
	 */
	public function negotiate()
	{
		$this->schedule_plan->negotiate();
		
		return $this;
	}
	
	/**
	 * Check if schedule time is available for guests.
	 * 
	 * @return	$this
	 * @throws	Kohana_Exception	When time check is not allowed
	 */
	public function time_check()
	{
		$this->is_allowed(Model_Schedule::ACTION_TIME_CHECK, TRUE);
		
		// Check limit
		list($limit_start, $limit_end) = Cval_Schedule_Timer::planning_interval();
		
		// Check if time is in limit
		if ($this->start < $limit_start)
		{
			throw new Request_Exception(440, 'Schedule can\'t be sent because is in past!');
		}
		if ($this->end > $limit_end)
		{
			throw new Request_Exception(440, 'Schedule can\'t be sent because isn\'t in limited future!');
		}
		
		// Set schedule as time checking
		$this->set_state(Model_Schedule::STATE_TIME_CHECKING);
		
		$uri = Route::get('schedule')->uri(array(
			'controller' => 'time_check_cli'
		));
		// Call asynchronnous cli time check controller
		CLI::internal_call_in_background($uri, array(
			'post' => array(
				'id' => $this->id()
			)
		));
		
		return $this;
	}
	
	/**
	 * Check if schedule time is available for guests and if so than asynchronous
	 * calls schedule sending.
	 * 
	 * @return	bool	TRUE if time was checked
	 * @throws	Kohana_Exception	When time check is not allowed
	 */
	public function time_check_cli()
	{
		$this->is_allowed(Model_Schedule::ACTION_TIME_CHECK_CLI, TRUE);
		// Check if schedule can be planned
		$this->planning_interval_check(TRUE);
		
		$status = FALSE;
		
		try 
		{
			Database::instance()->transaction_start();
			
			$negotiation = Schedule_Time_Check::factory();
			$status = $negotiation->start($this);
			
			if ($status)
			{
				// Imediatelly set state to sending
				$this->set_state(Model_Schedule::STATE_SENDING);
			}
			else
			{
				// Save this object
				$this->save();
			}
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			// Move state
			$this->set_state(Model_Schedule::STATE_TIME_CHECKING_FAILED);
			
			throw $e;
		}
		
		// Imediatelly send schedule if status is positive
		if ($status)
		{
			$uri = Route::get('schedule')->uri(array(
				'controller' => 'send_cli'
			));
			// Call asynchronnous cli send controller
			CLI::internal_call_in_background($uri, array(
				'post' => array(
					'id' => $this->id()
				)
			));
		}
		
		return $status;
	}
	
	/**
	 * Sends schedule to guest.
	 * 
	 * @return	int				number of failed guests
	 * @throws	Kohana_Exception	When sending is not allowed
	 */
	public function send_cli()
	{
		$this->is_allowed(Model_Schedule::ACTION_SEND_CLI, TRUE);
		// Check if schedule can be planned
		$this->planning_interval_check(TRUE);
		
		try 
		{
			Database::instance()->transaction_start();
			
			$negotiation = Schedule_Send::factory();
			$failed_guests_count = $negotiation->start($this);
			
			// Save this object
			$this->save();
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			// Move state
			$this->set_state(Model_Schedule::STATE_SENDING_FAILED);
			
			throw $e;
		}
		
		return $failed_guests_count;
	}
	
	public function is_allowed($action, $exception = FALSE)
	{
		if (in_array($action, array(
				Model_Schedule::ACTION_PROPERTIES, 
				Model_Schedule::ACTION_PLAN, 
				Model_Schedule::ACTION_CONTACTS
				))
			)
		{
			if ( ! $this->allow_edit())
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				$message = NULL;
				
				switch ($action)
				{
					case Model_Schedule::ACTION_PROPERTIES:
						$message = 'Schedule properties can not be modified in state :state';
						break;
					case Model_Schedule::ACTION_PLAN:
						$message = 'Schedule proposed times can not be modified in state :state';
						break;
					case Model_Schedule::ACTION_CONTACTS:
						$message = 'Schedule guests can not be modified in state :state';
						break;
				}

				throw Schedule_State_Exception::create($this, $message, array(
					':state' => $this->state_label()
				));
			}
		}
		
		if ($action == Model_Schedule::ACTION_NEGOTIATE)
		{
			if ($this->check_state_less(Model_Schedule::STATE_VALID) 
					OR ! $this->allow_edit())
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule available time can not be found in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_NEGOTIATE_CLI)
		{
			if ( ! $this->check_state_equal(Model_Schedule::STATE_NEGOTIATING))
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule available time can not be found from command line in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_SELECT_TIME)
		{
			if ($this->check_state_less(Model_Schedule::STATE_TIME_NOT_SELECTED) 
					OR ! $this->allow_edit())
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule time can not be selected in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_TIME_CHECK)
		{
			if ($this->check_state_less(Model_Schedule::STATE_TIME_SELECTED) 
					OR ! $this->allow_edit())
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule time can not be checked in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_TIME_CHECK_CLI)
		{
			if ( ! $this->check_state_equal(Model_Schedule::STATE_TIME_CHECKING))
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule time can not be checked from command line in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_SEND)
		{
			if ($this->check_state_less(Model_Schedule::STATE_TIME_CHECKED) 
					OR ! $this->allow_edit())
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule can not be sent in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_SEND_CLI)
		{
			if ( ! $this->check_state_equal(Model_Schedule::STATE_SENDING))
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule can not be sent from command line in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
		
		if ($action == Model_Schedule::ACTION_DETAIL)
		{
			if ( ! $this->check_state_greater(Model_Schedule::STATE_SENT, TRUE))
			{
				if ( ! $exception)
				{
					return FALSE;
				}
				
				throw Schedule_State_Exception::create($this,
						'Schedule detail can not be displayed in state :state', 
					array(
						':state' => $this->state_label()
					));
			}
		}
	}
	
	/**
	 * Sets date selected for schedule in timeplanning.
	 * 
	 * @param	array	Start and end timestamp
	 * @return	$this
	 */
	public function set_timeplanning_date(array $date = NULL)
	{
		if (empty($date) OR count($date) != 2)
		{
			throw new Request_Exception(440, 'No time selected!');
		}
		
		$start = Cval_Date::from_timezone(strtotime($date[0]));
		$end = Cval_Date::from_timezone(strtotime($date[1]));
		
		if ($end - $start < 60)
		{
			throw new Request_Exception(440, 'Shortest selected time can be 1 minute!');
		}
		
		list($limit_start, $limit_end) = Cval_Schedule_Timer::planning_interval();
		
		// Check if time is in limit
		if ($start < $limit_start)
		{
			throw new Request_Exception(440, 'Time in past can\'t be selected!');
		}
		if ($end > $limit_end)
		{
			throw new Request_Exception(440, 'Only time in limited future can be selected!');
		}
		
		// Check if time is in one of negotiated periods
		$in_period = FALSE;
		foreach ($this->schedule_plan->schedule_plan_negotiated_periods as $schedule_period)
		{
			if ($start < $schedule_period->start OR $end > $schedule_period->end)
			{
				continue;
			}
			$in_period = TRUE;
			break;
		}
		
		if ( ! $in_period)
		{
			throw new Request_Exception(440, 'Selected time is out of free time!');
		}
		
		$this->start = $start;
		$this->end = $end;
		$this->set_state(Model_Schedule::STATE_TIME_SELECTED, FALSE);
		$this->save();
		
		return $this;
	}
	
	/**
	 * Returns serialized data for remote calls.
	 *
	 * @return	array	serialized data
	 */
	public function serialize()
	{
		$data = $this->as_array_internal(array(
			'hash', 'start', 'end', 'label', 'description', 'place'
		));
		
		// Add user shared profile serialized data
		$data['user'] = $this->user->user_profile()->shared_profile()->serialize();
		// Add organizer serialized data
		$data['organizer'] = $this->organizer->serialize();
		
		return $data;
	}
	
	/**
	 * Checks schedule against planning interval.
	 *
	 * @param	boolean	TRUE for throwing exception if check not passed
	 * @return	boolean
	 * @throws	Kohana_Exception	If check not passed and first param is TRUE
	 */
	public function planning_interval_check($exception = FALSE)
	{
		// Check limit
		list($limit_start, $limit_end) = Cval_Schedule_Timer::planning_interval();
		
		// Check if time is in limit
		if ($this->start < $limit_start OR $this->end > $limit_end)
		{
			if ($exception)
			{
				throw new Kohana_Exception('Schedule is out of planning interval!');
			}
			return FALSE;
		}
		
		return TRUE;
	}
	
	/**
	 * Marks this schedule as read.
	 * 
	 * @param	bool	TRUE for autosaving read token
	 * @return	$this 
	 */
	public function mark_read($save = TRUE)
	{
		if ($this->is_unread())
		{
			$this->is_unread = FALSE;
			if ($save)
			{
				$this->save();
			}
		}
		return $this;
	}
	
	/**
	 * Returns URL to detail page for this schedule 
	 * 
	 * @return	string
	 */
	public function detail_url()
	{
		return Route::url_protocol('schedule_main').'&'.http_build_query(array(
			'id' => $this->id()
		));
	}

}
