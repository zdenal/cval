<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Schedule_Plan
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Schedule_Plan extends Jelly_Model
{
	/**
	 * @var	Model_Schedule	It is cached schedule object between before and
	 *						after save methods. If not used, changes in schedule
	 *						are overidden after this object database injection.
	 */
	protected $_schedule_to_save;
	
	public static function initialize(Jelly_Meta $meta)
	{
		$meta->fields(array(
			'id' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(61),
				)
			)),
			'schedule' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'start_minutes' => new Field_Integer(array(
				'default' => 480,
				'rules' => array(
					'not_empty' => NULL,
					'range' => array(0, 1439),
					'less' => array('end_minutes')
				)
			)),
			'end_minutes' => new Field_Integer(array(
				'default' => 960,
				'rules' => array(
					'not_empty' => NULL,
					'range' => array(1, 1440),
					'greater' => array('start_minutes')
				)
			)),
			'min_length' => new Field_Integer(array(
				'default' => 30,
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'min_length_units' => new Field_Enum(array(
				'default' => 'm',
				'choices' => array(
					'm' => 'minutes',
					'h' => 'hours'
				),
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'skip_organizer' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_negotiated' => new Field_Boolean(array(
				'default' => 0
			)),
			'schedule_plan_periods' => new Field_HasMany,
			'schedule_plan_negotiated_periods' => new Field_HasMany
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('id')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// Check min length according minutes
		if ($this->get_min_length() > ($this->end_minutes - $this->start_minutes)
				AND ! $this->is_all_day_range())
		{
			throw new Kohana_Exception('Time range is less than minimum schedule length!');
		}
		
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('id')->rules['not_empty'] = NULL;
		}
		
		// Create IDMPK
		$this->id = $this->_create_id($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _after_save($loaded = NULL)
	{
		// Save schedule so fields about plan validity are updated
		if ($this->_schedule_to_save)
		{
			$this->_schedule_to_save->save();
		}
		else
		{
			$this->schedule->set_state(Model_Schedule::STATE_INVALID);
		}
		
		parent::_after_save($loaded);
	}
	
	protected function _create_id($loaded)
	{
		if ($loaded AND ! empty($this->id))
		{
			return $this->id;
		}
		
		return $this->schedule->id();
	}
	
	/**
	 * Test if plan is valid, i.e. has at least one period.
	 * 
	 * @return boolean 
	 */
	public function is_valid()
	{
		return $this->loaded() AND $this->schedule_plan_periods->count();
	}
	
	/**
	 * Sets min length in specific units.
	 * 
	 * @param	float	value to set
	 * @param	string	units of value, 'h', 'm'
	 * @return	$this
	 */
	public function set_min_length($value, $units = 'm')
	{	
		switch ($units)
		{
			case 'h':
				$value *= (Date::HOUR / Date::MINUTE);
				break;
			default:
				// Default act as minutes
				$units = 'm';
		}
		
		$this->min_length = $value;
		$this->min_length_units = $units;
		
		return $this;
	}
	
	/**
	 * Gets min length in specific units. If no units specified, units are
	 * fetched from min_length_units field.
	 * 
	 * @param	string	units of value, 'h', 'm', or one of Date::MINUTE etc.
	 * @return	float
	 */
	public function get_min_length($units = NULL)
	{	
		if ($units === NULL)
		{
			$units = $this->min_length_units;
		}
		
		// Get value in seconds
		$value = $this->min_length*Date::MINUTE;
		
		switch ($units)
		{
			case 'h':
				$value /= Date::HOUR;
				break;
			case 'm':
				$value /= Date::MINUTE;
				break;
			default:
				$value /= $units;
				break;
		}
		
		return $value;
	}
	
	/**
	 * Sets start and end minutes.
	 * 
	 * @param	int
	 * @param	int
	 * @return	$this
	 */
	public function set_minutes_range($start, $end)
	{
		$this->start_minutes = $start;
		$this->end_minutes = $end;
		
		return $this;
	}
	
	/**
	 * Check if minutes range defines all day time range, 
	 * i.e. from 0:00 to 0:00.
	 * 
	 * @return	bool 
	 */
	public function is_all_day_range()
	{
		return ($this->end_minutes - $this->start_minutes) == 1440;
	}
	
	/**
	 * Sets dates in timestamps from timeplanning.
	 * 
	 * @param	array
	 * @return	$this
	 */
	public function set_timeplanning_dates(array $dates)
	{
		if ( ! count($dates))
		{
			throw new Request_Exception(440, 'At least one day must be selected!');
		}
		
		$last_key = NULL;
		$periods = array();
		foreach ($dates as $key => $date)
		{
			$date = strtotime($date);
			$timestamp = Date::reset_time($date);
			$start = Date::move($timestamp, $this->start_minutes, Date::MINUTE);
			$end = Date::move($timestamp, $this->end_minutes, Date::MINUTE);
			
			//echo Kohana::debug(Date::format($start), Date::format($end));
			
			if ($last_key !== NULL AND $periods[$last_key]['end'] == $start)
			{
				$start = $periods[$last_key]['start'];
				unset($periods[$last_key]);
			}
			
			$date = array(
				'start' => $start,
				'end' => $end
			);
			
			$periods[$key] = $date;
			//echo Schedule_Period::debug(array($date['start'], $date['end']));
			$last_key = $key;
		}
		
		//exit;
		
		foreach ($periods as $key => $period)
		{
			// Move period to server time
			$period['start'] = Cval_Date::from_timezone($period['start']);
			$period['end'] = Cval_Date::from_timezone($period['end']);
			
			$limited_period = 
				$this->_limit_plan_period(array($period['start'], $period['end']));
			
			if ( ! $limited_period)
			{
				unset($periods[$key]);
				continue;
			}
			
			list($start, $end) = $limited_period;
			
			// Update filtered period
			$periods[$key] = array(
				'start' => $start,
				'end' => $end
			);
			
			//echo Schedule_Period::debug(array($start, $end));
		}
		
		//exit;
		
		//throw Debug_Exception::create($periods);
		
		if ( ! count($periods))
		{
			throw new Request_Exception(440, 'All selected days are out of schedule limits!');
		}
		
		try
		{
			Database::instance()->transaction_start();
			
			foreach ($this->schedule_plan_periods as $plan_period)
			{
				$plan_period->delete();
			}
			
			foreach ($periods as $period)
			{
				$plan_period = Jelly::factory('schedule_plan_period')
					->set(array(
						'schedule_plan' => $this,
						'start' => $period['start'],
						'end' => $period['end']
					))->save();
			}
			
			// Save this also, so fields about plan validity are updated in schedule
			$this->save();
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 * Gets dates in timestamps for timeplanning.
	 * 
	 * @return	array
	 */
	public function get_timeplanning_dates()
	{
		$dates = array();
		
		foreach ($this->schedule_plan_periods as $schedule_period)
		{
			$start = Date::reset_time(Cval_Date::to_timezone($schedule_period->start));
			$end = Date::reset_time(Cval_Date::to_timezone($schedule_period->end) - 1);
			
			while ($start <= $end)
			{
				$dates[$start] = Date::format($start);
				// Move start for one day
				$start = Date::move($start);
			}
		}
		// Return only values with indexed keys
		return array_values($dates);
	}
	
	/**
	 * Gets periods in timestamps for timeplanning.
	 * 
	 * @return	array
	 */
	public function get_timeplanning_negotiated_periods()
	{
		$periods = array();
		
		foreach ($this->schedule_plan_negotiated_periods as $schedule_period)
		{
			$start = Cval_Date::to_timezone($schedule_period->start);
			$end = Cval_Date::to_timezone($schedule_period->end);
			
			$periods[$start] 
				= array(Date::format($start), Date::format($end));
		}
		// Return only values with indexed keys
		return array_values($periods);
	}
	
	/**
	 * Find available time in organizer and guests calendars according
	 * plan periods.
	 * 
	 * @return	$this
	 * @throws	Kohana_Exception	When schedule is in other than VALID state
	 */
	public function negotiate()
	{
		$this->schedule->is_allowed(Model_Schedule::ACTION_NEGOTIATE, TRUE);
		
		// Set schedule as negotiating
		$this->schedule = $this->schedule->set_state(Model_Schedule::STATE_NEGOTIATING);
		
		$uri = Route::get('schedule')->uri(array(
			'controller' => 'negotiate_cli'
		));
		// Call asynchronnous cli negotiate controller
		CLI::internal_call_in_background($uri, array(
			'post' => array(
				'id' => $this->schedule->id()
			)
		));
		
		return $this;
	}
	
	/**
	 * Find available time in organizer and guests calendars according
	 * plan periods.
	 * 
	 * @return	int		Number of negotiated periods
	 * @throws	Kohana_Exception	When schedule is in other than VALID state
	 */
	public function negotiate_cli()
	{
		$this->schedule->is_allowed(Model_Schedule::ACTION_NEGOTIATE_CLI, TRUE);
		
		$periods = array();
		
		try 
		{
			Database::instance()->transaction_start();
			
			$negotiation = Schedule_Negotiation::factory();
			$periods = $negotiation->start($this->schedule);
			
			// Delete old negotiated periods
			foreach ($this->schedule_plan_negotiated_periods as $plan_period)
			{
				$plan_period->delete();
			}
			
			foreach ($periods as $period)
			{
				$plan_period = Jelly::factory('schedule_plan_negotiated_period')
					->set(array(
						'schedule_plan' => $this,
						'start' => $period[0],
						'end' => $period[1]
					))->save();
			}
			
			// Set plan as negotiated
			$this->is_negotiated = $this->schedule->check_state_in(array(
				Model_Schedule::STATE_TIME_PLAN_FULL_ORGANIZER,
				Model_Schedule::STATE_TIME_PLAN_FULL_GUEST,
				Model_Schedule::STATE_TIME_NOT_SELECTED,
			));
			$this->_schedule_to_save = $this->schedule;
			// Save this object, also schedule is autosaved
			$this->save();
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			// Set plan as negotiated
			$this->is_negotiated = FALSE;
			// Move schedule state
			$this->schedule->set_state(Model_Schedule::STATE_NEGOTIATING_FAILED, FALSE);
			$this->_schedule_to_save = $this->schedule;
			// Save this object, also schedule is autosaved
			$this->save();
			
			throw $e;
		}
		
		return count($periods);
	}
	
	/**
	 * Limits plan periods according actual planning intervals.
	 * As default returns reloaded object instead of this, so 
	 * schedule_plan_periods field contains updated objects.
	 * 
	 * @param	bool	TRUE for returning reload object instead of this
	 * @return	$this
	 */
	protected function _limit_plan_periods($reload_after = TRUE)
	{
		foreach ($this->schedule_plan_periods as $period)
		{
			$limited_period = $this->_limit_plan_period($period);
			
			if ( ! $limited_period)
			{
				$period->delete();
			}
			
			list($start, $end) = $limited_period;
			
			$period->set(array(
				'start' => $start,
				'end' => $end
			))->save();
		}
		
		if ($reload_after)
		{
			return $this->reload();
		}
		
		return $this;
	}
	
	/**
	 * Limits one plan period according actual planning intervals. 
	 * It accepts Model_Schedule_Plan_Period or array($start, $end) as first 
	 * parameter.
	 * Returns array($limited_start, $limited_end) or FALSE when period is out
	 * of planning intervals or is shorter than schedule plan minimum length.
	 * 
	 * @param	mixed	Model_Schedule_Plan_Period or array($start, $end)
	 * @return	mixed	array($limited_start, $limited_end) or FALSE
	 */
	protected function _limit_plan_period($period)
	{
		if ($period instanceof Model_Schedule_Plan_Period)
		{
			$period = array($period->start, $period->end);
		}
		
		list($start, $end) = $period;
		list($limit_start, $limit_end) = Cval_Schedule_Timer::planning_interval();
		
		// Delete periods which are not in limits
		if ($start > $limit_end OR $end < $limit_start)
		{
			return FALSE;
		}

		// Easy fix to make sure timestamps are cropped on start/end limits
		$start = max($limit_start, $start);
		$end = min($limit_end, $end);
		
		// Get min length in seconds
		$min_length = $this->get_min_length(Date::SECOND);
		// And check if period greater or equal to min length
		if (($end - $start) < $min_length)
		{
			return FALSE;
		}
		
		return array($start, $end);
	}
	
	/**
	 * Returns all periods as timestamps:
	 * 
	 *	array(
	 *		array($start1, $end1),
	 *		array($start2, $end2),
	 *		...
	 *  )
	 * 
	 * @return	array 
	 */
	public function get_periods_as_timestamps()
	{
		$timestamps = array();
		
		foreach ($this->schedule_plan_periods as $period)
		{
			$timestamps[] = array($period->start, $period->end);
		}
		
		return $timestamps;
	}
	
}
