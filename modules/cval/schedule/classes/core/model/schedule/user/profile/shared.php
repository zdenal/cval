<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Schedule_User_Profile_Shared
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Schedule_User_Profile_Shared extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('label');
		
		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(40),
				)
			)),
			'schedule' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'user_profile_shared' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'has_received_schedule' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_responding' => new Field_Boolean(array(
				'default' => 1
			)),
			'is_full' => new Field_Boolean(array(
				'default' => 0
			)),
		));

		Behavior_Labelable::initialize($meta, array(
			'rules' => array(
				'not_empty' => NULL,
				'max_length' => array(255)
			)
		));
		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// Set label on profile change
		if ($this->changed('user_profile_shared'))
		{
			$this->label = $this->user_profile_shared->label;
		}
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
		}

		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _after_save($loaded = NULL)
	{
		// If guest is added to schedule set state to invalid
		if ( ! $loaded)
		{
			// Save schedule so fields about plan validity are updated
			$this->schedule->set_state(Model_Schedule::STATE_INVALID);
		}
		
		parent::_after_save($loaded);
	}
	
	protected function _after_delete()
	{
		// Save schedule so fields about plan validity are updated
		$this->schedule->set_state(Model_Schedule::STATE_INVALID);
		
		parent::_after_delete();
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		return implode('__', array($this->schedule->id(), $this->user_profile_shared->id()));
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-schedule_user_profile_shared-id-'.$this->id();
	}
	
	/**
	 * Returns friendly name, i.e. when profile is used for actual logged
	 * user, name is 'me'.
	 * 
	 * @return	string 
	 */
	public function friendly_name()
	{
		return $this->user_profile_shared->friendly_name();
	}

}
