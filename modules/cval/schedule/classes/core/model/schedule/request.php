<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Schedule_Request
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Schedule_Request extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(108),
				)
			)),
			/**
			 * Guest's user
			 */
			'user' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			/**
			 * Organizer's location 
			 */
			'location' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			/**
			 * Organizer's hash 
			 */
			'organizer_hash' => new Field_String(array(
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(32),
					'min_length' => array(32),
				)
			)),
			/**
			 * Schedule's hash 
			 */
			'hash' => new Field_String(array(
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(32),
					'min_length' => array(32),
				)
			)),
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}
	
	/**
	 * Deletes old schedule requests.
	 * 
	 * @return	void
	 * @throws	Kohana_Exception	When schedule request lifetime is not configured	
	 */
	public static function delete_old()
	{
		$meta = Jelly::meta('schedule_request');
		
		$created_at_column = $meta->fields('created_at')->column;
		$lifetime = Kohana::config('cval/schedule/timer.request.lifetime');
		
		if (empty($lifetime) OR $lifetime < 0)
		{
			throw new Kohana_Exception('Schedule request lifetime is not configured properly.');
		}
		
		// Delete older request than lifetime
		$delete = new Database_Query_Builder_Delete($meta->table());
		$delete->where($created_at_column, '<', Date::time() - $lifetime)
			->execute();
	}
	
	/**
	 * Try to load request for given schedule
	 * 
	 * @param	Model_Schedule		Requested schedule
	 * @return	Model_Schedule_Request 
	 */
	public static function load_for_schedule(Model_Schedule $schedule)
	{
		return Jelly::select('schedule_request')
			->where('user', '=', $schedule->user->id())
			->where('location', '=', $schedule->organizer->location->id())
			->where('hash', '=', $schedule->hash)
			->where('organizer_hash', '=', $schedule->organizer->hash)
			->load();
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
		}
		
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		return implode('__', array($this->user->id(), $this->location->id(), $this->organizer_hash, $this->hash));
	}
	
}
