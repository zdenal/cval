<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Schedule_Plan_Negotiated_Period
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Schedule_Plan_Negotiated_Period extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->sorting(array('start' => 'ASC'));

		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(82),
				)
			)),
			'schedule_plan' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			/**
			 * Automatically created before save.
			 */
			'id' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'start' => new Field_Datetime(array(
				'rules' => array(
					'not_empty' => NULL,
					'less' => array('end')
				)
			)),
			'end' => new Field_Datetime(array(
				'rules' => array(
					'not_empty' => NULL,
					'greater' => array('start')
				)
			)),
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
			unset($this->meta()->fields('id')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
			$this->meta()->fields('id')->rules['not_empty'] = NULL;
		}
		
		// Generate ID
		$this->id = $this->_create_id($loaded);
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_id($loaded)
	{
		if ($loaded AND ! empty($this->id))
		{
			return $this->id;
		}
		
		$last = Jelly::select($this)
				->where('schedule_plan', '=', $this->schedule_plan->id())
				->order_by('id', 'DESC')
				->load();
		
		$id = 1;
		
		if ($last->loaded())
		{
			$id = $last->id + 1;
		}
		
		return $id;
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		if (empty($this->id))
		{
			throw new Kohana_Exception('Model `:model` needs to have field `id` set', array(
				':model' => $this->meta()->model
			));
		}
		
		return implode('__', array($this->schedule_plan->id(), $this->id));
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-schedule_plan_negotiated_period-id-'.$this->id();
	}
	
}
