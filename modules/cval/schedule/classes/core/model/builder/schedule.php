<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Builder_Schedule
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Builder_Schedule extends Jelly_Builder
{
	/**
	 * Filter draft.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_draft($positive = TRUE)
	{
		return $this
			->where_open()
			->where('state', $positive ? '<' : '>=', Model_Schedule::STATE_SENT)
			->where_close();
	}
	
	/**
	 * Filter outbound.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_outbound($positive = TRUE)
	{
		$this->where_open();
		
		if ($positive)
		{
			$this->where('is_outgoing', '=', TRUE)
				->is_draft(FALSE);
		}
		else
		{
			$this->where('is_outgoing', '!=', TRUE)
				->or_where_open()
				->is_draft()
				->or_where_close();
		}
		
		return $this->where_close();
	}
	
	/**
	 * Filter inbound.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_inbound($positive = TRUE)
	{
		$this->where_open();
		
		if ($positive)
		{
			$this->where('is_outgoing', '!=', TRUE);
		}
		else
		{
			$this->where('is_outgoing', '=', TRUE);
		}
		
		return $this->where_close();
	}
	
	/**
	 * Filter favorite.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_favorite($positive = TRUE)
	{
		return $this
			->where_open()
			->where('is_favorite', $positive ? '=' : '!=', TRUE)
			->where_close();
	}
	
	/**
	 * Filter unsync.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_unsync($positive = TRUE)
	{
		return $this
			->where_open()
			->where('is_unsync', $positive ? '=' : '!=', TRUE)
			->where_close();
	}
	
	/**
	 * Filter unread.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_unread($positive = TRUE)
	{
		$this->where_open();
		
		if ($positive)
		{
			$this->where('is_unread', '=', TRUE);
		}
		else
		{
			$this->where('is_unread', '!=', TRUE);
		}
		
		return $this->where_close();
	}
	
	/**
	 * Order schedules in default order
	 * 
	 * @return	$this 
	 */
	public function use_default_order()
	{
		return $this
			->order_by('has_date', 'ASC')
			->order_by('start', 'DESC')
			->order_by('id', 'DESC');
	}
	
	/**
	 * Filters only schedules for specific user
	 *
	 * @param	Model_User $user
	 * @return	$this 
	 */
	public function filter_owner(Model_User $user)
	{
		return $this->where('user', '=', $user->id());
	}
	
	/**
	 * Filters inbound from profile.
	 *
	 * @param	Model_User_Profile_Shared $profile
	 * @return	$this 
	 */
	public function filter_organizer(Model_User_Profile_Shared $profile)
	{
		return $this->where('organizer', '=', $profile->id());
	}
	
	/**
	 * Filters outbound to profile.
	 *
	 * @param	Model_User_Profile_Shared $profile
	 * @return	$this 
	 */
	public function filter_guest(Model_User_Profile_Shared $profile)
	{
		return $this->join_guests('ssups')
				->where('ssups.user_profile_shared_id', '=', $profile->id());
	}
	
	/**
	 * Joins organizer.
	 * 
	 * @param	string	join alias for user profile shared model
	 * @return	$this
	 */
	public function join_organizer($alias = 'so')
	{
		return $this->join(array('user_profile_shared', $alias))
				->on('organizer_id', '=', $alias.'.idmpk');
	}
	
	/**
	 * Joins guests.
	 * 
	 * @param	string	join alias for schedule user profile shared model
	 * @return	$this
	 */
	public function join_guests($alias = 'ssups')
	{
		return $this->join(array('schedule_user_profile_shared', $alias))
				->on(':primary_key', '=', $alias.'.schedule_id');
	}
	
	/**
	 * Searches this model from one string.
	 * 
	 * @param	string	filter value
	 * @return	$this
	 */
	public function global_search($value)
	{
		$value = trim($value);
		if (strlen($value))
		{
			$this->distinct(TRUE)
					->join_organizer('so')
					->join_guests('ssups')
					->where_open()
					->where('label', 'LIKE', '%'.$value.'%')
					->or_where('so.label', 'LIKE', '%'.$value.'%')
					->or_where('ssups.label', 'LIKE', '%'.$value.'%')
					->where_close();
		}

		return $this;
	}
	
	/**
	 * Filter schedule which should be displayed on timetable.
	 * 
	 * @param	timestamp	start of interval
	 * @param	timestamp	end of interval
	 * @return	$this
	 */
	public function filter_cval_events($start = NULL, $end = NULL)
	{
		$this->where_open();
		
		$this->where_open();
		
		// Draft with time
		$this->where_open();
		$this->where('state', '>=', Model_Schedule::STATE_TIME_SELECTED);
		$this->is_draft();
		$this->where_close();
		
		// Unsync and not draft 
		$this->or_where_open();
		$this->is_draft(FALSE);
		$this->is_unsync();
		$this->or_where_close();
		
		$this->where_close();
		
		if ($start !== NULL)
		{
			$start = $this->meta()->fields('start')->format_for_db($start);
			$this->where('end', '>=', $start);
		}
		if ($end !== NULL)
		{
			$end = $this->meta()->fields('end')->format_for_db($end);
			$this->where('start', '<=', $end);
		}
		$this->where_close();
		
		return $this;
	}

}
