<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Schedules Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedules extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Schedules',
		));
		
		$view = View::factory('cval/content/schedules')
			->bind('schedule_groups', $schedule_groups);
		
		$schedule_groups = $this->cval->user()->schedule_groups();
		
		$js_data = array();
		
		foreach ($schedule_groups as $schedule_group)
		{
			// Set js data
			$js_data[$schedule_group->html_class(TRUE)] 
					= $schedule_group->rest();
		}
		
		$this->request->response = array(
			'html' => $view->render(),
			'active_menu' => 'schedules',
			'js' => array(
				'data' => $js_data
			),
		);
	}

}

