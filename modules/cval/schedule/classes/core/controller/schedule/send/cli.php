<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Send_CLI
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Send_CLI extends Controller_Cval_CLI 
{
	
	public function action_index() 
	{	
		$id = Arr::get($_POST, 'id', 0);
		
		$schedule = Jelly::select('schedule')
				->load($id);

		if ( ! $schedule->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($schedule->meta()->model(), $id);
		}
		
		// Check if sending is allowed
		$schedule->is_allowed(Model_Schedule::ACTION_SEND_CLI, TRUE);
		// Run command line sending
		$status = $schedule->send_cli();
		
		$message = strtr("Sending DONE! Schedule is SENT.\n", array());
		$this->request->response = $message;
	}

}

