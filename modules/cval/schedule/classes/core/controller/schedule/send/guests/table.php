<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Send_Guests_Table
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Send_Guests_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{
		$this->request->response = Request::factory(Route::get('schedule')->uri(array(
			'controller' => 'guests_table'
		)))->internal_param_set(array(
			'view_prefix' => 'cval/content/schedule/send/guests/list/table'
		))->execute()->response;
	}
	
}

