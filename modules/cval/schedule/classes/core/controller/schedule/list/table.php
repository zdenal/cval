<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_List
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_List_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var	Pagination	Pagination object. It is set after _paginate method call. 
	 */
	protected $_pagination;
	
	public function action_index() 
	{
		$view = Arr::get($_POST, 'body_only') 
				? 'cval/content/schedules/list/table/body'
				: 'cval/content/schedules/list/table';
		
		$html = View::factory($view)
				->bind('schedules', $schedules)
				->bind('count', $count)
				->bind('user_profile_shared', $user_profile_shared);
		
		$schedules = NULL;
		$user_profile_shared = $this->cval->user_profile_shared();
		
		// Get group ID
		$group_id = Arr::get($_POST, 'group', 'all');
		$group = Schedule_Group::factory($group_id);
		
		$schedules = $group->schedules($this->cval->user(), FALSE);
		
		$schedules = $this->_search($schedules);
		$schedules = $this->_paginate($schedules)
			->use_default_order()
			->execute();
		
		$count = $schedules->count();
		$js_data = array();
		
		foreach ($schedules as $schedule)
		{
			// Set js data
			$js_data[$schedule->html_class(TRUE)] 
					= Cval_REST::instance()->model($schedule)->render();
		}
		
		// Append all groups js data on demand
		if (Arr::get($_POST, 'update_groups'))
		{
			$schedule_groups = $this->cval->user()->schedule_groups();

			foreach ($schedule_groups as $schedule_group)
			{
				// Set js data
				$js_data[$schedule_group->html_class(TRUE)] 
						= $schedule_group->rest(array(
							'user' => $this->cval->user(),
							'highlighted_schedules_count' => TRUE
						));
			}
		}
		
		$this->request->response = array(
			'html' => $html->render(),
			'count' => $count,
			'group' => $group->rest(),
			'js' => array(
				'data' => $js_data
			),
			'pagination' => $this->_pagination ? $this->_pagination->render_array() : NULL
		);
	}
	
	/**
	 * Returns filtered builder.
	 * 
	 * @param	Model_Builder_Schedule	Builder to filter
	 * @return	Model_Builder_Schedule
	 */
	protected function _search(Model_Builder_Schedule $builder)
	{
		return $builder->global_search(Arr::path($_POST, 'search.global'));
	}
	
	/**
	 * Returns paginated builder.
	 * 
	 * @param	Jelly_Builder	Builder to paginate
	 * @return	Jelly_Builder
	 */
	protected function _paginate(Jelly_Builder $builder)
	{
		$this->_pagination = Pagination::factory(array(
			'total_items' => $builder->count(),
			'items_per_page' => 30,
			'current_page' => array(
				'page' => Arr::get($_POST, 'pagination', 1)
			),
			'auto_hide' => FALSE,
		));

		$builder->limit($this->_pagination->items_per_page)
			->offset($this->_pagination->offset);

		return $builder;
	}

}

