<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Create
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Create extends Controller_Schedule_Instance 
{
	protected $_not_loaded = FALSE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		if (Arr::get($_POST, 'not_loaded'))
		{
			$this->_not_loaded = TRUE;
			$this->_acl_resource = $this->_object = Jelly::factory('schedule')
				->set(array(
					'is_outgoing' => TRUE
				));
			return;
		}
		
		parent::_acl_load_resource();
	}
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_object->loaded() ? $this->_object->name() : Cval::i18n('Untitled'),
		));
		
		$view = View::factory('cval/content/schedule/create')
				->set('csrf_groups_assign', Security::token(FALSE, 'cval/schedule/groups/assign'))
				->bind('schedule', $schedule)
				->bind('schedule_groups', $schedule_groups)
				->bind('selected_schedule_groups', $selected_schedule_groups)
				->bind('assigned_schedule_groups_labels', $assigned_schedule_groups_labels);
		
		$schedule = $this->_object;
		
		$schedule_groups = $this->cval->user()->schedule_groups_choices_assignable(array('unsync'));
		
		$assigned_schedule_groups = $schedule->schedule_groups_instances();
		$assigned_schedule_groups_labels = array();
		$selected_schedule_groups = array();
		foreach ($assigned_schedule_groups as $id => $group)
		{
			$selected_schedule_groups[] = $id;
			$assigned_schedule_groups_labels[] = $group->name();
		}
		
		if ($this->_not_loaded AND Arr::get($_POST, 'group'))
		{
			$schedule_group = Schedule_Group::factory(Arr::get($_POST, 'group'));
			if ($schedule_group->assignable() AND $schedule_group->allow_draft())
			{
				$selected_schedule_groups[] = $schedule_group->id();
				$assigned_schedule_groups_labels[] = $schedule_group->name();
			}
		}
		
		$this->request->response = array(
			'html' => $view->render(),
			'active_menu' => 'schedules',
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
		);
	}
	
}

