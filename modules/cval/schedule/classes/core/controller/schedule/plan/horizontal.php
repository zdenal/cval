<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Plan_Horizontal
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Plan_Horizontal extends Controller_Schedule_Instance 
{
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/plan/horizontal')
				->set('csrf', Security::token(TRUE, 'cval/schedule/plan/horizontal'))
				->bind('schedule', $schedule);
		
		$schedule = $this->_object;
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'schedules'
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/plan/horizontal', TRUE);
		
		throw new Kohana_Exception('Not yet');
	}

}

