<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Step_Negotiate
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Step_Negotiate extends Controller_Schedule_Instance 
{
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/negotiate')
				->set('csrf', Security::token(TRUE, 'cval/schedule/step/negotiate'))
				->bind('schedule', $schedule)
				->bind('schedule_plan', $schedule_plan);
		
		$schedule = $this->_object;
		$schedule_plan = $schedule->schedule_plan;
		
		// Fetch saved dates
		$dates = $schedule->schedule_plan->get_timeplanning_dates();
		
		$this->request->response = array(
			'html' => $html->render(),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
			'schedule_plan' => Cval_REST::instance()->model($schedule_plan)->render(),
			'schedule_limits' => Cval_Schedule_Timer::planning_limits(),
			'dates' => $dates,
			'active_menu' => 'schedules'
		);
	}

}

