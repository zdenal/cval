<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Step_Time
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Step_Time extends Controller_Schedule_Instance 
{
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/time')
				->set('csrf', Security::token(TRUE, 'cval/schedule/step/time'))
				->bind('schedule', $schedule)
				->bind('schedule_plan', $schedule_plan);
		
		$schedule = $this->_object;
		$schedule_plan = $schedule->schedule_plan;
		
		// Fetch saved dates
		$periods = $schedule->schedule_plan->get_timeplanning_negotiated_periods();
		
		$this->request->response = array(
			'html' => $html->render(),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
			'schedule_plan' => Cval_REST::instance()->model($schedule_plan)->render(),
			'schedule_limits' => Cval_Schedule_Timer::planning_limits(),
			'free_periods' => $periods,
			'active_menu' => 'schedules',
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		// Check if properties are allowed
		$this->_object->is_allowed(Model_Schedule::ACTION_SELECT_TIME, TRUE);
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/step/time', TRUE);
		
		$post = Security::xss_clean($_POST);
		
		//throw Debug_Exception::create($post);
		
		Database::instance()->transaction_start();
		
		$post_selected_time = Arr::get($post, 'selected_time');
		
		$schedule = $this->_object;
		$schedule->set_timeplanning_date($post_selected_time);
		
		$schedule->save();
		
		// Reload schedule and also plan from DB
		$schedule = $this->_object = $this->_object->reload();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, 'Schedule time was set!'),
			'schedule' => Cval_REST::instance()->model($this->_object)->render(),
		);
		
		Database::instance()->transaction_commit();
	}

}

