<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Step_Plan
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Step_Plan extends Controller_Schedule_Instance
{
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/plan')
				->set('csrf', Security::token(TRUE, 'cval/schedule/step/plan'))
				->bind('schedule', $schedule)
				->bind('schedule_plan', $schedule_plan);
		
		$schedule = $this->_object;
		$schedule_plan = $schedule->schedule_plan;
		
		if ( ! $schedule_plan->loaded())
		{
			$user_profile = $this->cval->user_profile();
			$schedule_plan->min_length = $user_profile->schedule_plan_min_length;
			$schedule_plan->min_length_units = $user_profile->schedule_plan_min_length_units;
		}
		
		// Fetch saved dates
		$dates = $schedule->schedule_plan->get_timeplanning_dates();
		
		$this->request->response = array(
			'html' => $html->render(),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
			'schedule_plan' => Cval_REST::instance()->model($schedule_plan)->render(),
			'schedule_limits' => Cval_Schedule_Timer::planning_limits(),
			'dates' => $dates,
			'active_menu' => 'schedules'
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		// Check if properties are allowed
		$this->_object->is_allowed(Model_Schedule::ACTION_PLAN, TRUE);
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/step/plan', TRUE);
		
		$post = Security::xss_clean($_POST);
		
		//throw Debug_Exception::create($post);
		
		Database::instance()->transaction_start();
		
		$schedule = $this->_object;
		$schedule_plan = $schedule->schedule_plan;
		$schedule_plan->set_min_length(Arr::get($post, 'min_length'), Arr::get($post, 'min_length_units'));
		$schedule_plan->set_minutes_range(Arr::get($post, 'start_minutes'), Arr::get($post, 'end_minutes'));
		
		//throw Debug_Exception::create($schedule_plan->as_array_internal());
		
		$schedule_plan->set(array(
			'schedule' => $schedule
		));
		
		$schedule_plan->save();
		
		$post_dates = Arr::get($post, 'dates');
		if (empty($post_dates))
		{
			$post_dates = array();
		}
		$schedule_plan->set_timeplanning_dates($post_dates);
		
		// Reload schedule and also plan from DB
		$schedule = $this->_object = $this->_object->reload();
		// Fetch saved dates
		$dates = $schedule->schedule_plan->get_timeplanning_dates();
		$dates_modified = (bool) count(array_diff($post_dates, $dates));
		
		$message = $dates_modified
			? Cval::i18n('Proposed times were updated! Times in past were removed from plan.')
			: Cval::i18n('Proposed times were updated!');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message),
			'schedule' => Cval_REST::instance()->model($this->_object)->render(),
			'dates' => $dates,
			'dates_modified' => $dates_modified
		);
		
		Database::instance()->transaction_commit();
	}

}

