<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Step_Properties
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Step_Properties extends Controller_Schedule_Instance 
{
	
	protected $_not_loaded = FALSE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		if (Arr::get($_POST, 'not_loaded'))
		{
			$this->_not_loaded = TRUE;
			$this->_acl_resource = $this->_object = Jelly::factory('schedule')
				->set(array(
					'is_outgoing' => TRUE
				));
			return;
		}
		
		parent::_acl_load_resource();
	}
	
	public function before()
	{
		parent::before();
		
		if ( ! $this->_not_loaded)
		{
			// Check if properties are allowed
			$this->_object->is_allowed(Model_Schedule::ACTION_PROPERTIES, TRUE);
		}
	}
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/properties')
				->set('csrf', Security::token(TRUE, 'cval/schedule/step/properties'))
				->bind('schedule', $schedule);
		
		$schedule = $this->_object;
		
		$this->request->response = array(
			'html' => $html->render(),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
			'active_menu' => 'schedules'
		);
	}
	
	public function action_submit()
	{
		if ($this->_not_loaded)
		{
			$this->_action_submit_new();
			return;
		}
		
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		// Check if properties are allowed
		$this->_object->is_allowed(Model_Schedule::ACTION_PROPERTIES, TRUE);
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/step/properties', TRUE);
		
		$post = Security::xss_clean($_POST);
		
		Database::instance()->transaction_start();
		
		$this->_object
			->set(Arr::extract($post, array(
				'label',
				'place',
				'description',
			)));
		
		$this->_object->save();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Schedule properties were updated!')),
			'schedule' => Cval_REST::instance()->model($this->_object)->render(),
		);
		
		Database::instance()->transaction_commit();
	}
	
	protected function _action_submit_new()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/step/properties', TRUE);
		
		$post = Security::xss_clean($_POST);
		
		$schedule = $this->_object;
		$owner = $this->cval->user();
		
		$post_groups = Arr::get($post, 'groups', array());
		// Make sure post groups is array
		if (empty($post_groups))
		{
			$post_groups = array();
		}
		
		$groups = array();
		$set_favorite = FALSE;
		foreach ($post_groups as $post_group)
		{
			$group_id = Arr::get($post_group, 'id');
			if ($group_id == 'favorite')
			{
				$set_favorite = TRUE;
				continue;
			}
			
			$group = Jelly::select('schedule_group')
				->where('user', '=', $owner->id())
				->load($group_id);
			
			if ( ! $group->loaded())
			{
				continue;
			}
			
			$groups[] = $group;
		}
		
		Database::instance()->transaction_start();
		
		$schedule
			->set(Arr::extract($post, array(
				'label',
				'place',
				'description',
			)))
			->set(array(
				'schedule_groups' => $groups,
				'is_favorite' => $set_favorite,
				'user' => $owner,
				'organizer' => $this->cval->user_profile_shared(),
				'is_outgoing' => TRUE
			));
		
		$schedule->save();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Schedule properties were updated!')),
			'schedule' => Cval_REST::instance()->model($schedule)->render()
		);
		
		Database::instance()->transaction_commit();
	}

}

