<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Step_Contacts
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Step_Contacts extends Controller_Schedule_Instance 
{
	public function before()
	{
		parent::before();
		
		// Check if properties are allowed
		$this->_object->is_allowed(Model_Schedule::ACTION_CONTACTS, TRUE);
	}
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/contacts')
				->set('csrf', Security::token(TRUE, 'cval/schedule/step/contacts'))
				->bind('schedule', $schedule)
				->bind('contact_groups_choices', $contact_groups_choices)
				->bind('selected_contact_group_id', $selected_contact_group_id)
				->bind('contacts_table', $contacts_table)
				->bind('contacts_count', $contacts_count)
				->bind('guests_table', $guests_table)
				->bind('guests_count', $guests_count);
		
		$schedule = $this->_object;
		$contact_groups_choices = $this->cval->user()->contact_groups_choices();
		foreach ($contact_groups_choices as $key => $contact_group_choice)
		{
			$selected_contact_group_id = $key;
			break;
		}
		
		$js_data = array();
		
		foreach ($this->cval->user()->contact_groups() as $contact_group)
		{
			// Set js data
			$js_data[$contact_group->html_class(TRUE)] 
					= $contact_group->rest();
		}
		
		$action_contacts_table = Request::factory(Route::get('schedule')->uri(array(
			'controller' => 'contacts_search_table'
		)))->execute();
		$contacts_table = $action_contacts_table->response['html'];
		
		$_POST['schedule'] = $schedule->id();
		$action_guests_table = Request::factory(Route::get('schedule')->uri(array(
			'controller' => 'contacts_guests_table'
		)))->execute();
		$guests_table = $action_guests_table->response['html'];
		
		$this->request->response = array(
			'html' => $html->render(),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
			'active_menu' => 'schedules',
			'js' => array(
				'data' => $js_data
			),
		);
	}

}

