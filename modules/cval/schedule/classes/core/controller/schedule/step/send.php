<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Step_Send
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Step_Send extends Controller_Schedule_Instance 
{
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => $this->_page_title(),
		));
		
		$html = View::factory('cval/content/schedule/step/send')
				->set('csrf', Security::token(TRUE, 'cval/schedule/step/send'))
				->bind('schedule', $schedule)
				->bind('guests_table', $guests_table)
				->bind('guests_count', $guests_count);
		
		$schedule = $this->_object;
		
		$_POST['schedule'] = $schedule->id();
		$action_guests_table = Request::factory(Route::get('schedule')->uri(array(
			'controller' => 'guests_table'
		)))->execute();
		$guests_table = $action_guests_table->response['html'];
		
		$this->request->response = array(
			'html' => $html->render(),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
			'active_menu' => 'schedules'
		);
	}

}

