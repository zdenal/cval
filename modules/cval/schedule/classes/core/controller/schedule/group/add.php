<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Group_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Group_Add extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/schedule/group/add')
				->set('csrf', Security::token(TRUE, 'cval/schedule/group/add'))
				->bind('schedule_group', $schedule_group);
		
		$schedule_group = Jelly::factory('schedule_group');
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/group/add', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array('label')));
		
		Database::instance()->transaction_start();
		
		$schedule_group = Jelly::factory('schedule_group');
		$schedule_group->set($post_fields);
		$schedule_group->user = $this->cval->user();
		$schedule_group->save();
		
		$vertical_menu_view = View::factory('cval/content/schedules/groups/menu/vertical')
				->bind('schedule_groups', $schedule_groups)
				->set('notlive', TRUE);
		
		$schedule_groups = $this->cval->user()->schedule_groups();
		
		$js_data = array();
		
		foreach ($schedule_groups as $tmp_schedule_group)
		{
			// Set js data
			$js_data[$tmp_schedule_group->html_class(TRUE)] 
					= $tmp_schedule_group->rest();
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Schedule group was created!')),
			'schedule_group' => Cval_REST_Model::get($schedule_group),
			'vertical_menu_html' => $vertical_menu_view->render(),
			'js' => array(
				'data' => $js_data
			),
		);
		
		Database::instance()->transaction_commit();
	}

}

