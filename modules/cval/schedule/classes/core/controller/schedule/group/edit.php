<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Group_Edit
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Group_Edit extends Controller_Cval_Ajax {

	/**
	 * @var Model_Schedule_Group
	 */
	protected $_schedule_group;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$this->_schedule_group = Jelly::select('schedule_group')
				->load(Arr::get($_POST, 'id', 0));

		if ( ! $this->_schedule_group->loaded())
		{
			throw Model_Not_Found_Exception::create($this->_schedule_group->meta()->model(), Arr::get($_POST, 'id', 0));
		}

		$this->_acl_resource = $this->_schedule_group;
	}
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/schedule/group/edit')
				->set('csrf', Security::token(TRUE, 'cval/schedule/group/edit'))
				->bind('schedule_group', $schedule_group);
		
		$schedule_group = $this->_schedule_group;
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/group/edit', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array('label')));
		
		Database::instance()->transaction_start();
		
		$this->_schedule_group->set($post_fields);
		$this->_schedule_group->save();
		
		$vertical_menu_view = View::factory('cval/content/schedules/groups/menu/vertical')
				->bind('schedule_groups', $schedule_groups)
				->set('notlive', TRUE);
		
		$schedule_groups = $this->cval->user()->schedule_groups();
		
		$js_data = array();
		
		foreach ($schedule_groups as $tmp_schedule_group)
		{
			// Set js data
			$js_data[$tmp_schedule_group->html_class(TRUE)] 
					= $tmp_schedule_group->rest();
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Schedule group was updated!')),
			'schedule_group' => Cval_REST_Model::get($this->_schedule_group),
			'vertical_menu_html' => $vertical_menu_view->render(),
			'js' => array(
				'data' => $js_data
			),
		);
		
		Database::instance()->transaction_commit();
	}

}

