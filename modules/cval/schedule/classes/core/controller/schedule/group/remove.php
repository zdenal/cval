<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Group_Remove
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Group_Remove extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to delete, `id` parameter is empty');
		}
		
		$ids = $id;
		$multi_delete = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_delete = TRUE;
		}
		
		$not_found = array();
		
		Database::instance()->transaction_start();
		
		foreach ($ids as $id)
		{
			$schedule_group = Jelly::select('schedule_group')
					->where('user', '=', $this->cval->user()->id())
					->load($id);
			
			if ( ! $schedule_group->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			// Simple delete schedule object
			$schedule_group->delete();
		}
		
		$message = $multi_delete ? Cval::i18n('Schedule groups were removed')
					: Cval::i18n('Schedule group was removed');
		
		$vertical_menu_view = View::factory('cval/content/schedules/groups/menu/vertical')
				->bind('schedule_groups', $schedule_groups)
				->set('notlive', TRUE);
		
		$schedule_groups = $this->cval->user()->schedule_groups();
		
		$js_data = array();
		
		foreach ($schedule_groups as $tmp_schedule_group)
		{
			// Set js data
			$js_data[$tmp_schedule_group->html_class(TRUE)] 
					= $tmp_schedule_group->rest();
		}
		
		reset($schedule_groups);
		$active_schedule_group = current($schedule_groups);
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message),
			'active_schedule_group' => $active_schedule_group->rest(),
			'vertical_menu_html' => $vertical_menu_view->render(),
			'js' => array(
				'data' => $js_data
			),
		);
		
		Database::instance()->transaction_commit();
	}

}

