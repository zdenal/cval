<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Contacts_Search_Table
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Contacts_Search_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{
		$this->request->response = Request::factory(Route::get('contact')->uri(array(
			'controller' => 'list_table'
		)))->internal_param_set(array(
			'view_prefix' => 'cval/content/schedule/contacts/search/list/table'
		))->execute()->response;
	}
	
}

