<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Time_Check
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Time_Check extends Controller_Schedule_Instance 
{
	
	public function before()
	{
		parent::before();
		
		// Check if properties are allowed
		$this->_object->is_allowed(Model_Schedule::ACTION_TIME_CHECK, TRUE);
	}
	
	public function action_index() 
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$schedule = $this->_object;
		$schedule->time_check();
		
		$schedule = $this->_object = $schedule->reload();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, 'Cval started to check if guests are available on scheduled time.'),
			'schedule' => Cval_REST::instance()->model($schedule)->render(),
		);
	}

}

