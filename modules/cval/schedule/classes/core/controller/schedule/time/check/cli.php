<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Time_Check_CLI
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Time_Check_CLI extends Controller_Cval_CLI 
{
	
	public function action_index() 
	{	
		$id = Arr::get($_POST, 'id', 0);
		
		$schedule = Jelly::select('schedule')
				->load($id);

		if ( ! $schedule->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($schedule->meta()->model(), $id);
		}
		
		// Check if time checking is allowed
		$schedule->is_allowed(Model_Schedule::ACTION_TIME_CHECK_CLI, TRUE);
		// Run command line negotiation
		$status = $schedule->time_check_cli();
		
		$message = strtr("Time checking DONE! Time is :result.\n", array(
			':result' => $status ? 'AVAILABLE' : 'UNAVAILABLE'
		));
		$this->request->response = $message;
	}

}

