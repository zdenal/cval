<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Guest_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Guest_Add extends Controller_Cval_Ajax {

	/**
	 * @var Model_Schedule
	 */
	protected $_object;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$id = Arr::get($_POST, 'schedule', 0);
		
		$this->_object = Jelly::select('schedule')
				->where('user', '=', $this->cval->user()->id())
				->load($id);

		if ( ! $this->_object->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($this->_object->meta()->model(), $id);
		}

		$this->_acl_resource = $this->_object;
	}
	
	public function before()
	{
		parent::before();
		
		// Check if properties are allowed
		$this->_object->is_allowed(Model_Schedule::ACTION_CONTACTS, TRUE);
	}
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to add, `id` parameter is empty');
		}
		
		$schedule = $this->_object;
		$ids = $id;
		$multi_add = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_add = TRUE;
		}
		
		$not_found = array();
		
		Database::instance()->transaction_start();
		
		foreach ($ids as $id)
		{
			$contact = Jelly::select('contact')
					->where('user', '=', $this->cval->user()->id())
					->load($id);
			
			if ( ! $contact->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			$schedule->add_contact($contact);
		}
		
		// Reload schedule and also plan from DB
		$schedule = $this->_object = $this->_object->reload();
		
		$message = $multi_add ? Cval::i18n('Contacts were added to guests')
					: Cval::i18n('Contact was added to guests');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
		
		Database::instance()->transaction_commit();
	}
	
	public function action_group()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to add, `id` parameter is empty');
		}
		
		// Get contact group
		$contact_group = Contact_Group::factory($id);
		$schedule = $this->_object;
		
		Database::instance()->transaction_start();
		
		foreach ($contact_group->contacts($this->cval->user()) as $contact)
		{
			$schedule->add_contact($contact);
		}
		
		// Reload schedule and also plan from DB
		$schedule = $this->_object = $this->_object->reload();
		
		$message = Cval::i18n('All contacts from group were added to guests');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
		
		Database::instance()->transaction_commit();
	}

}

