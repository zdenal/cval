<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Download
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Download extends Controller_Cval_Secure
{
	/**
	 * @var Model_Schedule
	 */
	protected $_object;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * @var array   Actions requiring ACL checking
	 */
	protected $_acl_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$id = $this->request->param('id');
		
		$this->_object = Jelly::select('schedule')
				->where('deleted', '!=', 1)
				->where('user', '=', $this->cval->user()->id())
				->load($id);

		if ( ! $this->_object->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($this->_object->meta()->model(), $id);
		}

		$this->_acl_resource = $this->_object;
	}
	
	public function action_index()
	{
		Schedule_Download::format($this->request->param('format'), $this->_object);
	}
	
}

