<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Negotiate_Request
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Negotiate_Request extends Controller_Location_Request 
{
	
	public function before()
	{
		parent::before();
		
		$this->_error_codes(array(
			2	=> 'Source and organizer location does not match',
			3	=> 'Guest not exists',
			4	=> 'Guest is deleted or blocked',
			5	=> 'Guest plan exception'
		));
	}
	
	public function action_index() 
	{
		try
		{
			$request = Schedule_Negotiation_Request::factory()->unserialize($this->data());
		}
		catch (Exception $e)
		{
			$throw = FALSE;
			if ($e instanceof Model_Not_Found_Exception)
			{
				if ($e->get_model_name() == 'Model_User_Shared_Profile')
				{
					$this->response(3);
				}
				else
				{
					$throw = TRUE;
				}
			}
			if ($throw)
			{
				throw $e;
			}
			Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			return;
		}
		
		if ( ! $request->get_guest()->user()->enabled(TRUE))
		{
			return $this->response(4);
		}
		
		if ( ! $request->get_organizer()->location()->compare($this->_location))
		{
			return $this->response(2);
		}
		
		try 
		{
			$response = Schedule_Negotiation_Response::factory()->create($request);
		}
		catch (Exception $e)
		{
			Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			return $this->response(5);
		}
		
		$this->response($response->serialize());
	}

}

