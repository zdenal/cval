<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Negotiate_CLI
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Negotiate_CLI extends Controller_Cval_CLI 
{
	
	public function action_index() 
	{	
		$id = Arr::get($_POST, 'id', 0);
		
		$schedule = Jelly::select('schedule')
				->load($id);

		if ( ! $schedule->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($schedule->meta()->model(), $id);
		}
		
		// Check if negotiating is allowed
		$schedule->is_allowed(Model_Schedule::ACTION_NEGOTIATE_CLI, TRUE);
		// Run command line negotiation
		$periods_count = $schedule->schedule_plan->negotiate_cli();
		
		$message = strtr("Negotiation DONE! Found :number free periods.\n", array(
			':number' => $periods_count
		));
		$this->request->response = $message;
	}

}

