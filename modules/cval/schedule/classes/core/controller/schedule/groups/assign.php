<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Groups_Assign
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Groups_Assign extends Controller_Schedule_Instance {

	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/schedule/groups/assign', TRUE);
		
		$post = Security::xss_clean($_POST);
		$post_groups = Arr::get($post, 'groups', array());
		// Make sure post groups is array
		if (empty($post_groups))
		{
			$post_groups = array();
		}
		
		$owner = $this->cval->user();
		$groups = array();
		$set_favorite = FALSE;
		$set_unsync = FALSE;
		foreach ($post_groups as $post_group)
		{
			$group_id = Arr::get($post_group, 'id');
			if ($group_id == 'favorite')
			{
				$set_favorite = TRUE;
				continue;
			}
			elseif ($group_id == 'unsync')
			{
				$set_unsync = TRUE;
				continue;
			}
			
			$group = Jelly::select('schedule_group')
				->where('user', '=', $owner->id())
				->load($group_id);
			
			if ( ! $group->loaded())
			{
				continue;
			}
			
			$groups[] = $group;
		}
		
		Database::instance()->transaction_start();
		
		$this->_object
			->set(array(
				'schedule_groups' => $groups,
				'is_favorite' => $set_favorite,
				'is_unsync' => $set_unsync
			))
			->save();
		
		$selected_groups = Schedule_Group::rest_list($this->_object->schedule_groups_instances());
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Schedule groups were assigned')),
			'schedule' => Cval_REST::instance()->model($this->_object)->render(),
			'assigned_groups' => $selected_groups
		);
		
		Database::instance()->transaction_commit();
	}

}

