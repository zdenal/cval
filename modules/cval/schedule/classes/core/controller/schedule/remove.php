<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Remove
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Remove extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to delete, `id` parameter is empty');
		}
		
		$group_id = Security::xss_clean(Arr::get($_POST, 'group_id'));
		
		//throw Debug_Exception::create($id, $group_id);
		
		$remove_favorite = FALSE;
		$group = NULL;
		if ($group_id)
		{
			if ($group_id == 'favorite')
			{
				$remove_favorite = TRUE;
			}
			else
			{
				$group = Jelly::select('schedule_group')
					->where('user', '=', $this->cval->user()->id())
					->load($group_id);

				if ( ! $group->loaded())
				{
					throw Model_Not_Found_Exception::create($group->meta()->model(), $group_id);
				}
			}
		}
		
		$ids = $id;
		$multi_delete = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_delete = TRUE;
		}
		
		$not_found = array();
		
		Database::instance()->transaction_start();
		
		foreach ($ids as $id)
		{
			$schedule = Jelly::select('schedule')
					->where('user', '=', $this->cval->user()->id())
					->load($id);
			
			if ( ! $schedule->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			if ($group)
			{
				// Remove group from schedule
				$schedule->remove('schedule_groups', $group);
				$schedule->save();
			}
			elseif ($remove_favorite)
			{
				// Set not favorite
				$schedule->is_favorite = FALSE;
				$schedule->save();
			}
			else
			{
				// Or simple delete schedule object
				$schedule->delete();
			}
		}
		
		$message = $multi_delete ? Cval::i18n('Schedules were removed')
					: Cval::i18n('Schedule was removed');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
		
		Database::instance()->transaction_commit();
	}

}

