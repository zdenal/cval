<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule_Guests_Table
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule_Guests_Table extends Controller_Cval_Ajax {
	
	/**
	 * @var	Pagination	Pagination object. It is set after _paginate method call. 
	 */
	protected $_pagination;
	
	/**
	 * @var Model_Schedule
	 */
	protected $_object;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$id = Arr::get($_POST, 'schedule', 0);
		
		$this->_object = Jelly::select('schedule')
				->where('user', '=', $this->cval->user()->id())
				->load($id);

		if ( ! $this->_object->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($this->_object->meta()->model(), $id);
		}

		$this->_acl_resource = $this->_object;
	}
	
	public function action_index() 
	{
		$view_prefix = $this->request->internal_param('view_prefix', 'cval/content/schedule/guests/list/table');
		$view = Arr::get($_POST, 'body_only') 
				? $view_prefix.'/body'
				: $view_prefix;
		
		$html = View::factory($view)
				->bind('guests', $guests)
				->bind('count', $count)
				->bind('schedule', $schedule);
		
		$schedule = $this->_object;
		$guests = $schedule->get('schedule_user_profiles_shared');
		
		$guests = $this->_paginate($guests)
			->order_by('label')
			->execute();
		
		$count = $guests->count();
		$js_data = array();
		
		foreach ($guests as $guest)
		{
			// Set js data
			$js_data[$guest->html_class(TRUE)] 
					= Cval_REST::instance()->model($guest)->render();
		}
		
		$this->request->response = array(
			'html' => $html->render(),
			'count' => $count,
			'js' => array(
				'data' => $js_data
			),
			'pagination' => $this->_pagination ? $this->_pagination->render_array() : NULL,
			'schedule' => Cval_REST::instance()->model($this->_object)->render(),
			'additional' => $this->_count_symptoms()
		);
	}
	
	/**
	 * Returns paginated builder.
	 * 
	 * @param	Jelly_Builder	Builder to paginate
	 * @return	Jelly_Builder
	 */
	protected function _paginate(Jelly_Builder $builder)
	{
		$this->_pagination = Pagination::factory(array(
			'total_items' => $builder->count(),
			'items_per_page' => 30,
			'current_page' => array(
				'page' => Arr::get($_POST, 'pagination', 1)
			),
			'auto_hide' => FALSE,
		));

		$builder->limit($this->_pagination->items_per_page)
			->offset($this->_pagination->offset);

		return $builder;
	}
	
	protected function _count_symptoms()
	{
		$symptoms = array();
		
		// Number of not responding
		$symptoms['not_responding_count'] = $this->_object->get('schedule_user_profiles_shared')
				->where('is_responding', '=', FALSE)
				->count();
		
		// Number of full
		$symptoms['full_count'] = $this->_object->get('schedule_user_profiles_shared')
				->where('is_full', '=', TRUE)
				->count();
		
		// Number of not received schedule
		$symptoms['not_received_schedule_count'] = $this->_object->get('schedule_user_profiles_shared')
				->where('has_received_schedule', '=', FALSE)
				->count();
		
		return $symptoms;
	}

}

