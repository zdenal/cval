<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Schedule
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Schedule extends Controller_Schedule_Instance {
	
	public function action_index() 
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		View::set_global(array(
			'page_title' => $this->_object->name(),
		));
		
		$view = View::factory('cval/content/schedule')
				->set('csrf_groups_assign', Security::token(FALSE, 'cval/schedule/groups/assign'))
				->bind('schedule', $schedule)
				->bind('schedule_groups', $schedule_groups)
				->bind('selected_schedule_groups', $selected_schedule_groups)
				->bind('assigned_schedule_groups_labels', $assigned_schedule_groups_labels)
				->bind('guests_table', $guests_table)
				->bind('guests_count', $guests_count);
		
		Database::instance()->transaction_start();
		
		$schedule = $this->_object;
		// Mark schedule as read on first show
		$schedule->mark_read();
		
		$schedule_groups = $this->cval->user()->schedule_groups_choices_assignable();
		
		$assigned_schedule_groups = $this->_object->schedule_groups_instances();
		$assigned_schedule_groups_labels = array();
		$selected_schedule_groups = array();
		foreach ($assigned_schedule_groups as $id => $group)
		{
			$selected_schedule_groups[] = $id;
			$assigned_schedule_groups_labels[] = $group->name();
		}
		
		$_POST['schedule'] = $schedule->id();
		$action_guests_table = Request::factory(Route::get('schedule')->uri(array(
			'controller' => 'guests_table'
		)))->execute();
		$guests_table = $action_guests_table->response['html'];
		
		$this->request->response = array(
			'html' => $view->render(),
			'active_menu' => 'schedules',
			'refresh_interval' => $this->_refresh_interval(),
			'schedule' => Cval_REST::instance()->model($schedule)->render()
		);
		
		Database::instance()->transaction_commit();
	}
	
	public function action_download()
	{
		// Get format
		$format = Arr::get($_POST, 'format');
		if (empty($format))
		{
			throw new Kohana_Exception('Format argument is missing in POST');
		}
		
		Schedule_Download::format(Arr::get($_POST, 'format'), $this->_object);
	}
	
	protected function _refresh_interval()
	{
		$refresh_interval = Cval_Calendar_Timer::refresh_interval();
		$refresh_interval[0] = Date::format(Cval_Date::to_timezone($refresh_interval[0]));
		$refresh_interval[1] = Date::format(Cval_Date::to_timezone($refresh_interval[1]));
		
		return $refresh_interval;
	}

}

