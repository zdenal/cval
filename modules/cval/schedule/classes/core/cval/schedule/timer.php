<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Cval Schedule Timer
 * @package Cval
 * @author	zdennal
 */
class Core_Cval_Schedule_Timer
{
	/**
	 * @var	array	Allowed planning interval used in self::planning_interval()
	 */
	protected static $_planning_interval;
	
	/**
	 * @var	array	Planning limits used in self::planning_limits()
	 */
	protected static $_planning_limits;
	
	/**
	 * Gets actual planning interval for updating calendars
	 * 
	 * @return	array	Array of start and end interval in timestamp format
	 */
	public static function planning_interval()
	{
		if (Cval_Schedule_Timer::$_planning_interval === NULL)
		{
			$limits = Cval_Schedule_Timer::planning_limits();
			// Get actual time
			$time = Date::time();
			// Get start and end point with moving limits from now
			$start = Date::move($time, $limits[0], 'second');
			$end = Date::move($time, $limits[1], 'second');
			
			Cval_Schedule_Timer::$_planning_interval = array($start, $end);
		}
		
		return Cval_Schedule_Timer::$_planning_interval;
	}
	
	/**
	 * Gets actual planning limits from 'now' for new schedules
	 * 
	 * @return	array	Array of start and end limits in seconds
	 */
	public static function planning_limits()
	{
		if (Cval_Schedule_Timer::$_planning_limits === NULL)
		{
			$config = Kohana::config('cval/schedule/timer.limit');
			
			// Get start
			$start = Arr::get($config, 'start');
			// Check start
			if (empty($start) OR ! is_array($start))
			{
				throw new Kohana_Exception('Limit config start parameter not valid');
			}
			
			// Get end
			$end = Arr::get($config, 'end');
			// Check end
			if ($end !== TRUE AND (empty($end) OR ! is_array($end)))
			{
				throw new Kohana_Exception('Limit config end parameter not valid');
			}
			
			// Get actual time
			$time = Date::time();
			// Add time to start config
			array_unshift($start, $time);
			// Get difference for start limit from now in seconds
			$limit_start = call_user_func_array('Date::move', $start) - $time;
			
			// If end is true
			if ($end === TRUE)
			{
				// Than we use calendar refresh interval length
				$limit_end = Cval_Calendar_Timer::refresh_length();
			}
			else
			{
				// Add time to end config
				array_unshift($end, $time);
				// Get difference for end limit from now in seconds
				$limit_end = call_user_func_array('Date::move', $end) - $time;
			}
			
			Cval_Schedule_Timer::$_planning_limits = array($limit_start, $limit_end);
		}
		
		return Cval_Schedule_Timer::$_planning_limits;
	}
	
}