<?php defined('SYSPATH') or die('No direct script access.');

class Field_Schedule_Update_Hash extends Field_String
{
	
	/**
	 * Called just before saving if the field is $in_db, and just after if it's not.
	 *
	 * If $in_db, it is expected to return a value suitable for insertion
	 * into the database.
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @return  mixed
	 */
	public function save($model, $value, $loaded)
	{
		// Set it only when we are creating record or when updating and record was changed
		// and value of this field value is not changed
		if ( ! $model->changed($this->name) AND ( ! $loaded OR $model->is_user_changed()))
		{
			$value = $model->generate_update_hash();
		}

		return parent::save($model, $value, $loaded);
	}
}