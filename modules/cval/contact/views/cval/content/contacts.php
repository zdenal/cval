<div class="container_16">	
	
	<div class="grid_3 Cval-page-label">
		<?php echo Cval::i18n($page_title) ?>
	</div>
	<div class="grid_13 Cval-menu-id-contacts" style="text-align: right">
		<div class="Cval-helper-float-left Cval-contact-list-controls ui-helper-hidden">
			<span class="Cval-selectall"></span>
			<span class="Cval-paginator"></span>
		</div>
		<div class="Cval-helper-float-right">
			<!--<span class="Cval-schedule-add"><?php echo Cval::i18n('Create a schedule from selected contacts') ?></span>-->
			<span class="Cval-contact-controls">
				<span class="Cval-contact-add"><?php echo Cval::i18n('Add contact') ?></span>
				<span class="Cval-contact-remove"><?php echo Cval::i18n('Remove contact') ?></span>
			</span>
		</div>
		<div class="grid_4 Cval-helper-float-right">
			<?php echo View::factory('cval/util/search/input'); ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="grid_3">
		<?php echo View::factory('cval/content/contacts/groups/menu/vertical')->include_view($thiss) ?>
		<div class="Cval-helper-margin-top-50 Cval-contact_group-controls">
			<span class="ui-icon ui-icon-plus Cval-helper-float-left Cval-trigger-create" title="<?php echo Cval::i18n('Create a contact group') ?>"></span>
			<span class="ui-icon ui-icon-trash Cval-helper-float-left Cval-trigger-remove" title="<?php echo Cval::i18n('Delete the contact group') ?>"></span>
			<span class="ui-icon ui-icon-pencil Cval-helper-float-left Cval-trigger-edit" title="<?php echo Cval::i18n('Rename the contact group') ?>"></span>
		</div>
	</div>
	
	<div class="Cval-page-id-contacts-content grid_13"></div>
	
	<div class="clear"></div>
</div>