<div class="jWizard">
	<div class="Cval-dialog-contact-add-search">
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live')) ?>
		<?php echo Form::hidden('csrf', $csrf) ?>
		<div class="grid_10">
			<div class="field-label grid_16">
				<?php echo Cval::i18n('Enter the name or email address of the user you\'re looking for:') ?>
			</div>
			<div class="field grid_12">
				<?php echo Form::input('name') ?>
			</div>
			<div class="grid_4">
				<span class="Cval-button Cval-button-live Cval-contact-search-trigger"><?php echo Cval::i18n('Find') ?></span>
			</div>
			<div class="clear"></div>
			<div class="grid_16">
				<!--<span class="field-label">
					<?php echo Cval::i18n('Results') ?>
				</span>-->
				<span class="Cval-selectall"></span>
				<span class="Cval-paginator"></span>
				<div>
					<?php echo $table ?>
					<div class="Cval-contact-search-noresult Cval-helper-margin-top ui-helper-hidden"><?php echo Cval::i18n('No users found, try to modify your search phrase.') ?></div>
				</div>
			</div>
		</div>
		<div class="grid_6">
			<div class="grid_16">
				<div class="field-label">
					<?php echo Cval::i18n('From Location') ?>
				</div>
				<?php echo Form::select('location', $locations, $location, array(
					'class' => 'Cval-contact-add-locations-select'
				)) ?>
			</div>
			<div class="clear"></div>
			<div class="grid_16 Cval-helper-margin-top">
				<div class="field-label">
					<?php echo Cval::i18n('Target contact groups') ?>
				</div>
				<?php echo Form::select('target_contact_groups', $contact_groups, $target_contact_group, array(
					'class' => 'Cval-contact-add-target-groups-select'
				)) ?>
			</div>
		</div>
		<div class="clear"></div>
		
		<?php echo Form::close(); ?>
	</div>
</div>