<?php foreach ($users as $user): ?>
<tr class="<?php echo $user->html_class() ?>">
	<td class="Cval-column-listcheck"><div><input type="checkbox" class="Cval-list-checkbox"<?php echo ($owner_profile_shared->id() == $user->id()) ? ' disabled="disabled"' : '' ?>/></div></td>
	<td class="Cval-column-label">
		<div><?php echo $user->label ?></div>
	</td>
	<td class="Cval-column-actions">
		<div>
			<!--<span class="ui-icon ui-icon-info Cval-helper-float-right Cval-trigger-info" title="<?php echo Cval::i18n('Info') ?>"></span>-->
		</div>
	</td>
</tr>
<?php endforeach; ?>