<?php foreach ($contacts as $contact): ?>
<tr class="<?php echo $contact->html_class() ?>">
	<td class="Cval-column-listcheck"><input type="checkbox" class="Cval-list-checkbox"/></td>
	<td class="Cval-column-label">
		<div><?php echo $contact->name() ?></div>
	</td>
	<td class="Cval-column-location">
		<div><?php echo $contact->user_profile_shared->location->name() ?></div>
	</td>
	<td class="Cval-column-incoming">
		<div><?php echo $contact->count_schedules() ?></div>
	</td>
	<td class="Cval-column-outgoing">
		<div><?php echo $contact->count_schedules(FALSE) ?></div>
	</td>
	<td class="Cval-column-actions">
		<span class="ui-icon ui-icon-trash Cval-helper-float-right Cval-trigger-remove" title="<?php echo Cval::i18n('Remove') ?>"></span>
		<?php if ($contact->user_profile_shared->blocked): ?>
		<span class="ui-icon ui-icon-notice ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('User seems to be blocked') ?>"></span>
		<?php elseif ($contact->user_profile_shared->deleted): ?>
		<span class="ui-icon ui-icon-notice ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('User seems to be deleted') ?>"></span>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; ?>