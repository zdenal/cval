<div class="container_16 full">
	
	<!--<div class="grid_16 full">
		<div class="Cval-helper-float-right">
			<span class="Cval-paginator"></span>
			<span class="Cval-button Cval-button-live Cval-contact-actions"><?php echo Cval::i18n('Actions') ?></span>
		</div>
	</div>
	<div class="clear"></div>-->
	
	<div class="Cval-list-contacts-message-positions-content ui-helper-hidden">
		<?php echo View::factory('cval/content/contacts/list/message/positions')->include_view($thiss) ?>
	</div>
	<div class="Cval-list-contacts-message-nocontact-content ui-helper-hidden">
		<?php echo View::factory('cval/content/contacts/list/message/nocontact')->include_view($thiss) ?>
	</div>
	
	<div class="grid_16 full Cval-list-contacts-info-box Cval-helper-margin-top-50"></div>
	
	<div class="grid_16 full Cval-list-wrapper Cval-list-contacts-wrapper">
		<?php echo $table ?>
	</div>
		
	<div class="clear"></div>
</div>