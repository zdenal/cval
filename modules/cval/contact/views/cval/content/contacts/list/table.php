<table cellspacing="0" cellpadding="0" class="Cval-list Cval-list-contacts">
	<thead>
		<tr>
			<th class="Cval-column-listcheck"><div></div></th>
			<th class="Cval-column-label"><div><?php echo Cval::i18n('Name') ?></div></th>
			<th class="Cval-column-location"><div><?php echo Cval::i18n('Location') ?></div></th>
			<th class="Cval-column-incoming" title="<?php echo Cval::i18n('Number of Inbound schedules') ?>">
				<div><?php echo Cval::i18n('In') ?></div>
			</th>
			<th class="Cval-column-outgoing" title="<?php echo Cval::i18n('Number of Outbound schedules') ?>">
				<div><?php echo Cval::i18n('Out') ?></div>
			</th>
			<th class="Cval-column-actions"><div></div></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>