<ul class="Cval-menu-vertical Cval-menu-vertical-id-contacts<?php echo ! empty($notlive) ? '' : ' Cval-menu-vertical-live'?>">
	<?php foreach ($contact_groups as $id => $contact_group): ?>
	<li class="Cval-menu-item-contacts_group_<?php echo $id ?> <?php echo $contact_group->html_class() ?>"><a href="<?php echo Route::url_protocol('contacts', array(
			'controller' => 'list'
		)).'&'.http_build_query(array(
			'group' => $id
		)) ?>" class="Cval-link-page"><?php echo $contact_group->name() ?></a></li>
	<?php endforeach; ?>
</ul>