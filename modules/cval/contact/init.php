<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('contacts_main', 'contacts')
        ->defaults(array(
            'controller' => 'contacts',
        ))->config_set('route_dash', 'page');

Route::set('contacts', 'contacts/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'contacts',
        ))->config_set('route_dash', 'page');

Route::set('contact', 'contact/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'contact',
        ));

Route::set('contact_group', 'contact_group/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'contact/group',
        ));

