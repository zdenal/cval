<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Contacts Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contacts extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Contacts',
		));
		
		$view = View::factory('cval/content/contacts')
			->bind('contact_groups', $contact_groups);
		
		$contact_groups = $this->cval->user()->contact_groups();
		
		$js_data = array();
		
		foreach ($contact_groups as $contact_group)
		{
			// Set js data
			$js_data[$contact_group->html_class(TRUE)] 
					= $contact_group->rest();
		}
		
		$this->request->response = array(
			'html' => $view->render(),
			'active_menu' => 'contacts',
			'js' => array(
				'data' => $js_data
			),
		);
	}

}

