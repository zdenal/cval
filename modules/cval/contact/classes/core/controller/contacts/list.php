<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Calendars List Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contacts_List extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/contacts/list/content')
				->bind('table', $table)
				->bind('contacts_count', $contacts_count);
		
		$action_table = Request::factory(Route::get('contact')->uri(array(
			'controller' => 'list_table'
		)))->execute();
		
		$table = $action_table->response['html'];
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'contacts'
		);
	}

}

