<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_Add_Search_Results_Table
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_Add_Search_Results_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var	Pagination	Pagination object. It is set after _paginate method call. 
	 */
	protected $_pagination;
	
	public function action_index() 
	{
		$users = Jelly::select('user')
				->where('id', '=', 0)
				->execute();
		
		$this->_render($users);
	}
	
	public function action_search()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array('value', 'location')));
		
		$validate = Validate::factory($post_fields)
			->filter(TRUE, 'trim')
			->label('value', Cval::i18n('Search phrase'))
			->rules('location', array(
				'not_empty' => NULL,
			))
			->label('location', Cval::i18n('Location'));
		
		$validate->check(FALSE, TRUE);

		$owner = Cval::instance()->user();
		$location = Model_Location::load($validate->offsetGet('location'));
		
		$data = $location->find_users(array(
			'value' => $validate->offsetGet('value')
		), array(
			'items_per_page' => 30,
			'current_page' => Arr::get($_POST, 'pagination', 1)
		));
		
		$this->_paginate($data['pagination']);
		
		$this->_render($data['collection']);
	}
	
	/**
	 * Init pagination object from pagination data.
	 * 
	 * @param	array	Data needed for pagination
	 * @return	$this
	 */
	protected function _paginate(array $pagination_data = array())
	{
		$this->_pagination = Pagination::factory(array(
			'total_items' => Arr::get($pagination_data, 'total_items'),
			'items_per_page' => Arr::get($pagination_data, 'items_per_page'),
			'current_page' => array(
				'page' => Arr::get($pagination_data, 'current_page', 1)
			),
			'auto_hide' => FALSE,
		));
		
		return $this;
	}
	
	/**
	 * Inserts user as Jelly Collection to view object and fill request's
	 * response.
	 * 
	 * @param	mixed	Jelly_Collection of local users or array of remote users 
	 */
	protected function _render($users)
	{
		$remote = ! ($users instanceof Jelly_Collection);

		$view = Arr::get($_POST, 'body_only') 
				? 'cval/content/contact/add/search/results/table/body'.($remote ? '/remote' : '')
				: 'cval/content/contact/add/search/results/table';
		
		$html = View::factory($view)
				->bind('users', $users)
				->bind('owner_profile_shared', $owner_profile_shared)
				->bind('count', $count);
		
		$owner_profile_shared = Cval::instance()->user_profile_shared();
		$count = count($users);
		$js_data = array();
		
		foreach ($users as $key => $user)
		{
			// Set js data
			if ($remote)
			{
				$html_class = 'Cval-user-profile-shared-remote-'.$user['hash'];
				$users[$key]['html_class'] = $html_class;
				
				$js_data['.'.$html_class] = array(
					'hash' => $user['hash']
				);
			}
			else
			{
				$js_data[$user->html_class(TRUE)] = array(
					'hash' => $user->hash
				);
			}
		}
		
		$this->request->response = array(
			'html' => $html->render(),
			'count' => $count,
			'js' => array(
				'data' => $js_data
			),
			'pagination' => $this->_pagination ? $this->_pagination->render_array() : NULL
		);
	}

}

