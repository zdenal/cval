<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_List
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_List_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var	Pagination	Pagination object. It is set after _paginate method call. 
	 */
	protected $_pagination;
	
	public function action_index() 
	{
		$view_prefix = $this->request->internal_param('view_prefix', 'cval/content/contacts/list/table');
		$view = Arr::get($_POST, 'body_only') 
				? $view_prefix.'/body'
				: $view_prefix;
		
		$html = View::factory($view)
				->bind('contacts', $contacts)
				->bind('count', $count);
		
		$contacts = NULL;
		
		// Get group ID
		$group_id = Arr::get($_POST, 'group', 'all');
		$group = Contact_Group::factory($group_id);
		
		$contacts = $group->contacts($this->cval->user(), FALSE);
		
		$contacts = $this->_search($contacts);
		$contacts = $this->_paginate($contacts)
			->use_default_order()
			->execute();
		
		$count = $contacts->count();
		$js_data = array();
		
		foreach ($contacts as $contact)
		{
			// Set js data
			$js_data[$contact->html_class(TRUE)] 
					= Cval_REST::instance()->model($contact)->render();
		}
		
		$this->request->response = array(
			'html' => $html->render(),
			'count' => $count,
			'js' => array(
				'data' => $js_data
			),
			'pagination' => $this->_pagination ? $this->_pagination->render_array() : NULL
		);
	}
	
	/**
	 * Returns filtered builder.
	 * 
	 * @param	Model_Builder_Contact	Builder to filter
	 * @return	Model_Builder_Contact
	 */
	protected function _search(Model_Builder_Contact $builder)
	{
		return $builder->global_search(Arr::path($_POST, 'search.global'));
	}
	
	/**
	 * Returns paginated builder.
	 * 
	 * @param	Model_Builder_Contact	Builder to paginate
	 * @return	Model_Builder_Contact
	 */
	protected function _paginate(Model_Builder_Contact $builder)
	{
		$this->_pagination = Pagination::factory(array(
			'total_items' => $builder->count(),
			'items_per_page' => 30,
			'current_page' => array(
				'page' => Arr::get($_POST, 'pagination', 1)
			),
			'auto_hide' => FALSE,
		));

		$builder->limit($this->_pagination->items_per_page)
			->offset($this->_pagination->offset);

		return $builder;
	}

}

