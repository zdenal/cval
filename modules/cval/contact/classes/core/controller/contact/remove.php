<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_Remove
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_Remove extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to delete, `id` parameter is empty');
		}
		
		$group_id = Security::xss_clean(Arr::get($_POST, 'group_id'));
		
		//throw Debug_Exception::create($id, $group_id);
		
		$remove_favorite = FALSE;
		$group = NULL;
		if ($group_id)
		{
			if ($group_id == 'favorite')
			{
				$remove_favorite = TRUE;
			}
			else
			{
				$group = Jelly::select('contact_group')
					->where('user', '=', $this->cval->user()->id())
					->load($group_id);

				if ( ! $group->loaded())
				{
					throw Model_Not_Found_Exception::create($group->meta()->model(), $group_id);
				}
			}
		}
		
		$ids = $id;
		$multi_delete = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_delete = TRUE;
		}
		
		$not_found = array();
		
		Database::instance()->transaction_start();
		
		foreach ($ids as $id)
		{
			$contact = Jelly::select('contact')
					->where('user', '=', $this->cval->user()->id())
					->load($id);
			
			if ( ! $contact->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			if ($group)
			{
				// Remove group from contact
				$contact->remove('contact_groups', $group);
				$contact->save();
			}
			elseif ($remove_favorite)
			{
				// Set not favorite
				$contact->is_favorite = FALSE;
				$contact->save();
			}
			else
			{
				// Or simple delete contact object
				$contact->delete();
			}
		}
		
		$message = $multi_delete ? Cval::i18n('Contacts were removed')
					: Cval::i18n('Contact was removed');
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
		
		Database::instance()->transaction_commit();
	}

}

