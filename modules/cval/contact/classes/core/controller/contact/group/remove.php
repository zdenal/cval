<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_Group_Remove
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_Group_Remove extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to delete, `id` parameter is empty');
		}
		
		$ids = $id;
		$multi_delete = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_delete = TRUE;
		}
		
		$not_found = array();
		
		Database::instance()->transaction_start();
		
		foreach ($ids as $id)
		{
			$contact_group = Jelly::select('contact_group')
					->where('user', '=', $this->cval->user()->id())
					->load($id);
			
			if ( ! $contact_group->loaded())
			{
				$not_found[] = $id;
				continue;
			}
			
			// Simple delete contact object
			$contact_group->delete();
		}
		
		$message = $multi_delete ? Cval::i18n('Contact groups were removed')
					: Cval::i18n('Contact group was removed');
		
		$vertical_menu_view = View::factory('cval/content/contacts/groups/menu/vertical')
				->bind('contact_groups', $contact_groups)
				->set('notlive', TRUE);
		
		$contact_groups = $this->cval->user()->contact_groups();
		
		$js_data = array();
		
		foreach ($contact_groups as $tmp_contact_group)
		{
			// Set js data
			$js_data[$tmp_contact_group->html_class(TRUE)] 
					= $tmp_contact_group->rest();
		}
		
		reset($contact_groups);
		$active_contact_group = current($contact_groups);
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message),
			'active_contact_group' => $active_contact_group->rest(),
			'vertical_menu_html' => $vertical_menu_view->render(),
			'js' => array(
				'data' => $js_data
			),
		);
		
		Database::instance()->transaction_commit();
	}

}

