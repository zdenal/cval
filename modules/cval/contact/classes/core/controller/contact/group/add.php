<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_Group_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_Group_Add extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/contact/group/add')
				->set('csrf', Security::token(TRUE, 'cval/contact/group/add'))
				->bind('contact_group', $contact_group);
		
		$contact_group = Jelly::factory('contact_group');
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/contact/group/add', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array('label')));
		
		Database::instance()->transaction_start();
		
		$contact_group = Jelly::factory('contact_group');
		$contact_group->set($post_fields);
		$contact_group->user = $this->cval->user();
		$contact_group->save();
		
		$vertical_menu_view = View::factory('cval/content/contacts/groups/menu/vertical')
				->bind('contact_groups', $contact_groups)
				->set('notlive', TRUE);
		
		$contact_groups = $this->cval->user()->contact_groups();
		
		$js_data = array();
		
		foreach ($contact_groups as $tmp_contact_group)
		{
			// Set js data
			$js_data[$tmp_contact_group->html_class(TRUE)] 
					= $tmp_contact_group->rest();
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Contact group was created!')),
			'contact_group' => Cval_REST_Model::get($contact_group),
			'vertical_menu_html' => $vertical_menu_view->render(),
			'js' => array(
				'data' => $js_data
			),
		);
		
		Database::instance()->transaction_commit();
	}

}

