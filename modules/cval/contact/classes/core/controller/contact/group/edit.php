<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_Group_Edit
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_Group_Edit extends Controller_Cval_Ajax {

	/**
	 * @var Model_Contact_Group
	 */
	protected $_contact_group;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$this->_contact_group = Jelly::select('contact_group')
				->load(Arr::get($_POST, 'id', 0));

		if ( ! $this->_contact_group->loaded())
		{
			throw Model_Not_Found_Exception::create($this->_contact_group->meta()->model(), Arr::get($_POST, 'id', 0));
		}

		$this->_acl_resource = $this->_contact_group;
	}
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/contact/group/edit')
				->set('csrf', Security::token(TRUE, 'cval/contact/group/edit'))
				->bind('contact_group', $contact_group);
		
		$contact_group = $this->_contact_group;
		
		$this->request->response = array(
			'html' => $html->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/contact/group/edit', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array('label')));
		
		Database::instance()->transaction_start();
		
		$this->_contact_group->set($post_fields);
		$this->_contact_group->save();
		
		$vertical_menu_view = View::factory('cval/content/contacts/groups/menu/vertical')
				->bind('contact_groups', $contact_groups)
				->set('notlive', TRUE);
		
		$contact_groups = $this->cval->user()->contact_groups();
		
		$js_data = array();
		
		foreach ($contact_groups as $tmp_contact_group)
		{
			// Set js data
			$js_data[$tmp_contact_group->html_class(TRUE)] 
					= $tmp_contact_group->rest();
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Contact group was updated!')),
			'contact_group' => Cval_REST_Model::get($this->_contact_group),
			'vertical_menu_html' => $vertical_menu_view->render(),
			'js' => array(
				'data' => $js_data
			),
		);
		
		Database::instance()->transaction_commit();
	}

}

