<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Contact_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Contact_Add extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/contact/add')
				->set('csrf', Security::token(TRUE, 'cval/contact/add'))
				->bind('table', $table)
				->bind('contact_groups', $contact_groups)
				->bind('target_contact_group', $target_contact_group)
				->bind('locations', $locations)
				->bind('location', $location);
		
		$action_table = Request::factory(Route::get('contact')->uri(array(
			'controller' => 'add_search_results_table'
		)))->execute();
		
		$table = $action_table->response['html'];
		
		$target_contact_group = Arr::get($_POST, 'target_contact_group');
		$contact_groups = $this->cval->user()->contact_groups_choices(array('all'));
		
		$locations = $this->cval->user()->locations_choices(FALSE);
		$location = $this->cval->location()->id();
		
		$this->request->response = array(
			'html' => $html->render(),
			'target_contact_group' => $target_contact_group,
			'location' => $location
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/contact/add', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array(
			'users', 'groups', 'location'
		)));
		
		$owner = Cval::instance()->user();
		$location = Model_Location::load(Arr::get($post_fields, 'location'));
		
		$post_users = Arr::get($post_fields, 'users', array());
		$post_groups = Arr::get($post_fields, 'groups', array());
		$is_remote = $location->is_remote();
		// Make sure post users is array
		if (empty($post_users))
		{
			$post_users = array();
		}
		// Make sure post groups is array
		if (empty($post_groups))
		{
			$post_groups = array();
		}
		
		$groups = array();
		$set_favorite = FALSE;
		foreach ($post_groups as $post_group)
		{
			$group_id = Arr::get($post_group, 'id');
			if ($group_id == 'favorite')
			{
				$set_favorite = TRUE;
				continue;
			}
			
			$group = Jelly::select('contact_group')
				->where('user', '=', $owner->id())
				->load($group_id);
			
			if ( ! $group->loaded())
			{
				continue;
			}
			
			$groups[] = $group;
		}
		
		$profile_hashes = array();
		
		foreach ($post_users as $post_user)
		{
			$profile_hashes[] = Arr::get($post_user, 'hash');
		}
		
		$data = $location->get_users($profile_hashes);
		$users = $data['collection'];
		
		Database::instance()->transaction_start();
		
		foreach ($users as $user)
		{	
			if ($is_remote)
			{
				$user = Model_User_Profile_Shared::unserialize($user);
				$user->save();
			}
			elseif ($owner->agent()->user_profile_shared()->id() == $user->id())
			{
				// Do not add owner to contacts
				continue;
			}
			
			$contact = Jelly::select('contact')
				->where('user', '=', $owner->id())
				->where('user_profile_shared', '=', $user->id())
				->load();
			
			if ( ! $contact->loaded())
			{
				$contact->set(array(
					'user' => $owner,
					'user_profile_shared' => $user
				));
			}
			
			if ($set_favorite)
			{
				$contact->is_favorite = TRUE;
			}
			
			foreach ($groups as $group)
			{
				$contact->add('contact_groups', $group);
			}
			$contact->save();
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Contacts were created or updated!')),
		);
		
		Database::instance()->transaction_commit();
	}

}

