<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Remote profile find request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Contact_Profile_Find_Request extends Controller_Location_Request
{

	public function before()
	{	
		parent::before();
		
		$this->_error_codes(array(
			2	=> 'Parameter "conditions" must be an array',
			3	=> 'Parameter "pagination" must be not empty array',
		));
	}
	
	public function action_index()
	{
		$data = $this->data();
		
		$conditions = Arr::get($data, 'conditions');
		if ( ! is_array($conditions))
		{
			return $this->response(2);
		}
		
		$pagination = Arr::get($data, 'pagination');
		if (empty($pagination) OR ! is_array($pagination))
		{
			return $this->response(3);
		}
		
		$result = Model_Location::load_local()->find_users($conditions, $pagination);
		
		$users_data = array();
		foreach ($result['collection'] as $user)
		{
			$users_data[] = $user->serialize(FALSE);
		}
		
		$result['collection'] = $users_data;
		
		$this->response($result);
	}

} // End Core_Controller_Contact_Profile_Find_Request
