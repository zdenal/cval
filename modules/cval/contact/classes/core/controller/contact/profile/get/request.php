<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Remote profile get request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Contact_Profile_Get_Request extends Controller_Location_Request
{
	public function before()
	{	
		parent::before();
		
		$this->_error_codes(array(
			2	=> 'Parameter "hashes" must be not empty array',
		));
	}

	public function action_index()
	{
		$data = $this->data();
		
		$hashes = Arr::get($data, 'hashes');
		if (empty($hashes) OR ! is_array($hashes))
		{
			return $this->response(2);
		}
		$result = Model_Location::load_local()->get_users($hashes);
		
		$users_data = array();
		foreach ($result['collection'] as $user)
		{
			$users_data[] = $user->serialize();
		}
		
		$result['collection'] = $users_data;
		
		$this->response($result);
	}

} // End Core_Controller_Contact_Profile_Get_Request
