<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Contact_Group
 * 
 * Class for system and database contact groups.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Contact_Group
{

	/**
	 * @param	mixed	Model_Contact_Group or string for system groups
	 * @return	Contact_Group 
	 */
	public static function factory($contact_group)
	{
		if ($contact_group instanceof Model_Contact_Group OR strpos($contact_group, '__') !== FALSE)
		{
			return Contact_Group_Model::instance($contact_group);
		}
		
		return Contact_Group_System::instance($contact_group);
	}
	
	/**
	 * Group unique id  
	 */
	abstract public function id();
	
	/**
	 * Group name 
	 */
	abstract public function name();
	
	/**
	 * Can be edited 
	 */
	abstract public function editable();
	
	/**
	 * Can be deleted 
	 */
	abstract public function deletable();
	
	/**
	 * Can user add/remove items to/from this group 
	 */
	abstract public function assignable();
	
	/**
	 * Returns builder for all contacts for this group and given user
	 * 
	 * @params	Model_User		Owner of contacts
	 * @return	Model_Builder_Contact
	 */
	abstract protected function _contacts(Model_User $user);
	
	/**
	 * Returns all contacts for this group and given user
	 * 
	 * @return	Jelly_Collection
	 * @return	Model_Builder_Contact		If second parameter is FALSE
	 */
	public function contacts(Model_User $user, $execute = TRUE, $order = TRUE)
	{
		$builder = $this->_contacts($user);
		
		if ($execute)
		{
			if ($order)
			{
				$builder->use_default_order();
			}
			return $builder->execute();
		}
		
		return $builder;
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-contact_group-id-'.$this->id();
	}
	
	public function rest()
	{
		return array(
			'id' => $this->id(),
			'label' => $this->name(),
			'fields' => array(
				'id' => array(
					'value' => $this->id()
				),
				'label' => array(
					'value' => $this->name()
				),
				'editable' => array(
					'value' => $this->editable()
				),
				'deletable' => array(
					'value' => $this->deletable()
				),
				'assignable' => array(
					'value' => $this->assignable()
				),
			)
		);
	}

}
