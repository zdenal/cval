<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Contact_Group_System_All
 * 
 * Class for system contact group with key 'all'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Contact_Group_System_All extends Contact_Group_System
{

	public function assignable()
	{
		return FALSE;
	}
	
	public function name()
	{
		return Cval::i18n('All');
	}

}
