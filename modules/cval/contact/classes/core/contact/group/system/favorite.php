<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Contact_Group_System_Favorite
 * 
 * Class for system contact group with key 'favorite'.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Contact_Group_System_Favorite extends Contact_Group_System
{

	public function assignable()
	{
		return TRUE;
	}
	
	public function name()
	{
		return Cval::i18n('Favorites');
	}
	
	protected function _contacts(Model_User $user)
	{
		return parent::_contacts($user)
				->is_favorite();
	}

}
