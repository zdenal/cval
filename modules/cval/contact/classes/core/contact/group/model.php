<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Contact_Group_Model
 * 
 * Class for database contact group.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Contact_Group_Model extends Contact_Group
{
	
	/**
	 * @param	Model_Contact_Group|string		database model instance or pk
	 * @return	Contact_Group_Model
	 */
	public static function instance($model)
	{
		if ( ! $model instanceof Model_Contact_Group)
		{
			$id = $model;
			$model = Jelly::select('contact_group')->load($id);
			
			if ( ! $model->loaded())
			{
				throw Model_Not_Found_Exception::create($model, $id);
			}
		}
		
		return new Contact_Group_Model($model);
	}
	
	public function __construct(Model_Contact_Group $model)
	{
		$this->_model = $model;
	}

	public function assignable()
	{
		return TRUE;
	}
	
	public function deletable()
	{
		return TRUE;
	}

	public function editable()
	{
		return TRUE;
	}

	public function id()
	{
		return $this->_model->id();
	}

	public function name()
	{
		return $this->_model->name();
	}
	
	protected function _contacts(Model_User $user)
	{
		return $this->_model->get('contacts');
	}
	
	/**
	 * @var	Model_Contact_Group
	 */
	protected $_model;

}
