<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Contact_Group_System
 * 
 * Class for system contact group.
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Contact_Group_System extends Contact_Group
{
	/**
	 * @param	string	system group key
	 * @return	Contact_Group_System
	 */
	public static function instance($key)
	{
		$class = 'contact_group_system_'.$key;
		
		return new $class($key);
	}
	
	/**
	 * Gets array of all system instances
	 * 
	 * @return	array 
	 */
	public static function instances()
	{
		return array(
			'all' => Contact_Group::factory('all'),
			'favorite' => Contact_Group::factory('favorite'),
		);
	}
	
	public function __construct($key)
	{
		$this->_key = $key;
	}

	public function deletable()
	{
		return FALSE;
	}

	public function editable()
	{
		return FALSE;
	}

	public function id()
	{
		return $this->_key;
	}
	
	/**
	 * Returns builder for all contacts for this group and given user
	 * 
	 * @params	Model_User		Owner of contacts
	 * @return	Model_Builder_Contact
	 */
	protected function _contacts(Model_User $user)
	{
		return Jelly::select('contact')
				->where('user', '=', $user->id());
	}
	
	/**
	 * @var	string
	 */
	protected $_key;

}
