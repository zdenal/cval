<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Builder_Contact
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Builder_Contact extends Jelly_Builder
{
	/**
	 * Filter favorite.
	 * 
	 * @param	bool	FALSE for not matching
	 * @return	$this
	 */
	public function is_favorite($positive = TRUE)
	{
		return $this
			->where_open()
			->where('is_favorite', $positive ? '=' : '!=', TRUE)
			->where_close();
	}
	
	/**
	 * Joins location through user profile shared.
	 * 
	 * @param	string	join alias for location model
	 * @param	string	join alias for user_profile_shared model
	 * @return	$this
	 */
	public function join_location($alias = 'cl', $profile_alias = 'ups')
	{
		return $this->join(array('user_profile_shared', $profile_alias))
				->on('user_profile_shared_id', '=', $profile_alias.'.idmpk')
				->join(array('location', $alias))
				->on($profile_alias.'.location_id', '=', $alias.'.id');
	}
	
	/**
	 * Searches this model from one string.
	 * 
	 * @param	string	filter value
	 * @return	$this
	 */
	public function global_search($value)
	{
		$value = trim($value);
		if (strlen($value))
		{
			$this->join_location('cl')
					->where_open()
					->where('label', 'LIKE', '%'.$value.'%')
					->or_where('cl.label', 'LIKE', '%'.$value.'%')
					->where_close();
		}

		return $this;
	}
	
	/**
	 * Order contacts in default order
	 * 
	 * @return	$this 
	 */
	public function use_default_order()
	{
		return $this
			->order_by('label');
	}

}
