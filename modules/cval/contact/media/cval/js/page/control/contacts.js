
(function(Cval, $) {
	
	Cval_page_control_contacts_object = {
		staticParams : {
			defaults : {
				contactControlsSelector : '.Cval-contact-controls',
				contactAddSelector : '.Cval-contact-add',
				contactRemoveSelector : '.Cval-contact-remove',
				contactSelectAllSelector : '.Cval-contact-selectall',
				
				scheduleAddSelector : '.Cval-schedule-add',
				
				activeSection : 'contacts_list',
				activeSectionParams : {},
				
				refreshOnShow : false,
				inHistory : false,
				loadUrl : {
					route : 'contacts_main'
				},
				menuSelector : '.Cval-menu-id-contacts',
				menuVerticalSelector : '.Cval-menu-vertical-id-contacts',
				contactGroupControlsSelector : '.Cval-contact_group-controls'
			},
			contactGroupControls : {
				add : null,
				remove : null,
				edit : null
			},
			show : function(params, callback, section, sectionParams) {
				if ( ! section) {
					Cval.page.show(this.defaults.activeSection, this.defaults.activeSectionParams);
					return;
				}

				// Set active section
				this.defaults.activeSection = section;
				this.defaults.activeSectionParams = sectionParams || {};

				this._super(params, callback);
			},
			_bindEvents : function() {
				this._super();
				var thiss = this;

				// Bind create schedule click
				this.getElement().find(this.defaults.scheduleAddSelector)
					.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-clock',
							secondary : 'ui-icon-plus'
						}
					})
					.click(function(){
						alert(Cval.todo)
						return false;
					});

				// Init add contact button
				this.getElement().find(this.defaults.contactAddSelector)
					.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-person',
							secondary : 'ui-icon-plus'
						}
					});
					
				// Init remove contact button
				var contactRemoveButton = this.getElement().find(this.defaults.contactRemoveSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-trash'
						}
					});
					
				// Init select contact button
				this.getElement().find(this.defaults.contactSelectAllSelector)
					.Cval_button({
						text : false,
						icons : {
							//primary : 'ui-icon-person',
							secondary : 'ui-icon-check'
						}
					});
				
				this.getElement().find('.Cval-contact-controls').buttonset();
				contactRemoveButton.Cval_button('option', 'disabled', true);
				
				var contactGroupControlsEl = this.getElement().find(this.defaults.contactGroupControlsSelector);
				this.contactGroupControls.add = contactGroupControlsEl.find('.Cval-trigger-create')
					.click(function(){
						Cval.dialog.loadControl('contact_group_add', function(){
							Cval.dialog.control.contact_group_add.show()
						});
						return false;
					});
				this.contactGroupControls.remove = contactGroupControlsEl.find('.Cval-trigger-remove')
					.addClass('ui-state-disabled')
					.click(function(){
						if ($(this).hasClass('ui-state-disabled'))
							return false;
						
						var childControl = thiss.getChildControl(),
							groupData = childControl
								.getContactGroupElFromId(childControl.defaults.selectedGroup).Cval_data();
						
						Cval.dialog.loadControl('contact_group_remove', function(){
							Cval.dialog.control.contact_group_remove.show(null, {
								data : groupData
							})
						});
						return false;
					});
				this.contactGroupControls.edit = contactGroupControlsEl.find('.Cval-trigger-edit')
					.addClass('ui-state-disabled')
					.click(function(){
						if ($(this).hasClass('ui-state-disabled'))
							return false;
						
						var childControl = thiss.getChildControl(),
							groupData = childControl
								.getContactGroupElFromId(childControl.defaults.selectedGroup).Cval_data();
						
						Cval.dialog.loadControl('contact_group_edit', function(){
							Cval.dialog.control.contact_group_edit.show(null, {
								data : groupData
							})
						});
						return false;
					});
			},
			refreshContacts : function(callback) {
				// Propagate refreshing to subpage control
				this.getChildControl().refreshContacts(callback);
			},
			getChildControl : function() {
				return Cval.page.getControl(this.defaults.activeSection);
			}
		},
		params : {}
	};
	
	Cval.page.control.def.extend('Cval.page.control.contacts', 
		Cval_page_control_contacts_object.staticParams,
		Cval_page_control_contacts_object.params
	);

})(Cval, jQuery);