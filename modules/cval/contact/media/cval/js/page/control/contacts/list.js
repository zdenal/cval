(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.contacts_list', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('contacts_def', function(){
					
					
					Cval.page.control.contacts_def.extend('Cval.page.control.contacts_list', {
						defaults : {
							refreshOnShow : false,
							
							contactInfoSelector : '.Cval-trigger-info',
							contactRemoveSelector : '.Cval-trigger-remove',
							contactListSelector : '.Cval-list-contacts-wrapper',
							contactSelectorPrefix : '.Cval-model-contact-id-',
							
							selectedGroup : 'all',
							contactGroupSelectorPrefix : '.Cval-contact_group-id-',

							positions : {
								modified : false
							}
						},
						lastRefresh : {
							data : null
						},
						checkerEl : null,
						paginationEl : null,
						addButtonEl : null,
						removeButtonEl : null,
						searchInputEl : null,
						init : function(callback) {
							var thiss = this;

							// Needed resources
							var resources = [
								Cval.route('cval/media', {
									file : 'css/page/contacts/list.css'
								})
							];

							Cval.resource.get(resources, function(){
								Cval.core.callback(callback);
							});	
						},
						show : function(params, callback) {
							if (params && params.group) {
								// Set active group
								this.defaults.selectedGroup = params.group;
							}
							this._super(params, callback);
						},
						_beforeDisplay : function(callback) {
							var thiss = this;

							thiss.defaults.showParams = {
								group : thiss.defaults.selectedGroup
							};
							Cval.page.show('contacts', thiss.defaults.showParams, function(){
								Cval.core.callback(callback);
							}, thiss.defaults.id);
						},
						_show : function() {
							this._super();
							this.refreshContactList();
						},
						_hide : function() {
							this._super();
						},
						_bindEvents : function() {
							this._super();
							var thiss = this,
								parentControl = this.getParentControl();

							// Bind remove contact
							this.getElement().find(this.defaults.contactRemoveSelector).livequery(function(){
								$(this).click(function(){
									var data = thiss.getContactEl($(this)).Cval_data(),
										groupData = thiss.getContactGroupElFromId(thiss.defaults.selectedGroup).Cval_data();

									Cval.dialog.loadControl('contact_remove', function(){
										Cval.dialog.control.contact_remove.show(null, {
											data : data,
											groupData : groupData
										})
									});
									return false;
								})
							})
							
							// Bind info contact
							this.getElement().find(this.defaults.contactInfoSelector).livequery(function(){
								$(this).click(function(){
									alert(Cval.todo);
									return false;
								})
							})
							
							this.addButtonEl = parentControl.getElement().find(parentControl.defaults.contactAddSelector)
								.click(function(){
									var groupData = thiss.getContactGroupElFromId(thiss.defaults.selectedGroup).Cval_data();
									
									Cval.dialog.loadControl('contact_add', function(){
										Cval.dialog.control.contact_add.show(null, {
											groupData : groupData
										})
									});
									return false;
								});
							
							this.removeButtonEl = parentControl.getElement().find(parentControl.defaults.contactRemoveSelector)
								.click(function(){
									var selectedContacts = thiss.getSelectedContactsEls(),
										groupData = thiss.getContactGroupElFromId(thiss.defaults.selectedGroup).Cval_data(),
										multipleData = [],
										multiple = true,
										data = null;
									
									$.each(selectedContacts, function(){
										var data = $(this).Cval_data();
										multipleData.push(data);
									});
									
									if (multipleData.length < 2) {
										multiple = false;
										data = multipleData.shift();
									} else {
										data = multipleData;
									}
									
									Cval.dialog.loadControl('contact_remove', function(){
										Cval.dialog.control.contact_remove.show(null, {
											data : data,
											groupData : groupData,
											multiple : multiple
										})
									});
									return false;
								});

							// Hover effects for table rows
							this.getElement().find(this.defaults.contactListSelector+' tbody tr').livequery(function(){
								$(this).hover(function(){
									$(this).addClass('Cval-hover');
								}, function(){
									$(this).removeClass('Cval-hover')
								});
								
								$(this).find('.Cval-list-checkbox').change(function(){
									thiss.listUpdate();
								});
							});
							
							this._initSearch();
							this._initListControls();
						},
						refreshContacts : function(callback) {
							this.refreshContactList(callback);
						},
						refreshContactList : function(callback, data) {
							var thiss = this;
							
							data = $.extend({}, {
								body_only : 1,
								group : thiss.defaults.selectedGroup,
								search : thiss._searchData()
							}, data || {});

							Cval.ajax.callModal({
								route : 'contact',
								routeParams : {
									controller : 'list_table'
								},
								data : data
							}, function(json) {
								thiss.lastRefresh = {
									data : data
								};
								
								thiss.updateContactList(json, false, callback);
							});
						},
						updateContactList : function(data, complete, callback) {
							var table = this.getElement().find(this.defaults.contactListSelector);

							// Display, show no contact message
							if (data.count > 0)
								this.noContactMessageHide();
							else
								this.noContactMessageShow();
							// Display/show table header
							table.toggle(data.count > 0);

							// Replace only body content
							if ( ! complete)
								table = table.find('tbody');

							table.html(data.html);
							// Inject js data
							Cval.ajax.data.inject(data);
							
							// Enable/Disable checker
							this.checkerEl.Cval_button_checker({
								checked : false,
								disabled : data.count < 1
							});
							// update pagination
							this.paginationEl.Cval_pagination('update', data.pagination);
							this.listUpdate();

							Cval.core.callback(callback);
						},
						getContactEl : function(el) {
							el = $(el);

							if ( ! el.is('tr'))
								el = el.parents('tr:first');

							return el;
						},
						getContactElFromId : function(id) {
							var selectorClass = this.defaults.contactSelectorPrefix+id,
								el = $(this.getElement().find(this.defaults.contactListSelector+' tbody tr'+selectorClass));

							return el;
						},
						noContactMessageShow : function() {
							var thiss = this,
								isSearchActive = this.isSearchActive(),
								messageElClass = 'Cval-list-contacts-message-nocontact_'+(isSearchActive ? 'search' : 'default');
							
							if (thiss.getElement().find('.Cval-list-contacts-info-box .Cval-list-contacts-message-nocontact.'+messageElClass).length)
								return;
							
							this.noContactMessageHide();
							
							var content = isSearchActive
									? Cval.i18n.get('No contacts found, try to modify your search phrase.')
									: thiss.getElement().find('.Cval-list-contacts-message-nocontact-content').html();
									
							Cval.message.info(content, null, {
								wrapper : thiss.getElement().find('.Cval-list-contacts-info-box'),
								autoDestroy : false,
								closer : false,
								outerClass : 'Cval-list-contacts-message-nocontact '+messageElClass
							});
						},
						noContactMessageHide : function() {
							var messageEl = this.getElement().find('.Cval-list-contacts-info-box .Cval-list-contacts-message-nocontact');
							Cval.message.destroy(messageEl, true);
						},
						_initListControls : function() {
							var thiss = this,
								listControls = this.getParentControl()
									.getElement()
									.find('.Cval-contact-list-controls');
							
							this.checkerEl = listControls.find('.Cval-selectall')
								.Cval_button_checker({
									disabled : true,
									clickCallback : function(check, button) {
										thiss.getElement().find(thiss.defaults.contactListSelector)
											.find('tbody tr .Cval-list-checkbox')
											.attr('checked', check);
										thiss.listUpdate();
									}
								})

							this.paginationEl = listControls.find('.Cval-paginator')
								.Cval_pagination({
									clickCallback : function(page) {
										thiss.paginateResults(page);
									}
								});
								
							listControls.show();
						},
						paginateResults : function(page) {
							this.refreshContactList(null, $.extend({}, this.lastRefresh.data || {}, {
								pagination : page
							}));
						},
						updateMenu : function() {
							$(this.defaults.menuVerticalSelector).Cval_menu_vertical('select', '.Cval-menu-item-contacts_group_'+this.defaults.selectedGroup);
							var groupData = this.getContactGroupElFromId(this.defaults.selectedGroup).Cval_data(),
								parentControl = this.getParentControl();
								
							parentControl.contactGroupControls.remove.toggleClass('ui-state-disabled', ! groupData.fields.deletable.value);
							parentControl.contactGroupControls.edit.toggleClass('ui-state-disabled', ! groupData.fields.editable.value);
						},
						_initSearch : function() {
							var thiss = this,
								form = this.getParentControl().getElement().find('form.Cval-form-search');
							
							this.searchInputEl = form.find('input[name=search]');
							form.submit(function(e){
								e.preventDefault();
								thiss.refreshContactList();
								return false;
							});
						},
						_searchData : function() {
							return {
								global : this.searchInputEl.val().trim()
							};
						},
						isSearchActive : function() {
							return this.lastRefresh.data.search.global.length > 0;
						},
						listUpdate : function() {
							var thiss = this;
							
							var checkedCount = thiss.getSelectedContactsEls().length;
							
							this.removeButtonEl.Cval_button('option', 'disabled', checkedCount < 1);
						},
						getContactGroupElFromId : function(id) {
							var selectorClass = this.defaults.contactGroupSelectorPrefix+id,
								el = $(this.defaults.menuVerticalSelector).find(selectorClass);

							return el;
						},
						getSelectedContactsEls : function() {
							return this.getElement().find(this.defaults.contactListSelector)
								.find('tbody tr')
								.filter(function(){
									return $(this).find('.Cval-list-checkbox').is(':checked')
								});
						}
					}, {});
					
					
					Cval.page.control.contacts_list.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);