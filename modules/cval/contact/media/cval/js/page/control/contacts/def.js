(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.contacts_def', {
		defaults : {
			/**
			 * Page holder
			 */
			parentElement : '.Cval-page-id-contacts-content',
			/**
			 * Page main element classes. It must be same for all instances,
			 * otherwise this.closeOtherPages will not work properly.
			 */
			pageClass : 'Cval-page-id-contacts-page',
			menuSelector : '.Cval-menu-id-contacts',
			menuVerticalSelector : '.Cval-menu-vertical-id-contacts'
		},
		_parentControl : null,
		refreshContacts : function(callback) {
			// Implement this, to refresh contacts list, timetable or etc.
			// Now just call the callback
			Cval.core.callback(callback);
		},
		_beforeDisplay : function(callback) {
			var thiss = this;

			Cval.page.show('contacts', null, function(){
				Cval.core.callback(callback);
			}, thiss.defaults.id);
		},
		getUrlFromId : function() {
			var parts = this.defaults.id.split('_');
			var routeConf = { 
				route : parts[0],
				routeParams : {
					controller : parts.slice(1).join('_')
				}
			};
			return routeConf;
		},
		updateMenu : function() {
			// To implement
		},
		_show : function() {
			this._super();
			this.updateMenu();
		},
		getParentControl : function() {
			if (this._parentControl === null) {
				this._parentControl = Cval.page.getControl(this.getElement().parents('.Cval-page:first'));
			}
			return this._parentControl;
		}
	}, {});

})(Cval, jQuery);