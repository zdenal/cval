(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.contact_group_add', {
		defaults : {
			dialog : {
				title : Cval.i18n.get('Create a contact group'),
				CvalHeight : false,
				destroyOnClose : true,
				width: 300
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'contact_group',
				routeParams : {
					controller : 'add'
				}
			},
			refreshOnShow : true
		},
		wizardEl : null,
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jWizard.base.css'
			}),
			Cval.route('cval/media', {
				file : 'css/jquery/ui/jWizard.css'
			}),
			Cval.route('cval/media', {
				file : 'js/jquery/ui/jWizard.js'
			})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_bindEvents : function() {
			var thiss = this;
			
			this.wizardEl = this.getElement().find('.jWizard');
			
			this.wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: false,
					//cancelText: Cval.i18n.get('I\'m Done'),
					finishText: Cval.i18n.get('Create')
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.wizardNext(event, ui);
				},
				finish : function(event, ui) {
					thiss.wizardFinish(event, ui);
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
			});
			
			this._initWizard();
		},
		_initWizard : function() {
			var thiss = this;
			
			this.wizardEl.find('form').submit(function(e){
				e.preventDefault();
				// On form submit go to nextStep
				thiss.wizardEl.Cval_jWizard('nextStep');
				return false;
			})
		},
		wizardNext : function(event, ui) {},
		wizardFinish : function() {
			this._wizardFinish();
		},
		_wizardFinish : function() {
			var thiss = this,
				form = this.wizardEl.find('form');

			var data = form.serializeJSON();

			Cval.ajax.callModal({
				route : 'contact_group',
				routeParams : {
					controller : 'add',
					action : 'submit'
				},
				data : data
			}, function(json) {
				thiss._wizardFinishSuccess(json);
			})

			return false;
		},
		_wizardFinishSuccess : function(json) {
			if (json.message !== undefined)
			{
				Cval.message.fromOptions(json.message);
			}
			
			// Get contacts page control
			var contactsControl = Cval.page.getControl('contacts');

			// Update complete menu
			contactsControl.getElement().find(contactsControl.defaults.menuVerticalSelector)
				.replaceWith(json.vertical_menu_html);
			// Inject data for menu
			Cval.ajax.data.inject(json);
			
			// Manual run vertical menu
			contactsControl.getElement().find(contactsControl.defaults.menuVerticalSelector).Cval_menu_vertical();
			
			// Set active group to new group
			contactsControl.defaults.activeSectionParams.group = json.contact_group.id;	
			// Then show contacts page
			Cval.page.show('contacts');
			
			this.hide();
		}
	}, {});

})(Cval, jQuery)