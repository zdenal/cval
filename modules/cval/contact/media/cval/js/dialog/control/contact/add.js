(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.contact_add', {
		defaults : {
			dialog : {
				title : Cval.i18n.get('Add contact'),
				CvalHeight : false,
				destroyOnClose : true
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'contact',
				routeParams : {
					controller : 'add'
				}
			},
			refreshOnShow : true,
			searchResultsTableSelector : '.Cval-list-contacts-add-search',
			searchResultsNoresultsSelector : '.Cval-contact-search-noresult',
			targetGroupsSelectSelector : '.Cval-contact-add-target-groups-select',
			locationsSelectSelector : '.Cval-contact-add-locations-select',
			userInfoSelector : '.Cval-trigger-info'
		},
		wizardEl : null,
		checkerEl : null,
		paginationEl : null,
		lastSearch : {
			data : null
		},
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jWizard.base.css'
			}),
			Cval.route('cval/media', {
				file : 'css/jquery/ui/jWizard.css'
			}),
			Cval.route('cval/media', {
				file : 'js/jquery/ui/jWizard.js'
			}),
			Cval.route('cval/media', {
				file : 'jquery/css/jquery.multiselect.css'
			}),
			Cval.route('cval/media', {
				file : 'jquery/js/jquery.multiselect.js'
			}),
			Cval.route('cval/media', {
				file : 'css/dialog/contact/add.css'
			})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_beforeHide : function(dialog, event, ui) {
			this._super.apply(this, arguments);
			
			Cval.core.callback(this.data(this.getElement()).options.hideCallback || function(){
				Cval.page.getControl('contacts').refreshContacts();
			});
		},
		_bindEvents : function() {
			var thiss = this;
			
			this.wizardEl = this.getElement().find('.jWizard');
			
			this.wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: false,
					cancelText: Cval.i18n.get('I\'m Done'),
					finishText: Cval.i18n.get('Add selected users')
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.wizardNext(event, ui);
				},
				finish : function(event, ui) {
					thiss.wizardFinish(event, ui);
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
			});
			
			this._initWizard();
			
			this._initSearch();
			
			this._initResults();
			this._initLocationSelect();
			this._initGroupSelect();
		},
		getLoadConfig : function() {			
			return $.extend({}, this._super(), {
				data : {
					target_contact_group : this.data(this.getElement()).options.groupData.id
				}
			});
		},
		_initWizard : function() {
			var thiss = this;
			
			this.wizardEl.find('.Cval-dialog-contact-add-search form').submit(function(e){
				e.preventDefault();
				// On form submit perform search
				thiss._search();
				return false;
			})
		},
		wizardNext : function(event, ui) {},
		wizardFinish : function() {
			this._wizardFinish();
		},
		_wizardFinish : function() {
			var thiss = this,
				listEl = $(this.wizardEl).find(this.defaults.searchResultsTableSelector),
				groupsEl = $(this.wizardEl).find(this.defaults.targetGroupsSelectSelector);

			var users = [];
			listEl.find('tbody tr .Cval-list-checkbox').filter(':checked').each(function(){
				var userData = thiss.getUserEl($(this)).Cval_data();
				users.push({
					id : userData.id,
					hash : userData.hash
				});
			});
			
			if ( ! users.length) {
				Cval.message.info(Cval.i18n.get('At least one user must be selected.'));
				return;
			}
			
			var groups = [];
			groupsEl.multiselect('getChecked').each(function(){
				groups.push({
					id : $(this).val()
				});
			});

			Cval.ajax.callModal({
				route : 'contact',
				routeParams : {
					controller : 'add',
					action : 'submit'
				},
				data: {
					users : users,
					groups : groups,
					csrf : $(this.wizardEl).find('input[name=csrf]').val(),
					location : thiss.lastSearch.data.location
				}
			}, function(json) {
				thiss._wizardFinishSuccess(listEl, json);
			})

			return;
		},
		_wizardFinishSuccess : function(listEl, json) {
			if (json.message !== undefined)
			{
				Cval.message.fromOptions(json.message);
			}
			
			// Uncheck all users
			this.checkerEl.Cval_button_checker('uncheckAndPropagate');
		},
		/**
		 * Returns data from dialog you want send to server after save is invoked.
		 * Should be overidden in subclasses.
		 */
		_finishAjaxData : function(listEl) {
			return $(listEl).find('form:first').serializeJSON();
		},
		_initSearch : function() {
			var thiss = this;
			this.wizardEl.find('.Cval-contact-search-trigger').click(function(){
				thiss._search();
			});
		},
		_search : function(data) {
			var thiss = this;

			data = $.extend({}, {
				body_only : 1,
				value : thiss.wizardEl.find('input[name=name]').val(),
				location : thiss.wizardEl.find('select[name=location]').val()
			}, data || {});

			Cval.ajax.callModal({
				route : 'contact',
				routeParams : {
					controller : 'add_search_results_table',
					action : 'search'
				},
				data : data
			}, function(json) {
				thiss.lastSearch = {
					data : data
				};
				
				thiss._updateResults(json, false);
			});
		},
		_updateResults : function(data, complete, callback) {
			var table = this.wizardEl.find(this.defaults.searchResultsTableSelector),
				noresults = this.wizardEl.find(this.defaults.searchResultsNoresultsSelector);
			
			// Display/show table header
			table.toggle(data.count > 0);
			noresults.toggle(data.count < 1);

			// Replace only body content
			if ( ! complete)
				table = table.find('tbody');

			table.html(data.html);
			// Inject js data
			Cval.ajax.data.inject(data);
			
			// Enable/Disable checker
			this.checkerEl.Cval_button_checker({
				checked : false,
				disabled : data.count < 1
			});
			// update pagination
			this.paginationEl.Cval_pagination('update', data.pagination);

			Cval.core.callback(callback);
		},
		_clearResults : function() {
			this._updateResults({
				count : 0
			});
			this.lastSearch = {};
		},
		_initGroupSelect : function() {
			var thiss = this;
			
			var select = this.getElement().find(this.defaults.targetGroupsSelectSelector);
			select.multiselect({
				noneSelectedText: Cval.i18n.get('None'),
				selectedText: function(numChecked, numTotal, checkedItems) {
					var text = checkedItems.slice(0, 2).map(function(item){
						return $(item).next().text(); 
					}).join(', ');
					var moreCnt = numChecked - 3;
					if (moreCnt > 0) {
						text += ' + '+moreCnt;
					}
					return text;
				},
				height : 'auto',
				minWidth: 250
			});
		},
		_initLocationSelect : function() {
			var thiss = this;
			
			var select = this.getElement().find(this.defaults.locationsSelectSelector);
			select.multiselect({
				minWidth: 250,
				multiple: false,
				header: false,
				selectedList: 1,
				height : 'auto'
			});
		},
		_initResults : function() {
			var thiss = this;
			
			this.checkerEl = this.getElement().find('.Cval-selectall')
				.Cval_button_checker({
					disabled : true,
					clickCallback : function(check, button) {
						thiss.wizardEl.find(thiss.defaults.searchResultsTableSelector)
							.find('tbody tr .Cval-list-checkbox:enabled')
							.attr('checked', check);
					}
				})
				
			this.paginationEl = this.getElement().find('.Cval-paginator').Cval_pagination({
				clickCallback : function(page) {
					thiss.paginateResults(page);
				}
			});
			
			// Hover effects for table rows
			this.getElement().find(this.defaults.searchResultsTableSelector+' tbody tr').livequery(function(){
				$(this).hover(function(){
					$(this).addClass('Cval-hover');
				}, function(){
					$(this).removeClass('Cval-hover')
				});
			});
			
			// Bind info user
			this.getElement().find(this.defaults.userInfoSelector).livequery(function(){
				$(this).unbind('click').click(function(){
					alert(Cval.todo);
					return false;
				})
			})
		},
		paginateResults : function(page) {
			this._search($.extend({}, this.lastSearch.data || {}, {
				pagination : page
			}));
		},
		getUserEl : function(el) {
			el = $(el);

			if ( ! el.is('tr'))
				el = el.parents('tr:first');

			return el;
		}
	}, {});

})(Cval, jQuery)