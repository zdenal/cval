<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('settings_main', 'settings')
        ->defaults(array(
            'controller' => 'settings',
        ))->config_set('route_dash', 'page');

Route::set('settings', 'settings/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'settings',
        ))->config_set('route_dash', 'page');

