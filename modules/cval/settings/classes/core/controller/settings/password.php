<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Dashboard Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Settings_Password extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Change password',
		));
		
		$html = View::factory('cval/content/settings/password')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/settings/password'));
		
		$user = $this->cval->user();
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}
	
	public function action_update()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/settings/password', TRUE);
		
		$post = Security::xss_clean(Arr::extract($_POST, array(
			'password',
			'password_confirm'
		)));
		
		$this->cval->user()->set($post)->save();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Password was updated!'))
		);
	}

}

