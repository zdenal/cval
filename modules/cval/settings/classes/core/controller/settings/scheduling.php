<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Settings Scheduling Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Settings_Scheduling extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Scheduling settings',
		));
		
		$html = View::factory('cval/content/settings/scheduling')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/settings/scheduling'));
		
		$user = $this->cval->user();
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}
	
	public function action_update()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/settings/scheduling', TRUE);
		
		$post_user_profile = Security::xss_clean(Arr::extract($_POST, array(
			'schedule_email_on_inbound',
			'schedule_plan_min_length',
			'schedule_plan_min_length_units',
		)));
		
		Database::instance()->transaction_start();
		
		$this->cval->user_profile()->set($post_user_profile)->save();
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Scheduling settings were updated!')),
			'user_profile' => Cval_REST::instance()->model($this->cval->user_profile())->render(),
		);
		
		Database::instance()->transaction_commit();
	}

}

