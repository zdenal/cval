<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Dashboard Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Settings_Account extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Account settings',
		));
		
		$html = View::factory('cval/content/settings/account')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/settings/account'));
		
		$user = $this->cval->user();
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}
	
	public function action_update()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/settings/account', TRUE);
		
		$post_user = Security::xss_clean(Arr::extract($_POST, array(
			'label',
			'email'
		)));
		
		$post_user_profile = Security::xss_clean(Arr::extract($_POST, array(
			'timezone',
			'time_24_hour',
			'date_month_second',
			'first_day'
		)));
		
		Database::instance()->transaction_start();
		
		$this->cval->user()->set($post_user)->save();
		$this->cval->user_profile()->set($post_user_profile)->set(array(
			'label' => $this->cval->user()->name()
		))->save();

		$message = Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Account was updated!'));
		Cval_Message::set_flash($message);

		$this->request->response = array(
			'message' => $message,
			'user' => Cval_REST::instance()->model($this->cval->user())->render(),
			'user_profile' => Cval_REST::instance()->model($this->cval->user_profile())->render(),
		);
		
		Database::instance()->transaction_commit();
	}
	
	public function action_update_timezone()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$post_user_profile = Security::xss_clean(Arr::extract($_POST, array(
			'timezone',
		)));
		
		Database::instance()->transaction_start();
		
		$this->cval->user_profile()->set($post_user_profile)->save();
		
		$this->request->response = array(
			'user' => Cval_REST::instance()->model($this->cval->user())->render(),
			'user_profile' => Cval_REST::instance()->model($this->cval->user_profile())->render(),
		);
		
		Database::instance()->transaction_commit();
	}

}

