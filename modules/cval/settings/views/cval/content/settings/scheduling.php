<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_16 ui-widget Cval-helper-margin-top')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="field-label grid_8">
		<?php echo Cval::i18n('Minimum schedule length') ?>
	</div>
	<div class="clear"></div>
	<div class="grid_7">
		<div class="field Cval-helper-float-left" style="width: 55px"><?php echo $user->user_profile()->input('schedule_plan_min_length') ?></div>
		<div class="Cval-helper-float-left Cval-helper-margin-left-50"><?php echo $user->user_profile()->input('schedule_plan_min_length_units') ?></div>
	</div>
	<div class="clear"></div>

	<div class="field-label grid_8">
		<?php echo Cval::i18n('Send me an email when inbound schedule comes') ?>
	</div>
	<div class="clear"></div>
	<div class="field grid_8">
		<?php echo $user->user_profile()->input('schedule_email_on_inbound') ?>
	</div>
	<div class="clear"></div>

	<div class="grid_16 button-line Cval-helper-margin-top">
		<input type="submit" value="<?php echo Cval::i18n('Update') ?>" class="Cval-button Cval-button-live"/>
	</div>
	<div class="clear"></div>

<?php echo Form::close(); ?>

<div class="clear"></div>