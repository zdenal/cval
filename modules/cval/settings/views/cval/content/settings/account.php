<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_16 ui-widget Cval-helper-margin-top')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="field-label grid_8">
		<?php echo UTF8::ucfirst(Cval::i18n($user->meta()->fields('label')->label)) ?>
	</div>
	<div class="clear"></div>
	<div class="field grid_8">
		<?php echo $user->input('label') ?>
	</div>
	<div class="clear"></div>

	<div class="field-label grid_8">
		<?php echo UTF8::ucfirst(Cval::i18n($user->meta()->fields('email')->label)) ?>
		<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
	</div>
	<div class="clear"></div>
	<div class="field grid_8">
		<?php echo $user->input('email') ?>
	</div>
	<div class="clear"></div>
	
	<div class="field-label grid_8">
		<?php echo UTF8::ucfirst(Cval::i18n($user->user_profile()->meta()->fields('timezone')->label)) ?>
	</div>
	<div class="clear"></div>
	<div class="field grid_8">
		<?php echo $user->user_profile()->input('timezone') ?>
	</div>
	<div class="clear"></div>
	
	<div class="field-label grid_4">
		<?php echo UTF8::ucfirst(Cval::i18n('Date format')) ?>
	</div>
	<div class="field-label grid_4">
		<?php echo UTF8::ucfirst(Cval::i18n('Time format')) ?>
	</div>
	<div class="field-label grid_4">
		<?php echo UTF8::ucfirst(Cval::i18n('Week starts on')) ?>
	</div>
	<div class="clear"></div>
	<div class="field grid_4">
		<?php echo $user->user_profile()->input('date_month_second') ?>
	</div>
	<div class="field grid_4">
		<?php echo $user->user_profile()->input('time_24_hour') ?>
	</div>
	<div class="field grid_4">
		<?php echo $user->user_profile()->input('first_day') ?>
	</div>
	<div class="clear"></div>

	<div class="grid_16 button-line Cval-helper-margin-top">
		<input type="submit" value="<?php echo Cval::i18n('Update') ?>" class="Cval-button Cval-button-live"/>
	</div>
	<div class="clear"></div>

<?php echo Form::close(); ?>

<div class="clear"></div>