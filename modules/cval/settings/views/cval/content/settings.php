<div class="container_16">	
	
	<div class="grid_3">
		<ul class="Cval-menu-vertical Cval-menu-vertical-live Cval-menu-vertical-id-settings">
			<li class="Cval-menu-item-settings_account"><a href="<?php echo Route::url_protocol('settings', array(
					'controller' => 'account'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Account settings') ?></a></li>
			<li class="Cval-menu-item-settings_password"><a href="<?php echo Route::url_protocol('settings', array(
					'controller' => 'password'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Change password') ?></a></li>
			<li class="Cval-menu-item-settings_scheduling"><a href="<?php echo Route::url_protocol('settings', array(
					'controller' => 'scheduling'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Scheduling settings') ?></a></li>
		</ul>
	</div>
	
	<div class="Cval-page-id-settings-content grid_13"></div>
	
	<div class="clear"></div>
</div>