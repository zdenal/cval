(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.settings_account', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('settings_def', function(){
					
					
					Cval.page.control.settings_def.extend('Cval.page.control.settings_account', {
						_bindEvents : function() {
							this.getElement().find('form').submit(function(){
								var form = $(this);

								var data = form.serializeJSON();

								Cval.ajax.callModal({
									route : 'settings',
									routeParams : {
										controller : 'account',
										action : 'update'
									},
									data : data
								}, function(json) {
									Cval.message.fromOptions(json.message);

									// Reload browser after success save
									window.location.reload();
								})

								return false;
							})
						}
					}, {});
					
					
					Cval.page.control.settings_account.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);