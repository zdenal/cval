(function(Cval, $) {

	Cval.page.control.def.extend('Cval.page.control.settings', {
		defaults : {
			activeSection : 'settings_account',
			refreshOnShow : false,
			inHistory : false,
			loadUrl : {
				route : 'settings_main'
			},
			menuSelector : '.Cval-menu-vertical-id-settings'
		},
		show : function(params, callback, section) {
			if ( ! section) {
				Cval.page.show(this.defaults.activeSection);
				return;
			}
			
			// Set active section
			this.defaults.activeSection = section;
			
			this._super(params, callback);
		}
	}, {});

})(Cval, jQuery)