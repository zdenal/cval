(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.settings_password', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('settings_def', function(){
					
					
					Cval.page.control.settings_def.extend('Cval.page.control.settings_password', {
						_bindEvents : function() {
							this.getElement().find('form').submit(function(){
								var form = $(this);

								var data = form.serializeJSON();

								Cval.ajax.callModal({
									route : 'settings',
									routeParams : {
										controller : 'password',
										action : 'update'
									},
									data : data
								}, function(json) {
									Cval.message.fromOptions(json.message);
									
									form.Cval_form('clear');
								})

								return false;
							})
						}
					}, {});
					
					
					Cval.page.control.settings_password.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);