(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.settings_scheduling', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('settings_def', function(){
					
					
					Cval.page.control.settings_def.extend('Cval.page.control.settings_scheduling', {
						defaults : {
							minLengthSelector : 'input[name=schedule_plan_min_length]',
							minLengthUnitsSelector : 'select[name=schedule_plan_min_length_units]'
						},
						minLengthEl : null,
						minLengthUnitsEl : null,
						init : function(callback) {
							var thiss = this;

							// Needed resources
							var resources = [
								Cval.route('cval/media', {
									file : 'jquery/css/jquery.multiselect.css'
								}),
								Cval.route('cval/media', {
									file : 'jquery/js/jquery.multiselect.js'
								})
							];

							Cval.resource.get(resources, function(){
								Cval.core.callback(callback);
							});
						},
						_bindEvents : function() {
							this.getElement().find('form').submit(function(){
								var form = $(this);

								var data = form.serializeJSON();

								Cval.ajax.callModal({
									route : 'settings',
									routeParams : {
										controller : 'scheduling',
										action : 'update'
									},
									data : data
								}, function(json) {
									Cval.message.fromOptions(json.message);
								})

								return false;
							});
							
							this._initMinLengthUnits();
						},
						_initMinLengthUnits : function() {
							var thiss = this;

							this.minLengthUnitsEl = this.getElement().find(this.defaults.minLengthUnitsSelector);
							this.minLengthUnitsEl.multiselect({
								minWidth: 80,
								multiple: false,
								header: false,
								selectedList: 1,
								height: 'auto'
							}).change(function(){
								thiss._minLengthChange();
							});

							this.minLengthEl = this.getElement().find(this.defaults.minLengthSelector);
							this.minLengthEl.change(function(){
								thiss._minLengthChange();
							});

							thiss._minLengthChange();
						},
						_minLengthChange : function(){
							var length = parseInt(this.minLengthEl.val());

							if ( ! length || length < 1) {
								length = 1;
								this.minLengthEl.val(length);
							}
						}
					}, {});
					
					
					Cval.page.control.settings_scheduling.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);