<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Cval_Feed
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Cval_Feed extends Jelly_Model 
{
	
    public static function initialize(Jelly_Meta $meta) 
	{	
        $meta->name_key('label')
             ->sorting(array('label' => 'ASC'));

		$meta->fields(array(
            'id' => new Field_Primary,
			'user' => new Field_BelongsTo(array(
			)),
			'type' => new Field_Enum(array(
				'choices' => array(
					'calendar_add',
					'calendar_remove'
				),
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'author' => new Field_BelongsTo,
        ));
		
        Behavior_Labelable::initialize($meta, array(
			'label' => 'name',
			'rules' => array('not_empty' => NULL)
		));
		Behavior_Descriptionable::initialize($meta);

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }

}
