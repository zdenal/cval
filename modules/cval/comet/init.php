<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('comet', 'comet/<user>')
        ->defaults(array(
			'directory' => 'cval',
            'controller' => 'comet',
        ));

