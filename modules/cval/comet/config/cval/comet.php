<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	/**
	 * Allows to enable/disable comet feature 
	 */
	'enabled' => TRUE,
	
	/**
	 * Comet url, default cval location url from cval/location.url 
	 * config is replaced with this url for comet call.
	 * If you want to allow have more than 2 browser windows opened
	 * for user using Cval, set comet url so communication between comet url
	 * and location url is considered as cross-domain request. 
	 * 
	 * Example:
	 *		Location URL:	cval.localhost
	 *		Comet URL:		TRUE -> cval.localhost also
	 *			-> not cross-domain request
	 * 
	 *		Location URL:	cval.localhost
	 *		Comet URL:		cval-comet.localhost
	 *			-> cross-domain request
	 */
	'url' => TRUE,
	
	/**
	 * Maximum execution time in second for comet call. It can be reached
	 * if nothing is updated on server for long time. After this time, request
	 * is restarted from client side. 
	 */
	'maximum_execution_time' => 120

);

