<?php

//-- Configuration --------------------------------------------------------

/**
 * The directory in which your Cval installation is located. Can be relative
 * to this file or absolute.
 */
$path = '../cval';

/**
 * Name of index file in Cval installation directory. Almost always index.php. 
 */
$index = 'index.php';

//-- Environment setup --------------------------------------------------------

// Allow only comet call go through 
if (empty($_SERVER['PATH_INFO'])
		OR ! preg_match('/^\/comet\/[0-9]+/', $_SERVER['PATH_INFO']))
{
	die('Only comet request is allowed');
}

// Set the full path to the cval installation index file
define('CVALCOMETINDEX', rtrim($path, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.ltrim($index, DIRECTORY_SEPARATOR));

// Clean up the configuration vars
unset($path, $index);

// Bootstrap the application
require CVALCOMETINDEX;
