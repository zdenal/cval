(function(Cval, $) {

	Cval.comet = {
		timestamp : 0,
		hash : '',
		url : null,
		config : null,
		error : false,
		errorDelay : 5000,
		ajax : null,
		callbacks : {},
		init: function() {
			this.config = Cval.core.config('comet');
			this.url = this.config.url;
			this.connect();
		},
		connect: function() {
			// Check if comet is enabled
			if ( ! this.config.enabled)
				return;
			
			var thiss = this,
				data = {
					timestamp: this.timestamp,
					hash: this.hash
				};

			var settings = {
				crossDomain : true,
				url : this.url,
				type : 'GET',
				cache: false,
				dataType: 'json',
				data : data,
				success: function(json){
					thiss.error = json.error ? true : false;
					if ( ! json.flag)
						thiss.handleResponse(json);
				},
				error: function(jqXHR, textStatus, errorThrown){
					thiss.error = true;
					$.Console.Debug([jqXHR.status, jqXHR.statusText, jqXHR.responseText]);
				},
				complete: function(jqXHR, textStatus){
					$.Console.Debug('COMET complete')
					// send a new ajax request when this request is finished
					if (thiss.error) {
						$.Console.Debug('COMET error');
						// if a connection problem occurs, try to reconnect each 5 seconds
						setTimeout(function(){
							thiss.connect()
						}, thiss.errorDelay); 
					} else {
						$.Console.Debug('COMET success');
						thiss.connect();
					}
				}
			};

			this.ajax = $.ajax(settings);
			
			$.Console.Debug('COMET connect')
		},
		handleResponse: function(json) {
			this.timestamp = json.timestamp;
			this.hash = json.hash;
			$.Console.Debug(json);
			var key = json.key;
			
			// Iterate over each callback
			$.each(this.callbacks, function(name, data){
				// check if callback is associated for comet response key
				if (data.keys && (data.keys === true || $.inArray(key, data.keys) > -1)) {
					var options = data.options;
					// If is associated run callback with comet data
					Cval.core.callbackApply(data.callback, options.context || null, json.data, key, json.timestamp, json.hash);
				}
			});
		},
		registerCallback: function(name, callback, keys, options) {
			this.callbacks[name] = {
				callback : callback,
				keys : keys || [],
				options : options || {}
			}
		},
		removeCallback: function(name) {
			//$.Console.Debug(name, this.callbacks)
			if (this.callbacks[name])
				delete this.callbacks[name];
		}
	}

})(Cval, jQuery)