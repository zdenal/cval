<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Comet Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Cval_Comet extends Controller_Cval_Ajax {

	/**
	 * @var array	Array of actions which need not to be called as AJAX or internal request
	 *				Call from another origin are not marked as AJAX.
	 */
	public $non_ajax_actions = array('index');
	
	/**
	 * @var	bool	TRUE for auto appending system response to json array
	 */
	public $add_system_response = FALSE;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	public function action_index() 
	{	
		$start_time = time();
		$max_exec_time = Cval_Comet::config('maximum_execution_time', 120);
		// Set maximum php execution time
		ini_set('max_execution_time', $max_exec_time);
		$max_exec_time = max(1, $max_exec_time - 1);
		
		// Allow request coming only from cval url only
		if (Request::$referrer != Cval_Comet::cval_referrer())
		{
			throw new Request_Exception(403);
		}
		
		$user = Jelly::select('user')->load($this->request->param('user'));
		
		if ( ! $user->loaded())
		{
			throw new Kohana_Exception('User with ID :id not exists!', array(
				':id' => $this->request->param('user')
			));
		}
		
		$timestamp = Arr::get($_REQUEST, 'timestamp');
		$hash = Arr::get($_REQUEST, 'hash');
		
		$last_timestamp = Cval_Comet::last_modified($user);
		
		if (empty($timestamp))
		{
			$timestamp = $last_timestamp;
		}
		if (empty($hash))
		{
			$last_hash = Arr::get(Cval_Comet::read($user), 'hash');
			$hash = $last_hash;
		}
		
		$data = array();
		while (TRUE)
		{
			if ($last_timestamp > $timestamp)
			{
				$last_hash = Arr::get(Cval_Comet::read($user), 'hash');
				if ($last_hash != $hash)
				{
					$data = Cval_Comet::read($user);
					$data['timestamp'] = $last_timestamp;
					break;
				}
			}
			if ((time() - $start_time) > $max_exec_time)
			{
				$data = array(
					'flag' => 'MAX_EXEC_TIME_REACHED'
				);
				break;
			}
			sleep(1); // sleep 1s to unload the CPU
			$last_timestamp = Cval_Comet::last_modified($user);
		}
		
		// Modify headers to allow cross domain call to this controller from cval url only
		$this->request->headers['Access-Control-Allow-Origin'] = Cval_Comet::cval_origin();
		
		$this->request->response = $data;
	}

}

