<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Cval_Comet
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_Comet
{
	/**
	 * @var	string 
	 */
	protected static $_url;
	
	/**
	 * @var	array
	 */
	protected static $_config;
	
	/**
	 * @var	string 
	 */
	protected static $_cval_origin;
	
	/**
	 * @var	string 
	 */
	protected static $_cval_referrer;
	
	/**
	 * @var	string 
	 */
	protected static $_cache_path;
	
	/**
	 * Url for comet calls.
	 *
	 * @return	string
	 */
	public static function url()
	{	
		if (Cval_Comet::$_url === NULL)
		{
			$cval = Cval::instance();
			
			Cval_Comet::$_url = str_replace(
					Cval_Location::config('url'), 
					Cval_Comet::config('url'),
					Route::url_protocol('comet', array(
				'user' => $cval->user()->id()
			)));
		}
		
		return Cval_Comet::$_url;
	}
	
	/**
	 * Cval Url origin which needs to be set in 'Access-Control-Allow-Origin' 
	 * header parametr in Comet response header to allow cross domain calls.
	 *
	 * @return	string
	 */
	public static function cval_origin()
	{	
		if (Cval_Comet::$_cval_origin === NULL)
		{
			$cval_url = Cval_Location::config('url');
			$host = parse_url($cval_url, PHP_URL_HOST);
			
			if (empty($host))
			{
				$path_parts = explode('/', parse_url($cval_url, PHP_URL_PATH));
				$host = array_shift($path_parts);
			}
			
			if (empty($host))
			{
				throw new Kohana_Exception('Cval location is not configured properly for Comet calls.');
			}
			
			Cval_Comet::$_cval_origin = parse_url(Request::instance()->url(NULL, TRUE), PHP_URL_SCHEME).'://'.$host;
		}
		
		return Cval_Comet::$_cval_origin;
	}
	
	/**
	 * Cval Url referrer which is test in start of comet call 
	 * to check if request is comming from cval installation.
	 *
	 * @return	string
	 */
	public static function cval_referrer()
	{	
		if (Cval_Comet::$_cval_referrer === NULL)
		{
			$cval_url = rtrim(Cval_Location::config('url'), '/').'/';
			
			Cval_Comet::$_cval_referrer = parse_url(Request::instance()->url(NULL, TRUE), PHP_URL_SCHEME).'://'.$cval_url;
		}
		
		return Cval_Comet::$_cval_referrer;
	}
	
	/**
	 * Gets cval comet config.
	 * If key is NULL complete config array is returned.
	 * 
	 * @param	string	config key
	 * @param	mixed	default value to use if key is not set
	 * @return	mixed	config value 
	 */
	public static function config($key = NULL, $default = NULL)
	{
		if (Cval_Comet::$_config === NULL)
		{
			Cval_Comet::$_config = (array) Kohana::config('cval/comet');
			
			// If comet url is TRUE, use location url for comet calls
			if (Arr::get(Cval_Comet::$_config, 'url', TRUE) === TRUE)
			{
				Cval_Comet::$_config['url'] = Cval_Location::config('url');
			}
		}
		
		if ($key === NULL)
		{
			return Cval_Comet::$_config;
		}
		
		return Arr::path(Cval_Comet::$_config, $key, $default);
	}
	
	/**
	 * Configuration passed to client on first Cval call.
	 * 
	 * @return	array 
	 */
	public static function config_js()
	{
		$enabled = Cval_Comet::config('enabled');
		
		$data = array(
			'enabled' => $enabled
		);
		
		if ($enabled)
		{
			// If comet is enabled try to create cache path on js load
			Cval_Comet::_create_cache_path();
			$data['url'] = Cval_Comet::url();
		}
		
		return $data;
	}
	
	public static function write(Model_User $user, $key, array $data = array())
	{
		$data = array(
			'key' => $key,
			'hash' => Date::time().Text::random('default', 10),
			'data' => $data
		);
		File::write(Cval_Comet::_cache_path($user), json_encode($data));
	}
	
	public static function read(Model_User $user)
	{
		$data = File::read(Cval_Comet::_cache_path($user));
		if (empty($data))
		{
			$data = array(
				'key' => '',
				'hash' => '',
				'data' => array()
			);
		}
		else
		{
			$data = json_decode($data, TRUE);
		}
		return $data;
	}
	
	public static function last_modified(Model_User $user)
	{
		$path = Cval_Comet::_cache_path($user);
		if ( ! file_exists($path))
		{
			return 0;
		}
		clearstatcache();
		return filemtime($path);
	}
	
	protected static function _cache_path(Model_User $user = NULL)
	{
		if (Cval_Comet::$_cache_path === NULL)
		{
			Cval_Comet::$_cache_path = APPPATH.'cache'.DIRECTORY_SEPARATOR.'comet'.DIRECTORY_SEPARATOR;
		}
		
		if ($user)
		{
			return Cval_Comet::$_cache_path.$user->id();
		}
		return Cval_Comet::$_cache_path;
	}
	
	protected static function _create_cache_path()
	{
		$path = Cval_Comet::_cache_path();
		
		// Ensure path is writeable
		if ( ! is_writable($path))
		{
			if (file_exists($path) OR ! Dir::create($path))
			{
				throw new Kohana_Exception('Comet path \':path\' is not writeable.', array(
					':path' => $path
				));
			}
		}
	}

}