
SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(2, 1),
(2, 3);

INSERT INTO `users` (`id`, `label`, `username`, `email`, `password`, `verified`, `verification_id`) VALUES
(2, 'Peter Pan', 'peter.pan@mymail.com', 'peter.pan@mymail.com', 'e513c273f4517c3af5d0e5377550fe2020a6afc48c7b8ced81', 1, 'b930b8ae87bcaf8301c4a8747d5cc9b7');

SET FOREIGN_KEY_CHECKS=1;
COMMIT;