<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('users', 'users/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'users',
        ))->config_set('route_dash', 'page');

Route::set('user', 'user/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'user',
        ));

