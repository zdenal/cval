Welcome to <?php echo Cval::instance()->location()->name() ?>.

We have created an account for you.
You can log in at <?php echo $login_url ?> using these credentials:

Email address:	<?php echo $user->username."\n" ?>
Password:		<?php echo $password."\n" ?>

Please update your password in your account settings as soon as possible.

<?php echo View::factory('cval/email/footer/text')->include_view($thiss) ?>