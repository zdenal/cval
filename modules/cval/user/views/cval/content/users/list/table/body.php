<?php foreach ($users as $user): ?>
<tr class="<?php echo $user->html_class() ?>">
	<td class="Cval-column-listcheck"><input type="checkbox" class="Cval-list-checkbox"<?php echo ($logged_user->id() == $user->id()) ? ' disabled="disabled"' : '' ?>/></td>
	<td class="Cval-column-label">
		<div><?php echo $user->label ?></div>
	</td>
	<td class="Cval-column-email">
		<div><?php echo $user->email ?></div>
	</td>
	<td class="Cval-column-verified">
		<?php if ($user->is_verified()): ?>
		<span class="ui-state-highlight-text" title="<?php echo Cval::i18n('User is verified') ?>"><?php echo Cval::i18n('Yes') ?></span>
		<?php else: ?>
		<span title="<?php echo Cval::i18n('User is not verified') ?>"><?php echo Cval::i18n('No') ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-blocked">
		<?php if ($user->is_blocked()): ?>
		<span class="ui-state-error-text" title="<?php echo Cval::i18n('User is blocked') ?>"><?php echo Cval::i18n('Yes') ?></span>
		<?php else: ?>
		<span title="<?php echo Cval::i18n('User is not blocked') ?>"><?php echo Cval::i18n('No') ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-actions">
		<?php if ($logged_user->id() == $user->id()): ?>
		&nbsp;
		<?php else: ?>
		<span class="ui-icon ui-icon-trash Cval-helper-float-right Cval-trigger-remove" title="<?php echo Cval::i18n('Delete') ?>"></span>
		<span class="ui-icon ui-icon-pencil Cval-helper-float-right Cval-trigger-edit" title="<?php echo Cval::i18n('Modify') ?>"></span>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; ?>