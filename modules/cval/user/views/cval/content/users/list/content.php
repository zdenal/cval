<div class="container_16 full">
	
	<div class="Cval-list-users-message-nouser-content ui-helper-hidden">
		<?php echo View::factory('cval/content/users/list/message/nouser')->include_view($thiss) ?>
	</div>
	
	<div class="grid_16 full Cval-list-users-info-box Cval-helper-margin-top-50"></div>
	
	<div class="grid_16 full Cval-list-wrapper Cval-list-users-wrapper">
		<?php echo $table ?>
	</div>
		
	<div class="clear"></div>
</div>