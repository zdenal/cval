<table cellspacing="0" cellpadding="0" class="Cval-list Cval-list-users">
	<thead>
		<tr>
			<th class="Cval-column-listcheck"><div></div></th>
			<th class="Cval-column-label"><div><?php echo Cval::i18n('Name') ?></div></th>
			<th class="Cval-column-email"><div><?php echo Cval::i18n('Email address') ?></div></th>
			<th class="Cval-column-verified" title="<?php echo Cval::i18n('Verified') ?>"><div><?php echo Cval::i18n('V') ?></div></th>
			<th class="Cval-column-blocked" title="<?php echo Cval::i18n('Blocked') ?>"><div><?php echo Cval::i18n('B') ?></div></th>
			<th class="Cval-column-actions"><div></div></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>