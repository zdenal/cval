<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<div class="grid_16 Cval-menu-id-users Cval-helper-margin-top-50" style="text-align: right">
	<div class="Cval-helper-float-left Cval-user-list-controls ui-helper-hidden">
		<span class="Cval-selectall"></span>
		<span class="Cval-paginator"></span>
	</div>
	<div class="Cval-helper-float-right">
		<span class="Cval-user-controls">
			<span class="Cval-user-add"><?php echo Cval::i18n('Add user') ?></span>
			<span class="Cval-user-remove-more"><?php echo Cval::i18n('Remove user') ?></span>
		</span>
	</div>
	<div class="grid_5 Cval-helper-float-right">
		<?php echo View::factory('cval/util/search/input'); ?>
	</div>
</div>
<div class="clear"></div>

<div class="Cval-page-id-users-content grid_16">
	<?php echo View::factory('cval/content/users/list/content')->include_view($thiss); ?>
</div>

<div class="clear"></div>