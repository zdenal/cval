<?php defined('SYSPATH') or die ('No direct script access.');
/**
 * Model_Cval_User
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Cval_User extends Model_Auth_Cval_User
{
	
	public static function initialize(Jelly_Meta $meta)
    {
		parent::initialize($meta);
		
		$meta->fields(array(
			'user_profile' => new Field_HasOne
		));
    }
	
	/**
	 * @var	array	Array of Contact_Group instances
	 */
	protected $_contact_groups;
	
	/**
	 * @var	array	Array of Schedule_Group instances
	 */
	protected $_schedule_groups;
	
	/**
	 * @var	Jelly_Collection		contains Model_Location instances
	 */
	protected $_locations;
	
	/**
	 * Verify user, and adds needed roles for it. Then create its profile.
	 * 
	 * @return	$this
	 */
	public function verify()
	{	
		try
		{
			Database::instance()->transaction_start();
			
			// Run standard verification
			parent::verify();
		
			// And make sure it has profile
			if ( ! $this->has_profile())
			{
				// If no, create one
				$this->create_profile();
			}
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			throw $e;
		}
		
		return $this;
	}
	
	/**
	 * Check if user has profile created. If no it can be used by agent, etc.
	 * 
	 * @return	bool
	 */
	public function has_profile()
	{
		return $this->user_profile->loaded();
	}
	
	/**
	 * Creates user profile
	 * 
	 * @return	$this
	 */
	public function create_profile()
	{
		if ($this->has_profile())
		{
			return $this;
		}
		
		try
		{
			Database::instance()->transaction_start();
			
			$user_profile = Jelly::factory('user_profile')
				->set(array(
					'user' => $this
				))->save();

			// Reload this object, so $this->has_profile() is valid
			$this->reload();
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			
			throw $e;
		}
		
		return $this;
	}
	
	public function agent()
	{
		return $this->user_profile()->shared_profile()->agent();
	}
	
	/**
	 * Returns local user profile object
	 * 
	 * @return Model_User_Profile 
	 */
	public function user_profile()
	{
		return $this->user_profile;
	}
	
	/**
	 * Returns translation of a string using this user language.
	 *
	 * @param   string  text to translate
	 * @param   array   values to replace in the translated text
	 * @return  string
	 */
	public static function i18n($string, array $values = NULL)
	{
		return Cval::i18n($string, $values, $this->user_profile()->lang);
	}
	
	/**
	 * Get all events. Default as timestamps.
	 * 
	 * @param	timestamp	start of interval
	 * @param	timestamp	end of interval
	 * @param	bool		TRUE return events as data or object
	 * @return	array		array of events
	 */
	public function events($start = NULL, $end = NULL, $data = TRUE)
	{
		$events = array();
		
		$calendars = Jelly::select('calendar')
			->where('user', '=', $this->id())
			->execute();
		
		foreach ($calendars as $calendar)
		{
			foreach ($calendar->events($start, $end, $data) as $event)
			{
				$events[] = $event;
			}
		}
		
		return $events;
	}
	
	/**
	 * Get cval events. Default as timestamps.
	 * 
	 * @param	timestamp	start of interval
	 * @param	timestamp	end of interval
	 * @return	array		array of events
	 */
	public function cval_events($start = NULL, $end = NULL, $exclude = NULL)
	{
		$events = array();
		
		$event_color = Cval_Calendar_Color::color('cval_event', TRUE);
		$draft_color = Cval_Calendar_Color::color('cval_draft', TRUE);
		
		$schedules = Jelly::select('schedule')
			->filter_owner($this)
			->filter_cval_events($start, $end);
		
		// Exclude some events if set
		if ( ! empty($exclude))
		{
			if ( ! is_array($exclude))
			{
				$exclude = array($exclude);
			}
			$schedules->where(':primary_key', 'NOT IN', $exclude);
		}
			
		$schedules = $schedules->execute();
		
		foreach ($schedules as $schedule)
		{
			$event_data = array(
				'idmpk' => 'Cval__Schedule__'.$schedule->id(),
				'start' => $schedule->start,
				'end' => $schedule->end,
				'title' => $schedule->label,
				'length' => $schedule->length,
				'color' => $schedule->is_draft() ? $draft_color : $event_color
			);
			$events[] = $event_data;
		}
		
		return $events;
	}
	
	/**
	 * Checks if user has free time in given interval.
	 * 
	 * @param	timestamp	start of interval
	 * @param	timestamp	end of interval
	 * @return	bool		FALSE when user has at least one event in interval
	 */
	public function has_free_time($start, $end)
	{
		$calendars = Jelly::select('calendar')
			->where('user', '=', $this->id())
			->execute();
		
		foreach ($calendars as $calendar)
		{
			if ( ! $calendar->has_free_time($start, $end))
			{
				return FALSE;
			}
		}
		
		return TRUE;
	}
	
	/**
	 * Returns all contact groups instances. 
	 * Also adds favorite and all groups.
	 * 
	 * @param	array		optional array of ids to exclude
	 * @return	array		array of Contact_Group instances					
	 */
	public function contact_groups(array $exclude = array())
	{
		if ($this->_contact_groups === NULL)
		{
			$items = Contact_Group_System::instances();

			$groups = Jelly::select('contact_group')
				->where('user', '=', $this->id())
				->order_by('label')
				->execute();

			foreach ($groups as $group)
			{
				$items[$group->id()] = Contact_Group::factory($group);
			}
			
			$this->_contact_groups = $items;
		}
		
		$contact_groups = array();
		
		foreach ($this->_contact_groups as $id => $contact_group)
		{
			if (in_array($id, $exclude))
			{
				continue;
			}
			
			$contact_groups[$id] = $contact_group;
		}
		
		return $contact_groups;
	}
	
	/**
	 * Returns all contact groups choices, usable in html select. 
	 * Also adds favorites and all groups.
	 * 
	 * @param	array		optional array of ids to exclude
	 * @return	array		array of contact groups id => label
	 * @uses	$this::contact_groups()
	 */
	public function contact_groups_choices(array $exclude = array())
	{
		$choices = array();
		
		foreach ($this->contact_groups($exclude) as $id => $contact_group)
		{
			$choices[$id] = $contact_group->name();
		}
		
		return $choices;
	}
	
	/**
	 * Returns all schedule groups instances. 
	 * Also adds favorite and all groups.
	 * 
	 * @param	array		optional array of ids to exclude
	 * @return	array		array of Schedule_Group instances					
	 */
	public function schedule_groups(array $exclude = array())
	{
		if ($this->_schedule_groups === NULL)
		{
			$items = Schedule_Group_System::instances();

			$groups = Jelly::select('schedule_group')
				->where('user', '=', $this->id())
				->order_by('label')
				->execute();

			foreach ($groups as $group)
			{
				$items[$group->id()] = Schedule_Group::factory($group);
			}
			
			$this->_schedule_groups = $items;
		}
		
		$schedule_groups = array();
		
		foreach ($this->_schedule_groups as $id => $schedule_group)
		{
			if (in_array($id, $exclude))
			{
				continue;
			}
			
			$schedule_groups[$id] = $schedule_group;
		}
		
		return $schedule_groups;
	}
	
	/**
	 * Returns all schedule groups choices, usable in html select. 
	 * Also adds favorites and all groups.
	 * 
	 * @param	array		optional array of ids to exclude
	 * @return	array		array of schedule groups id => label
	 * @uses	$this::schedule_groups()
	 */
	public function schedule_groups_choices(array $exclude = array())
	{
		$choices = array();
		
		foreach ($this->schedule_groups($exclude) as $id => $schedule_group)
		{
			$choices[$id] = $schedule_group->name();
		}
		
		return $choices;
	}
	
	/**
	 * Returns all schedule groups choices, usable in html select.
	 * Do not add groups which are not assignable.
	 * 
	 * @param	array		optional array of ids to exclude
	 * @return	array		array of schedule groups id => label
	 * @uses	$this::schedule_groups()
	 */
	public function schedule_groups_choices_assignable(array $exclude = array())
	{
		$choices = array();
		
		foreach ($this->schedule_groups($exclude) as $id => $schedule_group)
		{
			if ($schedule_group->assignable())
			{
				$choices[$id] = $schedule_group->name();
			}
		}
		
		return $choices;
	}
	
	/**
	 * Returns all locations collection. 
	 * 
	 * @return	Jelly_Collection		contains Model_Location instances					
	 */
	public function locations()
	{
		if ($this->_locations === NULL)
		{
			$locations = Jelly::select('location')
				->where('is_paired', '=', TRUE)
				->where('local_block', '!=', TRUE)
				->where('remote_block', '!=', TRUE)
				->order_by('is_remote')
				->order_by('label')
				->execute();
			
			$this->_locations = $locations;
		}
		
		return $this->_locations;
	}
	
	/**
	 * Returns all locations choices, usable in html select.
	 * 
	 * @param	bool		separate remote locations with optgroup
	 * @return	array		array of locations id => label
	 * @uses	$this::locations()
	 */
	public function locations_choices($remote_optgroup = TRUE)
	{
		$choices = array();
		$remote_choices = array();
		
		foreach ($this->locations() as $location)
		{
			if ($remote_optgroup AND $location->is_remote())
			{
				$remote_choices[$location->id()] = $location->name();
			}
			else
			{
				$choices[$location->id()] = $location->name();
			}
		}
		
		if ($remote_optgroup AND count($remote_choices))
		{
			$choices[Cval::i18n('Remote')] = $remote_choices;
		}
		
		return $choices;
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-user-id-'.$this->id();
	}
	
	/**
	 * Returns last schedule update timestamp.
	 * 
	 * @return	int		Timestamp of last schedule update or 0 when no updates yet
	 */
	public function last_updated_schedule_time()
	{
		if ( ! $this->last_updated_schedule_time)
		{
			return 0;
		}
		
		return $this->last_updated_schedule_time;
	}
	
	/**
	 * Checks if user is enabled, i.e. can log in application.
	 * 
	 * @param	bool	TRUE for checking also if is verified
	 */
	public function enabled($verified = FALSE)
	{
		$enabled = ($this->loaded() AND ! $this->blocked);
		
		if ($verified)
		{
			$enabled = ($enabled AND $this->is_verified());
		}
		return $enabled;
	}
	
	/**
	 * Blocks/Unblocks user.
	 * 
	 * @param	boolean		TRUE for block, FALSE for unblock
	 * @return  $this
	 */
	public function block($positive = TRUE)
	{
		$positive = (bool) $positive;
		
		try
		{
			Database::instance()->transaction_start();
			
			$this->blocked = $positive;
			$this->save();
				
			// We already have user profile, so mark it as blocked also
			if ($this->user_profile()->loaded())
			{
				$this->user_profile()->shared_profile()->block($positive);
			}
			
			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();
			throw $e;
		}
		
		return $this;
	}
	
	protected function _before_delete()
	{
		parent::_before_delete();
		
		// We already have user profile, so only mark user as deleted
		if ($this->user_profile()->loaded())
		{
			// Delete shared user profile, it is only marked as deleted
			$this->user_profile()->shared_profile()->delete();
		}
	}
	
} // End Model_Cval_User