<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Model_User_Profile
 * @package Cval
 * @author	zdennal
 */
class Core_Model_User_Profile extends Jelly_Model
{

	/**
	 * @var	array	Array of Contact_Group instances
	 */
	protected $_contact_groups;
	
	/**
	 * @var	array	Array of Schedule_Group instances
	 */
	protected $_schedule_groups;
	
	public static function initialize(Jelly_Meta $meta)
    {	
		$meta->fields(array(
			'id' => new Field_Primary(array(
				'autoincrement' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
				)
			)),
			'user' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL,
				)
			)),
			'user_profile_shared' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'theme' => new Field_Enum(array(
				'default' => Kohana::config('cval.defaults.theme'),
				'choices' => Cval::themes_options(),
				'rules' => array(
					'not_empty' => NULL,
				)
			)),
			'icon_text' => new Field_Boolean(array(
				'default' => Kohana::config('cval.defaults.icon_text')
			)),
			'lang' => new Field_Enum(array(
				'default' => Kohana::config('cval.defaults.lang'),
				'choices' => I18n::enabled_langs(),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(2)
				)
			)),
			'timezone' => new Field_Enum(array(
				'default' => Kohana::config('cval.defaults.timezone'),
				'choices' => DateTimeZone::listIdentifiers(),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(50)
				)
			)),
			'is_timezone_set' => new Field_Boolean,
			'date_month_second' => new Field_Boolean(array(
				'default' => Kohana::config('cval.defaults.date_month_second'),
				'label_true' => '31/12/2012',
				'label_false' => '12/31/2012'
			)),
			'time_24_hour' => new Field_Boolean(array(
				'default' => Kohana::config('cval.defaults.time_24_hour'),
				'label_true' => '13:00',
				'label_false' => '1:00pm'
			)),
			'first_day' => new Field_Enum(array(
				'integer' => TRUE,
				'choices' => array(
					'1' => 'Monday',
					'2' => 'Tuesday',
					'3' => 'Wednesday',
					'4' => 'Thursday',
					'5' => 'Friday',
					'6' => 'Saturday',
					'0' => 'Sunday',
				),
				'default' => Kohana::config('cval.defaults.first_day'),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(1)
				)
			)),
			'email_sharing' => new Field_Enum(array(
				'default' => Kohana::config('cval.defaults.email_sharing'),
				'choices' => array(
					10 => 'None',
					50 => 'Local',
					90 => 'Global'
				),
				'integer' => TRUE
			)),
			'schedule_email_on_inbound' => new Field_Boolean(array(
				'default' => Kohana::config('cval.defaults.schedule_email_on_inbound'),
			)),
			'schedule_plan_min_length' => new Field_Integer(array(
				'default' => Kohana::config('cval.defaults.schedule_plan_min_length'),
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			'schedule_plan_min_length_units' => new Field_Enum(array(
				'default' => Kohana::config('cval.defaults.schedule_plan_min_length_units'),
				'choices' => array(
					'm' => 'minutes',
					'h' => 'hours'
				),
				'rules' => array(
					'not_empty' => NULL
				)
			)),
		));
    }
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// Make sure profile is created
		$this->_create_shared_profile();
		
		if ($loaded)
		{
			// Update timezone set token on first timezone change
			if ($this->changed('timezone'))
			{
				$this->is_timezone_set = TRUE;
			}
		}
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('id')->rules['not_empty']);
		}
	}
	
	protected function _after_save($loaded = NULL)
	{
		// Propagate changing of email sharing
		if ($this->last_changed('email_sharing'))
		{
			$this->user_profile_shared = $this->email_sharing < 50
					? NULL
					: $this->user->email;
			$this->user_profile_shared->save();
		}
		
		parent::_after_save($loaded);
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('id')->rules['not_empty'] = NULL;
		}
		
		// Create IDMPK
		$this->id = $this->_create_id($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_id($loaded)
	{
		if ($loaded AND ! empty($this->id))
		{
			return $this->id;
		}
		
		return $this->user->id();
	}
	
	protected function _create_shared_profile()
	{
		if ($this->user_profile_shared->loaded())
		{
			return $this;
		}
		
		if ( ! $this->user->loaded())
		{
			throw new Kohana_Exception('User profile is not associated with user object. Shared user profile can\'t be created.');
		}
		
		$hash = md5($this->user->email.uniqid());
		
		$profile = Jelly::factory('user_profile_shared')
			->set(array(
				'label' => $this->user->name(),
				'location' => Model_Location::load(),
				'hash' => $hash
			))->save();
		
		$this->user_profile_shared = $profile;
		
		return $this;
	}
	
	/**
	 * Delete method is not supported and throws exception every time.
	 * Call delete method on associated user object instead.
	 * 
	 * @throw	Kohana_Exception
	 */
	public function delete($key = NULL)
	{
		throw new Kohana_Exception('User profile can not be deleted. Call delete method on associated user object instead.');
	}
	
	/**
	 * Shared user profile object
	 * 
	 * @return	Model_User_Profile_Shared
	 */
	public function shared_profile()
	{
		return $this->user_profile_shared;
	}
	
} // End Model_Cval_User