<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Model_User_Profile_Shared
 * @package Cval
 * @author	zdennal
 */
class Core_Model_User_Profile_Shared extends Jelly_Model
{
	
	public static function initialize(Jelly_Meta $meta)
    {
		$meta->name_key('label')
             ->sorting(array('label' => 'ASC'));

		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(40),
				)
			)),
			'location' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			/**
			 * Automatically created before save.
			 */
			'id' => new Field_Integer(array(
				'rules' => array(
					'not_empty' => NULL,
					'min_not_equal' => array(0)
				)
			)),
			/**
			 * Connection to local only user profile.
			 * Applies only for local users
			 */
			'user_profile' => new Field_HasOne,
			'is_remote' => new Field_Boolean(array(
				'default' => 0
			)),
			'hash' => new Field_String(array(
				'callbacks' => array(
					'matches' => array('Model_User_Profile_Shared', '_check_hash_unique')
				),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(32),
					'min_length' => array(32),
				)
			)),
			'deleted' => new Field_Boolean(array(
				'default' => 0
			)),
			'blocked' => new Field_Boolean(array(
				'default' => 0
			)),
        ));
		
        Behavior_Labelable::initialize($meta, array(
			'label' => 'name',
			'rules' => array(
				'not_empty' => NULL,
				'max_length' => array(255)
			)
		));
		
		$meta->fields(array(
			'email' => new Field_Email(array(
				'label' => 'email address',
				'rules' => array(
					'max_length' => array(255),
					'min_length' => array(3),
				)
			)),
		));

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }
	
	/**
	 * Validate callback wrapper for checking hash unique for location
	 * @param	Validate $array
	 * @param	string   $field
	 * @param	array		array of params with model
	 * @return	void
	 */
	public static function _check_hash_unique(Validate $array, $field, array $params = array())
	{
		$model = $params[0];
		
		$duplicated = Jelly::select($model->meta()->model())
			->where('location', '=', $model->location->id())
			->where('hash', '=', $array[$field]);
		
		if ($model->loaded())
		{
			$duplicated->where(':primary_key', '!=', $model->id());
		}
		
		$duplicated = $duplicated->load();
		
		if ($duplicated->loaded())
		{
			$array->error($field, 'unique');
		}
	}
	
	/**
	 * Creates an instance of Model_User_Profile_Shared from serialized data.
	 *
	 * @param	array	serialized data
	 * @param	bool	TRUE for throwing exception if profile not exists
	 * @param	bool	TRUE for throwing exception if profile location not exists
	 * @param	bool	TRUE for throwing exception if local user for profile not exists
	 * @return	Model_User_Profile_Shared
	 */
	public static function unserialize($data, $exception = FALSE, $location_exception = TRUE, $local_exception = TRUE)
	{
		if (empty($data) OR ! is_array($data))
		{
			throw Cval_Unserialize_Exception::create('Model_User_Profile_Shared', 'Parameter "data" must be not empty array');
		}
		$hash = Arr::get($data, 'hash');
		if (empty($hash))
		{
			throw Cval_Unserialize_Exception::create('Model_User_Profile_Shared', 'Parameter "data" must contain "hash" value');
		}
		
		$location = Model_Location::unserialize(Arr::get($data, 'location'), $location_exception);
		
		$object = Jelly::select('user_profile_shared')
			->where('location', '=', $location->id())
			->where('hash', '=', $hash)
			->load();
		
		if ( ! $object->loaded() AND $exception)
		{
			throw Model_Not_Found_Exception::create($object, array(
				'hash' => $hash,
				'location_url' => $location->url
			));
		}
		
		if ( ! $location->is_remote() 
				AND ! $object->deleted 
				AND ! $object->user_profile->user->loaded() 
				AND $local_exception)
		{
			throw Model_Not_Found_Exception::create($object, array(
				'hash' => Arr::get($data, 'hash'),
			));
		}
		
		$object->set(Arr::extract($data, array(
			'hash', 'deleted', 'blocked', 'label', 'email'
		)))->set(array(
			'location' => $location
		));
		
		// Set objects as retrieved, otherwise if not loaded, they are discarded
		$object->_retrieved += array(
			'location' => $location
		);
		
		return $object;
	}
	
	/**
	 * @var	User_Agent	Instance of user agent 
	 */
	protected $_agent;
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// Set is remote value
		$this->is_remote = $this->location->is_remote;
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
			unset($this->meta()->fields('id')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
			$this->meta()->fields('id')->rules['not_empty'] = NULL;
		}
		
		// Generate ID
		$this->id = $this->_create_id($loaded);
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_id($loaded)
	{
		if ($loaded AND ! empty($this->id))
		{
			return $this->id;
		}
		
		$last = Jelly::select($this)
				->where('location', '=', $this->location->id())
				->order_by('id', 'DESC')
				->load();
		
		$id = 1;
		
		if ($last->loaded())
		{
			$id = $last->id + 1;
		}
		
		return $id;
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		if (empty($this->id))
		{
			throw new Kohana_Exception('Model `:model` needs to have field `id` set', array(
				':model' => $this->meta()->model
			));
		}
	
		return implode('__', array($this->location->id(), $this->id));
	}
	
	protected function _before_delete()
	{
		parent::_before_delete();
		
		// Shared user profiles can not be deleted
		$this->deleted = TRUE;
		$this->save();

		return FALSE;
	}
	
	/**
	 * Restores deleted object
	 * 
	 * @return $this
	 */
	public function restore()
	{
		$this->deleted = FALSE;
		$this->save();
		
		return $this;
	}
	
	/**
	 * Blocks/Unblocks user.
	 * 
	 * @param	boolean		TRUE for block, FALSE for unblock
	 * @return  $this
	 */
	public function block($positive = TRUE)
	{
		$positive = (bool) $positive;
		
		$this->blocked = $positive;
		$this->save();
		
		return $this;
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-user_profile-id-'.$this->id();
	}
	
	/**
	 * Returns unique id for profile in scope of this location.
	 * 
	 * @return	string
	 * @throws	Kohana_Exception	when profile location is not loaded
	 */
	public function unique_id()
	{
		if ( ! $this->location->loaded())
		{
			throw new Kohana_Exception('User profile has unknown location. Unique ID can\'t be generated.');
		}
		
		return implode('__', array($this->location->id(), $this->hash));
	}
	
	/**
	 * Updates data for this shared profile. It is used for creating or updating
	 * shared profile for remote location.
	 * 
	 * @param	mixed	array of fields or Model_User_Profile_Shared object
	 * @param	bool	TRUE for saving this object after data update
	 * @return	$this
	 */
	public function update_data($source_data, $save = TRUE)
	{
		// Convert data to array if it is object
		if ($source_data instanceof Model_User_Profile_Shared)
		{
			$source_data = $source_data->as_array_internal();
		}
		
		// Update only needed data
		$this->set(Arr::extract($source_data, array(
			'location',
			'hash',
			'deleted', 
			'blocked',
			'label',
			'email'
		)));
		
		// Save on demand
		if ($save)
		{
			$this->save();
		}
		
		return $this;
	}
	
	/**
	 * Returns friendly name, i.e. when profile is used for actual logged
	 * user, name is 'me'.
	 * 
	 * @return	string 
	 */
	public function friendly_name()
	{
		if (Cval::instance()->user_profile_shared()->id() == $this->id())
		{
			return Cval::i18n('me');
		}
		return $this->name();
	}
	
	/**
	 * Returns serialized data for remote calls.
	 *
	 * @return	array	serialized data
	 */
	public function serialize($include_location = TRUE)
	{
		$data = $this->as_array_internal(array(
			'hash', 'deleted', 'blocked', 'label', 'email'
		));
		if ($include_location)
		{
			$data['location'] = $this->location->serialize();
		}
		
		return $data;
	}
	
	/**
	 * Returns agent instance for this profile.
	 *
	 * @return	User_Agent 
	 */
	public function agent()
	{
		if ($this->_agent === NULL)
		{
			$this->_agent = User_Agent::factory($this);
		}
		return $this->_agent;
	}
	
	/**
	 * Check if profile is remote
	 * 
	 * @return	bool 
	 */
	public function is_remote()
	{
		return (bool) $this->is_remote;
	}
	
}