<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_User_Pair
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_User_Pair extends Jelly_Model 
{
	
    public static function initialize(Jelly_Meta $meta) 
	{
		$meta->fields(array(
			'idmpk' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(61),
				)
			)),
			'user' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'user_profile_shared' => new Field_BelongsTo(array(
				'rules' => array(
					'not_empty' => NULL
				)
			)),
			'paired' => new Field_Boolean(array(
				'default' => 0
			)),
			'pairing_key' => new Field_String(array(
				'rules' => array(
					'max_length' => array(32),
				)
			))
        ));

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// On create
		if ( ! $loaded)
		{
			// Disable required rule for fields which are created automatically 
			// after validation
			unset($this->meta()->fields('idmpk')->rules['not_empty']);
		}
	}
	
	protected function _after_validate_save($loaded = NULL)
	{
		// On create
		if ( ! $loaded)
		{
			// Enable required rule for fields which are created automatically 
			// after validation
			$this->meta()->fields('idmpk')->rules['not_empty'] = NULL;
		}
		
		// Create IDMPK
		$this->idmpk = $this->_create_idmpk($loaded);
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _create_idmpk($loaded)
	{
		if ($loaded AND ! empty($this->idmpk))
		{
			return $this->idmpk;
		}
		
		return implode('__', array($this->user->id(), $this->user_profile_shared->id()));
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-user_profile_pair-id-'.$this->id();
	}

}
