<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Builder_User_Profile_Shared
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Builder_User_Profile_Shared extends Jelly_Builder
{	
	/**
	 * Filters only enabled profiles.
	 *
	 * @return	$this 
	 */
	public function filter_enabled()
	{
		return $this
			->where('deleted', '=', FALSE)
			->where('blocked', '=', FALSE);
	}
}
