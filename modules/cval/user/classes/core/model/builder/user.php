<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Builder_User_Profile_Shared
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Builder_User extends Jelly_Builder
{	
	/**
	 * Filters only enabled users.
	 *
	 * @return	$this 
	 */
	public function filter_enabled()
	{
		return $this
			->where('blocked', '=', FALSE);
	}
	
	/**
	 * Searches this model from one string.
	 * 
	 * @param	string	filter value
	 * @return	$this
	 */
	public function global_search($value)
	{
		$value = trim($value);
		if (strlen($value))
		{
			$this->where_open()
					->where('label', 'LIKE', '%'.$value.'%')
					->or_where('email', 'LIKE', '%'.$value.'%')
					->where_close();
		}

		return $this;
	}
}
