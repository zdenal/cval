<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Calendars List Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Users_List extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/users/list/content')
				->bind('table', $table)
				->bind('users_count', $users_count);
		
		$action_table = Request::factory(Route::get('user')->uri(array(
			'controller' => 'list_table'
		)))->execute();
		
		$table = $action_table->response['html'];
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}

}

