<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Pairing request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_User_Pairing_Request extends Controller_User_Request
{
	
	/**
	 * @var bool	If FALSE request allows calls only from paired users 
	 */
	protected $_allow_unpaired = TRUE;

	public function action_index()
	{
		$response = User_Pairing::response($this->_user, $this->data());
		$this->request->response($response);
	}

} // End Core_Controller_User_Pairing_Request
