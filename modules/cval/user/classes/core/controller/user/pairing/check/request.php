<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Pairing check request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_User_Pairing_Check_Request extends Controller_User_Request
{
	
	/**
	 * @var bool	If FALSE request allows calls only from paired users 
	 */
	protected $_allow_unpaired = TRUE;
	
	public function action_index()
	{
		$data = $this->data();
		
		// Check if pairing allowed
		if ( ! User_Pairing::allow())
		{
			$this->_response_finish(3);
			return;
		}
		
		if ($this->_user->is_blocked(TRUE))
		{
			$this->_response_finish(3);
			return;
		}
		
		if ( ! $this->_user->loaded())
		{
			$this->_response_finish(5);
			return;
		}
		
		if ($this->_user->pairing_key != Arr::get($data, 'pairing_key'))
		{
			$this->_response_finish(5);
			return;
		}
		
		// Finally paired
		$this->_response_finish(1);
	}
	
	protected function _response_finish($code)
	{
		$this->request->response(array(
			'code' => $code,
			'user' => Model_User::load_local()->serialize()
		));
	}

} // End Core_Controller_User_Pairing_Check_Request
