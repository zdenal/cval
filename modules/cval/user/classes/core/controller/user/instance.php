<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_User_Instance
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_User_Instance extends Controller_Cval_Ajax {

	/**
	 * @var Model_User
	 */
	protected $_object;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'user';
	
	/**
	 * Load a specific ACL resource
	 */
	protected function _acl_load_resource()
	{	
		$id = Arr::get($_POST, 'id', 0);
		
		$this->_object = Jelly::select('user')
				->load($id);

		if ( ! $this->_object->loaded())
		{
			// TODO - allow to throw deny with classic ACL exception
			throw Model_Not_Found_Exception::create($this->_object->meta()->model(), $id);
		}

		$this->_acl_resource = $this->_object;
	}

}

