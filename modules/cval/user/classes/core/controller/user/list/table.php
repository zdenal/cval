<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_User_List
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_User_List_Table extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var	Pagination	Pagination object. It is set after _paginate method call. 
	 */
	protected $_pagination;
	
	public function action_index() 
	{
		$view_prefix = $this->request->internal_param('view_prefix', 'cval/content/users/list/table');
		$view = Arr::get($_POST, 'body_only') 
				? $view_prefix.'/body'
				: $view_prefix;
		
		$html = View::factory($view)
				->set('logged_user', $this->cval->user())
				->bind('users', $users)
				->bind('count', $count);
		
		// Create basic builder
		$users = Jelly::select('user');
		
		$users = $this->_search($users);
		$users = $this->_paginate($users)
			->order_by('label')
			->order_by('username')
			->execute();
		
		$count = $users->count();
		$js_data = array();
		
		foreach ($users as $user)
		{
			// Set js data
			$js_data[$user->html_class(TRUE)] 
					= Cval_REST::instance()->model($user)->render();
		}
		
		$this->request->response = array(
			'html' => $html->render(),
			'count' => $count,
			'js' => array(
				'data' => $js_data
			),
			'pagination' => $this->_pagination ? $this->_pagination->render_array() : NULL
		);
	}
	
	/**
	 * Returns filtered builder.
	 * 
	 * @param	Model_Builder_Contact	Builder to filter
	 * @return	Model_Builder_Contact
	 */
	protected function _search(Model_Builder_User $builder)
	{
		return $builder->global_search(Arr::path($_POST, 'search.global'));
	}
	
	/**
	 * Returns paginated builder.
	 * 
	 * @param	Jelly_Builder	Builder to paginate
	 * @return	Jelly_Builder
	 */
	protected function _paginate(Model_Builder_User $builder)
	{
		$this->_pagination = Pagination::factory(array(
			'total_items' => $builder->count(),
			'items_per_page' => 30,
			'current_page' => array(
				'page' => Arr::get($_POST, 'pagination', 1)
			),
			'auto_hide' => FALSE,
		));

		$builder->limit($this->_pagination->items_per_page)
			->offset($this->_pagination->offset);

		return $builder;
	}

}

