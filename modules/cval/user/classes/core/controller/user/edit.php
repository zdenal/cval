<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_User_Edit
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_User_Edit extends Controller_User_Instance {
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/user/edit')
				->set('csrf', Security::token(TRUE, 'cval/user/edit'))
				->bind('user', $user);
		
		$user = $this->_object;
		
		$this->request->response = array(
			'html' => $html->render(),
			'user' => Cval_REST::instance()->model($user)->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/user/edit', TRUE);
		
		$post = Security::xss_clean(Arr::extract_existed($_POST, array(
			'label',
			'email',
			'verified',
			'blocked'
		)));
		
		Database::instance()->transaction_start();
		
		$user = $this->_object
			->set($post)
			->save();
		
		if ($user->last_changed('verified'))
		{
			if ($user->verified)
			{
				$user->verify();
			}
			else
			{
				// Unverified is denied
				throw new Kohana_Exception('User can not be unverified!');
			}
		}
		if ($user->last_changed('blocked'))
		{
			$user->block($user->blocked);
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('User was modified!')),
			'user' => Cval_REST::instance()->model($user)->render()
		);
		
		Database::instance()->transaction_commit();
	}

}

