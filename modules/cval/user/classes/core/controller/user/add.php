<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_User_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_User_Add extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'user';
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/user/add')
				->set('csrf', Security::token(TRUE, 'cval/user/add'))
				->bind('user', $user);
		
		$user = Jelly::factory('user')
				->set(array(
					'verified' => TRUE
				));
		
		$this->request->response = array(
			'html' => $html->render(),
			'user' => Cval_REST::instance()->model($user)->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/user/add', TRUE);
		
		$post = Security::xss_clean(Arr::extract($_POST, array(
			'label',
			'email',
			'blocked'
		)));
		
		$user = Jelly::factory('user')
			->set($post);
		
		// User is always verified when created by admin
		$user->verified = TRUE;
		// Create password
		$password = Text::random('alnum', 6);
		$user->password = $user->password_confirm = $password;
		// Copy username from email
		$user->username = Arr::get($post, 'email');
		// Set email to required and username not
		$user->meta()->fields('email')->rules['not_empty'] = NULL;
		Arr::remove($user->meta()->fields('username')->rules, array('not_empty'));
		array_pop($user->meta()->fields('username')->callbacks);
		
		Database::instance()->transaction_start();
		
		// Try to save user
		$user->save();
		
		// Create verification id
		$user->verification_id = md5(uniqid());
		
		$needed_roles = array('cval');
		// Get user roles
		$roles = Jelly::select('role')
				->where('name', 'IN', $needed_roles)
				->execute();
		
		if ($roles->count() < count($needed_roles))
		{
			throw new Kohana_Exception('One of roles with names `:names` not found, user not created.', array(
				':names' => implode(', ', $needed_roles)
			));
		}
		
		// Add user roles to user
		$user->add('roles', $roles);
		$user->save(NULL, TRUE);
		
		// Verify user if should be
		if ($user->verified)
		{
			$user->verify();
		}
		// Block user if should be
		if ($user->blocked)
		{
			$user->block();
		}
		
		if ( ! $this->_send_registration_email($user, $password))
		{
			throw new Kohana_Exception('Registration email for new user with username `:username` to email address `:email` not send', array(
				':username' => $user->username,
				':email' => $user->email
			));
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('User was created!')),
			'user' => Cval_REST::instance()->model($user)->render()
		);
		
		Database::instance()->transaction_commit();
	}
	
	protected function _send_registration_email(Model_User $user, $password)
	{
		$params = array(
			'user' => $user,
			'password' => $password,
			'login_url' => Route::url_protocol('login')
		);

		return Email::send_template(array($user->email, $user->name()), 'admin/user/create', $params);
	}

}

