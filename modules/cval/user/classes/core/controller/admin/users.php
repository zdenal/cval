<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Admin Users Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Admin_Users extends Controller_Admin_Only {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Users',
		));
		
		$html = View::factory('cval/content/admin/users')
				->bind('table', $table);
		
		$action_table = Request::factory(Route::get('user')->uri(array(
			'controller' => 'list_table'
		)))->execute();
		
		$table = $action_table->response['html'];
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}
	
}

