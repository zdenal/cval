<?php defined('SYSPATH') OR die('No direct script access.');

class REST_Model_User_Profile_Shared extends Jelly_REST_Model 
{
	public function render(array $fields = array())
	{
		$data = parent::render($fields);

		$data['hash'] = $this->_jelly_model->hash;

		return $data;
	}
}

