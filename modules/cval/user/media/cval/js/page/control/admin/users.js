(function(Cval, $) {
	
	Cval_page_control_admin_users_object = {
		staticParams : {
			defaults : {
				userControlsSelector : '.Cval-user-controls',
				userAddSelector : '.Cval-user-add',
				userRemoveMoreSelector : '.Cval-user-remove-more',
				userSelectAllSelector : '.Cval-user-selectall',
				
				userEditSelector : '.Cval-trigger-edit',
				userPairSelector : '.Cval-trigger-pair',
				userInfoSelector : '.Cval-trigger-info',
				userRemoveSelector : '.Cval-trigger-remove',
				userListSelector : '.Cval-list-users-wrapper',
				userSelectorPrefix : '.Cval-model-user-id-',
				
				refreshOnShow : false
			},
			lastRefresh : {
				data : null
			},
			checkerEl : null,
			paginationEl : null,
			addButtonEl : null,
			removeButtonEl : null,
			searchInputEl : null,
			init : function(callback) {
				var thiss = this;

				// Needed resources
				var resources = [
					Cval.route('cval/media', {
						file : 'css/page/admin/users.css'
					})
				];

				Cval.resource.get(resources, function(){
					Cval.core.callback(callback);
				});	
			},
			_show : function() {
				this._super();
				this.refreshUserList();
			},
			_bindEvents : function() {
				this._super();
				var thiss = this;

				// Init add user button
				this.addButtonEl = this.getElement().find(this.defaults.userAddSelector)
					.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-person',
							secondary : 'ui-icon-plus'
						}
					})
					.click(function(){
						Cval.dialog.loadControl('user_add', function(){
							Cval.dialog.control.user_add.show();
						});
						return false;
					});
					
				// Init remove user button
				this.removeButtonEl = this.getElement().find(this.defaults.userRemoveMoreSelector)
					.Cval_button({
						text : false,
						icons : {
							secondary : 'ui-icon-trash'
						}
					})
					.click(function(){
						var selectedUsers = thiss.getSelectedUsersEls(),
							multipleData = [],
							multiple = true,
							data = null;

						$.each(selectedUsers, function(){
							var data = $(this).Cval_data();
							multipleData.push(data);
						});

						if (multipleData.length < 2) {
							multiple = false;
							data = multipleData.shift();
						} else {
							data = multipleData;
						}

						Cval.dialog.loadControl('user_remove', function(){
							Cval.dialog.control.user_remove.show(null, {
								data : data,
								multiple : multiple
							})
						});
						return false;
					});
					
				// Init select user button
				this.checkerEl = this.getElement().find(this.defaults.userSelectAllSelector)
					.Cval_button({
						text : false,
						icons : {
							secondary : 'ui-icon-check'
						}
					});
				
				this.getElement().find('.Cval-user-controls').buttonset();
				this.removeButtonEl.Cval_button('option', 'disabled', true);
				
				// Bind pair user
				this.getElement().find(this.defaults.userPairSelector).livequery(function(){
					$(this).click(function(){
						var data = thiss.getUserEl($(this)).Cval_data();

						Cval.dialog.loadControl('user_pair', function(){
							Cval.dialog.control.user_pair.show(null, {
								data : data
							})
						});
						return false;
					})
				});
				
				// Bind edit user
				this.getElement().find(this.defaults.userEditSelector).livequery(function(){
					$(this).click(function(){
						var data = thiss.getUserEl($(this)).Cval_data();

						Cval.dialog.loadControl('user_edit', function(){
							Cval.dialog.control.user_edit.show(null, {
								data : data
							})
						});
						return false;
					})
				});
				
				// Bind remove user
				this.getElement().find(this.defaults.userRemoveSelector).livequery(function(){
					$(this).click(function(){
						var data = thiss.getUserEl($(this)).Cval_data();

						Cval.dialog.loadControl('user_remove', function(){
							Cval.dialog.control.user_remove.show(null, {
								data : data
							})
						});
						return false;
					})
				});
				
				// Bind info user
				this.getElement().find(this.defaults.userInfoSelector).livequery(function(){
					$(this).click(function(){
						alert(Cval.todo);
						return false;
					})
				});
				
				// Hover effects for table rows
				this.getElement().find(this.defaults.userListSelector+' tbody tr').livequery(function(){
					$(this).hover(function(){
						$(this).addClass('Cval-hover');
					}, function(){
						$(this).removeClass('Cval-hover')
					});

					$(this).find('.Cval-list-checkbox').change(function(){
						thiss.listUpdate();
					});
				});

				this._initSearch();
				this._initListControls();
			},
			refreshUsers : function(callback) {
				this.refreshUserList(callback);
			},
			refreshUserList : function(callback, data) {
				var thiss = this;

				data = $.extend({}, {
					body_only : 1,
					search : thiss._searchData()
				}, data || {});

				Cval.ajax.callModal({
					route : 'user',
					routeParams : {
						controller : 'list_table'
					},
					data : data
				}, function(json) {
					thiss.lastRefresh = {
						data : data
					};
					
					thiss.updateUserList(json, false, callback);
				});
			},
			updateUserList : function(data, complete, callback) {
				var table = this.getElement().find(this.defaults.userListSelector);

				// Display, show no user message
				if (data.count > 0)
					this.noUserMessageHide();
				else
					this.noUserMessageShow();
				// Display/show table header
				table.toggle(data.count > 0);

				// Replace only body content
				if ( ! complete)
					table = table.find('tbody');

				table.html(data.html);
				// Inject js data
				Cval.ajax.data.inject(data);

				// Enable/Disable checker
				this.checkerEl.Cval_button_checker({
					checked : false,
					disabled : data.count < 1
				});
				// update pagination
				this.paginationEl.Cval_pagination('update', data.pagination);
				this.listUpdate();

				Cval.core.callback(callback);
			},
			getUserEl : function(el) {
				el = $(el);

				if ( ! el.is('tr'))
					el = el.parents('tr:first');

				return el;
			},
			getUserElFromId : function(id) {
				var selectorClass = this.defaults.userSelectorPrefix+id,
					el = $(this.getElement().find(this.defaults.userListSelector+' tbody tr'+selectorClass));

				return el;
			},
			noUserMessageShow : function() {
				var thiss = this,
					isSearchActive = this.isSearchActive(),
					messageElClass = 'Cval-list-users-message-nouser_'+(isSearchActive ? 'search' : 'default');

				if (thiss.getElement().find('.Cval-list-users-info-box .Cval-list-users-message-nouser.'+messageElClass).length)
					return;
				
				this.noUserMessageHide();

				var content = isSearchActive
						? Cval.i18n.get('No users found, try to modify your search phrase.')
						: thiss.getElement().find('.Cval-list-users-message-nouser-content').html();
						
				Cval.message.info(content, null, {
					wrapper : thiss.getElement().find('.Cval-list-users-info-box'),
					autoDestroy : false,
					closer : false,
					outerClass : 'Cval-list-users-message-nouser '+messageElClass
				});
			},
			noUserMessageHide : function() {
				var messageEl = this.getElement().find('.Cval-list-users-info-box .Cval-list-users-message-nouser');
				Cval.message.destroy(messageEl, true);
			},
			_initListControls : function() {
				var thiss = this,
					listControls = this.getElement()
						.find('.Cval-user-list-controls');

				this.checkerEl = listControls.find('.Cval-selectall')
					.Cval_button_checker({
						disabled : true,
						clickCallback : function(check, button) {
							thiss.getElement().find(thiss.defaults.userListSelector)
								.find('tbody tr .Cval-list-checkbox:enabled')
								.attr('checked', check);
							thiss.listUpdate();
						}
					})

				this.paginationEl = listControls.find('.Cval-paginator')
					.Cval_pagination({
						clickCallback : function(page) {
							thiss.paginateResults(page);
						}
					});

				listControls.show();
			},
			paginateResults : function(page) {
				this.refreshUserList(null, $.extend({}, this.lastRefresh.data || {}, {
					pagination : page
				}));
			},
			_initSearch : function() {
				var thiss = this,
					form = this.getElement().find('form.Cval-form-search');

				this.searchInputEl = form.find('input[name=search]');
				form.submit(function(e){
					e.preventDefault();
					thiss.refreshUserList();
					return false;
				});
			},
			_searchData : function() {
				return {
					global : this.searchInputEl.val().trim()
				};
			},
			isSearchActive : function() {
				return this.lastRefresh.data.search.global.length > 0;
			},
			listUpdate : function() {
				var thiss = this;

				var checkedCount = thiss.getSelectedUsersEls().length;

				this.removeButtonEl.Cval_button('option', 'disabled', checkedCount < 1);
			},
			getSelectedUsersEls : function() {
				return this.getElement().find(this.defaults.userListSelector)
					.find('tbody tr')
					.filter(function(){
						return $(this).find('.Cval-list-checkbox').is(':checked')
					});
			}
		},
		params : {}
	};
		
	$.Class.extend('Cval.page.control.admin_users', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('admin_def', function(){
					
					Cval.page.control.admin_def.extend('Cval.page.control.admin_users', 
						Cval_page_control_admin_users_object.staticParams,
						Cval_page_control_admin_users_object.params
					);
					
					Cval.page.control.admin_users.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);