(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.user_edit', {
		defaults : {
			dialog : {
				title : Cval.i18n.get('Edit user'),
				CvalHeight : false,
				destroyOnClose : true,
				width : 400
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'user',
				routeParams : {
					controller : 'edit'
				}
			},
			refreshOnShow : true
		},
		wizardEl : null,
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jWizard.base.css'
			}),
			Cval.route('cval/media', {
				file : 'css/jquery/ui/jWizard.css'
			}),
			Cval.route('cval/media', {
				file : 'js/jquery/ui/jWizard.js'
			})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_beforeHide : function(dialog, event, ui) {
			this._super.apply(this, arguments);
			
			Cval.core.callback(this.data(this.getElement()).options.hideCallback || function(){
				Cval.page.getControl('admin_users').refreshUsers();
			});
		},
		_bindEvents : function() {
			var thiss = this;
			
			this.wizardEl = this.getElement().find('.jWizard');
			
			this.wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: false,
					cancelText: Cval.i18n.get('Cancel'),
					finishText: Cval.i18n.get('Save')
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.wizardNext(event, ui);
				},
				finish : function(event, ui) {
					thiss.wizardFinish(event, ui);
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
			});
			
			this._initWizard();
		},
		getLoadConfig : function() {
			return $.extend({}, this._super(), {
				data : {
					id : this.data(this.getElement()).options.data.id
				}
			});
		},
		_initWizard : function() {
			var thiss = this;
			
			this.wizardEl.find('.Cval-dialog-user-edit form').submit(function(e){
				e.preventDefault();
				// On form submit go to nextStep
				thiss.wizardEl.Cval_jWizard('nextStep');
				return false;
			})
		},
		wizardNext : function(event, ui) {},
		wizardFinish : function() {
			this._wizardFinish();
		},
		_wizardFinish : function() {
			var thiss = this,
				data = this._finishAjaxData();
				
			$.Console.Debug(data)

			Cval.ajax.callModal({
				route : 'user',
				routeParams : {
					controller : 'edit',
					action : 'submit'
				},
				data: data
			}, function(json) {
				thiss._wizardFinishSuccess(json);
			})

			return;
		},
		_wizardFinishSuccess : function(json) {
			if (json.messages !== undefined)
			{
				$.each(json.messages, function(){
					Cval.message.fromOptions(this);
				});
			}
			this.hide();
		},
		/**
		 * Returns data from dialog you want send to server after save is invoked.
		 * Should be overidden in subclasses.
		 */
		_finishAjaxData : function() {
			var formData = this.wizardEl.find('.Cval-dialog-user-edit form').serializeJSON();
			formData.id = this.data(this.getElement()).options.data.id;
			return formData;
		}
	}, {});

})(Cval, jQuery)