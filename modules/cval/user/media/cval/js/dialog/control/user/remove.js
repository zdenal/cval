// KEXT[on]
(function(Cval, $) {

	Cval_dialog_control_user_remove_object = {
		staticParams : {
			defaults : {
				dialog : {
					title : Cval.i18n.get('Remove the user'),
					CvalHeight : false,
					destroyOnClose : true,
					width: 500
				},
				mainClass : 'alpha omega Cval-dialog-no-padding',
				loadRemoteContent : false,
				refreshOnShow : false,
				buttons : {
					confirm : $('<span />').text(Cval.i18n.get('Remove')),
					cancel : $('<span />').text(Cval.i18n.get('Cancel'))
				},
				userLabel : $('<span />').addClass('Cval-helper-font-weight-bold'),
				contentHtml : null
			},
			init : function(callback) {
				this._createHtml();
				Cval.core.callback(callback);
			},
			_beforeHideConfirmed : function(dialog, event, ui) {
				this._beforeHide.apply(this, arguments);
				Cval.page.getControl('admin_users').refreshUsers();
			},
			_show : function() {
				var options = this.data(this.getElement()).options,
					data = options.data,
					multiple = options.multiple || false;
				
				// Update user and group label
				this.defaults.userLabel.text(multiple ? data.length : data.label);
				
				// Update content text
				this.defaults.contentHtml.html(multiple ? this._constructHtmlContentMultiple() : this._constructHtmlContent());	
				
				this._super();
			},
			_bindEvents : function() {
				var thiss = this,
					options = this.data(this.getElement()).options,
					data = options.data,
					multiple = options.multiple || false,
					id = null;
					
				if (multiple) {
					id = [];
					$.each(data, function(){
						id.push(this.id);
					})
				} else {
					id = data.id;
				}

				this.defaults.buttons.confirm.click(function(){
					Cval.ajax.callModal({
						route : 'user',
						routeParams : {
							controller : 'remove'
						},
						data : {
							id : id
						}
					}, function(json) {
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}

						thiss.hide(null, 'confirmed');
					});

					return false;
				});

				this.defaults.buttons.cancel.click(function(){
					thiss.hide();

					return false;
				});
			},
			_createHtml : function() {
				if (this.defaults.loadedResponse.html)
					return;

				var thiss = this;
				this.defaults.contentHtml = $('<div />')
						.addClass('Cval-dialog-content');

				var footer = $('<div />')
						.addClass('Cval-dialog-footer ui-widget-header ui-corner-bottom Cval-helper-align-right');

				this.defaults.buttons.cancel
					.addClass('Cval-button Cval-button-live')
					.appendTo(footer);

				this.defaults.buttons.confirm
					.addClass('Cval-button Cval-button-live')
					.addClass('ui-state-highlight')
					.appendTo(footer);

				this.defaults.loadedResponse.html = $('<div />')
					.append(this.defaults.contentHtml).append(footer);
			},
			_constructHtmlContent : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you really want to remove the user "')))
					.append(this.defaults.userLabel)
					.append($('<span />').text(Cval.i18n.get('"?')));
			},
			_constructHtmlContentMultiple : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you want to remove selected users (')))
					.append(this.defaults.userLabel)
					.append($('<span />').text(Cval.i18n.get(')?')));
			}
		},
		params : {}
	}
	
	// KEXTSTART
	
	Cval.dialog.control.def.extend('Cval.dialog.control.user_remove', 
		Cval_dialog_control_user_remove_object.staticParams,
		Cval_dialog_control_user_remove_object.params
	);

})(Cval, jQuery);