<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	'models' => array(
		'user', 'role', 'user_token',
		'user_profile',
		'user_profile_shared',
		'user_pair'
	)

);

