<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(

	'templates' => array(
		'admin/user/create' => array(
			'subject' => Cval::instance()->location()->name().' - New account',
			'view' => 'email/templates/cval/admin/user/create',
			'html' => FALSE
		),
	)

);