<?php

defined('SYSPATH') or die('No direct script access.');	

// Init Date class
Date::init();

// Deactivate test controllers in production
Route::set('test', 'test/<controller>(/<action>)')
->defaults(array(
	'directory' => 'test',
))->i18n(I18n::enabled_langs(TRUE))->environment(Kohana::PRODUCTION, TRUE);

// Cval media route
Route::set('cval/media', 'cval/media(/<file>)', array('file'=>'.+'))
->defaults(array(
	'directory'  => 'cval',
	'controller' => 'media',
	'action'     => 'file',
	'file'       => NULL,
	'development' => FALSE,
))->i18n(I18n::enabled_langs(TRUE));

// Cval media route
Route::set('favicon', 'favicon.ico')
->defaults(array(
	'directory'  => 'cval',
	'controller' => 'media',
	'action'     => 'file',
	'file'       => 'favicon.ico',
));
