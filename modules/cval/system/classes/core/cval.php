<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * Base Cval class
 *
 * @package     Cval
 * @author      Zdenal
 */
abstract class Core_Cval
{

	/**
	 * @var	string	prefix used in i18n files
	 */
	public static $i18n_prefix = 'cval.';
	
	/**
	 * @var	string	session key that tells if user was just logged in
	 */
	public static $just_logged_in = 'cval_just_logged_in';

	/**
	 * cval instance
	 * @staticvar cval $instance
	 * @return cval
	 */
	public static function instance()
	{
		if ( ! isset(Cval::$_instance))
		{
			Cval::$_instance = new Cval;
		}
		return Cval::$_instance;
	}

	/**
	 * Test if user has onsite permissions
	 *
	 * @return	boolean
	 */
	public static function allowed($exception = FALSE)
	{
		// Check if user is allowed for cval resource
		return Cval::instance()->a2()->allowed('cval', NULL, $exception);
	}
	
	/**
	 * Test if user is logged in
	 *
	 * @return	boolean
	 */
	public static function logged_in()
	{
		return Cval::instance()->a2()->logged_in();
	}

	/**
	 * Returns available themes for cval as options for enum field
	 *
	 * @return array
	 */
	public static function themes_options()
	{
		$themes = Kohana::config('cval.themes');

		$options = array();
		foreach ($themes as $key => $theme)
		{
			$options[$key] = Arr::get($theme, 'label', $key);
		}

		return $options;
	}

	/**
	 * Returns Cval language.
	 *
	 * @return  string
	 */
	public static function lang()
	{
		return Cval::instance()->user_profile()->lang;
	}

	/**
	 * Returns translation of a string.
	 *
	 * @param   string  text to translate
	 * @param   array   values to replace in the translated text
	 * @return  string
	 */
	public static function i18n($string, array $values = NULL, $lang = NULL)
	{
		// Use logged users lang if not specified 
		if ($lang === NULL)
		{
			$lang = Cval::lang();
		}
		// Try to get transaltion with cval prefix
		$translated = __(Cval::$i18n_prefix.$string, $values, $lang);

		// If not found try without prefix
		if (strpos($translated, Cval::$i18n_prefix) === 0)
		{
			$translated = __($string, $values, $lang);
		}

		return $translated;
	}
	
	/**
	 * This data are appended to every ajax response. Used for example
	 * for checking same timezone offset on client side and server side.
	 * 
	 * @return	array 
	 */
	public static function system_response_data()
	{
		$data = array();
		
		// Add timezone offset only for logged in user
		if (Cval::logged_in())
		{
			$local_timezone_offset = Cval_Date::timezone_offset_UTC(Date::HOUR);
		
			$data['timezone'] = array(
				'gmt_offset' => $local_timezone_offset*-1
			);
		}
		
		return $data;
	}
	
	/**
	 * @var	Cval	Cval instance
	 */
	protected static $_instance;
	
	protected static $_db_configs = array(
		'registration_type'
	);

	/**
	 * @var		A2		Instance of A2
	 */
	public $a2;

	/**
	 * @var		Model_User		Instance of Model_User
	 */
	public $user;
	
	/**
	 * Returns auth instance
	 * 
	 * @return	A2
	 */
	public function a2()
	{
		if ($this->a2 === NULL)
		{
			$this->a2 = A2::instance();
		}
		return $this->a2;
	}

	/**
	 * Checks if user is allowed for specific acl config
	 * 
	 * @param	array	Config array with acl data to check
	 * @return	bool
	 */
	public function is_allowed(array $config)
	{
		if (isset($config['acl_resource']))
		{
			// If resource is not allowed do not add it to menu
			if ( ! $this->a2()->allowed($config['acl_resource'], Arr::get($config, 'acl_privilege', 'manage')))
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * Get specific key from cval config.
	 * 
	 * @param	string	Needed key from config
	 * @return	mixed
	 */
	public function config($key = NULL, $default = NULL)
	{
		return Cval_Config::get($key, $default);
	}

	/**
	 * Returns json config.
	 * 
	 * @param	bool	When TRUE config array is converted to JSON
	 * @return	mixed	JSON string if first param is TRUE or config array
	 */
	public function config_js($json = TRUE)
	{
		$config = array();

		$config['location'] = Cval_Location::config_js();
		
		foreach ($this->config('defaults') as $key => $value)
		{
			$config[$key] = $this->user_profile()->{$key};
		}
		// Add onload messages
		$config['onload_messages'] = Cval_Message::get();

		if ($this->logged_in())
		{
			$config['logged_user'] = array(
				'id' => $this->user()->id(),
				'name' => $this->user()->name(),
				'username' => $this->user()->username,
				'verified' => Cval::allowed()
			);
			if (Session::instance()->get_once(Cval::$just_logged_in))
			{
				$config['logged_user']['just_logged_in'] = TRUE;
			}
			$config['comet'] = Cval_Comet::config_js();
			// Schedule config
			$config['schedule'] = array(
				'states' => Schedule_State::config_js()
			);
		}
		$config['time_offset'] = Cval_Date::timezone_offset_UTC();
		$config['is_timezone_set'] = $this->user_profile()->is_timezone_set;
		
		$config['Loader'] = array(
			'unique' => TRUE,
			'uniqueToken' => Kohana::$environment == Kohana::DEVELOPMENT ? NULL : $this->config('release')
		);

		if ( ! $json)
		{
			return $config;
		}

		return Text::json_indent(json_encode((object) $config));
	}

	/**
	 * Returns I18n json config.
	 * 
	 * @param	bool	When TRUE config array is converted to JSON
	 * @return	mixed	JSON string if first param is TRUE or config array
	 */
	public function i18n_js($json = TRUE)
	{
		$data = array();

		foreach ($this->config('i18n.js.onload') as $value)
		{
			$data[$value] = Cval::i18n($value);
		}

		if ( ! $json)
		{
			return $data;
		}

		return Text::json_indent(json_encode((object) $data));
	}
	
	/**
	 * Returns logged in user.
	 * 
	 * @return	Model_User
	 */
	public function user($refresh = FALSE)
	{
		if ($this->user === NULL OR $refresh)
		{
			// Get logged in user
			$user = $this->a2()->get_user();

			if (empty($user) OR ! $user->loaded())
			{
				$user = Jelly::factory('user');
			}
			
			$this->user = $user;
		}

		return $this->user;
	}

	/**
	 * Returns logged in user profile.
	 * 
	 * @return	Model_User_Profile
	 */
	public function user_profile($refresh = FALSE)
	{
		return $this->user($refresh)->user_profile();
	}
	
	/**
	 * Returns logged in user profile local.
	 * 
	 * @return	Model_User_Profile_Shared
	 */
	public function user_profile_shared($refresh = FALSE)
	{
		return $this->user_profile($refresh)->shared_profile();
	}
	
	/**
	 * Gets local location object
	 * 
	 * @param	bool	Refresh loaded location
	 * @return	Model_Location
	 */
	public function location($refresh = FALSE)
	{
		return Model_Location::load_local($refresh);
	}

}

