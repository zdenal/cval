<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Cval_Unserialize_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Cval_Unserialize_Exception extends Kohana_Exception 
{
	/**
	 * Creates a new cval unserialize exception:
	 *
	 *	   throw Cval_Unserialize_Exception::create($object, $message);
	 *
	 * @param   string|object   Class name or object being unserialized
	 * @param   string     error message
	 * @param   array      translation variables
	 * @param   integer    the exception code
	 * @return  Cval_Unserialize_Exception
	 */
	public static function create($class, $message, array $variables = NULL, $code = 0)
	{
		if (is_object($class))
		{
			$class = get_class($class);
		}
		
		$message = '['.$class.'] '.$message;
		
		return new Cval_Unserialize_Exception($message, $variables, $code);
	}
	
}
