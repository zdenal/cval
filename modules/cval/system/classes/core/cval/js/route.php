<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Category Classes
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_JS_Route
{

	protected static $_routes = array();

	public static function get($name = NULL)
	{
		if ($name === NULL)
		{
			return Cval_JS_Route::$_routes;
		}

		Arr::get(Cval_JS_Route::$_routes, $name);
	}

	public static function set($route, $uri = NULL)
	{
		if (is_array($route))
		{
			foreach ($route as $r => $uri)
			{
				Cval_JS_Route::set($r, $uri);
			}
			return;
		}

		if (is_object($uri) AND $uri instanceof Route)
		{
			$route_object = $uri;
			$uri = array(
				'uri' => $route_object->get_empty_url(),
				'defaults' => $route_object->get_defaults(TRUE)
			);
		}

		Cval_JS_Route::$_routes[$route] = $uri;
	}

	public static function json()
	{
		// Replace back slashes
		return Text::json_indent(str_replace('\\/', '/', json_encode( (object) Cval_JS_Route::$_routes)));
	}
	
} // End Cval_JS_Route