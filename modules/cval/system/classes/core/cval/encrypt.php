<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * Cval Encrypt class
 * Using RC4
 *
 * @package     Cval
 * @author      Zdenek Kalina
 */
class Core_Cval_Encrypt
{

	/**
	 * Encrypts a string and returns an encrypted string that can be decoded.
	 *
	 *     $data = Cval_Encrypt::encode($data, $salt);
	 *
	 * The encrypted data is encoded using [base64](http://php.net/base64_encode)
	 * to convert it to a string. This string can be stored in a database,
	 * displayed, and passed using most other means without corruption.
	 *
	 * @param   string  data to be encrypted
	 * @param   string  encryption key
	 * @return  string
	 */
	public static function encode($data, $salt)
	{
		return Cval_Encrypt::_crypt($data, $salt, TRUE);
	}
	
	/**
	 * Decrypts an encoded string back to its original value.
	 *
	 *     $data = Cval_Encrypt::decode($data, $salt);
	 *
	 * @param   string  encoded string to be decrypted
	 * @param   string  encryption key
	 * @return  string
	 */
	public static function decode($data, $salt)
	{
		return Cval_Encrypt::_crypt($data, $salt, FALSE);
	}

	protected static function _crypt($data, $salt, $encrypt = TRUE)
	{
		$key = array();
		$result = "";
		$state = array();
		$salt = md5(str_rot13($salt));
		$len = strlen($salt);

		if ($encrypt)
		{
			$data = str_rot13($data);
		}
		else
		{
			$data = base64_decode($data);
		}

		$ii = -1;

		while ( ++ $ii < 256)
		{
			$key[$ii] = ord(substr($salt, (($ii % $len) + 1), 1));
			$state[$ii] = $ii;
		}

		$ii = -1;
		$j = 0;

		while ( ++ $ii < 256)
		{
			$j = ($j + $key[$ii] + $state[$ii]) % 255;
			$t = $state[$j];

			$state[$ii] = $state[$j];
			$state[$j] = $t;
		}

		$len = strlen($data);
		$ii = -1;
		$j = 0;
		$k = 0;

		while ( ++ $ii < $len)
		{
			$j = ($j + 1) % 256;
			$k = ($k + $state[$j]) % 255;
			$t = $key[$j];

			$state[$j] = $state[$k];
			$state[$k] = $t;

			$x = $state[(($state[$j] + $state[$k]) % 255)];
			$result .= chr(ord($data[$ii]) ^ $x);
		}

		if ($encrypt)
		{
			$result = base64_encode($result);
		}
		else
		{
			$result = str_rot13($result);
		}

		return $result;
	}

}	