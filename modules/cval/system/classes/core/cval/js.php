<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Category Classes
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_JS
{

	protected static $_files = array();

	public static function get($file = NULL)
	{
		if ($file === NULL)
		{
			return Cval_JS::$_files;
		}

		Arr::get(Cval_JS::$_files, $file);
	}

	public static function set($file)
	{
		if (is_array($file))
		{
			foreach ($file as $f)
			{
				Cval_JS::set($f);
			}
			return;
		}

		Cval_JS::$_files[ ($file instanceof View) ? $file->get_filename() : $file ] = $file;
	}

	public static function set_first($file)
	{
		$original_files = Cval_JS::$_files;

		Cval_JS::$_files = array();
		Cval_JS::set($file);

		Cval_JS::$_files = array_merge(Cval_JS::$_files, $original_files);
	}

	public static function set_last($file)
	{
		$original_files = Cval_JS::$_files;

		Cval_JS::$_files = array();
		Cval_JS::set($file);

		Cval_JS::$_files = array_merge($original_files, Cval_JS::$_files);
	}
	
} // End Cval_JS