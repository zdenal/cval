<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Additional request exception handler
 *
 * @package     Cval
 * @author      Zdenek Kalina
 */
abstract class Core_Cval_Exception_Handler {

	/**
	 * @return	array
	 */
	public static function get($type, array $params = array())
	{
		$callback = NULL;
		
		switch ($type)
		{
			case 'save':
			case 'delete':
				$callback = 'Cval_Exception_Handler::call_rollback';
				break;
			case 'default':
				$callback = 'Cval_Exception_Handler::call';
				break;
			default:
				throw new Cval_Exception('Exception handler `:type` is not supported.', array(
					':type' => $type
				));
		}

		// Get all passed variables
		$variables = func_get_args();

		return array(
			'callback' => $callback,
			'params' => $variables
		);
	}

	/**
	 * @return	boolean
	 */
	public static function call(Exception $e, Request $request, $type, array $params = array())
	{
		if (Arr::get($params, 'rollback'))
		{
			// Rollback the transaction
			Database::instance()->transaction_rollback();
		}
		
		if ($e instanceof Validate_Exception)
		{
			$errors = $e->array->detailed_errors(Arr::get($params, 'detailed_errors_file', 'cval/validate'), TRUE, TRUE);

			// Set wrong user input on errors
			$request->status = 440;
			$request->response(array('errors' => $errors));

			return TRUE;
		}

		return FALSE;
	}
	
	/**
	 * @return	boolean
	 */
	public static function call_rollback(Exception $e, Request $request, $type, array $params = array())
	{
		if ( ! isset($params['rollback']))
		{
			$params['rollback'] = TRUE;
		}
		
		return Cval_Exception_Handler::call($e, $request, $type, $params);
	}

}

