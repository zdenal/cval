<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Cval_Date
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_Date
{
	/**
	 * Format timestamp to display format for logged user in its timezone.
	 *
	 * @return  string
	 */
	public static function format($timestamp, $with_time = FALSE, $friendly = FALSE, $in_timezone = FALSE)
	{	
		if (empty($timestamp) OR ! is_int($timestamp))
		{
			return $timestamp;
		}
		
		$profile = Cval::instance()->user_profile();
		$time_24_hour = $profile->time_24_hour;
		$date_month_second = $profile->date_month_second;
		
		return $friendly
			? Cval_Date::_friendly_format($timestamp, $date_month_second, $with_time, $time_24_hour, $in_timezone)
			: Cval_Date::_classic_format($timestamp, $date_month_second, $with_time, $time_24_hour, $in_timezone);
	}
	
	/**
	 * Format timestamp to display time format for logged user in its timezone.
	 *
	 * @return  string
	 */
	public static function format_time($timestamp, $in_timezone = FALSE)
	{	
		if (empty($timestamp) OR ! is_int($timestamp))
		{
			return $timestamp;
		}
		
		$profile = Cval::instance()->user_profile();
		$time_24_hour = $profile->time_24_hour;
		
		return Cval_Date::_time_format($timestamp, $time_24_hour, $in_timezone = FALSE);
	}
	
	/**
	 * Format timestamp to friendly format.
	 *
	 * @return  string
	 */
	protected static function _friendly_format($timestamp, $month_second, $with_time, $time_24_hour, $in_timezone = FALSE)
	{	
		$now = Date::time();
		
		$same_year = (Date::format($now, 'Y') == Date::format($timestamp, 'Y'));
		$same_day = ($same_year AND (Date::format($now, 'z') == Date::format($timestamp, 'z')));
		
		if ( ! $same_year)
		{
			return Cval_Date::_classic_format($timestamp, $month_second, $with_time, $time_24_hour, $in_timezone);
		}
		if ( ! $in_timezone)
		{
			// Move timestamp to timezone
			$timestamp = Cval_Date::to_timezone($timestamp);
		}
		
		$parts = array();
		
		if ($same_day)
		{
			$with_time = TRUE;
		}
		elseif ($same_year)
		{
			$month = Cval::i18n(Date::format($timestamp, 'M'));
			$day = Date::format($timestamp, 'j');
			
			$parts[] = $month_second ? $day.' '.$month : $month.' '.$day;
		}
		
		if ($with_time)
		{
			$parts[] = Cval_Date::_time_format($timestamp, $time_24_hour, TRUE);
		}
		
		return implode(' ', $parts); 
	}
	
	/**
	 * Format timestamp to classic format.
	 *
	 * @return  string
	 */
	protected static function _classic_format($timestamp, $month_second, $with_time, $time_24_hour, $in_timezone = FALSE)
	{	
		if ( ! $in_timezone)
		{
			// Move timestamp to timezone
			$timestamp = Cval_Date::to_timezone($timestamp);
		}
		
		$parts = array();
		$parts[] = Date::format($timestamp, $month_second ? 'j/n/Y' : 'n/j/Y');
		
		if ($with_time)
		{
			$parts[] = Cval_Date::_time_format($timestamp, $time_24_hour, TRUE);
		}
		
		return implode(' ', $parts);
	}
	
	/**
	 * Format timestamp to time.
	 *
	 * @return  string
	 */
	protected static function _time_format($timestamp, $time_24_hour, $in_timezone = FALSE)
	{	
		if ( ! $in_timezone)
		{
			// Move timestamp to timezone
			$timestamp = Cval_Date::to_timezone($timestamp);
		}
		
		$time = Date::format($timestamp, $time_24_hour ? 'G:i' : 'g:i');
		if ( ! $time_24_hour)
		{
			$time .= ' '.Cval::i18n(Date::format($timestamp, 'a'));
		}
		
		return $time;
	}

	/**
	 * Convert timestamp from server timezone to logged user timezone.
	 * 
	 * @param	integer|array	timestamp or array of timestamps
	 * @return	integer|array	timestamp or array of timestamps
	 */
	public static function to_timezone($timestamp)
	{
		if (is_array($timestamp))
		{
			foreach ($timestamp as $key => $tstamp)
			{
				$timestamp[$key] = Cval_Date::to_timezone($tstamp);
			}
			return $timestamp;
		}
		
		if (empty($timestamp) OR ! is_int($timestamp))
		{
			return $timestamp;
		}
		
		// Get timezones offest
		$offset = Date::offset(Date::timezone(), Cval_Date::timezone(), $timestamp);

		// Move timestamp for given offset
		$timestamp = $timestamp - $offset;
		
		return $timestamp;
	}
	
	/**
	 * Convert timestamp from logged user timezone to server timezone.
	 * 
	 * @param	integer
	 * @return	integer 
	 */
	public static function from_timezone($timestamp)
	{
		if (empty($timestamp))
		{
			return $timestamp;
		}
		
		// Convert strings to timestamps
		if ( ! is_int($timestamp))
		{
			$timestamp = strtotime($timestamp);
		}
		
		// Get timezones offest
		$offset = Date::offset(Cval_Date::timezone(), Date::timezone(), $timestamp);

		// Move timestamp for given offset
		$timestamp = $timestamp - $offset;
		
		return $timestamp;
	}
	
	/**
	 * Returns timezone for logged user.
	 * 
	 * @return	string 
	 */
	public static function timezone()
	{
		$timezone = Cval::instance()->user_profile()->timezone;
		
		if (empty($timezone))
		{
			$timezone = Date::timezone();
		}
		
		return $timezone;
	}
	
	/**
	 * Returns timezone offset from server time for logged user.
	 * 
	 * @return	string 
	 */
	public static function timezone_offset($units = Date::SECOND)
	{
		// Get timezones offest
		return Date::offset(Date::timezone(), Cval_Date::timezone()) / $units;
	}
	
	/**
	 * Returns timezone offset from UTC time for logged user.
	 * 
	 * @return	string 
	 */
	public static function timezone_offset_UTC($units = Date::SECOND)
	{
		// Get timezones offset from UTC
		return Date::offset('UTC', Cval_Date::timezone()) / $units;
	}

}

// End Model_Config