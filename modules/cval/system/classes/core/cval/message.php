<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Class for storing and getting onload messages
 *
 * @package     Cval
 * @author      Zdennal
 */
abstract class Core_Cval_Message {

	const ERROR = 'error';
	const INFO = 'info';
	const SUCCESS = 'success';

	protected static $_display = array();

	/**
	 * @return	array
	 */
	public static function create($type, $message = NULL, $title = NULL, array $options = array())
	{
		// In case we are calling create from already created message
		if (is_array($type))
		{
			return $type;
		}
		
		if ($message instanceof View)
		{
			$message = $message->render();
		}

		return array_merge($options, array(
			'type' => $type,
			'message' => $message,
			'title' => $title
		));
	}
	
	public static function set_flash($type, $message = NULL, $title = NULL, array $options = array())
	{
		$message = Cval_Message::create($type, $message, $title, $options);
		
		$saved = Session::instance()->get('Cval_onload_messages', array());
		$saved[] = $message;
		Session::instance()->set('Cval_onload_messages', $saved);
	}

	public static function set_display($type, $message = NULL, $title = NULL, array $options = array())
	{
		Cval_Message::$_display[] = Cval_Message::create($type, $message, $title, $options);
	}

	public static function get_flash()
	{
		return Session::instance()->get_once('Cval_onload_messages', array());
	}

	public static function get_display()
	{
		return Cval_Message::$_display;
	}

	public static function get()
	{
		return array_merge(Cval_Message::get_flash(), Cval_Message::get_display());
	}

}

