<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Category Classes
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_CSS
{

	protected static $_files = array();

	public static function get($file = NULL)
	{
		if ($file === NULL)
		{
			return Cval_CSS::$_files;
		}

		Arr::get(Cval_CSS::$_files, $file);
	}

	public static function set($file, $attributes = NULL)
	{
		if (is_array($file))
		{
			foreach ($file as $f => $attributes)
			{
				Cval_CSS::set($f, $attributes);
			}
			return;
		}

		Cval_CSS::$_files[$file] = $attributes;
	}

	public static function set_first($file, $attributes = NULL)
	{
		$original_files = Cval_CSS::$_files;

		Cval_CSS::$_files = array();
		Cval_CSS::set($file, $attributes);

		Cval_CSS::$_files = array_merge(Cval_CSS::$_files, $original_files);
	}

	public static function set_last($file, $attributes = NULL)
	{
		$original_files = Cval_CSS::$_files;

		Cval_CSS::$_files = array();
		Cval_CSS::set($file, $attributes);

		Cval_CSS::$_files = array_merge($original_files, Cval_CSS::$_files);
	}
	
} // End Cval_CSS