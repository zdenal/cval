<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Cval_Config
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_Config
{

	/**
	 * @var	array	Array defining configs stored in DB with their default values
	 */
	protected static $_db_configs = array(
		'registration_type' => 10, // Closed registration
		'remote_connectivity' => 10, // Closed connectivity
	);
	
	/**
	 * @var	array	Storage for loaded db configs
	 */
	protected static $_fetched_db_configs = array();
	
	/**
	 * Gets cval config.
	 * If key is NULL complete config cval array is returned but without DB items.
	 * 
	 * @param	string	config key
	 * @param	mixed	default value to use if key is not set
	 * @return	mixed	config value 
	 */
	public static function get($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			return (array) Kohana::config('cval');
		}
		
		if (Cval_Config::_is_db($key))
		{
			return Cval_Config::_get_db($key, $default);
		}
		
		return Arr::path(Kohana::config('cval'), $key, $default);
	}
	
	public static function set($key, $value)
	{
		if ( ! Cval_Config::_is_db($key))
		{
			throw new Kohana_Exception('Config key `:key` can not be set', array(
				':key' => $key
			));
		}
		
		return Cval_Config::_set_db($key, $value);
	}
	
	/**
	 * Check if given config ID is configured for DB
	 * 
	 * @param	string	Config ID
	 * @return	bool
	 */
	protected static function _is_db($id)
	{
		return key_exists($id, Cval_Config::$_db_configs);
	}
	
	/**
	 * Gets config value from DB
	 *
	 * @param	string	Config ID
	 * @param	mixed	Defualt value if config is not set
	 * @return	mixed
	 */
	protected static function _get_db($id, $default = NULL)
	{
		if ( ! key_exists($id, Cval_Config::$_fetched_db_configs))
		{
			$config = Jelly::select('cval_config', $id);
			
			if ( ! $config->loaded())
			{
				$config = Cval_Config::_create_db($id);
			}
			
			Cval_Config::$_fetched_db_configs[$id] = Cval_Config::_get_db_value($config);
		}
		
		$value = Cval_Config::$_fetched_db_configs[$id];
		
		return $value === NULL ? $default : $value;
	}
	
	/**
	 * Sets config value in DB
	 * 
	 * @param	string	Config ID
	 * @param	mixed	Value to set
	 * @return	mixed	Value after fetching from DB
	 */
	protected static function _set_db($id, $value)
	{
		$config = Jelly::select('cval_config', $id);
			
		if ( ! $config->loaded())
		{
			$config = Cval_Config::_create_db($id);
		}
		
		Cval_Config::_set_db_value($config, $value);
		$config->save();
		
		$value = Cval_Config::_get_db_value($config);
		Cval_Config::$_fetched_db_configs[$id] = $value;
		
		return $value;
	}
	
	/**
	 * Creates config object in database from default value
	 * 
	 * @param	string	Config ID
	 * @param	mixed	Value to set for new config, if NULL, the default one is used
	 * @return	Model_Config	Created and saved config object
	 */
	protected static function _create_db($id, $value = NULL)
	{
		$config = Jelly::factory('cval_config');
		$config->id = $id;
		Cval_Config::_set_db_value($config, $value === NULL ? Arr::get(Cval_Config::$_db_configs, $id) : $value);
		$config->save();
		
		return $config;
	}

	/**
	 * Set only value for DB object and determines its type.
	 * 
	 * @param	Model_Cval_Config	Config object
	 * @param	mixed				Value to set
	 */
	protected static function _set_db_value(Model_Cval_Config $config, $value)
	{
		if (is_numeric($value))
		{
			$numeric_value = $value + 0;
			$use_numeric = TRUE; 
			if (is_int($numeric_value))
			{
				$config->type = 'int';
			}
			elseif (is_float($numeric_value))
			{
				$config->type = 'float';
			}
			else
			{
				$use_numeric = FALSE; 
			}
			
			if ($use_numeric)
			{
				$value = $numeric_value;
			}
		}
		elseif (is_bool($value))
		{
			$config->type = 'bool';
		}
		else
		{
			$config->type = 'string';
		}
		
		$config->value = $value;
	}

	/**
	 * Gets only value from DB object according to its type.
	 * 
	 * @param	Model_Cval_Config	Config object for getting value from
	 * @return	mixed	Stored value in config object
	 */
	protected static function _get_db_value(Model_Cval_Config $config)
	{
		switch ($config->type)
		{
			case 'int':
				$value = (int) $config->value;
				break;
			case 'float':
				$value = (float) $config->value;
				break;
			case 'bool':
				$value = (bool) $config->value;
				break;
			case 'string':
				$value = (string) $config->value;
				break;
			default:
				$value = NULL;
		}
		return $value;
	}

}

// End Model_Config