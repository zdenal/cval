<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Not_Found_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Not_Found_Exception extends Kohana_Exception {

	/**
	 * @var	string
	 */
	public $model_name;
	
	/**
	 * @var	array 
	 */
	public $load_variables;

	/**
	 * Creates an exception used when model is not found:
	 *
	 *	   throw Model_Not_Found_Exception::create($model, $load_variables);
	 *
	 * @param   mixed		model name or Jelly_Model
	 * @param	mixed		primary key value or array of key => variable 
	 *						used for loading the model
	 * @param   string		error message
	 * @param   array		error message variables
	 * @param   integer		the exception code
	 * @return  Model_Not_Found_Exception
	 */
	public static function create($model, $load_variables, $message = NULL, array $message_variables = NULL, $code = 0)
	{	
		$model = Jelly::model_name($model);
		$meta = Jelly::meta($model);
		
		if ( ! is_array($load_variables))
		{
			$load_variables = array($meta->primary_key() => $load_variables);
		}
		
		if ($message === NULL)
		{
			$message_variables = array();
			foreach ($load_variables as $key => $value)
			{
				$message_variables[] = strtr(':key: :value', array(
					':key' => $key,
					':value' => $value
				));
			}
			$message = strtr('Model [:model] not found [:variables]', array(
				':model' => $model,
				':variables' => implode('; ', $message_variables)
			));
			$message_variables = NULL;
		}
		
		$exception = new Model_Not_Found_Exception($message, $message_variables, $code);
		$exception->model_name = $model;
		$exception->load_variables = $load_variables;
		
		return $exception;
	}
	
	/**
	 * @return	string
	 */
	public function get_model_name()
	{
		return $this->model_name;
	}

	/**
	 * @return	array
	 */
	public function get_load_variables()
	{
		return $this->load_variables;
	}

} // End Core_Model_Not_Found_Exception
