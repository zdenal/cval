<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model Cval Config
 * @package Model
 * @author	Zdennal
 */
abstract class Core_Model_Cval_Config extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('id');

		$meta->fields(array(
			'id' => new Field_Primary(array(
				'integer' => FALSE,
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(255),
				)
			)),
		));

		$meta->fields(array(
			'value' => new Field_String(array(
				'rules' => array(
					'max_length' => array(255),
				)
			)),
			'type' => new Field_Enum(array(
				'choices' => array(
					'string' => 'String',
					'int' => 'Integer',
					'bool' => 'Boolean',
				),
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(10),
				)
			)),
		));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}

}

// End Model_Config