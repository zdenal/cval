<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Cval
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Cval extends Jelly_Model 
{
	
    public static function initialize(Jelly_Meta $meta) 
	{	
        $meta->name_key('label')
             ->sorting(array('label' => 'ASC'));

        Behavior_Labelable::initialize($meta, array(
			'label' => 'name',
			'rules' => array(
				'not_empty' => NULL,
				'max_length' => array(255)
			)
		));

        $meta->fields(array(
            'id' => new Field_Primary,
        ));

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }

}
