<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Validate_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Validate_Exception extends Validate_Exception {

	/**
	 * @var	string			Validated instance model name
	 */
	public $model_name;
	
	/**
	 * @var	Jelly_Model		Validated instance
	 */
	public $model;
	
	/**
	 * @var	Validate		Validate instance
	 */
	public $array;

	/**
	 * Creates an exception used when model is not validated:
	 *
	 *	   throw Model_Validate_Exception::create($validate, $model);
	 * 
	 * @param   Jelly_Model	validated instance
	 * @param   Validate	validate instance
	 * @param   string		error message
	 * @param   array		error message variables
	 * @param   integer		the exception code
	 * @return  Model_Validate_Exception
	 */
	public static function create(Jelly_Model $model, Validate $validate, $message = NULL, array $message_variables = NULL, $code = 11)
	{	
		$model_name = Jelly::model_name($model);
		if ($message === NULL)
		{
			$message = strtr('Model [:model] not validated', array(
				':model' => $model_name
			));
			$message_variables = NULL;
		}
		
		$exception = new Model_Validate_Exception($validate, $message, $message_variables, $code);
		$exception->model = $model;
		$exception->model_name = $model_name;
		
		return $exception;
	}
	
	/**
	 * @return	Jelly_Model
	 */
	public function get_model()
	{
		return $this->model;
	}

	/**
	 * @return	Validate 
	 */
	public function get_array()
	{
		return $this->array;
	}
	
	/**
	 * @return	string
	 */
	public function get_model_name()
	{
		return $this->model_name;
	}

} // End Core_Model_Validate_Exception
