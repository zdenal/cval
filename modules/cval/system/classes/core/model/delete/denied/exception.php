<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Delete_Denied_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Delete_Denied_Exception extends Kohana_Exception {

	/**
	 * @var	string			Delete_Deniedd instance model name
	 */
	public $model_name;
	
	/**
	 * @var	Jelly_Model		Delete_Deniedd instance
	 */
	public $model;

	/**
	 * Creates an exception used when model delete is denied:
	 *
	 *	   throw Model_Delete_Denied_Exception::create($model);
	 * 
	 * @param   Jelly_Model	validated instance
	 * @param   string		error message
	 * @param   array		error message variables
	 * @param   integer		the exception code
	 * @return  Model_Delete_Denied_Exception
	 */
	public static function create(Jelly_Model $model, $message = NULL, array $message_variables = NULL, $code = 11)
	{	
		$model_name = Jelly::model_name($model);
		if ($message === NULL)
		{
			$message = strtr('Model [:model] can not be deleted', array(
				':model' => $model_name
			));
			$message_variables = NULL;
		}
		
		$exception = new Model_Delete_Denied_Exception($message, $message_variables, $code);
		$exception->model = $model;
		$exception->model_name = $model_name;
		
		return $exception;
	}
	
	/**
	 * @return	Jelly_Model
	 */
	public function get_model()
	{
		return $this->model;
	}
	
	/**
	 * @return	string
	 */
	public function get_model_name()
	{
		return $this->model_name;
	}

} // End Core_Model_Delete_Denied_Exception
