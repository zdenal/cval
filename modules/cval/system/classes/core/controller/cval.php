<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller cval class.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Cval extends Controller_Ext 
{

	/**
	 * @var Cval	Cval instance 
	 */
	public $cval;

	/**
	 * Defines cval instance.
	 */
	public function before()
	{
		$this->cval = Cval::instance();
		
		parent::before();
	}

} // End Core_Controller_Cval
