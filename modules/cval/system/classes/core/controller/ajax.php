<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for AJAX calls.
 *
 * @package    Core
 * @category   Controller
 */
abstract class Core_Controller_Ajax extends Controller_Secure 
{	
	/**
	 * @var array	Tells if no ajax redirect map should be tested
	 */
	protected $_no_ajax_redirect = FALSE;
	
	/**
	 * @var array	Array of actions which can be redirected to 
	 */
	protected $_no_ajax_redirect_map = array();
	
	/**
	 * @var string	Output format
	 */
	public $response_format = 'json';
	
	/**
	 * @var array	Array of actions which need not to be called as AJAX or internal request
	 */
	public $non_ajax_actions = array();
	
	/**
	 * @var	bool	TRUE for auto appending system response to json array
	 */
	public $add_system_response = TRUE;

	/**
	 * Checks the requested method against the available methods. If the method
	 * is supported, sets the request action from the map. If not supported,
	 * the "invalid" action will be called.
	 */
	public function before()
	{
		// Check if internal request
		if ($this->request !== Request::instance())
		{
			$this->internal = TRUE;
		}

		if ( ! $this->internal 
				AND ! Request::$is_ajax 
				AND $this->non_ajax_actions !== TRUE
				AND ! in_array($this->request->action, $this->non_ajax_actions))
		{
			if ( ! $this->internal AND $this->_no_ajax_redirect)
			{
				$redirect_url = $this->no_ajax_redirect_url();
				if ($redirect_url !== FALSE)
				{
					$this->request->redirect($redirect_url);
				}
			}
			throw new Request_Exception(460);
		}

		// Set default response to NULL
		$this->request->response = NULL;
		
		if ( ! $this->internal)
		{
			if (Arr::get($_REQUEST, 'response_format'))
			{
				$this->response_format = Arr::get($_REQUEST, 'response_format');
			}
			// Set response request format in before method
			$this->request->format = $this->response_format;
		}

		// Call parent tasks, ie secure tasks
		parent::before();
	}

	/**
	 * Formats output accroding to response format
	 */
	public function after()
	{	
		if ( ! $this->internal)
		{
			// Update request format in after method
			$this->request->format = $this->response_format;
		}
		
		// If we have redirect param in response
		if (is_array($this->request->response) AND ! empty($this->request->response['redirect']))
		{
			// If we have message param in response
			if ( ! empty($this->request->response['message']) AND is_array($this->request->response['message']))
			{
				// Set onload message
				Cval_Message::set_flash($this->request->response['message']);
			}
			// Request is not internal nor ajax
			if ( ! $this->internal AND ! Request::$is_ajax)
			{
				// Redirect to given url
				$this->request->redirect($this->request->response['redirect']);
			}
		}
		
		// Append system response for ajax calls
		if ($this->add_system_response AND in_array($this->request->format, array('json', 'json-text')))
		{
			if ( ! is_array($this->request->response))
			{
				$this->request->response = array();
			}
			
			$data = Cval::system_response_data();
			if ( ! empty($data))
			{
				$this->request->response['cval'] = $data;
			}
		}
		
		// Format response according to response format set in before
		$this->request->response($this->request->response);

		parent::after();
	}
	
	public function no_ajax_redirect_url()
	{
		$map = Arr::get($this->_no_ajax_redirect_map, $this->request->action,
				Arr::get($this->_no_ajax_redirect_map, 'default', FALSE));
		
		if (empty($map))
		{
			return FALSE;
		}
		
		if (is_string($map))
		{
			return Route::url($map);
		}
		if (is_array($map))
		{
			$callback = array_shift($map);
			
			if ( ! is_array($callback) AND strpos($callback, '::') === FALSE)
			{
				$callback = array($this, $callback);
			}
			
			return Method::call($callback, $map);
		}
		
		return FALSE;
	}

} // End Ajax
