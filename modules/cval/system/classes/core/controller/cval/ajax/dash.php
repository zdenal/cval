<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for AJAX calls.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Cval_Ajax_Dash extends Controller_Cval_Ajax 
{

	/**
	 * @var array	Tells if no ajax redirect map should be tested
	 */
	protected $_no_ajax_redirect = TRUE;
	
	/**
	 * @var array	Array of actions which can be redirected to 
	 */
	protected $_no_ajax_redirect_map = array(
		'index' => array('no_ajax_redirect_dash')
	);
	
	public function no_ajax_redirect_dash()
	{
		return '#'.$this->request->uri();
	}

} // End Core_Controller_Cval_Ajax_Dash
