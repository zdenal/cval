<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Secure template controller
 *
 * @package     Cval
 * @author      Zdenal
 */
abstract class Core_Controller_Cval_Template_Secure extends Controller_Template_Secure
{
	/**
	 * @var Cval	Cval instance 
	 */
	public $cval;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;

	/**
	 * @var array   Actions requiring ACL checking
	 */
	protected $_acl_required = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'cval';
	
	/**
	 * @var  string  page template
	 */
	public $template = 'cval/template';
	
	public $template_sections = array(
		'head'		=> 'cval/head',
		'logo'		=> 'cval/logo',
		'menu'		=> NULL,
		'submenu'	=> 'cval/submenu',
		'content'	=> NULL,
		'footer'	=> 'cval/footer'
	);
	
	/**
	 * The before() method is called before your controller action.
	 * In our template controller we override this method so that we can
	 * set up default values. These variables are then available to our
	 * controllers if they need to be modified.
	 */
	public function before()
	{
		$this->cval = Cval::instance();
		
		parent::before();
		
		$this->_init();
	}
	
	protected function _init()
	{
		View::set_global(array(
			'page_title' => NULL,
			'active_menu' => NULL
		));
		
		Cval_JS_Route::set(array(
			'cval/media' => Route::get('cval/media', $this->cval->user_profile()->lang),
			'auth' => Route::get('auth'),
			'login' => Route::get('login'),
			'dashboard' => Route::get('dashboard'),
			'comet' => Route::get('comet'),
			'contacts' => Route::get('contacts'),
			'contacts_main' => Route::get('contacts_main'),
			'contact' => Route::get('contact'),
			'contact_group' => Route::get('contact_group'),
			'schedules' => Route::get('schedules'),
			'schedules_main' => Route::get('schedules_main'),
			'schedule' => Route::get('schedule'),
			'schedule_main' => Route::get('schedule_main'),
			'schedule_download' => Route::get('schedule_download'),
			'schedule_group' => Route::get('schedule_group'),
			'calendars' => Route::get('calendars'),
			'calendars_main' => Route::get('calendars_main'),
			'calendar' => Route::get('calendar'),
			'locations' => Route::get('locations'),
			'location' => Route::get('location'),
			'users' => Route::get('users'),
			'user' => Route::get('user'),
			'settings' => Route::get('settings'),
			'settings_main' => Route::get('settings_main'),
			'admin' => Route::get('admin'),
			'admin_main' => Route::get('admin_main'),
		));
		
		Cval_JS::set_first(array(
			'/cval/media/js/base.js',
			'/cval/media/jquery/js/jquery-1.5.1.min.js',
			'/cval/media/jquery/js/jquery-noconflict.js',
			'/cval/media/jquery/js/jquery.livequery.min.js',
			'/cval/media/jquery/js/jQuery.Console.js',
			'/cval/media/jquery/js/jquery-ui-1.8.14.custom.min.js',
			'/cval/media/jquery/js/jquery.ui.subclass.js',
			'/cval/media/jquery/js/jquery.timers-1.2.js',
			'/cval/media/jquery/js/jquery.lang.js',
			'/cval/media/jquery/js/jquery.class.js',
			'/cval/media/jquery/js/jquery.serializeJSON.js',
			'/cval/media/vendor/datejs/date.js',
			'/cval/media/js/Loader.js',
			'/cval/media/js/i18n.js',
			View::factory('cval/js/i18n/data'),
			View::factory('cval/js/config'),
			View::factory('cval/js/config/uri'),
			'/cval/media/js/core.js',
			'/cval/media/js/helper.js',
			'/cval/media/js/uri.js',
			'/cval/media/js/ajax.js',
			'/cval/media/js/async.js',
			'/cval/media/js/resource.js',
			'/cval/media/js/jquery/data.js',
			'/cval/media/js/jquery/ui/button.js',
			'/cval/media/js/jquery/ui/button/checker.js',
			'/cval/media/js/jquery/ui/dialog.js',
			'/cval/media/js/jquery/ui/form.js',
			'/cval/media/js/jquery/ui/pagination.js',
			'/cval/media/js/jquery/ui/menu/vertical.js',
			'/cval/media/js/ui.js',
			'/cval/media/js/message.js',
			'/cval/media/js/menu.js',
		));
		
		if (Cval::logged_in()) {
			Cval_JS::set(array(
				'/cval/media/js/menu/'.(Cval::allowed() ? 'verified' : 'logged').'.js',
				'/cval/media/js/logout.js',
			));
		}
		
		$this->_init_additional_js();
		
		Cval_JS::set(array(
			'/cval/media/js/init.js',
		));
		
		if (Cval::logged_in()) {
			Cval_JS::set(array(
				'/cval/media/js/logout.js',
			));
		}
		
		if (Cval::allowed()) {
			Cval_JS::set(array(
				'/cval/media/js/comet.js',
			));
		}
		
		if (Arr::path(Kohana::config('firebug'), 'on'))
		{
			$options = array();
			foreach (Arr::path(Kohana::config('firebug'), 'options', array()) as $key => $value)
			{
				$options[] = $key.'='.json_encode($value);
			}
			Cval_JS::set_last(array(
				'/cval/media/firebug-lite/build/firebug-lite.js#'.implode(',', $options),
			));
		}
		
		Cval_CSS::set_first(array(
			'/cval/media/css/960.css' => array('media'=>'screen'),
			'/cval/media/css/960/custom.css' => array('media'=>'screen'),
			Kohana::config('cval.themes.'.$this->cval->user_profile()->theme.'.css') => array('media'=>'screen'),
			'/cval/media/css/layout.css' => array('media'=>'screen'),
			'/cval/media/css/main.css' => array('media'=>'screen'),
			'/cval/media/css/form.css' => array('media'=>'screen'),
			'/cval/media/css/message.css' => array('media'=>'screen'),
			'/cval/media/css/table.css' => array('media'=>'screen'),
			'/cval/media/css/auth.css' => array('media'=>'screen'),
			'/cval/media/css/jquery/ui/pagination.css' => array('media'=>'screen'),
		));
		
		$this->_init_additional_css();
	}
	
	protected function _init_additional_js() {}
	
	protected function _init_additional_css() {}
	
	/**
	 * Method called in before method if auto render is enabled
	 */
	protected function _before_auto_render()
	{
		if ($this->template_sections['menu'] === NULL)
		{
			$this->template_sections['menu'] = 
				Cval::logged_in() 
					? 'cval/menu/'.(Cval::allowed() ? 'verified' : 'logged') 
					: 'cval/menu';
		}
		
		foreach ($this->template_sections as $name => $view)
		{
			if (empty($view))
			{
				continue;
			}
			$this->template_sections[$name] = View::factory($view);
		}
		
		// default values for verified menu
		if (Cval::logged_in() AND Cval::allowed())
		{
			// Set number of unread schedules for displaying in menu
			$this->template_sections['menu']->set('unread_schedules_cnt', 
					Schedule_Group::factory('inbound')->unread_schedules_count($this->cval->user()));
		}
		
		parent::_before_auto_render();
	}
	
}