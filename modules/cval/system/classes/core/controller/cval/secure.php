<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller cval secure class.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Cval_Secure extends Controller_Secure 
{

	/**
	 * @var Cval	Cval instance 
	 */
	public $cval;

	/**
	 * Defines cval instance.
	 */
	public function before()
	{
		$this->cval = Cval::instance();
		
		parent::before();
	}

} // End Core_Controller_Cval
