<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for CLI calls.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Cval_CLI extends Controller_Ext 
{

	/**
	 * @var Cval	Cval instance 
	 */
	public $cval;

	/**
	 * Defines cval instance. Checks if request is from CLI.
	 */
	public function before()
	{
		if( ! Kohana::$is_cli)
		{
			throw new Request_Exception(404);
		}
		
		$this->cval = Cval::instance();
		
		parent::before();
	}

} // End Core_Controller_Cval_CLI
