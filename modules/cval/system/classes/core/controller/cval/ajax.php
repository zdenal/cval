<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for AJAX calls.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Cval_Ajax extends Controller_Ajax 
{

	/**
	 * @var Cval	Cval instance 
	 */
	public $cval;
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;

	/**
	 * @var array   Actions requiring ACL checking
	 */
	protected $_acl_required = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'cval';

	/**
	 * Checks the requested method against the available methods. If the method
	 * is supported, sets the request action from the map. If not supported,
	 * the "invalid" action will be called.
	 */
	public function before()
	{	
		if (Kohana::$environment === Kohana::DEVELOPMENT)
		{
			if ($this->non_ajax_actions !== TRUE
				AND ! in_array($this->request->action, $this->non_ajax_actions))
			{
				Request::$is_ajax = TRUE; // All calls are ajax
			}
			if (empty($_POST) AND ! empty($_GET))
			{
				$_POST = $_REQUEST; // Allow pass params in $_GET also
			}
		}
		
		$this->cval = Cval::instance();
		
		parent::before();
	}

} // End Core_Controller_Cval_Ajax
