<?php defined('SYSPATH') or die('No direct script access.');

class Request extends Core_Request 
{
	/**
	 * This method is called when some exception occurs on main request call
	 *
	 * @uses    Kohana::exception_handler
	 * @param   Exception   exception object
	 * @param   Request		request object
	 * @return  boolean
	 */
	public static function exception_handler(Exception $e, Request $request = NULL)
	{
		if ($request AND $request->exception_handler)
		{
			$params = array($e, $request);
			$callback = $request->exception_handler;
			
			if (is_array($callback) AND Arr::is_assoc($callback))
			{
				$params = array_merge($params, Arr::get($callback, 'params', array()));
				$callback = Arr::get($callback, 'callback');
			}

			if (is_callable($callback))
			{
				$valid_response = call_user_func_array($callback, $params);

				if ($valid_response)
				{
					if ($request->send_headers()->response)
					{
						echo $request->response;
					}
					exit();
				}
			}
		}

		// Default error status
		$status = 500;

		if ($e instanceof ReflectionException)
		{
			$status = 404;
		}
		elseif ($e instanceof A2_Exception)
		{
			if (A2::instance()->logged_in())
			{
				if (A2::instance()->get_user()->has_role('user'))
				{
					// Withou permissions
					$status = 403;
				}
				else
				{
					// Not verified
					$status = 470;
				}
			}
			else
			{
				// Not logged in
				$status = 401;
			}
			
			if ( ! Request::$is_ajax)
			{
				A2::exception_redirect($status);
			}
			
			// Fetch standard request message
			if (isset(Request::$messages[$status]))
			{
				$e = new A2_Exception(Request::$messages[$status], NULL, $status);
			}
		}
		elseif (isset(Request::$messages[$e->getCode()]))
		{
			$status = $e->getCode();
		}

		if ( ! $e->getMessage())
		{
			$message = isset(Request::$messages[$status]) ? Request::$messages[$status] : 'Undefined exception';

			$e = new Exception($message, $status, $e);
		}

		if ( ! headers_sent())
		{
			if (isset($_SERVER['SERVER_PROTOCOL']))
			{
				// Use the default server protocol
				$protocol = $_SERVER['SERVER_PROTOCOL'];
			}
			else
			{
				// Default to using newer protocol
				$protocol = 'HTTP/1.1';
			}

			// HTTP status line
			header($protocol.' '.$status.' '.$e->getMessage());
			// Make sure the proper content type is sent with a 500 status
			header('Content-Type: text/html; charset='.Kohana::$charset, TRUE, $status);

			// Send headers
			flush();
		}

		// Debug exception output
		if ($e instanceof Debug_Exception)
		{
			echo $e->debug;

			return;
		}

		if ($e instanceof Request_Exception AND Request::$is_ajax)
		{
			// Create a text version of the exception
			$error = Kohana::exception_text($e);

			if (is_object(Kohana::$log))
			{
				// Add this exception to the log
				Kohana::$log->add(Kohana::ERROR, $error);

				// Make sure the logs are written
				Kohana::$log->write();
			}

			echo json_encode($e->getMessage());

			return;
		}

		// Pass controlled exception to Kohana Exception handler
		return Kohana::exception_handler($e);
	}
}
