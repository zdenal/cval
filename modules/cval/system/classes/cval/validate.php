<?php defined('SYSPATH') or die('No direct script access.');

class Cval_Validate extends Core_Validate {

	/**
	 * @var	array	Array of validated keys. 
	 */
	protected $_validated_keys;
	
	/**
	 * Sets the unique "any field" key and creates an ArrayObject from the
	 * passed array.
	 *
	 * @param   array   array to validate
	 * @return  void
	 */
	public function __construct(array $array)
	{
		parent::__construct($array, ArrayObject::STD_PROP_LIST);
		
		$this->_validated_keys = array_keys($array);
	}
	
	/**
	 * Returns the array of validated values.
	 *
	 * @return  array
	 */
	public function validated_array()
	{
		$array = array();
		
		foreach ($this->_validated_keys as $key)
		{
			$array[$key] = $this[$key];
		}
		
		return $array;
	}
	
	/**
	 * Works exactly the same as Validate::errors() but for each field
	 * returns array with error name and message instead of message only.
	 *
	 *     // Get errors from messages/forms/login.php
	 *     $errors = $validate->detailed_errors('forms/login');
	 *
	 * @uses    Kohana::message
	 * @param   string  file to load error messages from
	 * @param   mixed   translate the message
	 * @param	bool	ucfirst message
	 * @return  array
	 */
	public function detailed_errors($file = NULL, $translate = TRUE, $ucfirst = FALSE)
	{
		if ($file === NULL)
		{
			$errors = array();
			foreach ($this->_errors as $field => $error)
			{
				$errors[$field] = array(
					'name' => $error[0],
					'params' => $error[1],
					'value' => isset($this[$field]) ? $this[$field] : NULL,
				);
			}
			// Return the error list
			return $errors;
		}

		$messages = $this->errors($file, $translate, $ucfirst);

		foreach ($messages as $field => $message)
		{
			$messages[$field] = array(
				'name' => $this->_errors[$field][0],
				'params' => $this->_errors[$field][1],
				'value' => isset($this[$field]) ? $this[$field] : NULL,
				'message' => $message
			);
		}

		return $messages;
	}
	
}