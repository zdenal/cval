<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * Cval Extend class
 *
 * @package     Cval
 * @author      Zdenek Kalina
 */
class Cval_Extend
{
	
	public static function point_static($class, $method)
	{
		$class_name = is_object($class) ? get_class($class) : $class;
		
		$extension_points = array();
		
		foreach (Kohana::config('extend.directories') as $directory => $config)
		{
			if (empty($config))
			{
				continue;
			}
			
			$extending_class = 'Extend_'.str_replace('/', '_', $directory).'_'.$class_name;
			
			if (class_exists($extending_class) AND is_callable(array($extending_class, $method)))
			{
				$extension_points[] = array($extending_class, $method);
			}
		}
		
		return $extension_points;
	}
	
	public static function point_media($path, $relative_path_no_ext, $ext, $lang = FALSE)
	{
		$extension_points = array();
		
		foreach (Kohana::config('extend.media_directories') as $directory => $config)
		{
			if (empty($config))
			{
				continue;
			}
			
			if ( ! empty($lang)
					AND $path = Kohana::find_file('media', 'extend/'.$directory.'/'.$lang.'/'.$relative_path_no_ext, $ext))
			{
				$extension_points[] = $path;
			}
			elseif ($path = Kohana::find_file('media', 'extend/'.$directory.'/'.$relative_path_no_ext, $ext))
			{
				$extension_points[] = $path;
			}
		}
		
		return $extension_points;
	}

}

