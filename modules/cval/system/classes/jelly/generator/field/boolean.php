<?php defined('SYSPATH') or die('No direct script access.');

class Jelly_Generator_Field_Boolean extends Core_Jelly_Generator_Field_Boolean
{
	/**
	 * @return	string			SQL string
	 */
	public function column()
	{
		$sql = "`".$this->field->column."` ";

		$sql .= $this->type()." UNSIGNED NOT NULL DEFAULT ".($this->field->default ? 1 : 0);

		return $sql;
	}
}