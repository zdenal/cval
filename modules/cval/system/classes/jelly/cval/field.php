<?php defined('SYSPATH') or die('No direct script access.');

abstract class Jelly_Cval_Field extends Jelly_Ext_Field
{
	/**
	 * Sets all options
	 *
	 * @return  void
	 **/
	public function __construct($options = array())
	{
		// Fix default variable
		$this->_default_variable_fix();
	
		parent::__construct($options);
	}
	
	/**
	 * Tests if field needs update in DB. It is called in model save process.
	 * 
	 * @param	mixed	Actual field value
	 * @param	mixed	Original field value from DB
	 * @return	bool 
	 */
	public function needs_update($value, $original)
	{
		return $value !== $original;
	}

	/**
	 * @var	array	storage for default variables of all instantiated field classes 
	 */
	protected static $_default_variables_fix = array();
	
	/**
	 * Specific override of standard field class variable 'default'.
	 * On barcelona server, when it is NULL, then it sometimes is overriden
	 * with first not NULL value. It totally breaks PHP core behavior, because
	 * class default variables can be set only before runtime, and in this case,
	 * they are overriden in runtime. 
	 */
	protected function _default_variable_fix()
	{
		// Get this field class
		$class = get_class($this);
		// Check if class was already instantiated
		if ( ! isset(Jelly_Cval_Field::$_default_variables_fix[$class]))
		{
			// Not yet so cache its default value
			// If value is NULL, use string __NULL__ instead
			Jelly_Cval_Field::$_default_variables_fix[$class] = $this->default === NULL ? '__NULL__' : $this->default;
		}
		
		// Get cached default value
		$this->default = Jelly_Cval_Field::$_default_variables_fix[$class];
		if ($this->default  === '__NULL__')
		{
			// Convert specific null value to real NULL
			$this->default = NULL;
		}
	}
	
}
