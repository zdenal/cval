<?php

defined('SYSPATH') or die('No direct script access.');

class Jelly_Cval_Model extends Jelly_Ext_Model
{
	
	/**
	 * As default try to find fields in cval/field dir.
	 * 
	 * Returns a view object that represents the field.
	 *
	 * If $prefix is an array, it will be used for the data
	 * and $prefix will be set to the default.
	 *
	 * @param   string        $name
	 * @param   string|array  $prefix
	 * @param   array         $data
	 * @return  View
	 */
	public function input($name, $prefix = 'cval/field', $data = array())
	{
		return parent::input($name, $prefix, $data);
	}
	
	/**
	 * Creates or updates the current record.
	 *
	 * If $key is passed, the record will be assumed to exist
	 * and an update will be executed, even if the model isn't loaded().
	 *
	 * @param   mixed  $key
	 * @return  $this
	 * */
	protected function _do_save($key = NULL)
	{
		// Run validation
		$data = $this->validate_save($key);
		
		// Run after validate call
		$data = $this->_after_validate_save_call($this->_loaded, $data);
		
		// Set the key to our id if it isn't set
		if ($this->_loaded)
		{
			$key = $this->_original[$this->_meta->primary_key()];
		}

		// These will be processed later
		$values = $relations = array();
		
		// Iterate through all fields in original incase any unchanged fields
		// have save() behavior like timestamp updating...
		foreach ($this->_changed + $this->_original as $column => $value)
		{
			// Filters may have been applied to data, so we should use that value
			if (array_key_exists($column, $data))
			{
				$value = $data[$column];
			}

			$field = $this->_meta->fields($column);
			
			// Only save in_db values
			if ($field->in_db)
			{	
				// Call before save method
				$value = $field->before_save($this, $value, (bool) $key);

				// See if field wants to alter the value on save()
				$value = $field->save($this, $value, (bool) $key);

				// Call after save method
				$value = $field->after_save($this, $value, (bool) $key);

				if ($field->needs_update($value, $this->_original[$column]))
				{
					// Value has changed (or has been changed by field:save())
					$values[$field->name] = $value;
				}
				else
				{
					// Insert defaults
					if ( ! $key AND ! $this->changed($field->name) AND ! $field->primary)
					{
						$values[$field->name] = $field->default;
					}
				}
			}
			elseif ($this->changed($column) AND $field instanceof Jelly_Field_Behavior_Saveable)
			{
				$relations[$column] = $value;
			}
		}

		// If we have a key, we're updating
		if ($key)
		{
			// Do we even have to update anything in the row?
			if ($values)
			{
				Jelly::update($this)
						->where(':unique_key', '=', $key)
						->set($values)
						->execute();
			}
		}
		else
		{	
			list($id) = Jelly::insert($this)
					->columns(array_keys($values))
					->values(array_values($values))
					->execute();

			// Gotta make sure to set this
			$this->_changed[$this->_meta->primary_key()] = $id;
		}

		$this->_audit($values);
		
		// Reset the saved data, since save() may have modified it
		$this->set($values);

		// Make it appear as original data instead of changed
		$this->_original = array_merge($this->_original, $this->_changed);
		
		// Iterate through all fields and call after save method
		foreach ($this->_original as $column => $value)
		{
			$field = $this->_meta->fields($column);
			
			if (empty($field))
			{
				continue;
			}
			// Call after save method
			$field->after_save_model($this, $value, (bool) $key);
		}

		// Set last changed fields
		$this->_last_changed = $this->_changed;

		// We're good!
		$this->_loaded = $this->_saved = TRUE;
		$this->_retrieved = $this->_changed = array();

		// Save the relations
		foreach ($relations as $column => $value)
		{
			$this->_meta->fields($column)->before_save($this, $value, (bool) $key);

			$this->_meta->fields($column)->save($this, $value, (bool) $key);

			$this->_meta->fields($column)->after_save($this, $value, (bool) $key);
		}

		return $this;
	}
	
	/**
	 * Validates the current state of the model.
	 *
	 * Only changed data is validated, unless $data is passed.
	 *
	 * @param   array	$data
	 * @param	bool	$return_array	return as array or as Validate object
	 * @throws  Validate_Exception
	 * @return  array
	 */
	public function validate($data = NULL)
	{
		if ($data === NULL)
		{
			$data = $this->_changed;
		}

		if (empty($data))
		{
			return $data;
		}

		// Create the validation object
		$data = Validate::factory($data);

		// If we are passing a unique key value through, add a filter to ensure it isn't removed
		if ($data->offsetExists(':unique_key'))
		{
			$data->filter(':unique_key', 'trim');
		}

		// Loop through all columns, adding rules where data exists
		foreach ($this->_meta->fields() as $column => $field)
		{
			// Do not add any rules for this field
			if ( ! $data->offsetExists($column))
			{
				continue;
			}

			$data->label($column, $field->label);

			// Add model object to filter as third parameter
			foreach ($field->filters as $filter)
			{
				$data->filter($column, $filter, array($this));
			}

			$data->rules($column, $field->rules);

			// Add model object to callback as third parameter
			foreach ($field->callbacks as $callback)
			{
				$data->callback($column, $callback, array($this));
			}
		}

		if ( ! $data->check())
		{
			throw Model_Validate_Exception::create($this, $data);
		}

		return $data->validated_array();
	}
	
	/**
	 * Deletes a single record.
	 *
	 * @param   $key  A key to use for non-loaded records
	 * @return  boolean
	 * */
	public function delete($key = NULL)
	{
		$result = FALSE;

		try
		{
			Database::instance()->transaction_start();

			$deleted = $this->_before_delete();
			if ($deleted !== FALSE)
			{
				$deleted = TRUE;
			}

			// Are we loaded? Then we're just deleting this record
			if ($deleted AND ($this->_loaded OR $key))
			{
				if ($this->_loaded)
				{
					$key = $this->id();
				}

				$this->_audit_delete();

				$result = Jelly::delete($this)
						->where(':unique_key', '=', $key)
						->execute();
			}

			$this->_after_delete_call_fields($deleted);

			$this->_after_delete($deleted);

			Database::instance()->transaction_commit();
		}
		catch (Exception $e)
		{
			Database::instance()->transaction_rollback();

			throw $e;
		}

		if ($deleted)
		{
			// Clear the object so it appears deleted anyway
			$this->clear();
		}

		return (boolean) $result;
	}
	
	/**
	 * Compares this model with another. 
	 * 
	 * @param	Jelly_Model $model
	 * @return	bool
	 */
	public function compare(Jelly_Model $model)
	{
		if ( ! $this->loaded() OR ! $model->loaded())
		{
			return FALSE;
		}
		return $this->id() == $model->id();
	}

}
