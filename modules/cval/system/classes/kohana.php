<?php defined('SYSPATH') or die('No direct script access.');

class Kohana extends Core_Core 
{
	/**
	 * Initializes the environment:
	 *
	 * - Disables register_globals and magic_quotes_gpc
	 * - Determines the current environment
	 * - Set global settings
	 * - Sanitizes GET, POST, and COOKIE variables
	 * - Converts GET, POST, and COOKIE variables to the global character set
	 *
	 * Any of the global settings can be set here:
	 *
	 * Type      | Setting    | Description                                    | Default Value
	 * ----------|------------|------------------------------------------------|---------------
	 * `string`  | environment| current environment name					   | `Kohana::$environment`
	 * `boolean` | errors     | use internal error and exception handling?     | `TRUE`
	 * `boolean` | profile    | do internal benchmarking?                      | `TRUE`
	 * `boolean` | caching    | cache the location of files between requests?  | `FALSE`
	 * `string`  | charset    | character set used for all input and output    | `"utf-8"`
	 * `string`  | base_url   | set the base URL for the application           | `"/"`
	 * `string`  | index_file | set the index.php file name                    | `"index.php"`
	 * `string`  | cache_dir  | set the cache directory path                   | `APPPATH."cache"`
	 * `integer` | cache_life | set the default cache lifetime                 | `60`
	 * `string`  | error_view | set the error rendering view                   | `"kohana/error"`
	 *
	 * @throws  Kohana_Exception
	 * @param   array   global settings
	 * @return  void
	 * @uses    Kohana::globals
	 * @uses    Kohana::sanitize
	 * @uses    Kohana::cache
	 * @uses    Profiler
	 */
	public static function init(array $settings = NULL)
	{
		if (Kohana::$_init)
		{
			// Do not allow execution twice
			return;
		}

		// Check for environment value
		if (isset($settings['environment']))
		{
			Kohana::$environment = $settings['environment'];
		}
		
		// Autoset profile and caching if is not set explicit according environment
		if ( ! isset($settings['profile']))
		{
			$settings['profile'] = Kohana::$environment !== Kohana::PRODUCTION;
		}
		if ( ! isset($settings['caching']))
		{
			$settings['caching'] = Kohana::$environment === Kohana::PRODUCTION;
		}
		
		// Call parent
		parent::init($settings);
	}
	
	/**
	 * Returns an HTML string or print_r output when Kohana::$is_cli 
	 * of debugging information about any number of
	 * variables:
	 *
	 *     // Displays the type and value of each variable
	 *     echo Kohana::debug($foo, $bar, $baz);
	 *
	 * @param   mixed   variable to debug
	 * @param   ...
	 * @return  string
	 */
	public static function debug()
	{
		if (func_num_args() === 0)
			return;

		// Get all passed variables
		$variables = func_get_args();

		$output = array();
		foreach ($variables as $var)
		{
			$output[] = Kohana::_dump($var, 1024);
		}

		return Kohana::$is_cli
			? implode("\n", $output)."\n"
			: '<pre class="debug">'.implode("\n", $output).'</pre>';
	}
	
	/**
	 * Helper for Kohana::dump(), handles recursion in arrays and objects.
	 *
	 * @param   mixed    variable to dump
	 * @param   integer  maximum length of strings
	 * @param   integer  recursion level (internal)
	 * @return  string
	 */
	protected static function _dump( & $var, $length = 128, $level = 0)
	{
		if (Kohana::$is_cli)
		{
			return Kohana::_dump_cli($var, $length, $level);
		}
		
		return parent::_dump($var, $length, $level);
	}
	
	/**
	 * Helper for Kohana::dump(), handles recursion in arrays and objects.
	 *
	 * @param   mixed    variable to dump
	 * @param   integer  maximum length of strings
	 * @param   integer  recursion level (internal)
	 * @return  string
	 */
	protected static function _dump_cli( & $var, $length = 128, $level = 0)
	{
		if ($var === NULL)
		{
			return 'NULL';
		}
		elseif (is_bool($var))
		{
			return 'bool '.($var ? 'TRUE' : 'FALSE');
		}
		elseif (is_float($var))
		{
			return 'float '.$var;
		}
		elseif (is_resource($var))
		{
			if (($type = get_resource_type($var)) === 'stream' AND $meta = stream_get_meta_data($var))
			{
				$meta = stream_get_meta_data($var);

				if (isset($meta['uri']))
				{
					$file = $meta['uri'];

					if (function_exists('stream_is_local'))
					{
						// Only exists on PHP >= 5.2.4
						if (stream_is_local($file))
						{
							$file = Kohana::debug_path($file);
						}
					}

					return 'resource('.$type.') '.$file;
				}
			}
			else
			{
				return 'resource('.$type.')';
			}
		}
		elseif (is_string($var))
		{
			// Clean invalid multibyte characters. iconv is only invoked
			// if there are non ASCII characters in the string, so this
			// isn't too much of a hit.
			$var = UTF8::clean($var);

			if (UTF8::strlen($var) > $length)
			{
				// Encode the truncated string
				$str = UTF8::substr($var, 0, $length).' ...';
			}
			else
			{
				// Encode the string
				$str = $var;
			}

			return 'string('.strlen($var).') "'.$str.'"';
		}
		elseif (is_array($var))
		{
			$output = array();

			// Indentation for this variable
			$space = str_repeat($s = '    ', $level);

			static $marker;

			if ($marker === NULL)
			{
				// Make a unique marker
				$marker = uniqid("\x00");
			}

			if (empty($var))
			{
				// Do nothing
			}
			elseif (isset($var[$marker]))
			{
				$output[] = "(\n$space$s*RECURSION*\n$space)";
			}
			elseif ($level < 5)
			{
				$output[] = "(";

				$var[$marker] = TRUE;
				foreach ($var as $key => & $val)
				{
					if ($key === $marker) continue;
					if ( ! is_int($key))
					{
						$key = '"'.$key.'"';
					}

					$output[] = "$space$s$key => ".Kohana::_dump_cli($val, $length, $level + 1);
				}
				unset($var[$marker]);

				$output[] = "$space)";
			}
			else
			{
				// Depth too great
				$output[] = "(\n$space$s...\n$space)";
			}

			return 'array('.count($var).') '.implode("\n", $output);
		}
		elseif (is_object($var))
		{
			// Copy the object as an array
			$array = (array) $var;

			$output = array();

			// Indentation for this variable
			$space = str_repeat($s = '    ', $level);

			$hash = spl_object_hash($var);

			// Objects that are being dumped
			static $objects = array();

			if (empty($var))
			{
				// Do nothing
			}
			elseif (isset($objects[$hash]))
			{
				$output[] = "{\n$space$s*RECURSION*\n$space}";
			}
			elseif ($level < 10)
			{
				$output[] = "{";

				$objects[$hash] = TRUE;
				foreach ($array as $key => & $val)
				{
					if ($key[0] === "\x00")
					{
						// Determine if the access is protected or protected
						$access = ''.($key[1] === '*' ? 'protected' : 'private').'';

						// Remove the access level from the variable name
						$key = substr($key, strrpos($key, "\x00") + 1);
					}
					else
					{
						$access = 'public';
					}

					$output[] = "$space$s$access $key => ".Kohana::_dump_cli($val, $length, $level + 1);
				}
				unset($objects[$hash]);

				$output[] = "$space}";
			}
			else
			{
				// Depth too great
				$output[] = "{\n$space$s...\n$space}";
			}

			return 'object '.get_class($var).'('.count($array).') '.implode("\n", $output);
		}
		else
		{
			return ''.gettype($var).' '.print_r($var, TRUE);
		}
	}
}
