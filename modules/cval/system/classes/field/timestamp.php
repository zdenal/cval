<?php defined('SYSPATH') or die('No direct script access.');

class Field_Timestamp extends Jelly_Ext_Field_Timestamp
{
	
	/**
	 * Convert string timestamp to time
	 * 
	 * @param	string	$value
	 * @return	int 
	 */
	protected function _strtotime($value, Jelly_Model $model = NULL)
	{	
		// Convert to timestamp
		$timestamp = strtotime($value);
		
		// Move timestamp only when object is constructed, not when DB data
		// are set to object
		if ($model AND $model->constructed())
		{
			// Get timezones offest
			$offset = Date::offset($this->_timezone(), Date::timezone(), $timestamp);

			// Move timestamp for given offset
			$timestamp = $timestamp - $offset;
		}
		
		return $timestamp;
	}
	
	protected function _timezone()
	{
		return Cval_Date::timezone();
	}
	
	public function to_display_timezone($timestamp)
	{
		return Cval_Date::to_timezone($timestamp);
	}
	
	public function display_value($value)
	{	
		// Move timestamp to display timezone
		$value = $this->to_display_timezone($value);
		
		// Do not format empty values
		if (empty($value))
		{
			return '';
		}
		
		// Return formatted date
		return Date::format($value, $this->pretty_format);
	}
	
	/**
	 * Tests if field needs update in DB. It is called in model save process.
	 * 
	 * @param	mixed	Actual field value
	 * @param	mixed	Original field value from DB
	 * @return	bool 
	 */
	public function needs_update($value, $original)
	{
		// We need to convert database string back to timestamp, because
		// original value also passed this way
		return $this->set($value) !== $original;
	}

}