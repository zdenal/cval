<?php defined('SYSPATH') or die('No direct script access.');

class Field_Boolean extends Jelly_Ext_Field_Boolean
{
	/**
	 * @var	boolean		Defaults to FALSE
	 */
	public $default = FALSE;
	
	/**
	 * Tests if field needs update in DB. It is called in model save process.
	 * 
	 * @param	mixed	Actual field value
	 * @param	mixed	Original field value from DB
	 * @return	bool 
	 */
	public function needs_update($value, $original)
	{
		// Do not compare boolean fields exact, just normally
		return $value != $original;
	}
}