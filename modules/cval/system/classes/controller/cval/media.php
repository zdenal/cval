<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Cval media controller
 *
 * @package     Cval
 * @category    Controller
 * @author      Zdennal
 */
class Controller_Cval_Media extends Controller {

	protected $_media_headers = array();
	
	public function action_file()
	{
		$request = Request::instance();
		$file = $request->param('file');

		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$file_no_ext = substr($file, 0, -(strlen($ext) + 1));

		$response = FALSE;

		$file_path = NULL;
		
		if ($view = $this->find($file))
		{
			$response = $this->response_from_view($view);
		}
		elseif (($lang = $request->route->i18n_lang())
					AND ($path = Kohana::find_file('media', $lang.'/cval/'.$file_no_ext, $ext)))
		{
			$file_path = $path;
			$response = $this->response_from_file($path, 'cval/'.$file_no_ext, $ext, $lang);
		}
		elseif ($path = Kohana::find_file('media', 'cval/'.$file_no_ext, $ext))
		{
			$file_path = $path;
			$response = $this->response_from_file($path, 'cval/'.$file_no_ext, $ext);
		}

		if ($response === FALSE)
		{
			Kohana::$log->add(Kohana::ERROR, 'Cval media controller error while loading file, '.$file);
			$request->status = 404;
		}
		else
		{
			$request->response = $response;
			$request->headers['Content-Type'] = File::mime_by_ext($ext);
		}
		
		if ($file_path)
		{
			$this->_add_file_headers($path);
		}
	}

	public function find($file)
	{
		try
		{
			$view = View::factory('cval/media/'.$file);
		}
		catch (Kohana_View_Exception $e)
		{
			$view = FALSE;
		}

		return $view;
	}
	
	public function response_from_view(View $view)
	{
		return $view->render();
	}
	
	public function response_from_file($path, $relative_path_no_ext, $ext, $lang = FALSE)
	{
		$response = file_get_contents($path);
		$closing = NULL;
		
		$last_modified = Arr::get(File::info($path, 'date'), 'date');
		
		$extension_points = Cval_Extend::point_media($path, $relative_path_no_ext, $ext, $lang = FALSE);
		
		if (count($extension_points))
		{
			$response = $this->_prepare_file_extend($response);

			if (is_array($response))
			{
				$closing = $response[1];
				$response = $response[0];
			}	
		}
		
		foreach ($extension_points as $extension_point)
		{
			$tmp_last_modified = Arr::get(File::info($extension_point, 'date'), 'date');
			
			if ($tmp_last_modified > $last_modified)
			{
				$last_modified = $tmp_last_modified;
			}
			
			$response .= "\n".file_get_contents($extension_point);
		}
		
		if ($closing !== NULL)
		{
			$response .= "\n".$closing;
		}
		
		$this->_media_headers['Last-Modified'] = gmdate('D, d M Y H:i:s', $last_modified).' GMT';
		
		return $response;
	}
	
	protected function _add_file_headers($path)
	{
		if (empty($this->_media_headers['Last-Modified']))
		{
			$last_modified = Arr::get(File::info($path, 'date'), 'date');
		
			$this->_media_headers['Last-Modified'] = gmdate('D, d M Y H:i:s', $last_modified).' GMT';
		}
		
		$this->request->headers['Last-Modified'] = $this->_media_headers['Last-Modified'];
	}
	
	protected function _prepare_file_extend($response)
	{
		$open = substr($response, 0, 20);
		
		if (($extconf_pos = strpos($open, "KEXT[on]")) !== FALSE)
		{
			if (($ext_pos = strpos($response, "KEXTSTART", $extconf_pos+1)) !== FALSE)
			{	
				return array(
					substr($response, 0, $ext_pos+9)."\n",
					substr($response, $ext_pos+9)
				);
			}
		}
		
		return $response;
	}

}

