<?php defined('SYSPATH') or die('No direct script access.');

class Remote extends Kohana_Remote 
{
	/**
	 * Returns the output of a remote URL. Second parameter is array 
	 * of POST data. Any [curl option](http://php.net/curl_setopt)
	 * may be used in array for third parameter. 
	 *
	 *     // Do a POST request
	 *     $data = Remote::post($url, $data);
	 *
	 * @param   string   remote URL
	 * @param	array	 post data
	 * @param   array    curl options
	 * @param	bool	 FALSE for returning error info array instead of exception
	 * @return  string
	 * @return  array	 If error occurs and third param is FALSE
	 * @throws  Kohana_Exception
	 * @uses	Remote::get()
	 */
	public static function post($url, array $data = array(), array $options = array(), $error_exception = TRUE)
	{	
		$merged_options = array(
			CURLOPT_POST		=> TRUE,
			CURLOPT_POSTFIELDS	=> http_build_query($data)
		);
		
		foreach ($options as $key => $value)
		{
			$merged_options[$key] = $value;
		}
		
		// Just for debugging
		if (FALSE)
		{
			echo "\n".$url.'?'.$merged_options[CURLOPT_POSTFIELDS]."\n\n";
			exit;
		}
		
		Kohana::$log->add(Kohana::INFO, '[Remote::post] '.$url.'?'.$merged_options[CURLOPT_POSTFIELDS]);
		
		return Remote::get($url, $merged_options, $error_exception);
	}
	
	/**
	 * Returns the output of a remote URL. Any [curl option](http://php.net/curl_setopt)
	 * may be used.
	 *
	 *     // Do a simple GET request
	 *     $data = Remote::get($url);
	 *
	 *     // Do a POST request
	 *     $data = Remote::get($url, array(
	 *         CURLOPT_POST       => TRUE,
	 *         CURLOPT_POSTFIELDS => http_build_query($array),
	 *     ));
	 *
	 * @param   string   remote URL
	 * @param   array    curl options
	 * @param	bool	 FALSE for returning error info array instead of exception
	 * @return  string
	 * @return  array	 If error occurs and third param is FALSE
	 * @throws  Kohana_Exception
	 */
	public static function get($url, array $options = NULL, $error_exception = TRUE)
	{
		//echo Kohana::debug_exit($url, $options);
		if ($options === NULL)
		{
			// Use default options
			$options = Remote::$default_options;
		}
		else
		{
			// Add default options
			$options = $options + Remote::$default_options;
		}

		// The transfer must always be returned
		$options[CURLOPT_RETURNTRANSFER] = TRUE;

		// Open a new remote connection
		$remote = curl_init($url);

		// Set connection options
		if ( ! curl_setopt_array($remote, $options))
		{
			throw new Kohana_Exception('Failed to set CURL options, check CURL documentation: :url',
				array(':url' => 'http://php.net/curl_setopt_array'));
		}

		// Get the response
		$response = curl_exec($remote);

		// Get the response information
		$code = curl_getinfo($remote, CURLINFO_HTTP_CODE);

		if ($code AND $code < 200 OR $code > 299)
		{
			$error = $response;
		}
		elseif ($response === FALSE)
		{
			$error = curl_error($remote);
		}

		// Close the connection
		curl_close($remote);
		
		if (isset($error))
		{
			if ($error_exception)
			{
				throw new Kohana_Exception('Error fetching remote :url [ status :code ]',
					array(':url' => $url, ':code' => $code));
			}
			
			// Return info array instead of throwing exception
			return array(
				'url' => $url,
				'code' => $code,
				'error' => $error,
				'response' => $response
			);
		}

		return $response;
	}
}
