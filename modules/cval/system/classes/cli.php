<?php

defined('SYSPATH') or die('No direct script access.');

class CLI extends Kohana_CLI
{
	/**
	 * Starts command and returns it response.
	 * Only Unix version.
	 * 
	 * @param	string	Command to execute
	 * @return  string	Complete response of cli call
	 */
	public static function run($cmd)
	{
		$output = array();
		exec($cmd.' 2>&1', $output);
		return implode("\n", $output);
	}

	/**
	 * Starts command in background, i.e. not waiting for its response.
	 * Can be used both under Windows and Unix
	 * 
	 * @param	string	Command to execute
	 * @return  void
	 */
	public static function run_in_background($cmd)
	{
		if (Kohana::$is_windows)
		{
			pclose(popen("start /B ".$cmd, "r"));
		}
		else
		{
			exec($cmd." > /dev/null &");
		}
	}
	
	/**
	 * Starts command in background, i.e. not waiting for its response.
	 * Can be used both under Windows and Unix
	 * 
	 * @param	string	Command to execute
	 * @return  void
	 */
	public static function internal_call($uri = NULL, array $options = array())
	{
		// Construct command
		$cmd = CLI::internal_call_cmd($uri, $options);
		// Start running procces and return output
		return CLI::run($cmd);
	}
	
	/**
	 * Starts command in background, i.e. not waiting for its response.
	 * Can be used both under Windows and Unix
	 * 
	 * @param	string	Command to execute
	 * @return  void
	 */
	public static function internal_call_in_background($uri = NULL, array $options = array())
	{
		// Construct command
		$cmd = CLI::internal_call_cmd($uri, $options);
		// Start running procces in background
		CLI::run_in_background($cmd);
	}
	
	/**
	 * Starts command in background, i.e. not waiting for its response.
	 * Can be used both under Windows and Unix
	 * 
	 * @param	string	Command to execute
	 * @return  void
	 */
	public static function internal_call_cmd($uri = NULL, array $options = array())
	{
		$cmd = 'php index.php';
		
		$options['uri'] = $uri ? $uri : "";
		
		foreach ($options as $option => $value)
		{
			if (is_array($value))
			{
				$value = http_build_query($value);
			}
			
			$options[$option] = ('--'.$option.'="'.$value.'"');
		}
		
		if ( ! empty($options))
		{
			$cmd .= (' '.implode(' ', $options));
		}
		
		return $cmd;
	}

}