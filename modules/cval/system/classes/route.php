<?php defined('SYSPATH') or die('No direct script access.');

class Route extends Core_Route 
{
	/**
	 * Generates a URI for the current route based on the parameters given.
	 *
	 *     // Using the "default" route: "users/profile/10"
	 *     $route->uri(array(
	 *         'controller' => 'users',
	 *         'action'     => 'profile',
	 *         'id'         => '10'
	 *     ));
	 *
	 * @param   array   URI parameters
	 * @param	string	language
	 * @return  string
	 * @throws  Kohana_Exception
	 * @uses    Route::REGEX_Key
	 */
	public function uri(array $params = NULL, $lang = NULL)
	{
		$uri = parent::uri($params);
		
		// Add dash to uri if specified
		if ($this->config('route_dash') AND Cval::instance()->config('route_dash'))
		{
			$uri = '#'.($this->config('route_dash') !== TRUE ? ($this->config('route_dash').'=') : '').$uri;
		}
		
		return $uri;
	}
}
