<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
	'default' => array
	(
		'connection' => array(
			'dsn'		 => 'mysql:host=localhost;dbname=cval',
			'username'   => 'root',
			'password'   => 'root',
		),
	)
);