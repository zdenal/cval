<?php

/**
 * The directory in which your application specific resources are located.
 * The application directory must contain the bootstrap.php file.
 *
 * @see  http://kohanaframework.org/guide/about.install#application
 */
$application = 'application';

/**
 * The directory in which your modules are located.
 *
 * @see  http://kohanaframework.org/guide/about.install#modules
 */
$modules = 'modules';

/**
 * The directory in which cval modules are located.
 */
$cvalmodules = 'modules/cval';

/**
 * The directory in which core modules are located.
 */
$coremodules = 'modules';

/**
 * The directory in which the Kohana resources are located. The system
 * directory must contain the classes/kohana.php file.
 *
 * @see  http://kohanaframework.org/guide/about.install#system
 */
$system = 'system';

/**
 * The directory in which boot files are located.
 */
$boot = 'boot';

/**
 * The default extension of resource files. If you change this, all resources
 * must be renamed to use the new extension.
 *
 * @see  http://kohanaframework.org/guide/about.install#ext
 */
define('EXT', '.php');

/**
 * Set the PHP error reporting level. If you set this in php.ini, you remove this.
 * @see  http://php.net/error_reporting
 *
 * When developing your application, it is highly recommended to enable notices
 * and strict warnings. Enable them by using: E_ALL | E_STRICT
 *
 * In a production environment, it is safe to ignore notices and strict warnings.
 * Disable them by using: E_ALL ^ E_NOTICE
 *
 * When using a legacy application with PHP >= 5.3, it is recommended to disable
 * deprecated notices. Disable with: E_ALL & ~E_DEPRECATED
 */
error_reporting(E_ALL | E_STRICT);

/**
 * End of standard configuration! Changing any of the code below should only be
 * attempted by those with a working knowledge of Kohana internals.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 */

// Set the full path to the docroot
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);

// Make the application relative to the docroot
if ( ! is_dir($application) AND is_dir(DOCROOT.$application))
	$application = DOCROOT.$application;

// Make the modules relative to the docroot
if ( ! is_dir($modules) AND is_dir(DOCROOT.$modules))
	$modules = DOCROOT.$modules;

// Make the modules relative to the docroot
if ( ! is_dir($cvalmodules) AND is_dir(DOCROOT.$cvalmodules))
	$cvalmodules = DOCROOT.$cvalmodules;

// Make the coremodules relative to the docroot
if ( ! is_dir($coremodules) AND is_dir(DOCROOT.$coremodules))
	$coremodules = DOCROOT.$coremodules;	
	
// Make the system relative to the docroot
if ( ! is_dir($system) AND is_dir(DOCROOT.$system))
	$system = DOCROOT.$system;

// Make the boot relative to the docroot
if ( ! is_dir($boot) AND is_dir(DOCROOT.$boot))
	$boot = DOCROOT.$boot;

// Define the absolute paths for configured directories
define('APPPATH', realpath($application).DIRECTORY_SEPARATOR);
define('MODPATH', realpath($modules).DIRECTORY_SEPARATOR);
define('CVALMODPATH', realpath($cvalmodules).DIRECTORY_SEPARATOR);
define('COREMODPATH', realpath($coremodules).DIRECTORY_SEPARATOR);
define('SYSPATH', realpath($system).DIRECTORY_SEPARATOR);
define('BOOTPATH', realpath($boot).DIRECTORY_SEPARATOR);

// Clean up the configuration vars
unset($application, $modules, $cvalmodules, $coremodules, $system, $boot);

if (file_exists('install'.EXT))
{
	// Load the installation check
	return include 'install'.EXT;
}

if (is_file(APPPATH.'base'.EXT))
{
	// Load the application Kohana class
	require APPPATH.'base'.EXT;
}
elseif (is_file(MODPATH.'system-ext/base'.EXT))
{
	// Load the coremod Kohana class
	require MODPATH.'system-ext/base'.EXT;
}
elseif (is_file(COREMODPATH.'system-ext/base'.EXT))
{
	// Load the coremod Kohana class
	require COREMODPATH.'system-ext/base'.EXT;
}
else
{
	// Load the base, low-level functions
	require SYSPATH.'base'.EXT;
}

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/core/core'.EXT))
{
	// Load the application Kohana class
	require APPPATH.'classes/core/core'.EXT;	
}
elseif (is_file(MODPATH.'system-ext/classes/core/core'.EXT))
{
	// Load the mod Kohana class
	require MODPATH.'system-ext/classes/core/core'.EXT;
}
elseif (is_file(CVALMODPATH.'system/classes/core/core'.EXT))
{
	// Load the cvalmod Kohana class
	require CVALMODPATH.'system/classes/core/core'.EXT;
}
elseif (is_file(COREMODPATH.'system-ext/classes/core/core'.EXT))
{
	// Load the coremod Kohana class
	require COREMODPATH.'system-ext/classes/core/core'.EXT;
}

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
elseif (is_file(MODPATH.'system-ext/classes/kohana'.EXT))
{
	// Coremod extends the core
	require MODPATH.'system-ext/classes/kohana'.EXT;
}
elseif (is_file(CVALMODPATH.'system/classes/kohana'.EXT))
{
	// Cvalmod extends the core
	require CVALMODPATH.'system/classes/kohana'.EXT;
}
elseif (is_file(COREMODPATH.'system-ext/classes/kohana'.EXT))
{
	// Coremod extends the core
	require COREMODPATH.'system-ext/classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

// Bootstrap the application
require BOOTPATH.'bootstrap'.EXT;
