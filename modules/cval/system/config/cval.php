<?php

return array(

	'version' => '0.1',
	'release' => '120325',
	
	'defaults' => array(
		'theme' => 'cval-light',
		'icon_text' => TRUE,
		'lang' => 'en',
		'email_sharing' => 50, // Local
		
		'timezone' => 'UTC',
		'date_month_second' => 0,
		'time_24_hour' => 0,
		'first_day' => 0,
		
		// Scheduling
		'schedule_email_on_inbound' => TRUE,
		'schedule_plan_min_length' => 30,
		'schedule_plan_min_length_units' => 'm'
	),
	
	/**
	 * Set this TRUE if you want for routes with config 'route_dash' = TRUE,
	 * prepend dash to uri.
	 */
	'route_dash' => TRUE,
	
	'error' => array(
		'view' => 'cval/error'
	)

);