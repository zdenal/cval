<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(

	/**
	 * This domain will be used when from email contains no '@' character.
	 * If this is empty $_SERVER['SERVER_NAME'] will be used.
	 */
	'default_from_domain' => NULL,
	
	'from' => array(
		'default' => array('email' => 'support', 'name' => 'Cval Support'),
		'scheduler' => array('email' => 'scheduler', 'name' => 'Cval Scheduler'),
	),
	
	/**
	 * Email templates config.
	 * Each template is defined with key, which is used in Email class.
	 * Usage:
	 *
	 *		test' => array(
	 *			'from' => 'test',
	 *			'subject' => 'Test',
	 *			'view' => 'email/templates/test',
	 *			'html' => FALSE
	 *		),
	 *
	 * Definition of each variable:
	 *
	 *		from		string|array	sender email (and name) or named config
	 *		subject		string			message subject
	 *		view		string			view used for message body
	 *		html		boolean			send email as HTML
	 */
	'templates' => array()

);