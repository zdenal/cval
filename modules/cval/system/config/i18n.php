<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
	'multilang' => FALSE,
	'multilang_media' => FALSE,
	'default_lang' => 'en',
	'enabled_langs' => array('en'),
	'available_langs' => array('en'),

	'lang_labels' => array(
		'en' => 'English',
	)
);