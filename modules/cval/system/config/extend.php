<?php

return array(

	/**
	 * Array of directories used on extension points in classes folder.
	 * 
	 * Example:
	 * 
	 * 'directories' => array(
	 *		// Try to find files starting with 'Extend_'.My_Directory.'_'.$extended_class_name
	 *		'my/directory' => TRUE
	 * );
	 */
	'directories' => array(),

	/**
	 * Array of directories used on extension points in media folder.
	 * 
	 * Example:
	 * 
	 * 'media_directories' => array(
	 *		// Try to find files placed in 'media/extend/my/directory'
	 *		'my/directory' => TRUE
	 * );
	 */
	'media_directories' => array(),
	
);