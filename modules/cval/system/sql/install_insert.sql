
SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;

INSERT INTO `roles` (`id`, `name`, `description`, `label`) VALUES
(1, 'user', 'Login privileges, granted after account verification', 'Login'),
(2, 'admin', 'Administrative user, has access to everything.', 'Admin'),
(3, 'cval', 'Has access to Cval restricted area.', 'Cval');

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

INSERT INTO `users` (`id`, `username`, `password`, `email`, `label`, `verified`) VALUES
(1, 'admin@cval.localhost', 'cbf439bb43f3add7480e5aaf73c1aa8b993a00dac155dbcfb5', 'admin@cval.localhost', 'Admin', 1);

SET FOREIGN_KEY_CHECKS=1;
COMMIT;