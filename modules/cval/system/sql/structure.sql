SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;

DROP TABLE IF EXISTS `cval_configs`;

CREATE TABLE IF NOT EXISTS `cval_configs` (
	`id` VARCHAR(255) NOT NULL,
	`value` VARCHAR(255),
	`type` VARCHAR(10) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`id`),
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_cval_configs_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_cval_configs_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users` (
	`id` BIGINT(19) UNSIGNED AUTO_INCREMENT NOT NULL,
	`label` VARCHAR(255) NOT NULL,
	`username` VARCHAR(255) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`password` VARCHAR(50) NOT NULL,
	`logins` BIGINT(19) DEFAULT 0,
	`last_login` DATETIME,
	`verified` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`verification_id` VARCHAR(32),
	`blocked` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`block_message` TEXT,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`id`),
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_users_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_users_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

	
DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users`(
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`role_id` BIGINT(19) UNSIGNED NOT NULL,
	PRIMARY KEY (`user_id`,`role_id`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_roles_users_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_role_id` (`role_id`),
	CONSTRAINT `fk_roles_users_role_id_roles`
		FOREIGN KEY (`role_id`)
		REFERENCES `roles` (`id`)
		ON DELETE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roles`;

CREATE TABLE IF NOT EXISTS `roles` (
	`id` BIGINT(19) UNSIGNED AUTO_INCREMENT NOT NULL,
	`label` VARCHAR(255),
	`name` VARCHAR(32) NOT NULL,
	`parent_role_id` BIGINT(19) UNSIGNED,
	`description` TEXT,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`id`),
	INDEX `idx_parent_role_id` (`parent_role_id`),
	CONSTRAINT `fk_roles_parent_role_id_roles`
		FOREIGN KEY (`parent_role_id`)
		REFERENCES `roles` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_roles_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_roles_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_tokens`;

CREATE TABLE IF NOT EXISTS `user_tokens` (
	`id` BIGINT(19) UNSIGNED AUTO_INCREMENT NOT NULL,
	`token` VARCHAR(32),
	`user_id` BIGINT(19) UNSIGNED,
	`user_agent` VARCHAR(50),
	`created` TIMESTAMP,
	`expires` TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_user_tokens_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE IF NOT EXISTS `user_profiles` (
	`id` BIGINT(19) UNSIGNED NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`user_profile_shared_id` VARCHAR(40) NOT NULL,
	`theme` VARCHAR(50) NOT NULL DEFAULT 'cval-light',
	`icon_text` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`lang` VARCHAR(2) NOT NULL DEFAULT 'en',
	`timezone` VARCHAR(50) NOT NULL DEFAULT 'UTC',
	`is_timezone_set` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`date_month_second` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`time_24_hour` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`first_day` TINYINT(1) NOT NULL DEFAULT 0,
	`email_sharing` BIGINT(19) DEFAULT 50,
	`schedule_email_on_inbound` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`schedule_plan_min_length` BIGINT(19) NOT NULL DEFAULT 30,
	`schedule_plan_min_length_units` VARCHAR(50) NOT NULL DEFAULT 'm',
	PRIMARY KEY (`id`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_user_profiles_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_user_profile_shared_id` (`user_profile_shared_id`),
	CONSTRAINT `fk_user_profiles_user_profile_shared_id_user_profiles_shared`
		FOREIGN KEY (`user_profile_shared_id`)
		REFERENCES `user_profiles_shared` (`idmpk`)
		ON DELETE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_profiles_shared`;

CREATE TABLE IF NOT EXISTS `user_profiles_shared` (
	`idmpk` VARCHAR(40) NOT NULL,
	`location_id` BIGINT(19) UNSIGNED NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`is_remote` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`hash` VARCHAR(32) NOT NULL,
	`deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`blocked` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`label` VARCHAR(255) NOT NULL,
	`email` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_location_id` (`location_id`),
	CONSTRAINT `fk_user_profiles_shared_location_id_locations`
		FOREIGN KEY (`location_id`)
		REFERENCES `locations` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_user_profiles_shared_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_user_profiles_shared_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user_pairs`;

CREATE TABLE IF NOT EXISTS `user_pairs` (
	`idmpk` VARCHAR(61) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`user_profile_shared_id` VARCHAR(40) NOT NULL,
	`paired` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`pairing_key` VARCHAR(32),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_user_pairs_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_user_profile_shared_id` (`user_profile_shared_id`),
	CONSTRAINT `fk_user_pairs_user_profile_shared_id_user_profiles_shared`
		FOREIGN KEY (`user_profile_shared_id`)
		REFERENCES `user_profiles_shared` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_user_pairs_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_user_pairs_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `locations`;

CREATE TABLE IF NOT EXISTS `locations` (
	`id` BIGINT(19) UNSIGNED AUTO_INCREMENT NOT NULL,
	`url` VARCHAR(1000) NOT NULL,
	`pairing_key` VARCHAR(32),
	`is_paired` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_remote` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`is_responding` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`remote_block` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`remote_request` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`local_block` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`local_request` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`label` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`id`),
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_locations_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_locations_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE IF NOT EXISTS `contacts` (
	`idmpk` VARCHAR(61) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`user_profile_shared_id` VARCHAR(40) NOT NULL,
	`is_favorite` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`label` VARCHAR(255) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_contacts_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_user_profile_shared_id` (`user_profile_shared_id`),
	CONSTRAINT `fk_contacts_user_profile_shared_id_user_profiles_shared`
		FOREIGN KEY (`user_profile_shared_id`)
		REFERENCES `user_profiles_shared` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_contacts_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_contacts_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

	
DROP TABLE IF EXISTS `contact_groups_contacts`;

CREATE TABLE `contact_groups_contacts`(
	`contact_id` VARCHAR(61) NOT NULL,
	`contact_group_id` VARCHAR(40) NOT NULL,
	PRIMARY KEY (`contact_id`,`contact_group_id`),
	INDEX `idx_contact_id` (`contact_id`),
	CONSTRAINT `fk_contact_groups_contacts_contact_id_contacts`
		FOREIGN KEY (`contact_id`)
		REFERENCES `contacts` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_contact_group_id` (`contact_group_id`),
	CONSTRAINT `fk_contact_groups_contacts_contact_group_id_contact_groups`
		FOREIGN KEY (`contact_group_id`)
		REFERENCES `contact_groups` (`idmpk`)
		ON DELETE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `contact_groups`;

CREATE TABLE IF NOT EXISTS `contact_groups` (
	`idmpk` VARCHAR(40) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`label` VARCHAR(255) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_contact_groups_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_contact_groups_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_contact_groups_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedules`;

CREATE TABLE IF NOT EXISTS `schedules` (
	`idmpk` VARCHAR(61) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`organizer_id` VARCHAR(40) NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`hash` VARCHAR(32) NOT NULL,
	`deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_outgoing` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_unread` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_favorite` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_unsync` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`has_contacts` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`has_plan` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`has_date` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`state` TINYINT(3) NOT NULL DEFAULT 10,
	`start` DATETIME,
	`end` DATETIME,
	`length` BIGINT(19),
	`update_hash` VARCHAR(32),
	`label` VARCHAR(255) NOT NULL,
	`description` TEXT,
	`place` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_schedules_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_organizer_id` (`organizer_id`),
	CONSTRAINT `fk_schedules_organizer_id_user_profiles_shared`
		FOREIGN KEY (`organizer_id`)
		REFERENCES `user_profiles_shared` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedules_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedules_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

	
DROP TABLE IF EXISTS `schedule_groups_schedules`;

CREATE TABLE `schedule_groups_schedules`(
	`schedule_id` VARCHAR(61) NOT NULL,
	`schedule_group_id` VARCHAR(40) NOT NULL,
	PRIMARY KEY (`schedule_id`,`schedule_group_id`),
	INDEX `idx_schedule_id` (`schedule_id`),
	CONSTRAINT `fk_schedule_groups_schedules_schedule_id_schedules`
		FOREIGN KEY (`schedule_id`)
		REFERENCES `schedules` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_schedule_group_id` (`schedule_group_id`),
	CONSTRAINT `fk_schedule_groups_schedules_schedule_group_id_schedule_groups`
		FOREIGN KEY (`schedule_group_id`)
		REFERENCES `schedule_groups` (`idmpk`)
		ON DELETE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_groups`;

CREATE TABLE IF NOT EXISTS `schedule_groups` (
	`idmpk` VARCHAR(40) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`label` VARCHAR(255) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_schedule_groups_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedule_groups_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedule_groups_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_plans`;

CREATE TABLE IF NOT EXISTS `schedule_plans` (
	`id` VARCHAR(61) NOT NULL,
	`schedule_id` VARCHAR(61) NOT NULL,
	`start_minutes` BIGINT(19) NOT NULL DEFAULT 480,
	`end_minutes` BIGINT(19) NOT NULL DEFAULT 960,
	`min_length` BIGINT(19) NOT NULL DEFAULT 30,
	`min_length_units` VARCHAR(50) NOT NULL DEFAULT 'm',
	`skip_organizer` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_negotiated` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`id`),
	INDEX `idx_schedule_id` (`schedule_id`),
	CONSTRAINT `fk_schedule_plans_schedule_id_schedules`
		FOREIGN KEY (`schedule_id`)
		REFERENCES `schedules` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedule_plans_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedule_plans_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_plan_periods`;

CREATE TABLE IF NOT EXISTS `schedule_plan_periods` (
	`idmpk` VARCHAR(82) NOT NULL,
	`schedule_plan_id` VARCHAR(61) NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`start` DATETIME NOT NULL,
	`end` DATETIME NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_schedule_plan_id` (`schedule_plan_id`),
	CONSTRAINT `fk_schedule_plan_periods_schedule_plan_id_schedule_plans`
		FOREIGN KEY (`schedule_plan_id`)
		REFERENCES `schedule_plans` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedule_plan_periods_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedule_plan_periods_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_plan_negotiated_periods`;

CREATE TABLE IF NOT EXISTS `schedule_plan_negotiated_periods` (
	`idmpk` VARCHAR(82) NOT NULL,
	`schedule_plan_id` VARCHAR(61) NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`start` DATETIME NOT NULL,
	`end` DATETIME NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_schedule_plan_id` (`schedule_plan_id`),
	CONSTRAINT `fk_chedule_plan_negotiated_periods_schedule_plan_id_schedule_pla`
		FOREIGN KEY (`schedule_plan_id`)
		REFERENCES `schedule_plans` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedule_plan_negotiated_periods_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedule_plan_negotiated_periods_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_user_profiles_shared`;

CREATE TABLE IF NOT EXISTS `schedule_user_profiles_shared` (
	`idmpk` VARCHAR(40) NOT NULL,
	`schedule_id` VARCHAR(61) NOT NULL,
	`user_profile_shared_id` VARCHAR(40) NOT NULL,
	`has_received_schedule` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`is_responding` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`is_full` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`label` VARCHAR(255) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_schedule_id` (`schedule_id`),
	CONSTRAINT `fk_schedule_user_profiles_shared_schedule_id_schedules`
		FOREIGN KEY (`schedule_id`)
		REFERENCES `schedules` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_user_profile_shared_id` (`user_profile_shared_id`),
	CONSTRAINT `fk_le_user_profiles_shared_user_profile_shared_id_user_profiles_`
		FOREIGN KEY (`user_profile_shared_id`)
		REFERENCES `user_profiles_shared` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedule_user_profiles_shared_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedule_user_profiles_shared_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schedule_requests`;

CREATE TABLE IF NOT EXISTS `schedule_requests` (
	`idmpk` VARCHAR(108) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`location_id` BIGINT(19) UNSIGNED NOT NULL,
	`organizer_hash` VARCHAR(32) NOT NULL,
	`hash` VARCHAR(32) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_schedule_requests_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_location_id` (`location_id`),
	CONSTRAINT `fk_schedule_requests_location_id_locations`
		FOREIGN KEY (`location_id`)
		REFERENCES `locations` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_schedule_requests_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_schedule_requests_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `calendars`;

CREATE TABLE IF NOT EXISTS `calendars` (
	`idmpk` VARCHAR(61) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`calendar_id` BIGINT(19) NOT NULL,
	`calendar_type_id` BIGINT(19) NOT NULL,
	`refreshed_at` DATETIME,
	`refreshing` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`color` VARCHAR(6) NOT NULL DEFAULT 'AC725E',
	`label` VARCHAR(255) NOT NULL,
	`position` BIGINT(19) DEFAULT 0,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_calendars_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_calendars_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_calendars_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `events`;

CREATE TABLE IF NOT EXISTS `events` (
	`idmpk` VARCHAR(82) NOT NULL,
	`calendar_id` VARCHAR(61) NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`start` DATETIME NOT NULL,
	`end` DATETIME NOT NULL,
	`length` BIGINT(19) NOT NULL,
	`title` VARCHAR(255),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_calendar_id` (`calendar_id`),
	CONSTRAINT `fk_events_calendar_id_calendars`
		FOREIGN KEY (`calendar_id`)
		REFERENCES `calendars` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_events_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_events_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `calendar_icalendars`;

CREATE TABLE IF NOT EXISTS `calendar_icalendars` (
	`idmpk` VARCHAR(40) NOT NULL,
	`user_id` BIGINT(19) UNSIGNED NOT NULL,
	`id` BIGINT(19) NOT NULL,
	`calendar_id` VARCHAR(61) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`created_by` BIGINT(19) UNSIGNED,
	`updated_by` BIGINT(19) UNSIGNED,
	`url` VARCHAR(1000) NOT NULL,
	PRIMARY KEY (`idmpk`),
	INDEX `idx_user_id` (`user_id`),
	CONSTRAINT `fk_calendar_icalendars_user_id_users`
		FOREIGN KEY (`user_id`)
		REFERENCES `users` (`id`)
		ON DELETE CASCADE,
	INDEX `idx_calendar_id` (`calendar_id`),
	CONSTRAINT `fk_calendar_icalendars_calendar_id_calendars`
		FOREIGN KEY (`calendar_id`)
		REFERENCES `calendars` (`idmpk`)
		ON DELETE CASCADE,
	INDEX `idx_created_by` (`created_by`),
	CONSTRAINT `fk_calendar_icalendars_created_by_users`
		FOREIGN KEY (`created_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL,
	INDEX `idx_updated_by` (`updated_by`),
	CONSTRAINT `fk_calendar_icalendars_updated_by_users`
		FOREIGN KEY (`updated_by`)
		REFERENCES `users` (`id`)
		ON DELETE SET NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=1;
COMMIT;