<?php

return array(

	'themes' => array(
		'cval-light' => array(
			'label' => 'Cval light',
			'css' => '/cval/media/jquery/css/ui-themes/cval-light/jquery-ui-1.8.16.custom.css',
		),
		'ui-lightness' => array(
			'label' => 'UI lightness',
			'css' => '/cval/media/jquery/css/ui-themes/ui-lightness/jquery-ui-1.8.14.custom.css',
		),
		'smoothness' => array(
			'label' => 'Smoothness',
			'css' => '/cval/media/jquery/css/ui-themes/smoothness/jquery-ui-1.8.14.custom.css',
		),
		'blitzer' => array(
			'label' => 'Blitzer',
			'css' => '/cval/media/jquery/css/ui-themes/blitzer/jquery-ui-1.8.14.custom.css',
		),
		'cupertino' => array(
			'label' => 'Cupertino',
			'css' => '/cval/media/jquery/css/ui-themes/cupertino/jquery-ui-1.8.16.custom.css',
		),
	),
	
	'i18n' => array(
		// JS strings from i18n
		'js' => array(
			'onload' => array(
				'Logout anyway',
				'Cancel',
				'Yes',
				'No'
			)
		)
	)

);