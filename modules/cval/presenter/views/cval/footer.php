<ul id="Cval-footer-nav">
	<li title="<?php echo Cval::i18n('Release').': '.Cval::instance()->config('release') ?>"><?php echo Cval::i18n('Cval') ?> v<?php echo Cval::instance()->config('version') ?></li>
  <li><?php echo Cval::i18n('Copyright') ?> © 2012 Zdennal</li>
  <li><a href="http://www.cvut.cz/" target="_blank"><?php echo Cval::i18n('CTU') ?></a></li>
</ul>	