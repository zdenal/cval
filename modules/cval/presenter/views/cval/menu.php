<ul id="Cval-main-nav">
	<?php if (Cval_Registration::is_enabled()): ?>
	<li class="Cval-main-nav-id-signup<?php echo ($active_menu == 'signup') ? ' Cval-main-nav-active' : '' ?>"><a href="<?php echo Route::url_protocol('signup') ?>"><?php echo Cval::i18n('Sign up') ?></a></li>
	<?php endif; ?>
	<li class="Cval-main-nav-id-login<?php echo ($active_menu == 'login') ? ' Cval-main-nav-active' : '' ?>"><a href="<?php echo Route::url_protocol('login') ?>"><?php echo Cval::i18n('Log in') ?></a></li>
</ul>