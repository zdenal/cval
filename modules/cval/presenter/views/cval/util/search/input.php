<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget Cval-form-search')) ?>
<div class="field" style="position: relative;">
	<?php echo Form::input('search', NULL, array(
		'style' => 'padding-left: 1.7em'
	)) ?>
	<div class="ui-icon ui-icon-search" style="position: absolute; top: .5em; left: .4em;"></div>
</div>
<?php echo Form::close(); ?>