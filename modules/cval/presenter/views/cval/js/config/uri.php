
(function(Cval, $) {

	// Route config
	Cval.config.route = {
		regexKey : new RegExp('<?php echo str_replace('++', '+', Route::REGEX_KEY) // Ommit Possessive Quantifier from Regex, JS not supports it ?>')
	};

	// Cval url
	Cval.config.url = '<?php echo URL::base(TRUE, TRUE) ?>';

	// Cval routes
	Cval.config.routes = <?php echo Cval_JS_Route::json() ?>;
	
})(Cval, jQuery)