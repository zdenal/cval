<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo I18n::lang() ?>" lang="<?php echo I18n::lang() ?>">
<head>
	<title><?php echo __('Cval Exception') ?></title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<style type="text/css">
	#cval_error { background: #ddd; font-size: 1em; font-family:sans-serif; text-align: left; color: #111; }
	#cval_error h1,
	#cval_error h2 { margin: 0; padding: 1em; font-size: 1em; font-weight: normal; background: #911; color: #fff; }
	#cval_error h1 a,
	#cval_error h2 a { color: #fff; }
	#cval_error h2 { background: #222; }
	#cval_error h3 { margin: 0; padding: 0.4em 0 0; font-size: 1em; font-weight: normal; }
	#cval_error div.content { padding: 1em 1em 1em; overflow: hidden; }
	</style>
</head>
<body>
	<div id="cval_error">
		<h1><?php echo __('Cval Exception') ?></h1>
		<div class="content">
			<?php echo html::chars($message) ?>
		</div>
		<h2><a href="<?php echo URL::base(TRUE, TRUE) ?>"><?php echo __('Return to Homepage') ?></a></h2>
	</div>
</body>
</html>
