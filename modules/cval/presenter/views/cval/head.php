<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title><?php echo Cval::instance()->location()->name() ?><?php echo ( ! empty($page_title) ? (' - '.Cval::i18n($page_title)) : '') ?></title>
	<?php echo View::factory('cval/css')->include_view($thiss) ?>
	<?php echo View::factory('cval/js')->include_view($thiss) ?>
</head>

