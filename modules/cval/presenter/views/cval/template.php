<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<?php echo $head; ?>
	<body>
		<div id="Cval-wrapper">
			<div id="Cval-header">
				<div id="Cval-header-holder">
					<?php echo $submenu; ?>
					<?php echo $logo; ?>
					<?php echo $menu; ?>
					<div class="ui-helper-clearfix"></div>
				</div>
			</div>
			<div id="Cval-main">
				<div id="Cval-main-holder">
					<div id="Cval-content">
						<?php echo $content; ?>
						<div class="clear"></div>
					</div>
					<div id="Cval-footer">
						<?php echo $footer; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="Cval-message-wrapper" />
	</body>
</html>