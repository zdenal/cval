<div class="container_16">
	
	<div class="grid_16 Cval-page-label Cval-helper-margin-bottom">
		<?php echo Cval::i18n($page_title) ?>
	</div>
	
	<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_10 ui-widget Cval-password-reset-form')) ?>

		<?php echo Form::hidden('csrf', $csrf) ?>
	
		<div class="field-label grid_8">
			<?php echo UTF8::ucfirst(Cval::i18n($user->meta()->fields('email')->label)) ?>
			<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
		</div>
		<div class="clear"></div>
		<div class="field grid_8">
			<?php echo $user->input('email') ?>
		</div>
		<div class="clear"></div>
		
		<div class="grid_16 button-line">
			<input type="submit" value="<?php echo Cval::i18n('Send a new password') ?>" class="Cval-button Cval-button-live"/>
		</div>
		<div class="clear"></div>

	<?php echo Form::close(); ?>
		
	<div class="clear"></div>
</div>