<div class="container_16">
	
	<div class="grid_16 Cval-page-label">
		<?php echo Cval::instance()->location()->name() ?>
	</div>
		
	<div class="grid_16 Cval-helper-margin-top">
		<?php echo Cval::i18n('Automatic meeting scheduling controlled by user preferences.') ?>
	</div>	
		
	<div class="clear"></div>
</div>


