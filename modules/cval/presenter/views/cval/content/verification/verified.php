<div class="container_16">
	
	<div class="grid_16 Cval-page-label">
		<?php echo Cval::i18n($page_title) ?>
	</div>
		
	<div class="grid_16">
		<p>
			<?php echo Cval::i18n('Your email address was verified.') ?>
		</p>
		<p>
			<?php if (Cval::logged_in()): ?>
			<?php echo Cval::i18n('Continue to :dashboard', array(
				':dashboard' => HTML::anchor(Route::url_protocol('home'), Cval::i18n('Cval'))
			)) ?>.
			<?php else: ?>
			<?php echo Cval::i18n('Now you can :login', array(
				':login' => HTML::anchor(Route::url_protocol('login'), Cval::i18n('log in'))
			)) ?>.
			<?php endif; ?>
		</p>
	</div>	
		
	<div class="clear"></div>
</div>