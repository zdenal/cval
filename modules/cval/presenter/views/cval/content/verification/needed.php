<div class="container_16">
	
	<div class="grid_16 Cval-page-label">
		<?php echo Cval::i18n($page_title) ?>
	</div>
		
	<div class="grid_16">
		<p>
			<?php echo Cval::i18n('Please verify your email address.') ?>
			<?php echo Cval::i18n('Click on link in registration email we sended to :email.', array(
				':email' => $user->email
			)) ?>
		</p>
	</div>
	
	<div class="grid_16 Cval-registration-resend-area">
		<div class="label-before-send">
			<?php echo Cval::i18n('If you don\'t have the email we can :resend it to you.', array(
				':resend' => HTML::anchor(Request::instance()->uri, Cval::i18n('resend'), array(
					'class' => 'Cval-registration-resend-trigger'
				))
			)) ?>
		</div>
		<div class="label-after-send ui-helper-hidden">
			<?php echo Cval::i18n('Registration email was resent to :email', array(
				':email' => $user->email
			)) ?>.
		</div>
		
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live ui-widget Cval-registration-resend-form')) ?>
		
			<?php echo Form::hidden('csrf', $csrf) ?>

		<?php echo Form::close(); ?>
	</div>
		
	<div class="clear"></div>
</div>