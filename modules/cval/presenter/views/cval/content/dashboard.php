<div class="container_16">
		
	<div class="grid_8">
		<span class="Cval-h3"><?php echo Cval::i18n('Newsfeed') ?></span>
		<span><?php echo HTML::image(Route::url_protocol('cval/media', array('file' => 'image/rss.png'))) ?></span>
	</div>	
	
	<div class="grid_8 Cval-dashboard-section-schedules">
		<div>
			<a href="<?php echo $inbound_group->url() ?>" class="Cval-link-page Cval-dashboard-schedules-goto-list">
			<span class="Cval-h3 Cval-helper-float-left"><?php echo Cval::i18n('Inbound') ?></span>
			<span class="ui-icon ui-icon-clock Cval-helper-float-left"></span>
			</a>
			<div class="clear"></div>
			<div>
				<?php echo $inbound_schedules_cnt > 0 ? $inbound_schedules_cnt : 'None' ?>
			</div>
		</div>
		
		<div class="Cval-helper-margin-top">
			<a href="<?php echo $outbound_group->url() ?>" class="Cval-link-page Cval-dashboard-schedules-goto-list">
			<span class="Cval-h3 Cval-helper-float-left"><?php echo Cval::i18n('Outbound') ?></span>
			<span class="ui-icon ui-icon-clock Cval-helper-float-left"></span>
			</a>
			<div class="clear"></div>
			<div>
				<?php echo $outbound_schedules_cnt > 0 ? $outbound_schedules_cnt : 'None' ?>
			</div>
		</div>
	</div>
		
	<div class="clear"></div>
</div>