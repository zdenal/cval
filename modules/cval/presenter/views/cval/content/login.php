<div class="container_16">
	
	<div class="grid_16 Cval-page-label Cval-helper-margin-bottom">
		<?php echo Cval::i18n($page_title) ?>
	</div>
	
	<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_10 ui-widget Cval-login-form')) ?>
	
		<?php echo Form::hidden('csrf', $csrf) ?>
	
		<div class="field-label grid_8">
			<?php echo UTF8::ucfirst(Cval::i18n($user->meta()->fields('email')->label)) ?>
			<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
		</div>
		<div class="clear"></div>
		<div class="field grid_8">
			<?php echo $user->input('email') ?>
		</div>
		<div class="clear"></div>

		<div class="field-label grid_8">
			<?php echo UTF8::ucfirst(Cval::i18n($user->meta()->fields('password')->label)) ?>
			<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
		</div>
		<div class="clear"></div>
		<div class="field grid_8">
			<?php echo $user->input('password') ?>
		</div>
		<div class="clear"></div>
		
		<div class="grid_16 button-line">
			<input type="submit" value="<?php echo Cval::i18n('Log in') ?>" class="Cval-button Cval-button-live"/>
		</div>
		<div class="clear"></div>

	<?php echo Form::close(); ?>
		
	<div class="grid_6">
		<ul class="Cval-big-list-vertical">
			<li>Did you lose your password? <a class="Cval-size-11" href="<?php echo Route::url_protocol('reset_password') ?>"><?php echo Cval::i18n('Reset it!') ?></a></li>
			<?php if (Cval_Registration::is_enabled()): ?>
			<li>Don't have an account yet? <a class="Cval-size-11" href="<?php echo Route::url_protocol('signup') ?>"><?php echo Cval::i18n('Sign up now!') ?></a></li>
			<?php endif; ?>
		</ul>
	</div>	
		
	<div class="clear"></div>
</div>