<div class="container_16">
	
	<div class="grid_16 Cval-page-label">
		<?php echo Cval::i18n($page_title) ?>
	</div>
	
	<div class="grid_16 Cval-helper-margin-top">
		<?php echo Cval::i18n('Sorry, but registration is disabled.') ?>
	</div>	
		
	<div class="clear"></div>
</div>