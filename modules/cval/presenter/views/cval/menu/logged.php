<ul id="Cval-main-nav">
	<li class="Cval-main-nav-dropdown Cval-main-nav-id-account<?php echo ($active_menu == 'account') ? ' Cval-main-nav-active' : '' ?>">
		<a href="#" class="Cval-user-label"><?php echo Cval::instance()->user()->name() ?></a>
		<div class="ui-icon ui-icon-triangle-1-s"></div>
		<ul class="Cval-main-nav-submenu right-align ui-corner-bottom">
			<li>
				<ul>
					<li><a href="#" class="Cval-logout-trigger"><?php echo Cval::i18n('Log out') ?></a></li>
				</ul>
			</li>
		</ul>
	</li>
</ul>