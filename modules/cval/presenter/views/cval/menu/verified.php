<ul id="Cval-main-nav">
	<!--<li class="Cval-main-nav-id-dashboard<?php echo ($active_menu == 'dashboard') ? ' Cval-main-nav-active' : '' ?>"><a href="<?php echo Route::url_protocol('dashboard') ?>" class="Cval-link-page"><?php echo Cval::i18n('Dashboard') ?></a></li>-->
	<li class="Cval-main-nav-id-schedules<?php echo ($active_menu == 'schedules') ? ' Cval-main-nav-active' : '' ?>">
		<a href="<?php echo Route::url_protocol('schedules_main') ?>" class="Cval-link-page">
			<?php echo Cval::i18n('Schedules') ?>
			<span class="Cval-unread-schedules-cnt-box Cval-helper-bold"><?php echo $unread_schedules_cnt ? strtr('(:cnt)', array(':cnt'=>$unread_schedules_cnt)) : '' ?></span>
		</a>
	</li>
	<li class="Cval-main-nav-id-contacts<?php echo ($active_menu == 'contacts') ? ' Cval-main-nav-active' : '' ?>"><a href="<?php echo Route::url_protocol('contacts_main') ?>" class="Cval-link-page"><?php echo Cval::i18n('Contacts') ?></a></li>
	<li class="Cval-main-nav-id-calendars<?php echo ($active_menu == 'calendars') ? ' Cval-main-nav-active' : '' ?>"><a href="<?php echo Route::url_protocol('calendars_main') ?>" class="Cval-link-page"><?php echo Cval::i18n('Calendars') ?></a></li>
	<li class="Cval-main-nav-dropdown Cval-main-nav-id-account<?php echo ($active_menu == 'account') ? ' Cval-main-nav-active' : '' ?>">
		<a href="<?php echo Route::url_protocol('settings_main')?>" class="Cval-user-label Cval-link-page"><?php echo Cval::instance()->user()->name() ?></a>
		<div class="ui-icon ui-icon-triangle-1-s"></div>
		<ul class="Cval-main-nav-submenu right-align ui-corner-bottom">
			<li>
				<ul>
					<li><a href="<?php echo Route::url_protocol('settings_main')?>" class="Cval-link-page"><?php echo Cval::i18n('Settings') ?></a></li>
					<?php if (A2::instance()->a1->is_admin()): ?>
					<li><a href="<?php echo Route::url_protocol('admin_main')?>" class="Cval-link-page"><?php echo Cval::i18n('Administration') ?></a></li>
					<?php endif; ?>
					<li><a href="#" class="Cval-logout-trigger"><?php echo Cval::i18n('Log out') ?></a></li>
				</ul>
			</li>
		</ul>
	</li>
</ul>