<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('home', '')
        ->defaults(array(
            'controller' => 'home',
        ));

Route::set('signup', 'signup')
        ->defaults(array(
            'controller' => 'signup',
        ));

Route::set('login', 'login')
        ->defaults(array(
            'controller' => 'login',
        ));

Route::set('reset_password', 'reset-password')
        ->defaults(array(
            'controller' => 'password',
			'action' => 'reset'
        ));

Route::set('verification', 'account-verification')
        ->defaults(array(
            'controller' => 'verification',
			'action' => 'needed'
        ));

Route::set('verified', 'account-verified/<csrf>')
        ->defaults(array(
            'controller' => 'verification',
			'action' => 'verified'
        ));

Route::set('home_dash', '')
        ->defaults(array(
            'controller' => 'home',
        ))->config_set('route_dash', TRUE);

Route::set('dashboard', 'dashboard')
        ->defaults(array(
            'controller' => 'dashboard',
        ))->config_set('route_dash', 'page');

