/*
 * JUP - plugin for file uploads with JSON support
 *
 * @name: JUP 
 * @author: Khaled Jouda
 * @version 1.0.1
 * @date 04.08.2009
 * @category jQuery plugin 
 * @Copyright (c) 2009 Khaled Jouda (doroubna.com)
 *
 */
(function($){
	function JUPST(form, settings) {
		var defaults = {
			url : null, /** if not set, form 'action' will be used  */
			onComplete:null, /** optional function to call after uploading is completed*/
			json:true /** wether to expect JSON response from upload script or not*/
		};

		var result = null;

		var settings = $.extend({}, defaults, settings || {});

		form = $(form);

		if( settings.url != null ){
			form.attr('action', settings.url);
		}

		form.attr("enctype", "multipart/form-data");

		/** Generate the iframe to use*/
		var iframeId = "JUPiFrame" + '_' + form.attr('id');
		var iFrame = null;
		if ($("#"+iframeId).length){
			iFrame = $("#"+iframeId);
			iFrame.contents().find("body").html('');
		} else {
			iFrame = $('<iframe id="' + iframeId + '" name="' + iframeId + '" src="javascript:;" style="display:none" />').appendTo(document.body);
		}

		form.attr("target", iframeId);

		iFrame.load(function(){
			/** form submission is complete,, */

			form.find("input[type=submit]").attr("disabled", false);

			var response = settings.json ? iFrame.contents().find("body").text() : iFrame.contents().find("body").html();

			if( $.isFunction(settings.onComplete) ){
				if(settings.json){
					try{
						response = $.parseJSON(response);
					}catch(e){
						response = {
							status : false,
							error : 'Not specified error'
						};
					}
				}

				settings.onComplete(response, form.attr('id'));

				// Clear iframe content, do not remove otherwise loading do not stop showing
				iFrame.contents().find("body").html('');
			}
		});
	};
	
	$.fn.jupST = function(settings){
		return this.each(function(){
			var jupST = JUPST($(this), settings);
		});
	};
})(jQuery);

