/**
 * Uses classic jQuery.serializeArray method, but returns it as JSON object.
 */
(function( $ ){
	$.fn.serializeJSON=function() {
		var json = {};
		$.map($(this).serializeArray(), function(n, i){
			var name = n['name'],
				value = n['value'];
			
			// If we have multiple items with same name in form create an
			// array of them
			if (json[name] !== undefined) {
				if (typeof json[name] == 'string') {
					json[name] = [json[name]];
				}
				json[name].push(value);
			} else {
				json[name] = value;
			}
		});
		return json;
	};
})( jQuery );