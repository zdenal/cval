/*
 * Custom cursor plugin.
 * Not completed
 *
 * @category	jQuery plugin 
 * @author		zdennal
 *
 */
(function($){
	$.fn.customcursor = function(imgUrl){
		var remove = imgUrl === false;
		
		return this.each(function(){
			var el = $(this);
			var cursor = $('<img style="position:absolute;display:none;cursor:none;" class="customcursor" src="'+imgUrl+'" />');
			$('body').append(cursor);
			
			el.data('customcursor')
			el.css('cursor','none');
			$(el).hover(function() {
				$('#mycursor').show();
			},function() {
				$('#mycursor').hide();		
			});
			$(el).mousemove(function(e){
				$('#mycursor').css('left', e.clientX - 1).css('top', e.clientY + 1);
			});
		});
	};
})(jQuery);

