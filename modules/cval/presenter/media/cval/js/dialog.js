(function(Cval, $) {

	Cval.dialog = {
		control : {}, // storage for dialog controls
		config : {
			controlClassPrefix : 'Cval-dialog-control-'
		},
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
				'css/dialog.css'
			];
			
			Cval.resource.get(resources, function(){
				thiss.loadControl('def', callback);
			});
		},
		loadControl : function(type, callback) {
			var thiss = this;
			// load default control if type is 
			if (type === null)
				Cval.core.exception('dialog control type must be set');

			// If we have array of resources
			if (Object.prototype.toString.apply(type) === '[object Array]') {
				var i;
				// We have only one item in array
				if (type.length == 1) {
					// So create a single resource from it
					for (i in type) {
						type = type[i];
						break;
					}
				} else {
					for (i in type) {
						// Get first resource
						var t = type[i];
						// Remove this element from array
						type.splice(i, 1);
						// Load single resource and construct callback
						// to call other resources
						Cval.async('dialog.control.'+t, function(){
							thiss.loadControl(type, callback);
						});
						break;
					}
					return;
				}
			}
			Cval.async('dialog.control.'+type, function(){
				Cval.core.callback(callback);
			});
		},
		/**
		 * Get element for specific type of page.
		 */
		getElement : function(type) {
			if (typeof type == 'object') {
				return type;
			}
			return $('.'+this.config.controlClassPrefix+type);
		},
		getControl : function(type) {
			var el = this.getElement(type);
			if (el.length < 1)
				return null;
			
			return Cval.dialog.control.def.data(el).control;
		},
		/**
		 * Allows to call control.
		 * You must supply function as second parametr. This function is called
		 * in dialog control scope and first parameter is dialog element.
		 */
		callControl : function(type, callback) {
			var thiss = this;
			
			var el = this.getElement(type);
			if (el.length < 1)
				Cval.core.exception("Cval.dialog.callControl received not existed element");
			
			callback.call(thiss.getControl(el), el);
		}
	}

})(Cval, jQuery)