(function(Cval, $) {

	Cval.registration_resend = {
		init : function() {
			$('.Cval-registration-resend-form').submit(function(){
				var form = $(this);
				
				if (form.hasClass('Cval-form-submitted'))
					return true;
				
				var data = form.serializeJSON();
				
				Cval.ajax.callModal({
					route : 'auth',
					routeParams : {
						action : 'resend_registration_email'
					},
					data : data
				}, function(json) {
					// We need to redirect
					if (json.redirect !== undefined)
					{
						form
							.attr('action', json.redirect)
							.addClass('Cval-form-submitted')
							.submit();
						
						return;
					}

					// Have message, so display it
					if (json.message !== undefined)
					{
						Cval.message.fromOptions(json.message);
					}
					
					// Display succes message
					form.parent().find('.label-before-send').fadeOut('fast', function(){
						form.parent().find('.label-after-send').fadeIn('slow')
					});
				})
				
				return false;
			})
			
			$('.Cval-registration-resend-trigger').click(function(){
				$('.Cval-registration-resend-form').submit();
				return false;
			});
		}
	}

	$(document).ready(function(){
		Cval.registration_resend.init();
	})

})(Cval, jQuery)