(function(Cval, $) {

	Cval.password_reset = {
		init : function() {
			$('.Cval-password-reset-form').submit(function(){
				var form = $(this);
				
				if (form.hasClass('Cval-form-submitted'))
					return true;
				
				var data = form.serializeJSON();
				
				Cval.ajax.callModal({
					route : 'auth',
					routeParams : {
						action : 'password_reset'
					},
					data : data
				}, function(json) {
					// We need to redirect
					if (json.redirect !== undefined)
					{
						form
							.attr('action', json.redirect)
							.addClass('Cval-form-submitted')
							.submit();
						
						return;
					}

					// Have message, so display it
					if (json.message !== undefined)
					{
						Cval.message.fromOptions(json.message);
					}
					
					// Update HTML if passed
					if (json.html !== undefined)
					{
						var html = $(json.html).hide();
						
						form.after(html);
						form.fadeOut('fast', function(){
							html.fadeIn('slow');
						});
					}
				})
				
				return false;
			})
		}
	}

	$(document).ready(function(){
		Cval.password_reset.init();
	})

})(Cval, jQuery)