
(function(Cval, $) {
	
	/**
	 * Returns url to Cval with appended uri.
	 */
	Cval.url = function (uri) {
		if ( ! uri )
			uri = '';

		return this.config.url + uri;
	}

	/**
	 * Returns url to Cval defined in passed route.
	 * If route is not specified or not exists in config.routes Cval home page
	 * url is returned.
	 */
	Cval.route = function (name, params, append, appendAsQueryString)
	{
		if (typeof name == 'object')
		{
			return Cval.route(
				name.name,
				name.params,
				name.append,
				name.appendAsQueryString
			);
		}

		if ( ! name || ! this.config.routes[name])
			return this.url();

		// Get route object
		var route = this.config.routes[name];
		var url = null;

		if (typeof(route) == 'string')
		{
			// Only string is passed, do not check for params
			url = route;
		}
		else
		{
			// Otherwise use uri function to add needed parameters
			url = uri(route, params);
		}

		// Does not have a protocol
		if (url.match(/^[-a-z0-9+.]+:\/\/.*/) === null)
		{
			// Append it
			url = this.url(url);
		}

		if (append !== undefined)
		{
			if (appendAsQueryString)
			{
				url += '?';
			}
			url += append;
		}

		// return url
		return url;

		function uri(route, params)
		{
			if (params === undefined)
			{
				// Use the default parameters
				params = $.extend({}, route.defaults);
			}
			else
			{
				// Add the default parameters
				var origParams = params;
				params = $.extend({}, route.defaults);

				for (param in origParams)
				{
					params[param] = origParams[param];
				}
			}

			// Start with the routed URI
			var uri = route.uri;

			if (uri.indexOf("<") === -1 && uri.indexOf("(") === -1)
			{
				// This is a static route, no need to replace anything
				return uri;
			}

			var match, search, replace, key, param, pos;

			while ((match = uri.match(/\([^()]+\)/)) !== null)
			{
				// Search for the matched value
				search = match[0];

				// Remove the parenthesis from the match as the replace
				replace = match[0].slice(1, -1);

				while((match = replace.match(Cval.config.route.regexKey)) !== null)
				{	
					key = match[0];
					param = match[1];

					if (params[param] !== undefined)
					{
						// Replace the key with the parameter value
						replace = str_replace(key, params[param], replace);
					}
					else
					{
						// This group has missing parameters
						replace = "";
						break;
					}
				}

				// Replace the group in the URI
				uri = str_replace(search, replace, uri);
			}

			while((match = uri.match(Cval.config.route.regexKey)) !== null)
			{
				key = match[0];
				param = match[1];

				if (params[param] === undefined)
				{
					// Ungrouped parameters are required
					throw ("Required route parameter not passed: "+param);
				}

				uri = str_replace(key, params[param], uri);
			}

			// Trim all extra slashes from the URI
			uri.replace(/\/+$/,"").replace(/\/\/+/, "/");

			return uri;
		}

		function str_replace (search, replace, subject, count) {
			// Replaces all occurrences of search in haystack with replace
			//
			// version: 1103.1210
			// discuss at: http://phpjs.org/functions/str_replace
			// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			// +   improved by: Gabriel Paderni
			// +   improved by: Philip Peterson
			// +   improved by: Simon Willison (http://simonwillison.net)
			// +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
			// +   bugfixed by: Anton Ongson
			// +      input by: Onno Marsman
			// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			// +    tweaked by: Onno Marsman
			// +      input by: Brett Zamir (http://brett-zamir.me)
			// +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			// +   input by: Oleg Eremeev
			// +   improved by: Brett Zamir (http://brett-zamir.me)
			// +   bugfixed by: Oleg Eremeev
			// %          note 1: The count parameter must be passed as a string in order
			// %          note 1:  to find a global variable in which the result will be given
			// *     example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
			// *     returns 1: 'Kevin.van.Zonneveld'
			// *     example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
			// *     returns 2: 'hemmo, mars'
			var i = 0,
				j = 0,
				temp = '',
				repl = '',
				sl = 0,
				fl = 0,
				f = [].concat(search),
				r = [].concat(replace),
				s = subject,
				ra = r instanceof Array,
				sa = s instanceof Array;
			s = [].concat(s);
			if (count) {
				this.window[count] = 0;
			}

			for (i = 0, sl = s.length; i < sl; i++) {
				if (s[i] === '') {
					continue;
				}
				for (j = 0, fl = f.length; j < fl; j++) {
					temp = s[i] + '';
					repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
					s[i] = (temp).split(f[j]).join(repl);
					if (count && s[i] !== temp) {
						this.window[count] += (temp.length - s[i].length) / f[j].length;
					}
				}
			}
			return sa ? s : s[0];
		}

	}

})(Cval, jQuery)