
(function(Cval, $) {

$.fn.Cval_data = function(key, value) {
	var data = $(this).data('Cval');

	if (data === undefined) {
		data = {};
		$(this).data('Cval', data);
	}

	if (key === undefined) {
		return data;
	}

	if (value !== undefined) {
		data[key] = value;
		
		$(this).data('Cval', data);
	}

	return data[key];
}

})(Cval, jQuery);