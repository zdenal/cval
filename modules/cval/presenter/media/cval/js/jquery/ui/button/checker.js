
(function(Cval, $) {

// -------------- Cval_button ---------------//
// Enable subclassing on button
$.ui.Cval_button.subclass = $.ui.widget.subclass;
// Inherit Cval_button from button
$.ui.Cval_button.subclass('ui.Cval_button_checker',{
	options: {
		text : false,
		checkIcon : 'ui-icon-check',
		uncheckIcon : 'ui-icon-closethick',
		checked : false,
		clickCallback : null,
		useCustomClickEvent : false,
		checkedText : Cval.i18n.get('Deselect all'),
		uncheckedText : Cval.i18n.get('Select all')
	},
	_create: function() {
		var thiss = this;
		
		this._setOption('checked', this.options.checked);
		
		if ( ! this.options.useCustomClickEvent)
		{
			this.element.click(function(){
				thiss._propagate( ! thiss.isChecked());
				thiss._setOption('checked',  ! thiss.isChecked());
			})
		}
	},
	_setOption: function( key, value ) {
		switch (key) {
			case 'checked':
				this._setChecked(value);
				break;
		}
		this._super(key, value);
	},
	_setChecked: function(value) {
		this._setOption('icons', {
			primary : value ? this.options.uncheckIcon : this.options.checkIcon
		});
		this._setOption('label', value ? this.options.checkedText : this.options.uncheckedText);
	},
	check : function() {
		this._setOption('checked', true);
	},
	uncheck : function() {
		this._setOption('checked', false);
	},
	isChecked : function() {
		return this.options.checked;
	},
	checkAndPropagate : function() {
		this._propagate(true);
		this._setOption('checked', true);
	},
	uncheckAndPropagate : function() {
		this._propagate(false);
		this._setOption('checked', false);
	},
	_propagate : function(checked) {
		var clickCallback = this.options.clickCallback;
				
		if (typeof clickCallback == 'function') {
			clickCallback.call(null, checked, this.element);
		}
	}
});

})(Cval, jQuery);