
(function(Cval, $) {

// -------------- Cval_button ---------------//
// Enable subclassing on button
$.ui.button.subclass = $.ui.widget.subclass;
// Inherit Cval_button from button
$.ui.button.subclass('ui.Cval_button',{
	options: {
		text : Cval.config.icon_text
	}
});

})(Cval, jQuery);