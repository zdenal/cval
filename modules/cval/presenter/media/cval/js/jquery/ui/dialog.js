
(function(Cval, $) {

// -------------- Cval_dialog ---------------//
// Enable subclassing on dialog
$.ui.dialog.subclass = $.ui.widget.subclass;
// Inherit Cval_dialog from dialog
$.ui.dialog.subclass('ui.Cval_dialog',{
	options: {
		autoOpen: false,
		modal: true,
		width: 750,
		dialogClass: 'Cval dialog-drop-shadow',
		minHeight: 20,
		position: {
				at : 'center top',
				my : 'center top',
				collision : 'fit',
				offset : '0 100'
			},
		CvalHeight: true,
		destroyOnClose: false,
		create : function(event, ui) {
			$(this).dialog('widget').find(' > .ui-dialog-titlebar')
				.removeClass('ui-corner-all')
				.addClass('ui-corner-top');
				
			if ($(this).dialog('option', 'CvalHeight') && $(this).dialog('option', 'height') == 'auto') {
				$(this).dialog('option', 'height', $(window).height()-200);
			}
		},
		close : function(event, ui) {
			if ($(this).dialog('option', 'destroyOnClose')) {
				$(this).remove();
			}
		}
	}
});

})(Cval, jQuery);