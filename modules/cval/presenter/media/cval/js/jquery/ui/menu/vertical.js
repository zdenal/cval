
(function(Cval, $) {

$.ui.widget.subclass('ui.Cval_menu_vertical',{
	options: {
		selectedItem : null,
		itemSelector : ' > li' 
	},
	_init:function() {
		this.element.addClass('ui-widget');
		
		$.each(this.element.find(this.options.itemSelector), function(){
			$(this)
				.addClass('ui-widget-header')
				.addClass('ui-state-default')
				.hover(function(){
					if ( ! $(this).hasClass('ui-state-disabled'))
						$(this).addClass('ui-state-hover')
				}, function(){
					$(this).removeClass('ui-state-hover')
				});
		})
	},
	_setOption: function( key, value ) {
		if (key == 'activeItem') {
			if (value === null) {
				this.deselect();
			} else {
				this.select(value);
			}
		}
		
		this._super( "_setOption", key, value );
	},
	select : function(selector) {
		this.deselect();
		this.element.find(selector).addClass('ui-state-active');
		this.options.selectedItem = selector;
	},
	deselect : function(selector) {
		if (selector === undefined) {
			selector = this.element.find(this.options.itemSelector);
		} else {
			selector = this.element.find(selector).addClass('ui-state-active');
		}
		selector.removeClass('ui-state-active');
	},
	disableItem : function(selector) {
		this.element.find(selector).addClass('ui-state-disabled');
	},
	enableItem : function(selector) {
		this.element.find(selector).removeClass('ui-state-disabled');
	},
	items : function(selector) {
		return this.element.find(selector ? selector : this.options.itemSelector);
	}
});

})(Cval, jQuery);