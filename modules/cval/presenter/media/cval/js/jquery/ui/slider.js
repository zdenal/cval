
(function(Cval, $) {

// -------------- Cval_slider ---------------//
// Enable subclassing on slider
$.ui.slider.subclass = $.ui.widget.subclass;
// Inherit Cval_slider from slider
$.ui.slider.subclass('ui.Cval_slider',{
	options: {
		create : function(event, ui) {
			$(this).slider('widget').find('.ui-slider-handle:first')
				.css({
					height : '1.8em',
					top : '-0.9em'
				});
		}
	}
});

})(Cval, jQuery);