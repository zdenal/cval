
(function(Cval, $) {

// -------------- Cval_form ---------------//
$.ui.widget.subclass('ui.Cval_form',{
	options: {
		disabled: false
	},
	_init:function(){
		if (this.element.hasClass('Cval-form-disabled')) {
			//this.options.disabled = true;
		}
		
		var object = this;
		// Select all inputs but not in buttons set
		var inputs = object.findInputs().not(function(){
			return $(this).parent().hasClass('Cval-buttonset')
		});

		this.element.find('.Cval-buttonset').buttonset();

		this.element.find("legend").addClass("ui-widget-header ui-corner-all");

		$.each(inputs,function(){
			if($(this).is(":reset ,:submit"))
				object.buttons($(this));
			else if($(this).is(":checkbox"))
				object.checkboxes($(this));
			else if($(this).is("input[type='text']")||$(this).is("textarea")||$(this).is("input[type='password']"))
				object.textelements($(this));
			else if($(this).is(":radio"))
				object.radio($(this));
			else if($(this).is("select"))
				object.selector($(this));
			else if($(this).is("input[type='hidden']"))
				object.hidden($(this));
			
			object.fieldInit($(this));
		});

		$(".Cval-hover").hover(function(){
			$(this).addClass("ui-state-hover");
		},function(){
			$(this).removeClass("ui-state-hover");
		});
		
		this._setOption( "disabled", this.options.disabled );
		
		// Make form visible and set focus to first visible item
		this.element.css('visibility', 'visible');
		this.findInputs().not(':hidden').filter(':first').focus();
	},
	_setOption: function( key, value ) {
		this._super(key, value);
	},
	findInputs : function(){
		return this.element.find("input, select, textarea");
	},
	textelements:function(element){
		if ($(element).hasClass('Cval-field-notlive'))
			return;
		
		$(element).wrap($('<span/>').addClass('ui-button-text-only'))
			.addClass('ui-state-default ui-corner-all ui-button-text')
			.bind({
				focusin: function() {
					$(this).toggleClass('ui-state-focus');
				},
				focusout: function() {
					$(this).toggleClass('ui-state-focus');
				}
			});
	},
	buttons:function(element)
	{
		if ($(element).hasClass('Cval-field-notlive'))
			return;
		
		$(element).button();
	},

	checkboxes:function(element){
		if ($(element).hasClass('Cval-field-notlive'))
			return;
		
		$(element).Cval_button({
			icons: {primary: "ui-icon-check"},
			text: false
		});
	},
	radio:function(element){
		
	},
	selector:function(element){
		
	},
	hidden:function(element){
		
	},
	fieldInit:function(element) {
		if ( ! $(element).hasClass('Cval-field-live'))
			return;
		
		Cval.field.add(element);
	},
	clear : function() {
		this.element.find(':input').each(function() {
			switch(this.type) {
				case 'password':
				case 'select-multiple':
				case 'select-one':
				case 'text':
				case 'textarea':
					$(this).val('');
					break;
				case 'checkbox':
				case 'radio':
					this.checked = false;
			}
		});
	}
});

})(Cval, jQuery);