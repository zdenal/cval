
(function($){

$.widget("ui.Cval_pagination", {
	
	// default options
	options: {
		// Current page number
		current_page: 1,
		// Total item count
		total_items: 0,
		// How many items to show per page
		items_per_page: 10,
		// Total page count
		total_pages: 0,
		// Item offset for the first item displayed on the current page
		current_first_item: 0,
		// Item offset for the last item displayed on the current page
		current_last_item: 0,
		// Previous page number; FALSE if the current page is the first one
		previous_page: false,
		// Next page number; FALSE if the current page is the last one
		next_page: false,
		// First page number; FALSE if the current page is the first one
		first_page: false,
		// Last page number; FALSE if the current page is the last one
		last_page: false,
		// Query offset
		offset: 0,
		
		clickCallback: null,
		boxClass: 'Cval-pagination',
		previousClass: 'Cval-pagination-previous',
		nextClass: 'Cval-pagination-next',
		summaryClass: 'Cval-pagination-summary'
	},

	_create: function(){
		var thiss = this,
			el = this.element,
			o = this.options;
		
		this.previousButton = $('<span />')
			.text(Cval.i18n.get("Previous"))
			.addClass(o.previousClass)
			.button({
				text: false,
				icons: {
					primary: "ui-icon-circle-triangle-w"
				}
			})
			.removeClass('ui-corner-all')
			.addClass('ui-corner-left')
			.click(function(){
				thiss.previous();
			});
			
		this.nextButton = $('<span />')
			.text(Cval.i18n.get("Next"))
			.addClass(o.nextClass)
			.button({
				text: false,
				icons: {
					primary: "ui-icon-circle-triangle-e"
				}
			})
			.removeClass('ui-corner-all')
			.addClass('ui-corner-right')
			.click(function(){
				thiss.next();
			});
			
		this.summaryText = $('<span />')
			.text('')
			.addClass(o.summaryClass);
		
		el
			.addClass(o.boxClass)
			.append(
				$('<span />')
					.append(this.previousButton)
					.append(this.nextButton)
			)
			.append(this.summaryText);
				
		this.update({});
	},
	
	update: function( options ){
		var thiss = this;
			
		$.each(options || {}, function(key, value){
			thiss._setOption(key, value);
		});
		
		this._recalculate();
	},
	_recalculate: function() {
		var el = this.element,
			o = this.options;
			
		// Update summary text
		this.summaryText.text(o.current_first_item+'-'+o.current_last_item+' of '+o.total_items);
		
		if (o.previous_page !== false)
			this.previousButton.button('enable');
		else
			this.previousButton.button('disable');
		
		if (o.next_page !== false)
			this.nextButton.button('enable');
		else
			this.nextButton.button('disable');
	},
	
	// react to option changes after initialization
	_setOption: function( key, value ){
		$.Widget.prototype._setOption.apply( this, arguments );
	},
	
	previous : function() {
		if ( ! this.options.previous_page || ! this.options.clickCallback)
			return;
		
		this.options.clickCallback.call(null, this.options.previous_page, this.element);
	},
	
	next : function() {
		if ( ! this.options.next_page || ! this.options.clickCallback)
			return;
		
		this.options.clickCallback.call(null, this.options.next_page, this.element);
	}
});

})(jQuery);
