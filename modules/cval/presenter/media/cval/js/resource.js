
(function(Cval, $) {

	Cval.resource = {
		config : {
			baseRoute : 'cval/media',
			loadedResources : {}
		},
		init : function() {
			// Set unique media urls
			Loader.unique = Cval.config.Loader.unique;
			if (Loader.unique)
				Loader.uniqueToken = Cval.config.Loader.uniqueToken;
		},
		get : function(resource, callback) {
			var thiss = this;

			// If we have array of resources
			if (Object.prototype.toString.apply(resource) === '[object Array]') {
				var i;
				// We have only one item in array
				if (resource.length == 1) {
					// So create a single resource from it
					for (i in resource) {
						resource = resource[i];
						break;
					}
				} else {
					for (i in resource) {
						// Get first resource
						var r = resource[i];
						// Remove this element from array
						resource.splice(i, 1);
						// Load single resource and construct callback
						// to call other resources
						Loader.once(thiss._url(r), function(){
							thiss.get(resource, callback);
						});
						break;
					}
					return;
				}
			}
			Cval.core.modal.show();
			Loader.once(thiss._url(resource), function(){
				Cval.core.modal.hide();
				thiss._callCallback(callback);
			});
		},
		_url : function(resource) {
			// Does have a protocol
			if (resource.match(/^[-a-z0-9+.]+:\/\/.*/) !== null)
			{
				// Return do not append to route
				return resource;
			}
			return Cval.route(this.config.baseRoute, {
				file : resource
			});
		},
		_callCallback : function(callback) {
			return Cval.core.callback(callback);
		}
	}

})(Cval, jQuery)