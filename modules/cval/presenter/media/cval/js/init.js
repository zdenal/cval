
(function(Cval, $) {

	// Add trim functionality to IE
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
		return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
		return this.replace(/\s+$/,"");
	}

	// Load Cval
	$(document).ready(function(){
		$.Console.Log('Cval init START');
		Cval.core.init();
		$.Console.Log('Cval init END');
	});

})(Cval, jQuery)