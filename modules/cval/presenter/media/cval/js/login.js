(function(Cval, $) {

	Cval.login = {
		init : function() {
			$('.Cval-login-form').submit(function(){
				var form = $(this);
				
				if (form.hasClass('Cval-form-submitted'))
					return true;
				
				var data = form.serializeJSON();
				
				Cval.ajax.callModal({
					route : 'auth',
					routeParams : {
						action : 'login'
					},
					data : data
				}, function(json) {
					// We need to redirect
					if (json.redirect !== undefined)
					{
						form
							.attr('action', json.redirect)
							.addClass('Cval-form-submitted')
							.submit();
						
						return;
					}

					// Have message, so display it
					if (json.message !== undefined)
					{
						Cval.message.fromOptions(json.message);
					}
				})
				
				return false;
			})
		}
	}

	$(document).ready(function(){
		Cval.login.init();
	})

})(Cval, jQuery)