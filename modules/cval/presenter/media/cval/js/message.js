(function(Cval, $) {

	Cval.message = {
		config : {
			wrapper : {
				el : '#Cval-message-wrapper',
				maxWidth : 600
			}
		},
		init : function () {
			var thiss = this;
			
			var width = null
			var	left = null;

			if ($("body").width() < this.config.wrapper.maxWidth) {
				width = $("body").width();
				left = 0;
			} else {
				width = this.config.wrapper.maxWidth;
				left = ($("body").width() - width) / 2;
			}

			var wrapper = $(this.config.wrapper.el);
			wrapper.width(width - 20);
			if (left) {
				wrapper.css('left', left);
			}

			// Set event on close icon
			$('.Cval-message-close').livequery('click', function(){
				thiss.destroy($(this).parent());
			});

			// Display onload messages
			if (Cval.config.onload_messages) {
				$.each(Cval.config.onload_messages, function(i, options){
					Cval.message.fromOptions(options);
				})
			}
		},
		error : function (message, title, options) {
			options = this.options(message, title, $.extend({
				destroyDelay : 10000
			}, options || {}));
			//options.icon = 'ui-icon-alert';
			options.addClass = 'ui-state-error';

			return this.show(options);
		},
		info : function (message, title, options) {
			options = this.options(message, title, $.extend({
				destroyDelay : 5000
			}, options || {}));
			//options.icon = 'ui-icon-info';
			options.addClass = 'ui-state-highlight';

			return this.show(options);
		},
		success : function (message, title, options) {
			options = this.options(message, title, $.extend({
				destroyDelay : 5000
			}, options || {}));
			//options.icon = 'ui-icon-info';
			options.addClass = 'ui-state-highlight';

			return this.show(options);
		},
		fromOptions : function(options) {
			options = options || {};

			switch (options.type) {
				case 'error':
					return this.error(options.message, options.title, options);
					break;
				case 'info':
					return this.info(options.message, options.title, options);
					break;
				case 'success':
					return this.success(options.message, options.title, options);
					break;
				default:
					$.Console.Log('Not supported message type', [options.type]);
			}
			
			return false;
		},
		options : function (message, title, options) {
			options = $.extend({}, {
				autoShow : true,
				autoDestroy : true,
				wrapper : this.config.wrapper.el,
				draggable : {
					cancel : "div"
				},
				closer : true,
				outerClass : null
			}, options || {});

			options.message = message;
			options.title = title;
			

			return options;
		},
		show : function (options) {
			var thiss = this,
				message = this.create.complete(options),
				wrapper = options.wrapper;
			
			this._appendToWrapper(wrapper, message);
			
			if (options.autoShow)
				message.fadeIn('slow');
			
			if (options.draggable)	
				message.draggable(options.draggable);
			
			if (options.autoDestroy && options.destroyDelay !== undefined) {
				// Remove message after specific time
				if (options.destroyDelay !== undefined) {
					message.oneTime(options.destroyDelay, function(){
						thiss.destroy($(this));
					});
				}
			}
			return message;
		},
		destroy : function (messageEl, speed) {
			var thiss = this;
			
			messageEl = $(messageEl);
			if ( ! messageEl.length)
				return;

			// Default hide speed is slow
			if (speed === undefined)
				speed = 'slow';
			
			var removeCallback = function(){
				$(this).remove();
				thiss._makeMessageFirst($(thiss.config.wrapper.el).find('.Cval-message:first-child'));
			}
			
			// If speed is true, it means that message is not faded out, but simple removed
			if (speed === true) {
				Cval.core.callbackApply(removeCallback, messageEl);
			} else {
				messageEl.fadeOut(speed, removeCallback);
			}
		},
		create : {
			complete : function(options) {
				options = options || {};

				var content = this.content();
				if (options.icon) {
					content.append(this.icon().addClass(options.icon));
				}
				if (options.title) {
					content.append(this.title(options.title));
				}
				content.append(this.message(options.message));

				var el = this.outerBox();
				
				if (options.outerClass)
					el.addClass(options.outerClass);
				
				if (options.closer)
					el.append(this.closer());
				
				el.append(this.innerBox()
						.addClass(options.addClass)
						.append(content)
					);
				return el;
			},
			outerBox : function() {
				return $('<div class="Cval Cval-message ui-widget ui-helper-hidden"/>');
			},
			closer : function() {
				return $('<span class="Cval-message-close ui-icon ui-icon-close"/>');
			},
			innerBox : function() {
				return $('<div class="Cval-message-inner-box"/>');
				//return $('<div class="Cval-message-inner-box ui-corner-all"/>');
			},
			content : function() {
				return $('<div class="Cval-message-content"/>');
			},
			icon : function() {
				return $('<span class="Cval-message-icon ui-icon"/>');
			},
			title : function(content) {
				return $('<strong/>').html(content).append(':&nbsp;');
			},
			message : function(content) {
				return content;
			}
		},
		_appendToWrapper : function(wrapper, message) {
			$(wrapper).append(message);
			
			if (typeof wrapper == 'string' 
					&& wrapper == this.config.wrapper.el) 
			{
				if (message.is(':first-child'))
					this._makeMessageFirst(message);
			}
			else
			{
				message.addClass('Cval-message-standalone');
			}
		},
		_makeMessageFirst : function(message) {
			var innerBox = $(message).find('.Cval-message-inner-box');
				
			innerBox
				//.removeClass('ui-corner-all')
				//.addClass('ui-corner-bottom')
				.addClass('first');
		}
	}

})(Cval, jQuery)