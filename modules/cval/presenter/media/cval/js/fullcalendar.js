
(function(Cval, $) {

	Cval.fullcalendar = {
		init : function(callback) {
			// Needed resources
			var resources = [
				Cval.route('cval/media', {
					file : 'jquery/fullcalendar/fullcalendar.css'
				}),
				Cval.route('cval/media', {
					file : 'css/fullcalendar.css'
				}),
				Cval.route('cval/media', {
					file : 'jquery/fullcalendar/fullcalendar.js'
				})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});	
		},
		defaults : {
			theme: true,
			//height: 332,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			loading: function(bool) {
				if (bool) Cval.core.modal.show();
				else Cval.core.modal.hide();
			},
			events : {
				url : Cval.route('calendars', {
					controller : 'timetable',
					action : 'events'
				}),
				type : 'POST',
				success: function(json) {
					return json.events;
				}
			},
			eventColor: '#4986E7',
			defaultView : 'agendaWeek',
			allDayDefault : false,
			ignoreTimezone : false,
			firstDay: Cval.core.config('first_day'),
			timeFormat: {
				agenda: Cval.core.config('time_24_hour') ? 'H(:mm){ - H(:mm)}' : 'h(:mm)t{ - h(:mm)t}',
				'' : Cval.core.config('time_24_hour') ? 'H(:mm)' : 'h(:mm)t'
			},
			axisFormat: Cval.core.config('time_24_hour') ? 'H:mm' : 'h:mm tt',
			allDaySlot: false,
			columnFormat: {
				month: 'ddd',
				week: Cval.core.config('date_month_second') ? 'ddd d/M' : 'ddd M/d',
				day: Cval.core.config('date_month_second') ? 'dddd d/M' : 'dddd M/d'
			},
			titleFormat: {
				month: 'MMMM yyyy',
				week: Cval.core.config('date_month_second') ? "d[ MMM][ yyyy]{ '&#8212;' d MMM yyyy}" : "[MMM ]d[ yyyy]{ '&#8212;' MMM d yyyy}",
				day: Cval.core.config('date_month_second') ? 'dddd, d MMM yyyy' : 'dddd, MMM d yyyy' 
			},
			eventAfterRender: function(event, element, view) {
				// Make sure minimum height of event is 18px
				element.css('height', Math.max(parseInt(element.css('height').replace('px', '')), 18)+'px');
			}
		},
		view : {}, // storage for view classes
		getViewControl : function(view) {
			return this.view[view.name];
		},
		getDay : function(view, date) {
			return this.getViewControl(view).getDay(view, date);
		},
		getDaysFromInterval : function(view, from, to) {
			var thiss = this,
				start = from.clone().clearTime(),
				end = to.clone().clearTime(),
				days = [];
			
			while (start <= end) {
				var day = thiss.getDay(view, start);
				if (day)
					days.push(day);
				start.addDays(1);
			}
		
			return days;
		},
		getDays : function(view, dates) {
			var thiss = this,
				days = [];
			
			$.each(dates, function(){
				var day = thiss.getDay(view, this);
				if (day)
					days.push(day);
			});
		
			return days;
		},
		getAllDays : function(view) {
			return this.getViewControl(view).getAllDays(view);
		},
		getDaysBeforeDate : function(view, date) {
			return this.getViewControl(view).getDaysBeforeDate(view, date);
		},
		getDaysAfterDate : function(view, date) {
			return this.getViewControl(view).getDaysAfterDate(view, date);
		},
		getTimeRanges : function(view, times) {
			var thiss = this,
				items = [];
			
			$.each(times, function(){
				var item = thiss.getTimeRange(view, this[0], this[1]);
				if (item)
					items.push(item);
			});
		
			return items;
		},
		getTimeRange : function(view, start, end) {
			return this.getViewControl(view).getTimeRange(view, start, end);
		},
		clearTimeRanges : function(view) {
			return this.getViewControl(view).clearTimeRanges(view);
		},
		data : function(el, data) {
			if (data !== undefined)
				$(el).data('Cval_fullcalendar', data);
			
			data = $(el).data('Cval_fullcalendar');
			if (data === undefined)
				$(el).data('Cval_fullcalendar', {});
			
			return $(el).data('Cval_fullcalendar');
		},
		getInitCurrentDateFromDate : function(date) {
			return {
				year: date.getFullYear(),
				month: date.getMonth(),
				date: date.getDate()
			};
		}
	}
	
	$.Class.extend('Cval.fullcalendar.view.def', {
		isDayVisible : function(view, date) {
			if (date < view.visStart || date >= view.visEnd)
				return false;
			return true;
		},
		isTimeVisible : function(view, start, end) {
			if (start >= view.visEnd || end <= view.visStart)
				return false;
			return true;
		},
		getDay : function(view, date) {
			if ( ! this.isDayVisible(view, date))
				return false;
			return this._getDay(view, date);
		},
		getAllDays : function(view) {
			return this._getAllDays(view);
		},
		_getDay : function(view, date) {
			Cval.core.exception('Method _getDay is not implemeted in :clazz', {
				clazz : this.fullName
			});
		},
		_getAllDays : function(view) {
			Cval.core.exception('Method _getAllDays is not implemeted in :clazz', {
				clazz : this.fullName
			});
		},
		getDaysBeforeDate : function(view, date) {
			var thiss = this,
				start = view.visStart.clone().clearTime(),
				end = date.clone().clearTime().addDays(-1),
				days = [];
			
			if (end < start)
				return days;
			
			if (end > view.visEnd)
				end = view.visEnd.clone().clearTime();
			
			while (start <= end) {
				var day = thiss.getDay(view, start);
				if (day)
					days.push(day);
				start.addDays(1);
			}
		
			return days;
		},
		getDaysAfterDate : function(view, date) {
			var thiss = this,
				start = date.clone().clearTime().addDays(1),
				end = view.visEnd.clone().clearTime(),
				days = [];

			if (start > end)
				return days;
			
			if (start < view.visStart)
				end = view.visStart.clone().clearTime();

			while (start <= end) {
				var day = thiss.getDay(view, start);
				if (day)
					days.push(day);
				start.addDays(1);
			}

			return days;
		},
		getTimeRange : function(view, start, end) {
			if ( ! this.isTimeVisible(view, start, end))
				return false;
			return this._getTimeRange(view, start.clone(), end.clone());
		},
		clearTimeRanges : function(view) {
			return this._clearTimeRanges(view);
		}
	}, {});
	
	Cval.fullcalendar.view.def.extend('Cval.fullcalendar.view.month', {
		_getDay : function(view, date) {
			return this._getDayForCell(view, view.dateCell(date)); 
		},
		_getAllDays : function(view) {
			var rowCnt = view.getRowCnt(),
				colCnt = view.getColCnt(),
				days = [];
				
			for (var row = 0; row < rowCnt; row++) {
				for (var col = 0; col < colCnt; col++) {
					days.push(this._getDayForCell(view, {
						row : row,
						col : col
					}));
				}
			}
			
			return days;
		},
		_getDayForCell : function(view, cell) {
			var cellClass = '.fc-week'+cell.row+' .fc-day'+(cell.row*7 + cell.col);
				
			return view.element.find(cellClass);
		}
	}, {});
	
	Cval.fullcalendar.view.def.extend('Cval.fullcalendar.view.agendaWeek', {
		_getDay : function(view, date) {
			return this._getDayForCell(view, view.dateCell(date)); 
		},
		_getAllDays : function(view) {
			var colCnt = view.getColCnt(),
				days = [];
				
			for (var col = 0; col < colCnt; col++) {
				days.push(this._getDayForCell(view, {
					row : 0,
					col : col
				}));
			}
			
			return days;
		},
		_getDayForCell : function(view, cell) {
			var cellClass = '.fc-col'+cell.col;
				
			return view.element.find(cellClass);
		},
		_getTimeRangeLayer : function(view) {
			var layer = view.element.find('.Cval-fullcalendar-timerange-wrapper');
			if ( ! layer.length) {
				layer = $('<div/>').css({
					position : 'absolute',
					'z-index' : 5,
					top : 0,
					left : 0
				}).addClass('Cval-fullcalendar-timerange-wrapper');
				view.element.find('> div > div > div').append(layer);
			}
				
			return layer;
		},
		_getTimeRange : function(view, start, end) {
			var elsSelectors = [];
			
			if (start < view.visStart)
				start = view.visStart;
			if (end > view.visEnd)
				end = view.visEnd;
			
			var dayEnd, tmpEnd, el;
			while (true) {
				dayEnd = start.clone().clearTime().addDays(1).addSeconds(-1);
				tmpEnd = dayEnd < end ? dayEnd : end;
				el = this._createOneDayTimeRangeEl(view, start, tmpEnd);
				elsSelectors.push('.Cval-fullcalendar-timerange-item-'+Cval.core.htmlClassValue(el, 'Cval-fullcalendar-timerange-item-'));
				if (dayEnd < end) {
					start = dayEnd.clone().addSeconds(1);
				} else {
					break;
				}
			}
			
			return this._getTimeRangeLayer(view).find(elsSelectors.join(', '));
		},
		_createOneDayTimeRangeEl : function(view, start, end) {
			var timeRangeSelector = 'Cval-fullcalendar-timerange-item-'+start.getTime()+'-'+end.getTime(),
				timeRangeEl = this._getTimeRangeLayer(view).find('.'+timeRangeSelector);
			
			if (timeRangeEl.length)
				return timeRangeEl;
			
			var minuteHeight = 1008 / 1440,
				startMinutes = start.getHours() * 60 + start.getMinutes(),
				endMinutes = end.getHours() * 60 + end.getMinutes();
			
			var cell = view.dateCell(start),
				firstCellEl = this._getDayForCell(view, {col : 0}),
				itemWidth = firstCellEl.width(), 
				col = cell.col,
				left = firstCellEl.prev().outerWidth() + col*itemWidth,
				top = startMinutes * minuteHeight,
				height = (endMinutes - startMinutes) * minuteHeight,
				width = col == 6 ? itemWidth+20 : itemWidth,
				el = $('<div/>');
			
			el.css({
				left : left,
				top : top,
				height : height,
				width : width,
				display : 'none'
			}).addClass('Cval-fullcalendar-timerange-item')
			.addClass(timeRangeSelector);
			
			// Add data to element
			Cval.fullcalendar.data(el).timerange_item = {
				start : start,
				end : end
			};
			el.bind('click.fullcalendar', function(e){
				var rangeEl = $(this),
					data = Cval.fullcalendar.data(rangeEl).timerange_item;
				
				var y = e.pageY - rangeEl.offset().top,
					deltaMinutes = Math.floor(y / minuteHeight),
					clickedDate = data.start.clone().addMinutes(deltaMinutes);
					
				if (clickedDate > data.end)
					clickedDate.setTime(data.end.getTime());
				
				rangeEl.trigger('fullcalendarClick', [data, clickedDate, e]);
			});
			
			this._getTimeRangeLayer(view).append(el);
			return el;
		},
		_clearTimeRanges : function(view) {
			this._getTimeRangeLayer(view).empty();
		}
	}, {});
	
	Cval.fullcalendar.view.def.extend('Cval.fullcalendar.view.agendaDay', {
		_getDay : function(view, date) {
			return this._getDayForCell(view, view.dateCell(date)); 
		},
		_getAllDays : function(view) {
			var days = [];
				
			days.push(this._getDayForCell(view, {
				row : 0,
				col : 0
			}));
			
			return days;
		},
		_getDayForCell : function(view, cell) {
			var cellClass = '.fc-col'+cell.col;
			return view.element.find(cellClass);
		},
		_getTimeRangeLayer : function(view) {
			var layer = view.element.find('.Cval-fullcalendar-timerange-wrapper');
			if ( ! layer.length) {
				layer = $('<div/>').css({
					position : 'absolute',
					'z-index' : 5,
					top : 0,
					left : 0
				}).addClass('Cval-fullcalendar-timerange-wrapper');
				view.element.find('> div > div > div').append(layer);
			}
				
			return layer;
		},
		_getTimeRange : function(view, start, end) {
			var elsSelectors = [];
			
			if (start < view.visStart)
				start = view.visStart;
			if (end > view.visEnd)
				end = view.visEnd;
			
			var dayEnd, tmpEnd, el;
			while (true) {
				dayEnd = start.clone().clearTime().addDays(1).addSeconds(-1);
				tmpEnd = dayEnd < end ? dayEnd : end;
				el = this._createOneDayTimeRangeEl(view, start, tmpEnd);
				elsSelectors.push('.Cval-fullcalendar-timerange-item-'+Cval.core.htmlClassValue(el, 'Cval-fullcalendar-timerange-item-'));
				if (dayEnd < end) {
					start = dayEnd.clone().addSeconds(1);
				} else {
					break;
				}
			}
			
			return this._getTimeRangeLayer(view).find(elsSelectors.join(', '));
		},
		_createOneDayTimeRangeEl : function(view, start, end) {
			var timeRangeSelector = 'Cval-fullcalendar-timerange-item-'+start.getTime()+'-'+end.getTime(),
				timeRangeEl = this._getTimeRangeLayer(view).find('.'+timeRangeSelector);
			
			if (timeRangeEl.length)
				return timeRangeEl;
			
			var minuteHeight = 1008 / 1440,
				startMinutes = start.getHours() * 60 + start.getMinutes(),
				endMinutes = end.getHours() * 60 + end.getMinutes();
			
			var cell = view.dateCell(start),
				firstCellEl = this._getDayForCell(view, {col : 0}),
				itemWidth = firstCellEl.width(), 
				col = cell.col,
				left = firstCellEl.prev().outerWidth() + col*itemWidth,
				top = startMinutes * minuteHeight,
				height = (endMinutes - startMinutes) * minuteHeight,
				width = itemWidth,
				el = $('<div/>');
			
			el.css({
				left : left,
				top : top,
				height : height,
				width : width,
				display : 'none'
			}).addClass('Cval-fullcalendar-timerange-item')
			.addClass(timeRangeSelector);
			
			// Add data to element
			Cval.fullcalendar.data(el).timerange_item = {
				start : start,
				end : end
			};
			el.bind('click.fullcalendar', function(e){
				var rangeEl = $(this),
					data = Cval.fullcalendar.data(rangeEl).timerange_item;
				
				var y = e.pageY - rangeEl.offset().top,
					deltaMinutes = Math.floor(y / minuteHeight),
					clickedDate = data.start.clone().addMinutes(deltaMinutes);
					
				if (clickedDate > data.end)
					clickedDate.setTime(data.end.getTime());
				
				rangeEl.trigger('fullcalendarClick', [data, clickedDate, e]);
			});
			
			this._getTimeRangeLayer(view).append(el);
			return el;
		},
		_clearTimeRanges : function(view) {
			this._getTimeRangeLayer(view).empty();
		}
	}, {});

})(Cval, jQuery)