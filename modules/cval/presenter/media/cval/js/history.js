(function(Cval, $) {

	Cval.history = {	
		state:{
			page : null
		},
		lastState : false,
		stateChangedFromUi : false,
		//indicates whether we are restoring now the state, so we don't want the events triggered to change hash
		restoring : false,
		init : function () {
			var thiss = this;
			//bind hashChange for different elements
			$(window).bind( 'hashchange', function(event) {
				// Get the hash (fragment) as a string, with any leading # removed. Note that
				// in jQuery 1.4, you should use e.fragment instead of $.param.fragment().
				var url = $.param.fragment();
				thiss.loadState();
				if ( ! thiss.stateChangedFromUi) {
					thiss.restore(url);
				}
				thiss.stateChangedFromUi = false;
			});
			// Since the event is only triggered when the hash changes, we need to trigger
			// the event now, to handle the hash the page may have loaded with.
			$(window).trigger( 'hashchange' );
		},
		loadState: function(){
			this.lastState = this.lastState === false ? null : $.extend({}, this.state);
			this.state = $.bbq.getState(); //triggers hashchange only if changed
		},
		/**
		 * type variable:
		 *    0: params in the params argument will override any params in the current state.
		 *	  1: any params in the current state will override params in the params argument.
		 *	  2: params argument will completely replace current state.
		 */
		saveState: function(type, state, fromUi) {
			this.stateChangedFromUi = fromUi || fromUi === undefined;
			if(state) {
				$.bbq.pushState(state,type);
			} else {
				$.bbq.pushState(this.state,type);
			}
		},
		// Called when bbq state changed
		restore : function(url) {
			var thiss = this;
			Cval.page.init(function(){
				// Show page every time, if not in state show dashboard
				Cval.page.show(thiss.state.page || 'schedules', $.extend({}, thiss.state));
			});
		},
		pageShow : function(pageId, params) {
			var newState = $.extend({}, {
				page : pageId
			}, params || {});
			
			if (Cval.helper.json.compare(this.state, newState)) {
				return;
			}
			
			this.saveState(2, newState);
		},
		back : function(emptyHistoryCallback) {
			if ( ! this.lastState) { // is null or false
				Cval.core.callback(emptyHistoryCallback);
				return false;
			}
			
			this.saveState(2, $.extend({}, this.lastState), false);
			return true;
		},
		getLastState : function() {
			return this.lastState;
		},
		restoreState : function(state, emptyStateCallback) {
			if ( ! state) { // is null or false
				Cval.core.callback(emptyStateCallback);
				return false;
			}
			
			this.saveState(2, $.extend({}, state), false);
			return true;
		}
	}   
	
})(Cval, jQuery)