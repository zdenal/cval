(function(Cval, $) {

	Cval.menu = {
		config : {
			// Callbacks to run at menu init
			initEvents : {},
			mainNavSelector : '#Cval-main-nav',
			mainNavItemSelector : ' > li',
			mainNavItemDropdownSelector : ' > li.Cval-main-nav-dropdown',
			mainNavItemIdSelectorPrefix : '.Cval-main-nav-id-',
			mainNavItemActiveClass : 'Cval-main-nav-active'
		},
		init : function() {
			this.initDropdownItems();
			this.bindInitEvents();
		},
		initDropdownItems : function() {
			var thiss = this,
				dropdownEls = $(thiss.config.mainNavSelector).find(thiss.config.mainNavItemDropdownSelector);
			
			dropdownEls.hover(function(){
				$(this)
					.addClass('Cval-main-nav-hover')
					.addClass('ui-corner-top');
			}, function(){
				$(this)
					.removeClass('Cval-main-nav-hover')
					.removeClass('ui-corner-top');
			}).find('li').hover(function(){
				$(this).addClass('Cval-main-nav-submenu-hover');
			}, function(){
				$(this).removeClass('Cval-main-nav-submenu-hover');
			});
		},
		bindInitEvents : function() {
			var thiss = this;
			
			$.each(this.config.initEvents, function(i, fn){
				fn.apply(thiss);
			})
		},
		activateItem : function(id) {
			if (typeof id == 'object') {
				id = id.active_menu;
			}
			
			this.deactivateItems();
			if (id) {
				$(this.config.mainNavSelector)
					.find(this.config.mainNavItemSelector)
					.filter(this.config.mainNavItemIdSelectorPrefix + id)
					.addClass(this.config.mainNavItemActiveClass);
			}
		},
		deactivateItems : function() {
			$(this.config.mainNavSelector)
				.find(this.config.mainNavItemSelector)
				.removeClass(this.config.mainNavItemActiveClass);
		}
	}

})(Cval, jQuery)