(function(Cval, $) {

	Cval.page.control.def.extend('Cval.page.control.dashboard', {
		init : function(callback) {
			var thiss = this;

			// Needed resources
			var resources = [
				Cval.route('cval/media', {
					file : 'css/page/dashboard.css'
				})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});	
		}
	}, {});

})(Cval, jQuery)