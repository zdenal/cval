(function(Cval, $) {

	$.Class.extend('Cval.page.control.def', {
		defaults : {
			/**
			 * Page class element selector. Generated from selectorClass.
			 */
			selector : null,
			/**
			 * Page class element selector, if not set, is generated from id and
			 * selector prefix.
			 */
			selectorClass : null,
			/**
			 * Class selector prefix used for selector construct if not set.
			 */
			selectorClassPrefix : 'Cval-page-id-',
			/**
			 * id used for selector construct if not set.
			 */
			id : null,
			/**
			 * Refresh page at each show() call.
			 */
			refreshOnShow : true,
			/**
			 * Refresh page at each show() call.
			 */
			destroyOnClose : false,
			/**
			 * Page holder
			 */
			parentElement : '#Cval-content',
			/**
			 * Page main element
			 */
			mainHtml : '<div/>',
			/**
			 * Page main element additional classes
			 */
			mainClass : '',
			/**
			 * Page main element classes. It must be same for all instances,
			 * otherwise this.closeOtherPages will not work properly.
			 */
			pageClass : 'Cval-page',
			/**
			 * Load URL used for content loading. Can be string or JSON object
			 * with route and routeParams
			 */
			loadUrl : null,
			/**
			 * Load config data used in load call.
			 */
			loadConfig : {},
			/**
			 * Initiated
			 */
			isInitiated : false,
			/**
			 * If TRUE this page will set to history after show
			 */
			inHistory : true,
			/**
			 * Show params. Params from show method argument.
			 */
			showParams : {}
		},
		init : function(callback) {
			Cval.core.callback(callback);	
		},
		data : function(el, data) {
			if (data !== undefined)
				$(el).data('Cval_page', data);
			
			data = $(el).data('Cval_page');
			if (data === undefined)
				$(el).data('Cval_page', {});
			
			return $(el).data('Cval_page');
		},
		dataExtend : function(el, data) {
			return this.data(el, $.extend(true, this.data(el), data));
		},
		show : function(params, callback) {
			var thiss = this;
			
			this.defaults.showParams = params || {};
			this._initId();
			this._beforeDisplay(function(){
				thiss._display(callback);
			})
		},
		reload : function(callback) {
			var thiss = this;
			
			this.refresh(function(){
				thiss._show();
				Cval.core.callback(callback);
			})
		},
		_initId : function() {
			if ( ! this.defaults.isInitiated) {
				// Construct id and selector
				if (this.defaults.id === null) {
					this.defaults.id = this.shortName;
				}
				if (this.defaults.selectorClass === null) {
					this.defaults.selectorClass = this.defaults.selectorClassPrefix + this.defaults.id;
				}
				// create selector
				this.defaults.selector = '.' + this.defaults.selectorClass;

				this.defaults.isInitiated = true;
			}
		},
		_beforeDisplay : function(callback) {
			Cval.core.callback(callback);
		},
		_display : function(callback) {
			var thiss = this;
			
			if (this.elementExists() && ! this.defaults.refreshOnShow) {
				thiss._show();
				Cval.core.callback(callback);
				return;
			}
			if ( ! this.elementExists()) {
				this.createElement();
			}
			this.refresh(function(){
				thiss._show();
				Cval.core.callback(callback);
			})
		},
		elementExists : function() {
			return ($(this.defaults.selector).length < 1) ? false : true;
		},
		getElement : function() {
			if ( ! this.elementExists()) {
				this.createElement();
			}
			return $(this.defaults.selector);
		},
		createElement : function() {
			$(this.defaults.parentElement).append(
				$(this.defaults.mainHtml)
					.addClass(this.defaults.pageClass)
					.addClass(this.defaults.selectorClass)
					.addClass(this.defaults.mainClass)
					.hide()
			);
			this.dataExtend($(this.defaults.selector), {
				control : this
			});
		},
		_addToHistory : function() {
			if (this.defaults.inHistory) {
				Cval.history.pageShow(this.defaults.id.replace('_', '/'), this.defaults.showParams);
			}
		},
		_show : function() {
			this.closeOtherPages();
			this.getElement().show();
			// Try to activate menu
			Cval.menu.activateItem(this.getLoadedResponse());
			this._addToHistory();
		},
		hide : function() {
			this._hide();
		},
		_hide : function() {
			if (this.defaults.destroyOnClose)
				this.getElement().remove();
			else
				this.getElement().hide();
		},
		setContent : function(content, callback) {
			this.getElement().html(content);
			this._afterSetContent(callback);
		},
		_afterSetContent : function(callback) {
			this._bindEvents();
			
			// Inject js data
			Cval.ajax.data.inject(this.getLoadedResponse());
			// Run callback
			Cval.core.callback(callback);
		},
		_bindEvents : function() {},
		closeOtherPages : function() {
			var otherPages = $(this.defaults.parentElement)
				.find('.'+this.defaults.pageClass)
				.not(this.defaults.selector);
				
			$.each(otherPages, function(){
				var control = Cval.page.getControl($(this));
				if (control)
					control.hide();
			});
		},
		refresh : function(callback) {
			var thiss = this;
			
			thiss.loadContent(function(){
				thiss._refreshContentUpdate(function(){
					thiss._afterRefresh(callback);
				});
			})
		},
		_refreshContentUpdate : function(callback) {
			this.setContent(this.getLoadedResponse('html', ''), callback);
		},
		_afterRefresh : function(callback) {
			// Have message, so display it
			var loadedResponse = this.getLoadedResponse();
			if (loadedResponse.message !== undefined)
			{
				Cval.message.fromOptions(loadedResponse.message);
			}
			
			// Run callback
			Cval.core.callback(callback);
		},
		getLoadedResponse : function(key, def) {
			var loadedResponse = this.data(this.getElement()).loadedResponse;
			
			if ( ! loadedResponse)
				loadedResponse = {};
			
			if (key === undefined)
				return loadedResponse;
			
			var val = loadedResponse[key];
			return val === undefined ? def : val;
		},
		loadContent : function(callback) {
			var thiss = this;
			
			Cval.ajax.callModal(this.getLoadConfig(), function(json) {
				//$.Console.Debug(thiss.getLoadConfig())
				// Backup json response in element
				thiss.data(thiss.getElement()).loadedResponse = json;
				
				// Run callback
				Cval.core.callback(callback);
			})
		},
		getLoadConfig : function() {
			return $.extend({}, this.defaults.loadConfig, this.getLoadUrl());
		},
		getLoadUrl : function() {
			var loadUrl = this.defaults.loadUrl;
			
			if ( ! loadUrl) {
				loadUrl = this.getUrlFromId();
				this.defaults.loadUrl = loadUrl;
			}
			
			if (typeof loadUrl == 'object') {
				return loadUrl;
			}
			return { url : loadUrl };
		},
		getUrlFromId : function() {
			return { route : this.defaults.id };
		},
		isVisible : function() {
			return this.getElement().is(':visible');
		}
	}, {});

})(Cval, jQuery)