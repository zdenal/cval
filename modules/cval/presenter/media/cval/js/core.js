
(function(Cval, $) {

	Cval.core = {
		init : function() {
			Cval.resource.init();
			Cval.core.modal.init();
			Cval.message.init();
			Cval.ui.init();
			Cval.menu.init();
			Cval.dialog && Cval.dialog.init();
			Cval.calendar && Cval.calendar.init();
			Cval.history && Cval.history.init();
		},
		isLogged : function() {
			return Cval.config.logged_user.username ? true : false;
		},
		exception : function(str, params) {
			if (params !== undefined && typeof params == 'object') {
				$.each(params, function(key, val){
					str = str.replace(key, val);
				})
			}
			
			// Show error in message
			if (Cval.message !== undefined)
				Cval.message.error(str, null, {
					autoDestroy : false
				});
			
			throw str;
		},
		callback : function(callback) {
			var args = [];
			$.each(arguments, function(i, arg){
				if (i)
					args.push(arg);
			})
			
			if (callback) return callback.apply(null, args);
			return null;
		},
		callbackApply : function(callback, context) {
			var args = [];
			$.each(arguments, function(i, arg){
				if (i > 1)
					args.push(arg);
			})
			
			if (callback) return callback.apply(context, args);
			return null;
		},
		/**
		 * Save all selector buttons state and disable them
		 */
		freeze : function(selector, key){
			if ( ! key)
				key = '';

			$(selector).each(function(){
				$(this).data('Cval-button-freeze-disabled'+key, $(this).button("option", "disabled"));
				$(this).button("disable");
			})
		},
		/**
		 * Restore all selector buttons state
		 */
		unfreeze : function(selector, key){
			if ( ! key)
				key = '';

			$(selector).each(function(){
				$(this).button("option", "disabled", $(this).data('Cval-button-freeze-disabled'+key));
			})
		},
		config : function (key, default_) {
			var value = Cval.config[key];
			if (value === undefined) {
				value = default_;
			}
			return value;
		},
		dialog : function (html, addClass, removeClass) {
			var dialogClass = 'Cval-dialog';
			
			var suffix = 0;
			while (true) {
				if ($('.'+dialogClass+suffix).length > 0)
					suffix++;
				else
					break;
			}
			dialogClass += suffix;
			
			$('body').append('<div class="Cval-dialog '+dialogClass+' Cval-dialog-ajax container_16 ui-helper-hidden"/>');
			
			var dialog = $('.'+dialogClass);
			
			if (html !== undefined)
				dialog.html(html).prepend('<a href="" class="Cval-focus-catcher"/>');
			
			if (addClass)
				dialog.addClass(addClass);
			
			if (removeClass)
				dialog.removeClass(removeClass);
			
			return dialog;
		},
		tabsDialog : function (html, addClass, removeClass) {
			var dialog = this.dialog(html, 
				addClass ? 'Cval-dialog-tabs '+addClass : 'Cval-dialog-tabs', 
				removeClass ? 'container_16 '+removeClass : 'container_16');
			
			return dialog;
		},
		/**
		 * Standart OK/Cancel dialog, on OK  callbackOKFunction is triggered
		 * on cancel callbackCancelFunction
		 */		
		confirmDialog : function (message, title, callbackOKFunction, callbackCancelFunction){
			var dialog = $('#Cval-dialog-confirm').Cval_dialog({		
					title: title,
					width: 300,
					resizable: false,
					height: 'auto',
					minHeight: 20,
					buttons: [
						{
							text : Cval.i18n.get("OK"),
							click : function() {
								Cval.core.callback(callbackOKFunction);
								$(this).dialog( "close" );
							}
						},
						{
							text : Cval.i18n.get("Cancel"),
							click : function() {
								Cval.core.callback(callbackCancelFunction);
								$(this).dialog( "close" );
							}
						}
					]
				});	
			dialog.html(message);
			dialog.Cval_dialog("open");
			return null;
		},
		data : {
			inject : function (data) {
				$.each(data, function(el, elData){
					el = $(el);
					$.each(elData, function(key, value){
						el.Cval_data(key, value);
					})
				})
			}
		},
		memory : {
			storage : {},
			set : function(key, val) {
				this.storage[key] = val;
				return val;
			},
			get : function(key) {
				return this.storage[key];
			}
		},
		modal : {
			config : {
				modalEl : '.Cval-modal',
				modalClass : 'Cval-modal',
				defaultHtml : '<div class="Cval-modal-ajax-loading"/>',
				counter : 0
			},
			init : function() {
				// Append body overlay
				$('body').append($('<div/>').addClass(this.config.modalClass));
			},
			show : function(html) {
				if (this.config.counter == 0)
					$(this.config.modalEl).html(html === undefined ? this.config.defaultHtml : html).show();
				
				this.config.counter++;
			},
			hide : function() {
				if (this.config.counter > 0)
					this.config.counter--;
				
				if (this.config.counter == 0)
					$(this.config.modalEl).html('').hide();
			}
		},
		htmlClassValue : function (el, prefix, replace, replaceWith) {
			if (replace === undefined)
				replace = prefix;
			if (replaceWith === undefined)
				replaceWith = "";
			
			var val = null;
			
			$.each($(el).attr('class').split(/\s+/), function(i, htmlClass) {
				if (htmlClass.indexOf(prefix) === 0) {
					val = htmlClass.replace(replace, replaceWith);
					return false; // break the loop
				}
			})
			
			return val;
		}
	}

})(Cval, jQuery)