
(function(Cval, $) {
	
	Cval.async = function(resource, callback){
		var loaded = eval('Cval.'+resource) !== undefined;
		
		Cval.resource.get("js/"+(resource.replace(/\./g,"/")).replace(/\_/g,"/")+".js", function(){
			if ( ! loaded) {
				//$.Console.Debug('ASYNC loading: '+resource);
				eval('Cval.'+resource).init(callback);
			} else {
				Cval.core.callback(callback);
			}
		});
	}

})(Cval, jQuery)