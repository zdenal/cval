(function(Cval, $) {

	Cval.page = {
		control : {}, // storage for page controls
		config : {
			controlClassPrefix : 'Cval-page-id-',
			initiated : false
		},
		init : function(callback) {
			var thiss = this;
			
			if (thiss.config.initiated) {
				Cval.core.callback(callback);
				return;
			}
			
			thiss.config.initiated = true;
			
			this.loadControl('def', function(){
				thiss.selfInit();
				Cval.core.callback(callback);
			})
		},
		selfInit : function() {
			// Nothing to do for now...
		},
		loadControl : function(type, callback) {
			var thiss = this;
			
			// load default control if type is 
			if (type === null)
				Cval.core.exception('Page control type must be set');

			// If we have array of resources
			if (Object.prototype.toString.apply(type) === '[object Array]') {
				var i;
				// We have only one item in array
				if (type.length == 1) {
					// So create a single resource from it
					for (i in type) {
						type = type[i];
						break;
					}
				} else {
					for (i in type) {
						// Get first resource
						var t = type[i];
						// Remove this element from array
						type.splice(i, 1);
						// Load single resource and construct callback
						// to call other resources
						Cval.async('page.control.'+t, function(){
							thiss.loadControl(type, callback);
						});
						break;
					}
					return;
				}
			}
			Cval.async('page.control.'+type, function(){
				Cval.core.callback(callback);
			});
		},
		/**
		 * Get element for specific type of page.
		 */
		getElement : function(type) {
			if (typeof type == 'object') {
				return type;
			}
			return $('.'+this.config.controlClassPrefix+type);
		},
		getControl : function(type) {
			var pageEl = this.getElement(type);
			if (pageEl.length < 1)
				return null;
			
			return Cval.page.control.def.data(pageEl).control;
		},
		show : function(type, params, callback) {
			// If type is url, not string yet
			if (this.isUrl(type)) {
				// Extract page type and params from url
				var urlParams = this.deparamUrl(type);
				type = this.extractTypeFromUrlParams(urlParams);
				params = $.extend({}, params || {}, this.extractParamsFromUrlParams(urlParams))
			}
			// Replace slashes with underscores
			type = type.replace(/\//gi, '_');
			
			var args = [];
			args.push(params);
			$.each(arguments, function(i, arg){
				if (i > 1)
					args.push(arg);
			})
			
			// Load control
			this.loadControl(type, function(){
				// Call show method on control with given args
				Cval.page.control[type].show.apply(Cval.page.control[type], args);
			});
		},
		isUrl : function(type) {
			return type.search('#') !== -1;
		},
		extractTypeFromUrlParams : function(params) {
			return params.page;
		},
		extractParamsFromUrlParams : function(params) {
			if (params.page)
				delete params.page;
			
			return params;
		},
		deparamUrl : function(url) {
			url = url
				.replace(Cval.config.url, '')
				.replace('#', '');
				
			return $.deparam(url);
		}
	}

})(Cval, jQuery);