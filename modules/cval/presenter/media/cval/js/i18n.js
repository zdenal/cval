
(function(Cval, $) {
	
	Cval.i18n = {
		get : function (key, params) {
			var str;
		
			if (this.data[key] == undefined) {
				str = key;
			} else {
				str = this.data[key];
			}
			
			if (params !== undefined && typeof params == 'object') {
				$.each(params, function(pkey, val){
					str = str.replace(pkey, val);
				})
			}
			return str;
		},
		data : {}
	}

})(Cval, jQuery)