
(function(Cval, $) {
	
	/**
	 * Cval helper
	 */
	Cval.helper = {
		form : {
			errorDebug : function(json) {
				if (json.errors)
				{
					for (var field in json.errors) {
						var error = json.errors[field];
						alert('Error['+field+']['+error.name+']: '+error.message);
					}
				}
			}
		},
		string : {
			ucfirst : function(str) {
				return str.substr(0, 1).toUpperCase() + str.substr(1);
			}
		},
		json : {
			compare : function(s1, s2) {
				for(var p in s1){
					if(s1.hasOwnProperty(p)){
						if( ! Cval.helper.compare(s1[p], s2[p])){
							return false;
						}
					}
				}
				for(var p in s2){
					if(s2.hasOwnProperty(p)){
						if( ! Cval.helper.compare(s2[p], s1[p])){
							return false;
						}
					}
				}
				return true;
			}
		},
		array : {
			compare : function(s1, s2) {
				for(var p in s1){
					if(s1[p]){
						if( ! Cval.helper.compare(s1[p], s2[p])){
							return false;
						}
					}
				}
				for(var p in s2){
					if(s2[p]){
						if( ! Cval.helper.compare(s2[p], s1[p])){
							return false;
						}
					}
				}
				return true;
			}
		},
		compare : function(val1, val2) {
			var type1 = Cval.helper.typeOf(val1),
				type2 = Cval.helper.typeOf(val2);
				
			if (type1 !== type2)
				return false;
			
			switch (type1) {
				case 'object':
					return Cval.helper.json.compare(val1, val2);
				case 'array':
					return Cval.helper.array.compare(val1, val2);
				case 'null':
					return true;
				default:
					return val1 === val2;
			}
		},
		typeOf : function(value) {
			var s = typeof value;
			if (s === 'object') {
				if (value) {
					if (value instanceof Array) {
						s = 'array';
					}
				} else {
					s = 'null';
				}
			}
			return s;
		},
		user : {
			data : function() {
				return Cval.core.config('logged_user', false);
			},
			id : function() {
				if ( ! this.loggedIn())
					return false;
				
				return this.data().id;
			},
			verified : function() {
				if ( ! this.loggedIn())
					return false;
				
				return this.data().verified;
			},
			loggedIn : function() {
				var loggedUser = this.data();
				return loggedUser ? loggedUser.username.length > 0 : false;
			}
		},
		date : {
			toString : function(date, format) {
				if ( ! format)
					format = "yyyy-MM-dd HH:mm:ss";
				
				return date.toString(format);
			}
		}
	}

})(Cval, jQuery)