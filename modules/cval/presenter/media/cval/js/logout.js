(function(Cval, $) {

	Cval.logout = {
		init : function() {
			$('.Cval-logout-trigger').click(function(){
				Cval.ajax.callModal({
					route : 'auth',
					routeParams : {
						action : 'logout'
					}
				}, function(json) {
					// We need to redirect
					if (json.redirect !== undefined)
					{
						// Redirect to new page
						window.location = json.redirect;
						return;
					}

					// Have message, so display it
					if (json.message !== undefined)
					{
						Cval.message.fromOptions(json.message);
					}
				})
				
				return false;
			})
		}
	}

	$(document).ready(function(){
		Cval.logout.init();
	})

})(Cval, jQuery)