(function(Cval, $) {

	Cval.ui = {
		config : {
			// Callbacks to run at UI init
			initEvents : {}
		},
		init : function() {
			// Create UI tabs from specific elements
			$('.Cval-tabs.Cval-tabs-live').livequery(function(){
				$(this).tabs();
			});
			// Stylize accordion elements
			$('.Cval-accordion.Cval-accordion-live').livequery(function(){
				$(this).accordion();
			});
			// Stylize form elements in specific inputs
			$('.Cval-form.Cval-form-live').livequery(function(){
				$(this).Cval_form();
			});
			// Stylize buttons
			$('.Cval-button.Cval-button-live').livequery(function(){
				$(this).Cval_button();
			});
			// Stylize hidden buttons
			$('.Cval-button-invisible').livequery(function(){
				$(this).text('.').Cval_button();
			});
			// Stylize form elements in specific inputs
			$('.Cval-menu-vertical.Cval-menu-vertical-live').livequery(function(){
				$(this).Cval_menu_vertical();
			});
		
			this.bindInitEvents();
		},
		bindInitEvents : function() {
			var thiss = this;
			
			$.each(this.config.initEvents, function(i, fn){
				fn.apply(thiss);
			})
		}
	}

})(Cval, jQuery)