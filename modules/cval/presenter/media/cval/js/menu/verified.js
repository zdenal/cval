(function(Cval, $) {

	$.extend(Cval.menu.config.initEvents, {
		checkTimeZone : function() {
			var isSet = Cval.core.config('is_timezone_set');
			if (isSet)
				return;
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'vendor/detect_timezone.js'
			})
			];

			Cval.resource.get(resources, function(){
				var timezone = jstz.determine_timezone();
				Cval.ajax.callModal({
					route : 'settings',
					routeParams : {
						controller : 'account',
						action : 'update_timezone'
					},
					data : {
						timezone : timezone.name()
					}
				}, function(json) {});
			});
		},
		linkPageClick : function() {
			$('.Cval-link-page').livequery(function(){
				$(this).click(function(){
					Cval.page.show($(this).attr('href'));
					return false;
				})
			});
		},
		startComet : function() {
			Cval.comet.init();
		},
		registerCometUnreadSchedulesCntCallback : function() {
			Cval.comet.registerCallback('unreadSchedulesCnt', function(data){
				var el = $('.Cval-unread-schedules-cnt-box');
				if (data.unread_cnt) {
					el.html('('+data.unread_cnt+')');
				} else {
					el.html('');
				}
			}, ['schedule_update', 'schedule_delete']);
		}
	});

})(Cval, jQuery)