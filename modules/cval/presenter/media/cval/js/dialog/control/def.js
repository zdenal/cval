(function(Cval, $) {

	$.Class.extend('Cval.dialog.control.def', {
		defaults : {
			dialog : {
				resizable : false,
				destroyOnClose : false
			},
			/**
			 * Page class element selector. Generated from selectorClass.
			 */
			selector : null,
			/**
			 * Page class element selector, if not set, is generated from id and
			 * selector prefix.
			 */
			selectorClass : null,
			/**
			 * Class selector prefix used for selector construct if not set.
			 */
			selectorClassPrefix : 'Cval-dialog-id-',
			/**
			 * id used for selector construct if not set.
			 */
			id : null,
			/**
			 * Refresh page at each show() call.
			 */
			refreshOnShow : true,
			/**
			 * Page main element
			 */
			mainHtml : '<div/>',
			/**
			 * Page main element additional classes
			 */
			mainClass : '',
			/**
			 * Load URL used for content loading. Can be string or JSON object
			 * with route and routeParams
			 */
			loadUrl : null,
			/**
			 * Load config data used in load call.
			 */
			loadConfig : {},
			/**
			 * Initiated
			 */
			isInitiated : false,
			/**
			 * If TRUE this page will set to history after show
			 */
			inHistory : false,
			/**
			 * If use ajax call for content load
			 */
			loadRemoteContent : true,
			/**
			 * This is usefull when loadRemoteContent is set to false, so this
			 * is used instead of remote response.
			 */
			loadedResponse : {
				/**
				 * Content of this variable is used for dialog content
				 */
				html : null
			},
			options : {},
			hideType : null
		},
		init : function(callback) {
			Cval.core.callback(callback);			
		},
		data : function(el, data) {
			if (data !== undefined)
				$(el).data('Cval-dialog', data);
			
			data = $(el).data('Cval-dialog');
			if (data === undefined)
				$(el).data('Cval-dialog', {});
			
			return $(el).data('Cval-dialog');
		},
		dataExtend : function(el, data) {
			return this.data(el, $.extend(true, this.data(el), data));
		},
		show : function(callback, options) {
			var thiss = this;
			
			this._initId();
			this._beforeDisplay(function(){
				thiss._display(callback, options);
			})
		},
		hide : function(callback, type) {
			this._hide(type);
			Cval.core.callback(callback);
		},
		_initId : function() {
			if ( ! this.defaults.isInitiated) {
				// Construct id and selector
				if (this.defaults.id === null) {
					this.defaults.id = this.shortName;
				}
				if (this.defaults.selectorClass === null) {
					this.defaults.selectorClass = this.defaults.selectorClassPrefix + this.defaults.id;
				}
				// create selector
				this.defaults.selector = '.' + this.defaults.selectorClass;

				this._hideType = this.defaults.hideType;
				this.defaults.isInitiated = true;
			}
		},
		_beforeDisplay : function(callback) {
			Cval.core.callback(callback);
		},
		_display : function(callback, options) {
			var thiss = this;
			
			if (this.elementExists() && ! this.defaults.refreshOnShow) {
				this._updateOptions(options);
				thiss._show();
				Cval.core.callback(callback);
				return;
			}
			if ( ! this.elementExists()) {
				this.createElement();
				this._updateOptions(options);
			}
			this.refresh(function(){
				thiss._show();
				Cval.core.callback(callback);
			})
		},
		_updateOptions : function(options) {
			this.dataExtend(this.getElement(), {
				options : $.extend({}, this.defaults.options, options || {})
			});
		},
		elementExists : function() {
			return ($(this.defaults.selector).length < 1) ? false : true;
		},
		getElement : function() {
			if ( ! this.elementExists()) {
				this.createElement();
			}
			return $(this.defaults.selector);
		},
		createElement : function() {
			var thiss = this,
				options = $.extend({}, this.defaults.dialog);
			options.autoOpen = false;
			options.beforeClose = function(event, ui) {
				var callbackName = '_beforeHide';
				if (thiss._hideType) {
					var hideType = Cval.helper.string.ucfirst(thiss._hideType);
					if (thiss[callbackName+hideType] !== undefined)
						callbackName = callbackName+hideType;
				}
				return thiss[callbackName].call(thiss, $(this), event, ui);
			};
		
			var dialog = options.tabs ? Cval.core.tabsDialog('') : Cval.core.dialog(''),
				thiss = this;
				
			dialog
				.addClass(this.defaults.selectorClass)
				.addClass(this.defaults.mainClass);
		
			dialog.Cval_dialog($.extend({}, options));
			
			// Backup open params
			this.dataExtend(dialog, {
				control : thiss
			})
		},
		_addToHistory : function() {
			if (this.defaults.inHistory) {
				//Cval.history.pageShow(this.defaults.id.replace('_', '/'));
			}
		},
		_show : function() {
			this.getElement().Cval_dialog('open');
			this._addToHistory();
		},
		/**
		* Is automatically set when hiding dialog. If is not empty then if
		* '_beforeHide'+_hideType method exists is called, otherwise standard
		* _beforeHide method is called.
		*/
		_hideType : null,
		_hide : function(type) {
			this._hideType = type === undefined ? this.defaults.hideType : type;
			this.getElement().Cval_dialog('close');
			this._hideType = this.defaults.hideType;
		},
		_beforeHide : function(dialog, event, ui) {},
		setContent : function(content, callback) {
			this.getElement().html(content);
			this._afterSetContent(callback);
		},
		_afterSetContent : function(callback) {
			this._bindEvents();
			// Run callback
			Cval.core.callback(callback);
		},
		_bindEvents : function() {},
		refresh : function(callback) {
			var thiss = this;
			
			thiss.loadContent(function(){
				thiss._refreshContentUpdate(function(){
					thiss._afterRefresh(callback);
				});
			})
		},
		_refreshContentUpdate : function(callback) {
			this.setContent(this.getLoadedResponse('html', ''), callback);
		},
		_afterRefresh : function(callback) {
			// Have message, so display it
			var loadedResponse = this.getLoadedResponse();
			if (loadedResponse.message !== undefined)
			{
				Cval.message.fromOptions(loadedResponse.message);
			}
			
			// Run callback
			Cval.core.callback(callback);
		},
		getLoadedResponse : function(key, def) {
			if (key === undefined)
				return this.data(this.getElement()).loadedResponse;
			
			var val = this.data(this.getElement()).loadedResponse[key];
			return val === undefined ? def : val;
		},
		loadContent : function(callback) {
			var thiss = this;
			
			// If we not need to load remote response use it from defaults
			if ( ! thiss.defaults.loadRemoteContent) {
				// Backup json response in element
				thiss.data(thiss.getElement()).loadedResponse = thiss.defaults.loadedResponse;
				
				// Run callback
				Cval.core.callback(callback);
				return;
			}
			
			Cval.ajax.callModal(this.getLoadConfig(), function(json) {
				// Backup json response in element
				thiss.data(thiss.getElement()).loadedResponse = json;
				
				// Run callback
				Cval.core.callback(callback);
			})
		},
		getLoadConfig : function() {
			return $.extend({}, this.defaults.loadConfig, this.getLoadUrl());
		},
		getLoadUrl : function() {
			var loadUrl = this.defaults.loadUrl;
			
			if ( ! loadUrl) {
				loadUrl = this.getUrlFromId();
				this.defaults.loadUrl = loadUrl;
			}
			
			if (typeof loadUrl == 'object') {
				return loadUrl;
			}
			return { url : loadUrl };
		},
		getUrlFromId : function() {
			return { route : this.defaults.id };
		}
	}, {});

})(Cval, jQuery)