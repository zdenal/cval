(function(Cval, $) {

	Cval.ajax = {
		lastAjaxCall : {
			options : {},
			success : {},
			error : {},
			errorCodes : {}
		},
		call : function (options, success, error, errorCodes, doNotSaveToLastAjax) {
			if ( ! options.url) {
				options.url = Cval.route(options.route || 'cval/ajax', options.routeParams || {});
			}

			if ( ! error) {
				error = Cval.ajax._error.global;
			}

			if ( ! errorCodes)
				errorCodes = {};

			// Set wrong user input error catcher
			if ( ! errorCodes[440]) {
				errorCodes[440] = Cval.ajax._error[440];
			}
			
			// User not logged in 
			if ( ! errorCodes[401]) {
				errorCodes[401] = Cval.ajax._error[401];
			}
			
			if(!doNotSaveToLastAjax){
				this.lastAjaxCall.options = options;
				this.lastAjaxCall.success = success;
				this.lastAjaxCall.error = error;
				this.lastAjaxCall.errorCodes = errorCodes;
			}

			return Cval.ajax._callSystem(options, success, error, errorCodes);
		},
		callModal : function (options, success, error, errorCodes, doNotSaveToLastAjax) {
			options.beforeSend = function(jqXHR, settings) {
				Cval.core.modal.show();
			}
			options.complete = function() {
				Cval.core.modal.hide();
			}
			
			return this.call(options, success, error, errorCodes, doNotSaveToLastAjax);
		},
		data : {
			inject : function (JSONobject) {
				var js = JSONobject.js || {};

				if (js.data) {
					Cval.core.data.inject(js.data);
				}
			}
		},
		_error : {
			global : function (status, statusText) {
				Cval.message.error(statusText, status);
			},
			440 : function (statusText, JSONobject) {
				var message = '';
				
				if (JSONobject)
				{
					if (typeof JSONobject == 'string')
					{
						message = JSONobject;
					}
					else if (JSONobject.errors)
					{
						$.each(JSONobject.errors, function(field, error){
							message += error.message+'.&nbsp;';
						})
					}
				}
				else
				{
					message = statusText;
				}

				Cval.message.error(message);
			},
			401 : function (statusText) {
				// For now just redirect to login page
				if (false && Cval.helper.user.loggedIn()) {
					// We are in secured area, so we can popup login dialog
				} else {
					// Otherwise redirect to standard login page
					window.location = Cval.route('login');
				}
			}
		},
		invokeLastAjaxCall: function (){			
			this.callModal(this.lastAjaxCall.options, this.lastAjaxCall.success, this.lastAjaxCall.error, this.lastAjaxCall.errorCodes);
		},
		_callSystem : function (options, success, error, errorCodes) {
			var thiss = this;
			
			if ( ! errorCodes)
				errorCodes = {};

			var settings = $.extend({
				type : 'POST',
				cache: false,
				dataType: 'json',
				success: function(JSONobject){
					thiss.processSystemResponse(JSONobject);
					if (success)
					{
						success(JSONobject);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					switch(textStatus){
						case 'timeout':
							alert('Error: Specified timeout was exceeded!');
							break;
						case 'notmodified':
							alert('Error: Requested resource was not modified since last request!');
							break;
						case 'parsererror':
							alert('Error: Wrong response format!');
							break;
						case 'error':
							if ( ! XMLHttpRequest.status)
							{
								alert('Error: You are not connected to internet or server not responds!');
							}
							else if (XMLHttpRequest.status == 555)
							{
								var errorEl = $('<div></div>')
									.css('font-family', 'sans-serif')
									.css('width', '100%')
									.css('background-color', '#911')
									.css('color', '#fff')
									.css('margin-top', '20px')
									.append($('<div></div>')
											.html(XMLHttpRequest.statusText+'<br/>'+XMLHttpRequest.responseText)
											.css('padding', '10px 20px'))

								$('body').append(errorEl);
							}
							else if (errorCodes[XMLHttpRequest.status])
							{
								try
								{
									JSONobject = $.parseJSON( XMLHttpRequest.responseText );
								}
								catch(e)
								{
									JSONobject = null;
								}
								errorCodes[XMLHttpRequest.status](
									errorThrown,
									JSONobject,
									XMLHttpRequest
								);
							}
							else if (error)
							{
								try
								{
									JSONobject = $.parseJSON( XMLHttpRequest.responseText );
								}
								catch(e)
								{
									JSONobject = null;
								}
								error(
									XMLHttpRequest.status,
									errorThrown,
									JSONobject,
									XMLHttpRequest
								);
							}
							else
							{
								alert('Error: '+XMLHttpRequest.statusText);
							}
							break;
						default:
							alert('Error: Not specified!');
					}
				}
			}, options || {});

			$.ajax(settings);

			return false;
		},
		processSystemResponse : function(JSONobject) {
			// Check for system data
			if ( ! JSONobject.cval)
				return;
			
			var data = JSONobject.cval;
			
			if (data.timezone && Cval.helper.user.verified()) {
				var date = new Date(),
					clientGMTOffset = -date.getTimezoneOffset()/60,
					differentTimeMessageClass = 'Cval-message-system-different-time';
				
				var messageEl = $('.'+differentTimeMessageClass);
				if (clientGMTOffset != data.timezone.gmt_offset) {
					var gmtOffsetsHash = clientGMTOffset+'_'+data.timezone.gmt_offset;
					
					if ( ! messageEl.length || gmtOffsetsHash != messageEl.Cval_data('gmtOffsetsHash')) {
						var messageText = 'Your computer timezone (GMT:clientOffset) differs from Cval timezone (GMT:serverOffset).'
							+ ' It can lead to wrong time calculations.'
							+ ' Please update your Timezone value in'
							+ ' <span class="Cval-helper-bold Cval-message-system-different-time-trigger-settings Cval-helper-cursor-pointer">Account settings</span>.';

						messageText = Cval.i18n.get(messageText, {
							':clientOffset' : (clientGMTOffset < 0 ? '' : '+')+''+clientGMTOffset,
							':serverOffset' : (data.timezone.gmt_offset < 0 ? '' : '+')+''+data.timezone.gmt_offset
						});
						
						if ( ! messageEl.length) {
							messageEl = Cval.message.error(messageText, null, {
								autoDestroy : false,
								closer : false,
								outerClass : differentTimeMessageClass
							});
						} else {
							messageEl.find('.Cval-message-content').html(messageText);
						}
						messageEl.find('.Cval-message-system-different-time-trigger-settings').click(function(){
							Cval.page.show('settings/account');
						});
						messageEl.Cval_data('gmtOffsetsHash', clientGMTOffset+'_'+data.timezone.gmt_offset);
					}
				} else {
					Cval.message.destroy(messageEl);
				}
			}
		}
	}

})(Cval, jQuery)