<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Sign Up controller class
 *
 * @package     Cval
 * @author      Zdenal
 */
abstract class Core_Controller_SignUp extends Controller_Cval_Template_Secure 
{
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	public function action_index()
	{
		if (Cval::logged_in())
		{
			$this->request->redirect(Route::url_protocol('home'));
		}
		
		View::set_global(array(
			'page_title' => 'Sign up',
			'active_menu' => 'signup'
		));
		
		if (Cval_Registration::is_enabled())
		{
			Cval_JS::set(array(
				'/cval/media/js/signup.js',
			));

			$this->template->content = View::factory('cval/content/signup')
					->bind('user', $user)
					->set('csrf', Security::token(TRUE, 'cval/auth/signup'));

			$user = $this->cval->user();
		}
		else
		{
			$this->template->content = View::factory('cval/content/signup/disabled');
		}
	}

} // End Core_Controller_SignUp
