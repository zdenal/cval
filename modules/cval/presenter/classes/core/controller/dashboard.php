<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Dashboard Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Dashboard extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Dashboard',
		));
		
		$view = View::factory('cval/content/dashboard')
			->bind('inbound_group', $inbound_group)
			->bind('outbound_group', $outbound_group)
			->bind('inbound_schedules_cnt', $inbound_schedules_cnt)
			->bind('outbound_schedules_cnt', $outbound_schedules_cnt);
		
		$inbound_group = Schedule_Group::factory('inbound');
		$inbound_schedules_cnt = $inbound_group->schedules_count($this->cval->user());
		
		$outbound_group = Schedule_Group::factory('outbound');
		$outbound_schedules_cnt = $outbound_group->schedules_count($this->cval->user());
		
		$this->request->response = array(
			'html' => $view->render(),
			'active_menu' => 'dashboard'
		);
	}

}

