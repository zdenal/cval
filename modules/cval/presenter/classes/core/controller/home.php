<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Home controller class
 *
 * @package     Cval
 * @author      Zdenal
 */
abstract class Core_Controller_Home extends Controller_Cval_Template_Secure 
{
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	public function action_index()
	{
		if ( ! Cval::logged_in())
		{
			$this->not_logged();
			return;
		}
		
		if ( ! Cval::allowed())
		{
			$this->request->redirect(Route::url_protocol('verification'));
			return;
		}
		
		Cval_JS::set(array(
			'/cval/media/jquery/js/jquery.ba-bbq.js',
			'/cval/media/js/page.js',
			'/cval/media/js/dialog.js',
			'/cval/media/js/calendar.js',
			'/cval/media/js/history.js',
		));
		
		$this->template->content = NULL;
	}
	
	public function not_logged()
	{
		$this->template->content = View::factory('cval/content/home');
	}

} // End Core_Controller_Dashboard
