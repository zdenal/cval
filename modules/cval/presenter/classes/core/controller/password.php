<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Login controller class
 *
 * @package     Cval
 * @author      Zdenal
 */
abstract class Core_Controller_Password extends Controller_Cval_Template_Secure 
{
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	public function action_reset()
	{
		if (Cval::logged_in())
		{
			$this->request->redirect(Route::url_protocol('home'));
		}
		
		View::set_global(array(
			'page_title' => 'Reset your password',
		));
		
		Cval_JS::set(array(
			'/cval/media/js/password/reset.js',
		));
		
		$this->template->content = View::factory('cval/content/password/reset')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/auth/password/reset'));
		
		$user = $this->cval->user();
	}

} // End Core_Controller_LogIn
