<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Dashboard controller class
 *
 * @package     Cval
 * @author      Zdenal
 */
abstract class Core_Controller_Verification extends Controller_Cval_Template_Secure 
{
	
	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = FALSE;
	
	public function action_needed()
	{	
		if (Cval::allowed())
		{
			$this->request->redirect(Route::url_protocol('home'));
		}
		if ( ! Cval::logged_in())
		{
			$this->request->redirect(Route::url_protocol('login'));
		}
		
		View::set_global(array(
			'page_title' => 'Verification needed',
		));
		
		Cval_JS::set(array(
			'/cval/media/js/registration/resend.js',
		));
		
		$this->template->content = View::factory('cval/content/verification/needed')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/auth/verification/resend'));
		
		$user = $this->cval->user();
	}
	
	public function action_verified()
	{	
		$this->template->content = View::factory('cval/content/verification/verified');
		
		Security::check($this->request->param('csrf'), 'cval/auth/verification/verified', TRUE);
		
		View::set_global(array(
			'page_title' => 'Verified!',
		));
	}
	
} // End Core_Controller_Dashboard
