<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Admin System Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Admin_System extends Controller_Admin_Only {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'System overview',
		));
		
		$html = View::factory('cval/content/admin/system')
				->bind('cval', $cval)
				->bind('location', $location)
				->bind('comet', $comet)
				->bind('kohana', $kohana)
				->bind('database', $database);
		
		$cval = array(
			'version' => Cval::instance()->config('version'),
			'release' => Cval::instance()->config('release')
		);
		$location = Cval_Location::config();
		$comet = Cval_Comet::config();
		
		$kohana = array(
			'environment' => Kohana::$environment,
		);
		
		$database_config = Kohana::config('database.default.connection');
		$dsn = Arr::get($database_config, 'dsn');
		parse_str(str_replace(';', '&', substr($dsn, strpos($dsn, ':')+1)), $dsn_parts);
		
		$database = array(
			'host' => Arr::get($dsn_parts, 'host'),
			'dbname' => Arr::get($dsn_parts, 'dbname'),
			'username' => Arr::get($database_config, 'username'),
			'password' => Arr::get($database_config, 'password') ? TRUE : FALSE
		);
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}

}

