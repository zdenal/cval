<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Basic admin abstract controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Admin_Only extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'admin';

}

