<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Admin Registration Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Admin_Registration extends Controller_Admin_Only {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Registration',
		));
		
		$html = View::factory('cval/content/admin/registration')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/admin/registration'))
				->bind('registration_type', $registration_type);
		
		$registration_type = Cval_Config::get('registration_type');
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}
	
	public function action_update()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/admin/registration', TRUE);
		
		$post = Security::xss_clean(Arr::extract($_POST, array(
			'registration_type',
		)));
		
		$registration_type = Arr::get($post, 'registration_type');
		
		if ( ! in_array($registration_type, array(10, 90)))
		{
			throw new Kohana_Exception('That registration type is not allowed');
		}
		
		$registration_type = Cval_Config::set('registration_type', $registration_type);
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Registration setting was updated!')),
		);
	}

}

