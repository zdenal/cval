<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Admin Remote Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Admin_Remote extends Controller_Admin_Only {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		View::set_global(array(
			'page_title' => 'Remote configuration',
		));
		
		$html = View::factory('cval/content/admin/remote')
				->bind('user', $user)
				->set('csrf', Security::token(TRUE, 'cval/admin/remote'))
				->bind('remote_connectivity', $remote_connectivity);
		
		$remote_connectivity = Cval_Config::get('remote_connectivity');
		
		$this->request->response = array(
			'html' => $html->render(),
			'active_menu' => 'account'
		);
	}
	
	public function action_update()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save', array(
			'detailed_errors_file' => 'cval/auth'
		));
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/admin/remote', TRUE);
		
		$post = Security::xss_clean(Arr::extract($_POST, array(
			'remote_connectivity',
		)));
		
		$validate = Validate::factory($post)
			->filter(TRUE, 'trim')
			->rule('remote_connectivity', 'regex', array('/^(10|50|90)$/'));

		// Check if fields are filled with exception
		$validate->check(FALSE, TRUE);
		
		Database::instance()->transaction_start();
		
		Cval_Config::set('remote_connectivity', $validate->offsetGet('remote_connectivity'));
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Remote configuration was updated!')),
		);
		
		Database::instance()->transaction_commit();
	}

}

