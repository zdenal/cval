<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Dashboard Controller
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Admin extends Controller_Admin_Only {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index() 
	{	
		$this->request->response = array(
			'html' => View::factory('cval/content/admin')->render(),
			'active_menu' => 'account'
		);
	}

}

