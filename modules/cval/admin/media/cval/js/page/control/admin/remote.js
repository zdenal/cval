(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.admin_remote', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('admin_def', function(){
					
					
					Cval.page.control.admin_def.extend('Cval.page.control.admin_remote', {
						_bindEvents : function() {
							this.initRemoteConnectivityButtons();
							
							this.getElement().find('form').submit(function(){
								var form = $(this);

								var data = form.serializeJSON();

								Cval.ajax.callModal({
									route : 'admin',
									routeParams : {
										controller : 'remote',
										action : 'update'
									},
									data : data
								}, function(json) {
									Cval.message.fromOptions(json.message);
								})

								return false;
							})
						},
						initRemoteConnectivityButtons : function() {
							var thiss = this,
								buttonset = this.getElement().find('.Cval-admin-remote_connectivity-buttonset'),
								descriptionBox = this.getElement().find('.Cval-admin-remote_connectivity-description'),
								descriptions = descriptionBox.find('> div');
							
							buttonset.find('input[name=remote_connectivity]').change(function(){
								thiss.remoteConnectivityButtonClick($(this), descriptions);
							});
							
							buttonset.buttonset();
						},
						remoteConnectivityButtonClick : function(button, descriptions) {
							var value = $(button).val();
							
							descriptions.hide();
							descriptions.filter('.Cval-admin-remote_connectivity-description-'+value).show();
						}
					}, {});
					
					
					Cval.page.control.admin_remote.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);