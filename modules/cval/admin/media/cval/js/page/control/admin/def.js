(function(Cval, $) {
	
	Cval.page.control.def.extend('Cval.page.control.admin_def', {
		defaults : {
			/**
			 * Page holder
			 */
			parentElement : '.Cval-page-id-admin-content',
			/**
			 * Page main element classes. It must be same for all instances,
			 * otherwise this.closeOtherPages will not work properly.
			 */
			pageClass : 'Cval-page-id-admin-page',
			menuSelector : '.Cval-menu-vertical-id-admin'
		},
		_beforeDisplay : function(callback) {
			var thiss = this;

			Cval.page.show('admin', null, function(){
				Cval.core.callback(callback);
			}, thiss.defaults.id);
		},
		getUrlFromId : function() {
			var parts = this.defaults.id.split('_');
			var routeConf = { 
				route : parts[0],
				routeParams : {
					controller : parts.slice(1).join('_')
				}
			};
			return routeConf;
		},
		updateMenu : function() {
			$(this.defaults.menuSelector).Cval_menu_vertical('select', '.Cval-menu-item-'+this.defaults.id);
		},
		_show : function() {
			this._super();
			this.updateMenu();
		}
	}, {});

})(Cval, jQuery);