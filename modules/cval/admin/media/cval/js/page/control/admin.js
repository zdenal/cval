(function(Cval, $) {

	Cval.page.control.def.extend('Cval.page.control.admin', {
		defaults : {
			activeSection : 'admin_registration',
			refreshOnShow : false,
			inHistory : false,
			loadUrl : {
				route : 'admin_main'
			},
			menuSelector : '.Cval-menu-vertical-id-admin'
		},
		show : function(params, callback, section) {
			if ( ! section) {
				Cval.page.show(this.defaults.activeSection);
				return;
			}
			
			// Set active section
			this.defaults.activeSection = section;
			
			this._super(params, callback);
		}
	}, {});

})(Cval, jQuery)