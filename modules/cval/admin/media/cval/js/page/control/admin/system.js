(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.admin_system', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('admin_def', function(){
					
					
					Cval.page.control.admin_def.extend('Cval.page.control.admin_system', {
					}, {});
					
					
					Cval.page.control.admin_system.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);