(function(Cval, $) {
	
	$.Class.extend('Cval.page.control.admin_registration', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('admin_def', function(){
					
					
					Cval.page.control.admin_def.extend('Cval.page.control.admin_registration', {
						_bindEvents : function() {
							this.initRegistrationTypeButtons();
							
							this.getElement().find('form').submit(function(){
								var form = $(this);

								var data = form.serializeJSON();

								Cval.ajax.callModal({
									route : 'admin',
									routeParams : {
										controller : 'registration',
										action : 'update'
									},
									data : data
								}, function(json) {
									Cval.message.fromOptions(json.message);
								})

								return false;
							})
						},
						initRegistrationTypeButtons : function() {
							var thiss = this,
								buttonset = this.getElement().find('.Cval-admin-registration_type-buttonset'),
								descriptionBox = this.getElement().find('.Cval-admin-registration_type-description'),
								descriptions = descriptionBox.find('> div');
							
							buttonset.find('input[name=registration_type]').change(function(){
								thiss.registrationTypeButtonClick($(this), descriptions);
							});
							
							buttonset.buttonset();
						},
						registrationTypeButtonClick : function(button, descriptions) {
							var value = $(button).val();
							
							descriptions.hide();
							descriptions.filter('.Cval-admin-registration_type-description-'+value).show();
						}
					}, {});
					
					
					Cval.page.control.admin_registration.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);