<div class="container_16">	
	
	<div class="grid_3">
		<ul class="Cval-menu-vertical Cval-menu-vertical-live Cval-menu-vertical-id-admin">
			<li class="Cval-menu-item-admin_registration"><a href="<?php echo Route::url_protocol('admin', array(
					'controller' => 'registration'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Registration') ?></a></li>
			<li class="Cval-menu-item-admin_remote"><a href="<?php echo Route::url_protocol('admin', array(
					'controller' => 'remote'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Remote configuration') ?></a></li>
			<li class="Cval-menu-item-admin_locations"><a href="<?php echo Route::url_protocol('admin', array(
					'controller' => 'locations'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Remote locations') ?></a></li>
			<li class="Cval-menu-item-admin_users"><a href="<?php echo Route::url_protocol('admin', array(
					'controller' => 'users'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('Users') ?></a></li>
			<li class="Cval-menu-item-admin_system"><a href="<?php echo Route::url_protocol('admin', array(
					'controller' => 'system'
				))?>" class="Cval-link-page"><?php echo Cval::i18n('System overview') ?></a></li>
		</ul>
	</div>
	
	<div class="Cval-page-id-admin-content grid_13"></div>
	
	<div class="clear"></div>
</div>