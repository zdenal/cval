<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_16 ui-widget')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="field-label grid_16">
		<?php echo Cval::i18n('Type') ?>
	</div>
	<div class="clear"></div>
	<div class="grid_16">
		<div class="Cval-admin-registration_type-buttonset">
			<?php echo Form::radio('registration_type', 10, $registration_type == 10, array(
				'id' => 'Cval-admin-registration_type-radio-10',
			)) ?>
			<label for="Cval-admin-registration_type-radio-10"><?php echo Cval::i18n('Closed') ?></label>
			<?php echo Form::radio('registration_type', 90, $registration_type == 90, array(
				'id' => 'Cval-admin-registration_type-radio-90',
			)) ?>
			<label for="Cval-admin-registration_type-radio-90"><?php echo Cval::i18n('Open') ?></label>
		</div>
	</div>
	<div class="grid_16 Cval-admin-registration_type-description Cval-helper-margin-top">
		<div class="Cval-admin-registration_type-description-10<?php echo $registration_type != 10 ? ' ui-helper-hidden' : ''?>">
			Users are created from Admin panel only. 
		</div>
		<div class="Cval-admin-registration_type-description-90<?php echo $registration_type != 90 ? ' ui-helper-hidden' : ''?>">
			Everybody is able to register to this application using e-mail address.<br/>
			Also users can be created from Admin panel. 
		</div>
	</div>
	<div class="clear"></div>

	<div class="grid_16 button-line Cval-helper-margin-top">
		<input type="submit" value="<?php echo Cval::i18n('Update') ?>" class="Cval-button Cval-button-live"/>
	</div>
	<div class="clear"></div>

<?php echo Form::close(); ?>

<div class="clear"></div>