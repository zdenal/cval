<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live grid_16 ui-widget')) ?>

	<?php echo Form::hidden('csrf', $csrf) ?>

	<div class="field-label grid_16">
		<?php echo Cval::i18n('Connectivity') ?>
	</div>
	<div class="clear"></div>
	<div class="grid_16">
		<div class="Cval-admin-remote_connectivity-buttonset">
			<?php echo Form::radio('remote_connectivity', 10, $remote_connectivity == 10, array(
				'id' => 'Cval-admin-remote_connectivity-radio-10',
			)) ?>
			<label for="Cval-admin-remote_connectivity-radio-10"><?php echo Cval::i18n('Closed') ?></label>
			<?php echo Form::radio('remote_connectivity', 50, $remote_connectivity == 50, array(
				'id' => 'Cval-admin-remote_connectivity-radio-50',
			)) ?>
			<label for="Cval-admin-remote_connectivity-radio-50"><?php echo Cval::i18n('Limited') ?></label>
			<?php echo Form::radio('remote_connectivity', 90, $remote_connectivity == 90, array(
				'id' => 'Cval-admin-remote_connectivity-radio-90',
			)) ?>
			<label for="Cval-admin-remote_connectivity-radio-90"><?php echo Cval::i18n('Open') ?></label>
		</div>
	</div>
	<div class="grid_16 Cval-admin-remote_connectivity-description Cval-helper-margin-top">
		<div class="Cval-admin-remote_connectivity-description-10<?php echo $remote_connectivity != 10 ? ' ui-helper-hidden' : ''?>">
			No remote connection are allowed.
		</div>
		<div class="Cval-admin-remote_connectivity-description-50<?php echo $remote_connectivity != 50 ? ' ui-helper-hidden' : ''?>">
			Only connection to/from paired remote locations is allowed.
		</div>
		<div class="Cval-admin-remote_connectivity-description-90<?php echo $remote_connectivity != 90 ? ' ui-helper-hidden' : ''?>">
			Connection to/from every remote locations, also not defined one, is allowed.<br/>
			When remote location requests pairing with this application, it's is automatically approved.<br/>
			Remote location is immediately paired and added to known remote locations.
		</div>
	</div>
	<div class="clear"></div>

	<div class="grid_16 button-line Cval-helper-margin-top">
		<input type="submit" value="<?php echo Cval::i18n('Update') ?>" class="Cval-button Cval-button-live"/>
	</div>
	<div class="clear"></div>

<?php echo Form::close(); ?>

<div class="clear"></div>