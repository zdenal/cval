<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<div class="grid_16 Cval-helper-margin-top-150 Cval-helper-margin-bottom-50 Cval-h3"><?php echo Cval::i18n('Cval') ?></div>
<div class="grid_2"><?php echo Cval::i18n('Version') ?>:</div>
<div class="grid_14"><?php echo $cval['version'] ?></div>
<div class="grid_2"><?php echo Cval::i18n('Release') ?>:</div>
<div class="grid_14"><?php echo $cval['release'] ?></div>
<div class="clear"></div>

<div class="grid_16 Cval-helper-margin-top-150 Cval-helper-margin-bottom-50 Cval-h3"><?php echo Cval::i18n('Location') ?></div>
<div class="grid_2"><?php echo Cval::i18n('Label') ?>:</div>
<div class="grid_14"><?php echo $location['label'] ?></div>
<div class="clear"></div>
<div class="grid_2"><?php echo Cval::i18n('URL') ?>:</div>
<div class="grid_14"><?php echo $location['url'] ?></div>
<div class="clear"></div>

<div class="grid_16 Cval-helper-margin-top-150 Cval-helper-margin-bottom-50 Cval-h3"><?php echo Cval::i18n('Comet') ?></div>
<div class="grid_2"><?php echo Cval::i18n('Enabled') ?>:</div>
<div class="grid_14"><?php echo $comet['enabled'] ? Cval::i18n('Yes') : Cval::i18n('No') ?></div>
<div class="clear"></div>
<div class="grid_2"><?php echo Cval::i18n('URL') ?>:</div>
<div class="grid_14"><?php echo $comet['url'] ?></div>
<div class="clear"></div>

<div class="grid_16 Cval-helper-margin-top-150 Cval-helper-margin-bottom-50 Cval-h3"><?php echo Cval::i18n('Database') ?></div>
<div class="grid_2"><?php echo Cval::i18n('Host') ?>:</div>
<div class="grid_14"><?php echo $database['host'] ?></div>
<div class="clear"></div>
<div class="grid_2"><?php echo Cval::i18n('Name') ?>:</div>
<div class="grid_14"><?php echo $database['dbname'] ?></div>
<div class="clear"></div>
<div class="grid_2"><?php echo Cval::i18n('User') ?>:</div>
<div class="grid_14"><?php echo $database['username'] ?></div>
<div class="clear"></div>
<div class="grid_2"><?php echo Cval::i18n('Password') ?>:</div>
<div class="grid_14"><?php echo $database['password'] ? Cval::i18n('Yes') : Cval::i18n('No') ?></div>
<div class="clear"></div>

<div class="grid_16 Cval-helper-margin-top-150 Cval-helper-margin-bottom-50 Cval-h3"><?php echo Cval::i18n('Kohana') ?></div>
<div class="grid_2"><?php echo Cval::i18n('Environment') ?>:</div>
<div class="grid_14"><?php echo $kohana['environment'] ?></div>
<div class="clear"></div>

<div class="clear"></div>