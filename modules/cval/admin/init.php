<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('admin_main', 'admin')
        ->defaults(array(
            'controller' => 'admin',
        ))->config_set('route_dash', 'page');

Route::set('admin', 'admin/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'admin',
        ))->config_set('route_dash', 'page');

