<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_ReadOnly_User
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_ReadOnly_User extends Model_User
{
	
	public function save()
	{
		throw new Kohana_Exception('Class :class do not support method :method', array(
			':class' => __CLASS__,
			':method' => __METHOD__
		));
	}
	
	public function delete()
	{
		throw new Kohana_Exception('Class :class do not support method :method', array(
			':class' => __CLASS__,
			':method' => __METHOD__
		));
	}
	
}