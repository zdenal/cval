<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for Remote Requests.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Cval_Remote_Request extends Controller_Ext 
{

	/**
	 * @var Cval	Cval instance 
	 */
	public $cval;
	
	/**
	 * @var	List of error codes and its values
	 */
	protected $_error_codes = array(
		0	=> 'Unknown error', // Exception
		100 => 'Unserialization error', // Cval_Unserialize_Exception
		150 => 'Model not found', // Model_Not_Found_Exception
		151 => 'Model not validated', // Model_Validate_Exception
	);
	
	/**
	 * @var	List of success codes and its values
	 */
	protected $_success_codes = array(
		1	=> 'OK' // Default success code
	);
	
	/**
	 * @var	int
	 */
	protected $_response_code;
	
	/**
	 * @var	array
	 */
	protected $_response_data;
	
	/**
	 * @var	int
	 */
	protected $_default_response_code = 1;
	
	/**
	 * @var	array
	 */
	protected $_default_response_data = array();

	/**
	 * Defines cval instance. In development are get data changed to post.
	 * Sets default response to json.
	 */
	public function before()
	{	
		if (Kohana::$environment === Kohana::DEVELOPMENT)
		{
			if (empty($_POST) AND ! empty($_GET))
			{
				$_POST = $_REQUEST; // Allow pass params in $_GET also
			}
		}
		
		$this->cval = Cval::instance();
		
		parent::before();
		
		// As default response is JSON
		$this->request->format = 'json';
		// Set specific exception handler
		$this->request->exception_handler = $this->_exception_handler();
	}
	
	/**
	 * Set complete response to standard Kohana response parameter.
	 */
	public function after()
	{
		// Set complete response
		$this->request->response($this->complete_response());
		
		parent::after();
	}
	
	/**
	 * Create complete response including system part and data part.
	 * 
	 * @return	array
	 */
	public function complete_response()
	{
		$system_response = $this->_system_response();
		
		if ($this->_response_data === NULL)
		{
			$this->_response_data = array();
		}
		
		// Extend response data with defaults
		$system_response['data'] = $this->_response_data + $this->_default_response_data();
		
		return $system_response;
	}
	
	/**
	 * Set response data to request response.
	 * 
	 * @param	int		$code
	 * @param	array	$data 
	 */
	public function response($code = NULL, array $data = NULL)
	{
		if (is_array($code))
		{
			$data = $code;
			$code = NULL;
		}
		$this->_response_code = $code;
		
		if ($data === NULL)
		{
			$data = array();
		}
		$this->_response_data = $data;
	}
	
	/**
	 * Gets response params from success codes array defined with code key.
	 * 
	 * @param	int		Success code
	 * @return	string|array	Single string as message or array of keys for response 
	 */
	public function success($code)
	{
		return Arr::get($this->_success_codes, $code);
	}
	
	/**
	 * Gets response params from error codes array defined with code key.
	 * 
	 * @param	int		Error code
	 * @return	string|array	Single string as message or array of keys for response 
	 */
	public function error($code)
	{
		return Arr::get($this->_error_codes, $code);
	}
	
	/**
	 * Extends error codes
	 * 
	 * @param array $codes 
	 */
	protected function _error_codes(array $codes)
	{
		$this->_error_codes = $codes + $this->_error_codes;
	}
	
	/**
	 * Extends success codes
	 * 
	 * @param array $codes 
	 */
	protected function _success_codes(array $codes)
	{
		$this->_success_codes = $codes + $this->_success_codes;
	}
	
	/**
	 * Gets default response code. Can be overidden.
	 * 
	 * @return	int
	 */
	protected function _default_response_code()
	{
		return $this->_default_response_code;
	}
	
	/**
	 * Gets default response data. Can be overidden.
	 * 
	 * @return	array
	 */
	protected function _default_response_data()
	{
		return $this->_default_response_data;
	}
	
	/**
	 * Gets system response. Can be overidden.
	 * 
	 * @return	array
	 */
	protected function _system_response()
	{
		if ($this->_response_code === NULL)
		{
			$this->_response_code = $this->_default_response_code();
		}
		$code = $this->_response_code;
		
		$data = array(
			'code' => $code
		);
		
		$extend = $this->success($code);
		if ($extend === NULL)
		{
			$data['error'] = TRUE;
			
			$extend = $this->error($code);
			if ($extend === NULL)
			{
				$extend = $this->error(0);
			}
		}
		
		if ( ! is_array($extend))
		{
			$extend = array('message' => $extend);
		}
		
		return $extend + $data;
	}
	
	/**
	 * Gets exception handler callback. Can be overriden.
	 * 
	 * @return	array
	 */
	protected function _exception_handler()
	{
		$handler = new Cval_Remote_Request_Exception_Handler($this);
		return $handler->callback();
	}

} // End Core_Controller_Cval_Remote_Request
