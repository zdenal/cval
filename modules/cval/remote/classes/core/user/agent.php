<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * User_Agent Class
 * Used for negotiating with locations
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_User_Agent 
{
	
	/**
	 * Instantiate user agent object from shared profile.
	 * 
	 * @param	Model_User_Profile_Shared	Profile for agent
	 * @return	User_Agent 
	 */
	public static function factory(Model_User_Profile_Shared $user = NULL)
	{
		if ($user === NULL)
		{
			$user = Cval::instance()->user_profile_shared();
		}
		
		$agent = new User_Agent($user);
		
		return $agent;
	}
	
	/**
	 * Creates an instance of User_Agent from serialized data.
	 *
	 * @param	array	serialized data
	 * @return	User_Agent
	 */
	public static function unserialize($data, $exception = FALSE)
	{
		$profile = Model_User_Profile_Shared::unserialize($data, $exception);
		
		$agent = $profile->agent();
		
		return $agent;
	}
	
	public function __construct(Model_User_Profile_Shared $user)
	{
		if ( ! $user->location->loaded())
		{
			throw new Kohana_Exception('Constructing user agent is allowed for user profiles with known location');
		}
		$this->_user_profile_shared = $user;
	}
	
	public function __toString()
	{
		return $this->id();
	}
	
	/**
	 * It is unique ID accross all instances.
	 * 
	 * @return string 
	 */
	public function id()
	{
		return $this->_user_profile_shared->unique_id();
	}
	
	/**
	 * Compares this agent with another. 
	 * 
	 * @param	User_Agent $agent
	 * @return	bool
	 */
	public function compare(User_Agent $agent)
	{
		return $this->id() == $agent->id();
	}
	
	/**
	 * Agent location
	 * 
	 * @return	Model_Location
	 */
	public function location()
	{
		return $this->_user_profile_shared->location;
	}
	
	/**
	 * Agent shared profile
	 * 
	 * @return	Model_User_Profile_Shared
	 */
	public function user_profile_shared()
	{
		return $this->_user_profile_shared;
	}
	
	/**
	 * Agent local profile
	 * 
	 * @return	Model_User_Profile
	 */
	public function user_profile()
	{
		if ($this->_user_profile_shared->is_remote())
		{
			throw new Kohana_Exception('Remote user agents has no local profiles');
		}
		return $this->_user_profile_shared->user_profile;
	}
	
	/**
	 * Agent local user
	 * 
	 * @return	Model_User
	 */
	public function user()
	{
		return $this->user_profile()->user;
	}
	
	/**
	 * Returns serialized data for remote calls.
	 *
	 * @return	array	serialized data
	 */
	public function serialize()
	{
		return $this->_user_profile_shared->serialize();
	}
	
	/**
	 * @var	Model_User_Profile_Shared	User profile which we create the agent for
	 */
	protected $_user_profile_shared;
}
