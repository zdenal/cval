<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Cval remote request exception handler
 *
 * @package     Cval
 * @author      Zdenek Kalina
 */
abstract class Core_Cval_Remote_Request_Exception_Handler 
{

	/**
	 * @param Controller_Cval_Remote_Request $controller 
	 */
	public function __construct(Controller_Cval_Remote_Request $controller)
	{
		$this->_controller = $controller;
	}
	
	/**
	 * Gets callback which is called when exception arise during request.
	 * 
	 * @param	string	callback type
	 * @param	array	additional params
	 * @return	Cval_Remote_Request_Exception_Handler
	 */
	public function callback($type = NULL, array $params = array())
	{
		$this->_type = $type;
		$this->_params = $params;
		
		$callback = NULL;
		
		switch ($type)
		{
			case 'save':
			case 'delete':
				$callback = array($this, 'call_rollback');
				break;
			default:
				$callback = array($this, 'call');
				break;
		}

		return $callback;
	}

	/**
	 * @return	boolean
	 */
	public function call(Exception $e, Request $request)
	{
		$success = TRUE;
		
		if (Arr::get($this->_params, 'rollback'))
		{
			// Rollback the transaction
			Database::instance()->transaction_rollback();
		}
		
		// Run internal call method to get success or error
		$success = $this->_call($e, $request);
		
		// If success, just log exception, but status is 200
		if ($success)
		{
			Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
			$request->status = 200;
			// Set complete response
			$request->response($this->_controller->complete_response());
		}
		
		return $success;
	}
	
	/**
	 * @return	boolean
	 */
	public function call_rollback(Exception $e, Request $request)
	{
		if ( ! isset($this->_params['rollback']))
		{
			$this->_params['rollback'] = TRUE;
		}
		
		return $this->call($e, $request);
	}
	
	/**
	 * @var Controller_Cval_Remote_Request 
	 */
	protected $_controller;
	
	/**
	 * @var	string
	 */
	protected $_type;
	
	/**
	 * @var	array
	 */
	protected $_params;
	
	/**
	 * @return	boolean
	 */
	protected function _call(Exception $e, Request $request)
	{
		$success = TRUE;
		
		if ($e instanceof Model_Validate_Exception)
		{
			$errors = $e->get_array()->detailed_errors(Arr::get($this->_params, 'detailed_errors_file', 'cval/validate'));

			$this->_controller->response(151, array(
				'code' => $e->getCode(),
				'message' => $e->getMessage(),
				'data' => array(
					'errors' => $errors,
					'model' => $e->get_model_name(),
					'id' => $e->get_model()->id(),
				)
			));
		}
		elseif ($e instanceof Model_Not_Found_Exception)
		{
			$this->_controller->response(150, array(
				'code' => $e->getCode(),
				'message' => $e->getMessage(),
				'data' => array(
					'model' => $e->get_model_name(),
					'load_variables' => $e->get_load_variables()
				)
			));
		}
		elseif ($e instanceof Cval_Unserialize_Exception)
		{
			$this->_controller->response(100, array(
				'code' => $e->getCode(),
				'message' => $e->getMessage(),
			));
		}
		else
		{
			// Everything else is low-level error so let throw the exception
			$success = FALSE;
		}
		
		return $success;
	}

}

