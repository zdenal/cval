<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Acts same as Jelly collection, but returns readonly models.
 * 
 * @package  Jelly
 */
class Core_Jelly_Collection_ReadOnly extends Jelly_Collection
{
	/**
	 * @var  Jelly  The current model we're placing results into
	 *
	 */
	protected $_model = NULL;

	/**
	 * @var  mixed  The current result set
	 */
	protected $_result = NULL;

	/**
	 * Tracks a database result
	 *
	 * @param  mixed  model name, model object or jelly collection
	 * @param  mixed  $result
	 */
	public function __construct($model, $result)
	{
		if (empty($model))
		{
			throw new Kohana_Exception('Jelly readonly collection needs to know its model in constructor.');
		}
		
		if ($model instanceof Jelly_Collection)
		{
			$result = $model->_result;
			$model = $model->_model;
		}
		
		if ( ! $result instanceof Database_Result)
		{
			throw new Kohana_Exception('TODO - Creating jelly readonly collection is not yet implemented for other than Database_Result object.');
		}
		
		// Get only model name
		$model = Jelly::model_prefix().'readonly_'.Jelly::model_name($model);

		// Instantiate the model, which we'll continually
		// fill with values when iterating
		$this->_model = new $model;
		
		$this->_result = $result;
	}
	
}
