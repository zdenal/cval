(function(Cval, $) {
	
	Cval_page_control_admin_locations_object = {
		staticParams : {
			defaults : {
				locationControlsSelector : '.Cval-location-controls',
				locationAddSelector : '.Cval-location-add',
				locationRemoveMoreSelector : '.Cval-location-remove-more',
				locationSelectAllSelector : '.Cval-location-selectall',
				
				locationEditSelector : '.Cval-trigger-edit',
				locationPairSelector : '.Cval-trigger-pair',
				locationInfoSelector : '.Cval-trigger-info',
				locationRemoveSelector : '.Cval-trigger-remove',
				locationListSelector : '.Cval-list-locations-wrapper',
				locationSelectorPrefix : '.Cval-model-location-id-',
				
				refreshOnShow : false
			},
			lastRefresh : {
				data : null
			},
			checkerEl : null,
			paginationEl : null,
			addButtonEl : null,
			removeButtonEl : null,
			searchInputEl : null,
			init : function(callback) {
				var thiss = this;

				// Needed resources
				var resources = [
					Cval.route('cval/media', {
						file : 'css/page/admin/locations.css'
					})
				];

				Cval.resource.get(resources, function(){
					Cval.core.callback(callback);
				});	
			},
			_show : function() {
				this._super();
				this.refreshLocationList();
			},
			_bindEvents : function() {
				this._super();
				var thiss = this;

				// Init add location button
				this.addButtonEl = this.getElement().find(this.defaults.locationAddSelector)
					.Cval_button({
						text : false,
						icons : {
							primary : 'ui-icon-home',
							secondary : 'ui-icon-plus'
						}
					})
					.click(function(){
						Cval.dialog.loadControl('location_add', function(){
							Cval.dialog.control.location_add.show();
						});
						return false;
					});
					
				// Init remove location button
				this.removeButtonEl = this.getElement().find(this.defaults.locationRemoveMoreSelector)
					.Cval_button({
						text : false,
						icons : {
							secondary : 'ui-icon-trash'
						}
					})
					.click(function(){
						var selectedLocations = thiss.getSelectedLocationsEls(),
							multipleData = [],
							multiple = true,
							data = null;

						$.each(selectedLocations, function(){
							var data = $(this).Cval_data();
							multipleData.push(data);
						});

						if (multipleData.length < 2) {
							multiple = false;
							data = multipleData.shift();
						} else {
							data = multipleData;
						}

						Cval.dialog.loadControl('location_remove', function(){
							Cval.dialog.control.location_remove.show(null, {
								data : data,
								multiple : multiple
							})
						});
						return false;
					});
					
				// Init select location button
				this.checkerEl = this.getElement().find(this.defaults.locationSelectAllSelector)
					.Cval_button({
						text : false,
						icons : {
							secondary : 'ui-icon-check'
						}
					});
				
				this.getElement().find('.Cval-location-controls').buttonset();
				this.removeButtonEl.Cval_button('option', 'disabled', true);
				
				// Bind pair location
				this.getElement().find(this.defaults.locationPairSelector).livequery(function(){
					$(this).click(function(){
						var data = thiss.getLocationEl($(this)).Cval_data();

						Cval.dialog.loadControl('location_pair', function(){
							Cval.dialog.control.location_pair.show(null, {
								data : data
							})
						});
						return false;
					})
				});
				
				// Bind edit location
				this.getElement().find(this.defaults.locationEditSelector).livequery(function(){
					$(this).click(function(){
						var data = thiss.getLocationEl($(this)).Cval_data();

						Cval.dialog.loadControl('location_edit', function(){
							Cval.dialog.control.location_edit.show(null, {
								data : data
							})
						});
						return false;
					})
				});
				
				// Bind remove location
				this.getElement().find(this.defaults.locationRemoveSelector).livequery(function(){
					$(this).click(function(){
						var data = thiss.getLocationEl($(this)).Cval_data();

						Cval.dialog.loadControl('location_remove', function(){
							Cval.dialog.control.location_remove.show(null, {
								data : data
							})
						});
						return false;
					})
				});
				
				// Bind info location
				this.getElement().find(this.defaults.locationInfoSelector).livequery(function(){
					$(this).click(function(){
						alert(Cval.todo);
						return false;
					})
				});
				
				// Hover effects for table rows
				this.getElement().find(this.defaults.locationListSelector+' tbody tr').livequery(function(){
					$(this).hover(function(){
						$(this).addClass('Cval-hover');
					}, function(){
						$(this).removeClass('Cval-hover')
					});

					$(this).find('.Cval-list-checkbox').change(function(){
						thiss.listUpdate();
					});
				});

				this._initSearch();
				this._initListControls();
			},
			refreshLocations : function(callback) {
				this.refreshLocationList(callback);
			},
			refreshLocationList : function(callback, data) {
				var thiss = this;

				data = $.extend({}, {
					body_only : 1,
					search : thiss._searchData()
				}, data || {});

				Cval.ajax.callModal({
					route : 'location',
					routeParams : {
						controller : 'list_table'
					},
					data : data
				}, function(json) {
					thiss.lastRefresh = {
						data : data
					};
					
					thiss.updateLocationList(json, false, callback);
				});
			},
			updateLocationList : function(data, complete, callback) {
				var table = this.getElement().find(this.defaults.locationListSelector);

				// Display, show no location message
				if (data.count > 0)
					this.noLocationMessageHide();
				else
					this.noLocationMessageShow();
				// Display/show table header
				table.toggle(data.count > 0);

				// Replace only body content
				if ( ! complete)
					table = table.find('tbody');

				table.html(data.html);
				// Inject js data
				Cval.ajax.data.inject(data);

				// Enable/Disable checker
				this.checkerEl.Cval_button_checker({
					checked : false,
					disabled : data.count < 1
				});
				// update pagination
				this.paginationEl.Cval_pagination('update', data.pagination);
				this.listUpdate();

				Cval.core.callback(callback);
			},
			getLocationEl : function(el) {
				el = $(el);

				if ( ! el.is('tr'))
					el = el.parents('tr:first');

				return el;
			},
			getLocationElFromId : function(id) {
				var selectorClass = this.defaults.locationSelectorPrefix+id,
					el = $(this.getElement().find(this.defaults.locationListSelector+' tbody tr'+selectorClass));

				return el;
			},
			noLocationMessageShow : function() {
				var thiss = this,
					isSearchActive = this.isSearchActive(),
					messageElClass = 'Cval-list-locations-message-nolocation_'+(isSearchActive ? 'search' : 'default');

				if (thiss.getElement().find('.Cval-list-locations-info-box .Cval-list-locations-message-nolocation.'+messageElClass).length)
					return;
				
				this.noLocationMessageHide();

				var content = isSearchActive
						? Cval.i18n.get('No locations found, try to modify your search phrase.')
						: thiss.getElement().find('.Cval-list-locations-message-nolocation-content').html();
						
				Cval.message.info(content, null, {
					wrapper : thiss.getElement().find('.Cval-list-locations-info-box'),
					autoDestroy : false,
					closer : false,
					outerClass : 'Cval-list-locations-message-nolocation '+messageElClass
				});
			},
			noLocationMessageHide : function() {
				var messageEl = this.getElement().find('.Cval-list-locations-info-box .Cval-list-locations-message-nolocation');
				Cval.message.destroy(messageEl, true);
			},
			_initListControls : function() {
				var thiss = this,
					listControls = this.getElement()
						.find('.Cval-location-list-controls');

				this.checkerEl = listControls.find('.Cval-selectall')
					.Cval_button_checker({
						disabled : true,
						clickCallback : function(check, button) {
							thiss.getElement().find(thiss.defaults.locationListSelector)
								.find('tbody tr .Cval-list-checkbox:enabled')
								.attr('checked', check);
							thiss.listUpdate();
						}
					})

				this.paginationEl = listControls.find('.Cval-paginator')
					.Cval_pagination({
						clickCallback : function(page) {
							thiss.paginateResults(page);
						}
					});

				listControls.show();
			},
			paginateResults : function(page) {
				this.refreshLocationList(null, $.extend({}, this.lastRefresh.data || {}, {
					pagination : page
				}));
			},
			_initSearch : function() {
				var thiss = this,
					form = this.getElement().find('form.Cval-form-search');

				this.searchInputEl = form.find('input[name=search]');
				form.submit(function(e){
					e.preventDefault();
					thiss.refreshLocationList();
					return false;
				});
			},
			_searchData : function() {
				return {
					global : this.searchInputEl.val().trim()
				};
			},
			isSearchActive : function() {
				return this.lastRefresh.data.search.global.length > 0;
			},
			listUpdate : function() {
				var thiss = this;

				var checkedCount = thiss.getSelectedLocationsEls().length;

				this.removeButtonEl.Cval_button('option', 'disabled', checkedCount < 1);
			},
			getSelectedLocationsEls : function() {
				return this.getElement().find(this.defaults.locationListSelector)
					.find('tbody tr')
					.filter(function(){
						return $(this).find('.Cval-list-checkbox').is(':checked')
					});
			}
		},
		params : {}
	};
		
	$.Class.extend('Cval.page.control.admin_locations', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.page.loadControl('admin_def', function(){
					
					Cval.page.control.admin_def.extend('Cval.page.control.admin_locations', 
						Cval_page_control_admin_locations_object.staticParams,
						Cval_page_control_admin_locations_object.params
					);
					
					Cval.page.control.admin_locations.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);