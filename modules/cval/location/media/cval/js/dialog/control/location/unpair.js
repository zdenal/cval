// KEXT[on]
(function(Cval, $) {

	Cval_dialog_control_location_unpair_object = {
		staticParams : {
			defaults : {
				dialog : {
					title : Cval.i18n.get('Unpair with the location'),
					CvalHeight : false,
					destroyOnClose : true,
					width: 500
				},
				mainClass : 'alpha omega Cval-dialog-no-padding',
				loadRemoteContent : false,
				refreshOnShow : false,
				buttons : {
					confirm : $('<span />').text(Cval.i18n.get('Yes')),
					cancel : $('<span />').text(Cval.i18n.get('No'))
				},
				locationLabel : $('<span />').addClass('Cval-helper-font-weight-bold'),
				contentHtml : null
			},
			init : function(callback) {
				this._createHtml();
				Cval.core.callback(callback);
			},
			_beforeHideConfirmed : function(dialog, event, ui) {
				this._beforeHide.apply(this, arguments);
				this.data(this.getElement()).options.locationControl.refresh();
				Cval.page.getControl('admin_locations').refreshLocations();
			},
			_show : function() {
				var options = this.data(this.getElement()).options;
				
				// Update location label
				this.defaults.locationLabel.text(options.data.label);
				// Update content text
				this.defaults.contentHtml.html(this._constructHtmlContent());	
				
				this._super();
			},
			_bindEvents : function() {
				var thiss = this,
					options = this.data(this.getElement()).options;

				this.defaults.buttons.confirm.click(function(){
					Cval.ajax.callModal({
						route : 'location',
						routeParams : {
							controller : 'unpair'
						},
						data : {
							id : options.data.id
						}
					}, function(json) {
						if (json.message !== undefined)
						{
							Cval.message.fromOptions(json.message);
						}

						thiss.hide(null, 'confirmed');
					});

					return false;
				});

				this.defaults.buttons.cancel.click(function(){
					thiss.hide();

					return false;
				});
			},
			_createHtml : function() {
				if (this.defaults.loadedResponse.html)
					return;

				var thiss = this;
				this.defaults.contentHtml = $('<div />')
						.addClass('Cval-dialog-content');

				var footer = $('<div />')
						.addClass('Cval-dialog-footer ui-widget-header ui-corner-bottom Cval-helper-align-right');

				this.defaults.buttons.cancel
					.addClass('Cval-button Cval-button-live')
					.appendTo(footer);

				this.defaults.buttons.confirm
					.addClass('Cval-button Cval-button-live')
					.addClass('ui-state-highlight')
					.appendTo(footer);

				this.defaults.loadedResponse.html = $('<div />')
					.append(this.defaults.contentHtml).append(footer);
			},
			_constructHtmlContent : function() {
				return $('<div />')
					.append($('<span />').text(Cval.i18n.get('Do you really want to unpair with the location "')))
					.append(this.defaults.locationLabel)
					.append($('<span />').text(Cval.i18n.get('"?')));
			}
		},
		params : {}
	}
	
	// KEXTSTART
	
	Cval.dialog.control.def.extend('Cval.dialog.control.location_unpair', 
		Cval_dialog_control_location_unpair_object.staticParams,
		Cval_dialog_control_location_unpair_object.params
	);

})(Cval, jQuery);