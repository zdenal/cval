(function(Cval, $) {

	Cval.dialog.control.def.extend('Cval.dialog.control.location_edit', {
		defaults : {
			dialog : {
				title : Cval.i18n.get('Edit location'),
				CvalHeight : false,
				destroyOnClose : true,
				width : 400
			},
			mainClass : 'alpha omega Cval-dialog-no-padding',
			loadUrl : {
				route : 'location',
				routeParams : {
					controller : 'edit'
				}
			},
			refreshOnShow : true,
			
			locationPairSelector : '.Cval-trigger-pair',
			locationUnpairSelector : '.Cval-trigger-unpair',
			
			locationBlockSelector : '.Cval-trigger-block',
			locationUnblockSelector : '.Cval-trigger-unblock'
		},
		wizardEl : null,
		init : function(callback) {
			var thiss = this;
			
			// Needed resources
			var resources = [
			Cval.route('cval/media', {
				file : 'jquery/css/jWizard.base.css'
			}),
			Cval.route('cval/media', {
				file : 'css/jquery/ui/jWizard.css'
			}),
			Cval.route('cval/media', {
				file : 'js/jquery/ui/jWizard.js'
			})
			];

			Cval.resource.get(resources, function(){
				Cval.core.callback(callback);
			});
		},
		_beforeHide : function(dialog, event, ui) {
			this._super.apply(this, arguments);
			
			/*Cval.core.callback(function(){
				Cval.page.getControl('admin_locations').refreshLocations();
			});*/
		},
		_bindEvents : function() {
			var thiss = this;
			
			this.wizardEl = this.getElement().find('.jWizard');
			
			this.wizardEl.Cval_jWizard({
				headerClass : '',
				buttons: {
					cancelHide: true,
					cancelText: Cval.i18n.get('Close'),
					finishText: Cval.i18n.get('Close')
				},
				hideTitle : true,
				next : function(event, ui) {
					thiss.hide();
				},
				finish : function(event, ui) {
					thiss.hide();
				},
				cancel : function(event, ui) {
					thiss.hide();
				}
			});
			
			this._initWizard();
			
			this._initButtons();
		},
		getLoadConfig : function() {
			return $.extend({}, this._super(), {
				data : {
					id : this.data(this.getElement()).options.data.id
				}
			});
		},
		_initWizard : function() {
			var thiss = this;
			
			this.wizardEl.find('.Cval-dialog-location-edit form').submit(function(e){
				e.preventDefault();
				// On form submit do nothing
				return false;
			})
		},
		_initButtons : function() {
			var thiss = this,
				pairTrigger = this.wizardEl.find(this.defaults.locationPairSelector),
				allowPair = this.getLoadedResponse('allow_pair'),
				pairingDisabledText = this.getLoadedResponse('pairing_disabled_text');
			
			pairTrigger.click(function(){
				if ( ! allowPair) {
					alert(pairingDisabledText);
				} else {
					thiss._pair();
				}
			}).Cval_button();
			if ( ! allowPair) {
				pairTrigger.Cval_button('disable');
			}

			this.wizardEl.find(this.defaults.locationUnpairSelector).click(function(){
				thiss._unpair();
			}).Cval_button();
			
			this.wizardEl.find(this.defaults.locationBlockSelector).click(function(){
				thiss._block();
			}).Cval_button();
			
			this.wizardEl.find(this.defaults.locationUnblockSelector).click(function(){
				thiss._unblock();
			}).Cval_button();
		},
		_pair : function() {
			var thiss = this,
				data = this.data(this.getElement()).options.data;

			Cval.dialog.loadControl('location_pair', function(){
				Cval.dialog.control.location_pair.show(null, {
					data : data,
					locationControl : thiss
				})
			});
			return false;
		},
		_unpair : function() {
			var thiss = this,
				data = this.data(this.getElement()).options.data;

			Cval.dialog.loadControl('location_unpair', function(){
				Cval.dialog.control.location_unpair.show(null, {
					data : data,
					locationControl : thiss
				})
			});
			return false;
		},
		_block : function() {
			var thiss = this,
				data = this.data(this.getElement()).options.data;

			Cval.dialog.loadControl('location_block', function(){
				Cval.dialog.control.location_block.show(null, {
					data : data,
					locationControl : thiss
				})
			});
			return false;
		},
		_unblock : function() {
			var thiss = this,
				data = this.data(this.getElement()).options.data;

			Cval.dialog.loadControl('location_unblock', function(){
				Cval.dialog.control.location_unblock.show(null, {
					data : data,
					locationControl : thiss
				})
			});
			return false;
		}
	}, {});

})(Cval, jQuery)