<?php

defined('SYSPATH') or die('No direct script access.');	

Route::set('locations', 'locations/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'locations',
        ))->config_set('route_dash', 'page');

Route::set('location', 'location/<controller>(/<action>)')
        ->defaults(array(
            'directory' => 'location',
        ));

