<div class="grid_16 Cval-page-label">
	<?php echo Cval::i18n($page_title) ?>
</div>

<div class="grid_16 Cval-menu-id-locations Cval-helper-margin-top-50" style="text-align: right">
	<div class="Cval-helper-float-left Cval-location-list-controls ui-helper-hidden">
		<span class="Cval-selectall"></span>
		<span class="Cval-paginator"></span>
	</div>
	<div class="Cval-helper-float-right">
		<span class="Cval-location-controls">
			<span class="Cval-location-add"><?php echo Cval::i18n('Add location') ?></span>
			<span class="Cval-location-remove-more"><?php echo Cval::i18n('Remove location') ?></span>
		</span>
	</div>
	<div class="grid_5 Cval-helper-float-right">
		<?php echo View::factory('cval/util/search/input'); ?>
	</div>
</div>
<div class="clear"></div>

<div class="Cval-page-id-locations-content grid_16">
	<?php echo View::factory('cval/content/locations/list/content')->include_view($thiss); ?>
</div>

<div class="clear"></div>