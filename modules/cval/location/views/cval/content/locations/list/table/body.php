<?php foreach ($locations as $location): ?>
<tr class="<?php echo $location->html_class() ?>">
	<td class="Cval-column-listcheck"><input type="checkbox" class="Cval-list-checkbox"/></td>
	<td class="Cval-column-label">
		<div><?php echo $location->label ?></div>
	</td>
	<td class="Cval-column-url">
		<div><?php echo $location->url ?></div>
	</td>
	<td class="Cval-column-paired">
		<?php if ($location->is_paired()): ?>
		<span class="ui-state-highlight-text" title="<?php echo Cval::i18n('Location is paired') ?>"><?php echo Cval::i18n('Yes') ?></span>
		<?php else: ?>
		<span title="<?php echo Cval::i18n('Location is not paired') ?>"><?php echo Cval::i18n('No') ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-requested">
		<?php if ($location->is_requested()): ?>
			<?php if ($location->is_requested(TRUE) AND $location->is_requested(FALSE)): ?>
			<span class="ui-state-highlight-text" title="<?php echo Cval::i18n('Both local and remote pairing requests are registered') ?>"><?php echo Cval::i18n('Yes') ?></span>
			<?php elseif ($location->is_requested(TRUE)): ?>
			<span title="<?php echo Cval::i18n('Local pairing requests is registered') ?>"><?php echo Cval::i18n('Yes') ?></span>
			<?php else: ?>
			<span class="ui-state-highlight-text" title="<?php echo Cval::i18n('Remote pairing requests is registered') ?>"><?php echo Cval::i18n('Yes') ?></span>
			<?php endif; ?>
		<?php else: ?>
			<span title="<?php echo Cval::i18n('No pairing request is registered') ?>"><?php echo Cval::i18n('No') ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-blocked">
		<?php if ($location->is_blocked()): ?>
			<?php if ($location->is_blocked(TRUE) AND $location->is_blocked(FALSE)): ?>
			<span class="ui-state-error-text" title="<?php echo Cval::i18n('Location is blocked both locally and remotelly') ?>"><?php echo Cval::i18n('Yes') ?></span>
			<?php elseif ($location->is_blocked(TRUE)): ?>
			<span title="<?php echo Cval::i18n('Location is blocked locally') ?>"><?php echo Cval::i18n('Yes') ?></span>
			<?php else: ?>
			<span class="ui-state-error-text" title="<?php echo Cval::i18n('Location is blocked remotelly') ?>"><?php echo Cval::i18n('Yes') ?></span>
			<?php endif; ?>
		<?php else: ?>
			<span title="<?php echo Cval::i18n('Location is not blocked') ?>"><?php echo Cval::i18n('No') ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-used">
		<?php if ($location->is_used()): ?>
		<span title="<?php echo Cval::i18n('Location is used') ?>"><?php echo Cval::i18n('Yes') ?></span>
		<?php else: ?>
		<span title="<?php echo Cval::i18n('Location is not used') ?>"><?php echo Cval::i18n('No') ?></span>
		<?php endif; ?>
	</td>
	<td class="Cval-column-actions">
		<span class="ui-icon ui-icon-trash Cval-helper-float-right Cval-trigger-remove" title="<?php echo Cval::i18n('Remove') ?>"></span>
		<span class="ui-icon ui-icon-pencil Cval-helper-float-right Cval-trigger-edit" title="<?php echo Cval::i18n('Modify') ?>"></span>
		<?php if ($location->is_responding(FALSE)): ?>
		<span class="ui-icon ui-icon-notice ui-state-error-icon Cval-helper-float-right ui-state-noclick" title="<?php echo Cval::i18n('Location is not responding') ?>"></span>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach; ?>