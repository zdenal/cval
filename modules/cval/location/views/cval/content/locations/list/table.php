<table cellspacing="0" cellpadding="0" class="Cval-list Cval-list-locations">
	<thead>
		<tr>
			<th class="Cval-column-listcheck"><div></div></th>
			<th class="Cval-column-label"><div><?php echo Cval::i18n('Name') ?></div></th>
			<th class="Cval-column-url"><div><?php echo Cval::i18n('URL') ?></div></th>
			<th class="Cval-column-paired" title="<?php echo Cval::i18n('Paired') ?>"><div><?php echo Cval::i18n('P') ?></div></th>
			<th class="Cval-column-requested" title="<?php echo Cval::i18n('Pairing requested') ?>"><div><?php echo Cval::i18n('R') ?></div></th>
			<th class="Cval-column-blocked" title="<?php echo Cval::i18n('Blocked') ?>"><div><?php echo Cval::i18n('B') ?></div></th>
			<th class="Cval-column-used" title="<?php echo Cval::i18n('Used') ?>"><div><?php echo Cval::i18n('U') ?></div></th>
			<th class="Cval-column-actions"><div></div></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>