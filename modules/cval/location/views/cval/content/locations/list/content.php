<div class="container_16 full">
	
	<div class="Cval-list-locations-message-nolocation-content ui-helper-hidden">
		<?php echo View::factory('cval/content/locations/list/message/nolocation')->include_view($thiss) ?>
	</div>
	
	<div class="grid_16 full Cval-list-locations-info-box Cval-helper-margin-top-50"></div>
	
	<div class="grid_16 full Cval-list-wrapper Cval-list-locations-wrapper">
		<?php echo $table ?>
	</div>
		
	<div class="clear"></div>
</div>