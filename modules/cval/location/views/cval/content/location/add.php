<div class="jWizard">
	<div class="Cval-dialog-location-add">
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live')) ?>
		<?php echo Form::hidden('csrf', $csrf) ?>
		<h3>Insert location URL</h3>

		<div class="field-label grid_16">
			<?php echo UTF8::ucfirst(Cval::i18n($location->meta()->fields('url')->label)) ?>
			<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
		</div>
		<div class="clear"></div>
		<div class="field grid_16">
			<?php echo $location->input('url') ?>
		</div>
		<div class="clear"></div>
		<div class="field grid_16">
			<?php echo Form::checkbox('autopair', 1, $allow_pairing, array(
				'disabled' => $allow_pairing ? NULL : 'disabled',
				'class' => 'Cval-field-notlive'
			)) ?>
			<?php echo Cval::i18n('Pair with this location after create.') ?>
			<?php if ( ! $allow_pairing): ?>
			<span class="field-description"><?php echo Cval::i18n('This option is enabled only for other than Closed connectivity.') ?></span>
			<?php endif; ?>
		</div>
		<div class="clear"></div>
		
		<?php echo Form::close(); ?>
	</div>
</div>