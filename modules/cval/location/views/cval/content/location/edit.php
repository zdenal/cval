<div class="jWizard">
	<div class="Cval-dialog-location-edit">
		<?php echo Form::open(NULL, array('class' => 'Cval-form Cval-form-live')) ?>

		<div class="field-label grid_16">
			<?php echo Cval::i18n('Name') ?>
		</div>
		<div class="clear"></div>
		<div class="grid_16">
			<?php if ( ! strlen($location->label)): ?>
			<span class="field-description"><?php echo Cval::i18n('Unknown') ?></span>
			<?php else: ?>
			<?php echo $location->label ?>
			<?php endif; ?>
		</div>
		<div class="clear"></div>
		<div class="field-label Cval-helper-margin-top grid_16">
			<?php echo Cval::i18n('URL') ?>
		</div>
		<div class="clear"></div>
		<div class="grid_16">
			<?php echo $location->url ?>
		</div>
		<div class="clear"></div>
		<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Is used') ?>
		</div>
		<div class="clear"></div>
		<div class="grid_8 Cval-helper-margin-top-50">
			<?php echo ($location->is_used() ? Cval::i18n('Yes') : Cval::i18n('No')) ?>
		</div>
		<div class="clear"></div>
		<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Paired') ?>
		</div>
		<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Pairing request') ?>
		</div>
		<div class="clear"></div>
		<div class="grid_2 Cval-helper-margin-top-50">
			<?php echo ($location->is_paired() ? Cval::i18n('Yes') : Cval::i18n('No')) ?>
		</div>
		<div class="grid_6">
			<?php if ($location->is_paired()): ?>
			<span class="Cval-trigger-unpair"><?php echo Cval::i18n('Unpair') ?></span>
			<?php else: ?>
			<span class="Cval-trigger-pair"><?php echo Cval::i18n('Pair') ?></span>
			<?php endif; ?>
		</div>
		<div class="grid_8 Cval-helper-margin-top-50">
			<?php if ($location->is_requested()): ?>
				<?php if ($location->is_requested(TRUE) AND $location->is_requested(FALSE)): ?>
				<?php echo Cval::i18n('Local') ?>, <span class="ui-state-highlight-text"><?php echo Cval::i18n('Remote') ?></span>
				<?php elseif ($location->is_requested(TRUE)): ?>
				<?php echo Cval::i18n('Local') ?>
				<?php else: ?>
				<span class="ui-state-highlight-text"><?php echo Cval::i18n('Remote') ?></span>
				<?php endif; ?>
			<?php else: ?>
				<?php echo Cval::i18n('No') ?>
			<?php endif; ?>
		</div>
		<div class="clear"></div>
		<!--<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Local pairing request') ?>
		</div>
		<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Remote pairing request') ?>
		</div>
		<div class="clear"></div>
		<div class="grid_8">
			<?php echo ($location->is_requested(TRUE) ? Cval::i18n('Yes') : Cval::i18n('No')) ?>
		</div>
		<div class="grid_8">
			<?php echo ($location->is_requested(FALSE) ? Cval::i18n('Yes') : Cval::i18n('No')) ?>
		</div>
		<div class="clear"></div>-->
		<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Local block') ?>
		</div>
		<div class="field-label Cval-helper-margin-top grid_8">
			<?php echo Cval::i18n('Remote block') ?>
		</div>
		<div class="clear"></div>
		<div class="grid_2 Cval-helper-margin-top-50">
			<?php echo ($location->is_blocked(TRUE) ? Cval::i18n('Yes') : Cval::i18n('No')) ?>
		</div>
		<div class="grid_6">
			<?php if ($location->is_blocked(TRUE)): ?>
			<span class="Cval-trigger-unblock"><?php echo Cval::i18n('Unblock') ?></span>
			<?php else: ?>
			<span class="Cval-trigger-block"><?php echo Cval::i18n('Block') ?></span>
			<?php endif; ?>
		</div>
		<div class="grid_8 Cval-helper-margin-top-50">
			<?php echo ($location->is_blocked(FALSE) ? Cval::i18n('Yes') : Cval::i18n('No')) ?>
		</div>
		<div class="clear"></div>
		
		<?php echo Form::close(); ?>
	</div>
</div>