<?php

defined('SYSPATH') or die('No direct script access.');

class Location_Not_Responding_Exception extends Core_Location_Not_Responding_Exception {}
