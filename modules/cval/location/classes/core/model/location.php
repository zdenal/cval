<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Location
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Location extends Jelly_Model 
{
	const STATE_NOT_REQUESTED = 5;
	const STATE_REQUESTED_LOCAL = 10;
	const STATE_REQUESTED_REMOTE = 20;
	const STATE_BLOCKED_LOCAL = 50;
	const STATE_BLOCKED_REMOTE = 60;
	const STATE_PAIRED = 90;
	
	/**
	 * @var	Model_Location	Local location
	 */
	protected static $_local_location;
	
    public static function initialize(Jelly_Meta $meta) 
	{	
        $meta->name_key('label')
             ->sorting(array('label' => 'ASC'));

		$meta->fields(array(
            'id' => new Field_Primary,
			'url' => new Field_URL(array(
				'label' => 'URL',
				'unique' => TRUE,
				'rules' => array(
					'max_length' => array(1000),
					'not_empty' => NULL
				),
				'after_set_callback' => 'Model_Location::_after_set_url'
			)),
			'pairing_key' => new Field_String(array(
				'rules' => array(
					'max_length' => array(32),
					'min_length' => array(32),
				)
			)),
			'is_paired' => new Field_Boolean(array(
				'default' => 0
			)),
			'is_remote' => new Field_Boolean(array(
				'default' => TRUE
			)),
			'is_responding' => new Field_Boolean(array(
				'default' => 1
			)),
			'remote_block' => new Field_Boolean(array(
				'default' => 0
			)),
			'remote_request' => new Field_Boolean(array(
				'default' => 0
			)),
			'local_block' => new Field_Boolean(array(
				'default' => 0
			)),
			'local_request' => new Field_Boolean(array(
				'default' => 0
			)),
			'user_profiles_shared' => new Field_HasMany,
			'schedule_requests' => new Field_HasMany
        ));
		
        Behavior_Labelable::initialize($meta, array(
			'label' => 'name',
			'rules' => array(
				'max_length' => array(255)
			)
		));

        Behavior_Timestampable::initialize($meta);
        Behavior_Userable::initialize($meta);
    }
	
	/**
	 * Loads location from ID. If ID is not specified, local instance is
	 * returned.
	 * 
	 * @param	int|Model_Location	Location ID or loaded location
	 * @return	Model_Location		Location instance
	 * @throws	Kohana_Exception	If location is not found
	 */
	public static function load($id = NULL)
	{
		if (empty($id))
		{
			return Model_Location::load_local();
		}
		
		// Location is already loaded
		if ($id instanceof Model_Location)
		{
			return $id;
		}
		
		$location = Jelly::select('location')
			->load($id);
		
		if ( ! $location->loaded())
		{
			throw Model_Not_Found_Exception::create($location->meta()->model(), $id);
		}
		
		return $location;
	}
	
	/**
	 * Loads local location. If local location is not created yet, it is
	 * created at first call.
	 * 
	 * @param	bool				TRUE for reload it once more from DB
	 * @return	Model_Location		Location instance
	 */
	public static function load_local($refresh = FALSE)
	{
		if (Model_Location::$_local_location === NULL OR $refresh)
		{
			$location = Jelly::select('location')
				->where('is_remote', '=', FALSE)
				->load();
			
			if ( ! $location->loaded())
			{
				$location = $location->set(array(
					'label' => Cval_Location::config('label'),
					'url' => Cval_Location::config('url'),
					'is_remote' => FALSE,
					'is_paired' => TRUE
				))->save();
			}
			
			Model_Location::$_local_location = $location;
		}
		
		return Model_Location::$_local_location;
	}
	
	/**
	 * Creates an instance of Model_Location from serialized data.
	 *
	 * @param	array	serialized data
	 * @return	Model_Location
	 */
	public static function unserialize($data, $exception = TRUE)
	{
		if (empty($data) OR ! is_array($data))
		{
			throw Cval_Unserialize_Exception::create('Model_Location', 'Parameter "data" must be not empty array');
		}
		$url = Arr::get($data, 'url');
		if (empty($url))
		{
			throw Cval_Unserialize_Exception::create('Model_Location', 'Parameter "data" parameter must contain "url" value');
		}
		
		$object = Jelly::select('location')
			->where('url', '=', $url)
			->load();
		
		if ( ! $object->loaded() AND $exception)
		{
			throw Model_Not_Found_Exception::create($object, array(
				'url' => $url
			));
		}
		
		// Update data but not save
		$object->update_from_serialized($data, FALSE);
		// If object is not loaded yet
		if ( ! $object->loaded())
		{
			// Block location as default when autopairing is not allowed
			$object->local_block = ! Location_Pairing::allow_autopair();
		}
		
		return $object;
	}
	
	/**
	 * Callback called after setting url field value
	 * 
	 * @param	string	field value
	 * @return	string
	 */
	public static function _after_set_url($value)
	{
		// Do not modifiy empty values
		if (empty($value))
		{
			return $value;
		}
		
		// Make sure url do not ends with slash
		return rtrim($value, '/');
	}
	
	protected function _before_save($loaded = NULL)
	{
		parent::_before_save();
		
		// Autocreate pairing key if remote location do not have it
		if ( ! $loaded AND $this->is_remote() AND ! strlen($this->pairing_key))
		{
			$this->pairing_key = md5(Text::random('default', 10));
		}
	}
	
	/**
	 * Check if location can be deleted
	 * 
	 * @throws Kohana_Exception 
	 */
	protected function _before_delete()
	{
		parent::_before_delete();
		
		if ( ! $this->is_remote())
		{
			throw Model_Delete_Denied_Exception::create($this, 'Only remote locations can be deleted');
		}
		
		if ($this->user_profiles_shared->count())
		{
			throw Model_Delete_Denied_Exception::create($this, 'Only locations without profiles can be deleted');
		}
		if ($this->schedule_requests->count())
		{
			throw Model_Delete_Denied_Exception::create($this, 'Only locations without schedule requests can be deleted');
		}
	}

	protected function _after_load()
	{	
		// Try update values of local instance from configs
		if ($this->loaded() AND ! $this->is_remote())
		{
			$this->set(array(
				'label' => Cval_Location::config('label'),
				'url' => Cval_Location::config('url'),
			));
			
			// If fields are changed resave location
			if ($this->changed(array('label', 'url'), FALSE))
			{
				$this->save();
			}
		}
		
		parent::_after_load();
	}
	
	/**
	 * Use URL instead label if not set for location name
	 * 
	 * @return	string 
	 */
	public function name()
	{
		if ( ! strlen($this->label))
		{
			return $this->url;
		}
		return $this->label;
	}
	
	/**
	 * Html class attribute value for this object
	 * 
	 * @param	bool	TRUE return as jquery selector
	 * @return	string
	 */
	public function html_class($selector = FALSE)
	{
		return ($selector ? '.' : '').'Cval-model-location-id-'.$this->id();
	}
	
	/**
	 * Test if this location is remote
	 * 
	 * @return	bool
	 */
	public function is_remote($positive = TRUE)
	{
		$is = (bool) $this->is_remote;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Test if this location is paired
	 * 
	 * @return	bool
	 */
	public function is_paired($positive = TRUE)
	{
		$is = (bool) $this->is_paired;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Test if this location is responding
	 * 
	 * @return	bool
	 */
	public function is_responding($positive = TRUE)
	{
		$is = (bool) $this->is_responding;
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Check if location is blocked.
	 * 
	 * @param	mixed	If TRUE only local block is checked, FALSE only remote
	 *					block is checked, NULL local and remote block is checked
	 * @return	bool 
	 */
	public function is_blocked($local = NULL, $positive = TRUE)
	{
		if ($local === TRUE)
		{
			$is = (bool) $this->local_block;
		}
		elseif ($local === FALSE)
		{
			$is = (bool) $this->remote_block;
		}
		else
		{
			$is = ($this->local_block OR $this->remote_block);
		}
		
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Check if location is requested.
	 * 
	 * @param	mixed	If TRUE only local is checked, FALSE only remote
	 *					is checked, NULL local and remote is checked
	 * @return	bool 
	 */
	public function is_requested($local = NULL, $positive = TRUE)
	{
		if ($local === TRUE)
		{
			$is = (bool) $this->local_request;
		}
		elseif ($local === FALSE)
		{
			$is = (bool) $this->remote_request;
		}
		else
		{
			$is = ($this->local_request OR $this->remote_request);
		}
		
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Test if this location is used
	 * 
	 * @return	bool
	 */
	public function is_used($positive = TRUE)
	{
		$is = (($this->user_profiles_shared->count() > 0)
			OR ($this->schedule_requests->count() > 0));
		
		return $positive ? $is : ! $is;
	}
	
	/**
	 * Returns users for given conditions in this location. Pagination can be
	 * passed also. It returns collection data, which should be Jelly_Collection
	 * or array constructed from Jelly_Collection using as_array method.
	 * 
	 * @param	array	Conditions for search, for now only value from form is passed
	 * @param	array	Pagination data with items_per_page and current_page data
	 * @return	array	 
	 */
	public function find_users(array $conditions, array $pagination = array())
	{
		if ( ! $this->is_remote())
		{
			return $this->_find_users_local($conditions, $pagination);
		}
		
		return $this->_find_users_remote($conditions, $pagination);
	}
	
	protected function _find_users_local(array $conditions, array $pagination = array())
	{
		$user_profile_alias = 'up';
		$user_alias = 'u';

		$users = Jelly::select('user_profile_shared')
			->join(array('user_profile', $user_profile_alias))
			->on('idmpk', '=', $user_profile_alias.'.user_profile_shared_id')
			->join(array('user', $user_alias))
			->on($user_profile_alias.'.user_id', '=', $user_alias.'.id')
			->filter_enabled()
			->where('is_remote', '=', FALSE);

		if (strlen(Arr::get($conditions, 'value', '')))
		{
			$users
				->where_open()
				->where('label', 'LIKE', '%'.Arr::get($conditions, 'value').'%')
				->or_where($user_alias.'.email', '=', Arr::get($conditions, 'value'))
				->where_close();
		}

		$users->order_by('label');
		
		$pagination = Pagination::factory(array(
			'total_items' => $users->count(),
			'items_per_page' => Arr::get($pagination, 'items_per_page', 30),
			'current_page' => array(
				'page' => Arr::get($pagination, 'current_page', 1)
			),
			'auto_hide' => FALSE,
		));

		$users->limit($pagination->items_per_page)
			->offset($pagination->offset);

		$users = $users->execute();
		
		return array(
			'collection' => $users,
			'pagination' => $pagination->render_array()
		);
	}
	
	protected function _find_users_remote(array $conditions, array $pagination = array())
	{
		// Find contacts uri
		$uri = Route::get('contact')->uri(array(
			'controller' => 'profile_find_request'
		));
		
		$response = $this->request($uri, array(
			'conditions' => $conditions,
			'pagination' => $pagination
		));
		
		return $response->data();
	}
	
	/**
	 * Returns users for given hashes in this location. It returns collection 
	 * data, which should be Jelly_Collection or array constructed from 
	 * Jelly_Collection using as_array method.
	 * 
	 * @param	array	Hashes of users to get
	 * @return	array	 
	 */
	public function get_users(array $hashes)
	{
		if ( ! $this->is_remote())
		{
			return $this->_get_users_local($hashes);
		}
		
		return $this->_get_users_remote($hashes);
	}
	
	protected function _get_users_local(array $hashes)
	{
		$users = Jelly::select('user_profile_shared')
			->filter_enabled()
			->where('is_remote', '=', FALSE)
			->where('hash', 'IN', $hashes);

		$users = $users->execute();
		
		return array(
			'collection' => $users,
		);
	}
	
	protected function _get_users_remote(array $hashes)
	{
		// Find contacts uri
		$uri = Route::get('contact')->uri(array(
			'controller' => 'profile_get_request'
		));
		
		$response = $this->request($uri, array(
			'hashes' => $hashes,
		));
		
		return $response->data();
	}
	
	/**
	 * Returns the output of a URL. Second parameter is array 
	 * of POST data. Any [curl option](http://php.net/curl_setopt)
	 * may be used in array for third parameter.
	 *
	 * @param   mixed	 remote URI, Route object, array($route_name, $route_params)
	 * @param	array	 post data
	 * @param	bool	 TRUE for performing data checksum for paired locations
	 * @param   array    curl options
	 * @return  array	 JSON response decoded to array
	 * @throws  Location_Not_Responding_Exception
	 * @throws	Kohana_Exception
	 * @uses	Remote::post()
	 */
	public function request($uri, array $data = array(), $checksum = TRUE, array $options = array())
	{
		return Location_Request::send($this, $uri, $data, $checksum, $options);
	}
	
	/**
	 * Returns serialized data for remote calls.
	 *
	 * @return	array	serialized data
	 */
	public function serialize()
	{
		return $this->as_array_internal(array(
			'url', 'label'
		));
	}
	
	/**
	 * Update location data from serailized array
	 *
	 * @param	array	serialized data
	 * @return	$this
	 */
	public function update_from_serialized(array $data, $save = TRUE)
	{
		$this->set(Arr::extract_existed($data, array(
			'url', 'label' 
		)));
		
		if ($save)
		{
			$this->save();
		}
		
		return $this;
	}
	
	/**
	 * Updates paired status for location.
	 * 
	 * @param	bool	TRUE to set paired, FALSE to set unpaired
	 * @param	bool	TRUE for save location after update
	 * @return	$this
	 */
	public function set_paired($value = TRUE, $save = TRUE)
	{
		if ( ! $value)
		{
			$this->is_paired = FALSE;
		}
		else
		{
			$this->set(array(
				'is_paired' => TRUE,
				'remote_block' => FALSE,
				'local_block' => FALSE,
				'remote_request' => FALSE,
				'local_request' => FALSE
			));	
		}
		if ($save)
		{
			$this->save();
		}
		return $this;
	}
	
	/**
	 * Updates local blocked status for location.
	 * 
	 * @param	bool	TRUE to set blocked, FALSE to set unblocked
	 * @param	bool	TRUE for save location after update
	 * @return	$this
	 */
	public function set_blocked($value = TRUE, $save = TRUE)
	{
		$this->local_block = $value;
		if ($save)
		{
			$this->save();
		}
		return $this;
	}

}
