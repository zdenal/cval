<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Builder_Location
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Builder_Location extends Jelly_Builder
{	
	/**
	 * Searches this model from one string.
	 * 
	 * @param	string	filter value
	 * @return	$this
	 */
	public function global_search($value)
	{
		$value = trim($value);
		if (strlen($value))
		{
			$this->where_open()
					->where('label', 'LIKE', '%'.$value.'%')
					->or_where('url', 'LIKE', '%'.$value.'%')
					->where_close();
		}

		return $this;
	}
}
