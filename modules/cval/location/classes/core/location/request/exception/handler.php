<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Location request exception handler
 *
 * @package     Cval
 * @author      Zdenek Kalina
 */
abstract class Core_Location_Request_Exception_Handler extends Cval_Remote_Request_Exception_Handler 
{

	/**
	 * @return	boolean
	 */
	public function _call(Exception $e, Request $request)
	{	
		$success = parent::_call($e, $request);
		
		if ( ! $success)
		{
			$success = TRUE;
			if ($e instanceof Location_Not_Paired_Exception)
			{
				$this->_controller->response(80);
			}
			elseif ($e instanceof Location_Checksum_Exception)
			{
				$this->_controller->response(85);
			}
			else
			{
				$success = FALSE;
			}
		}
		
		return $success;
	}

}

