<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Location_Checksum_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Location_Checksum_Exception extends Location_Exception {

	/**
	 * Creates an exception used when location checksum is not valid:
	 *
	 *	   throw Location_Checksum_Exception::create($location);
	 *
	 * @param   Model_Location		Requested location
	 * @param	int			response code from location response
	 * @param   string		error message
	 * @param   array		error message variables
	 * @param   integer		the exception code
	 * @return  Model_Not_Found_Exception
	 */
	public static function create(Model_Location $location, $message = NULL, array $message_variables = NULL, $code = 0)
	{	
		if ($message === NULL)
		{
			$message = strtr('Location [:url] checksum failed', array(
				':url' => $location->url
			));
			$message_variables = NULL;
		}
		
		$exception = new Location_Checksum_Exception($message, $message_variables, $code);
		$exception->location = $location;
		
		return $exception;
	}

} // End Core_Location_Checksum_Exception
