<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Location_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Location_Exception extends Kohana_Exception {

	/**
	 * @var	Model_Location
	 */
	public $location;

	/**
	 * @return Model_Location 
	 */
	public function get_location()
	{
		return $this->location;
	}

} // End Core_Location_Exception
