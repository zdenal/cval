<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Location Blocking
 * Class for blocking/unblocking locations.
 * 
 * @package Cval
 * @author	zdennal
 */
class Core_Location_Blocking
{	
	public static function request(Model_Location $target_location, $block = TRUE)
	{
		// Load local location
		$local_location = Model_Location::load_local();
		// Check if we are not unblocking with local location for sure
		if ($local_location->id() == $target_location->id())
		{
			throw Location_Pairing_Denied_Exception::create(NULL, 'Local location can not be :action!', array(
				':action' => $block ? 'blocked' : 'unblocked'
			));
		}
		
		$is_blocked = $target_location->is_blocked(TRUE, $block);
		
		// Save status to location before remote request
		$target_location->set_blocked($block);
		
		if ($is_blocked)
		{
			// Location is not paired
			return 1;
		}
		if ( ! Location_Pairing::allow() OR $target_location->is_paired(FALSE))
		{
			// Do not connect to remote location
			// Just set location as unblocked
			return 1;
		}
		
		// Unblocking uri
		$uri = Route::get('location')->uri(array(
			'controller' => $block ? 'blocking_request' : 'unblocking_request'
		));
		
		$code = 0;
		
		try
		{
			$response = $target_location->request($uri);
			
			$code = $response->code();
		}
		catch (Location_Not_Responding_Exception $e)
		{
			// Is not responding, nothing to do
			// Just set location as unblocked
			return 1;
		}
		
		// Return response code
		return $code;
	}
	
	public static function response(Model_Location $source_location, $block = TRUE, array $data = array())
	{
		// Load local location
		$local_location = Model_Location::load_local();
		// Check if we are not unblocking with local location for sure
		if ($local_location->id() == $source_location->id())
		{
			throw Location_Pairing_Denied_Exception::create(NULL, 'Local location can not be :action!', array(
				':action' => $block ? 'blocked' : 'unblocked'
			));
		}
		
		// Update remote block status
		$source_location->remote_block = $block;
		$source_location->save();
		
		// OK
		return Location_Blocking::_response_finish(1);
	}
	
	protected static function _response_finish($code)
	{
		return $code;
	}
	
}