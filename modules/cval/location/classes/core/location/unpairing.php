<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Location Unpairing
 * Class for unpairing/blocking locations.
 * 
 * @package Cval
 * @author	zdennal
 */
class Core_Location_Unpairing
{	
	public static function request(Model_Location $target_location)
	{
		// Load local location
		$local_location = Model_Location::load_local();
		// Check if we are not unpairing with local location for sure
		if ($local_location->id() == $target_location->id())
		{
			throw Location_Pairing_Denied_Exception::create(NULL, 'Local location can not be unpaired!');
		}
		
		if ($target_location->is_paired(FALSE) OR ! Location_Pairing::allow())
		{
			// Do not connect to remote location
			// Just set location as unpaired
			$target_location->set_paired(FALSE);
			return 1;
		}
		
		$code = 0;
		
		try
		{
			// Unpairing uri
			$uri = Route::get('location')->uri(array(
				'controller' => 'unpairing_request'
			));
			$response = $target_location->request($uri);
			
			$code = $response->code();
		}
		catch (Exception $e)
		{
			// something wrong, no problem
		}
		
		// Save status to location before remote request
		$target_location->set_paired(FALSE);
		
		// Return response code
		return $code;
	}
	
	public static function response(Model_Location $source_location, array $data = array())
	{
		// Load local location
		$local_location = Model_Location::load_local();
		// Check if we are not unpairing with local location for sure
		if ($local_location->id() == $source_location->id())
		{
			throw Location_Pairing_Denied_Exception::create(NULL, 'Local location can not be unpaired!');
		}
		
		$source_location->set_paired(FALSE);
		// OK
		return Location_Unpairing::_response_finish(1);
	}
	
	protected static function _response_finish($code)
	{
		return $code;
	}
	
}