<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Location Request
 * Class for sending requests to another locations.
 * 
 * @package Cval
 * @author	zdennal
 */
class Core_Location_Request
{
	
	/**
	 * Returns the output of a URL. Third parameter is array 
	 * of POST data. Any [curl option](http://php.net/curl_setopt)
	 * may be used in array for fourth parameter.
	 *
	 * @param	Model_Location	Target location
	 * @param   mixed	 remote URI, Route object, array($route_name, $route_params)
	 * @param	array	 post data
	 * @param	bool	 TRUE for performing data checksum for paired locations
	 * @param   array    curl options
	 * @return  Location_Reaponse	 JSON converted to object
	 * @throws  Location_Not_Responding_Exception
	 * @throws  Kohana_Exception
	 * @uses	Remote::post()
	 */
	public static function send(Model_Location $location, $uri, array $data = array(), $checksum = TRUE, array $options = array())
	{
		if ($uri instanceof Route)
		{
			$uri = $uri->uri();
		}
		elseif (is_array($uri))
		{
			list($route_name, $route_params) = $uri;
			$route = Route::get($route_name);
			$uri = $route->uri($route_params);
		}
		
		// Construct absolute url for location
		$url = $location->url.'/'.$uri;
		
		$data = Location_Request::_encrypt_data($location, $data, $checksum);
		
		// Call location remotely, also if it is local location!
		$response = Remote::post($url, $data, $options, FALSE);
		
		$is_responding = TRUE;
		$error_code = 0;
		$json = NULL;
		
		// If error occured, response is array
		if (is_array($response))
		{
			$is_responding = FALSE;
			$error_code = Arr::get($response, 'code');
		}
		else
		{
			// Decode response to array
			$json = Location_Request::_decrypt_data($location, $response, $checksum);;

			// If JSON is not decoded, it is marked as not responding behavior
			if ($json === NULL)
			{
				$is_responding = FALSE;
			}
		}
		
		if ( ! $is_responding)
		{
			// Save not responding token
			$location->is_responding = FALSE;
			$location->save();
			
			throw Location_Not_Responding_Exception::create($location, $error_code);
		}
		
		// Save responding token
		$location->is_responding = TRUE;
		// Create response object
		$response = new Location_Response($location, $json);
		
		if ($location->loaded() AND $location->is_paired())
		{
			// Update location data if is paired
			$location->update_from_serialized($response->location_data());
		}
		$location->save();
		
		return $response;
	}
	
	/**
	 * Encrypts data for remote call using location pairing key if location
	 * is already paired. 
	 * 
	 * @param	Model_Location	Target location
	 * @param	array	Data to send to location
	 * @param	bool	 TRUE for performing data checksum for paired locations
	 * @return	array	Encrypted data
	 */
	protected static function _encrypt_data(Model_Location $target_location, array $data = array(), $checksum = TRUE)
	{
		// Encrypt data
		$encrypted_data = base64_encode(json_encode($data));
		
		// Create default post data array with source location
		$data = array(
			'location' => Model_Location::load_local()->serialize()
		);
		
		// Add checksum for paired locations
		if ($checksum AND $target_location->is_paired())
		{
			// Encrypt data
			$encrypted_data = Cval_Encrypt::encode($encrypted_data, $target_location->pairing_key);
			
			$data['checksum'] = md5($target_location->pairing_key.$encrypted_data);
		}
		
		//throw Debug_Exception::create($location->pairing_key, $data['checksum'], $encrypted_data);
		
		// Add data to post
		$data['data'] = $encrypted_data;
		
		return $data;
	}
	
	/**
	 * Decrypts data from remote call response using location pairing key 
	 * if location is already paired. 
	 * 
	 * @param	Model_Location	Target location
	 * @param	array	Received data
	 * @param	bool	TRUE for performing data checksum for paired locations
	 * @return	array	Encrypted data
	 */
	protected static function _decrypt_data(Model_Location $target_location, $data, $checksum = TRUE)
	{
		// Add checksum for paired locations
		if ($checksum AND $target_location->is_paired())
		{
			// Decrypt data
			$data = Cval_Encrypt::decode($data, $target_location->pairing_key);
		}
		
		return json_decode(base64_decode($data), TRUE);
	}
	
}