<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Location_Not_Responding_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Location_Not_Responding_Exception extends Location_Exception {
	
	/**
	 * @var int
	 */
	public $response_code;

	/**
	 * Creates an exception used when location is not responding:
	 *
	 *	   throw Location_Not_Responding_Exception::create($location);
	 *
	 * @param   Model_Location		Requested location
	 * @param	int			response code from location response
	 * @param   string		error message
	 * @param   array		error message variables
	 * @param   integer		the exception code
	 * @return  Model_Not_Found_Exception
	 */
	public static function create(Model_Location $location, $response_code, $message = NULL, array $message_variables = NULL, $code = 0)
	{	
		if ($message === NULL)
		{
			$message = strtr('Location [:url] is not responding [code: :code]', array(
				':url' => $location->url,
				':code' => $response_code
			));
			$message_variables = NULL;
		}
		
		$exception = new Location_Not_Responding_Exception($message, $message_variables, $code);
		$exception->location = $location;
		$exception->response_code = $response_code;
		
		return $exception;
	}

} // End Core_Location_Not_Responding_Exception
