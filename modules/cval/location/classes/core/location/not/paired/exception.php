<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Location_Not_Paired_Exception
 * 
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Location_Not_Paired_Exception extends Location_Exception {

	/**
	 * Creates an exception used when location is not paired:
	 *
	 *	   throw Location_Not_Paired_Exception::create($location);
	 *
	 * @param   Model_Location		Requested location
	 * @param	int			response code from location response
	 * @param   string		error message
	 * @param   array		error message variables
	 * @param   integer		the exception code
	 * @return  Model_Not_Found_Exception
	 */
	public static function create(Model_Location $location, $message = NULL, array $message_variables = NULL, $code = 0)
	{	
		if ($message === NULL)
		{
			$message = strtr('Location [:url] is not paired', array(
				':url' => $location->url
			));
			$message_variables = NULL;
		}
		
		$exception = new Location_Not_Paired_Exception($message, $message_variables, $code);
		$exception->location = $location;
		
		return $exception;
	}

} // End Core_Location_Not_Paired_Exception
