<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Location Pairing
 * Class for pairing/blocking locations.
 * 
 * @package Cval
 * @author	zdennal
 */
class Core_Location_Pairing
{
	
	public static function allow($exception = FALSE)
	{
		$allow = Cval_Config::get('remote_connectivity') > 10;
		
		if ( ! $allow AND $exception)
		{
			throw Location_Pairing_Denied_Exception::create();
		}
		
		return $allow;
	}
	
	public static function allow_autopair()
	{
		$allow = Cval_Config::get('remote_connectivity') >= 90;
		
		return $allow;
	}
	
	public static function request(Model_Location $target_location)
	{
		// Check if pairing allowed
		Location_Pairing::allow(TRUE);
		// Load local location
		$local_location = Model_Location::load_local();
		// Check if we are not pairing with local location for sure
		if ($local_location->id() == $target_location->id())
		{
			throw Location_Pairing_Denied_Exception::create(NULL, 'Local location can not be paired!');
		}
		
		// Pairing uri
		$uri = Route::get('location')->uri(array(
			'controller' => 'pairing_request'
		));
		
		$target_location->local_request = TRUE;
		$target_location->local_block = FALSE;
		$target_location->save();
		
		$code = 0;
		
		try
		{
			$response = $target_location->request($uri, array(
				'pairing_key' => $target_location->pairing_key
			));
			
			$code = $response->code();
			switch ($code)
			{
				case 3:
					$target_location->remote_block = TRUE;
					break;
				case 4:
					$target_location->local_block = TRUE;
					break;
				case 1:
					$target_location->set_paired(TRUE);
					break;
				default:
					// Other codes does not need saving, so return it immediately
					return $code;
			}
			
			$target_location->update_from_serialized($response->location_data(), FALSE);
			// Update location
			$target_location->save();
		}
		catch (Location_Not_Responding_Exception $e)
		{
			// Is not responding, nothing to do
			return 30;
		}
		
		// Return response code
		return $code;
	}
	
	public static function response(Model_Location $source_location, array $data = array())
	{
		// Check if pairing allowed
		if ( ! Location_Pairing::allow())
		{
			return Location_Pairing::_response_finish(3);
		}
		// Load local location
		$local_location = Model_Location::load_local();
		// Check if we are not pairing with local location for sure
		if ($local_location->id() == $source_location->id())
		{
			throw Location_Pairing_Denied_Exception::create(NULL, 'Local location can not be paired!');
		}
		
		// Save location to system if not yet
		if ( ! $source_location->loaded())
		{
			$source_location->save();
		}
		
		// We are already blocking this location
		if ($source_location->is_blocked(TRUE))
		{
			$source_location->remote_request = TRUE;
			$source_location->save();
			return Location_Pairing::_response_finish(3);
		}
		
		// Get pairing key from request data
		$pairing_key = Arr::get($data, 'pairing_key');
		
		// Hey pairing key is empty... low level exception
		if (empty($pairing_key) OR strlen($pairing_key) != 32)
		{
			// Do not allow malformated keys
			return Location_Pairing::_response_finish(6);
		}
		
		// We are already paired and pairing keys are same
		if ($source_location->pairing_key == $pairing_key
				AND $source_location->is_paired())
		{
			$source_location->set_paired(TRUE);
			return Location_Pairing::_response_finish(1);
		}
		
		// Now we know that we can start autopairing, so check if pairing key
		// is valid at source location
		
		// Pairing key check uri
		$uri = Route::get('location')->uri(array(
			'controller' => 'pairing_check_request'
		));
		
		try
		{
			$response = $source_location->request($uri, array(
				'pairing_key' => $pairing_key
			));
			
			$code = $response->code();
			// Pairing key does not match
			switch ($code)
			{
				case 5:
					return Location_Pairing::_response_finish(5);
				case 3:
					// This location is blocked by source location
					// very strange but it can happen between remote requests
					// is some time for changing blocking state
					$source_location->remote_block = TRUE;
					$source_location->save();
					return Location_Pairing::_response_finish(4);
				case 1:
					// Do nothing it is success
					break;
				default:
					// Any other unknown code return it imediatelly
					return Location_Pairing::_response_finish($code);
			}
		}
		catch (Location_Not_Responding_Exception $e)
		{
			// Source location is not responding
			return Location_Pairing::_response_finish(20);
		}
		
		$source_location->pairing_key = $pairing_key;
		$source_location->set_paired(TRUE);
		
		// Finally paired
		return Location_Pairing::_response_finish(1);
	}
	
	protected static function _response_finish($code)
	{
		Kohana::$log->add(Kohana::INFO, 'Location pairing done with code '.$code);
		return $code;
	}
	
}