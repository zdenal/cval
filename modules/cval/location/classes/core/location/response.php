<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Location Response
 * Class which holds request data from location.
 * 
 * @package Cval
 * @author	zdennal
 */
class Core_Location_Response
{
	/**
	 * @var	array	Complete response from request
	 */
	protected $_response;
	
	/**
	 * @var array
	 */
	protected $_data;
	
	/**
	 * @var int
	 */
	protected $_code;
	
	/**
	 * @var	string
	 */
	protected $_message;

	/**
	 * @var	boolean
	 */
	protected $_error;
	
	/**
	 * @var	Model_Location
	 */
	protected $_location;
	
	/**
	 * @var	array
	 */
	protected $_location_data;
	
	/**
	 * @param array	Array with response from location request
	 */
	public function __construct(Model_Location $location, array $response)
	{
		$this->_response = $response;
	}
	
	/**
	 * Gets a value from an response data array using a dot separated path.
	 * If path is NULL complete data array is returned.
	 * 
	 * @param   string  key path, delimiter separated
	 * @param   mixed   default value if the path is not set
	 * @param   string  key path delimiter
	 * @return  mixed
	 * @uses	Arr::path
	 * @throws	Cval_Unserialize_Exception	if response data param is set and is not array
	 */
	public function data($path = NULL, $default = NULL, $delimiter = NULL)
	{
		if ($this->_data === NULL)
		{
			$this->_data = Arr::get($this->_response, 'data', array());
			
			if ( ! is_array($this->_data))
			{
				throw Cval_Unserialize_Exception::create($this, 'Parameter "data" must be array');
			}
		}
		
		if ($path === NULL)
		{
			return $this->_data;
		}
		
		return Arr::path($this->_data, $path, $default, $delimiter);
	}
	
	/**
	 * Returns response code
	 * 
	 * @return	int
	 */
	public function code()
	{
		if ($this->_code === NULL)
		{
			$this->_code = (int) Arr::get($this->_response, 'code');
		}
		return $this->_code;
	}
	
	/**
	 * Returns response code
	 * 
	 * @return	int
	 */
	public function message()
	{
		if ($this->_message === NULL)
		{
			$this->_message = (string) Arr::get($this->_response, 'message');
		}
		return $this->_message;
	}
	
	/**
	 * Returns TRUE if response was error
	 * 
	 * @return	boolean 
	 */
	public function error()
	{
		if ($this->_error === NULL)
		{
			$this->_error = (bool) Arr::get($this->_response, 'error');
		}
		return $this->_error;
	}
	
	/**
	 * Gets a value from an location data array using a dot separated path.
	 * If path is NULL complete data array is returned.
	 * 
	 * @param   string  key path, delimiter separated
	 * @param   mixed   default value if the path is not set
	 * @param   string  key path delimiter
	 * @return  mixed
	 * @uses	Arr::path
	 * @throws	Cval_Unserialize_Exception	if response data param is set and is not array
	 */
	public function location_data($path = NULL, $default = NULL, $delimiter = NULL)
	{
		if ($this->_location_data === NULL)
		{
			$this->_location_data = Arr::get($this->_response, 'location', array());
			
			if ( ! is_array($this->_location_data))
			{
				throw Cval_Unserialize_Exception::create($this, 'Parameter "location" must be array');
			}
		}
		
		if ($path === NULL)
		{
			return $this->_location_data;
		}
		
		return Arr::path($this->_location_data, $path, $default, $delimiter);
	}
	
}