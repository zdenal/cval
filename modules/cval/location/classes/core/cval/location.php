<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Cval_Location
 * @package Cval
 * @author	Zdennal
 */
abstract class Core_Cval_Location
{
	
	/**
	 * Gets cval location config.
	 * If key is NULL complete config array is returned.
	 * 
	 * @param	string	config key
	 * @param	mixed	default value to use if key is not set
	 * @return	mixed	config value 
	 */
	public static function config($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			return (array) Kohana::config('cval/location');
		}
		
		return Arr::path(Kohana::config('cval/location'), $key, $default);
	}
	
	/**
	 * Configuration passed to client on first Cval call.
	 * 
	 * @return	array 
	 */
	public static function config_js()
	{
		$config = (array) Cval_Location::config();
		
		$config['url'] = Cval::instance()->location()->url;
		$config['label'] = Cval::instance()->location()->label;
		
		return $config;
	}

}