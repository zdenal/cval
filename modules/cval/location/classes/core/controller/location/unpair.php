<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Unpair
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Unpair extends Controller_Location_Instance {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$unpairing_message_parts = array();
		$unpairing_message = NULL;
		$unpairing_ok = FALSE;
		try 
		{
			$response_code = Location_Unpairing::request($this->_object);
			// All response code are success, despite remote errors
			$unpairing_ok = TRUE;
		}
		catch (Exception $e)
		{
			$unpairing_message = Cval::i18n('Unknown error.');
			$unpairing_ok = FALSE;
		}

		if ( ! $unpairing_ok)
		{
			$unpairing_message_parts[] = Cval::i18n('Unpairing FAILED!');
			if ($unpairing_message)
			{
				$unpairing_message_parts[] = $unpairing_message;
			}
			$message = Cval_Message::create(Cval_Message::ERROR, implode(' ', $unpairing_message_parts));
		}
		else
		{
			$message = Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Unpairing SUCCESS!'));
		}
		
		$this->_object = $this->_object->reload();
		
		$this->request->response = array(
			'message' => $message,
			'unpaired' => $unpairing_ok,
			'location' => Cval_REST::instance()->model($this->_object)->render()
		);
	}

}

