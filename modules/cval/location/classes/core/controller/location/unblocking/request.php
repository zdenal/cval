<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Unblocking request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Location_Unblocking_Request extends Controller_Location_Request
{

	public function action_index()
	{
		$code = Location_Blocking::response($this->_location, FALSE, $this->data());
		$this->response($code);
	}

} // End Core_Controller_Location_Unblocking_Request
