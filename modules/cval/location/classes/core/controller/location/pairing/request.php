<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Pairing request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Location_Pairing_Request extends Controller_Location_Request
{
	
	/**
	 * @var bool	If FALSE request allows calls only from paired locations 
	 */
	protected $_allow_unpaired = TRUE;
	
	public function before()
	{	
		parent::before();
		
		$this->_error_codes(array(
			3	=> 'Pairing is not allowed or source location is locally blocked', // TODO - different code for locall block
			4	=> 'Source location blocks this location',
			5	=> 'Pairing key does not match',
			6	=> 'Pairing key is mallformed',
			20	=> 'Source location is not responding'
		));
	}

	public function action_index()
	{
		$code = Location_Pairing::response($this->_location, $this->data());
		$this->response($code);
	}

} // End Core_Controller_Location_Pairing_Request
