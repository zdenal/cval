<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Pairing check request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Location_Pairing_Check_Request extends Controller_Location_Request
{
	
	/**
	 * @var bool	If FALSE request allows calls only from paired locations 
	 */
	protected $_allow_unpaired = TRUE;
	
	public function before()
	{	
		parent::before();
		
		$this->_error_codes(array(
			3	=> 'Pairing is not allowed or source location is locally blocked', // TODO - different code for locall block
			5	=> 'Pairing key does not match',
			6	=> 'Pairing key is mallformed',
		));
	}
	
	public function action_index()
	{
		$data = $this->data();
		
		// Check if pairing allowed
		if ( ! Location_Pairing::allow())
		{
			return $this->response(3);
		}
		
		if ($this->_location->is_blocked(TRUE))
		{
			return $this->response(3);
		}
		
		if ( ! $this->_location->loaded())
		{
			return $this->response(5);
		}
		
		// Get pairing key from request data
		$pairing_key = Arr::get($data, 'pairing_key');
		
		// Hey pairing key is empty... low level exception
		if (empty($pairing_key) OR strlen($pairing_key) != 32)
		{
			// Do not allow malformated keys
			return $this->response(6);
		}
		
		if ($this->_location->pairing_key != $pairing_key)
		{
			return $this->response(5);
		}
		
		Kohana::$log->add(Kohana::INFO, 'Location pairing check success with location '.$this->_location->url);
		// Finally paired
		$this->response();
	}

} // End Core_Controller_Location_Pairing_Check_Request
