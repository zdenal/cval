<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Unblock
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Unblock extends Controller_Location_Instance {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$unblocking_message_parts = array();
		$unblocking_message = NULL;
		$unblocking_ok = FALSE;
		try 
		{
			$response_code = Location_Blocking::request($this->_object, FALSE);
			// All response code are success, despite remote errors
			$unblocking_ok = TRUE;
		}
		catch (Exception $e)
		{
			$unblocking_message = Cval::i18n('Unknown error.');
			$unblocking_ok = FALSE;
		}

		if ( ! $unblocking_ok)
		{
			$unblocking_message_parts[] = Cval::i18n('Unblocking FAILED!');
			if ($unblocking_message)
			{
				$unblocking_message_parts[] = $unblocking_message;
			}
			$message = Cval_Message::create(Cval_Message::ERROR, implode(' ', $unblocking_message_parts));
		}
		else
		{
			$message = Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Unblocking SUCCESS!'));
		}
		
		$this->_object = $this->_object->reload();
		
		$this->request->response = array(
			'message' => $message,
			'unblocked' => $unblocking_ok,
			'location' => Cval_REST::instance()->model($this->_object)->render()
		);
	}

}

