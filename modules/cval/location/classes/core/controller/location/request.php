<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for Remote Location Requests.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Location_Request extends Controller_Cval_Remote_Request
{
	
	/**
	 * @var	array	Received and decrypted data
	 */
	protected $_data;
	
	/**
	 * @var Model_Location	Request origin location
	 */
	protected $_location;
	
	/**
	 * @var bool	If FALSE request allows calls only from paired locations 
	 */
	protected $_allow_unpaired = FALSE;

	/**
	 * Defines cval instance. In development are get data changed to post.
	 * Sets default response to json.
	 */
	public function before()
	{
		parent::before();
		
		// As default response is not converted to JSON
		// But is encrypted
		$this->request->format = 'direct';
		
		// Specific error codes for location
		$this->_error_codes(array(
			80 => 'Location is not paired',
			85 => 'Request data checksum is not valid',
		));
		
		// Decrypt data
		$this->_decrypt_data();
	}
	
	/**
	 * Create complete response including system part and data part.
	 * And is automatically encrypted
	 * 
	 * @return	string
	 */
	public function complete_response()
	{
		return $this->_encrypt_data(parent::complete_response());
	}
	
	/**
	 * Returns decrypted request data
	 * 
	 * @return	array
	 */
	public function data()
	{
		return $this->_data;
	}
	
	/**
	 * Gets system response.
	 * Adds serialized local location. 
	 * 
	 * @return	array
	 */
	protected function _system_response()
	{
		$data = parent::_system_response();
		
		return array(
			'location' => Model_Location::load_local()->serialize()
		) + $data;
	}
	
	/**
	 * Gets exception handler callback. Can be overriden.
	 * 
	 * @return	array
	 */
	protected function _exception_handler()
	{
		$handler = new Location_Request_Exception_Handler($this);
		return $handler->callback();
	}
	
	/**
	 * Decrypts data from POST 
	 */
	protected function _decrypt_data()
	{	
		$post = Security::xss_clean($_POST);
		
		$this->_location = Model_Location::unserialize(Arr::get($post, 'location'), FALSE);
		if ( ! $this->_allow_unpaired AND ! $this->_location->is_paired())
		{
			throw Location_Not_Paired_Exception::create($this->_location);
		}
		
		// Get data from POST
		$data = Arr::get($post, 'data');
		
		// Perform checksum test for paired locations
		if ( ! $this->_allow_unpaired)
		{	
			if (empty($data) OR (md5($this->_location->pairing_key.$data) != Arr::get($post, 'checksum')))
			{
				throw Location_Checksum_Exception::create($this->_location);
			}
			// Decrypt data
			$data = Cval_Encrypt::decode($data, $this->_location->pairing_key);
			
			if ($this->_location->loaded() AND $this->_location->is_paired())
			{
				// Update unserialized location, data are already updated
				$this->_location->save();
			}
		}
		
		// Decrypt data
		$this->_data = Security::xss_clean(json_decode(base64_decode($data), TRUE));
	}
	
	/**
	 * Encrypts data for response 
	 */
	protected function _encrypt_data($data)
	{	
		// Encrypt data
		$encrypted_data = base64_encode(json_encode($data));
		
		// Add checksum for paired locations
		if ( ! $this->_allow_unpaired)
		{
			// Encrypt data
			$encrypted_data = Cval_Encrypt::encode($encrypted_data, $this->_location->pairing_key);
		}
		
		return $encrypted_data;
	}

} // End Core_Controller_Location_Request
