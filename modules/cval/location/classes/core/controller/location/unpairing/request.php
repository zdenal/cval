<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Unpairing request controller.
 *
 * @package    Cval
 * @category   Controller
 */
abstract class Core_Controller_Location_Unpairing_Request extends Controller_Location_Request
{

	public function action_index()
	{
		$code = Location_Unpairing::response($this->_location, $this->data());
		$this->response($code);
	}

} // End Core_Controller_Location_Unpairing_Request
