<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Pair
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Pair extends Controller_Location_Instance {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		// Check if pairing enabled
		Location_Pairing::allow(TRUE);
		
		$pairing_message_parts = array();
		$pairing_message = NULL;
		$pairing_ok = FALSE;
		try 
		{
			$response_code = Location_Pairing::request($this->_object);

			switch ($response_code)
			{
				case 3:
					$pairing_message = Cval::i18n('This location is blocked by remote location.');
					break;
				case 4:
					$pairing_message = Cval::i18n('This location blocks remote location.');
					break;
				case 5:
					$pairing_message = Cval::i18n('Pairing key is empty.');
					break;
				case 6:
					$pairing_message = Cval::i18n('Pairing key is malformated.');
					break;
				case 20:
					$pairing_message = Cval::i18n('Remote location is not able to reach this location.');
					break;
				case 30:
					$pairing_message = Cval::i18n('This location is not able to reach remote location.');
					break;
				case 1:
					$pairing_ok = TRUE;
					break;
				default:
					$pairing_message = Cval::i18n('Unknown error.');
			}
		}
		catch (Exception $e)
		{
			$pairing_message = Cval::i18n('Unknown error.');
			$pairing_ok = FALSE;
		}

		if ( ! $pairing_ok)
		{
			$pairing_message_parts[] = Cval::i18n('Pairing FAILED!');
			if ($pairing_message)
			{
				$pairing_message_parts[] = $pairing_message;
			}
			$message = Cval_Message::create(Cval_Message::ERROR, implode(' ', $pairing_message_parts));
		}
		else
		{
			$message = Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Pairing SUCCESS!'));
		}
		
		$this->_object = $this->_object->reload();
		
		$this->request->response = array(
			'message' => $message,
			'paired' => $pairing_ok,
			'location' => Cval_REST::instance()->model($this->_object)->render()
		);
	}

}

