<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Edit
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Edit extends Controller_Location_Instance {
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/location/edit')
				->bind('location', $location);
		
		$location = $this->_object;
		
		$this->request->response = array(
			'html' => $html->render(),
			'location' => Cval_REST::instance()->model($location)->render(),
			'allow_pair' => Location_Pairing::allow(),
			'pairing_disabled_text' => Cval::i18n('This option is enabled only for other than Closed connectivity.')
		);
	}

}

