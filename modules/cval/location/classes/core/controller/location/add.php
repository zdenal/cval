<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Add extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'location';
	
	public function action_index() 
	{	
		$html = View::factory('cval/content/location/add')
				->set('csrf', Security::token(TRUE, 'cval/location/add'))
				->bind('location', $location)
				->set('allow_pairing', Location_Pairing::allow());
		
		$location = Jelly::factory('location');
		
		$this->request->response = array(
			'html' => $html->render(),
			'location' => Cval_REST::instance()->model($location)->render()
		);
	}
	
	public function action_submit()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		Security::check(Arr::get($_POST, 'csrf'), 'cval/location/add', TRUE);
		
		$post_fields = Security::xss_clean(Arr::extract($_POST, array(
			'url', 'autopair'
		)));
		
		$autopair = Arr::get($post_fields, 'autopair');
		if ($autopair)
		{
			// Check if pairing enabled
			Location_Pairing::allow(TRUE);
		}
		
		Database::instance()->transaction_start();
		
		$location = Jelly::factory('location')
				->set($post_fields)
				->set(array(
					'is_remote' => TRUE
				))->save();
		
		Database::instance()->transaction_commit();
		
		$messages = array(
			Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Location was created!')),
		);
		
		if ($autopair)
		{
			$_POST['id'] = $location->id();
			$pair_request = Request::factory(Route::get('location')->uri(array(
				'controller' => 'pair',
			)))->execute();

			if ( ! $pair_request->response['paired'])
			{
				$messages[] = $pair_request->response['message'];
			}
		}
		
		$this->request->response = array(
			'messages' => $messages,
			'location' => Cval_REST::instance()->model($location)->render()
		);
	}

}

