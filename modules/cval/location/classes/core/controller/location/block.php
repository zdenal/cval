<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Block
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Block extends Controller_Location_Instance {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	public function action_index()
	{
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$blocking_message_parts = array();
		$blocking_message = NULL;
		$blocking_ok = FALSE;
		try 
		{
			$response_code = Location_Blocking::request($this->_object, TRUE);
			// All response code are success, despite remote errors
			$blocking_ok = TRUE;
		}
		catch (Exception $e)
		{
			$blocking_message = Cval::i18n('Unknown error.');
			$blocking_ok = FALSE;
		}

		if ( ! $blocking_ok)
		{
			$blocking_message_parts[] = Cval::i18n('Blocking FAILED!');
			if ($blocking_message)
			{
				$blocking_message_parts[] = $blocking_message;
			}
			$message = Cval_Message::create(Cval_Message::ERROR, implode(' ', $blocking_message_parts));
		}
		else
		{
			$message = Cval_Message::create(Cval_Message::SUCCESS, Cval::i18n('Blocking SUCCESS!'));
		}
		
		$this->_object = $this->_object->reload();
		
		$this->request->response = array(
			'message' => $message,
			'blocked' => $blocking_ok,
			'location' => Cval_REST::instance()->model($this->_object)->render()
		);
	}

}

