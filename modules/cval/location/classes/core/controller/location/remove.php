<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Core_Controller_Location_Remove
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Location_Remove extends Controller_Cval_Ajax {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;
	
	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'location';
	
	public function action_index()
	{	
		$this->request->exception_handler = Cval_Exception_Handler::get('save');
		
		$id = Security::xss_clean(Arr::get($_POST, 'id', array()));
		
		if (empty($id))
		{
			throw new Kohana_Exception('Nothing to delete, `id` parameter is empty');
		}
		
		$ids = $id;
		$multi_delete = FALSE;
		
		if ( ! is_array($ids))
		{
			$ids = array($ids);
		}
		else
		{
			$multi_delete = TRUE;
		}
		
		$not_found = array();
		$denied = array();
		
		foreach ($ids as $id)
		{
			$location = Jelly::select('location')
					->load($id);
			
			if ( ! $location->loaded())
			{
				if ( ! $multi_delete)
				{
					throw Model_Not_Found_Exception::create($location, $id);
				}
				$not_found[] = $id;
				continue;
			}
			
			try
			{
				// First try to delete it
				try
				{
					Location_Blocking::request($location, TRUE);
				}
				catch (Exception $e)
				{
					// Do nothing
				}
				// Or simple delete location object
				$location->delete();
				
				if ( ! $multi_delete)
				{
					$message = Cval::i18n('Location was removed');
				}
			}
			catch (Model_Delete_Denied_Exception $e)
			{
				Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
				if ( ! $multi_delete)
				{
					$message = Cval::i18n('Location was blocked instead of removing. It has profiles or schedule requests already created and can\'t be removed.');
				}
				else
				{
					$denied[] = $id;
				}
			}
		}
		
		if ($multi_delete)
		{
			$total_cnt = count($ids);
			$not_found_cnt = count($not_found);
			$denied_cnt = count($denied);
			$deleted_cnt = $total_cnt - $not_found_cnt - $denied_cnt;
			
			$message_parts = array();
			if ($deleted_cnt)
			{
				$message_parts[] = Cval::i18n('Locations (:cnt) were removed.', array(
					':cnt' => $deleted_cnt
				));
			}
			if ($denied_cnt)
			{
				$message_parts[] = Cval::i18n('Locations (:cnt) were blocked instead of removing. They have profiles or schedule requests already created and can\'t be removed.', array(
					':cnt' => $denied_cnt
				));
			}
			if ($not_found_cnt)
			{
				$message_parts[] = Cval::i18n('Locations (:cnt) weren\'t found.', array(
					':cnt' => $not_found_cnt
				));
			}
			
			$message = implode(' ', $message_parts);
		}
		
		$this->request->response = array(
			'message' => Cval_Message::create(Cval_Message::SUCCESS, $message)
		);
	}

}

