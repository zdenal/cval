<?php

return array(
	
	/**
	 * Cval instance label.
	 * It is visible for remote locations. 
	 */
	'label' => 'Cval',
	
	/**
	 * Cval instance URL.
	 * It is URL to which the application is accessible from web 
	 * or local network. It identifies this application. It's modification 
	 * is propagated to all paired remote locations.
	 * If protocol is not present, http:// is automatically used.
	 * If it ends with slash, it is automatically trimmed.
	 * Example:
	 * 
	 *	'cval.localhost' => 'http://cval.localhost'
	 *  'https://cval.localhost/' => 'https://cval.localhost'
	 */
	'url' => 'cval.localhost'

);