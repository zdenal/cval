<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Helper for REST model loading
 *
 * @package     Cval
 * @author      zdennal
 */
abstract class Core_Cval_REST_Model {

	/**
	 * @return	array
	 */
	public static function get($model, $id = NULL)
	{
		if ($model instanceof Jelly_Model)
		{
			$params = array(
				'model' => $model->meta()->model(),
				'id' => $model->id()
			);
		}
		else
		{
			$params = array('model' => $model);

			if ($id !== NULL)
			{
				$params['id'] = $id;
			}
		}

		return Request::factory(Route::get('cval/rest/model')->uri($params))
				->execute()
				->response;
	}

}

