<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
abstract class Core_Controller_Cval_REST_Model extends Controller_Jelly_REST_Model {

	/**
	 * @var boolean   Whether needs all controller ACL checking
	 */
	protected $_acl_is_secure = TRUE;

	/**
	 * @var array   Actions requiring ACL checking
	 */
	protected $_acl_required = TRUE;

	/**
	 * @var mixed   ACL resource
	 */
	protected $_acl_resource = 'model';

	/**
	 * @var array   Actions requiring a loaded ACL resource
	 */
	protected $_acl_resource_required = TRUE;
	
}	// End of Core_Controller_Cval_REST_Model

