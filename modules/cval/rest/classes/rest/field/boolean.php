<?php defined('SYSPATH') OR die('No direct script access.');

class REST_Field_Boolean extends Jelly_REST_Field_Boolean 
{
	
	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		return array_merge(parent::_render_specific($model, $value), array(
			'value' => (bool) $value,
			'display_value' => $value ? $this->_jelly_field->label_true : $this->_jelly_field->label_false
		));
	}
	
}

