<?php defined('SYSPATH') OR die('No direct script access.');

class REST_Field_Timestamp extends Jelly_REST_Field_Timestamp 
{
	protected function _render_specific(Jelly_Model $model, $value = NULL)
	{
		$display_value = $this->_jelly_field->date ? Cval_Date::format($value, $this->_jelly_field->time) : Cval_Date::format_time($value);
		$string_value = '';
		if ( ! empty($display_value))
		{
			$client_value = Cval_Date::to_timezone($value);
			$string_value = $this->_jelly_field->date ? Date::format($client_value) : Date::format($client_value, 'H:i:s');
		}
		
		return array_merge(parent::_render_specific($model, $value), array(
			'value' => $value,
			'display_value' => empty($display_value) ? '' : $display_value,
			'string_value' => $string_value
		));
	}
}

