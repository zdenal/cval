<?php defined('SYSPATH') OR die('No direct script access.');

class Cval_REST extends Jelly_REST 
{
	/**
	 * Singleton control for REST instances
	 * 
	 * @param	string	Specific name for REST instance
	 * @return	Jelly_REST_Instance
	 */
	public static function instance($name = 'cval')
	{
		return parent::instance($name);
	}
}

