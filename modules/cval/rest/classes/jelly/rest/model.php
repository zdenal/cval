<?php defined('SYSPATH') OR die('No direct script access.');

class Jelly_REST_Model extends Jelly_REST_Model_Core 
{
	public function render(array $fields = array())
	{
		// If no fields passed used all fields
		if (empty($fields))
		{
			$fields = $this->_jelly_model->meta()->fields();
		}
		
		return parent::render($fields);
	}

	public function render_meta(array $fields = array())
	{
		// If no fields passed used all fields
		if (empty($fields))
		{
			$fields = $this->_jelly_model->meta()->fields();
		}
		
		return parent::render_meta($fields);
	}

	public function render_with_meta(array $fields = array())
	{
		$parent_data = parent::render_with_meta($fields);
		
		$data = $parent_data['object'];
		$data['meta'] = $parent_data['meta'];
		
		return $data;
	}
}

