<?php

defined('SYSPATH') or die('No direct script access.');	

// Rest route
Route::set('cval/rest/model', 'cval/rest/model/<model>(/<action>(/<id>))', array('id' => '[0-9a-zA-Z\._]+'))
	->defaults(array(
		'directory'  => 'cval/rest',
		'controller' => 'model',
		'action'     => 'index',
	));
