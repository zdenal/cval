(function(Cval, $) {
	
	$.Class.extend('Cval.dialog.control.calendar_icalendar_edit', {
		init : function(callback) {
			Cval.core.callback(function(){
				Cval.dialog.loadControl('calendar_def_edit', function(){
					
					
					Cval.dialog.control.calendar_def_edit.extend('Cval.dialog.control.calendar_icalendar_edit', {
						defaults : {
							calendarType : 'icalendar'
						}
					}, {});
					
					
					Cval.dialog.control.calendar_icalendar_edit.init(callback);
				})
			})
		}
	}, {})

})(Cval, jQuery);