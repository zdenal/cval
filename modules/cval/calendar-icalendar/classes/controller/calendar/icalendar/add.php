<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Controller_Calendar_iCalendar_Add
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
class Controller_Calendar_iCalendar_Add extends Controller_Calendar_Default_Add {
	
	/**
	 * @var	string	Calendar type
	 */
	protected $_calendar_type = 'icalendar';
	
	protected function _finish_post_fields()
	{
		return array('url');
	}

}

