<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Controller_Calendar_iCalendar_Edit
 *
 * @package     Cval
 * @category    Controller
 * @author      zdennal
 */
class Controller_Calendar_iCalendar_Edit extends Controller_Calendar_Default_Edit {
	
	/**
	 * @var	string	Calendar type
	 */
	protected $_calendar_type = 'icalendar';
	
	protected function _save_post_fields()
	{
		return parent::_save_post_fields() + array('url');
	}

}

