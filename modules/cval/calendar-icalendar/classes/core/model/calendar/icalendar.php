<?php defined('SYSPATH') or die ('No direct script access.');

/**
 * Core_Model_Calendar_iCalendar
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Model_Calendar_iCalendar extends Model_Calendar_Default
{

	public static function initialize(Jelly_Meta $meta)
    {	
		parent::initialize($meta);
		
		$meta->fields(array(
			'url' => new Field_URL(array(
				'label' => 'URL',
				'rules' => array(
					'not_empty' => NULL,
					'max_length' => array(1000)
				)
			))
		));
    }
	
	protected function _create_label(array $refresh_data)
	{
		return Arr::get($refresh_data, 'label');
	}
	
	/**
	 * Internal method for calendar model. Is called from refresh method with
	 * same parameters and also returns same data.
	 * 
	 * @param	int		Start timestamp
	 * @param	int		End timestamp
	 * @return	array	Array of timestamps
	 */
	public function _refresh($start, $end)
	{
		return Calendar_iCalendar_Manager::refresh($this->url, $start, $end);
	}
	
	/**
	 * Called after _refresh call.
	 * Return only timestamps value.
	 * 
	 * @param	array	Return data from _refresh method
	 * @return	array 
	 */
	protected function _after_refresh(array $refresh_data)
	{
		return $refresh_data['timestamps'];
	}
	
} // End Core_Model_Calendar_iCalendar