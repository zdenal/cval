<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Calendar_iCalendar_Manager
 * @package Cval
 * @author	zdennal
 */
abstract class Core_Calendar_iCalendar_Manager
{

	/**
	 * Method called to refresh data for calendar specified in URL. 
	 * It needs to return array of timestamps representing events between 
	 * $start end $end parameters.
	 * 
	 * @param	string	iCalendar URL
	 * @param	int		Start timestamp
	 * @param	int		End timestamp
	 * @return	array	Array of timestamps
	 */
	public static function refresh($url, $start, $end)
	{	
		// Perform first simple check using headers only, if url is live
		if ( ! Calendar_iCalendar_Manager::url_exists($url))
		{
			throw new Kohana_Exception('iCalendar URL is not valid or does not respond');
		}
		
		// Load iCalcreator class
		iCalcreator_Loader::init();
		// create a new calendar instance
		$v = new vcalendar(array(
			'nl' => "\n",
			'unique_id' => 'domain.com',
			'url' => $url
		));
		
		try
		{
			if ( ! $v->parse())
			{
				throw new Kohana_Exception('iCalendar URL is not valid iCal file or does not contain any components');
			}
		}
		catch (Exception $e)
		{
			throw new Kohana_Exception('iCalendar URL is not valid or does not respond');
		}

		// ensure start date order
		$v->sort();
		
		$label = $v->getProperty('X-WR-CALNAME');

		$start_arr = explode('-', Date::format($start, 'Y-m-d'));
		$end_arr = explode('-', Date::format($end, 'Y-m-d'));

		$years = $v->selectComponents(
			$start_arr[0], $start_arr[1], $start_arr[2], $end_arr[0], $end_arr[1], $end_arr[2], array('vevent', 'vfreebusy'), FALSE, TRUE, FALSE
		);
		// False is returned when no item in calendar found
		if ($years === FALSE)
		{
			$years = array();
		}

		$date_time_zone = new DateTimeZone(Date::timezone());
		$date_time = new DateTime("now", $date_time_zone);
		// Holder for timestamps
		$timestamps = array();

		foreach ($years as $year)
		{
			foreach ($year as $month)
			{
				foreach ($month as $day)
				{
					foreach ($day as $event)
					{
						$currstart = $event->getProperty('x-current-dtstart', 1, TRUE);
						$e_start_tmp = $currstart ? $currstart[1] : $event->getProperty('dtstart', 1, TRUE);
						if ( ! is_array($e_start_tmp['value']))
						{
							$e_start_tmp['value'] = iCalUtilityFunctions::_timestamp2date(strtotime($e_start_tmp['value']));
						}
						$e_start = iCalUtilityFunctions::_date2timestamp($e_start_tmp['value'], ' ');
						if (isset($e_start_tmp['params']['TZID']))
						{
							$date_time_zoneTmp = new DateTimeZone($e_start_tmp['params']['TZID']);
							$date_timeTmp = new DateTime("now", $date_time_zoneTmp);
							$e_start -= $date_time_zoneTmp->getOffset($date_time);
						}
						$currend = $event->getProperty('x-current-dtend', 1, TRUE);
						$e_end_tmp = $currend ? $currend[1] : $event->getProperty('dtend', 1, TRUE);
						if ( ! is_array($e_end_tmp['value']))
						{
							$e_end_tmp['value'] = iCalUtilityFunctions::_timestamp2date(strtotime($e_end_tmp['value']));
						}
						$e_end = iCalUtilityFunctions::_date2timestamp($e_end_tmp['value'], ' ');
						if (isset($e_end_tmp['params']['TZID']))
						{
							$date_time_zoneTmp = new DateTimeZone($e_end_tmp['params']['TZID']);
							$date_timeTmp = new DateTime("now", $date_time_zoneTmp);
							$e_end -= $date_time_zoneTmp->getOffset($date_time);
						}
						$title = $event->getProperty('summary', 1, TRUE);
						$title = $title ? Arr::get($title, 'value') : NULL;
						$timestamps[] = array(
							'start' => $e_start,
							'end' => $e_end,
							'title' => $title
						);
					}
				}
			}
		}

		return array(
			'label' => empty($label) ? NULL : $label[1],
			'timestamps' => $timestamps
		);
	}
	
	/**
	 * Check if url is live
	 * 
	 * @param	string	$url
	 * @return	boolean
	 */
	public static function url_exists($url) {
	    $hdrs = @get_headers($url);
	    return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : FALSE;
	}

}