<h3>Insert calendar URL</h3>

<div class="field-label grid_16">
	<?php echo UTF8::ucfirst(Cval::i18n($calendar->meta()->fields('url')->label)) ?>
	<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
</div>
<div class="clear"></div>
<div class="field grid_16">
	<?php echo $calendar->input('url') ?>
</div>
<div class="clear"></div>

