<div class="field-label grid_11">
	<?php echo UTF8::ucfirst(Cval::i18n($calendar->calendar->meta()->fields('label')->label)) ?>
	<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
</div>
<div class="field-label grid_5">
	<?php echo Cval::i18n('Color') ?>
</div>
<div class="clear"></div>
<div class="field grid_11">
	<?php echo $calendar->calendar->input('label') ?>
</div>
<div class="field grid_5">
	<?php echo $calendar->calendar->input('color') ?>
</div>

<div class="field-label grid_16">
	<?php echo UTF8::ucfirst(Cval::i18n($calendar->meta()->fields('url')->label)) ?>
	<span class="field-description">(<?php echo Cval::i18n('required') ?>)</span>
</div>
<div class="clear"></div>
<div class="field grid_16">
	<?php echo $calendar->input('url') ?>
</div>
<div class="clear"></div>

