<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Loader for iCalcreator main class.
 * @package iCalcreator
 * @author	zdennal
 */
abstract class Core_iCalcreator_Loader {

	/**
	 * @var	bool	Check if icalendar was already initiated
	 */
	protected static $_initiated = FALSE;
	
	public static function init()
	{
		if ( ! iCalcreator_Loader::$_initiated)
		{
			$path = Kohana::find_file('vendor', 'iCalcreator/iCalcreator.class');
			if ($path)
			{
				require_once $path;
			}
			else
			{
				throw new Kohana_Exception('iCalcreator file not found at `vendor/iCalcreator/iCalcreator.class.php`');
			}
			// Renew previous timezone, because iCalcreator sets
			// Europe/Stockholm if PHP >= 5.1
			Date::timezone(Date::timezone(), TRUE);
			// Increase memory limit
			ini_set("memory_limit","80M");
			
			iCalcreator_Loader::$_initiated = TRUE;
		}
	}
	
}

