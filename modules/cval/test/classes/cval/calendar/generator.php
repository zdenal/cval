<?php defined('SYSPATH') or die('No direct script access.');

class Cval_Calendar_Generator {

	public static $date_format = 'd/m/Y';

	public static $date_time_format = 'd/m/Y H:i';

	/**
	 * @var Lorem_Ipsum_Generator
	 */
	protected static $lorem;

	protected static $data;

	protected static $data_cnt = array(
		'calendars' => array(1, 1), // 0-100 calendars in every label
		'calendar_label_words' => array(1, 3),
		'calendar_description_words' => array(0, 20),
		'events' => array(7, 30), // every week 0-5 events in every calendar
		'event_period' => array('2012-03-01 00:00:00', '2012-09-01 23:59:59'), // events period in which they are generated
		'event_length' => array(900, 21600), // Events length in seconds
		'event_summary_words' => array(1, 4),
		'event_location_words' => array(0, 4),
	);

	public static function init($refresh = FALSE)
	{
		Cval_Calendar_Generator::$data = array();

		Cval_Calendar_Generator::calendars();
		Cval_Calendar_Generator::events();
	}

	/**
	 * @return Lorem_Ipsum_Generator
	 */
	protected static function lorem()
	{
		if ( ! Cval_Calendar_Generator::$lorem)
		{
			Cval_Calendar_Generator::$lorem = Lorem_Ipsum_Generator::instance('cval-calendar-generator');
		}
		return Cval_Calendar_Generator::$lorem;
	}

	public static function calendars($cnt = NULL)
	{
		if ( ! isset(Cval_Calendar_Generator::$data['calendars']))
		{
			$count = Cval_Calendar_Generator::data_cnt('calendars', $cnt);
			for ($i=1; $i < $count+1; $i++)
			{
				$label = ucfirst(substr(Cval_Calendar_Generator::lorem()->get_content(Cval_Calendar_Generator::data_cnt('calendar_label_words'), 'plain', false), 0, -2));
				Cval_Calendar_Generator::$data['calendars'][$i] = array(
					'id' => $i,
					'label' => $label,
					'description' => Cval_Calendar_Generator::lorem()->get_content(Cval_Calendar_Generator::data_cnt('calendar_description_words'), 'plain', false),
				);
			}
			Cval_Calendar_Generator::$data['calendars'] = Cval_Calendar_Generator::sort(Cval_Calendar_Generator::$data['calendars'], 'label');
		}
		return Cval_Calendar_Generator::$data['calendars'];
	}

	public static function calendar($id = NULL)
	{
		$calendars = Cval_Calendar_Generator::calendars();

		return isset($calendars[$id]) ? $calendars[$id] : NULL;
	}

	public static function events($calendar_filter = NULL, $from = NULL, $to = NULL, $cnt = NULL)
	{
		if ( ! isset(Cval_Calendar_Generator::$data['events']))
		{
			list($period_start, $period_end) = Cval_Calendar_Generator::$data_cnt['event_period'];
			$period_start = strtotime($period_start);
			$period_end = strtotime($period_end);
			$week_seconds = Date::WEEK;
			
			foreach (Cval_Calendar_Generator::$data['calendars'] as $id_calendar => $calendar)
			{
				$period_start_tmp = $period_start;
				$total_id = 1;
				while ($period_start_tmp < $period_end)
				{
					for ($i = 1; $i <= Cval_Calendar_Generator::data_cnt('events', $cnt); $i++)
					{
						$start = $period_start_tmp + mt_rand(0, $week_seconds);
						$end = $start + Cval_Calendar_Generator::data_cnt('event_length');
						
						Cval_Calendar_Generator::$data['events'][$id_calendar][$total_id] = array(
							'id' => $total_id,
							'calendar' => $id_calendar,
							'summary' => ucfirst(substr(Cval_Calendar_Generator::lorem()->get_content(Cval_Calendar_Generator::data_cnt('event_summary_words'), 'plain', false), 0, -2)),
							'location' => ucfirst(substr(Cval_Calendar_Generator::lorem()->get_content(Cval_Calendar_Generator::data_cnt('event_location_words'), 'plain', false), 0, -2)),
							'start' => $start,
							'end' => $end
						);
						$total_id++;
					}
					
					$period_start_tmp = Date::move($period_start_tmp, 1, Date::WEEK);
				}
			}
		}
		if ($calendar_filter)
		{
			if ( ! isset(Cval_Calendar_Generator::$data['events'][$calendar_filter]))
				return array();

			$events = Cval_Calendar_Generator::$data['events'][$calendar_filter];
		}
		else
		{
			$events = Cval_Calendar_Generator::$data['events'];
		}

		if ($from AND $to)
		{
			foreach ($events as $key=>$event)
			{
				$in = ($event['end'] > $event['start'] AND $event['end'] > $from AND $event['start'] < $to);
				if ( ! $in)
				{
					unset($events[$key]);
				}
			}
		}

		return $events;
	}

	public static function event($calendar_filter, $id)
	{
		$events = Cval_Calendar_Generator::events($calendar_filter);

		return isset($events[$id]) ? $events[$id] : NULL;
	}

	protected static function get_rand($min_max)
	{
		if ( ! is_array($min_max))
			return $min_max;
		
		list($min, $max) = $min_max;
		return mt_rand($min, $max);
	}

	protected static function data_cnt($key, $override = NULL, $get_rand = TRUE)
	{
		$cnt = $override ? $override : Cval_Calendar_Generator::$data_cnt[$key];
		return $get_rand ? Cval_Calendar_Generator::get_rand($cnt) : $cnt;
	}

	public static function sort($items, $sort_key)
	{
		$sort = array();
		foreach ($items as $key=>$item)
		{
			$sort[$key] = $item[$sort_key];
		}
		asort($sort);

		$sorted_items = array();
		foreach ($sort as $key=>$value)
		{
			$sorted_items[$key] = $items[$key];
		}
		return $sorted_items;
	}

} // End Cval_Calendar_Generator
