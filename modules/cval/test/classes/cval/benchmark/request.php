<?php defined('SYSPATH') OR die('No direct script access.');

class Cval_Benchmark_Request 
{
	protected static $_sharedmemory;
	
	protected static $_sharedmemory_key;
	
	/**
	 * @return	SharedMemory_Base
	 */
	public static function sharedmemory(array $options = array())
	{
		if (Cval_Benchmark_Request::$_sharedmemory === NULL)
		{
			$tmp_path = APPPATH.'cache'.DIRECTORY_SEPARATOR.'sharedmemory';
			Dir::create($tmp_path);
			
			Cval_Benchmark_Request::$_sharedmemory = SharedMemory::factory(array_merge(array(
				'tmp' => $tmp_path
			), $options), 'file');
		}
		return Cval_Benchmark_Request::$_sharedmemory;
	}
	
	public static function sharedmemory_key()
	{
		if (Cval_Benchmark_Request::$_sharedmemory_key === NULL)
		{
			Cval_Benchmark_Request::$_sharedmemory_key = md5(DOCROOT).'cval_benchmark_request';
		}
		return Cval_Benchmark_Request::$_sharedmemory_key;
	}
	
	public static function add(Request $request)
	{
		// Do not add test routes to stats
		if ($request->directory == 'test')
		{
			return;
		}
		
		$benchmark = Cval_Benchmark_Request::get();
		
		$stats = Arr::get(Profiler::group_stats('requests'), 'requests');
		
		$time = $stats['total']['time'];
		$memory = $stats['total']['memory'];
		
		$benchmark['count']++;
		if ($request->directory == 'cval' AND $request->controller == 'media')
		{
			$benchmark['count_media']++;
		}
		
		if ($benchmark['min']['time'] === NULL OR $time < $benchmark['min']['time'])
		{
			$benchmark['min']['time'] = $time;
		}
		if ($benchmark['max']['time'] === NULL OR $time > $benchmark['max']['time'])
		{
			$benchmark['max']['time'] = $time;
		}
		$benchmark['total']['time'] += $time;
		$benchmark['average']['time'] = $benchmark['total']['time'] / $benchmark['count'];
		
		if ($benchmark['min']['memory'] === NULL OR $memory < $benchmark['min']['memory'])
		{
			$benchmark['min']['memory'] = $memory;
		}
		if ($benchmark['max']['memory'] === NULL OR $memory > $benchmark['max']['memory'])
		{
			$benchmark['max']['memory'] = $memory;
		}
		$benchmark['total']['memory'] += $memory;
		$benchmark['average']['memory'] = $benchmark['total']['memory'] / $benchmark['count'];
		
		$success = Cval_Benchmark_Request::sharedmemory()->set(Cval_Benchmark_Request::sharedmemory_key(), $benchmark);
	}
	
	public static function clear()
	{
		Cval_Benchmark_Request::sharedmemory()->rm(Cval_Benchmark_Request::sharedmemory_key());
	}
	
	public static function get()
	{
		return Cval_Benchmark_Request::sharedmemory()->get(Cval_Benchmark_Request::sharedmemory_key(), array(
			'total' => array(
				'time' => 0,
				'memory' => 0
			),
			'average' => array(
				'time' => 0,
				'memory' => 0
			),
			'min' => array(
				'time' => NULL,
				'memory' => NULL
			),
			'max' => array(
				'time' => NULL,
				'memory' => NULL
			),
			'count' => 0,
			'count_media' => 0
		));
	}
	
}
