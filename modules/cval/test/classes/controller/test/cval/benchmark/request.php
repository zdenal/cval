<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Cval_Benchmark_Request extends Controller_Ext
{

	public function action_index()
	{	
		$this->request->response = Kohana::debug(Cval_Benchmark_Request::get());
	}
	
	public function action_info()
	{	
		$this->request->response = Kohana::debug(array(
			'sharedMemory driver' => Cval_Benchmark_Request::sharedmemory()->engineName()
		));
	}
	
	public function action_clear()
	{
		Cval_Benchmark_Request::clear();
		$this->request->response = 'Benchmark cleared';
	}

}

