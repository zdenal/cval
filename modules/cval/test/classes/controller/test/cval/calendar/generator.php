<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Cval_Calendar_Generator extends Controller_Ext
{

	public function action_generate()
	{	
		Cval_Calendar_Generator::init();
		
		// Load iCalcreator class
		iCalcreator_Loader::init();
		// create a new calendar instance
		$v = new vcalendar(array(
			'unique_id' => 'cval-calendar-generator-'.Date::time(),
		));
		$v->setProperty('method', 'PUBLISH');
		$v->setProperty("X-WR-TIMEZONE", Date::timezone());
		
		foreach (Cval_Calendar_Generator::calendars() as $id_calendar => $calendar)
		{
			$v->setProperty("X-WR-CALNAME", Arr::get($calendar, 'label'));
			if (Arr::get($calendar, 'description'))
			{
				$v->setProperty("X-WR-CALDESC", Arr::get($calendar, 'description'));
			}
			
			foreach (Cval_Calendar_Generator::events($id_calendar) as $event)
			{
				$vevent = & $v->newComponent('vevent');
				
				$vevent->setProperty('dtstart', Date::format(Arr::get($event, 'start'), 'Y-m-d H:i:s'));
				$vevent->setProperty('dtend', Date::format(Arr::get($event, 'end'), 'Y-m-d H:i:s'));
				
				$vevent->setProperty('summary', Arr::get($event, 'summary'));
				if (Arr::get($event, 'location'))
				{
					$vevent->setProperty('location', Arr::get($event, 'location'));
				}
			}
			break;
		}
		
		$v->returnCalendar();
		exit;
	}

}

