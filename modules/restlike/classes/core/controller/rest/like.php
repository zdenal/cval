<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract Controller class for RESTful controller mapping. Supports GET, PUT,
 * POST, and DELETE. You can interpret REST calling by setting method parameter
 * in $_REQUEST['method']. By default, these methods will be mapped to these actions:
 *
 * GET
 * :  Mapped to the "index" action, lists all objects
 *
 * POST
 * :  Mapped to the "create" action, creates a new object
 *
 * PUT
 * :  Mapped to the "update" action, update an existing object
 *
 * DELETE
 * :  Mapped to the "delete" action, delete an existing object
 *
 * Additional methods can be supported by adding the method and action to
 * the `$_action_map` property.
 *
 * [!!] Using this class within a website will require heavy modification,
 * due to most web browsers only supporting the GET and POST methods.
 * Generally, this class should only be used for web services and APIs.
 *
 * @package    Core
 * @category   Controller
 */
abstract class Core_Controller_REST_Like extends Controller_Secure {

	/**
	 * @var string	Output format
	 */
	public $response_format = 'json';

	/**
	 * @var array   Actions accessible as internal-only
	 */
	protected $_internal_only = array();

	/**
	 * @var array   Request method-to-action map
	 */
	protected $_action_map = array
	(
		'GET'    => 'index',
		'PUT'    => 'update',
		'POST'   => 'create',
		'DELETE' => 'delete',
	);

	/**
	 * @var string	Originally requested action
	 */
	protected $_action_requested = '';

	/**
	 * @var array   Action-to-privilege ACL map
	 */
	protected $_acl_map = array(
		'index'		=> 'view',
		'create'	=> 'create',
		'update'	=> 'edit',
		'delete'	=> 'delete',
		'default'	=> 'manage'
	);

	/**
	 * Checks the requested method against the available methods. If the method
	 * is supported, sets the request action from the map. If not supported,
	 * the "invalid" action will be called.
	 */
	public function before()
	{
		// Check if internal request
		if ($this->request !== Request::instance())
		{
			$this->internal = TRUE;
		}

		if ( ! $this->internal)
		{
			if (Arr::get($_REQUEST, 'request_method'))
			{
				Request::$method = strtoupper(Arr::get($_REQUEST, 'request_method'));
			}
			elseif ($this->request->action != 'index' OR Request::$is_ajax)
			{
				Request::$method = array_search($this->request->action, $this->_action_map) OR 'invalid';
			}
		}
		else
		{
			Request::$method = array_search($this->request->action, $this->_action_map) OR 'invalid';
		}

		$this->_action_requested = $this->request->action;

		if ( ! isset($this->_action_map[Request::$method]))
		{
			$this->request->action = 'invalid';
		}
		else
		{
			$this->request->action = $this->_action_map[Request::$method];
		}

		if ($this->internal)
		{
			$this->response_format = NULL;
		}
		elseif (Arr::get($_REQUEST, 'response_format'))
		{
			$this->response_format = Arr::get($_REQUEST, 'response_format');
		}

		// Call parent tasks, ie secure tasks
		parent::before();
	}

	/**
	 * Formats output accroding to response format
	 */
	public function after()
	{
		// Format response according to response format set in before
		$this->request->response($this->request->response, $this->response_format);

		parent::after();
	}

	/**
	 * Sends a 405 "Method Not Allowed" response and a list of allowed actions.
	 */
	public function action_invalid()
	{
		// Send the "Method Not Allowed" response
		$this->request->status = 405;
		$this->request->headers['Allow'] = implode(', ', array_keys($this->_action_map));
	}

} // End REST_Like
