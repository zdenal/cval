<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'unique'		=> ':field must be unique',
	'email'			=> ':field must contains a valid email address',
	'emails'		=> ':field must contains a valid email address or more email addresses separated with :param1',
	'date_format'	=> ':field must be a date in :param1 format',
	'timestamp'		=> ':field must be a date',
	'greater'       => ':field must be greater than :param1',
	'greater_equal' => ':field must be greater than or equal to :param1',
	'less'			=> ':field must be less than :param1',
	'less_equal'	=> ':field must be less than or equal to :param1',
	'equal'			=> ':field must be equal to :param1',
	'min'			=> ':field must be greater than or equal to :param1',
	'max'			=> ':field must be less than or equal to :param1',
	'min_not_equal'	=> ':field must be greater than :param1',
	'max_not_equal'	=> ':field must be less than :param1',
	'phone_cs'		=> ':field must be a czech phone number',
	'phone_sk'		=> ':field must be a slovak phone number',
);