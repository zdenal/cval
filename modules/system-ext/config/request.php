<?php defined('SYSPATH') or die('No direct script access.');

return array(

	'additional_messages' => array(
		430 => 'Already authorized',
		440 => 'Wrong user input',
		450 => 'CSRF token missing or wrong',
		451 => 'Captcha token missing or wrong',
		460 => 'AJAX or internal request needed',

		555 => 'Debug Error',
	),
	
);
