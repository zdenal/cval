<?php defined('SYSPATH') or die('No direct script access.');

return array(

	/**
	 * Default system timezone, in PHP timezone format (UTC, Pacific/Nauru)
	 * 
	 * If FALSE, timezone is generated from PHP.
	 */
	'timezone' => FALSE
	
);
