<?php defined('SYSPATH') or die('No direct script access.');

class Core_Request extends Kohana_Request {

	/**
	 * @var	bool	Do we have i18n token in uri
	 */
	public static $has_i18n_lang = FALSE;

	/**
	 * Main request singleton instance. If no URI is provided, the URI will
	 * be automatically detected.
	 *
	 *     $request = Request::instance();
	 *
	 * @param   string   URI of the request
	 * @return  Request
	 * @uses    Request::detect_uri
	 */
	public static function instance( & $uri = TRUE)
	{
		// Extend http messages on first request instance
		if ( ! Request::$instance)
		{
			// Sort routes
			Route::sort();
			
			Request::$messages = Arr::merge(Request::$messages, Kohana::config('request.additional_messages'));
		}

		return parent::instance($uri);
	}

	/**
	 * This method is called when some exception occurs on main request call
	 *
	 * @uses    Kohana::exception_handler
	 * @param   Exception   exception object
	 * @param   Request		request object
	 * @return  boolean
	 */
	public static function exception_handler(Exception $e, Request $request = NULL)
	{
		if ($request AND $request->exception_handler)
		{
			$params = array($e, $request);
			$callback = $request->exception_handler;
			
			if (is_array($callback) AND Arr::is_assoc($callback))
			{
				$params = array_merge($params, Arr::get($callback, 'params', array()));
				$callback = Arr::get($callback, 'callback');
			}

			if (is_callable($callback))
			{
				$valid_response = call_user_func_array($callback, $params);

				if ($valid_response)
				{
					if ($request->send_headers()->response)
					{
						echo $request->response;
					}
					exit();
				}
			}
		}

		// Default error status
		$status = 500;

		if ($e instanceof ReflectionException)
		{
			$status = 404;
		}
		elseif ($e instanceof A2_Exception)
		{
			$status = A2::instance()->logged_in() ? 403 : 401;
			
			if ( ! Request::$is_ajax)
			{
				A2::exception_redirect($status);
			}
		}
		elseif ($e instanceof Request_Exception OR $e instanceof Redirect_Exception OR $e instanceof Debug_Exception OR $e->getCode())
		{
			$status = $e->getCode();
		}

		if ( ! $e->getMessage())
		{
			$message = isset(Request::$messages[$status]) ? Request::$messages[$status] : 'Undefined exception';

			$e = new Exception($message, $status, $e);
		}

		if ( ! headers_sent())
		{
			if (isset($_SERVER['SERVER_PROTOCOL']))
			{
				// Use the default server protocol
				$protocol = $_SERVER['SERVER_PROTOCOL'];
			}
			else
			{
				// Default to using newer protocol
				$protocol = 'HTTP/1.1';
			}

			// HTTP status line
			header($protocol.' '.$status.' '.$e->getMessage());
			// Make sure the proper content type is sent with a 500 status
			header('Content-Type: text/html; charset='.Kohana::$charset, TRUE, $status);

			// Send headers
			flush();
		}

		// Debug exception output
		if ($e instanceof Debug_Exception)
		{
			echo $e->debug;

			return;
		}

		// Pass controlled exception to Kohana Exception handler
		return Kohana::exception_handler($e);
	}

	/**
	 * @var	 string		request output format
	 */
	public $format;

	/**
	 * @var  mixed		valid php callback for additional exception handler
	 */
	public $exception_handler;
	
	// Internal parameters
	protected $_internal_params = array();

	/**
	 * Override - check if it is first load of request.
	 * If so do the language operations, ie. set default lang, remove language
	 * string from uri for route matching, redirect to simple url when default
	 * lang is in uri.
	 *
	 * Creates a new request object for the given URI. New requests should be
	 * created using the [Request::instance] or [Request::factory] methods.
	 *
	 *     $request = new Request($uri);
	 *
	 * @param   string  URI of the request
	 * @return  void
	 * @throws  Kohana_Request_Exception
	 * @uses    Route::all
	 * @uses    Route::matches
	 */
	public function __construct($uri)
	{
		if ( ! Request::$instance AND I18n::multilang())
		{
			// Remove trailing slashes from the URI
			$uri = trim($uri, '/');

			/* get the lang_abbr from uri segments */
			$segments = explode('/', $uri);
			$lang = isset($segments[0]) ? $segments[0] : NULL;

			if (I18n::lang_enabled($lang))
			{
				I18n::lang($lang);

				unset($segments[0]);

				$uri = implode('/', $segments);

				Request::$has_i18n_lang = TRUE;
			}
		}

		parent::__construct($uri);
	}

	/**
	 * Set response and make sure that its format is respected.
	 * Usage in controller:
	 * 
	 *		without formating: $this->request->response = $response;
	 *		   with formating: $this->request->response($response);
	 *
	 * @param	mixed	response value
	 * @param	string	format
	 * @param	bool	TRUE for replacing already assigned format
	 * @return	Request
	 * @uses	Request::format
	 */
	public function response($response = NULL, $format = NULL, $replace = FALSE)
	{
		$this->format($format, $replace);

		switch ($this->format)
		{
			case 'json':
			case 'json-text':
			case 'json-indent':
			case 'json-indent-html':
				// Convert empty responses to object
				$response = json_encode(empty($response) ? (object) $response : $response);
				// Indent json for better reading
				if (in_array($this->format, array('json-indent', 'json-indent-html')))
				{
					$response = Text::json_indent($response, $this->format == 'json-indent-html');
				}
				// Allow to set text/html content type used ie for jQuery
				$this->headers['Content-Type']		= $this->format == 'json' ? 'application/json' : 'text/html';
				$this->headers['Cache-Control']		= 'no-cache, must-revalidate';
				$this->headers['Expires']			= 'Mon, 26 Jul 1997 05:00:00 GMT';
				break;
			case 'xml':
				$this->headers['Content-Type']		= 'text/xml';
				$this->headers['Content-Length']	= strlen($response);
				break;
			case 'debug':
				// Debug response
				$response = Kohana::debug($response);
				$this->headers['Content-Length']	= strlen($response);
				break;
			case 'direct':
				break;
		}
		
		$this->response = $response;

		return $this;
	}

	/**
	 * Set response format used by Request->response().
	 *
	 * @param	string	format
	 * @param	bool	TRUE for replacing already assigned format
	 * @return	Request
	 */
	public function format($format = NULL, $replace = FALSE)
	{
		if (( ! $this->format AND $format !== NULL) OR $replace)
		{
			$this->format = $format;
		}

		return $this;
	}
	
	/**
	 * Check if controller and its action is callable. You can use it before
	 * execute() method to check if controller exists.
	 *
	 *     $request->is_callable();
	 *
	 * @return  bool
	 */
	public function is_callable()
	{
		// Create the class prefix
		$prefix = 'controller_';

		if ($this->directory)
		{
			// Add the directory name to the class prefix
			$prefix .= str_replace(array('\\', '/'), '_', trim($this->directory, '/')).'_';
		}

		if ( ! class_exists($prefix.$this->controller))
		{
			return FALSE;
		}
		
		// Load the controller using reflection
		$class = new ReflectionClass($prefix.$this->controller);

		if ($class->isAbstract())
		{
			return FALSE;
		}
		
		// Determine the action to use
		$action = empty($this->action) ? Route::$default_action : $this->action;
		
		$action_method = 'action_'.$action;
		
		if ( ! $class->hasMethod($action_method))
		{
			return FALSE;
		}
		
		$method = $class->getMethod($action_method);
		
		if ( ! $method->isPublic() OR $method->isAbstract() OR $method->isStatic())
		{
			return FALSE;
		}

		return TRUE;
	}
	
	/**
	 * Retrieves internal value for this request.
	 *
	 *     $id = $request->internal_param('id');
	 *
	 * @param   string   key of the value
	 * @param   mixed    default value if the key is not set
	 * @return  mixed
	 */
	public function internal_param($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			// Return the full array
			return $this->_internal_params;
		}

		return isset($this->_internal_params[$key]) ? $this->_internal_params[$key] : $default;
	}
	
	/**
	 * Sets internal value for this request.
	 *
	 *     $id = $request->internal_param_set('id', 1);
	 *
	 * @param   mixed	 key of the value or array of "key" => "value"
	 * @param   mixed    value
	 * @return  this
	 */
	public function internal_param_set($key, $value = NULL)
	{
		if (is_array($key))
		{
			foreach ($key as $tkey => $value)
			{
				$this->internal_param_set($tkey, $value);
			}
			
			return $this;
		}
		
		$this->_internal_params[$key] = $value;
		
		return $this;
	}

} // End Request
