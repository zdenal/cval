<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_Text extends Kohana_Text {

	/**
	 * Indents a flat JSON string to make it more human-readable.
	 *
	 * @param	string	$json The original JSON string to process.
	 * @param	bool	$html Output as HTML
	 * @return	string	Indented version of the original JSON string.
	 */
	public static function json_indent($json, $html = FALSE) {
		$tabcount = 0;
		$result = '';
		$inquote = false;
		$ignorenext = false;

		if ($html) {
			$tab = "&nbsp;&nbsp;&nbsp;";
			$newline = "<br/>";
		} else {
			$tab = "\t";
			$newline = "\n";
		}

		for($i = 0; $i < strlen($json); $i++) {
			$char = $json[$i];

			if ($ignorenext) {
				$result .= $char;
				$ignorenext = false;
			} else {
				switch($char) {
					case '{':
						if ($inquote) $result .= $char;
						else
						{
							$tabcount++;
							$result .= $char . $newline . str_repeat($tab, $tabcount);
						}
						break;
					case '}':
						if ($inquote) $result .= $char;
						else
						{
							$tabcount--;
							$result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char;
						}
						break;
					case ',':
						if ($inquote) $result .= $char;
						else $result .= $char . $newline . str_repeat($tab, $tabcount);
						break;
					case '"':
						$inquote = !$inquote;
						$result .= $char;
						break;
					case '\\':
						if ($inquote) $ignorenext = true;
						$result .= $char;
						break;
					default:
						$result .= $char;
				}
			}
		}

		return $result;
	}

	/**
	 * Strip a string from the end of a string
	 *
	 * @param	string $str      the input string
	 * @param	string $remove   OPTIONAL string to remove
	 * @return	string the modified string
	 * @uses	rtrim
	 */
	public static function rstrtrim($str, $remove = NULL)
	{
		$str    = (string) $str;
		$remove = (string) $remove;

		if (empty($remove))
		{
			return rtrim($str);
		}

		$len = strlen($remove);
		$offset = strlen($str)-$len;
		while ($offset > 0 AND $offset == strpos($str, $remove, $offset))
		{
			$str = substr($str, 0, $offset);
			$offset = strlen($str)-$len;
		}

		return rtrim($str);
	}
	
	/**
	 * Strips wrapping HTML tag from string
	 * 
	 * @param	string	string to strip
	 * @param	string	tag name to strip
	 * @return	string
	 */
	public static function strip_wrapping_tag($str, $tag = 'div', $recursive = TRUE, $trim = TRUE)
	{	
		if ($trim)
		{
			$str = trim($str);
		}
		
		while (preg_match('#^<'.$tag.'>(.+)</'.$tag.'>$#', $str, $match) === 1) 
		{
			// Check if we really removed onclosing tags
			if (strpos($match[1], '</div>') < strpos($match[1], '<div>'))
			{
				break;
			}
			
			$str = $match[1]; // if matched, replace our string by the match
			
			if ($trim)
			{
				$str = trim($str);
			}
			
			if ( ! $recursive)
			{
				break;
			}
		}
		return $str;
	}
	
	/**
	 * Extract specific HTML tags and their attributes from a string.
	 *
	 * You can either specify one tag, an array of tag names, or a regular expression that matches the tag name(s).
	 * If multiple tags are specified you must also set the $selfclosing parameter and it must be the same for
	 * all specified tags (so you can't extract both normal and self-closing tags in one go).
	 *
	 * The function returns a numerically indexed array of extracted tags. Each entry is an associative array
	 * with these keys :
	 * 	tag_name	- the name of the extracted tag, e.g. "a" or "img".
	 * 	offset		- the numberic offset of the first character of the tag within the HTML source.
	 * 	contents	- the inner HTML of the tag. This is always empty for self-closing tags.
	 * 	attributes	- a name -> value array of the tag's attributes, or an empty array if the tag has none.
	 * 	full_tag	- the entire matched tag, e.g. '<a href="http://example.com">example.com</a>'. This key
	 * 		          will only be present if you set $return_the_entire_tag to true.
	 *
	 * @param string $html The HTML code to search for tags.
	 * @param string|array $tag The tag(s) to extract.
	 * @param bool $selfclosing	Whether the tag is self-closing or not. Setting it to null will force the script to try and make an educated guess.
	 * @param bool $return_the_entire_tag Return the entire matched tag in 'full_tag' key of the results array.
	 * @param string $charset The character set of the HTML code. Defaults to UTF-8.
	 *
	 * @return array An array of extracted tags, or an empty array if no matching tags were found.
	 */
	public static function extract_tags($html, $tag, $selfclosing = NULL, $return_the_entire_tag = FALSE, $charset = 'UTF-8')
	{
		if (is_array($tag))
		{
			$tag = implode('|', $tag);
		}

		//If the user didn't specify if $tag is a self-closing tag we try to auto-detect it
		//by checking against a list of known self-closing tags.
		$selfclosing_tags = array('area', 'base', 'basefont', 'br', 'hr', 'input', 'img', 'link', 'meta', 'col', 'param');
		if (is_null($selfclosing))
		{
			$selfclosing = in_array($tag, $selfclosing_tags);
		}

		//The regexp is different for normal and self-closing tags because I can't figure out
		//how to make a sufficiently robust unified one.
		if ($selfclosing)
		{
			$tag_pattern =
					'@<(?P<tag>'.$tag.')			# <tag
			(?P<attributes>\s[^>]+)?		# attributes, if any
			\s*/?>					# /> or just >, being lenient here
			@xsi';
		}
		else
		{
			$tag_pattern =
					'@<(?P<tag>'.$tag.')			# <tag
			(?P<attributes>\s[^>]+)?		# attributes, if any
			\s*>					# >
			(?P<contents>.*?)			# tag contents
			</(?P=tag)>				# the closing </tag>
			@xsi';
		}

		$attribute_pattern =
				'@
		(?P<name>\w+)							# attribute name
		\s*=\s*
		(
			(?P<quote>[\"\'])(?P<value_quoted>.*?)(?P=quote)	# a quoted value
			|							# or
			(?P<value_unquoted>[^\s"\']+?)(?:\s+|$)			# an unquoted value (terminated by whitespace or EOF)
		)
		@xsi';

		//Find all tags
		if ( ! preg_match_all($tag_pattern, $html, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE))
		{
			//Return an empty array if we didn't find anything
			return array();
		}

		$tags = array();
		foreach ($matches as $match)
		{

			//Parse tag attributes, if any
			$attributes = array();
			if ( ! empty($match['attributes'][0]))
			{

				if (preg_match_all($attribute_pattern, $match['attributes'][0], $attribute_data, PREG_SET_ORDER))
				{
					//Turn the attribute data into a name->value array
					foreach ($attribute_data as $attr)
					{
						if ( ! empty($attr['value_quoted']))
						{
							$value = $attr['value_quoted'];
						}
						else if ( ! empty($attr['value_unquoted']))
						{
							$value = $attr['value_unquoted'];
						}
						else
						{
							$value = '';
						}

						//Passing the value through html_entity_decode is handy when you want
						//to extract link URLs or something like that. You might want to remove
						//or modify this call if it doesn't fit your situation.
						$value = html_entity_decode($value, ENT_QUOTES, $charset);

						$attributes[$attr['name']] = $value;
					}
				}
			}

			$tag = array(
				'tag_name' => $match['tag'][0],
				'offset' => $match[0][1],
				'contents' => ! empty($match['contents']) ? $match['contents'][0] : '', //empty for self-closing tags
				'attributes' => $attributes,
				'full_length' => strlen($match[0][0]),
			);
			if ($return_the_entire_tag)
			{
				$tag['full_tag'] = $match[0][0];
			}

			$tags[] = $tag;
		}

		return $tags;
	}

} // End Core_Text