<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_Security extends Kohana_Security {

	/**
	 * Generate and store a unique token which can be used to help prevent
	 * [CSRF](http://wikipedia.org/wiki/Cross_Site_Request_Forgery) attacks.
	 *
	 *     $token = Security::token();
	 *
	 * You can insert this token into your forms as a hidden field:
	 *
	 *     echo Form::hidden('csrf', Security::token());
	 *
	 * And then check it when using [Validate]:
	 *
	 *     $array->rules('csrf', array(
	 *         'not_empty'       => NULL,
	 *         'Security::check' => NULL,
	 *     ));
	 *
	 * This provides a basic, but effective, method of preventing CSRF attacks.
	 *
	 * @param   boolean  force a new token to be generated?
	 * @param	string	 append string to token_name, allows to create different tokens for different forms
	 * @return  string
	 * @uses    Session::instance
	 */
	public static function token($new = FALSE, $append = NULL)
	{
		// Backup orig security token
		$orig_token_name = Security::$token_name;

		// Append part
		Security::$token_name .= $append;

		// Get token
		$token = parent::token($new);

		// Restore orig security token
		Security::$token_name = $orig_token_name;

		return $token;
	}

	/**
	 * Check that the given token matches the currently stored security token.
	 *
	 *     if (Security::check($token))
	 *     {
	 *         // Pass
	 *     }
	 *
	 * @param   string   token to check
	 * @param	string	 append string to token_name, allows to create different tokens for different forms
	 * @return  boolean
	 * @uses    Security::token
	 */
	public static function check($token, $append = NULL, $exception = FALSE)
	{
		// Check token with append part
		$success = Security::token(FALSE, $append) === $token;

		// Throw security exception for CSRF if set
		if ( ! $success AND $exception)
		{
			throw new Security_Exception(450);
		}
		return $success;
	}

} // End Core_Security