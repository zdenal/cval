<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Adds i18n using in views.
 * On creating a view object search for i18n versions of file.
 * Directory structure in views folder is:
 * 
 * +- views
 * |    +- i18n
 * |        +- '<language>'
 * |		|	+- views
 * |		|	|	+- view1.php
 * |        |   +- '<country>'.php
 * |        |		+- views
 * |		|			+- view1.php
 * 
 * @author zdenek
 *
 */

class Core_View extends Kohana_View {

	/**
	 * @var  string   target template: folder/some, template ...
	 */
	public static $template = FALSE;
	
	/**
	 * @var  boolean  try to find translated version in i18n view subfolders
	 */
	public static $i18n = FALSE;
	
	/**
	 * Returns a new View object. If you do not define the "file" parameter,
	 * you must call [View::set_filename].
	 *
	 *     $view = View::factory($file);
	 *
	 * @param   string  view filename
	 * @param   array   array of values
	 * @param	string	template folder from where start to search the views
	 * @param	boolean	if search in i18n subdirectories
	 * @param	string	language used for searching in i18n subdirectories
	 * @return  View
	 */
	public static function factory($file = NULL, array $data = NULL, $template = NULL, $i18n = NULL, $lang = NULL)
	{
		return new View($file, $data, $template, $i18n, $lang);
	}

	/**
	 * Get and set the target template.
	 *
	 *     // Get the current template
	 *     $template = View::template();
	 *
	 *     // Change the current template to another folder
	 *     View::template('template/another');
	 *
	 * @param   string   new template setting
	 * @return  string
	 */
	public static function template($template = NULL)
	{
		if ($template)
		{
			// Normalize the template
			View::$template = rtrim($template, '/').'/';
		}

		return View::$template;
	}

	/**
	 * Get and set view i18n searching
	 *
	 *     // Get the view i18n searching
	 *     $i18n = View::i18n();
	 *
	 *     // Change the view i18n searching
	 *     View::i18n(TRUE);
	 *
	 * @param   boolean   new i18n setting
	 * @return  boolean
	 */
	public static function i18n($i18n = NULL)
	{
		if ($i18n !== NULL)
		{
			// Convert to boolean
			View::$i18n = (bool) $i18n;
		}

		return View::$i18n;
	}
	
	/**
	 * Gets a global variable.
	 *
	 *     View::get_global($name);
	 *
	 * @param   string  variable name
	 * @param   mixed   default value if name is not set
	 * @return  mixed	value
	 */
	public static function get_global($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			return View::$_global_data;
		}
		if (is_array($key))
		{
			return Arr::extract(View::$_global_data, $key);
		}
		
		return Arr::get(View::$_global_data, $key, $default);
	}
	
	/**
	 * @var  string   target template for this view
	 */
	public $_template;
	
	/**
	 * @var  string   try to find translated version in i18n view subfolders of this view
	 */
	public $_i18n;
	
	/**
	 * @var  string   target view language: en-us, es-es, zh-cn, etc
	 */
	public $_lang;
	
	/**
	 * Sets the initial view filename and local data. Views should almost
	 * always only be created using [View::factory].
	 *
	 *     $view = new View($file);
	 *
	 * @param   string  view filename
	 * @param   array   array of values
	 * @param	string	template folder from where start to search the views
	 * @param	boolean	if search in i18n subdirectories
	 * @param	string	language used for searching in i18n subdirectories
	 * @return  void
	 * @uses    View::set_filename
	 */
	public function __construct($file = NULL, array $data = NULL, $template = NULL, $i18n = NULL, $lang = NULL)
	{
		if ($template === NULL)
		{
			$template = View::$template;	
		}
		$this->set_template($template);
		
		if ($i18n === NULL)
		{
			$i18n = View::$i18n;	
		}
		$this->set_i18n($i18n);
		
		if ($lang === NULL)
		{
			$lang = I18n::$lang;	
		}
		$this->set_lang($lang);
		
		parent::__construct($file, $data);

		// Add variable $thiss to view scope
		$this->bind('thiss', $this);
	}
	
	/**
	 * Sets the view filename and try to get translated view version if exists.
	 * Search first in template directory if set.
	 *
	 *     $view->set_filename($file);
	 *
	 * @param   string  view filename
	 * @return  View
	 * @throws  Kohana_View_Exception
	 */
	public function set_filename($file)
	{
		// Check if array of files is not passed
		if (is_array($file))
		{
			$found = FALSE;
			
			// If so go through all files and try to call this method
			foreach ($file as $tmp_file)
			{
				try 
				{
					$this->set_filename($tmp_file);
				}
				catch (Kohana_View_Exception $e)
				{
					// If view not found go to next file
					continue;
				}
				
				// We found a file, break cycle
				$found = TRUE;
				break;
			}
			
			// If file not found throw not found exception with last file
			if ( ! $found)
			{
				throw new Kohana_View_Exception('The requested view :file could not be found', array(
					':file' => $tmp_file,
				));
			}
			
			// Return this object
			return $this;
		}
		
		$path = FALSE;
		
		if ($this->_template)
		{
			$path = $this->find_filename($this->_template.$file);	
		}
		
		if ($path === FALSE)
		{
			$path = $this->find_filename($file);
		}
		
		if ($path === FALSE) 
		{
			throw new Kohana_View_Exception('The requested view :file could not be found', array(
				':file' => $file,
			));
		}

		// Store the file path locally
		$this->_file = $path;

		return $this;
	}

	/**
	 * Returns the view filename.
	 *
	 *     $view->get_filename();
	 *
	 * @return  string
	 */
	public function get_filename()
	{
		return $this->_file;
	}
	
	/**
	 * Finds the view filename and try to get translated view version if exists.
	 *
	 *     $view->find_filename($file);
	 *
	 * @param   string  view filename
	 * @return  mixed	string path or FALSE if not found
	 */
	public function find_filename($file)
	{
		$path = FALSE;
		
		// If i18n is TRUE then search for i18n version
		if ($this->_i18n)
		{		
			// Split the language: language, region, locale, etc
			$parts = explode('-', $this->_lang);
			
			$path = FALSE;
			
			do
			{
				// Create a path for this set of parts
				$path = $parts;
				array_unshift($path, 'i18n');
				array_push($path, 'views');
				$path = implode(DIRECTORY_SEPARATOR, $path);
				
				if (($path = Kohana::find_file('views', $path.DIRECTORY_SEPARATOR.$file)) !== FALSE)
				{
					break;
				}
		
				// Remove the last part
				array_pop($parts);
			}
			while ($parts);	
		}
		
		if ($path === FALSE)
		{
			$path = Kohana::find_file('views', $file);
		}
		
		return $path;
	}
	
	/**
	 * Sets the view template.
	 *
	 *     $view->set_template($template);
	 *
	 * @param   string  target template
	 * @return  View
	 */
	public function set_template($template)
	{
		$this->_template = $template;
		
		return $this;
	}
	
	/**
	 * Sets view i18n searching.
	 *
	 *     $view->set_i18n(TRUE);
	 *
	 * @param   boolean  i18n value
	 * @return  View
	 */
	public function set_i18n($i18n)
	{
		$this->_i18n = (bool) $i18n;
		
		return $this;
	}
	
	/**
	 * Sets the view language.
	 *
	 *     $view->set_lang($lang);
	 *
	 * @param   string  target language
	 * @return  View
	 */
	public function set_lang($lang)
	{
		$this->_lang = $lang;
		
		return $this;
	}

	/**
	 * Get a variable by name or by names or all variables if parameter
	 * key is not set:
	 *
	 *     // Get variable $foo from the view
	 *     $view->get('foo');
	 *
	 * You can also use an array to get several values at once:
	 *
	 *     // Get the values $food and $beverage from the view
	 *     $view->get(array('food', 'beverage'));
	 *
	 * Or you can also ommit parameter and get all variables at once:
	 *
	 *     // Get all the values from the view
	 *     $view->get();
	 *
	 * @param   string  variable name or an array of variables
	 * @return  mixed	value
	 */
	public function get($key = NULL, $default = NULL)
	{
		if ($key === NULL)
		{
			return $this->_data;
		}
		if (is_array($key))
		{
			return Arr::extract($this->_data, $key);
		}

		return Arr::get($this->_data, $key, $default);
	}

	/**
	 * Pass all variables from other View to another.
	 * If variable with same name exist than is overriden.
	 * It can be used also in view file passign variable $thiss
	 * as parameter.
	 *
	 *		// Usage in controller:
	 *		$view = View::factory($file)->include_view($another_view);
	 *
	 *		// Usage in view file:
	 *		<?php echo View::factory($file)->include_view($thiss) ?>
	 *
	 * @param	View	View object from which using the variables
	 * @return	View
	 */
	public function include_view(View $view)
	{
		$this->_data += $view->get();

		return $this;
	}

	/**
	 * Get all data without thiss variable.
	 *
	 * @return  array
	 */
	public function data($with_global = FALSE)
	{
		$data = $this->get();
		
		if ($with_global)
		{
			$data += View::get_global();
		}

		unset($data['thiss']);

		return $data;
	}

	/**
	 * Debug all data without thiss variable.
	 *
	 * @param	bool	exit after echoing dump
	 * @param	bool	echo output if not exit
	 * @return  string
	 * @uses	Kohana::debug_custom
	 */
	public function debug($exit = FALSE, $echo = TRUE)
	{
		$debug = Kohana::debug_custom($this->data());
		
		if ($exit)
		{
			echo $debug;
			exit;
		}

		if ( ! $echo)
		{
			return $debug;
		}

		echo $debug;
	}
	
}
