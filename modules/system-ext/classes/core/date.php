<?php defined('SYSPATH') or die('No direct script access.');

class Core_Date extends Kohana_Date {

	const SECOND = 1;
	
	protected static $_time;
	
	protected static $_timezone;
	
	/**
	 * Is called on first Date::time() call and sets actual time.
	 */
	public static function init()
	{
		// Set new timezone
		Date::timezone(NULL, TRUE);
		
		Date::$_time = time();
	}
	
	/**
	 * Sets/gets timezone.
	 * 
	 * @param	mixed	string or NULL, if NULL acts as simple getter
	 * @param	bool	TRUE to propagate timezone to date_default_timezone_set()
	 * @return	string	Actual timezone
	 */
	public static function timezone($timezone = NULL, $propagate = FALSE)
	{
		// Make sure timezone is initiated
		if (Date::$_timezone === NULL)
		{
			$timezone = Kohana::config('date.timezone');
		
			// If timezone not set, get it from system
			if ($timezone === FALSE)
			{
				$timezone = date_default_timezone_get();
			}
			
			Date::$_timezone = $timezone;
		}
		
		// Set timezone if passed
		if ($timezone)
		{
			Date::$_timezone = $timezone;
			
			if ($propagate)
			{
				// Set new timezone if needed
				date_default_timezone_set(Date::$_timezone);
			}
		}
		
		return Date::$_timezone;
	}
	
	/**
	 * Get and set the time.
	 *
	 *     // Get the time
	 *     $time = Date::time();
	 *
	 *     // Change the time
	 *     Date::time($timestamp);
	 *
	 * @param   timestamp   new time
	 * @return  timestamp
	 */
	public static function time($time = NULL)
	{
		// Make sure time is initiated
		if (Date::$_time === NULL)
		{
			Date::init();
		}
		// Set time if passed
		if ($time)
		{
			Date::$_time = (int) $time;
		}
		
		return Date::$_time;
	}
	
	/**
	 * Returns the day number in week
	 * 
	 * @param integer timestamp
	 * @return integer 1 (for Monday) through 7 (for Sunday)
	 */
	public static function day_number($timestamp)
	{
		return date('N', $timestamp);	
	}
	
	/**
	 * Returns timestamp moved by $step * $interval forward or not.
	 * 
	 * @param   integer  timestamp to move
	 * @param   integer  step
	 * @param   mixed    interval as string or integer
	 * @param	boolean	 forward
	 * @param	boolean	 strict use strtotime movement or just mathematic
	 * @return  integer    moved timestamp
	 */
	public static function move($timestamp = NULL, $step = 1, $interval = 'day', $forward = TRUE, $strict = TRUE)
	{
		if ($timestamp === NULL)
		{
			$timestamp = Date::time();
		}
		
		// Inverse step
		if ( ! $forward)
		{
			$step *= -1;
		}
		
		if ($strict OR is_numeric($interval))
		{
			return $timestamp + $step * (is_numeric($interval) ? $interval : constant('Date::'.strtoupper($interval)));
		}
		
		return strtotime(sprintf("%+d ".$interval, $step), $timestamp); 
	}
	
	/**
	 * Converts date/time value given format.
	 * 
	 * @param	mixed 	$value string, integer (timestamp), or DateTime value.  Empty string will
	 *					be treated as NULL.
	 * @param	string	$format output format
	 * @return  mixed	string, integer (timestamp), DateTime
	 */
	public static function format($value, $format = 'Y-m-d H:i:s')
	{
		// we treat '' as NULL because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($value === NULL || $value === '')
		{
			$dt = NULL;
		}
		elseif ($value instanceof DateTime)
		{
			$dt = $value;
		}
		else
		{
			// some string/numeric value passed; we normalize that so that we can
			// convert it.
			// if it's a unix timestamp
			if (is_numeric($value)) 
			{
				$dt = new DateTime('@'.$value, new DateTimeZone('UTC'));
				// We have to explicitly specify and then change the time zone because of a
				// DateTime bug: http://bugs.php.net/bug.php?id=43003
				$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			}
			else
			{
				$dt = new DateTime($value);
			}
		}
		
		if ($dt === NULL)
		{
			return NULL;
		}
		elseif ($format === NULL)
		{
			return $dt;
		}
		elseif (strpos($format, '%') !== false)
		{
			return strftime($format, $dt->format('U'));
		}
		else
		{
			return $dt->format($format);
		}
	}
	
	/**
	 * Same as standar getdate function but allows
	 * pass value other than timestamp.
	 * 
	 * @param	mixed 	$value string, integer (timestamp), or DateTime value.  Empty string will
	 *					be treated as NULL.
	 * @return  array	getdate array
	 * @return 	NULL	when value cannot be parsed to date/time value
	 */
	public static function getdate($value = NULL)
	{
		// Convert value to timestamp for getdate use
		$value = Date::format($value, 'U');
		
		return getdate($value);
	}
	
	/**
	 * Return formated day time in day
	 * 
	 * @param	mixed	string, integer (timestamp), DateTime value, 
	 * 					array(hours[optional], minutes[optional], seconds[optional]). 
	 * 					Empty string and NULL will be treated as today timestamp.
	 * 					Missing arguments in array will be replace with 0 value.
	 * @param	mixed 	string, integer (timestamp), or DateTime value. Empty string
	 *					and will be treated as today timestamp.
	 * @param	string	output format
	 * @return  mixed	string, integer (timestamp), DateTime
	 */
	public static function day_time($time = NULL, $day = NULL, $format = 'Y-m-d H:i:s')
	{
		// Use real time if not set
		if ($time === NULL)
		{
			$time = time();
		}
		// If time is array of hours, minutes, seconds make sure all three params are filled
		elseif (is_array($time))
		{
			list($hours, $minutes, $seconds) = array_slice(array_merge(array_fill(0, 3, 0), $time), 0, 3);
			$time = mktime($hours, $minutes, $seconds);	
		}
		
		// Convert time and day to date info array
		$time_arr = Date::getdate($time);
		$day_arr = Date::getdate($day);
		
		// Construct day time
		$timestamp = mktime($time_arr['hours'], $time_arr['minutes'], $time_arr['seconds'], 
						$day_arr['mon'], $day_arr['mday'], $day_arr['year']);
		
		// Return formated day time
		return Date::format($timestamp, $format);
	}

	/**
	 * Returns PHP microtime() output without float sign
	 *
	 *     // Get the microtime
	 *     $microtime = Date::microtime();
	 *
	 * @return  integer
	 */
	public static function microtime()
	{
		return (int) str_replace('.', '', microtime(TRUE));
	}
	
	

	/**
	 * Sets the time of the day to 00:00:00
	 * 
	 * @param	integer (timestamp), if NULL is passed or the argument is
	 *		not set, todays date will be taken
	 * 
	 * @return	integer (timestamp) with time set to zeros
	 */
	public static function reset_time($date = NULL)
	{
		if ($date === NULL)
		{
			$date = Date::time();
		}
		$date = Date::getdate($date);
		$date = mktime(0, 0, 0, $date['mon'], $date['mday'], $date['year']);
		return $date;
	}

}
