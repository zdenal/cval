<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_File extends Kohana_File
{
	/**
	 * @var	int		Default permission for file operation
	 */
	public static $permission = 0664;

	public static function permission($permission = NULL)
	{
		if ($permission !== NULL)
		{
			File::$permission = $permission;
		}

		return File::$permission;
	}

	/**
	 * Read File
	 *
	 * Opens the file specfied in the path and returns it as a string.
	 *
	 * @access	public
	 * @param	string	path to file
	 * @return	string
	 */
	public static function read($file)
	{
		if ( ! file_exists($file))
		{
			return FALSE;
		}

		if (function_exists('file_get_contents'))
		{
			return file_get_contents($file);
		}

		if ( ! $fp = @fopen($file, FOPEN_READ))
		{
			return FALSE;
		}

		flock($fp, LOCK_SH);

		$data = '';
		if (filesize($file) > 0)
		{
			$data = & fread($fp, filesize($file));
		}

		flock($fp, LOCK_UN);
		fclose($fp);

		return $data;
	}

	/**
	 * Write File
	 *
	 * Writes data to the file specified in the path.
	 * Creates a new file if non-existent.
	 *
	 * Mode examples:
	 * 
	 * FOPEN_READ							rb
	 * FOPEN_READ_WRITE						r+b
	 * FOPEN_WRITE_CREATE_DESTRUCTIVE 		wb // truncates existing file data, use with care
	 * FOPEN_READ_WRITE_CREATE_DESTRUCTIVE 	w+b // truncates existing file data, use with care
	 * FOPEN_WRITE_CREATE					ab
	 * FOPEN_READ_WRITE_CREATE				a+b
	 * FOPEN_WRITE_CREATE_STRICT			xb
	 * FOPEN_READ_WRITE_CREATE_STRICT		x+b
	 *
	 * @access	public
	 * @param	string	path to file
	 * @param	string	file data
	 * @return	bool
	 */
	public static function write($path, $data, $mode = 'wb')
	{
		if ( ! $fp = @fopen($path, $mode))
		{
			return FALSE;
		}

		flock($fp, LOCK_EX);
		fwrite($fp, $data);
		flock($fp, LOCK_UN);
		fclose($fp);

		return TRUE;
	}

	public static function delete($file)
	{
		if ( ! file_exists($file))
		{
			return TRUE;
		}
		return unlink($file);
	}

	/**
	 * Get File Info
	 *
	 * Given a file and path, returns the name, path, size, date modified
	 * Second parameter allows you to explicitly declare what information you want returned
	 * Options are: name, server_path, size, date, readable, writable, executable, fileperms
	 * Returns FALSE if the file cannot be found.
	 *
	 * @access	public
	 * @param	string	path to file
	 * @param	mixed	array or comma separated string of information returned
	 * @return	array
	 */
	public static function info($file, $returned_values = array('name', 'server_path', 'size', 'date'))
	{

		if ( ! file_exists($file))
		{
			return FALSE;
		}

		if (is_string($returned_values))
		{
			$returned_values = explode(',', $returned_values);
		}

		foreach ($returned_values as $key)
		{
			switch ($key)
			{
				case 'name':
					$fileinfo['name'] = substr(strrchr($file, DIRECTORY_SEPARATOR), 1);
					break;
				case 'server_path':
					$fileinfo['server_path'] = $file;
					break;
				case 'size':
					$fileinfo['size'] = filesize($file);
					break;
				case 'date':
					$fileinfo['date'] = filectime($file);
					break;
				case 'readable':
					$fileinfo['readable'] = is_readable($file);
					break;
				case 'writable':
					// There are known problems using is_weritable on IIS.  It may not be reliable - consider fileperms()
					$fileinfo['writable'] = is_writable($file);
					break;
				case 'executable':
					$fileinfo['executable'] = is_executable($file);
					break;
				case 'fileperms':
					$fileinfo['fileperms'] = fileperms($file);
					break;
			}
		}

		return $fileinfo;
	}

	/**
	 * Copy file or dir. When dir is passed it does a recursive copy.
	 *
	 * @param	string $path
	 * @param	string $dest
	 * @return	bool
	 */
	public static function copy($path, $dest)
	{
		if (is_dir($path))
		{
			// Normalize dirs
			$path = Dir::normalize($path);
			$dest = Dir::normalize($dest);

			// Create destination if not exists
			if ( ! file_exists($dest))
			{
				Dir::create($dest);
			}

			// Get filenames in actual level
			$filenames = Dir::filenames($path);

			foreach ($filenames as $filename)
			{
				// Do recursive copy of dirs
				File::copy($path.$filename, $dest.$filename);
			}
			return TRUE;
		}
		elseif (is_file($path))
		{
			return copy($path, $dest);
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Chmod file or dir. When dir is passed it does a recursive chmod.
	 *
	 * @param	string	$path
	 * @param	int		$file_perm
	 * @param	int		$dir_perm
	 * @return	bool
	 */
	public static function chmod($path, $file_perm = NULL, $dir_perm = NULL)
	{
		if ($file_perm === NULL)
		{
			$file_perm = File::permission();
		}
		if ($dir_perm === NULL)
		{
			$dir_perm = Dir::permission();
		}

		// Check if the path exists
		if ( ! file_exists($path))
		{
			return FALSE;
		}
		// See whether this is a file
		if (is_file($path))
		{
			// Chmod the file with our given filepermissions
			chmod($path, $file_perm);
		}
		// If this is a directory...
		elseif (is_dir($path))
		{
			// Normalize dir
			$path = Dir::normalize($path);

			// Get filenames in actual level
			$filenames = Dir::filenames($path);

			foreach ($filenames as $filename)
			{
				// Do recursive chmod of dirs
				File::chmod($path.$filename, $file_perm, $dir_perm);
			}
			
			// When we are done with the contents of the directory, we chmod the directory itself
			chmod($path, $dir_perm);
		}
		// Everything seemed to work out well, return TRUE
		return TRUE;
	}
	
	public static function rename($old_file, $new_file)
	{
		$old_file = realpath($old_file);

		return rename($old_file, $new_file);
	}
	
	/**
	 * Converts upload media sized used for Upload::size method to bytes.
	 *
	 * @param   string	size in format for Upload::size method
	 * @return  int		size converted to bytes
	 */
	public static function upload_size_to_bytes($size)
	{
		// Only one size is allowed
		$size = strtoupper(trim($size));

		if ( ! preg_match('/^[0-9]++[BKMG]$/', $size))
		{
			throw new Kohana_Exception('Size does not contain a digit and a byte value: :size', array(
				':size' => $size,
			));
		}

		// Make the size into a power of 1024
		switch (substr($size, -1))
		{
			case 'G': $size = intval($size) * pow(1024, 3); break;
			case 'M': $size = intval($size) * pow(1024, 2); break;
			case 'K': $size = intval($size) * pow(1024, 1); break;
			default:  $size = intval($size);                break;
		}
		
		return $size;
	}

}

// End file
