<?php defined('SYSPATH') or die('No direct script access.');

class Core_Route extends Kohana_Route {

	/**
	 * @var string	Config key used for route positioning
	 */
	public static $route_position_config_key = 'route_position';

	/**
	 * @var bool	Tells if we need to do i18n check in matches() method
	 */
	protected static $_do_i18n_match = TRUE;

	/**
	 * Sort routes by its position key in config array.
	 * Used only on Request::instance() call
	 *
	 * @return  void
	 */
	public static function sort()
	{
		// Empty positions array
		$positions = array();

		foreach (Route::$_routes as $name => $route)
		{
			$position = $route->config(Route::$route_position_config_key, 1);

			// Place each route to array with its position
			if ( ! isset($positions[$position]))
			{
				$positions[$position] = array();
			}

			$positions[$position][] = $name;
		}

		// Sort positions by key
		ksort($positions, SORT_NUMERIC);

		// Sorted routes array
		$routes = array();

		foreach ($positions as $route_names)
		{
			foreach ($route_names as $route_name)
			{
				// Add route object
				$routes[$route_name] = Route::get($route_name, FALSE);
			}
		}

		// Overwrite original routes with sorted routes
		Route::$_routes = $routes;

		//echo Kohana::debug_exit(array_keys(Route::$_routes));
	}

	/**
	 * Get the name of a route.
	 *
	 *     $name = Route::name($route)
	 *
	 * @param   object  Route instance
	 * @return  string
	 */
	public static function name(Route $route, $with_i18n = TRUE, $force_i18n = FALSE)
	{
		$name = parent::name($route);

		if ($name === FALSE)
		{
			return $name;
		}

		if (I18n::multilang())
		{
			if ( ! $with_i18n)
			{
				$name = preg_replace('/:i18n\[.+\]/', '', $name);
			}
			elseif ($force_i18n AND strpos($name, ':i18n[') === FALSE)
			{
				$lang = $route->lang();

				$name .= ':i18n['.$lang.']';
			}
		}

		return $name;
	}

	/**
	 * Create a URL from a route name. This is a shortcut for:
	 *
	 *     echo URL::site(Route::get($name)->uri($params), $protocol, $lang);
	 *
	 * @param   string   route name
	 * @param   array    URI parameters
	 * @param   mixed   protocol string or boolean, adds protocol and domain
	 * @param	string	language
	 * @return  string
	 * @since   3.0.7
	 * @uses    URL::site
	 */
	public static function url($name, array $params = NULL, $protocol = NULL, $lang = NULL)
	{
		$route = Route::get($name, $lang);
		// Create a URI with the route and convert it to a URL
		return URL::site($route->uri($params), $protocol, $route->i18n_lang());
	}

	/**
	 * Shortcut for Route::url with lang param at second instead fourth place
	 * 
	 * @param   string  route name
	 * @param	string	language
	 * @param   array	URI parameters
	 * @param   mixed   protocol string or boolean, adds protocol and domain
	 * @return	string
	 * @uses	Route::url
	 */
	public static function url_i18n($name, $lang, array $params = NULL, $protocol = NULL)
	{
		return Route::url($name, $params, $protocol, $lang);
	}
	
	/**
	 * Shortcut for Route::url but protocol is set to TRUE
	 * 
	 * @param   string  route name
	 * @param	string	language
	 * @param   array	URI parameters
	 * @param   mixed   protocol string or boolean, adds protocol and domain
	 * @return	string
	 * @uses	Route::url
	 */
	public static function url_protocol($name, array $params = NULL, $protocol = TRUE, $lang = NULL)
	{
		return Route::url($name, $params, $protocol, $lang);
	}

	/**
	 * Stores a named route and returns it. The "action" will always be set to
	 * "index" if it is not defined.
	 *
	 *     Route::set('default', '(<controller>(/<action>(/<id>)))')
	 *         ->defaults(array(
	 *             'controller' => 'welcome',
	 *         ));
	 *
	 * @param   string   route name
	 * @param   string   URI pattern
	 * @param   array    regex patterns for route keys
	 * @return  Route
	 */
	public static function set($name, $uri, array $regex = NULL, $lang = NULL)
	{
		$route = new Route($uri, $regex);

		if ($lang !== NULL)
		{
			$name .= ':i18n['.$lang.']';

			$route->lang($lang);
			$route->i18n_media(TRUE);
			$route->i18n_uri(TRUE);
		}
		else
		{
			$route->lang(I18n::DEFAULT_LANG);
		}

		// If we have this route already, do not rewrite it
		// But return the object so chained method can be called without error
		if (isset(Route::$_routes[$name]))
		{
			return $route;
		}

		return Route::$_routes[$name] = $route;
	}

	/**
	 * Removes a named route.
	 *
	 *     $route = Route::remove('default');
	 *
	 * @param   string  route name
	 * @param	boolean	remove with children
	 * @uses	Route::clear()
	 */
	public static function remove($name, $children = TRUE)
	{
		if ( ! isset(Route::$_routes[$name]))
		{
			return;
		}

		Route::$_routes[$name]->clear($children);

		unset(Route::$_routes[$name]);
	}

	/**
	 * Retrieves a named route.
	 *
	 *     $route = Route::get('default');
	 *
	 * @param   string  route name
	 * @return  Route
	 * @throws  Kohana_Exception
	 */
	public static function get($name, $lang = NULL, $exception = TRUE)
	{
		$route = NULL;

		if ($lang !== FALSE AND I18n::multilang())
		{
			if ($lang === NULL)
			{
				$lang = I18n::lang();
			}

			$i18n_name = $name.':i18n['.$lang.']';

			try
			{
				$route = parent::get($i18n_name);
			}
			catch (Kohana_Exception $e)
			{
				$route = NULL;
			}
		}

		if ($route !== NULL)
		{
			return $route;
		}

		try 
		{
			$route = parent::get($name);
		}
		catch (Kohana_Exception $e)
		{
			// If we should return exception, do so.
			if ($exception)
			{
				throw $e;
			}
			else
			{
				// Set route to FALSE
				$route = FALSE;
			}
		}
		
		return $route;
	}

	/**
	 * Retrieves all named routes.
	 *
	 *     $routes = Route::all();
	 *
	 * @param	string	i18n route filter
	 * @return  array	routes by name
	 */
	public static function all($lang = NULL)
	{
		if ($lang === NULL OR $lang === FALSE)
		{
			return parent::all();
		}

		$searched_langs = array($lang);

		if (I18n::is_default_lang($lang))
		{
			$searched_langs[] = I18n::DEFAULT_LANG;
		}

		$routes = array();

		foreach (Route::$_routes as $name => $route)
		{
			if (in_array($route->lang(), $searched_langs))
			{
				$routes[$name] = $route;
			}
		}

		return $routes;
	}

	/**
	 * @var	string	Route lang used to set I18n::lang() when it is requested.
	 */
	protected $_lang;

	/**
	 * @var	array	Array of copied routes
	 */
	protected $_children = array();

	/**
	 * @var array	Additional config params
	 */
	protected $_config = array();

	/**
	 * Tests if the route matches a given URI. A successful match will return
	 * all of the routed parameters as an array. A failed match will return
	 * boolean FALSE.
	 *
	 *     // Params: controller = users, action = edit, id = 10
	 *     $params = $route->matches('users/edit/10');
	 *
	 * This method should almost always be used within an if/else block:
	 *
	 *     if ($params = $route->matches($uri))
	 *     {
	 *         // Parse the parameters
	 *     }
	 *
	 * @param   string  URI to match
	 * @return  array   on success
	 * @return  FALSE   on failure
	 */
	public function matches($uri)
	{
		$params = parent::matches($uri);

		if (Route::$_do_i18n_match AND I18n::multilang() AND $params !== FALSE)
		{
			// We no have language in uri and this route is i18n and default language.
			// It is home page.
			if ( ! Request::$has_i18n_lang AND I18n::is_default_lang($this->i18n_lang()))
			{
				// Set multilang to true, so redirect will prepend language token
				I18n::lang($this->i18n_lang());
				I18n::multilang(TRUE);
				// This needs to be set otherwise no ended recursion
				Route::$_do_i18n_match = FALSE;

				$request = Request::factory($uri)->redirect($uri);
			}

			if (Request::$has_i18n_lang AND $this->i18n_lang() != I18n::lang())
			{
				return FALSE;
			}

			// This needs to be set otherwise no ended recursion
			Route::$_do_i18n_match = FALSE;
		}

		if ( ! Request::$instance AND $params !== FALSE)
		{
			// Set I18n environment according to route settings
			I18n::lang($this->lang() == I18n::DEFAULT_LANG ? I18n::default_lang() : $this->lang());
			if (I18n::multilang())
			{
				I18n::multilang($this->i18n_uri());
			}
			if (I18n::multilang_media())
			{
				I18n::multilang_media($this->i18n_media());
			}
		}

		return $params;
	}

	/**
	 * Returns uri parameter
	 *
	 * @return	string
	 */
	public function get_uri()
	{
		return $this->_uri;
	}

	/**
	 * Returns default parameter for this route.
	 *
	 * @param	bool	return only default parameter included in uri string
	 * @return	array
	 */
	public function get_defaults($public_only = FALSE)
	{
		if ($public_only)
		{
			$defaults = array();
			
			if (strpos($this->_uri, '<') === FALSE AND strpos($this->_uri, '(') === FALSE)
			{
				// This is a static route, no need to replace anything
				return $defaults;
			}

			foreach ($this->_defaults as $key => $param)
			{
				if (preg_match('#<'.$key.'>#', $this->_uri))
				{
					$defaults[$key] = $param;
				}
			}

			return $defaults;
		}

		return $this->_defaults;
	}

	/**
	 * Returns uri string. As default prepends with protocol.
	 *
	 * @param	mixed	Protocol to use
	 * @return	string
	 */
	public function get_empty_url($protocol = TRUE)
	{
		return URL::site($this->_uri, $protocol, $this->i18n_lang());
	}

	/**
	 * Do copy of actual Route object.
	 *
	 * @param	string $name
	 * @param	string $uri
	 * @param	array $regex
	 * @param	string $lang
	 * @return	Route
	 */
	public function copy($name, $uri = NULL, array $regex = NULL, $lang = NULL)
	{
		if ($uri === NULL)
		{
			$uri = $this->_uri;
		}
		if ($regex === NULL)
		{
			$regex = $this->_regex;
		}

		$route = Route::set($name, $uri, $regex, $lang);

		// Set same defaults
		$route->defaults($this->_defaults);
		// Copy config data
		$route->config($this->_config);

		// Add copied route to children
		$this->_children[] = Route::name($route);

		return $route;
	}

	public function copy_i18n($name, $lang, $uri = NULL, array $regex = NULL)
	{

		return $this->copy($name, $uri, $regex, $lang);
	}

	/**
	 * Create I18n copies of this route and removes default non I18n route.
	 *
	 * @param	mixed	array of languages to create or TRUE to include all available langs
	 * @return	Route
	 */
	public function i18n($langs = TRUE)
	{
		// If multilang not enabled, no i18n copies are created
		if ( ! I18n::multilang())
		{
			return $this;
		}

		if ($langs === TRUE)
		{
			$langs = I18n::available_langs(TRUE);
		}
		elseif( ! is_array($langs))
		{
			$langs = array($langs);
		}

		$name = Route::name($this);

		// Route not found, so return object directly, do not create copies
		if ($name === FALSE)
		{
			return $this;
		}

		$parent = null;

		foreach ($langs as $lang => $args)
		{
			if (is_numeric($lang))
			{
				$lang = $args;
				$args = NULL;
			}

			// Add only enabled langs
			if ( ! I18n::lang_enabled($lang))
			{
				continue;
			}

			// Args is only uri
			if ( ! is_array($args))
			{
				$args = array($args);
			}

			// Prepend lang and name to args
			array_unshift($args, $name, $lang);

			$i18n_copy = call_user_func_array(array($parent ? $parent : $this, 'copy_i18n'), $args);
			// Set i18n attribute
			$i18n_copy->i18n_lang($lang);

			if ($parent === NULL)
			{
				// Copy from this
				$parent = $i18n_copy;
				// But do not set it as parent
				array_pop($this->_children);
			}
		}

		// Fix if no language copy was created, do not remove this and return this
		if ($parent === NULL)
		{
			return $this;
		}

		// Remove this object
		Route::remove($name);
		// Return parent
		return $parent;
	}

	/**
	 * I18n lang used for lang token in uri.
	 *
	 * @param	mixed	NULL for getter, string for setter
	 * @return	mixed	Route as getter, string as setter when set or FALSE
	 */
	public function i18n_lang($lang = NULL)
	{
		if ($lang !== NULL)
		{
			$this->config_set('i18n_lang', $lang);
			return $this;
		}
		return $this->config('i18n_lang', FALSE);
	}

	/**
	 * Tels if add I18n strings to urls in Media::url().
	 *
	 * @param	mixed	NULL for getter, bool for setter
	 * @return	mixed	Route as getter, bool as setter when set or FALSE
	 */
	public function i18n_media($value = NULL)
	{
		if ($value !== NULL)
		{
			$this->config_set('i18n_media', (bool) $value);
			return $this;
		}

		return $this->config('i18n_media', FALSE);
	}

	/**
	 * Tels if add I18n strings to urls in URL::base()
	 *
	 * @param	mixed	NULL for getter, bool for setter
	 * @return	mixed	Route as getter, bool as setter when set or FALSE
	 */
	public function i18n_uri($value = NULL)
	{
		if ($value !== NULL)
		{
			$this->config_set('i18n_uri', (bool) $value);
			return $this;
		}

		return $this->config('i18n_uri', FALSE);
	}

	/**
	 * Tels which lang is set URL::base()
	 *
	 * @param	mixed	NULL for getter, bool for setter
	 * @return	mixed	Route as getter, bool as setter when set or FALSE
	 */
	public function lang($lang = NULL)
	{
		if ($lang !== NULL)
		{
			$this->_lang = $lang;
			return $this;
		}

		return $this->_lang;
	}

	/**
	 * Generates a URI for the current route based on the parameters given.
	 *
	 *     // Using the "default" route: "users/profile/10"
	 *     $route->uri(array(
	 *         'controller' => 'users',
	 *         'action'     => 'profile',
	 *         'id'         => '10'
	 *     ));
	 *
	 * @param   array   URI parameters
	 * @param	string	language
	 * @return  string
	 * @throws  Kohana_Exception
	 * @uses    Route::REGEX_Key
	 */
	public function uri(array $params = NULL, $lang = NULL)
	{
		if (I18n::multilang())
		{
			if ($lang !== NULL AND $lang != $this->lang())
			{
				$route = Route::get(Route::name($this), $lang);

				return $route->uri($params);
			}
		}

		return parent::uri($params);
	}

	/**
	 * Called from Route::remove().
	 *
	 *     // Delete route childrens
	 *     $route->clear();
	 *
	 * @param	boolean	remove children also
	 * @uses    Route::remove()
	 */
	public function clear($children = TRUE)
	{
		if ($children)
		{
			foreach ($this->_children as $name)
			{
				Route::remove($name, $children);
			}
		}
	}

	/**
	 * Set condition when route is active.
	 *
	 *     // Activate route only when not in production
	 *     $route->condition(Kohana::$environment !== Kohana::PRODUCTION);
	 *
	 * @param   boolean		condition for activating route
	 * @param	boolean		remove children also when not active
	 * @return  boolean		condition value
	 * @uses    Route::remove()
	 */
	public function condition($condition, $children = TRUE)
	{
		if ( ! $condition)
		{
			Route::remove(Route::name($this), $children);
		}

		return $condition;
	}

	/**
	 * Set position in route array.
	 *
	 *     $route->position(TRUE); // Set as first
	 *	   $route->position($position); // Set at specific position
	 *     $route->position(FALSE); // Set as last
	 *
	 * @param   mixed	TRUE (first), FALSE (last), int (specific)
	 * @return  Route
	 */
	public function position($position)
	{
		if ($position === TRUE)
		{
			$position = 0;
		}
		elseif ($position === FALSE)
		{
			$position = PHP_INT_MAX;
		}

		$this->config_set(Route::$route_position_config_key, $position);

		return $this;
	}

	/**
	 * Set environment when route is active.
	 *
	 *     // Activate route only when in production
	 *     $route->environment(Kohana::PRODUCTION);
	 *
	 *     // Activate route only when not in production
	 *     $route->environment(Kohana::PRODUCTION, TRUE);
	 *
	 * @param	string		environment
	 * @param   boolean		environment is not equal
	 * @param	boolean		remove children also when not active
	 * @return  boolean		environment test value
	 * @uses    Route::condition()
	 */
	public function environment($environment, $reverse = FALSE, $children = TRUE)
	{
		if ($reverse)
		{
			return $this->condition(Kohana::$environment !== $environment, $children);
		}
		else
		{
			return $this->condition(Kohana::$environment === $environment, $children);
		}
	}

	/**
	 * Set or get config params array or get specific key from array
	 *
	 * @param	mixed	config params array to set or key to get or NULL to get all config
	 * @param	mixed	default value if settings
	 * @return	mixed	Route object when setting config or config value
	 */
	public function config($config = NULL, $default = NULL, $overwrite = FALSE)
	{
		if ($config === NULL)
		{
			return $this->_config;
		}
		elseif ( ! is_array($config))
		{
			return Arr::path($this->_config, $config, $default);
		}

		if ($overwrite)
		{
			$this->_config = $config;
		}
		else
		{
			$this->_config = Arr::merge($this->_config, $config);
		}

		return $this;
	}

	/**
	 * Set config param
	 *
	 * @param	mixed	config key to set
	 * @param	mixed	config value
	 * @return	mixed	Route object
	 */
	public function config_set($key = NULL, $value = NULL)
	{
		$this->_config[$key] = $value;
		
		return $this;
	}

} // End Core_Route
