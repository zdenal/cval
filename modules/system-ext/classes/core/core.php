<?php defined('SYSPATH') or die('No direct script access.');

class Core_Core extends Kohana_Core {

	/**
	 * Override - check string type if string has to be returned
	 * Get a message from a file. Messages are arbitary strings that are stored
	 * in the messages/ directory and reference by a key. Translation is not
	 * performed on the returned values.
	 *
	 *     // Get "username" from messages/text.php
	 *     $username = Kohana::message('text', 'username');
	 *
	 * @param   string  file name
	 * @param   string  key path to get
	 * @param   mixed   default value if the path does not exist
	 * @return  string  message string for the given path
	 * @return  array   complete message list, when no path is specified
	 * @uses    Arr::merge
	 * @uses    Arr::path
	 */
	public static function message($file, $path = NULL, $default = NULL)
	{
		$_ret = parent::message($file, $path, $default);
		
		if ($path !== NULL AND ! is_string($_ret))
			$_ret = NULL;

		return $_ret;
	}

	/**
	 * Output an HTML string of debugging information about any number of
	 * variables, each wrapped in a "pre" tag and call exit php function after it:
	 *
	 *     // Displays the type and value of each variable
	 *     Kohana::debug_exit($foo, $bar, $baz);
	 *
	 * @param   mixed   variable to debug
	 * @param   ...
	 * @return  string
	 * @uses	Kohana::debug()
	 */
	public static function debug_exit()
	{
		// Get all passed variables
		$variables = func_get_args();

		echo call_user_func_array(array('Kohana', 'debug'), $variables);

		exit();
	}

	/**
	 * Returns the the currently active include paths, including the
	 * application and system paths.
	 *
	 * @param	array	list of paths
	 * @return  array
	 */
	public static function include_paths(array $paths = NULL)
	{
		if ($paths !== NULL)
		{
			// Changing paths
			Kohana::$_paths = $paths;
		}

		return Kohana::$_paths;
	}
	
	/**
	 * Returns a path of module by name
	 *
	 * @param   string  module name
	 * @return  string  module path
	 */
	public static function module_path($module_name)
	{
		if (isset(Kohana::$_modules[$module_name]))
		{
			return Kohana::$_modules[$module_name];	
		}
		return NULL;
	}

	/**
	 * Changes the currently enabled modules. Module paths may be relative
	 * or absolute, but must point to a directory:
	 *
	 *     Kohana::modules(array('modules/foo', MODPATH.'bar'));
	 *
	 * @param   array  list of module paths
	 * @return  array  enabled modules
	 */
	public static function modules(array $modules = NULL)
	{
		if ($modules === NULL)
		{
			// Not changing modules, just return the current set
			return Kohana::$_modules;
		}

		// Start a new list of include paths, APPPATH first
		$paths = array(APPPATH);

		foreach ($modules as $name => $path)
		{
			if (is_dir($path))
			{
				// Add the module to include paths
				$paths[] = $modules[$name] = realpath($path).DIRECTORY_SEPARATOR;
			}
			else
			{
				// This module is invalid, remove it
				unset($modules[$name]);
			}
		}

		// Finish the include paths by adding SYSPATH
		$paths[] = SYSPATH;

		// Set the new include paths
		Kohana::$_paths = $paths;

		// Set the current module list
		Kohana::$_modules = $modules;

		// Try to load init file from apppath
		$init = APPPATH.'init'.EXT;

		if (is_file($init))
		{
			// Include the app initialization file once
			require_once $init;
		}

		foreach (Kohana::$_modules as $path)
		{
			$init = $path.'init'.EXT;

			if (is_file($init))
			{
				// Include the module initialization file once
				require_once $init;
			}
		}

		return Kohana::$_modules;
	}

	/**
	 * Iterate over all paths and require file load.EXT if exists.
	 * There can be set forexample routes, because so we can modify modules routes
	 * in app folder.
	 *
	 *     Kohana::load_modules();
	 *
	 */
	public static function load_modules()
	{
		foreach (Kohana::$_paths as $path)
		{
			$load = $path.'load'.EXT;

			if (is_file($load))
			{
				// Include the module load file once
				require_once $load;
			}
		}
	}

	/**
	 * Returns an HTML string of debugging information about any number of
	 * variables, each wrapped in a "pre" tag:
	 *
	 *     // Displays the type and value of each variable
	 *     echo Kohana::debug($foo, $bar, $baz);
	 *
	 * @param   mixed   variable to debug
	 * @param   ...
	 * @return  string
	 */
	public static function debug_custom()
	{
		if (func_num_args() === 0)
			return;

		// Get all passed variables
		$variables = func_get_args();

		$output = array();
		foreach ($variables as $var)
		{
			$output[] = Kohana::_dump_custom($var, 1024);
		}

		return '<pre class="debug">'.implode("\n", $output).'</pre>';
	}

	/**
	 * Returns an HTML string of information about a single variable.
	 *
	 * Borrows heavily on concepts from the Debug class of [Nette](http://nettephp.com/).
	 *
	 * @param   mixed    variable to dump
	 * @param   integer  maximum length of strings
	 * @return  string
	 */
	public static function dump_custom($value, $length = 128)
	{
		return Kohana::_dump_custom($value, $length);
	}

	/**
	 * Helper for Kohana::dump_custom(), handles recursion in arrays and objects.
	 *
	 * @param   mixed    variable to dump
	 * @param   integer  maximum length of strings
	 * @param   integer  recursion level (internal)
	 * @return  string
	 */
	protected static function _dump_custom( & $var, $length = 128, $level = 0)
	{
		if ($var === NULL)
		{
			return '<small>NULL</small>';
		}
		elseif (is_bool($var))
		{
			return '<small>bool</small> '.($var ? 'TRUE' : 'FALSE');
		}
		elseif (is_float($var))
		{
			return '<small>float</small> '.$var;
		}
		elseif (is_resource($var))
		{
			if (($type = get_resource_type($var)) === 'stream' AND $meta = stream_get_meta_data($var))
			{
				$meta = stream_get_meta_data($var);

				if (isset($meta['uri']))
				{
					$file = $meta['uri'];

					if (function_exists('stream_is_local'))
					{
						// Only exists on PHP >= 5.2.4
						if (stream_is_local($file))
						{
							$file = Kohana::debug_path($file);
						}
					}

					return '<small>resource</small><span>('.$type.')</span> '.htmlspecialchars($file, ENT_NOQUOTES, Kohana::$charset);
				}
			}
			else
			{
				return '<small>resource</small><span>('.$type.')</span>';
			}
		}
		elseif (is_string($var))
		{
			// Clean invalid multibyte characters. iconv is only invoked
			// if there are non ASCII characters in the string, so this
			// isn't too much of a hit.
			$var = UTF8::clean($var);

			if (UTF8::strlen($var) > $length)
			{
				// Encode the truncated string
				$str = htmlspecialchars(UTF8::substr($var, 0, $length), ENT_NOQUOTES, Kohana::$charset).'&nbsp;&hellip;';
			}
			else
			{
				// Encode the string
				$str = htmlspecialchars($var, ENT_NOQUOTES, Kohana::$charset);
			}

			return '<small>string</small><span>('.strlen($var).')</span> "'.$str.'"';
		}
		elseif (is_array($var))
		{
			$output = array();

			// Indentation for this variable
			$space = str_repeat($s = '    ', $level);

			static $marker;

			if ($marker === NULL)
			{
				// Make a unique marker
				$marker = uniqid("\x00");
			}

			if (empty($var))
			{
				// Do nothing
			}
			elseif (isset($var[$marker]))
			{
				$output[] = "(\n$space$s*RECURSION*\n$space)";
			}
			elseif ($level < 5)
			{
				$output[] = "<span>(";

				$var[$marker] = TRUE;
				foreach ($var as $key => & $val)
				{
					if ($key === $marker) continue;
					if ( ! is_int($key))
					{
						$key = '"'.htmlspecialchars($key, ENT_NOQUOTES, self::$charset).'"';
					}

					$output[] = "$space$s$key => ".Kohana::_dump_custom($val, $length, $level + 1);
				}
				unset($var[$marker]);

				$output[] = "$space)</span>";
			}
			else
			{
				// Depth too great
				$output[] = "(\n$space$s...\n$space)";
			}

			return '<small>array</small><span>('.count($var).')</span> '.implode("\n", $output);
		}
		elseif (is_object($var))
		{
			if (is_callable(array($var, 'dump')))
			{
				$dump = call_user_func(array($var, 'dump'));

				return Kohana::_dump_custom($dump, $length, $level);
			}

			// Copy the object as an array
			$array = (array) $var;

			$output = array();

			// Indentation for this variable
			$space = str_repeat($s = '    ', $level);

			$hash = spl_object_hash($var);

			// Objects that are being dumped
			static $objects = array();

			if (empty($var))
			{
				// Do nothing
			}
			elseif (isset($objects[$hash]))
			{
				$output[] = "{\n$space$s*RECURSION*\n$space}";
			}
			elseif ($level < 10)
			{
				$output[] = "<code>{";

				$objects[$hash] = TRUE;
				foreach ($array as $key => & $val)
				{
					if ($key[0] === "\x00")
					{
						// Determine if the access is protected or protected
						$access = '<small>'.($key[1] === '*' ? 'protected' : 'private').'</small>';

						// Remove the access level from the variable name
						$key = substr($key, strrpos($key, "\x00") + 1);
					}
					else
					{
						$access = '<small>public</small>';
					}

					$output[] = "$space$s$access $key => ".Kohana::_dump_custom($val, $length, $level + 1);
				}
				unset($objects[$hash]);

				$output[] = "$space}</code>";
			}
			else
			{
				// Depth too great
				$output[] = "{\n$space$s...\n$space}";
			}

			return '<small>object</small> <span>'.get_class($var).'('.count($array).')</span> '.implode("\n", $output);
		}
		else
		{
			return '<small>'.gettype($var).'</small> '.htmlspecialchars(print_r($var, TRUE), ENT_NOQUOTES, Kohana::$charset);
		}
	}
	
	/**
	 * Returns maximum int size for kohana instance. Can be specified in config. 
	 * 
	 * @param	bool	Defaults FALSE returns max int value, TRUE max int length
	 * @return	int		 
	 */
	public static function max_int($max_length = FALSE)
	{
		$bits = Kohana::config('environment.bits');
		
		if ($bits == 64)
		{
			$max = $max_length ? 19 : (int) 9223372036854775807;
		}
		else
		{
			$max = $max_length ? 10 : (int) 2147483647;
		}
		
		return $max;
	}
	
	/**
	 * Returns maximum negative int size for kohana instance. 
	 * Can be specified in config. 
	 * 
	 * @param	bool	Defaults FALSE returns max negative int value, 
	 *					TRUE max negative int length
	 * @return	int		 
	 */
	public static function max_int_negative($max_length = FALSE)
	{
		$max_int = Kohana::max_int($max_length);
		
		return $max_length ? ($max_int + 1) : ($max_int * -1);
	}
}
