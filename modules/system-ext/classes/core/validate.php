<?php defined('SYSPATH') or die('No direct script access.');

class Core_Validate extends Kohana_Validate {

	/**
	 * Tests if a number is greater or equal than.
	 *
	 * @param   string   number to check
	 * @param   integer  minimum value
	 * @return  boolean
	 */
	public static function min($number, $min, $equal = TRUE)
	{
		return $equal ? ($number >= $min) : ($number > $min);
	}
	
	/**
	 * Tests if a number is lower or equal than.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public static function max($number, $max, $equal = TRUE)
	{
		return $equal ? ($number <= $max) : ($number < $max);
	}
	
	/**
	 * Tests if a number is greater than.
	 *
	 * @param   string   number to check
	 * @param   integer  minimum value
	 * @return  boolean
	 */
	public static function min_not_equal($number, $min)
	{
		return Validate::min($number, $min, FALSE);
	}
	
	/**
	 * Tests if a number is lower than.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public static function max_not_equal($number, $max)
	{
		return Validate::max($number, $max, FALSE);
	}
	
	/**
	 * Tests if a number is greater than value in specified field.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public function greater($number, $field, $equal = FALSE)
	{
		return $equal ? ($number >= $this[$field]) : ($number > $this[$field]);
	}
	
	/**
	 * Tests if a number is less than value in specified field.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public function less($number, $field, $equal = FALSE)
	{
		return $equal ? ($number <= $this[$field]) : ($number < $this[$field]);
	}

	/**
	 * Tests if a number is greater or equal than value in specified field.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public function greater_equal($number, $field)
	{
		return $this->greater($number, $field, TRUE);
	}

	/**
	 * Tests if a number is less or equal than value in specified field.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public function less_equal($number, $field, $equal = FALSE)
	{
		return $this->less($number, $field, TRUE);
	}
	
	/**
	 * Tests if a number is same as value in specified field.
	 *
	 * @param   string   number to check
	 * @param   integer  maximum value
	 * @return  boolean
	 */
	public static function equal($number, $field)
	{
		return ($number == $this[$field]);
	}

	/**
	 * Tests if a string is a valid date string according specific format.
	 *
	 * @param   string   date to check
	 * @return  boolean
	 */
	public static function date_format($str, $format = 'r')
	{
		return (DateTime::createFromFormat($format, $str) !== FALSE);
	}

	/**
	 * Tests if a string is a valid timestamp.
	 *
	 * @param   string   date to check
	 * @return  boolean
	 */
	public static function timestamp($str)
	{
		$date = new DateTime();

		return ($date->setTimestamp($str) !== FALSE);
	}

	/**
	 * Check an email addresses for correct format.
	 *
	 * @link  http://www.iamcal.com/publish/articles/php/parsing_email/
	 * @link  http://www.w3.org/Protocols/rfc822/
	 *
	 * @param   string   email addresses
	 * @param   string   email addresses delimiter
	 * @param   boolean  strict RFC compatibility
	 * @return  boolean
	 * @uses	Validate::email()
	 */
	public static function emails($emails, $delimiter = ',', $strict = FALSE)
	{
		foreach (explode($delimiter, $emails) as $email)
		{
			$email = trim($email);
			
			if ( ! strlen($email) OR Validate::email($email, $strict) === FALSE)
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	/**
	 * Checks if a phone number is valid for CZE.
	 *
	 * @param   string   phone number to check
	 * @param	mixed	 with international prefix, TRUE yes, FALSE no, NULL yes/no
	 * @return  boolean
	 */
	public static function phone_cs($number, $prefix = NULL)
	{
		if ($prefix === NULL)
		{
			$prefix = '(\+420)? ?';
		}
		elseif ($prefix)
		{
			$prefix = '\+420 ?';
		}
		else
		{
			$prefix = '';
		}

		return (bool) preg_match('/^'.$prefix.'[0-9]{3} ?[0-9]{3} ?[0-9]{3}$/D', $number);
	}

	/**
	 * Checks if a phone number is valid for SVK.
	 *
	 * @param   string   phone number to check
	 * @param	mixed	 with international prefix, TRUE yes, FALSE no, NULL yes/no
	 * @return  boolean
	 */
	public static function phone_sk($number, $prefix = NULL)
	{
		if ($prefix === NULL)
		{
			$prefix = '(\+421)? ?';
		}
		elseif ($prefix)
		{
			$prefix = '\+421 ?';
		}
		else
		{
			$prefix = '';
		}

		return (bool) preg_match('/^'.$prefix.'[0-9]{3} ?[0-9]{3} ?[0-9]{3}$/D', $number);
	}

	/**
	 * Overwrites or appends rules to a field. Each rule will be executed once.
	 * All rules must be string names of functions method names.
	 *
	 *     // The "username" must not be empty and have a minimum length of 4
	 *     $validation->rule('username', 'not_empty')
	 *                ->rule('username', 'min_length', array(4));
	 *
	 * @param   string  field name
	 * @param   string  function or static method name
	 * @param   array   extra parameters for the rule
	 * @return  $this
	 */
	public function rule($field, $rule, array $params = NULL)
	{
		parent::rule($field, $rule, $params);

		if (in_array($rule, array('greater', 'less', 'greater_equal', 'less_equal')) AND ! isset($this->_labels[$params[0]]))
		{
			$match_field = $params[0];
			$this->_labels[$match_field] = preg_replace('/[^\pL]+/u', ' ', $match_field);
		}

		return $this;
	}

	/**
	 * Returns the error messages. If no file is specified, the error message
	 * will be the name of the rule that failed. When a file is specified, the
	 * message will be loaded from "field/rule", or if no rule-specific message
	 * exists, "field/default" will be used. If neither is set, the returned
	 * message will be "file/field/rule".
	 *
	 * By default all messages are translated using the default language.
	 * A string can be used as the second parameter to specified the language
	 * that the message was written in.
	 *
	 *     // Get errors from messages/forms/login.php
	 *     $errors = $validate->errors('forms/login');
	 *
	 * @uses    Kohana::message
	 * @param   string  file to load error messages from
	 * @param   mixed   translate the message
	 * @param	bool	ucfirst message
	 * @return  array
	 */
	public function errors($file = NULL, $translate = TRUE, $ucfirst = FALSE)
	{
		if ($file === NULL)
		{
			// Return the error list
			return $this->_errors;
		}

		$messages = parent::errors($file, $translate);

		if ($ucfirst === TRUE)
		{
			foreach ($messages as $field => $message)
			{
				$messages[$field] = utf8::ucfirst($message);
			}
		}

		return $messages;
	}

	/**
	 * Works exactly the same as Validate::errors() but for each field
	 * returns array with error name and message instead of message only.
	 *
	 *     // Get errors from messages/forms/login.php
	 *     $errors = $validate->detailed_errors('forms/login');
	 *
	 * @uses    Kohana::message
	 * @param   string  file to load error messages from
	 * @param   mixed   translate the message
	 * @param	bool	ucfirst message
	 * @return  array
	 */
	public function detailed_errors($file = NULL, $translate = TRUE, $ucfirst = FALSE)
	{
		if ($file === NULL)
		{
			// Return the error list
			return $this->_errors;
		}

		$messages = $this->errors($file, $translate, $ucfirst);

		foreach ($messages as $field => $message)
		{
			$messages[$field] = array(
				'name' => $this->_errors[$field][0],
				'message' => $message
			);
		}

		return $messages;
	}

	/**
	 * Executes all validation filters, rules, and callbacks. This should
	 * typically be called within an if/else block.
	 *
	 *     if ($validation->check())
	 *     {
	 *          // The data is valid, do something here
	 *     }
	 *
	 * @param   boolean   allow empty array?
	 * @param	boolean	  throw Validate_Exception on errors
	 * @return  boolean
	 */
	public function check($allow_empty = FALSE, $exception = FALSE)
	{
		$success = parent::check($allow_empty);

		// Throw validate exception on errors and exception allowed
		if ( ! $success AND $exception)
		{
			throw new Validate_Exception($this);
		}

		return $success;
	}
	
}