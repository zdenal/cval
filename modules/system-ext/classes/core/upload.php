<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_Upload extends Kohana_Upload {

	/**
	 * Save an uploaded file to a new location. If no filename is provided,
	 * the original filename will be used, with a unique prefix added.
	 *
	 * This method should be used after validating the $_FILES array:
	 *
	 *     if ($array->check())
	 *     {
	 *         // Upload is valid, save it
	 *         Upload::save($_FILES['file']);
	 *     }
	 *
	 * @param   array    uploaded file data
	 * @param   string   new filename
	 * @param   string   new directory
	 * @param   integer  chmod mask
	 * @return  string   on success when verbose FALSE, full path to new file
	 * @return  array	 on success when verbose TRUE, array of save info
	 * @return  FALSE    on failure
	 */
	public static function save(array $file, $filename = NULL, $directory = NULL, $chmod = 0644, $verbose = FALSE)
	{
		if ( ! isset($file['tmp_name']) OR ! is_uploaded_file($file['tmp_name']))
		{
			// Ignore corrupted uploads
			return FALSE;
		}

		if ($filename === NULL)
		{
			// Use the default filename, with a timestamp pre-pended
			$filename = uniqid().$file['name'];
		}

		if (Upload::$remove_spaces === TRUE)
		{
			// Remove spaces from the filename
			$filename = preg_replace('/\s+/', '_', $filename);
		}

		if ($directory === NULL)
		{
			// Use the pre-configured upload directory
			$directory = Upload::$default_directory;
		}

		if ( ! is_dir($directory) OR ! is_writable(realpath($directory)))
		{
			throw new Kohana_Exception('Directory :dir must be writable',
				array(':dir' => Kohana::debug_path($directory)));
		}

		// Backup only filename part without path
		$filename_only = $filename;
		// Clear directory
		$directory = realpath($directory).DIRECTORY_SEPARATOR;

		// Make the filename into a complete path
		$filename = $directory.$filename_only;

		if (move_uploaded_file($file['tmp_name'], $filename))
		{
			if ($chmod !== FALSE)
			{
				// Set permissions on filename
				chmod($filename, $chmod);
			}

			// We are verbose
			if ($verbose)
			{
				// Return more data
				return array(
					'path' => $filename,
					'directory' => $directory,
					'filename' => $filename_only,
					'original_filename' => $file['name']
				);
			}
			else
			{
				// Return new file path
				return $filename;
			}
		}

		return FALSE;
	}

	/**
	 * Extended version of Upload::save. Set verbose param to TRUE.
	 *
	 * @param   array    uploaded file data
	 * @param   string   new filename
	 * @param   string   new directory
	 * @param   integer  chmod mask
	 * @return  array	 on success, array of save info
	 * @return  FALSE    on failure
	 */
	public static function save_verbose(array $file, $filename = NULL, $directory = NULL, $chmod = 0644)
	{
		return Upload::save($file, $filename, $directory, $chmod, TRUE);
	}

} // End Core_Upload