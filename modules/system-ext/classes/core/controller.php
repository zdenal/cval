<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Just change comments that request is Request not Object.
 * For better Netbeans navigation.
 */
abstract class Core_Controller extends Kohana_Controller {

	/**
	 * @var  Request  Request that created the controller
	 */
	public $request;

	/**
	 * Creates a new controller instance. Each controller must be constructed
	 * with the request object that created it.
	 *
	 * @param   Request  Request that created the controller
	 * @return  void
	 */
	public function __construct(Kohana_Request $request)
	{
		parent::__construct($request);
	}

} // End Core_Controller
