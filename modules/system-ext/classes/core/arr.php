<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_Arr extends Kohana_Arr {

	/**
	 * Gets a value from an array using a dot separated path.
	 *
	 *     // Get the value of $array['foo']['bar']
	 *     $value = Arr::path($array, 'foo.bar');
	 *
	 * Using a wildcard "*" will search intermediate arrays and return an array.
	 *
	 *     // Get the values of "color" in theme
	 *     $colors = Arr::path($array, 'theme.*.color');
	 *
	 * @param   array   array to search
	 * @param   string  key path, delimiter separated
	 * @param   mixed   default value if the path is not set
	 * @param   string  key path delimiter
	 * @return  mixed
	 */
	public static function path($array, $path, $default = NULL, $delimiter = NULL)
	{
		// Fix when array is NULL, in parent there are used array expecting methods
		if ($array === NULL)
		{
			$array = array();
		}
		
		return parent::path($array, $path, $default, $delimiter);
	}
	
	/**
	 * Removes multiple keys from an array. Works like multiple unset but also
	 * not existed keys can be removed without error.
	 *
	 *     // Remove the values "username", "password" from $_POST
	 *     Arr::remove($_POST, array('username', 'password'));
	 *
	 * @param   array   array to remove keys from
	 * @param   array   list of key names
	 * @return  array
	 */
	public static function remove( array & $array, array $keys)
	{
		foreach ($keys as $key)
		{
			if (array_key_exists($key, $array))
			{
				unset($array[$key]);
			}
		}

		return $array;
	}

	/**
	 * Retrieves multiple keys from an array. If the key does not exist in the
	 * array, it is not added to returned array.
	 *
	 *     // Get the values "username", "password" from $_POST
	 *     $auth = Arr::extract($_POST, array('username', 'password'));
	 *
	 * @param   array   array to extract keys from
	 * @param   array   list of key names
	 * @return  array
	 */
	public static function extract_existed($array, array $keys)
	{
		$found = array();
		foreach ($keys as $key)
		{
			if (isset($array[$key]))
			{
				$found[$key] = $array[$key];
			}
		}

		return $found;
	}

	/**
	 * Gets a value from an array using a dot separated path.
	 *
	 *     // Get the value of $array['foo']['bar']
	 *     $value = Arr::path($array, 'foo.bar');
	 *
	 * Using a wildcard "*" will search intermediate arrays and return an array.
	 *
	 *     // Get the values of "color" in theme
	 *     $colors = Arr::path($array, 'theme.*.color');
	 *
	 * @param   array   array to search
	 * @param   string  key path, delimiter separated
	 * @param   mixed   default value if the path is not set
	 * @param   string  key path delimiter
	 * @return  mixed
	 */
	public static function paths_to_array($paths, $delimiter = NULL)
	{
		// TODO - create this functionality
		if ($delimiter === NULL)
		{
			// Use the default delimiter
			$delimiter = Arr::$delimiter;
		}

		// Construct empty array
		$array = array();

		foreach ($paths as $path => $value)
		{

		}
	}
	
	/**
	 * Return all of the items in the array. Keeps same indexes if key param
	 * is not set.
	 *
	 *     // Complete array
	 *     $array = Arr::remake($array);
	 *
	 *     // Complete array but with indexes from "id" param in array
	 *	   // Simple reindexing by array key
	 *     $array = Arr::remake($array, 'id');
	 *
	 *     // Array of "id" => "name"
	 *     $array = Arr::remake($array, 'id', 'name');
	 * 
	 *	   // Array with same indexes but only with "name" value
	 *     $array = Arr::remake($array, NULL, 'name');	
	 * 
	 *	   // Array of "id" => "name", but with default value "Not found" where
	 *	   // "name" key not exists
	 *     $array = Arr::remake($array, 'id', 'name', 'Not found');
	 * 
	 *     // Array with same indexes but only with "name" value, but with 
	 *	   // default value "Not found" where "name" key not exists
	 *     $array = Arr::remake($array, NULL, 'name', 'Not found');
	 *
	 * @param   string  key for associative keys
	 * @param   string  key for values
	 * @param   string  default value for in key value not exists
	 * @return  array
	 */
	public static function remake(array $array, $key = NULL, $value = NULL, $default = NULL)
	{
		$remake = array();

		if ($key === NULL AND $value === NULL)
		{
			// Nothing to change
			$remake = $array;
		}
		elseif ($key === NULL)
		{
			// Same indexes with value
			foreach ($array as $index => $item)
			{
				$remake[$index] = Arr::get($item, $value, $default);
			}
		}
		elseif ($value === NULL)
		{
			// Indexes from key with complete item
			foreach ($array as $item)
			{
				$remake[$item[$key]] = $item;
			}
		}
		else
		{
			// Indexes from key with value
			foreach ($array as $item)
			{
				$remake[$item[$key]] = Arr::get($item, $value, $default);
			}
		}
		
		return $remake;
	}

} // End Core_Arr