<?php defined('SYSPATH') or die('No direct script access.');

class Core_Method {

	/**
	 * Call static or nonstatic object method or function with Reflection class
	 * 
	 * @param	mixed	string - object with static method separated by :: or function name
	 * 					array - object and method
	 * @param	array	Arguments passed to method/function
	 * @return	mixed	Returned value of method/function
	 */
	public static function call($callback, $arguments = array())
	{
		if ( ! is_array($callback) AND strpos($callback, '::') !== FALSE)
		{
			// Make the static callback into an array
			$callback = explode('::', $callback, 2);
		}

		if (is_array($callback))
		{
			// Separate the object and method
			list ($object, $method) = $callback;

			// Use a method in the given object
			$method = new ReflectionMethod($object, $method);

			if ( ! is_object($object))
			{
				// The object must be NULL for static calls
				$object = NULL;
			}

			// Call $object->$method($argument1, $argument2, ...) with Reflection
			return $method->invokeArgs($object, $arguments);
		}
		else
		{
			// Use a function call
			$function = new ReflectionFunction($callback);

			// Call $function($argument1, $argument2, ...) with Reflection
			return $function->invokeArgs($arguments);
		}	
	}
	
}