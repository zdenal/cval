<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_Object
{

	/**
	 * Convert an object to an array
	 *
	 * @param	object	object to convert
	 * @return	array	converted object to array
	 */
	public static function as_array($object)
	{
		if ( ! is_object($object) AND ! is_array($object))
		{
			return $object;
		}
		if (is_object($object))
		{
			$object = get_object_vars($object);
		}
		return Arr::map(array('Object', 'as_array'), $object);
	}

} // End object
