<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_URL extends Kohana_URL {

	/**
	 * Gets the base URL to the application. To include the current protocol,
	 * use TRUE. To specify a protocol, provide the protocol as a string.
	 * If a protocol is used, a complete URL will be generated using the
	 * `$_SERVER['HTTP_HOST']` variable.
	 *
	 *     // Absolute relative, no host or protocol
	 *     echo URL::base();
	 *
	 *     // Complete relative, with host and protocol
	 *     echo URL::base(TRUE, TRUE);
	 *
	 *     // Complete relative, with host and "https" protocol
	 *     echo URL::base(TRUE, 'https');
	 *
	 * @param   boolean  add index file to URL?
	 * @param   mixed    protocol string or boolean, add protocol and domain?
	 * @return  string
	 * @uses    Kohana::$index_file
	 * @uses    Request::$protocol
	 */
	public static function base($index = FALSE, $protocol = FALSE, $lang = NULL)
	{
		$base_url = parent::base($index, $protocol);

		if ($lang !== FALSE AND (I18n::multilang() OR $lang !== NULL))
		{
			if ($lang === NULL)
			{
				$lang = I18n::lang();
			}

			$base_url .= $lang.'/';
		}

		return $base_url;
	}

	/**
	 * Fetches an absolute site URL based on a URI segment.
	 *
	 *     echo URL::site('foo/bar');
	 *
	 * @param   string  site URI to convert
	 * @param   mixed   protocol string or boolean, add protocol and domain?
	 * @return  string
	 * @uses    URL::base
	 */
	public static function site($uri = '', $protocol = FALSE, $lang = NULL)
	{
		// Chop off possible scheme, host, port, user and pass parts
		$path = preg_replace('~^[-a-z0-9+.]++://[^/]++/?~', '', trim($uri, '/'));

		if ( ! UTF8::is_ascii($path))
		{
			// Encode all non-ASCII characters, as per RFC 1738
			$path = preg_replace('~([^/]+)~e', 'rawurlencode("$1")', $path);
		}

		// Concat the URL
		return URL::base(TRUE, $protocol, $lang).$path;
	}

	/**
	 * Convert a phrase to a URL-safe ascii title.
	 *
	 *     echo URL::title_ascii('My Blog Post'); // "my-blog-post"
	 *
	 * @param   string   phrase to convert
	 * @param   string   word separator (any single character)
	 * @return  string
	 * @uses    URL::title
	 */
	public static function title_ascii($title, $separator = '-')
	{
		return URL::title($title, $separator, TRUE);
	}

	/**
	 * Test if given URL is from this site.
	 *
	 *     echo URL::is_site(Request::$referrer);
	 *
	 * @param   string  URL to test
	 * @param   mixed   protocol string or boolean, add protocol and domain?
	 * @return  bool
	 * @uses    URL::base
	 */
	public static function is_site($url = '', $protocol = FALSE, $lang = NULL)
	{
		return preg_match('~^'.rtrim(URL::base(TRUE, $protocol, $lang), '/').'.*~u', $url);
	}

	/**
	 * Remove URL::base portion from given URL.
	 *
	 *     echo URL::unsite(Request::$referrer);
	 *
	 * @param   string  URL to unsite
	 * @param   mixed   protocol string or boolean, add protocol and domain?
	 * @return  string
	 * @uses    URL::base
	 */
	public static function unsite($url = '', $protocol = FALSE, $lang = NULL)
	{
		return preg_replace('~^'.rtrim(URL::base(TRUE, $protocol, $lang), '/').'/?~u', '', $url);
	}

	/**
	 * Check if URL is absolute, ie. has protocol or starts with slash.
	 *
	 *     echo URL::is_absolute($url);
	 *
	 * @param   string  URL to check
	 * @return  bool
	 */
	public static function is_absolute($url)
	{
		return parse_url($url, PHP_URL_SCHEME) OR strpos($url, '/') === 0;
	}

} // End Core_URL