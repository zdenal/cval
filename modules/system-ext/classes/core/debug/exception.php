<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @package    Core
 * @category   Exceptions
 * @author     Zdenal
 */
class Core_Debug_Exception extends Kohana_Exception {

	/**
	 * Creates a new debug exception of debugging information about any number of
	 * variables:
	 *
	 *	   throw Debug_Exception::create($foo, $bar, $baz);
	 *
	 * @param   mixed   variable to debug
	 * @param   ...
	 * @return  Debug_Exception
	 * @uses	Kohana::debug()
	 */
	public static function create()
	{
		// Get all passed variables
		$variables = func_get_args();

		// Create debug info from vars
		$debug = call_user_func_array(array('Kohana', 'debug'), $variables);

		// Create exception
		return new Debug_Exception(NULL, NULL, NULL, $debug);
	}

	public $debug;

	/**
	 * Creates a new debug exception.
	 *
	 *     throw new Debug_Exception('Something went terrible wrong, :user',
	 *         array(':user' => $user));
	 *
	 * @param   string   error message
	 * @param   array    translation variables
	 * @param   integer  the exception code
	 * @param	string	 string to debug
	 * @return  void
	 * @see		Debug_Exception::create()
	 */
	public function __construct($message = NULL, array $variables = NULL, $code = NULL, $debug = NULL)
	{
		$this->debug = $debug;

		if ($code === NULL)
		{
			$code = 555;
		}

		if ($message === NULL)
		{
			$message = isset(Request::$messages[$code]) ? Request::$messages[$code] : 'Debug Error';
		}

		// Pass the message to the parent
		parent::__construct($message, $variables, $code);
	}

} // End Core_Debug_Exception
