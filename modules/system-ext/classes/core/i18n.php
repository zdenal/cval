<?php defined('SYSPATH') or die('No direct script access.');

class Core_I18n extends Kohana_I18n {

	const DEFAULT_LANG = 'i18nDEF';

	public static $countries;

	public static $multilang;

	public static $multilang_media;

	protected static $_available_langs;
	protected static $_enabled_langs;

	public static function config($key = NULL)
	{
		if ($key)
		{
			return Kohana::config('i18n.'.$key);
		}
		return Kohana::config('i18n');
	}

	public static function lang_label($lang = NULL)
	{
		if ($lang === NULL)
		{
			// Normalize the language
			$lang = I18n::lang();
		}

		return Arr::get(I18n::enabled_langs(), $lang);
	}

	public static function default_lang()
	{
		return I18n::config('default_lang');
	}

	public static function enabled_langs($keys_only = FALSE)
	{
		if (I18n::$_enabled_langs === NULL)
		{
			$langs = I18n::config('enabled_langs');
			$langs = is_array($langs) ? $langs : array($langs);
			
			$lang_labels = I18n::config('lang_labels');

			$ready_langs = array();
			foreach ($langs as $lang)
			{
				$ready_langs[$lang] = Arr::get($lang_labels, $lang, $lang);
			}
			I18n::$_enabled_langs = $ready_langs;
		}

		$langs = I18n::$_enabled_langs;

		if ($keys_only)
		{
			$langs = array_keys($langs);
		}

		return $langs;
	}

	public static function available_langs($keys_only = FALSE, $associative = TRUE)
	{
		if (I18n::$_available_langs === NULL)
		{
			$langs = I18n::config('available_langs');
			$langs = is_array($langs) ? $langs : array($langs);

			$lang_labels = I18n::config('lang_labels');

			$ready_langs = array();
			foreach ($langs as $lang)
			{
				$ready_langs[$lang] = Arr::get($lang_labels, $lang, $lang);
			}
			I18n::$_available_langs = $ready_langs;
		}

		$langs = I18n::$_available_langs;

		if ($keys_only)
		{
			if ($associative)
			{
				foreach ($langs as $key => $lang)
				{
					$langs[$key] = $key;
				}
			}
			else
			{
				$langs = array_keys($langs);
			}
		}

		return $langs;
	}

	public static function available_langs_options($add_item = NULL, $add_as_first = TRUE)
	{
		$langs = I18n::available_langs();

		$options = array();

		// Add first item to select
		if ($add_as_first AND $add_item !== NULL AND is_array($add_item))
		{
			$options[$add_item[0]] = $add_item[1];
		}

		foreach ($langs as $lang => $label)
		{
			$options[$lang] = $label;
		}

		// Add last item to select
		if ( ! $add_as_first AND $add_item !== NULL AND is_array($add_item))
		{
			$options[$add_item[0]] = $add_item[1];
		}

		return $options;
	}

	public static function multilang($value = NULL)
	{
		if (I18n::$multilang === NULL)
		{
			I18n::$multilang = I18n::config('multilang');
		}
		if ($value !== NULL)
		{
			I18n::$multilang = (bool) $value;
		}
		return I18n::$multilang;
	}

	public static function multilang_media($value = NULL)
	{
		if (I18n::$multilang_media === NULL)
		{
			I18n::$multilang_media = I18n::config('multilang_media');
		}
		if ($value !== NULL)
		{
			I18n::$multilang_media = (bool) $value;
		}
		return I18n::$multilang_media;
	}

	public static function is_default_lang($lang = NULL)
	{
		if ($lang === NULL)
		{
			$lang = I18n::lang();
		}
		return $lang == I18n::default_lang();
	}

	public static function lang_available($lang)
	{
		return (Arr::get(I18n::available_langs(), $lang, NULL) !== NULL);
	}

	public static function lang_enabled($lang)
	{
		return (Arr::get(I18n::enabled_langs(), $lang, NULL) !== NULL);
	}

	/**
	 * Search for [:text:] patterns in text and perform translate on founded
	 * ones.
	 *
	 *     echo I18n::translate_content($text, $lang);
	 *
	 * [!!] This method is not foolproof since it uses regex to parse HTML.
	 *
	 * @param   string	text to auto link
	 * @param	string	target language
	 * @return  string
	 * @uses    HTML::anchor
	 */
	public static function translate_content($content, $lang = NULL)
	{
		if ($lang === NULL)
		{
			$lang = I18n::lang();
		}
		$original_lang = I18n::lang();

		I18n::lang($lang);

		try
		{
			$content = preg_replace_callback("~(\[:.[^\]]+:\])~i", 'I18n::_translate_content_callback', $content);

			// Restore language
			I18n::lang($original_lang);
		}
		catch (Exception $e)
		{
			// Restore language also when exception found
			I18n::lang($original_lang);
		}

		return $content;
	}

	protected static function _translate_content_callback($matches)
	{
		$str = str_replace(array('[:', ':]'), '', $matches[0]);

		return __($str);
	}

	/**
	 * Checks if a word is defined as uncountable. An uncountable word has a
	 * single form. For instance, one "fish" and many "fish", not "fishes".
	 *
	 *     Inflector::uncountable('fish'); // TRUE
	 *     Inflector::uncountable('cat');  // FALSE
	 *
	 * If you find a word is being pluralized improperly, it has probably not
	 * been defined as uncountable in `config/inflector.php`. If this is the
	 * case, please report [an issue](http://dev.kohanaphp.com/projects/kohana3/issues).
	 *
	 * @param   string   word to check
	 * @return  boolean
	 */
	public static function country($country = NULL, $lang = NULL)
	{
		if (I18n::$countries === NULL)
		{
			// Cache countries
			I18n::$countries = Kohana::config('i18n')->countries;
		}

		if ($country === NULL)
		{
			return I18n::$countries;
		}

		$country = strtoupper($country);

		if ( ! isset(I18n::$countries[$country]))
		{
			return NULL;
		}
		
		$value = I18n::$countries[$country];

		if ($lang === FALSE)
		{
			return $value;
		}

		return I18n::get($value, $lang);
	}

	public static function country_options($prefered = NULL, $lang = NULL)
	{
		$countries = I18n::country(NULL, $lang);
		
		if ($prefered !== NULL)
		{
			if ( ! is_array($prefered))
			{
				$prefered = array($prefered);
			}
			$p_countries = array();
			
			foreach ($prefered as $country)
			{
				if ( ! isset($countries[$country]))
				{
					continue;
				}
				// Copy prefered to array
				$p_countries[$country] = $countries[$country];
				// Delete it from all prefered
				unset($countries[$country]);
			}
			
			foreach ($countries as $country => $label)
			{
				$p_countries[$country] = $label;
			}
			
			$countries = $p_countries;
		}
		
		return $countries;
	}

} // End Core_I18n
