<?php defined('SYSPATH') or die('No direct script access.');

abstract class Core_Controller_Ext extends Controller
{
	/**
	 * @var boolean Internal or external request
	 */
	public $internal = FALSE;

	/**
	 * @var array   Actions accessible as internal-only
	 */
	public $internal_only = array();
	
	/**
	 * The before() method is called before your controller action.
	 * In our template controller we override this method so that we can
	 * set up default values. These variables are then available to our
	 * controllers if they need to be modified.
	 */
	public function before()
	{
		parent::before();
		
		// Check if internal request
		if ($this->request !== Request::instance())
		{
			$this->internal = TRUE;
		}

		// Check if internal-only request
		if (in_array($this->request->action, $this->internal_only)
			AND ! $this->internal)
		{
			throw new Kohana_Exception('Attempt to access internal uri `:uri` externally', array(
				':uri' => $this->request->uri
			));
		}
	}
	
}
