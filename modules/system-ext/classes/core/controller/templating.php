<?php defined('SYSPATH') or die('No direct script access.');

abstract class Core_Controller_Templating extends Controller_Template_Ext {

	/**
	 * @var		array	page configuration
	 */
	public $page_config;

	public $active_page;

	/**
	 * The before() method is called before your controller action.
	 * In our template controller we override this method so that we can
	 * set up default values. These variables are then available to our
	 * controllers if they need to be modified.
	 */
	public function before()
	{
		parent::before();

		// Save config to this object
		$this->page_config = $this->_page_config($this->request);

		// Set default template
		View::template($this->page_config['folder']);
		// If multilanguage site so search for i18n views in subdirectories
		if (I18n::multilang())
		{
			View::i18n(TRUE);
		}

		$this->active_page = $this->page_config['active_page'];
		View::bind_global('active_page', $this->active_page);
		View::set_global($this->page_config['params_global']);
		
		// Set template title
		$this->_template_title(array($this->page_config['title'], $this->page_config['subtitle']));
		
		$this->template = View::factory($this->page_config['template']);
		$this->template->set($this->page_config['params']);
		$this->template->page_config = $this->page_config;

		$this->template->template_styles = $this->page_config['styles'];
		foreach ($this->template->template_styles as $file => $attributes)
		{
			if ( ! is_array($attributes))
			{
				$this->template->template_styles[$file] = array('media' => $attributes);
			}
		}
		$this->template->template_scripts = $this->page_config['scripts'];

		$this->template_sections = $this->page_config['sections'];
		
		foreach ($this->template_sections as $section => $data)
		{
			// Just init template variable
			if ($data === NULL)
			{
				$this->template->{$section} = NULL;
			}
			// Section has only text content
			elseif (is_array($data))
			{
				$this->template->{$section} = __($data[0]);
			}
			// Load view
			else
			{
				$this->template->{$section} = View::factory($data);
			}
		}
	}

	/**
	 * Returns page config array for given request
	 * TODO - add parent parametr to configs
	 *
	 * @param	Request		$request
	 * @return	array		page config array
	 */
	protected function _page_config(Request $request)
	{
		$pages_config_default = Kohana::config('pages_default');
		if ( ! $pages_config_default)
		{
			throw new Kohana_Exception('Pages default config not found');
		}

		// Get views config
		$pages_config = Kohana::config('pages');
		if ( ! $pages_config)
		{
			$pages_config = array();
		}

		// Try to get page config from route name with i18n string if exists
		$page_config = Arr::get($pages_config, Route::name($request->route, TRUE, TRUE),
				// otherwise try to get by route name withou i18n string, and if not exists too use empty array
							Arr::get($pages_config, Route::name($request->route, FALSE), array()));
		// Merge with default config
		$page_config = array_merge(Object::as_array($pages_config_default), $page_config);

		// Merge sections
		foreach ($page_config['sections_add'] as $section => $data)
		{
			$page_config['sections'][$section] = $data;
		}
		foreach ($page_config['sections_remove'] as $section)
		{
			if (isset($page_config['sections'][$section]))
				unset($page_config['sections'][$section]);
		}

		// Merge styles
		foreach ($page_config['styles_add'] as $style => $data)
		{
			$page_config['styles'][$style] = $data;
		}
		foreach ($page_config['styles_remove'] as $style)
		{
			if (isset($page_config['styles'][$style]))
				unset($page_config['styles'][$style]);
		}

		// Merge scripts
		foreach ($page_config['scripts_add'] as $script)
		{
			if ( ! in_array($script, $page_config['scripts']))
				$page_config['scripts'][] = $script;
		}
		foreach ($page_config['scripts_remove'] as $script)
		{
			if (($key = array_search($script, $page_config['scripts'])) !== FALSE)
				unset($page_config['scripts'][$key]);
		}

		// Merge params
		foreach ($page_config['params_add'] as $section => $data)
		{
			$page_config['params'][$section] = $data;
		}
		foreach ($page_config['params_remove'] as $section)
		{
			if (isset($page_config['params'][$section]))
				unset($page_config['params'][$section]);
		}

		// Merge params global
		foreach ($page_config['params_global_add'] as $section => $data)
		{
			$page_config['params_global'][$section] = $data;
		}
		foreach ($page_config['params_global_remove'] as $section)
		{
			if (isset($page_config['params_global'][$section]))
				unset($page_config['params_global'][$section]);
		}

		if ($page_config['active_page'] === NULL)
		{
			$page_config['active_page'] = $this->_active_page();
		}
		
		return $page_config;
	}

	protected function _template_title($title = NULL, $append = TRUE, $translate = TRUE)
	{
		if ( ! empty($title))
		{
			if (is_array($title))
			{
				foreach ($title as $subtitle)
				{
					$this->_template_title($subtitle, $append, $translate);
				}
			}
			else
			{
				// translate title
				$title = __($title);

				if ( ! $append OR ! strlen($this->template_title))
				{
					$this->template_title = $title;
				}
				else
				{
					$this->template_title = implode(Arr::get($this->page_config, 'title_separator'), array(
						$this->template_title,
						$title
					));
				}
			}
		}

		return $this->template_title;
	}

	protected function _active_page()
	{
		return Route::name($this->request->route, FALSE);
	}
	
}
