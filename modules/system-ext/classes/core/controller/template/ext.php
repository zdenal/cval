<?php defined('SYSPATH') or die('No direct script access.');

abstract class Core_Controller_Template_Ext extends Controller_Template {

	/**
	 * @var boolean Internal or external request
	 */
	public $internal = FALSE;

	public $template_title = '';
	
	public $template_sections = array(
		'content'	=>	'',
	);
	
	public $template_styles = array();
	
	public $template_scripts = array();

	/**
	 * The before() method is called before your controller action.
	 * In our template controller we override this method so that we can
	 * set up default values. These variables are then available to our
	 * controllers if they need to be modified.
	 */
	public function before()
	{
		parent::before();

		// Check if internal request
		if ($this->request !== Request::instance() OR Request::$is_ajax)
		{
			$this->internal = TRUE;
		}
		
		if ($this->auto_render)
		{
			$this->_before_auto_render();
		}
	}

	/**
	 * The after() method is called after your controller action.
	 * In our template controller we override this method so that we can
	 * make any last minute modifications to the template before anything
	 * is rendered.
	 */
	public function after()
	{
		if ($this->auto_render)
		{
			$this->_after_auto_render();
		}
		parent::after();
	}
	
	/**
	 * Method called in before method if auto render is enabled
	 */
	protected function _before_auto_render()
	{
		// Initialize default values
		foreach($this->template_sections as $name=>$content)
		{
			$this->template->$name = $content;	
		}

		// Bind title to all views
		View::bind_global('template_title', $this->template_title);	

		// Initialize empty values
		$this->template->template_styles = array();
		$this->template->template_scripts = array();
	}
	
	/**
	 * Method called in after method if auto render is enabled
	 */
	protected function _after_auto_render()
	{
		$this->template->template_styles 	= array_merge( $this->template->template_styles, $this->template_styles );
		$this->template->template_scripts 	= array_merge( $this->template->template_scripts, $this->template_scripts );
	}
}
