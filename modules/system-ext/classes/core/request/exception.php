<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @package    Core
 * @category   Exceptions
 * @author     Zdenal
 */
class Core_Request_Exception extends Kohana_Exception {

	/**
	 * Creates a new translated exception.
	 *
	 *     throw new Kohana_Exception('Something went terrible wrong, :user',
	 *         array(':user' => $user));
	 *
	 * @param   string   error message
	 * @param   array    translation variables
	 * @param   integer  the exception code
	 * @return  void
	 */
	public function __construct($code = 460, $message = NULL, array $variables = NULL)
	{
		if ($message === NULL)
		{
			$message = isset(Request::$messages[$code]) ? Request::$messages[$code] : 'Undefined response code';
		}

		// Pass the message to the parent
		parent::__construct($message, $variables, $code);
	}

} // End Core_Request_Exception
