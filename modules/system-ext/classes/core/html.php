<?php defined('SYSPATH') or die('No direct access allowed.');

class Core_HTML extends Kohana_HTML {

	/**
	 * Close html in conditional comment.
	 *
	 *     echo HTML::conditon('gte IE 6', '<span>Test</span>');
	 *	Output:
	 *	   <!--[if gte IE 6]>
	 *	   <span>Test</span>
	 *	   <![endif]-->
	 *
	 * @param   string  condition
	 * @param   string  html
	 * @return  string
	 */
	public static function condition($condition, $html)
	{
		if ( ! empty($condition))
		{
			$html = '<!--[if '.$condition.']>'.PHP_EOL.$html.PHP_EOL.'<![endif]-->';
		}

		return $html;
	}

	/**
	 * Creates a style sheet link element.
	 * Style can be closed in conditional comment if you pass 'condition'
	 * parameter to $attributes array.
	 *
	 *     echo HTML::style('media/css/screen.css');
	 *
	 * @param   string  file name
	 * @param   array   default attributes
	 * @param   boolean  include the index page
	 * @return  string
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 * @uses	HTML::condition
	 */
	public static function style($file, array $attributes = NULL, $index = FALSE)
	{
		$condition = FALSE;

		if (isset($attributes['condition']))
		{
			$condition = $attributes['condition'];
			unset($attributes['condition']);
		}

		$html = parent::style($file, $attributes, $index);

		return HTML::condition($condition, $html);
	}

	/**
	 * Creates a script link.
	 *
	 *     echo HTML::script('media/js/jquery.min.js');
	 *
	 * @param   string   file name
	 * @param   array    default attributes
	 * @param   boolean  include the index page
	 * @return  string
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function script($file, array $attributes = NULL, $index = FALSE)
	{
		if (is_object($file) AND $file instanceof View)
		{
			// Set the script type
			$attributes['type'] = 'text/javascript';

			return '<script'.HTML::attributes($attributes).'>'."\n".$file->render()."\n".'</script>';
		}

		return parent::script($file, $attributes, $index);
	}

} // End Core_HTML