<?php

defined('SYSPATH') or die('No direct script access.');

class Core_Dir
{
	/**
	 * @var	int		Default permission for dir operation
	 */
	public static $permission = 0775;

	public static $ds_url = '/';

	public static function permission($permission = NULL)
	{
		if ($permission !== NULL)
		{
			Dir::$permission = $permission;
		}

		return Dir::$permission;
	}

	public static function ds($url = FALSE)
	{
		return $url ? Dir::$ds_url : DIRECTORY_SEPARATOR;
	}

	public static function is_writeable($directory)
	{
		if ( ! is_dir($directory) OR ! is_writable(realpath($directory)))
		{
			return FALSE;
		}
		return TRUE;
	}

	public static function create($path, $dirname = NULL, $permission = NULL)
	{
		if ($permission === NULL)
		{
			$permission = Dir::permission();
		}

		if ($dirname !== NULL)
		{
			if ( ! Dir::is_writeable($path))
			{
				return FALSE;
			}

			$path = realpath($path).DIRECTORY_SEPARATOR.$dirname;
		}
		else
		{
			$path = Dir::normalize($path);
		}

		// If already created
		if (is_dir($path))
		{
			return FALSE;
		}

		umask(0000);

		return mkdir($path, $permission, TRUE);
	}

	public static function delete($directory)
	{
		if ( ! Dir::is_writeable($directory))
		{
			return FALSE;
		}

		$directory = realpath($directory);

		Dir::delete_files($directory, TRUE);

		@rmdir($directory);

		return TRUE;
	}

	public static function rename($path, $old_dirname, $dirname)
	{
		$path = realpath($path);

		return rename($path.DIRECTORY_SEPARATOR.$old_dirname, $path.DIRECTORY_SEPARATOR.$dirname);
	}

	/**
	 * Delete Files
	 *
	 * Deletes all files contained in the supplied directory path.
	 * Files must be writable or owned by the system in order to be deleted.
	 * If the second parameter is set to TRUE, any directories contained
	 * within the supplied base directory will be nuked as well.
	 *
	 * @param	string	path to file
	 * @param	bool	whether to delete any directories found in the path
	 * @param	int		level
	 * @return	bool
	 */
	public static function delete_files($path, $del_dir = FALSE, $level = 0)
	{
		$path = realpath($path);

		if ( ! $current_dir = @opendir($path))
			return;

		while (FALSE !== ($filename = @readdir($current_dir)))
		{
			if ($filename != "." and $filename != "..")
			{
				if (is_dir($path.DIRECTORY_SEPARATOR.$filename))
				{
					// Ignore empty folders
					if (substr($filename, 0, 1) != '.')
					{
						Dir::delete_files($path.DIRECTORY_SEPARATOR.$filename, $del_dir, $level + 1);
					}
				}
				else
				{
					unlink($path.DIRECTORY_SEPARATOR.$filename);
				}
			}
		}
		@closedir($current_dir);

		if ($del_dir == TRUE AND $level > 0)
		{
			@rmdir($path);
		}
	}

	public static function delete_file($directory, $file)
    {
		$path = realpath($directory).DIRECTORY_SEPARATOR.$file;
		
		if( ! file_exists($path))
		{
			return TRUE;
		}
		return unlink($path);
    }

	/**
	 * Get Filenames
	 *
	 * Reads the specified directory and builds an array containing the filenames.
	 * Any sub-folders contained within the specified path are read as well.
	 *
	 * @param	string	path to source
	 * @param	bool	whether to include the path as part of the filename
	 * @param	bool	internal variable to determine recursion status - do not use in calls
	 * @return	array
	 */
	public static function filenames($source_dir, $include_path = FALSE, $level = 1, $include_dirs = FALSE, $_recursion = FALSE)
	{
		static $_filedata = array();

		if ($fp = @opendir($source_dir))
		{
			// reset the array and make sure $source_dir has a trailing slash on the initial call
			if ($_recursion === FALSE)
			{
				$_filedata = array();
				$source_dir = rtrim(realpath($source_dir), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
			}

			while (FALSE !== ($file = readdir($fp)))
			{
				if (@is_dir($source_dir.$file) && strncmp($file, '.', 1) !== 0)
				{
					if ($level === TRUE OR $level > 1)
					{
						if ($level !== TRUE)
						{
							$level--;
						}
						Dir::filenames($source_dir.$file.DIRECTORY_SEPARATOR, $include_path, $level, $include_dirs, TRUE);
					}
					if ($include_dirs)
					{
						$_filedata[] = ($include_path == TRUE) ? $source_dir.$file : $file;
					}
				}
				elseif (strncmp($file, '.', 1) !== 0)
				{
					$_filedata[] = ($include_path == TRUE) ? $source_dir.$file : $file;
				}
			}
			return $_filedata;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Normalize the path.
	 * Replaces backslahes with DIRECTORY_SEPARATOR and appends 
	 * DIRECTORY_SEPARATOR if not present.
	 *
	 * @param	string	path to normalize
	 * @return	string	normalize path
	 */
	public static function normalize($path)
    {
		return rtrim(str_replace('\\', DIRECTORY_SEPARATOR, $path), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
    }

	/**
	 * Check if given path is absolute, ie. starts with DIRECTORY_SEPARATOR
	 *
	 * @param	string	path to check
	 * @return	bool
	 */
	public static function is_absolute($path)
    {
		return strpos($path, DIRECTORY_SEPARATOR) === 0;
    }

}