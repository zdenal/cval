<?php defined('SYSPATH') or die('No direct script access.');

class Num extends Kohana_Num 
{
	
	/**
	 * Gets number of decimal places from variable.
	 * If non numeric value given 0 is returned.
	 * 
	 * @param	mixed	Number to check
	 * @return	int
	 */
	public static function decimals($value)
	{
		if ( ! is_numeric($value))
		{
			return 0;
		}
		if ( (int) $value == $value)
		{
			return 0;
		}
		
		return strlen($value) - strrpos($value, '.') - 1;
	}
	
}
