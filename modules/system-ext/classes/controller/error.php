<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Error extends Controller_Template_Ext
{
	public $template = 'error/template';

	public function action_error()
	{
		// Grab the Error Code
		$code = (int) $this->request->param('code', $this->request->uri());
		// Grab the Main Request URI
		$page = $this->request->param('id', $this->request->uri());

		// Set the Request's Status to passed code
		$this->request->status = $code;

		if (isset($this->request->redirect_exception))
		{
			Request::$messages[$code] = $this->request->redirect_exception->getMessage();
		}
		elseif ( ! isset(Request::$messages[$code]))
		{
			Request::$messages[$code] = __('Undefined response code');
		}
		else
		{
			Request::$messages[$code] = __(Request::$messages[$code]);
		}

		$this->template_title = Request::$messages[$code];

		// Here we need to strip our error page's text from the request URI if it is there.
		$pos = strpos($page, 'error/'.$code.'/');
		if ($pos === 0)
		{
			$page = substr($page, 10);
		}

		$content = View::factory('error/global')
						->set('page', $page)
						->set('code', $code)
						->set('message', $this->template_title);

		$this->template->content = View::factory('common/mainbox')
						->set('content', $content);
	}

}
