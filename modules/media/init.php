<?php defined('SYSPATH') or die('No direct script access.');

// Get media path from config file
$path = rtrim(Kohana::config('media.path'), '/').DIRECTORY_SEPARATOR;
// Get media path url from config file
$path_url = rtrim(Kohana::config('media.path_url'), '/').'/';

// Set the full path to the media folder
define('MEDIAPATH', $path);
// Set relative path to media folder, must be accesible from web
define('MEDIAFOLDER', $path_url);

$upload_path = rtrim(Kohana::config('media.upload_path'), '/').DIRECTORY_SEPARATOR;
$upload_path_url = rtrim(Kohana::config('media.upload_path_url'), '/').'/';

define('UPLOADPATH', Dir::is_absolute($upload_path) ? $upload_path : MEDIAPATH.$upload_path);
define('UPLOADFOLDER', URL::is_absolute($upload_path_url) ? $upload_path_url : MEDIAFOLDER.$upload_path_url);

$tmp_upload_path = rtrim(Kohana::config('media.tmp_upload_path'), '/').DIRECTORY_SEPARATOR;
$tmp_upload_path_url = rtrim(Kohana::config('media.tmp_upload_path_url'), '/').'/';

define('UPLOADTMPPATH', Dir::is_absolute($tmp_upload_path) ? $tmp_upload_path : UPLOADPATH.$tmp_upload_path);
define('UPLOADTMPFOLDER', URL::is_absolute($tmp_upload_path_url) ? $tmp_upload_path_url : UPLOADFOLDER.$tmp_upload_path_url);

$model_upload_path = rtrim(Kohana::config('media.model_upload_path'), '/').DIRECTORY_SEPARATOR;
$model_upload_path_url = rtrim(Kohana::config('media.model_upload_path_url'), '/').'/';

define('UPLOADMODELPATH', Dir::is_absolute($model_upload_path) ? $model_upload_path : UPLOADPATH.$model_upload_path);
define('UPLOADMODELFOLDER', URL::is_absolute($model_upload_path_url) ? $model_upload_path_url : UPLOADFOLDER.$model_upload_path_url);

/*Route::set('media', $media_folder.'(/<file>)', array('file'=>'.+'))
	->defaults(array(
		'controller' => 'media',
		'action'     => 'file',
		'file'       => NULL,
	));*/

Route::set('media/download', 'media/download/<id>')
	->defaults(array(
		'controller' => 'media_download',
	));

