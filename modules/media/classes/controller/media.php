<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Media extends Controller {

	public function action_file()
	{
		$request = Request::instance();
		$file = $request->param('file');
		
		$cached_file = MEDIAPATH.$file;
		
		$cached_file_exists = file_exists($cached_file);

		$status = TRUE;

		if ( ! file_exists($cached_file))
		{
			$status = FALSE;

			// Get path info
			$info = pathinfo($file);

			// Extract extension
			$extension = $info['extension'];
			// Create file name without extension
			$file_no_ext = $info['dirname'].DIRECTORY_SEPARATOR.$info['filename'];

			// Try to find real file in Kohana hierarchy
			$real_file = Kohana::find_file('media', $file_no_ext, $extension);
			// If file is found
			if ($real_file !== FALSE)
			{
				// Make sure that all subdirectories for cache file are created
				// Otherwise create them
				$subdirs = explode('/', $info['dirname']);
				$dir = MEDIAPATH;
				foreach ($subdirs as $subdir)
				{
					$dir .= $subdir.DIRECTORY_SEPARATOR;
					if ( ! file_exists($dir))
					{
						mkdir($dir);
					}
				}

				$status = symlink($real_file, $cached_file);
			}
		}

		if ( ! $status AND I18n::multilang() AND I18n::lang_available(Arr::get($_REQUEST, 'lang', NULL)))
		{
			Kohana::$log->add(Kohana::ERROR, 'Media controller can be caled only in default site language, file: '.$file);
			$request->status = 404;
			$request->headers['Content-Type'] = File::mime_by_ext($extension);
		}
		elseif ( ! $status OR ! file_exists($cached_file))
		{
			Kohana::$log->add(Kohana::ERROR, 'Media controller error while loading file, '.$file);
			$request->status = 404;
			$request->headers['Content-Type'] = File::mime_by_ext($extension);
		}
		else
		{
			Request::instance()->redirect( Route::url('media', array('file'=>$file), TRUE) );
		}
	}

} // End Media
