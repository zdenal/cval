<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Media_Download extends Controller {

	public function action_index()
	{
		$request = Request::instance();
		$id = $request->param('id');
		
		$media = Jelly::select('media')
			->load($id);
		
		if ( ! $media->loaded())
		{
			throw new Kohana_Exception('Media with id :id not found', array(
				':id' => $id
			));
		}
		
		$location = $media->destination(NULL, FALSE);

		$request->send_file($location, $media->name);
	}

} // End Media_Download
