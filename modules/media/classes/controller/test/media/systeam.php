<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Media_Cval extends Controller_Ext
{

	public function action_format()
	{	
		$media_id = Arr::get($_REQUEST, 'id', 0);
		$media = Jelly::select('media')->load($media_id);
		if ( ! $media->loaded())
		{
			throw new Kohana_Exception('Media with id `:id` does not exists.', array(
					':id' => $media_id
			));
		}
		
		$url = $media->destination(Arr::get($_REQUEST, 'format'));
		
		$this->request->response = HTML::image($url);
	}

}

