<?php defined('SYSPATH') or die('No direct script access.');

class Jelly_Field_BelongsTo_Media extends Field_BelongsTo 
{
	/**
	 * @var  array  Array of allowed media types
	 */
	public $allowed_types = NULL;
	
	/**
	 * @var  array  Array of allowed media extensions
	 */
	public $allowed_extensions = NULL;
	
	/**
	 * @var	string	Name of media field in related model
	 */
	public $foreign_media_field = 'file';
	
	/**
	 * This is called after construction so that fields can finish
	 * constructing themselves with a copy of the column it represents.
	 *
	 * @param   string  $model
	 * @param   string  $column
	 * @return  void
	 **/
	public function initialize($model, $column)
	{
		parent::initialize($model, $column);

		// Set allowed extensions if not yet
		if ($this->allowed_extensions === NULL)
		{
			$field_media_cval = Jelly::meta($this->foreign['model'])->fields($this->foreign_media_field);
			
			$this->allowed_extensions = $field_media_cval->get_media_type_extensions($this->allowed_types);
		}
	}
	
	/**
	 * Called just after save() method on model. Now ID is known.
	 *
	 * @param   Jelly	$model
	 * @param   mixed	$value
	 * @param	bool	$loaded
	 * @return  mixed
	 */
	public function after_save_model(Jelly_Model $model, $value, $loaded)
	{
		$relation = Jelly::select('media_relation')
			->where('model', '=', $model->meta()->model())
			->where('model_id', '=', $model->id())
			->where('model_field', '=', $this->name)
			->load();
		
		if ($value)
		{
			if ( ! $relation->loaded())
			{
				// Set values if not loaded
				$relation->set(array(
					'model' => $model->meta()->model(),
					'model_id' => $model->id(),
					'model_field' => $this->name
				));
			}
			// Update media id
			$relation->media = $value;
			// Update or create relation if we have value
			$relation->save();
		}
		elseif ($relation->loaded())
		{
			// Delete relation if loaded and we have no value
			$relation->delete();
		}
		
		return parent::after_save_model($model, $value, $loaded);
	}
	
	/**
	 * Called just after deleting object.
	 *
	 * @param   Jelly_Model  $model
	 * @param   mixed  $value
	 * @return  void
	 */
	public function delete(Jelly_Model $model, $value) 
	{
		$relation = Jelly::select('media_relation')
			->where('model', '=', $model->meta()->model())
			->where('model_id', '=', $model->id())
			->where('model_field', '=', $this->name)
			->load();
		
		if ($relation->loaded())
		{
			// Delete relation if loaded and we have no value
			$relation->delete();
		}
		
		parent::delete($model, $value);
	}
	
}