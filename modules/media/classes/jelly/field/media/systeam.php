<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Cval media field
 *
 * @package  Cval
 */
abstract class Jelly_Field_Media_Cval extends Field_Media
{
	/**
	 * @var string	path to folder
	 */
	public $path = UPLOADPATH;

	/**
	 * @var string	path to folder which is accesible to web
	 */
	public $folder = UPLOADFOLDER;

	/**
	 * @var string	suffix appended to path and folder
	 */
	public $path_suffix = 'cval';

	/**
	 * @var string	suffix appended to tmp path and tmp folder
	 */
	public $tmp_path_suffix = 'cval';

	public $chmod_dir = NULL;

	/**
	 * @var string	Name for original version of uploaded file without extension
	 */
	public $original_version_name = 'orig';

	/**
	 * This is called after construction so that fields can finish
	 * constructing themselves with a copy of the column it represents.
	 *
	 * @param   string  $model
	 * @param   string  $column
	 * @return  void
	 **/
	public function initialize($model, $column)
	{
		parent::initialize($model, $column);

		// Set type rule if not set
		if ( ! Arr::path($this->upload, 'rules.Upload::type'))
		{
			if ( ! Arr::get($this->upload, 'rules'))
			{
				$this->upload['rules'] = array();
			}
			
			// Get extensions for all types
			$this->upload['rules']['Upload::type'] = array($this->get_media_type_extensions(NULL));
		}
	}
	
	/**
	 * Only saves new value to database.
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @param   bool   $loaded
	 * @return  string|NULL
	 */
	public function save($model, $value, $loaded)
	{
		// Get original value
		$original = $model->get($this->name, FALSE);

		// Save original file value to model
		$model->original_file = $original;

		// If we have temp file uploaded
		if (file_exists($this->tmp_path.$value))
		{
			// Indicate new file upload
			$model->new_file_uploaded = TRUE;
		}
		else
		{
			// Change value to original value
			$value = $original;
		}

		return $value;
	}

	/**
	 * Uploads a file if we have a valid upload
	 *
	 * @param   Jelly  $model
	 * @param   mixed  $value
	 * @param   bool   $loaded
	 * @return  string|NULL
	 */
	public function after_model_save(Model_Media $model, $value, $loaded)
	{
		if ( ! $loaded)
		{
			throw new Kohana_Exception('Uploaded media can\'t be saved when model is not saved.');
		}

		// Get original value
		$original = $model->original_file;

		// Construct temp file path
		$tmp_file = $this->tmp_path.$value;

		$new_file = FALSE;

		// If new value and temp path exists
		if ($model->new_file_uploaded)
		{
			// If temp path exists
			if (file_exists($tmp_file))
			{
				$file = $this->path.$model->destination_prefix(FALSE).$value;

				if ($this->_copy($tmp_file, $file, $value, $model, $loaded))
				{
					if ($this->chmod !== FALSE)
					{
						// Set permissions on filename
						$this->_chmod($file, $this->chmod);
					}

					if ($this->delete_old_file AND ! empty($original) AND $original != $value AND $original != $this->default)
					{
						$this->unlink($original, $this->path.$model->destination_prefix(FALSE).$original, $model, $loaded);
					}

					$new_file = TRUE;
				}
				else
				{
					throw new Kohana_Exception('File upload failed. Moving temp file from `:from` to `:to` was not successful.', array(
						':from' => $tmp_file,
						':to' => $file
					));
				}

				$this->_unlink_tmp($tmp_file);
			}
			else
			{
				throw new Kohana_Exception('File upload failed. Temp file `:file` does not exists.', array(
					':file' => $tmp_file
				));
			}
			
			if ( ! $this->transform_on_upload)
			{
				// We have a new file here
				$this->_upload_transform($value, $this->path.$model->destination_prefix(FALSE).$value, $model, $loaded, $new_file);
			}
		}

		// Mark new file uploaded as FALSE
		$model->new_file_uploaded = FALSE;
	}
	
	/**
	 * Gets media type from extension.
	 * 
	 * @param	string	File extension
	 * @return	string
	 * @throws	Kohana_Exception	When no type is defined for passed ext
	 */
	public function get_media_type_from_extension($ext)
	{
		$ext = strtolower($ext);
		
		foreach (Kohana::config('media.types') as $type => $exts)
		{
			if (in_array($ext, $exts))
			{
				return $type;
			}
		}
		
		throw new Kohana_Exception('Field `:field` in model `:model` does not support media with ext `:ext`.', array(
			':model' => $this->model,
			':field' => $this->name,
			':ext' => $ext
		));
	}
	
	/**
	 * Gets extensions for media type. If type is NULL gets extensions for all
	 * types.
	 * 
	 * @param	string	File type
	 * @return	array	Array of extension
	 * @throws	Kohana_Exception	When passed type is not defined in config
	 */
	public function get_media_type_extensions($type = NULL)
	{
		$extensions = array();
		
		$types = Kohana::config('media.types');
		if ($type !== NULL)
		{
			if ( ! is_array($type))
			{
				$type = array($type);
			}
			foreach ($type as $t)
			{
				if ( ! Arr::get($types, $t))
				{
					throw new Kohana_Exception('Field `:field` in model `:model` does not support media type `:ext`.', array(
						':model' => $this->model,
						':field' => $this->name,
						':type' => $t
					));
				}	
			}
			// Keep only specific type in types array
			$types = Arr::extract($types, $type);
		}
		foreach ($types as $type => $exts)
		{
			foreach ($exts as $ext)
			{
				$extensions[$ext] = $ext;
			}
		}
		
		return array_values($extensions);
	}

	protected function _copy($tmp_file, $file, $value, Jelly_Model $model, $loaded)
	{
		// First delete upload info
		$this->_upload_info_remove($model, $value);
		
		// Copy dir recursively
		return File::copy($tmp_file, $file);
	}

	protected function _chmod($file, $chmod)
	{
		// Set permissions on filename
		return File::chmod($file, $chmod, $this->chmod_dir);
	}

	protected function _unlink_tmp($tmp_file)
	{
		// remove dir recursively
		return Dir::delete($tmp_file);
	}

	public function unlink($file, $path, Jelly_Model $model, $loaded)
	{
		// remove dir recursively
		return Dir::delete($path);
	}

	public function destination_cval(Jelly_Model $model, $value, $format = NULL, $url = FALSE, $default = NULL, $orig_not_found_exception = TRUE)
	{
		$filename = $this->format($model, $value, $format, $orig_not_found_exception);

		if ($filename === NULL)
		{
			return NULL;
		}
		
		$value = $model->destination_prefix($url).$value.Dir::ds($url).$filename;

		return $this->destination($value, $url, $default);
	}

	public function destination_alternate_cval($prepend, Jelly_Model $model, $value, $format = NULL, $url = FALSE, $default = NULL, $orig_not_found_exception = TRUE)
	{
		$filename = $this->format($model, $value, $format, $orig_not_found_exception);
		
		if ($filename === NULL)
		{
			return NULL;
		}
		
		$value = $model->destination_prefix($url).$value.Dir::ds($url).$prepend.$filename;

		return $this->destination_alternate(NULL, $value, $url, $default);
	}

	public function format(Jelly_Model $model, $value, $format = NULL, $orig_not_found_exception = TRUE)
	{
		$orig_destination = $this->path.$model->destination_prefix(FALSE).$value.Dir::ds().$this->original_version_name.'.'.$model->ext;
		
		// First check if orig file exists
		if ( ! file_exists($orig_destination))
		{
			// If we should not throw expection on not existed
			if ( ! $orig_not_found_exception)
			{
				return NULL;
			}
			
			throw new Kohana_Exception('Original file `:file` not exists.', array(
				':file' => $orig_destination
			));
		}
		
		// default format should be always on server
		if ($format === NULL)
		{
			// So return just the name
			return $this->original_version_name.'.'.$model->ext;
		}

		// Get format options
		$options = Arr::get((array) Kohana::config('media.formats'), $format, FALSE);

		if ($options === FALSE)
		{
			throw new Kohana_Exception('Media format `:format` does not exists.', array(
				':format' => $format
			));
		}

		// Convert non array values to empty array
		if ( ! is_array($options))
		{
			$options = array();
		}

		// Construct filename
		$filename = Arr::get($options, 'filename', $format).'.'.Arr::get($options, 'ext', $model->ext);
		// Create prefixed filename
		$filename_prefixed = $model->destination_prefix(FALSE).$value.Dir::ds().$filename;
		// Get real path of file
		$path = $this->destination($filename_prefixed, FALSE);

		// File found return filename
		if ($path)
		{
			return $filename;
		}

		$options['target'] = $this->path.$filename_prefixed;
		// Procces transform
		$this->transform($value, $orig_destination, $model, $model->loaded(), TRUE, $options);
		
		return $filename;
	}

	public function upload_info(Jelly_Model $model, $uniqid)
	{
		if (empty($uniqid))
		{
			return FALSE;
		}

		$info_file = $this->_upload_info_file($model, $uniqid);

		if ( ! is_readable($info_file))
		{
			return FALSE;
		}

		return json_decode(File::read($info_file), TRUE);
	}

	public function upload_uniqid($name, $ext)
	{
		// Create unique id
		$uniqid = 'm'.$name.uniqid();
		// Create unique path
		$this->init_path('tmp_path_uniq', $this->tmp_path.$uniqid);

		// Construct original uploaded file name
		return $uniqid.DIRECTORY_SEPARATOR.$this->original_version_name.'.'.$ext;
	}

	protected function _upload_response(Jelly_Model $model, $uniqid, $upload)
	{
		$response = parent::_upload_response($model, $uniqid, $upload);

		// Get only uniqid for temp folder
		$upload_uniqid = Text::rstrtrim($uniqid, DIRECTORY_SEPARATOR.$this->original_version_name.'.'.$upload['ext']);
		// Save filename
		$this->upload_info_save($model, $upload_uniqid, $upload);
		// Replace name with unique id
		$response['name'] = Arr::get($upload, 'original_filename', $upload_uniqid);
		$response['value'] = $upload_uniqid;

		return $response;
	}

	protected function _upload_transform($file, $path, Jelly_Model $model, $loaded, $new_file = FALSE, $options = NULL)
	{
		// Transform from original file on upload
		$path .= Dir::ds().$this->original_version_name.'.'.$model->ext;

		parent::transform($file, $path, $model, $loaded, $new_file, $options);
	}

	public function upload_info_save(Jelly_Model $model, $uniqid, $upload)
	{
		File::write($this->_upload_info_file($model, $uniqid), json_encode($upload));
	}

	protected function _upload_info_remove(Jelly_Model $model, $uniqid)
	{
		File::delete($this->_upload_info_file($model, $uniqid));
	}

	protected function _upload_info_file(Jelly_Model $model, $uniqid)
	{
		return $this->tmp_path.$uniqid.DIRECTORY_SEPARATOR.'info';
	}

}
