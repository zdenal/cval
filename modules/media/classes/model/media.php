<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Category Model
 * @package Media
 * @author	Zdennal
 */
class Model_Media extends Jelly_Model
{

	/**
	 * URL of media which is returned when original media is not found.
	 */
	public static $not_found_media = '/cval/media/not_found_media.png';
	
	/**
	 * @var	string	File field destination prefix lazy loading variable
	 */
	protected $_destination_prefix;
	/**
	 * @var	bool	Tells if new file was uploaded
	 */
	public $new_file_uploaded = FALSE;
	/**
	 * @var	mixed	Here is saved original file value
	 */
	public $original_file;
	
	/**
	 * @var	Image 
	 */
	protected $_image;

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('name')
			->sorting(array('name' => 'ASC'))
			->fields(array(
				'id' => new Field_Primary,
				'name' => new Field_String(array(
					'null' => TRUE,
					'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(1000)
					)
				)),
				'file' => new Field_Media_Cval(array(
					'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(1000)
					),
					'upload' => array(
						'rules' => array(
							'Upload::valid' => NULL,
							'Upload::size' => array('5M')
						)
					),
					'after_set_callback' => array('Model_Media', '_after_set_file')
				)),
				'ext' => new Field_String(array(
					'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(10)
					)
				)),
				'type' => new Field_String(array(
					'rules' => array(
						'not_empty' => NULL,
						'max_length' => array(50)
					)
				)),
				'media_folder' => new Field_BelongsTo(array(
					'null' => TRUE,
				)),
				'relation_counter' => new Field_Integer(array(
					'default' => 0
				)),
				'version_hash' => new Field_String(array(
					'rules' => array(
						'max_length' => array(20)
					)
				))
			));

		// We have a file upload
		$meta->multipart(TRUE);

		Behavior_Idstringable::initialize($meta);
		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);

		// Set action fields
		$meta->action_fields(array(
			'admin/list' => array('name' => 'action', 'idstring' => 'action', 'media_folder' => 'action', 'type', 'ext'),
			'admin/edit' => array('name', 'idstring', 'file', 'media_folder'),
			'admin/new' => array('name', 'idstring', 'file', 'media_folder'),
		))->action_fields_modify(array(
			'admin/view' => array('ext' => array('grid' => 2)),
		))->action_fields_remove(array(
			'admin/view' => array('id')
		));
	}

	/**
	 * Callback called after set method is called to file field
	 *
	 * @param <type> $value
	 * @param Model_Media $model
	 * @return <type>
	 */
	public static function _after_set_file($value, Model_Media $model)
	{
		// Try to get upload info only when not uploaded actually, upload is with ajax
		if ( ! is_array($value) AND ($upload_info = $model->meta()->fields('file')->upload_info($model, $value)))
		{
			if ( ! $model->changed('ext'))
			{
				$model->ext = Arr::get($upload_info, 'ext');
				$model->type = $model->meta()->fields('file')->get_media_type_from_extension($model->ext);
			}
			if ( ! $model->changed('name'))
			{
				$model->name = Arr::get($upload_info, 'original_filename');
			}
		}

		return $value;
	}

	protected function _before_delete()
	{
		parent::_before_delete();
		
		$this->test_delete();
	}
	
	public function test_delete()
	{
		if ( ! $this->allow_delete())
		{
			throw new Kohana_Exception('Media can\'t be deleted, it is in use (:cntx).', array(
				':cnt' => $this->relation_counter
			));
		}
	}
	
	protected function _after_save()
	{
		$this->meta()->fields('file')->after_model_save($this, $this->file, $this->loaded());
	}

	public function destination($format = NULL, $url = TRUE, $default = NULL, $orig_not_found_exception = FALSE)
	{
		$destination = $this->meta()->fields('file')->destination_cval($this, $this->version_file(), $format, $url, $default, $orig_not_found_exception);
		
		if ($destination === NULL AND $url)
		{
			return Media::url(Model_Media::$not_found_media, FALSE);
		}
		
		return $destination;
	}

	/**
	 * Returns path appended to file field path or folder.
	 *
	 * @param	bool	$url
	 * @return  string
	 */
	public function destination_prefix($url = FALSE)
	{
		if ( ! $this->loaded())
		{
			return FALSE;
		}

		if ($this->_destination_prefix === NULL)
		{
			$file_field = $this->meta()->fields('file');
			// Get directory separator
			$ds = Dir::ds($url);

			// Convert string to id
			$id = (string) $this->id();
			$id_len = strlen($id);
			// Second param in max tells how many id digits will create subfolders
			$parts_cnt = min($id_len, 4);

			$path = NULL;
			// Insert single id digits as subfolders
			for ($i = 0; $i < $parts_cnt; $i ++ )
			{
				$path .= $id[$i].$ds;
			}

			// Insert remaining id part as whole dir
			if ($id_len > $parts_cnt)
			{
				$path .= substr($id, $parts_cnt).$ds;
			}

			$this->_destination_prefix = $path;
		}

		return $this->_destination_prefix;
	}
	
	public function has_preview()
	{
		return $this->type == 'photo';
	}
	
	public function preview($format = 'media_preview_medium')
	{
		return $this->destination('media_preview_medium');
	}
	
	public function name_parsed()
	{
		$index = strpos($this->name, ".");
		$name = $this->name;
		if($index)
		{
			$name = substr($name, 0, $index);
		}
		return str_replace('_', ' ', $name);
	}

	public function in_use()
	{
		return $this->relation_counter > 0;
	}
	
	public function allow_delete()
	{
		return ! $this->in_use();
	}
	
	public function version_file($version_hash = NULL, $with_ids = FALSE)
	{
		if ($version_hash === NULL)
		{
			$version_hash = $this->version_hash;
		}
		
		$path = NULL;
		
		if ($with_ids)
		{
			$path .= $this->destination_prefix(FALSE);
		}
		
		$path .= $this->file;
		
		if (strlen($version_hash))
		{
			$path .= Dir::ds(FALSE).$version_hash;
		}
		
		return $path;
	}
	
	public function update_version()
	{
		$file_field = $this->meta()->fields('file');
		
		// Create new version
		$new_version = uniqid();
		$has_version = strlen($this->version_hash);
		
		if ($has_version)
		{
			Dir::rename($file_field->path.Dir::ds().$this->destination_prefix(FALSE), $this->version_file(), $this->version_file($new_version));
		}
		else
		{
			$old_version_path = $file_field->path.Dir::ds().$this->version_file(NULL, TRUE).Dir::ds();
			$new_version_path = $file_field->path.Dir::ds().$this->version_file($new_version, TRUE).Dir::ds();
			// Create new version dir
			Dir::create($new_version_path);
			
			foreach (Dir::filenames($old_version_path) as $filename)
			{
				File::rename($old_version_path.$filename, $new_version_path.$filename);
			}
		}
		
		$this->version_hash = $new_version;
		$this->save();
		
		return $this;
	}
	
	/**
	 * Returns instance of image for this media. Used only for photo type.
	 * 
	 * @return	Image
	 * @throws	Kohana_Exception	If media is not photo or file not exists
	 */
	public function image()
	{
		if ($this->_image === NULL)
		{	
			if ($this->type != 'photo')
			{
				throw new Kohana_Exception('Image instance can\'t be created for media type `:type`.', array(
					':type' => $this->type
				));
			}
			
			$destination = $this->destination(NULL, FALSE);
			
			if (empty($destination))
			{
				throw new Kohana_Exception('Original file for media `:media` not exists.', array(
				':media' => $this->name
			));
			}
			
			$this->_image = Image::factory($destination);
		}
		
		return $this->_image;
	}
	
	/**
	 * Removes all file formats already created from orig file.
	 * Used when orig file is modified and therefore its derivated images also.
	 * 
	 * @return	$this
	 */
	public function delete_formats()
	{
		$file_field = $this->meta()->fields('file');
		$path = $file_field->path.Dir::ds().$this->version_file(NULL, TRUE).Dir::ds();
		$orig_name = 'orig.'.$this->ext;
		
		foreach (Dir::filenames($path) as $filename)
		{
			if ($filename != $orig_name)
			{
				File::delete($path.$filename);
			}
		}
		
		return $this;
	}
	
	/**
	 * URL for downloading this file with its name.
	 * 
	 * @return	string
	 */
	public function download_url()
	{
		return Route::url('media/download', array(
			'id' => $this->id()
		), TRUE, FALSE);
	}

}

// End Model_Media