<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Media_Folder
 * @package Media
 * @author	Zdennal
 */
class Model_Media_Folder extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta->name_key('label')
			->sorting(array('label' => 'ASC'))
			->fields(array(
				'id' => new Field_Primary,
				'parent_folder' => new Field_BelongsTo(array(
					'column' => 'parent_folder_id',
					'foreign' => 'media_folder',
					'null' => TRUE,
				)),
				'medias' => new Field_HasMany(array(
					'foreign' => 'media',
				)),
				'folders' => new Field_HasMany(array(
					'foreign' => 'media_folder.parent_folder_id',
				))
			));

		Behavior_Labelable::initialize($meta, array(
			'rules' => array(
				'not_empty' => NULL
			)
		));
		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);

		// Set action fields
		$meta->action_fields(array(
			'admin/list' => array('label' => 'action', 'parent_folder')
		))->action_fields_remove(array(
			'admin/new' => array('id', 'created_at', 'updated_at', 'created_by', 'updated_by'),
			'admin/edit' => array('id', 'created_at', 'updated_at', 'created_by', 'updated_by'),
			'admin/view' => array('id'),
		));
	}
	
	/**
	 * Deletes a single record.
	 *
	 * @param   $key  A key to use for non-loaded records
	 * @return  boolean
	 * */
	public function delete($key = NULL)
	{
		$this->delete_medias();
		
		return parent::delete($key);
	}
	
	protected function _before_delete($loaded = NULL)
	{
		parent::_before_delete($loaded);
		
		$this->test_delete();
	}
	
	/**
	 * Deletes included medias. It is not enclosed in transaction.
	 * It is not call from delete method.
	 * 
	 * @return	$this
	 */
	protected function delete_medias()
	{
		foreach ($this->get('medias')->execute() as $media)
		{
			try
			{
				if ($media->allow_delete())
				{
					$media->delete();
				}
			}
			catch (Exception $e) {}
		}
		
		return $this;
	}
	
	public function test_delete()
	{
		if ( ! $this->allow_delete())
		{
			throw new Kohana_Exception('Media folder can\'t be deleted, it contains another folder(s) or media.', NULL, 2);
		}
	}
	
	/**
	 * Test if folder can be deleted. I.E. has no folder inside.
	 * 
	 * @return	bool
	 */
	public function allow_delete()
	{
		return $this->get('folders')->count() < 1 AND $this->media_count() < 1;
	}
	
	/**
	 * Returns number of associated medias
	 * 
	 * @param	bool	TRUE get number of deletable medias
	 *					FALSE get number of not deletable medias
	 *					NULL [default] all medias
	 * @return	int
	 */
	public function media_count($for_delete = NULL)
	{
		$builder = $this->get('medias');
		
		if ($for_delete === TRUE)
		{
			$builder->where('relation_counter', '=', 0);
		}
		elseif ($for_delete === FALSE)
		{
			$builder->where('relation_counter', '>', 0);
		}
		
		return $builder->count();
	}
	
}

// End Model_Media_Folder