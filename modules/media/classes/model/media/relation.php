<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Model_Media_Relation
 * @package Media
 * @author	Zdennal
 */
class Model_Media_Relation extends Jelly_Model
{

	public static function initialize(Jelly_Meta $meta)
	{
		$meta
				->fields(array(
						'id' => new Field_String(array(
								'primary' => TRUE,
								'rules' => array(
										'not_empty' => NULL,
										'max_length' => array(254)
								)
						)),
						'model' => new Field_String(array(
								'rules' => array(
										'not_empty' => NULL,
										'max_length' => array(100)
								)
						)),
						'model_id' => new Field_String(array(
								'rules' => array(
										'not_empty' => NULL,
										'max_length' => array(100)
								)
						)),
						'model_field' => new Field_String(array(
								'rules' => array(
										'not_empty' => NULL,
										'max_length' => array(50)
								)
						)),
						'media' => new Field_BelongsTo(array(
								'rules' => array(
										'not_empty' => NULL
								)
						)),
				));

		Behavior_Timestampable::initialize($meta);
		Behavior_Userable::initialize($meta);
	}

	protected function _before_save()
	{
		parent::_before_save();

		// Construct primary key
		$parts = array(
				$this->model,
				$this->model_id,
				$this->model_field,
		);

		$this->id = implode('__', $parts);
	}

	protected function _after_validate_save($loaded = NULL)
	{
		if ($this->changed('media') AND $loaded)
		{
			$last_media_id = $this->_original['media'];
			$media = Jelly::select('media')
					->where('id', '=', $last_media_id)
					->load();
			if ($media->loaded())
			{
				$media->relation_counter = max(0, $media->relation_counter - 1);
				$media->save();
			}
		}
		
		parent::_after_validate_save($loaded);
	}
	
	protected function _after_save($loaded = NULL)
	{
		if ($this->last_changed('media'))
		{
			$this->media->relation_counter = $this->media->relation_counter + 1;
			$this->media->save();
		}
		
		parent::_after_save($loaded);
	}

	protected function _before_delete($loaded = NULL)
	{
		parent::_before_delete($loaded);
		
		$this->media->relation_counter = max(0, $this->media->relation_counter - 1);
		$this->media->save();
	}

}

// End Model_Media_Relation