<?php defined('SYSPATH') or die('No direct script access.');

class Core_Media_Cache {

	/**
	 * Finds the media file in all application, modules, system folders.
	 * Then create the cached file in media folder if cached file not exists
	 * or has different modification time.
	 * If Kohana::$caching = FALSE this is done only when cached file not exists
	 * Everytime it appends the cached modification file to it
	 * to reflect changes in files to visitors
	 *
	 * @param	string	name of file to cache
	 * @return  string	name of the cached filed
	 * @uses    Kohana::find_file
	 */
	public static function cache($file)
	{
		// Create absolute path for cached file
		$cached_file = MEDIAPATH.$file;

		$cached_file_exists = file_exists($cached_file);

		if ( ! $cached_file_exists OR ! Kohana::$caching)
		{
			// Get path info
			$info = pathinfo($file);

			// Extract extension
			$extension = $info['extension'];
			// Create file name without extension
			$file_no_ext = $info['dirname'].DIRECTORY_SEPARATOR.$info['filename'];

			// Try to find real file in Kohana hierarchy
			$real_file = Kohana::find_file('media', $file_no_ext, $extension);
			// If file is found
			if ($real_file !== FALSE)
			{
				// First set to not update the cached file
				$update_cached_file = FALSE;
				// Get modification time of the real file
				$real_file_mtime = filemtime($real_file);
				// Create absolute path for cached file
				$cached_file = MEDIAPATH.$file;
				// If cached file not exists yet
				if ( ! file_exists($cached_file))
				{
					// Make sure that all subdirectories for cache file are created
					// Otherwise create them - needed for new cached files
					$subdirs = explode('/', $info['dirname']);
					$dir = MEDIAPATH;
					foreach ($subdirs as $subdir)
					{
						$dir .= $subdir.DIRECTORY_SEPARATOR;
						if ( ! file_exists($dir))
						{
							mkdir($dir);
						}
					}
					$update_cached_file = TRUE;
				}
				// Or real file modification time differs from cached file
				// Need to be set to differing time not only older,
				// because some modules can be deleted and app file can be older than cache.
				elseif (filemtime($cached_file) != $real_file_mtime)
				{
					$update_cached_file = TRUE;
				}

				// If we need to create/update the cached file
				if ($update_cached_file)
				{
					// Copy it
					copy($real_file, $cached_file);
					// And set its modification time to real file time
					try
					{
						if ( ! touch($cached_file, $real_file_mtime))
						{
							throw new Kohana_Exception('Can\'t touch.');
						}
					}
					catch (Exception $e)
					{
						// If cant touch try to delete the file and create once more, then try to touch
						unlink($cached_file);
						copy($real_file, $cached_file);
						chmod($cached_file, 0777);
						touch($cached_file, $real_file_mtime);
					}
					// Set exist to TRUE
					$cached_file_exists = file_exists($cached_file);
				}
			}
		}

		// Return file path for url in media path and append its modification time
		return MEDIAFOLDER.$file.($cached_file_exists ? '?'.filemtime($cached_file) : '');
	}

	/**
	 * Creates an HTML anchor to a file. Note that the title is not escaped,
	 * to allow HTML elements within links (images, etc).
	 *
	 *     echo Media::file_anchor('doc/user_guide.pdf', 'User Guide');
	 *
	 * @param   string  name of file to link to
	 * @param   string  link text
	 * @param   array   HTML anchor attributes
	 * @param   string  non-default protocol, eg: ftp
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function file_anchor($file, $title = NULL, array $attributes = NULL, $protocol = NULL)
	{
		return HTML::file_anchor(Media::cache($file), $title, $attributes, $protocol);
	}

	/**
	 * Creates a style sheet link element.
	 *
	 *     echo Media::style('css/screen.css');
	 *
	 * @param   string  file name
	 * @param   array   default attributes
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function style($file, array $attributes = NULL)
	{
		return HTML::style(Media::cache($file), $attributes);
	}

	/**
	 * Creates a script link.
	 *
	 *     echo Media::script('js/jquery.min.js');
	 *
	 * @param   string   file name
	 * @param   array    default attributes
	 * @param   boolean  include the index page
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function script($file, array $attributes = NULL)
	{
		return HTML::script(Media::cache($file), $attributes);
	}

	/**
	 * Creates a image link.
	 *
	 *     echo Media::image('img/logo.png', array('alt' => 'My Company'));
	 *
	 * @param   string   file name
	 * @param   array    default attributes
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function image($file, array $attributes = NULL)
	{
		return HTML::image(Media::cache($file), $attributes);
	}

}
