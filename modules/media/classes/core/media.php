<?php defined('SYSPATH') or die('No direct script access.');

class Core_Media {

	protected static $_mediafolder_protocol;
	
	/**
	 * @var	int	Lifetime in seconds for tmp files
	 */
	protected static $_tmp_lifetime = 1800;

	/**
	 * Check if given file exists in mediapath in specific language or in global
	 * language and return its path or url according to second parameter
	 *
	 * @param	string	path to media file in media folder
	 * @param	string	return url or path
	 * @param	string	language for checking it
	 * @return	mixed	NULL when not found, path or url otherwise
	 */
	public static function destination($path, $url = TRUE, $lang = NULL)
	{
		$path = $path;
		$exists = Media::exists($path, $lang);

		if ( ! $exists)
		{
			return NULL;
		}

		if ($url)
		{
			return Media::url($path, $lang);
		}

		return $exists;
	}

	/**
	 * Check if given exists in mediapath in specific language or in global
	 * language
	 *
	 * @param	string	path to media file in media folder withou language part
	 * @param	string	language for checking it
	 * @return	mixed	FALSE when not found, path otherwise
	 */
	public static function exists($file, $lang = NULL)
	{
		if (Dir::is_absolute($file))
		{
			return file_exists($file) ? $file : FALSE;
		}

		if ($lang === NULL)
		{
			$lang = I18n::lang();
		}

		if ($lang !== FALSE AND file_exists(MEDIAPATH.$lang.DIRECTORY_SEPARATOR.$file))
		{
			return MEDIAPATH.$lang.DIRECTORY_SEPARATOR.$file;
		}
		if (file_exists(MEDIAPATH.$file))
		{
			return MEDIAPATH.$file;
		}

		return FALSE;
	}

	/**
	 * Return media url
	 *
	 * @param	string	name of file to cache
	 * @return  string	name of the cached filed
	 * @uses    URL::site
	 */
	public static function url($file, $lang = NULL)
	{
		// Do not try to convert object
		if (is_object($file))
		{
			return $file;
		}

		// Already has a protocol
		if (preg_match('~^[-a-z0-9+.]++://.*~u', $file))
		{
			return $file;
		}

		// Set lang to FALSE if multilang media is not enabled
		if ($lang === NULL AND ! I18n::multilang_media())
		{
			$lang = FALSE;
		}

		// Starts with slash, so it means not in media folder
		if (preg_match('~^/.*~u', $file))
		{
			return URL::site($file, TRUE, $lang);
		}

		// If not tested mediafolder protocol do so
		if (Media::$_mediafolder_protocol === NULL)
		{
			Media::$_mediafolder_protocol = preg_match('~^[-a-z0-9+.]++://.*~u', MEDIAFOLDER);
		}

		// If we have a protocol in mediafolder just concat
		if (Media::$_mediafolder_protocol)
		{
			return MEDIAFOLDER.$file;
		}

		return URL::site(MEDIAFOLDER.$file, TRUE, $lang);
	}

	/**
	 * Creates an HTML anchor to a file. Note that the title is not escaped,
	 * to allow HTML elements within links (images, etc).
	 *
	 *     echo Media::file_anchor('doc/user_guide.pdf', 'User Guide');
	 *
	 * @param   string  name of file to link to
	 * @param   string  link text
	 * @param   array   HTML anchor attributes
	 * @param   string  non-default protocol, eg: ftp
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function file_anchor($file, $title = NULL, array $attributes = NULL, $protocol = NULL)
	{
		return HTML::file_anchor(Media::url($file), $title, $attributes, $protocol);
	}

	/**
	 * Creates a style sheet link element.
	 *
	 *     echo Media::style('css/screen.css');
	 *
	 * @param   string  file name
	 * @param   array   default attributes
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function style($file, array $attributes = NULL)
	{
		return HTML::style(Media::url($file), $attributes);
	}

	/**
	 * Creates a script link.
	 *
	 *     echo Media::script('js/jquery.min.js');
	 *
	 * @param   string   file name
	 * @param   array    default attributes
	 * @param   boolean  include the index page
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function script($file, array $attributes = NULL)
	{
		return HTML::script(Media::url($file), $attributes);
	}

	/**
	 * Creates a image link.
	 *
	 *     echo Media::image('img/logo.png', array('alt' => 'My Company'));
	 *
	 * @param   string   file name
	 * @param   array    default attributes
	 * @return  string
	 * @uses    Media::cache
	 * @uses    URL::base
	 * @uses    HTML::attributes
	 */
	public static function image($file, array $attributes = NULL)
	{
		return HTML::image(Media::url($file), $attributes);
	}
	
	/**
	 * Removes tmp files from specified path.
	 *
	 * @param	string	Path where we want remove files
	 * @param	int		Lifetime in seconds for tmp files, older files are removed
	 */
	public static function delete_old_tmp_files($path, $lifetime = NULL)
	{
		if ($lifetime === NULL)
		{
			$lifetime = Media::$_tmp_lifetime;
		}
		
		// Get filenames including dirs
		$filenames = Dir::filenames($path, TRUE, 1, TRUE);

		foreach ($filenames as $filename)
		{
			$info = File::info($filename, 'date');

			if ($info === FALSE)
			{
				continue;
			}

			if ($info['date'] < (time() - $lifetime))
			{
				if (is_dir($filename))
				{
					Dir::delete($filename);
				}
				else
				{
					File::delete($filename);
				}
			}
		}
	}

}
