<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
	'path' => DOCROOT.'media', // Absolute path to media folder
	'path_url' => '/media', // Url to access path - relative from DOCROOT or absolute

	'upload_path' => 'uploads', // Path for saving uploaded files - relative from media path or absolute
	'upload_path_url' => 'uploads', // Url to access upload path - relative from path url or absolute

	'tmp_upload_path' => 'tmp', // Path for saving temporary uploaded files - relative from upload path or absolute
	'tmp_upload_path_url' => 'tmp', // Url to access tmp upload path - relative from upload path url or absolute

	// Model media field path and url. May containts model and field wildcards.
	'model_upload_path' => 'model/:model/:field', // Path for saving model media field uploaded files - relative from upload path or absolute
	'model_upload_path_url' => 'model/:model/:field', // Url to access model upload path - relative from path url or absolute

	'formats' => array(
		'media_preview_medium' => array(
			'ext' => 'jpg',
			'image' => array(
				'quality' => 90,
				'force' => TRUE,
				'methods' => array(
					'resize' => array(172),
				)
			)
		),
		'photo_editor' => array(
			'ext' => 'jpg',
			'image' => array(
				'quality' => 90,
				'force' => TRUE,
				'methods' => array(
					'resize' => array(600,400),
				)
			)
		),
	),
	
	/**
	 * Array of allowed file types and its extensions for upload with 
	 * Field_Media_Cval.
	 */
	'types' => array(
		'photo' => array('jpg','png','gif'),
		'video' => array('swf','mov','avi','wmv'),
		'archive' => array('zip','rar'),
		'file' => array('pdf','doc','docx','xls','xlsx'),
		'mp3' => array('mp3'),
	)
);