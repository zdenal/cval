<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Email module
 *
 * It is based on https://github.com/banks/kohana-email.git module
 *
 * @package    Email
 * @author     Zdennal
 */
abstract class Core_Email {

	/**
	 * @var	Swift_Mailer
	 */
	protected static $mail;
	
	/**
	 * Creates a SwiftMailer instance.
	 *
	 * @param   string  DSN connection string
	 * @return  object  Swift object
	 */
	public static function connect($config = NULL)
	{
		if ( ! class_exists('Swift_Mailer', FALSE))
		{
			// Load SwiftMailer
			require Kohana::find_file('vendor', 'swift/swift_required');
		}

		// Load default configuration
		($config === NULL) and $config = Kohana::config('email');
		
		switch ($config['driver'])
		{
			case 'smtp':
				// Set port
				$port = empty($config['options']['port']) ? 25 : (int) $config['options']['port'];
				
				// Create SMTP Transport
				$transport = Swift_SmtpTransport::newInstance($config['options']['hostname'], $port);

				if ( ! empty($config['options']['encryption']))
				{
					// Set encryption
					$transport->setEncryption($config['options']['encryption']);
				}
				
				// Do authentication, if part of the DSN
				empty($config['options']['username']) or $transport->setUsername($config['options']['username']);
				empty($config['options']['password']) or $transport->setPassword($config['options']['password']);

				// Set the timeout to 5 seconds
				$transport->setTimeout(empty($config['options']['timeout']) ? 5 : (int) $config['options']['timeout']);
			break;
			case 'sendmail':
				// Create a sendmail connection
				$transport = Swift_SendmailTransport::newInstance(empty($config['options']) ? "/usr/sbin/sendmail -bs" : $config['options']);

			break;
			default:
				// Use the native connection
				$transport = Swift_MailTransport::newInstance($config['options']);
			break;
		}

		// Create the SwiftMailer instance
		return Email::$mail = Swift_Mailer::newInstance($transport);
	}

	/**
	 * Try to get named email address from config.
	 *
	 * @param   string|array  sender email (and name) or named config
	 * @return  string|array  sender email (and name)
	 */
	public static function from($from)
	{
		if ($from === NULL)
		{
			$from = 'default';
		}
		if ( ! is_array($from) AND ($from_config = Kohana::config('email.from.'.$from)))
		{
			$from = $from_config;
		}
		if (is_array($from) AND isset($from['email']) AND isset($from['name']))
		{
			$from = array($from['email'], $from['name']);
		}

		return Email::_from_prepare($from);
	}
	
	/**
	 * Allows to pass from email without domain which then is automatically
	 * determined from server domain.
	 *
	 * @param   string|array  sender email (and name) or named config
	 * @return  string|array  sender email (and name)
	 */
	protected static function _from_prepare($from)
	{
		if (is_array($from))
		{
			$from[0] = Email::_add_domain($from[0]);
		}
		else
		{
			$from = Email::_add_domain($from);
		}
		
		return $from;
	}
	
	/**
	 * Adds domain same as server if email do not contains it.
	 * 
	 * @param	string	$email 
	 */
	protected static function _add_domain($email)
	{
		if (strpos($email, '@') === FALSE)
		{
			// Get domain from config file
			$domain = Kohana::config('email.default_from_domain');
			
			// If not found use SERVER_NAME variable
			if (empty($domain))
			{
				$domain = $_SERVER['SERVER_NAME'];
			}
			
			$email .= '@'.$domain;
		}
		return $email;
	}

	/**
	 * Send an email message.
	 *
	 * @param   string|array  recipient email (and name), or an array of To, Cc, Bcc names
	 * @param   string|array  sender email (and name) or named config
	 * @param   string        message subject
	 * @param   string        message body
	 * @param   boolean       send email as HTML
	 * @param	array		  array of Swift_Attachement instances or paths to files
	 * @return  integer       number of emails sent
	 */
	public static function send($to, $from, $subject, $message, $html = FALSE, $attachements = NULL)
	{
		// Connect to SwiftMailer
		(Email::$mail === NULL) AND Email::connect();

		// Get named email address from config
		$from = Email::from($from);

		// Determine the message type
		$html = ($html === TRUE) ? 'text/html' : 'text/plain';

		// Create the message
		$message = Swift_Message::newInstance($subject, $message, $html, 'utf-8');

		if (is_string($to))
		{
			// Single recipient
			$message->setTo($to);
		}
		elseif (is_array($to))
		{
			if (isset($to[0]) AND isset($to[1]))
			{
				// Create To: address set
				$to = array('to' => $to);
			}

			foreach ($to as $method => $set)
			{
				if ( ! in_array($method, array('to', 'cc', 'bcc'), true))
				{
					// Use To: by default
					$method = 'to';
				}

				// Create method name
				$method = 'add'.ucfirst($method);

				if (is_array($set))
				{
					// Add a recipient with name
					$message->$method($set[0], $set[1]);
				}
				else
				{
					// Add a recipient without name
					$message->$method($set);
				}
			}
		}

		if (is_string($from))
		{
			// From without a name
			$message->setFrom($from);
		}
		elseif (is_array($from))
		{
			// From with a name
			$message->setFrom($from[0], $from[1]);
		}
		
		// Add attachements
		if ( ! empty($attachements))
		{
			if ( ! is_array($attachements))
			{
				$attachements = array($attachements);
			}
			foreach ($attachements as $attachement)
			{
				if ( ! $attachement instanceof Swift_Attachment)
				{
					$attachement = Swift_Attachment::fromPath($attachement);
				}
				$message->attach($attachement);
			}
		}
		
		return Email::$mail->send($message);
	}

	/**
	 * Send an email message using defined template.
	 *
	 * @param   string|array  recipient email (and name), or an array of To, Cc, Bcc names
	 * @param   string        template key in config
	 * @param   string        template data
	 * @param   string        message subject
	 * @param   string|array  sender email (and name) or named config
	 * @param   boolean       send email as HTML
	 * @param	array		  array of Swift_Attachement instances or paths to files
	 * @return  integer       number of emails sent
	 */
	public static function send_template($to, $template, $data = array(), $subject = NULL, $from = NULL, $html = NULL, $attachements = NULL)
	{
		$config = Kohana::config('email.templates.'.$template);

		if ( ! $config)
		{
			throw new Kohana_Exception('Email template config for `:template` not found', array(
				':template' => $template
			));
		}

		if ( ! isset($config['subject']))	$config['subject']	= $subject;
		if ( ! isset($config['from']))		$config['from']		= $from;
		if ( ! isset($config['html']))		$config['html']		= $html;

		$message = View::factory(Arr::get($config, 'view'))->set($data)->render();

		return Email::send($to, Arr::get($config, 'from'), Arr::get($config, 'subject'), $message, Arr::get($config, 'html'), $attachements);
	}

} // End email