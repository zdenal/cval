<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(
	/**
	 * SwiftMailer driver, used with the email module.
	 *
	 * Valid drivers are: native, sendmail, smtp
	 */
	'driver' => 'native',
	
	/**
	 * To use secure connections with SMTP, set "port" to 465 instead of 25.
	 * To enable TLS, set "encryption" to "tls".
	 * 
	 * Note for SMTP, 'auth' key no longer exists as it did in 2.3.x helper
	 * Simply specifying a username and password is enough for all normal auth methods
	 * as they are autodeteccted in Swiftmailer 4
	 * 
	 * PopB4Smtp is not supported in this module as I had no way to test it but 
	 * SwiftMailer 4 does have a PopBeforeSMTP plugin so it shouldn't be hard to implement
	 * 
	 * Encryption can be one of 'ssl' or 'tls' (both require non-default PHP extensions
	 *
	 * Driver options:
	 * @param   null    native: no options
	 * @param   string  sendmail: executable path, with -bs or equivalent attached
	 * @param   array   smtp: hostname, (username), (password), (port), (encryption)
	 */
	'options' => NULL,

	/**
	 * This domain will be used when from email contains no '@' character.
	 * If this is empty $_SERVER['SERVER_NAME'] will be used.
	 */
	'default_from_domain' => NULL,
	
	/**
	 * Named sender email addresses.
	 * Each email address is defined with key, which is used in templates config.
	 * Email address may the following form:
	 *
	 *		Email sender is test@test.com
	 *		'test' => 'test@test.com'
	 *
	 *		Email sender is test@test.com and his name is Test
	 *		'test' => array('email' => 'test@test.com', 'name' => 'Test')
	 */
	'from' => array(
		'default' => array('email' => 'test@test.com', 'name' => 'Test'),
	),

	/**
	 * Email templates config.
	 * Each template is defined with key, which is used in Email class.
	 * Usage:
	 *
	 *		test' => array(
	 *			'from' => 'test',
	 *			'subject' => 'Test',
	 *			'view' => 'email/templates/test',
	 *			'html' => FALSE
	 *		),
	 *
	 * Definition of each variable:
	 *
	 *		from		string|array	sender email (and name) or named config
	 *		subject		string			message subject
	 *		view		string			view used for message body
	 *		html		boolean			send email as HTML
	 */
	'templates' => array()
);