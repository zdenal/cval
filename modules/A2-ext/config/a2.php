<?php

return array(

	/*
	 * The Authentication library to use
	 * Make sure that the library supports:
	 * 1) A get_user method that returns FALSE when no user is logged in
	 *	  and a user object that implements Acl_Role_Interface when a user is logged in
	 * 2) A static instance method to instantiate a Authentication object
	 *
	 * array(CLASS_NAME,array $arguments)
	 */
	'lib' => array(
		'class'  => 'auth', // (or AUTH)
	),

	/*
	 * The ACL Roles (String IDs are fine, use of ACL_Role_Interface objects also possible)
	 * Use: ROLE => PARENT(S) (make sure parent is defined as role itself before you use it as a parent)
	 */
	'roles' => A2::get_roles(),
	
	/**
	 * A2 uris
	 */
	'uris' => array(
		/**
		 * Not allowed uri, user is already logged in
		 */
		403 => 'auth/denied',
		/**
		 * Login uri
		 */
		401 => 'auth/login'
	)

);