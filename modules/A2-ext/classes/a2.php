<?php defined('SYSPATH') OR die('No direct access allowed.');
 
class A2 extends A2_Core {

	/**
	 * Redirect to uri found in A2 uris config
	 * 
	 * @param int	Exception status
	 */
	public static function exception_redirect($status)
	{
		// Get error URI
		$uri = Kohana::config('a2.uris.'.$status);
		// Try to get Route object
		$route = Route::get($uri, NULL, FALSE);

		// Redirect to route or uri 
		Request::instance()->redirect($route ? Route::url_protocol($uri) : $uri);
	}
	
	/**
	 * Merge roles set in A2 config with database roles
	 *
	 * @param array $acl_roles
	 * @return array
	 */
	public static function get_roles(array $acl_roles = array())
	{
		// For every role
		foreach (Jelly::select('role')->execute() as $role)
		{
			// If is defined already in config do not set it
			if (isset($acl_roles[$role->name]))
				continue;

			// Otherwise define it without parents
			$acl_roles[$role->name] = NULL;
		}

		return $acl_roles;
	}

	/**
	 * Do not use this method, it tries to create parent roles from db.
	 * TODO - create db structure like ACL roles
	 *
	 * @return array
	 */
	private static function get_roles_devel()
	{
		// Define roles array
		$roles = array();

		// For every role
		foreach (Jelly::select('role')->execute() as $role)
		{
			if ( ! isset($roles[$role->name]))
			{
				$roles[$role->name] = array();
			}

			if ($role->parent_role->loaded())
			{
				if ( ! isset($roles[$role->parent_role->name]))
				{
					$roles[$role->parent_role->name] = array();
				}
				array_push($roles[$role->parent_role->name], $role->name);
			}
		}
		
		// Define acl roles array
		$acl_roles = array();
		
		// Reformat roles to acl roles
		foreach ($roles as $role => $parents)
		{
			$acl_roles[$role] = count($parents) ? $parents : NULL;
		}

		return $acl_roles;
	}

	/**
	 * Check if user is logged in and as default throws exception if not
	 */
	public function check_login($throw_exception = TRUE)
	{
		if ( ! $this->logged_in())
		{
			if ($throw_exception)
			{
				throw new A2_Exception('Needs to be logged');
			}
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Check if logged in user (or guest) has access to resource/privilege.
	 * Send resource and privilege to A2_Exception message.
	 *
	 * @param   mixed     Resource
	 * @param   string    Privilege
	 * @param   boolean   Override exception handling set by config
	 * @return  boolean   Is user allowed
	 * @throws  A2_Exception   In exception modus, when user is not allowed
	 */
	public function allowed($resource = NULL, $privilege = NULL, $exception = NULL)
	{
		$result = NULL;

		try
		{
			$result = parent::allowed($resource, $privilege, $exception);
		}
		catch (A2_Exception $e)
		{
			$error = $resource !== NULL
				? $resource instanceof Acl_Resource_Interface ? $resource->get_resource_id() : (string) $resource
				: 'resource';

			$error .= '.' . ($privilege !== NULL
				? $privilege
				: 'default');

			if( ! $message = Kohana::message('a2', $error))
			{
				// specific message not found - use default
				$message = Kohana::message('a2', 'default');
			}

			list($resource_string, $privilege_string) = explode('.', $error);

			throw new A2_Exception($message, array(
				':resource' => $resource_string,
				':privilege' => $privilege_string
			));
		}

		return $result;
	}
	
}