<?php defined('SYSPATH') or die('No direct script access.');

class Pagination extends Kohana_Pagination {

	/**
	 * Returns the pagination links as array.
	 *
	 * @return  array	same data as are rendered to view by render()
	 * @return	NULL	when auto_hide is TRUE and total pages <= 1
	 */
	public function render_array($force = FALSE)
	{
		// Automatically hide pagination whenever it is superfluous and not forced
		if ( ! $force AND $this->config['auto_hide'] === TRUE AND $this->total_pages <= 1)
			return NULL;

		return array_merge(get_object_vars($this), array('page' => $this));
	}

	/**
	 * Renders the pagination links to View object.
	 *
	 * @param   mixed   string of the view to use, or a Kohana_View object
	 * @return  View	View object with same data as rendered to view by render()
	 */
	public function render_view($view = NULL, $force = FALSE)
	{
		// Automatically hide pagination whenever it is superfluous and not forced
		if ( ! $force AND $this->config['auto_hide'] === TRUE AND $this->total_pages <= 1)
			return NULL;

		if ($view === NULL)
		{
			// Use the view from config
			$view = $this->config['view'];
		}

		if ( ! $view instanceof Kohana_View)
		{
			// Load the view file
			$view = View::factory($view);
		}

		// Pass on the whole Pagination object
		return $view->set(get_object_vars($this))->set('page', $this);
	}

	public function show()
	{
		return ! ($this->config['auto_hide'] === TRUE AND $this->total_pages <= 1);
	}

}