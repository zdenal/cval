<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'lorem-ipsum'	=> COREMODPATH.'lorem-ipsum', // Lorem Ipsum generator
	'pagination-ext'=> COREMODPATH.'pagination-ext', // Pagination extended
	'pagination'	=> COREMODPATH.'pagination', // Pagination
	'email'			=> COREMODPATH.'email', // Jelly REST
	'jelly-rest'	=> COREMODPATH.'jelly-rest', // Jelly REST
	'restlike'		=> COREMODPATH.'restlike', // REST Like
	'secure-controller'	=> COREMODPATH.'secure-controller', // Secure Controller
	'jelly-generator'	=> COREMODPATH.'jelly-generator', // Jelly Generator SQL module
	'media'			=> COREMODPATH.'media', // OnDemand media serving
	'A2-ext'		=> COREMODPATH.'A2-ext',
	'A2'			=> COREMODPATH.'A2',  // Wouter's module
	'ACL'			=> COREMODPATH.'ACL',  // Wouter's module
	'jelly-auth-ext'=> COREMODPATH.'jelly-auth-ext',
	'jelly-auth'	=> COREMODPATH.'jelly-auth',
	'auth'			=> COREMODPATH.'auth',
	'jelly-behavior'=> COREMODPATH.'jelly-behavior',  // Behaviors
	'jelly-ext'		=> COREMODPATH.'jelly-ext',  // ORM
	'jelly'			=> COREMODPATH.'jelly',  // ORM
	'database-ext'	=> COREMODPATH.'database-ext',  // PDO transactions
	'database'		=> COREMODPATH.'database',  // default database driver
	'cache-ext'		=> COREMODPATH.'cache-ext',
	'cache'			=> COREMODPATH.'cache',
	'system-ext'	=> COREMODPATH.'system-ext',  // Core modifications
);
	
