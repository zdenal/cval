<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'cval-calendar-icalendar' => CVALMODPATH.'calendar-icalendar', // iCalendar module
	'cval-calendar' => CVALMODPATH.'calendar', // Calendar module
	'cval-icalcreator' => CVALMODPATH.'icalcreator', // iCalcreator vendor class
	'cval-schedule' => CVALMODPATH.'schedule', // Schedule module
	'cval-contact' => CVALMODPATH.'contact', // Contact module
	'cval-settings' => CVALMODPATH.'settings', // Settings module
	'cval-location' => CVALMODPATH.'location', // Location module
	'cval-admin' => CVALMODPATH.'admin', // Admin module
	'cval-remote' => CVALMODPATH.'remote', // Remote module
	'cval-comet' => CVALMODPATH.'comet', // Comet module
	'cval-auth' => CVALMODPATH.'auth', // Auth module
	'cval-presenter' => CVALMODPATH.'presenter', // Presenter module
	'cval-user' => CVALMODPATH.'user', // User module
	'cval-rest' => CVALMODPATH.'rest', // REST module
	'cval-firebug' => CVALMODPATH.'firebug', // Firebug Lite module
	'cval-system' => CVALMODPATH.'system', // System module
	//'cval-sharedmemory' => CVALMODPATH.'sharedmemory', // SharedMemory module
	'cval-test' => CVALMODPATH.'test', // Test module
);
	
