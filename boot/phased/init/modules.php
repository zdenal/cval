<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Define modules
 */
if(file_exists(BOOTPHASEDPATH.'modules'.EXT))
{
	$modules = array();
	$mpath = BOOTPHASEDPATH.'modules'.DIRECTORY_SEPARATOR;
	foreach (Kohana::load(BOOTPHASEDPATH.'modules'.EXT) as $module_file)
	{
		if (file_exists($mpath.$module_file.EXT))
		{
			$modules = array_merge($modules, Kohana::load($mpath.$module_file.EXT));	
		}
	}

	/**
	 * Enable modules. Modules are referenced by a relative or absolute path.
	 */
	Kohana::modules($modules);
	unset($mpath, $modules);
}
	
