<?php defined('SYSPATH') or die('No direct script access.');

// Controls request flow
// This is the last part of bootstrap

if (Kohana::$environment === Kohana::PRODUCTION)
{
	Kohana::$error_view = Kohana::config('cval.error.view');
}

if ( ! defined('SUPPRESS_REQUEST'))
{
	$request = NULL;

	try
	{
		$request = Request::instance();
		$request->execute();
		if ($request->send_headers()->response)
		{
			echo $request->response;
		}
	}
	catch (Exception $e)
	{
		Request::exception_handler($e, $request);
	}
}
