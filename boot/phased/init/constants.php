<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Define specific constants
 */
if(file_exists(BOOTPHASEDPATH.'constants'.EXT))
{
	foreach(Kohana::load(BOOTPHASEDPATH.'constants'.EXT) as $constant=>$value)
	{
		define($constant, $value);	
	}	
}
	
