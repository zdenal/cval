<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'environment',
	'config',
	'constants',
	'modules',
	'lang',
	'template',
	'routes',
	'request'
);
	
